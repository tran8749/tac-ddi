import os, sys, time, collections

Node = collections.namedtuple('Node',  ['pos','parent','token','tag'])

def main():
    sentences = []
    with open('output.conllu','r') as f:
        sentence_buffer = {}
        for l in f:
            if l.rstrip() == '':
                sentences.append(sentence_buffer)
                sentence_buffer = {}
            elif l.startswith('#'):
                continue
            else:
                row = l.rstrip().split('\t')
                pos = int(row[0])
                token = row[1]
                parent = int(row[6])
                tag = row[7]
                sentence_buffer[pos] = Node(pos,parent,token,tag)
    
    for tree in sentences:
        for n in tree.values():
            context = []
            p = get_parent(n,tree)
            if p is not None:
                context.append('{}/{}_inv'.format(p.token.lower(),n.tag))
            else:
                context.append('root/root')
            
            context.append(n.token.lower())
            
            for c in get_children(n,tree):
                if c.tag in ['det','punct','case','cc','aux','auxpass','conj']:
                    continue
                
                context.append('{}/{}'.format(c.token.lower(),c.tag))
                print(' '.join(reversed(context)))
                context.pop(-1)

def get_parent(node,tree):
    if node.parent == 0:
        return None
    else:
        return tree[node.parent]

def get_children(node,tree):
    children = []
    for n in sorted(tree.values(), key=lambda x: abs(x.pos-node.pos)):
        if n.parent == node.pos:
            children.append(n)

    return children

if __name__ == '__main__':
    main()