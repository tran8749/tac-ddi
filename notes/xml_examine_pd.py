import os, sys, time, random, re, json, collections, argparse
import xml.etree.ElementTree as ET
import numpy as np

parser = argparse.ArgumentParser(description='Process drug labels')
parser.add_argument('--input-dir', type=str, default='tac_training',
                    help='path to directory with drug label XML files')
parser.add_argument('--output', type=str, default=None,
                    help='file output; defaults to STD OUT')

def main(args):
    if args.output is None:
        args.output = sys.stdout
    else:
        args.output = open(args.output,'w')

    drug_generics = {}
    with open('../drug_generics.txt','r',encoding='utf-8') as f:
        for l in f:
            drugmap = l.strip().split('|')
            drug_generics[drugmap[0]] = drugmap[1:]

    if os.path.exists(args.input_dir):
        basenames = []
        for fname in os.listdir(args.input_dir):
            if not fname.endswith('.xml'):
                continue

            fpath = os.path.join(args.input_dir,fname)
            if 'nlm' in args.input_dir:
                parse_nlm180(fpath, args.output, drug_generics)
            else:
                parse_xmldata(fpath, args.output, drug_generics)
    else:
        print('what')
        exit()

def parse_xmldata(fpath, fout, drug_generics={}):
    base_tree = ET.parse(fpath)
    base_root = base_tree.getroot()
    sent_seen = []

    labeldrug = base_root.get('drug')
    setid = base_root.get('setid')
    
    for s in base_root.findall('Sentences/Sentence'):
        if s.get('id') in sent_seen:
            continue
        else:
            sent_seen.append(s.get('id'))

        orig_sent = s.find('SentenceText').text
        sent = mask_druglabel(orig_sent, labeldrug.split(' '))

        aliases = drug_generics[labeldrug.lower()]
        for alias in aliases:
            sent = mask_druglabel(sent, alias.split(' '))

        mentions = {}
        for m in s.findall('Mention'):
            segments = m.get('span').split(';')
            num_segs = len(segments)
            
            for seg in segments:
                mstart, mlen = seg.split()
                mtype = m.get('type')
                mend = int(mstart) + int(mlen) - 1
                mentions[m.get('id')] = (int(mstart), mend, mtype, num_segs)
                break
        
        interactions = []
        spans = []
        for r in s.findall('Interaction'):
            pid = r.get('precipitant')
            tid = r.get('trigger')
            eid = r.get('effect')
            if pid not in mentions:
                continue

            if r.get('type') == 'Pharmacodynamic interaction':
                if mentions[pid][3] == 1: # num_seg == 1?
                    interactions.append(('D', pid, mentions[pid]))

                    eid_list = eid.split(';')
                    for eid in eid_list:
                        _, _, _, span_segs = mentions[eid]
                        if span_segs == 1:
                            spans.append(('E', eid, mentions[eid]))
                    
                            #print(get_context_pd(mentions[pid], mentions[eid], sent))

            elif r.get('type')  == 'Pharmacokinetic interaction':
                if mentions[pid][3] == 1: # num_seg == 1?
                    interactions.append(('K', pid, eid))
        
        for k in spans:
            if k[0] != 'E':
                continue
            
            tag, pid, (e_start, e_end, _, _) = k
            effect = sent[e_start:e_end+1]
            print(effect)


def parse_nlm180(fpath, fout, drug_generics={}):
    base_tree = ET.parse(fpath)
    base_root = base_tree.getroot()
    mtypes = ['Precipitant','Trigger','SpecificInteraction']
    sent_seen = []

    labeldrug = base_root.find('labelDrug').get('name')
    if labeldrug == 'Missing':
        print("Skipping missing drug label @ {}".format(fpath))
        return

    #generic_str = base_root.find('generic').get('name')
    #aliases = re.findall(r'[a-zA-z][a-zA-z\-\s]+[a-zA-z]', 
    #                        generic_str.replace('and', ','))
    #print('\t'.join([labeldrug.lower()] + [ a.lower() for a in aliases ]))

    precipitant_types = ['Biomedical_Entity','Substance','Drug','Drug_Class']
    #print(fpath)
    for s in base_root.findall('sentence'):
        sent_id = "{}#{}".format(labeldrug.replace(' ','_'), s.get('id'))
        if sent_id not in sent_seen:
            sent_seen.append(sent_id)
        else:
            continue

        orig_sent = s.get('text')
        sent = mask_druglabel(orig_sent, labeldrug.split(' '))

        aliases = drug_generics[labeldrug.lower()]
        for alias in aliases:
            sent = mask_druglabel(sent, alias.split(' '))
        
        sent = sent.replace('<main>','@main ').replace('<item>','@item ')

        mentions = {}
        for m in s.findall('entity'):
            segments = m.get('charOffset').split('|')
            for seg in segments:
                mstart, mend = seg.split(':')
                mtype = m.get('type')
                mentions[m.get('id')] = (int(mstart), int(mend)-1, mtype, len(segments))
                break
        
        linkage = []
        interactions = []
        spans = []
        contexts_seen = []
        for r in s.findall('drugInteraction'):
            int_node = r.find('interaction')
            #print(r.get('id'))

            ents = {}
            for obj in int_node.find('relations').findall('relation'):
                if obj.find('entity') == None:
                    continue
                
                if obj.get('type') not in ents:
                    ents[obj.get('type')] = []
                
                ents[obj.get('type')].append(obj.find('entity').get('id'))
            
            participants = []
            if 'hasPrecipitant' in ents:
                participants += ents['hasPrecipitant']
            
            if 'hasObject' in ents:
                participants += ents['hasObject']

            for pid in participants:
                if pid not in mentions:
                    continue
                
                startp, endp, mtype, ent_segs = mentions[pid]

                # Ignore cases where label drug itself is a precipitant
                if ('XX' in sent[startp:endp] and mtype in precipitant_types):
                    continue

                tid = int_node.get('trigger')
                _, _, int_type, int_segs = mentions[tid]
                
                if int_type == 'Specific_Interaction':
                    if ent_segs == 1:
                        interactions.append(('D', pid, mentions[pid]))
                        linkage.append((pid,tid))

                    if int_segs == 1:
                        spans.append(('E', tid, mentions[tid]))

                elif int_type in ['Decrease_Interaction','Increase_Interaction']:
                    if ent_segs == 1:
                        interactions.append(('K', pid, int_type))
                    
                    if int_segs == 1:
                        spans.append(('T', tid, mentions[tid]))

                elif int_type == 'Caution_Interaction':
                    if ent_segs == 1:
                        interactions.append(('U', pid, mentions[pid]))
                    
                    if int_segs == 1:
                        spans.append(('T', tid, mentions[tid]))

                else:
                    print('Unknown type', m.get('type'))
                    continue
            
            pharmacokinetics = {}
            for k in spans:
                if k[0] != 'E':
                    continue
                
                tag, pid, (e_start, e_end, _, _) = k
                effect = sent[e_start:e_end+1]
                print(effect)


increase_codes = ['C54355','C54602','C54603','C54604','C54605','C54357','C54610','C54611','C54612','C54613']

code_map = {'C54355': 'INCREASED DRUG LEVEL',
            'C54602': 'INCREASED DRUG CMAX',
            'C54603': 'INCREASED DRUG HALF LIFE',
            'C54604': 'INCREASED DRUG TMAX',
            'C54605': 'INCREASED DRUG AUC',
            'C54357': 'INCREASED CONCOMITANT DRUG LEVEL',
            'C54610': 'INCREASED CONCOMITANT DRUG CMAX',
            'C54611': 'INCREASED CONCOMITANT DRUG HALF LIFE',
            'C54612': 'INCREASED CONCOMITANT DRUG TMAX',
            'C54613': 'INCREASED CONCOMITANT DRUG AUC',
            'C54356': 'DECREASED DRUG LEVEL',
            'C54606': 'DECREASED DRUG CMAX',
            'C54607': 'DECREASED DRUG HALF LIFE',
            'C54608': 'DECREASED DRUG TMAX',
            'C54609': 'DECREASED DRUG AUC',
            'C54358': 'DECREASED CONCOMITANT DRUG LEVEL',
            'C54615': 'DECREASED CONCOMITANT DRUG CMAX',
            'C54616': 'DECREASED CONCOMITANT DRUG HALF LIFE',
            'C54617': 'DECREASED CONCOMITANT DRUG TMAX',
            'C54614': 'DECREASED CONCOMITANT DRUG AUC' }
         

'''
INCREASED DRUG LEVEL                    C54355  freq=56 (8.36%)
INCREASED DRUG CMAX                     C54602  freq=5 (0.75%)
INCREASED DRUG HALF LIFE                C54603
INCREASED DRUG TMAX                     C54604
INCREASED DRUG AUC                      C54605  freq=5 (0.75%)

INCREASED CONCOMITANT DRUG LEVEL        C54357  freq=30 (4.48%)
INCREASED CONCOMITANT DRUG CMAX         C54610  freq=1 (0.15%)
INCREASED CONCOMITANT DRUG HALF LIFE    C54611  freq=2 (0.30%)
INCREASED CONCOMITANT DRUG TMAX         C54612
INCREASED CONCOMITANT DRUG AUC          C54613  freq=1 (0.15%)

DECREASED DRUG LEVEL                    C54356  freq=32 (4.78%)
DECREASED DRUG CMAX                     C54606
DECREASED DRUG HALF LIFE                C54607  freq=3 (0.45%)
DECREASED DRUG TMAX                     C54608
DECREASED DRUG AUC                      C54609

DECREASED CONCOMITANT DRUG LEVEL        C54358  freq=6 (0.90%)
DECREASED CONCOMITANT DRUG CMAX         C54615  freq=1 (0.15%)
DECREASED CONCOMITANT DRUG HALF LIFE    C54616
DECREASED CONCOMITANT DRUG TMAX         C54617
DECREASED CONCOMITANT DRUG AUC          C54614
'''

def guess_pk(context):
    rules = { 
            # increase 
            r'I[^D]+?H': 'C54611',
            r'I[^D]+?M': 'C54602',
            #r'O[^ID]+?[X|N][^D]+?I': 'C54357',
            #r'D[^I]+?V': 'C54357',
            r'U[^D]+?I': 'C54357',
            r'I[^D]+?U': 'C54357',
            r'X[^D]+?T': 'C54357',
            r'T[^D]+?X': 'C54357',
            #r'U[^I]+?[X|N]+[^I]+?D[^I]+?B[^I]+?N': 'C54355',
            r'[I|T][^D]+?X': 'C54355',  
            r'X[^D]+?[I|T]': 'C54355',

            # decrease
            r'D[^I]+?H': 'C54607',
            r'D[^I]+?M': 'C54615',
            r'U[^I]+?D': 'C54358',
            r'D[^I]+?U': 'C54358',
            r'D[^I]+?X': 'C54356',  
            r'X[^I]+?D': 'C54356',

            # catchall
            r'I': 'C54355',
            r'D': 'C54356',
         }

    labels = []
    for pattern, label in rules.items():
        if re.search(pattern,context):
            labels.append(label)
            break

    if 'C54607' in labels:
        labels.append('C54356')
    elif 'C54602' in labels:
        labels.append('C54605')

    return labels

def get_context_pk(prep, sent, drug_generics):
    drug_names = []
    for k, v in drug_generics.items():
        drug_names.append(k)
        drug_names += v

    pstart, pend, _, _ = prep
    mention = sent[pstart:pend+1]
    
    keywords = {'decrease': 'D',
                'decreasing': 'D',
                'reduce': 'D', 
                'reducing': 'D',
                'lower': 'D',
                'reduction': 'D',
                'inhibited': 'D',
                'inhibits': 'D',
                'inhibiting': 'D',
                'limit': 'D',
                'peak': 'M', 
                'cmax': 'M', 
                'auc': 'M', 
                'max': 'M',
                'toxicity': 'T',
                'half-life': 'H',
                'steady-state': 'H',
                 'increase': 'I', 
                 'increasing': 'I', 
                 'elevate': 'I',
                 'metabolism': 'U',
                 'metabolite': 'U',
                 'metabolize': 'U',
                 'peripheral': 'U',
                 'bioavailability': 'U',
                 'levels': 'U',
                 'oral': 'O',
                 'agent': 'N',
                 'drug': 'N',
                 'dose': 'V' }

    context = sent.lower().replace(mention.lower(),'X')
    for keyword, code in keywords.items():
        context = context.replace(keyword,code)
    
    for dname in drug_names:
        if dname in context:
            context = context.replace(dname,'N')

    return context

def get_context_pd(prep, effect, sent):
    pstart, pend, _, _ = prep
    fstart, fend, _, _ = effect
    prep = sent[pstart:pend+1]
    effect = sent[fstart:fend+1]
    context = sent[min(pstart,fstart):max(pend,fend)+1]
    context = context.replace(prep,'XXXXX')
    context = context.replace(effect,'YYYYY')
    return context

def mask_druglabel(sent, drug_tokens):
    if len(drug_tokens) == 1:
        match = re.search(drug_tokens[0], sent, re.IGNORECASE)
        if match:
            sent = sent.replace(match.group(),'X' * len(match.group()))
    else:
        pattern = '.+?'.join(drug_tokens)
        match = re.search(r'{}'.format(pattern), sent, re.IGNORECASE)
        if match:
            sent = sent.replace(match.group(),'X' * len(match.group()))
    
    return sent

if __name__ == '__main__':
    main(parser.parse_args())