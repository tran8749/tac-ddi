import os, sys, time, json, re
import urllib.request
url = "https://id.nlm.nih.gov/mesh/sparql?query=PREFIX%20rdf%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%3E%0D%0APREFIX%20rdfs%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0D%0APREFIX%20xsd%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F2001%2FXMLSchema%23%3E%0D%0APREFIX%20owl%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F2002%2F07%2Fowl%23%3E%0D%0APREFIX%20meshv%3A%20%3Chttp%3A%2F%2Fid.nlm.nih.gov%2Fmesh%2Fvocab%23%3E%0D%0APREFIX%20mesh%3A%20%3Chttp%3A%2F%2Fid.nlm.nih.gov%2Fmesh%2F%3E%0D%0APREFIX%20mesh2015%3A%20%3Chttp%3A%2F%2Fid.nlm.nih.gov%2Fmesh%2F2015%2F%3E%0D%0APREFIX%20mesh2016%3A%20%3Chttp%3A%2F%2Fid.nlm.nih.gov%2Fmesh%2F2016%2F%3E%0D%0APREFIX%20mesh2017%3A%20%3Chttp%3A%2F%2Fid.nlm.nih.gov%2Fmesh%2F2017%2F%3E%0D%0APREFIX%20mesh2018%3A%20%3Chttp%3A%2F%2Fid.nlm.nih.gov%2Fmesh%2F2018%2F%3E%0D%0A%0D%0ASELECT%20%3Fd%20%3FdName%20%0D%0AFROM%20%3Chttp%3A%2F%2Fid.nlm.nih.gov%2Fmesh%3E%0D%0AWHERE%20%7B%0D%0A%20%20%3Fd%20a%20meshv%3ADescriptor%20.%0D%0A%20%20%3Fd%20rdfs%3Alabel%20%3FdName%20.%0D%0A%20%20FILTER(REGEX(%3FdName%2C%27{}%27%2C%27i%27))%20%0D%0A%7D%20%0D%0AORDER%20BY%20%3Fd%20%0D%0A&format=JSON&limit=1&offset=0&inference=true"

def main():
    bam = {}
    with open('d2018.bin','r',encoding='utf-8') as f:
        term = None
        for l in f:
            if l.count('=') != 1:
                continue
            
            key, value = l.split('=')
            if key.strip() == 'MH':
                term = value.strip()
            elif key.strip() == 'MN':
                bam[value.strip()] = term

    drug_generics = {}
    for fname in ['drug_generics.txt','drug_generics2.txt']:
        with open(fname,'r',encoding='utf-8') as f:
            for l in f:
                drugmap = l.strip().split('|')
                drug_generics[drugmap[0]] = drugmap[1:]

    with open('drug_alias.txt','w',encoding='utf-8') as f:
        for key, value in drug_generics.items():
            additionals = []
            for x in value:
                result = get_meshterm(x.replace(' ','%20'))
                if result == None:
                    continue

                palist, parents = result
                
                for p in parents:
                    if p in bam and bam[p].lower() not in additionals:
                        additionals.append(bam[p].lower())
                
                additionals += [ z.lower() for z in palist if z.lower() not in additionals ]
            
            additionals = augment(additionals)
            print(key, ','.join(value), ','.join(additionals), sep='|',file=f)

def augment(pa):
    aliases = { 
        'angiotensin-converting enzyme inhibitors':
            ['ace inhibitors'],
        'antihypertensive agents': 
            ['antihypertensive drugs',
             'antihypertensives'],
        'anti-arrhythmia agents': 
            ['anti-arrhythmia agents',
             'anti-arrhythmia drugs',
             'anti-arrhythmias',
             'antiarrhythmia agents',
             'antiarrhythmia drugs',
             'antiarrhythmias'],
        'diuretics':
            ['diuretic agents',
             'diuretic drugs'],
        'vasodilator agents':
            ['vasodilators'],
        'antidepressive agents':
            ['antidepressives']
         }

    synonym_groups = [('agents','drugs'),('blockers','antagonists','inhibitors')]

    new_pa = []
    for x in pa:
        if x in aliases:
            for z in aliases[x]:
                if z not in new_pa:
                    new_pa.append(z)
    
    for x in pa + new_pa:
        for g in synonym_groups:
            for z in get_variants(x,g):
                if z not in new_pa:
                    new_pa.append(z)

    return pa + [ x for x in new_pa if x not in pa ]

def get_variants(term,synonym_group):
    anchor = None
    for x in synonym_group:
        if x in term:
            anchor = x
    
    if anchor is None:
        return [term]
    
    variants = []
    for x in synonym_group:
        new_term = term.replace(anchor,x)
        if new_term not in variants:
            variants.append(new_term)
    
    return variants

def get_meshterm(query):
    contents = urllib.request.urlopen(url.format(query)).read()
    search_obj = json.loads(contents)
    
    print(query)
    if len(search_obj['results']['bindings']) == 0:
        return None
    
    term_url = search_obj['results']['bindings'][0]['d']['value']

    print('>> ' + term_url + '.json')
    contents = urllib.request.urlopen(term_url + '.json').read()
    obj = json.loads(contents)

    if 'pharmacologicalAction' not in obj['@graph'][0]:
        pa_list = []
    else:
        pa_list = obj['@graph'][0]['pharmacologicalAction']
        if isinstance(pa_list,str):
            pa_list = [pa_list]

    drug_classes = []
    for term_url in pa_list:
        print('  >>' + term_url + '.json')
        contents = urllib.request.urlopen(term_url + '.json').read()
        pa_obj = json.loads(contents)
        drug_classes.append(pa_obj['@graph'][0]['label']['@value'])

    parents = []
    print(term_url + '.json')
    tree_list = obj['@graph'][0]['treeNumber']
    if isinstance(obj['@graph'][0]['treeNumber'],str):
        tree_list = [tree_list]

    for x in tree_list:
        print(x)
        parents.append(strip_url(x))

    return (drug_classes, parents)

def strip_url(url):
    return re.search(r'([^/]+)$',url).group()

if __name__ == '__main__':
    main()