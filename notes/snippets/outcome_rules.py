import re


def guess_effects(prep,effects,context,total_preps):
    if len(effects) == 0:
        return []
    
    if len(effects) == 1 and total_preps == 1:
        return [effects[0][0]]
    
    print(context)
    print(prep)
    print(effects)


    return []

'''
INCREASED DRUG LEVEL                    C54355  freq=56 (8.36%)
INCREASED DRUG CMAX                     C54602  freq=5 (0.75%)
INCREASED DRUG HALF LIFE                C54603
INCREASED DRUG TMAX                     C54604
INCREASED DRUG AUC                      C54605  freq=5 (0.75%)

INCREASED CONCOMITANT DRUG LEVEL        C54357  freq=30 (4.48%)
INCREASED CONCOMITANT DRUG CMAX         C54610  freq=1 (0.15%)
INCREASED CONCOMITANT DRUG HALF LIFE    C54611  freq=2 (0.30%)
INCREASED CONCOMITANT DRUG TMAX         C54612
INCREASED CONCOMITANT DRUG AUC          C54613  freq=1 (0.15%)

DECREASED DRUG LEVEL                    C54356  freq=32 (4.78%)
DECREASED DRUG CMAX                     C54606
DECREASED DRUG HALF LIFE                C54607  freq=3 (0.45%)
DECREASED DRUG TMAX                     C54608
DECREASED DRUG AUC                      C54609

DECREASED CONCOMITANT DRUG LEVEL        C54358  freq=6 (0.90%)
DECREASED CONCOMITANT DRUG CMAX         C54615  freq=1 (0.15%)
DECREASED CONCOMITANT DRUG HALF LIFE    C54616
DECREASED CONCOMITANT DRUG TMAX         C54617
DECREASED CONCOMITANT DRUG AUC          C54614
'''

def guess_pk(context):
    rules = { 
            # increase 
            r'I[^D]+?H': 'C54611',
            r'I[^D]+?M': 'C54602',
            r'O[^ID]+?[X|N][^D]+?I': 'C54357',
            r'D[^I]+?V': 'C54357',
            r'U[^D]+?I': 'C54357',
            r'I[^D]+?U': 'C54357',
            r'X[^D]+?T': 'C54357',
            r'T[^D]+?X': 'C54357',
            r'U[^I]+?[X|N]+[^I]+?D[^I]+?B[^I]+?N': 'C54355',
            r'[I|T][^D]+?X': 'C54355',  
            r'X[^D]+?[I|T]': 'C54355',

            # decrease
            r'D[^I]+?H': 'C54607',
            r'D[^I]+?M': 'C54610',
            r'U[^I]+?D': 'C54358',
            r'D[^I]+?U': 'C54358',
            r'D[^I]+?X': 'C54356',  
            r'X[^I]+?D': 'C54356',

            # catchall
            r'I': 'C54355',
            r'D': 'C54356',
         }

    labels = []
    for pattern, label in rules.items():
        if re.search(pattern,context):
            labels.append(label)
            break

    if 'C54607' in labels:
        labels.append('C54356')
    elif 'C54602' in labels:
        labels.append('C54605')

    return labels

def get_context_pk(prep, sent, drug_names):
    pstart, pend, _, _ = prep
    mention = sent[pstart:pend+1]
    
    keywords = {'decrease': 'D',
                'decreasing': 'D',
                'reduce': 'D', 
                'reducing': 'D',
                'lower': 'D',
                'reduction': 'D',
                'inhibited': 'D',
                'inhibits': 'D',
                'inhibiting': 'D',
                'limit': 'D',
                'peak': 'M', 
                'cmax': 'M', 
                'auc': 'M', 
                'max': 'M',
                'toxicity': 'T',
                'half-life': 'H',
                'steady-state': 'H',
                 'increase': 'I', 
                 'increasing': 'I', 
                 'elevate': 'I',
                 'metabolism': 'U',
                 'metabolite': 'U',
                 'metabolize': 'U',
                 'peripheral': 'U',
                 'bioavailability': 'U',
                 'levels': 'U',
                 'oral': 'O',
                 'agent': 'N',
                 'drug': 'N',
                 'dose': 'V' }

    context = sent.lower().replace(mention.lower(),'X')
    for keyword, code in keywords.items():
        context = context.replace(keyword,code)
    
    for dname in drug_names:
        if dname in context:
            context = context.replace(dname,'N')

    return context