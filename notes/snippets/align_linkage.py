def align_linkage(linkage,alignment):
    print(linkage)

    nulltoken = 'X'
    mid2pos = {}
    for i, concept in enumerate(alignment):
        if concept is None:
            continue
        
        int_type, pid = concept
        if pid not in mid2pos:
            mid2pos[pid] = str(i)
    
    outcomes = []
    for concept in alignment:
        if concept is None:
            outcomes.append(nulltoken)
            continue
        int_type, pid1 = concept

        match = None
        for i, (pid2, effect) in enumerate(linkage):
            if pid1 == pid2:
                if isinstance(effect,list):
                    positions = [ mid2pos[pid] for pid in 
                            effect if pid in mid2pos ]
                    outcomes.append(':'.join(positions))
                else:
                    outcomes.append(effect)
                match = i
                break
        
        if match is not None:
            del linkage[i]
        else:
            outcomes.append(nulltoken)

    print(outcomes)

    return outcomes
