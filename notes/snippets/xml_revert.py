import os, sys, time, random, re, json, collections, argparse
import xml.etree.ElementTree as ET
from xml.dom import minidom
import outcome_rules

parser = argparse.ArgumentParser(description='Process drug labels')
parser.add_argument('--input', type=str, default=None,
                    help='file input, defaults to STDIN')
parser.add_argument('--output-dir', type=str, default='tac_prediction',
                    help='path to dump data')

def main(args):
    if args.input is None:
        dataset = load_data(sys.stdin)
    else:
        dataset = load_data(open(args.input,'r'))
    
    drug_labels = {}
    for (attribs, x), y in dataset:
        dname = attribs[0]
        if dname not in drug_labels:
            drug_labels[dname] = []

        drug_labels[dname].append((attribs,x,y))

    drug_names = []
    with open('drug_generics.txt','r',encoding='utf-8') as f:
        for l in f:
            x = l.strip().split('|')
            drug_names += [ z for z in x if len(z) > 0 ]

    for dname, sents in drug_labels.items():
        midc = 0
        intc = 0
        label_node = ET.Element('Label')
        ET.SubElement(label_node, 'Text')
        sents_node = ET.SubElement(label_node, 'Sentences')
        ET.SubElement(label_node, 'LabelInteractions')
        
        for attrib, x, y in sorted(sents):
            dname, sent_id, sect_id, set_id, orig_sent = attrib
            sent_node = ET.SubElement(sents_node, 'Sentence', 
                    attrib={'id': sent_id,
                            'section': sect_id, 
                            'LabelDrug': dname})

            senttext = ET.SubElement(sent_node, 'SentenceText')
            senttext.text = orig_sent
            
            mentions_span = extract_mentions(x,y,orig_sent)
            total_preps = len([ label for _,_,label,_ in mentions_span if label.endswith('D')])

            effects = []
            for start, end, label, span in mentions_span:
                midc += 1
                if label in ['T','E']:
                    if label == 'T':
                        mtype  = 'Trigger'
                    elif label == 'E':
                        effects.append((midc,(start, end, label, span)))
                        mtype = 'SpecificInteraction'

                    ment_node = ET.SubElement(sent_node, 'Mention', 
                                attrib={'id': 'M{}'.format(midc),
                                        'str': span, 
                                        'span': '{} {}'.format(start, end-start+1),
                                        'type': mtype,
                                        'code': 'NULL'})
                else:
                    midc += 1
                    ment_node = ET.SubElement(sent_node, 'Mention', 
                                attrib={'id': 'M{}'.format(midc),
                                        'str': span, 
                                        'span': '{} {}'.format(start, end-start+1),
                                        'type': 'Precipitant',
                                        'code': 'NULL'})

                    intc += 1
                    attr = {'id': 'I{}'.format(intc),
                                'precipitant': 'M{}'.format(midc),
                                'trigger': 'M{}'.format(midc) }
                    
                    if label.endswith('D'):
                        attr['type'] = 'Pharmacodynamic interaction'
                        
                        eids = outcome_rules.guess_effects((start, end, label, span), 
                                                                    effects, orig_sent,
                                                                    total_preps)

                        if len(eids) > 0:
                            attr['effect'] = ';'.join([ 'M{}'.format(midc) for midc in eids ])

                        int_node = ET.SubElement(sent_node, 'Interaction',attrib=attr)

                    elif label.endswith('K'):
                        context = outcome_rules.get_context_pk((start, end, label, span), 
                                                                orig_sent, drug_names)
                        
                        effects = outcome_rules.guess_pk(context)
                        attr['type'] = 'Pharmacokinetic interaction'

                        for effect in effects:
                            attr['id'] = 'I{}'.format(intc)
                            attr['effect'] = effect
                            int_node = ET.SubElement(sent_node, 'Interaction',attrib=attr)
                            intc += 1

                    elif label.endswith('U'):
                        attr['type'] = 'Unspecified interaction'
                        int_node = ET.SubElement(sent_node, 'Interaction',attrib=attr)
                    else:
                        raise Exception('Cant map label to correct interaction type')        
            
        label_node.set('setid', set_id)
        label_node.set('drug', dname)
        
        if not os.path.exists(args.output_dir):
            os.mkdir(args.output_dir)
        
        fname = "{}_{}.xml".format(dname.replace(' ','_'), set_id)
        outfile = os.path.join(args.output_dir,fname) 
        with open(outfile,'w') as f:
            print(prettify(label_node), file=f)

def prettify(elem):
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def fix_iob(y):
    y_new = [x for x in y ]
    for i in range(len(y)):
        if y[i] != 'O':
            if i > 0:
                if y[i-1] == 'O':
                    y_new[i] = 'B-' + y[i][-1]
                elif y[i-1][-1] != y[i][-1]:
                    y_new[i] = 'B-' + y[i][-1]
            else:
                if not y[i].startswith('B'):
                    y_new[i] = 'B-' + y[i][-1]

        #print(y[i],'\t',y_new[i])

    return y_new

def extract_mentions(x,y,orig_sent):
    mentions = []
    spans = []

    y = fix_iob(y)

    for (start, end, w), tag in reversed(list(zip(x,y))):
        #print(tag, start, end, w)
        if tag != 'O':
            spans.append((tag, start, end, w))
            if tag.startswith('B'):
                mentions.append(spans)
                spans = []
    
    mentions_out = []
    for ment in mentions:
        #print(ment)
        minstart = min([ start for tag, start, end, w in ment ])
        maxend = max([ end for tag, start, end, w in ment ])
        label = set([ tag[-1] for tag, start, end, w in ment ])

        assert(len(label) == 1)
        label = label.pop()
        mentions_out.append((minstart, maxend, label, orig_sent[minstart:maxend+1]))
 
    return mentions_out 
    
def load_data(f):
    examples = []
    while True:
        header = f.readline()
        if header == "":
            break

        orig_sent = f.readline().strip()
        if orig_sent == "":
            break
        
        dname, sent_id, sect_id, set_id, num_tokens = header.split('\t')

        x = []
        y = []
        for i in range(int(num_tokens)):
            _, st, ed, label, tok = f.readline().rstrip().split('\t')
            x.append((int(st), int(ed), tok))
            y.append(label)

        examples.append((((dname, sent_id, sect_id, set_id, orig_sent), x), y))
        f.readline()

    return examples

if __name__ == '__main__':
    main(parser.parse_args())