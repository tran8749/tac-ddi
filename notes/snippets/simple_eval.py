import random
import sklearn.metrics
from sklearn.preprocessing import MultiLabelBinarizer

def extract_rpr(seq, typed=False):
    mentions = []
    spans = []
    pos = len(seq)
    for tag in reversed(seq):
        pos -= 1
        if tag != 'O':
            spans.append((pos, tag[-1]))
            if tag.startswith('B'):
                final_spans = []
                for p, t in sorted(spans):
                    if t[-1] != tag[-1]:
                        break
                    
                    final_spans.append((p,t))

                mentions.append(final_spans)
                spans = []
        else:
            spans = []
    
    reps = []
    for ment in mentions:
        label = ''.join(sorted(set([ tag[-1] for _, tag in ment ])))
        pos_start = str(min([ pos for pos, _ in ment ]))
        pos_end = str(max([ pos for pos, _ in ment ]))
        if typed:
            reps.append(':'.join([pos_start, pos_end, label]))
        else:
            reps.append(':'.join([pos_start, pos_end]))

    return reps

def eval_rpr(y_true,y_pred,typed):
    y_true = [extract_rpr(y_true,typed)]
    y_pred = [extract_rpr(y_pred,typed)]
    print(y_true)
    print(y_pred)
    m = MultiLabelBinarizer().fit(y_true + y_pred)
    y_true = m.transform(y_true)
    print(y_true)
    y_pred = m.transform(y_pred)
    print(y_pred)
    print(m.classes_)

    f = sklearn.metrics.f1_score(y_true,y_pred,average='micro')
    p = sklearn.metrics.precision_score(y_true, y_pred,average='micro')
    r = sklearn.metrics.recall_score(y_true, y_pred,average='micro')
    return f, p, r

y_true = ['O','B-X','B-M','I-M','I-M','O','B-X']
y_pred = ['O','B-X','B-X','I-X','I-X','O','I-X']

print(eval_rpr(y_true,y_pred,typed=True))
print(eval_rpr(y_true,y_pred,typed=False))