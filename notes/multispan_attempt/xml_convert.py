import os, sys, time, random, re, json, collections, argparse
import xml.etree.ElementTree as ET
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(description='Process drug labels')
parser.add_argument('--input-dir', type=str, default='tac_training',
                    help='path to directory with drug label XML files')
parser.add_argument('--output', type=str, default=None,
                    help='file output; defaults to STD OUT')

def tokenize(x):
    tokens_raw = x.split(' ')

    # split hyphens
    tokens = []
    for i, t in enumerate(tokens_raw):
        tokens += [ e for e in t.split('-') if e ]

    final = []
    for t in tokens: # fix the broken tokenization  
        fixed = False 
        if len(t) > 1:
            for symb in [',',':']:
                if t.endswith(symb):
                    t_fin = t[:-1].strip('.;][()')
                    if len(t_fin) == 0:
                        continue

                    final.append(t_fin)
                    final.append(symb)
                    fixed = True
                    break
            
        if not fixed:
            t_fin = t.strip('.;][()')
            if len(t_fin) == 0:
                continue
            
            final.append(t_fin)
                
    return final

def main(args):
    if args.output is None:
        args.output = sys.stdout
    else:
        args.output = open(args.output,'w')

    drug_generics = {}
    with open('drug_generics.txt','r',encoding='utf-8') as f:
        for l in f:
            drugmap = l.strip().split('|')
            drug_generics[drugmap[0]] = drugmap[1:]

    if os.path.exists(args.input_dir):
        basenames = []
        for fname in os.listdir(args.input_dir):
            if not fname.endswith('.xml'):
                continue

            fpath = os.path.join(args.input_dir,fname)
            if args.input_dir.startswith('nlm'):
                parse_nlm180(fpath, args.output, drug_generics)
            else:
                parse_xmldata(fpath, args.output, drug_generics)
    else:
        print('what')
        exit()

def mask_druglabel(sent, drug_tokens):
    if len(drug_tokens) == 1:
        match = re.search(drug_tokens[0], sent, re.IGNORECASE)
        if match:
            sent = sent.replace(match.group(),'X' * len(match.group()))
    else:
        pattern = '.+?'.join(drug_tokens)
        match = re.search(r'{}'.format(pattern), sent, re.IGNORECASE)
        if match:
            sent = sent.replace(match.group(),'X' * len(match.group()))
    
    return sent

def parse_xmldata(fpath, fout, drug_generics={}):
    base_tree = ET.parse(fpath)
    base_root = base_tree.getroot()
    sent_seen = []

    labeldrug = base_root.get('drug')
    setid = base_root.get('setid')

    print(fpath)
    for s in base_root.findall('Sentences/Sentence'):
        sent_id = "{}#{}".format(labeldrug.replace(' ','_'), s.get('id'))
        if sent_id not in sent_seen:
            sent_seen.append(sent_id)
        else:
            continue

        orig_sent = s.find('SentenceText').text
        sent = mask_druglabel(orig_sent, labeldrug.split(' '))

        aliases = drug_generics[labeldrug.lower()]
        for alias in aliases:
            sent = mask_druglabel(sent, alias.split(' '))

        mentions = dict()
        for m in s.findall('Mention'):
            segments = m.get('span').split(';')
            num_segs = len(segments)
            offsets = []
            for seg in segments:
                mstart, mlen = seg.split()
                mtype = m.get('type')
                mend = int(mstart) + int(mlen) - 1
                offsets.append((int(mstart), mend))

            mentions[m.get('id')] = (offsets, mtype)
        
        linkage = []
        interactions = []
        spans = []
        for r in s.findall('Interaction'):
            pid = r.get('precipitant')
            tid = r.get('trigger')
            eid = r.get('effect')
            if pid not in mentions:
                continue

            if r.get('type') == 'Pharmacodynamic interaction':
                eid_list = eid.split(';')

                interactions.append(('D', pid, mentions[pid]))
                for eid in eid_list:
                    linkage.append((pid,eid))

                for eid in eid_list:
                    spans.append(('E', eid, mentions[eid]))
                        

            elif r.get('type')  == 'Pharmacokinetic interaction':
                interactions.append(('K', pid, mentions[pid]))
                linkage.append((pid,eid))

                tid = tid.split(';')[0] # only one
                spans.append(('T', tid, mentions[tid]))
                
            elif r.get('type')  == 'Unspecified interaction':
                interactions.append(('U', pid, mentions[pid]))

                tid = tid.split(';')[0]
                spans.append(('T', tid, mentions[tid]))
            else:
                raise Exception('Undefined interaction type')

        tokens = using_split2(sent)
        int_alignment = align_mentions(interactions, tokens)
        int_joint_tags = apply_iobes(int_alignment)

        spans_alignment = align_mentions(spans, tokens)
        spans_joint_tags = apply_iobes(spans_alignment)
        
        pd_outcomes, pk_outcomes = align_linkage(linkage, int_alignment, spans_alignment)
        
        print(labeldrug, s.get('id'), s.get('section'), 
                setid, len(tokens), sep='\t', file=fout)
        print(orig_sent, file=fout)
        
        for i, (x, xstart, xend) in enumerate(tokens):
            nerlabs = []

            if set(x) == set('X'):
                x = 'XXXXXXXX'

            print(i, xstart, xend, int_joint_tags[i], spans_joint_tags[i], x, sep='\t', file=fout)
        
        linkpd = ' '.join(['D/' + ':'.join(v) for v in pd_outcomes])
        linkpk = ' '.join(['K/' + ':'.join(v) for v in pk_outcomes])
        link = linkpd + linkpk
        if link == '':
            link = 'NULL'
        print(link, file=fout)
        print('', file=fout)

    print("Processed {} sentences from {}".format(len(sent_seen), fpath), file=sys.stderr)

def align_mentions(interactions,tokens):
    tags = []
    for i, (x, xstart, xend) in enumerate(tokens):
        match = []
        for ptype, pid, concept in interactions:
            (offsets, mtype) = concept

            for j, (ystart, yend) in enumerate(offsets):
                if xstart >= ystart and xend <= yend and ptype not in match:
                    match.append((ptype,pid,j))

        match = list(set(match))
        if len(match) > 0:
            if len(tags) > 0 and tags[-1] in match:  
                # prioritize completing the span
                tags.append(tags[-1])
            else:
                # otherwise pick prioritize PD interactions
                # arbitrarily prioritize lower IDs
                ranked = sorted(match)
                tags.append(ranked[0])
        else:
            if i > 1 and tokens[i-1][0] == 'of' and x == 'XXXXXXXX' and tags[-1] == None:
                tags.pop(-1)
                tags.append(tags[-1])
                tags.append(tags[-1])
            else:
                tags.append(None)
    
    return tags

def align_linkage(linkage, int_alignment, spans_alignment):
    pd_drugs = []
    pd_effects = []
    pd_outcomes = []
    pk_outcomes = []
    alignment = zip(int_alignment,spans_alignment)
    
    mid2pos = {}
    for i, concepts in enumerate(alignment):
        for concept in concepts:
            if concept is None:
                continue
            
            int_type, pid, _ = concept
            if pid not in mid2pos:
                mid2pos[pid] = str(i)
            
            if int_type == 'D':
                pd_drugs.append(pid)
            elif int_type == 'E':
                pd_effects.append(pid)
            elif int_type == 'K':
                lookup = dict(linkage)
                if pid in lookup:
                    edge = (mid2pos[pid],lookup[pid])
                    pk_outcomes.append(edge)
    
    for pd_drug in pd_drugs:
        for pd_effect in pd_effects:
            edge = (mid2pos[pd_drug],mid2pos[pd_effect])
            
            if (pd_drug,pd_effect) in linkage:
                pd_outcomes.append((*edge,'1'))
            else:
                pd_outcomes.append((*edge,'0'))
    
    return sorted(set(pd_outcomes)), sorted(set(pk_outcomes))

def parse_nlm180(fpath, fout, drug_generics={}):
    base_tree = ET.parse(fpath)
    base_root = base_tree.getroot()
    mtypes = ['Precipitant','Trigger','SpecificInteraction']
    sent_seen = []

    labeldrug = base_root.find('labelDrug').get('name')
    if labeldrug == 'Missing':
        print("Skipping missing drug label @ {}".format(fpath))
        return

    #generic_str = base_root.find('generic').get('name')
    #aliases = re.findall(r'[a-zA-z][a-zA-z\-\s]+[a-zA-z]', 
    #                        generic_str.replace('and', ','))
    #print('\t'.join([labeldrug.lower()] + [ a.lower() for a in aliases ]))

    
    pk_labels = pd.read_csv('pk_bootstrap/pk.nlm180.tsv',sep='\t',header=None)
    pk_lookup = {}
    for i, d in pk_labels.iterrows():
        pk_lookup[(d[0],d[1])] = d[3]

    precipitant_types = ['Biomedical_Entity','Substance','Drug','Drug_Class']
    #print(fpath)
    for s in base_root.findall('sentence'):
        sent_id = "{}#{}".format(labeldrug.replace(' ','_'), s.get('id'))
        if sent_id not in sent_seen:
            sent_seen.append(sent_id)
        else:
            continue

        orig_sent = s.get('text')
        sent = mask_druglabel(orig_sent, labeldrug.split(' '))

        aliases = drug_generics[labeldrug.lower()]
        for alias in aliases:
            sent = mask_druglabel(sent, alias.split(' '))
        
        sent = sent.replace('<main>','@main ').replace('<item>','@item ')

        mentions = {}
        for m in s.findall('entity'):
            segments = m.get('charOffset').split('|')
            offsets = []
            for seg in segments:
                mstart, mend = seg.split(':')
                mtype = m.get('type')
                offsets.append((int(mstart), int(mend)-1))
            
            mentions[m.get('id')] = (offsets, mtype)
        
        linkage = []
        interactions = []
        spans = []
        for r in s.findall('drugInteraction'):
            int_node = r.find('interaction')
            #print(r.get('id'))

            ents = {}
            for obj in int_node.find('relations').findall('relation'):
                if obj.find('entity') == None:
                    continue
                
                if obj.get('type') not in ents:
                    ents[obj.get('type')] = []
                
                ents[obj.get('type')].append(obj.find('entity').get('id'))
            
            participants = []
            if 'hasPrecipitant' in ents:
                participants += ents['hasPrecipitant']
            
            if 'hasObject' in ents:
                participants += ents['hasObject']

            for pid in participants:
                if pid not in mentions:
                    continue
                
                startp, endp, mtype, ent_segs = mentions[pid]

                # Ignore cases where label drug itself is a precipitant
                if ('XX' in sent[startp:endp] and mtype in precipitant_types):
                    continue

                tid = int_node.get('trigger')
                offsets, int_type = mentions[tid]
                
                if int_type == 'Specific_Interaction':
                    interactions.append(('D', pid, mentions[pid]))
                    linkage.append((pid,tid))

                    spans.append(('E', tid, mentions[tid]))

                elif int_type in ['Decrease_Interaction','Increase_Interaction']:
                    interactions.append(('K', pid, mentions[pid]))
                    key = (s.get('id'), pid)
                    if key in pk_lookup:
                        linkage.append((pid,pk_lookup[key]))
                
                    spans.append(('T', tid, mentions[tid]))

                elif int_type == 'Caution_Interaction':
                    interactions.append(('U', pid, mentions[pid]))
                    spans.append(('T', tid, mentions[tid]))

                else:
                    print('Unknown type', m.get('type'))
                    continue

        tokens = using_split2(sent)

        alignment = align_mentions(interactions + spans, tokens)
        joint_tags = apply_iobes(alignment)
        pd_outcomes, pk_outcomes = align_linkage(linkage,alignment)

        print(labeldrug, s.get('id'), 'NO_SECTION', 
                'NO_SETID', len(tokens), sep='\t', file=fout)
        print(orig_sent, file=fout)
        
        for i, (x, xstart, xend) in enumerate(tokens):
            nerlabs = []

            if set(x) == set('X'):
                x = 'XXXXXXXX'

            print(i, xstart, xend, joint_tags[i],x, sep='\t', file=fout)

        linkpd = ' '.join(['D/' + ':'.join(v) for v in pd_outcomes])
        linkpk = ' '.join(['K/' + ':'.join(v) for v in pk_outcomes])
        link = linkpd + linkpk
        if link == '':
            link = 'NULL'
        print(link, file=fout)
        print("", file=fout)

    print("Processed {} sentences from {}".format(len(sent_seen), fpath), file=sys.stderr)

def apply_iobes(seq):
    tags = ['O'] * len(seq)

    #print('===============================================')
    #print(seq)

    for i in range(len(tags)):
        if seq[i] == None:
            tags[i] = 'O'
            continue

        ctype, cid, j = seq[i]
        if i == 0: # first cast
            if seq[i+1] is None or cid != seq[i+1][1]:
                tags[i] = 'B-' + ctype #'S-' + ctype
            else:
                tags[i] = 'B-' + ctype
        else:
            if seq[i-1] == None: # currently outside
                if i+1 < len(seq): # middle case
                    if seq[i+1] is None or cid != seq[i+1][1]:
                        tags[i] = 'B-' + ctype #'S-' + ctype
                    else:
                        tags[i] = 'B-' + ctype
                else: # last cast
                    tags[i] = 'B-' + ctype #'S-' + ctype

            else: # currently inside
                if i+1 < len(seq): # middle case
                    if cid == seq[i-1][1]:
                        if seq[i+1] is None or cid != seq[i+1][1]:
                            tags[i] = 'I-' + ctype #'E-' + ctype
                        else:
                            tags[i] = 'I-' + ctype
                    else:
                        tags[i] = 'B-' + ctype #'S-' + ctype
                else: # last case
                    if cid == seq[i-1][1]:
                        tags[i] = 'I-' + ctype #'E-' + ctype
                    else:
                        tags[i] = 'I-' + ctype
        
        if i > 1 and tags[i].startswith('B') and j != 0:
            tags[i] = 'I' + tags[i][1:]
            z = i-1
            while z > 0:
                if tags[z] == 'O':
                    tags[z] = 'G' + tags[i][1:]

                if tags[z-1].startswith('B'):
                    break
                
                z -= 1
                    

    #if len(set(seq)) > 3:
    #    print('waddup')
    #    print(tags)
    #    exit()
    
    return tags

# from stackexchange; user: aquavitae
def using_split2(line, _len=len):
    words = tokenize(line)
    index = line.index
    offsets = []
    append = offsets.append
    running_offset = 0
    for word in words:
        word_offset = index(word, running_offset)
        word_len = _len(word)
        running_offset = word_offset + word_len
        append((word, word_offset, running_offset - 1))
    return offsets

if __name__ == '__main__':
    main(parser.parse_args())