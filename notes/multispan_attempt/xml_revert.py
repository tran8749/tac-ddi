import os, sys, time, random, re, json, collections, argparse
import xml.etree.ElementTree as ET
from xml.dom import minidom

parser = argparse.ArgumentParser(description='Process drug labels')
parser.add_argument('--input', type=str, default=None,
                    help='file input, defaults to STDIN')
parser.add_argument('--output-dir', type=str, default='tac_prediction',
                    help='path to dump data')

def main(args):
    if args.input is None:
        dataset = load_data(sys.stdin)
    else:
        dataset = load_data(open(args.input,'r'))
    
    drug_labels = {}
    for (attribs, x), y in dataset:
        dname = attribs[0]
        if dname not in drug_labels:
            drug_labels[dname] = []

        drug_labels[dname].append((attribs,x,y))

    for dname, sents in drug_labels.items():
        midc = 0
        intc = 0
        label_node = ET.Element('Label')
        ET.SubElement(label_node, 'Text')
        sents_node = ET.SubElement(label_node, 'Sentences')
        ET.SubElement(label_node, 'LabelInteractions')
        
        for attrib, x, y in sorted(sents):
            y_prep, y_span = zip(*y)
            dname, sent_id, sect_id, set_id, orig_sent, linkage = attrib
            linkage_pd, linkage_pk = linkage

            sent_node = ET.SubElement(sents_node, 'Sentence', 
                    attrib={'id': sent_id,
                            'section': sect_id, 
                            'LabelDrug': dname})

            senttext = ET.SubElement(sent_node, 'SentenceText')
            senttext.text = orig_sent
            
            pos2mid = {}
            mentions_spanlist = extract_mentions(x,y_span,orig_sent)

            for mentions_span in mentions_spanlist:
                offsets = []
                spans = []
                pos = mentions_span[0][0]
                for _, start, end, label, span in mentions_span:
                    if label.endswith('T'):
                        mtype  = 'Trigger'
                    elif label.endswith('E'):
                        mtype = 'SpecificInteraction'
                    else:
                        raise Exception('inappropriate mention type')

                    offsets.append('{} {}'.format(start, end-start+1))
                    spans.append(span)

                midc += 1
                ment_node = ET.SubElement(sent_node, 'Mention', 
                            attrib={'id': 'M{}'.format(midc),
                                    'str': '|'.join(spans), 
                                    'span': ';'.join(offsets),
                                    'type': mtype,
                                    'code': 'NULL'})
                
                if label.endswith('E'):
                    pos2mid[pos] = midc

            mentions_preplist = extract_mentions(x,y_prep,orig_sent)
            
            for mentions_prep in mentions_preplist:
                offsets = []
                spans = []
                pos = mentions_prep[0][0]
                for _, start, end, label, span in mentions_prep:
                    if label.endswith('D'):
                        int_type = 'Pharmacodynamic interaction'
                    elif label.endswith('K'):
                        int_type = 'Pharmacokinetic interaction'
                    elif label.endswith('U'):
                        int_type = 'Unspecified interaction'
                    else:
                        raise Exception('inappropriate mention type')
                    
                    offsets.append('{} {}'.format(start, end-start+1))
                    spans.append(span)

                midc += 1
                ment_node = ET.SubElement(sent_node, 'Mention', 
                            attrib={'id': 'M{}'.format(midc),
                                    'str': '|'.join(spans), 
                                    'span': ';'.join(offsets),
                                    'type': 'Precipitant',
                                    'code': 'NULL'})
            
                attr = {    'type': int_type,
                            'precipitant': 'M{}'.format(midc),
                            'trigger': 'M{}'.format(midc) }

                if label.endswith('D'):
                    links = [ epos for mpos, epos, pred in linkage_pd 
                                if pos == mpos and pred == 1]
                    
                    if len(links) == 0:
                        pass # switch this on/off for submission
                        #intc += 1
                        #attr['id'] = 'I{}'.format(intc)
                        #int_node = ET.SubElement(sent_node, 
                        #            'Interaction', attrib=attr)
                    else:
                        mids = [ 'M{}'.format(pos2mid[eid]) for eid in links if eid in pos2mid ]
                        intc += 1
                        attr['id'] = 'I{}'.format(intc)
                        attr['effect'] = ';'.join(sorted(mids))
                        int_node = ET.SubElement(sent_node, 
                                    'Interaction', attrib=attr)

                elif label.endswith('K'):
                    links = [ eff for mpos, eff in linkage_pk 
                                if pos == mpos ]

                    for eff in links:
                        intc += 1
                        attr['id'] = 'I{}'.format(intc)
                        attr['effect'] = eff
                        int_node = ET.SubElement(sent_node, 
                                    'Interaction', attrib=attr)

                else:
                    intc += 1
                    attr['id'] = 'I{}'.format(intc)
                    int_node = ET.SubElement(sent_node, 'Interaction', 
                                                attrib=attr)
            
        label_node.set('setid', set_id)
        label_node.set('drug', dname)
        
        if not os.path.exists(args.output_dir):
            os.mkdir(args.output_dir)
        
        fname = "{}_{}.xml".format(dname.replace(' ','_'), set_id)
        outfile = os.path.join(args.output_dir,fname) 
        with open(outfile,'w') as f:
            print(prettify(label_node), file=f)

def prettify(elem):
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def extract_mentions(x,y,orig_sent):
    mentions = []
    spans = []

    #y = fix_iob(y)

    pos = len(x)
    for (start, end, w), tag in reversed(list(zip(x,y))):
        pos -= 1
        #print(tag, pos, start, end, w)
        if tag != 'O':
            spans.append((tag, pos, start, end, w))
            if tag.startswith('B'):
                mentions.append(spans)
                spans = []
    
    mentions_out = []
    for ment in mentions:
        ment = sorted(ment)
        segments = []
        start = 0
        i = 1
        while i < len(ment):
            if not ment[i-1][0].startswith('G') and ment[i][0].startswith('G'):
                segments.append(ment[start:i])
            if ment[i][0].startswith('G'):
                start = i+1
            
            i += 1
        
        segments.append(ment[start:i])
        mention_segs = []
        for seg in segments:
            label = set([ tag[-1] for tag, _, _, _, _ in seg ])
            pos = min([ pos for _, pos, _, _, _ in seg ])
            minstart = min([ start for _, _, start, _, _ in seg ])
            maxend = max([ end for _, _, _, end, _ in seg ])
            assert(len(label) == 1)
            label = label.pop()
            mention_segs.append((pos, minstart, maxend, label, 
                                orig_sent[minstart:maxend+1]))
        
        mentions_out.append(sorted(mention_segs))

    return mentions_out 
    
def load_data(f):
    cast_int = lambda x: tuple([ int(z) if not z.startswith('C') else z for z in x ])

    examples = []
    while True:
        header = f.readline()
        if header == "":
            break

        orig_sent = f.readline().strip()
        if orig_sent == "":
            break
        
        dname, sent_id, sect_id, set_id, num_tokens = header.split('\t')

        x = []
        y = []
        for i in range(int(num_tokens)):
            _, st, ed, label1, label2, tok = f.readline().rstrip().split('\t')
            x.append((int(st), int(ed), tok))
            y.append((label1,label2))

        linkages = f.readline().strip()
        
        pds = [ cast_int(z) for z in 
                re.findall(r'D\/([0-9]+):([0-9]+):([01])',linkages) ]
        pks = [ cast_int(z) for z in 
                re.findall(r'K\/([0-9]+):(C[0-9]+)',linkages) ]
        
        examples.append((((dname, sent_id, sect_id, set_id, 
                            orig_sent, (pds,pks)), x), y))
        f.readline()

    return examples

if __name__ == '__main__':
    main(parser.parse_args())