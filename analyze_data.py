import os, sys, time, random, re, json, collections
import xml.etree.ElementTree as ET
import numpy as np

def main():
    stats_dict = stats_xmldata('ddi_training')

    print("Number of Sentences per Drug Label:")
    print_stats(stats_dict['sentcount'])
    print("Number of words per Sentence:")
    print_stats(stats_dict['len'])
    print("Number of Mentions per Sentence:")
    print_freq(stats_dict['mentcount'],sortkey=0)
    print("Number of Spans per Mention:")
    print_freq(stats_dict['mspans'])
    print("Mention type distribution:")
    print_freq(stats_dict['mtype'])
    print("Number of Interactions per Sentence:")
    print_freq(stats_dict['intcount'],sortkey=0)
    print("Mention type of precipitant distribution:")
    print_freq(stats_dict['precip_type'])
    print("Number of triggers per precipitant:")
    print_freq(stats_dict['trigger_count'],sortkey=0)
    print("Mention type of trigger distribution (includes multi):")
    print_freq(stats_dict['trigger_type'])
    print("Number of effects per precipitant:")
    print_freq(stats_dict['effect_count'],sortkey=0)
    print("Mention type of effect distribution (includes multi):")
    print_freq(stats_dict['effect_type'])
    print("Interaction type distribution:")
    print_freq(stats_dict['int_type'])
    print("Interaction precip/trigger/effect type distribution:")
    print_freq(stats_dict['triple'])
    print("Interaction type and effect type distribution:")
    print_freq(stats_dict['int_type_effect'])

def print_freq(freq, sortkey = 1):
    rev = (sortkey == 1)

    total_freq = sum(freq.values())
    for k, v in sorted(freq.items(), key=lambda x: x[sortkey], reverse=rev):
        print(" >> {}, freq={} ({:.2%})".format(k,v, v/total_freq))
    
    if type(list(freq.keys())[0]) in [int,float]:
        total = sum( x*v for x, v in freq.items())
        print(" >> total = {}".format(total))
    else:
        print(" >> total = {}".format(total_freq))

def print_stats(freq):
    ex = []
    for x, v in freq.items():
        ex += [x] * v

    mean = np.mean(ex)
    pc0 = np.percentile(ex, 0)
    pc25 = np.percentile(ex, 25)
    pc50 = np.percentile(ex, 50)
    pc75 = np.percentile(ex, 75)
    pc100 = np.percentile(ex, 100)

    print(" >> mean: {:0.0f}, pctile: {:0.0f}/{:0.0f}/{:0.0f}/{:0.0f}/{:0.0f}"
            .format(mean, pc0, pc25, pc50, pc75, pc100))

def stats_xmldata(fdir):
    freq_dict = { 'sentcount': {}, 'len': {}, 
            'mspans': {}, 'mentcount': {}, 'intcount': {},
            'mtype': {}, 'precip_type': {}, 'trigger_type': {}, 
            'effect_type': {}, 'triple': {}, 'effect_count': {},
            'trigger_count': {}, 'int_type': {},
            'int_type_effect': {} }

    if os.path.exists(fdir):
        for fname in os.listdir(fdir):
            fpath = os.path.join(fdir,fname)
            base_tree = ET.parse(fpath)
            base_root = base_tree.getroot()
            seen = [] # workaround for sentence duplication bug
            
            for s in base_root.findall('Sentences/Sentence'):
                # workaround for sentence duplication bug
                if s.get('id') not in seen:
                    seen.append(s.get('id'))
                else:
                    continue

                tokens = using_split2(s.find('SentenceText').text)
                if len(tokens) not in freq_dict['len']:
                    freq_dict['len'][len(tokens)] = 1
                else:
                    freq_dict['len'][len(tokens)] += 1
                
                mentcount = len(s.findall('Mention'))
                if mentcount not in freq_dict['mentcount']:
                    freq_dict['mentcount'][mentcount] = 1
                else:
                    freq_dict['mentcount'][mentcount] += 1

                mentiontype = {}
                for m in s.findall('Mention'):
                    spans = m.get('span').split(';')
                    if len(spans) not in freq_dict['mspans']:
                        freq_dict['mspans'][len(spans)] = 1
                    else:
                        freq_dict['mspans'][len(spans)] += 1
                    
                    mtype = m.get('type')
                    if mtype not in freq_dict['mtype']:
                        freq_dict['mtype'][mtype] = 1
                    else:
                        freq_dict['mtype'][mtype] += 1
                    
                    mentiontype[m.get('id')] = m.get('type')
                
                intcount = len(s.findall('Interaction'))
                
                if intcount not in freq_dict['intcount']:
                    freq_dict['intcount'][intcount] = 1
                else:
                    freq_dict['intcount'][intcount] += 1
                
                for r in s.findall('Interaction'):
                    int_type = r.get('type')
                    if int_type not in freq_dict['int_type']:
                        freq_dict['int_type'][int_type] = 1
                    else:
                        freq_dict['int_type'][int_type] += 1

                    precip_type = mentiontype[r.get('precipitant')]
                    if precip_type not in freq_dict['precip_type']:
                        freq_dict['precip_type'][precip_type] = 1
                    else:
                        freq_dict['precip_type'][precip_type] += 1
                    
                    mids = r.get('trigger').split(';')
                    
                    if len(mids) not in freq_dict['trigger_count']:
                        freq_dict['trigger_count'][len(mids)] = 1
                    else:
                        freq_dict['trigger_count'][len(mids)] += 1

                    for mid in mids:
                        trigger_type = mentiontype[mid]     
                        if trigger_type not in freq_dict['trigger_type']:
                            freq_dict['trigger_type'][trigger_type] = 1
                        else:
                            freq_dict['trigger_type'][trigger_type] += 1
                    
                    if r.get('effect') is None:
                        effect_type = None
                        if effect_type not in freq_dict['effect_type']:
                            freq_dict['effect_type'][effect_type] = 1
                        else:
                            freq_dict['effect_type'][effect_type] += 1
                    
                        if 0 not in freq_dict['effect_count']:
                            freq_dict['effect_count'][0] = 1
                        else:
                            freq_dict['effect_count'][0] += 1

                    elif r.get('effect').startswith('C'):
                        if 1 not in freq_dict['effect_count']:
                            freq_dict['effect_count'][1] = 1
                        else:
                            freq_dict['effect_count'][1] += 1

                        effect_type = r.get('effect')
                        if effect_type not in freq_dict['effect_type']:
                            freq_dict['effect_type'][effect_type] = 1
                        else:
                            freq_dict['effect_type'][effect_type] += 1
                    else:
                        mids = r.get('effect').split(';')
                        
                        if len(mids) not in freq_dict['effect_count']:
                            freq_dict['effect_count'][len(mids)] = 1
                        else:
                            freq_dict['effect_count'][len(mids)] += 1

                        for mid in mids:
                            effect_type = mentiontype[mid]
                            if effect_type not in freq_dict['effect_type']:
                                freq_dict['effect_type'][effect_type] = 1
                            else:
                                freq_dict['effect_type'][effect_type] += 1

                    triple = (precip_type, trigger_type, effect_type)
                    if triple not in freq_dict['triple']:
                        freq_dict['triple'][triple] = 1
                    else:
                        freq_dict['triple'][triple] += 1

                        
                    int_type_effect = (int_type,effect_type)
                    if int_type_effect not in freq_dict['int_type_effect']:
                        freq_dict['int_type_effect'][int_type_effect] = 1
                    else:
                        freq_dict['int_type_effect'][int_type_effect] += 1
            
            sentcount = len(seen)
            if sentcount not in freq_dict['sentcount']:
                freq_dict['sentcount'][sentcount] = 1
            else:
                freq_dict['sentcount'][sentcount] += 1

    else:
        print('what')
    
    return freq_dict

# from stackexchange; user: aquavitae
def using_split2(line, _len=len):
    words = line.split()
    index = line.index
    offsets = []
    append = offsets.append
    running_offset = 0
    for word in words:
        word_offset = index(word, running_offset)
        word_len = _len(word)
        running_offset = word_offset + word_len
        append((word, word_offset, running_offset - 1))
    return offsets

if __name__ == '__main__':
    main()