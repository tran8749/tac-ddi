<?xml version="1.0" ?>
<Label drug="SAPHRIS" setid="5429f134-839f-4ffc-9944-55f51238def8">
  
  
  <Text>
    
    
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS Antihypertensive Drugs: SAPHRIS may cause hypotension. ( 5.7 , 7.1 , 12.3 ) Paroxetine (CYP2D6 substrate and inhibitor): Reduce paroxetine by half when used in combination with SAPHRIS. ( 7.1 , 12.3 ) 7.1 Drugs Having Clinically Important Drug Interactions with SAPHRIS Table 12: Clinically Important Drug Interactions with SAPHRIS Concomitant Drug Name or Drug Class  Clinical Rationale  Clinical Recommendation  Antihypertensive Drugs  Because of its α 1 -adrenergic antagonism with potential for inducing hypotension, SAPHRIS may enhance the effects of certain antihypertensive agents [see Warnings and Precautions ( 5.7 )] . Monitor blood pressure and adjust dosage of antihypertensive drug accordingly. Strong CYP1A2 Inhibitors (e.g., Fluvoxamine) SAPHRIS is metabolized by CYP1A2. Marginal increase of asenapine exposure was observed when SAPHRIS is used with fluvoxamine at 25 mg administered twice daily [see Clinical Pharmacology ( 12.3 )] . However, the tested fluvoxamine dose was suboptimal. Full therapeutic dose of fluvoxamine is expected to cause a greater increase in asenapine exposure. Dosage reduction for SAPHRIS based on clinical response may be necessary. CYP2D6 substrates and inhibitors (e.g., paroxetine) SAPHRIS may enhance the inhibitory effects of paroxetine on its own metabolism. Concomitant use of paroxetine with SAPHRIS increased the paroxetine exposure by 2-fold as compared to use paroxetine alone [see Clinical Pharmacology ( 12.3 )].  Reduce paroxetine dose by half when paroxetine is used in combination with SAPHRIS. 7.2 Drugs Having No Clinically Important Interactions with SAPHRIS No dosage adjustment of SAPHRIS is necessary when administered concomitantly with paroxetine (see Table 12 in Drug Interactions ( 7.1 ) for paroxetine dosage adjustment), imipramine, cimetidine, valporate, lithium, or a CYP3A4 inducer (e.g., carbamazepine, phenytoin, rifampin). In addition, valproic acid and lithium pre-dose serum concentrations collected from an adjunctive therapy study were comparable between asenapine-treated patients and placebo-treated patients indicating a lack of effect of asenapine on valproic and lithium plasma levels.</Section>
    
    

    
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY 12.1 Mechanism of Action The mechanism of action of asenapine, in schizophrenia and bipolar I disorder, is unknown. It has been suggested that the efficacy of asenapine in schizophrenia could be mediated through a combination of antagonist activity at D 2 and 5-HT 2A receptors. 12.2 Pharmacodynamics Asenapine exhibits high affinity for serotonin 5-HT 1A , 5-HT 1B , 5-HT 2A , 5-HT 2B , 5-HT 2C , 5-HT 5A , 5-HT 6, and 5-HT 7 receptors (Ki values of 2.5, 2.7, 0.07, 0.18, 0.03, 1.6, 0.25, and 0.11 nM, respectively), dopamine D 2A , D 2B , D 3 , D 4 , and D 1 receptors (Ki values of 1.3, 1.4, 0.42, 1.1, and 1.4 nM, respectively), α 1A , α 2A , α 2B , and α 2C -adrenergic receptors (Ki values of 1.2, 1.2, 0.33 and 1.2 nM, respectively), and histamine H 1 receptors (Ki value 1.0 nM), and moderate affinity for H 2 receptors (Ki value of 6.2 nM). In in vitro assays asenapine acts as an antagonist at these receptors. Asenapine has no appreciable affinity for muscarinic cholinergic receptors (e.g., Ki value of 8128 nM for M 1 ). 12.3 Pharmacokinetics Following a single 5 mg dose of SAPHRIS, the mean C max was approximately 4 ng/mL and was observed at a mean t max of 1 hour. Elimination of asenapine is primarily through direct glucuronidation by UGT1A4 and oxidative metabolism by cytochrome P450 isoenzymes (predominantly CYP1A2). Following an initial more rapid distribution phase, the mean terminal half-life is approximately 24 hrs. With multiple-dose twice-daily dosing, steady-state is attained within 3 days. Overall, steady-state asenapine pharmacokinetics are similar to single-dose pharmacokinetics. Absorption: Following sublingual administration, asenapine is rapidly absorbed with peak plasma concentrations occurring within 0.5 to 1.5 hours. The absolute bioavailability of sublingual asenapine at 5 mg is 35%. Increasing the dose from 5 mg to 10 mg twice daily (a two-fold increase) results in less than linear (1.7 times) increases in both the extent of exposure and maximum concentration. The absolute bioavailability of asenapine when swallowed is low (&lt;2% with an oral tablet formulation). The intake of water several (2 or 5) minutes after asenapine administration resulted in decreased asenapine exposure. Therefore, eating and drinking should be avoided for 10 minutes after administration [see Dosage and Administration ( 2.1 )] . Distribution: Asenapine is rapidly distributed and has a large volume of distribution (approximately 20 - 25 L/kg), indicating extensive extravascular distribution. Asenapine is highly bound (95%) to plasma proteins, including albumin and α 1 -acid glycoprotein. Metabolism and Elimination: Direct glucuronidation by UGT1A4 and oxidative metabolism by cytochrome P450 isoenzymes (predominantly CYP1A2) are the primary metabolic pathways for asenapine. Asenapine is a high clearance drug with a clearance after intravenous administration of 52 L/h. In this circumstance, hepatic clearance is influenced primarily by changes in liver blood flow rather than by changes in the intrinsic clearance, i.e., the metabolizing enzymatic activity. Following an initial more rapid distribution phase, the terminal half-life of asenapine is approximately 24 hours. Steady-state concentrations of asenapine are reached within 3 days of twice daily dosing. After administration of a single dose of [ 14 C]-labeled asenapine, about 90% of the dose was recovered; approximately 50% was recovered in urine, and 40% recovered in feces. About 50% of the circulating species in plasma have been identified. The predominant species was asenapine N + -glucuronide; others included N-desmethylasenapine, N-desmethylasenapine N-carbamoyl glucuronide, and unchanged asenapine in smaller amounts. SAPHRIS activity is primarily due to the parent drug. In vitro studies indicate that asenapine is a substrate for UGT1A4, CYP1A2 and to a lesser extent CYP3A4 and CYP2D6. Asenapine is a weak inhibitor of CYP2D6. Asenapine does not cause induction of CYP1A2 or CYP3A4 activities in cultured human hepatocytes. Coadministration of asenapine with known inhibitors, inducers or substrates of these metabolic pathways has been studied in a number of drug-drug interaction studies [see Drug Interactions ( 7.1 )] . Food: A crossover study in 26 healthy adult male subjects was performed to evaluate the effect of food on the pharmacokinetics of a single 5 mg dose of asenapine. Consumption of food immediately prior to sublingual administration decreased asenapine exposure by 20%; consumption of food 4 hours after sublingual administration decreased asenapine exposure by about 10%. These effects are probably due to increased hepatic blood flow. In clinical trials establishing the efficacy and safety of SAPHRIS, patients were instructed to avoid eating for 10 minutes following sublingual dosing. There were no other restrictions with regard to the timing of meals in these trials [see Dosage and Administration ( 2.1 )] . Water: In clinical trials establishing the efficacy and safety of SAPHRIS, patients were instructed to avoid drinking for 10 minutes following sublingual dosing. The effect of water administration following 10 mg sublingual SAPHRIS dosing was studied at different time points of 2, 5, 10, and 30 minutes in 15 healthy adult male subjects. The exposure of asenapine following administration of water 10 minutes after sublingual dosing was equivalent to that when water was administered 30 minutes after dosing. Reduced exposure to asenapine was observed following water administration at 2 minutes (19% decrease) and 5 minutes (10% decrease) [see Dosage and Administration ( 2.1 )] . Drug Interaction Studies:  Effects of other drugs on the exposure of asenapine are summarized in Figure 1 . In addition, a population pharmacokinetic analysis indicated that the concomitant administration of lithium had no effect on the pharmacokinetics of asenapine.  Figure 1: Effect of Other Drugs on Asenapine Pharmacokinetics  The effects of asenapine on the pharmacokinetics of other co-administered drugs are summarized in Figure 2 . Coadministration of paroxetine with SAPHRIS caused a two-fold increase in the maximum plasma concentrations and systemic exposure of paroxetine. Asenapine enhances the inhibitory effects of paroxetine on its own metabolism by CYP2D6.  Figure 2: Effect of Asenapine on Other Drug Pharmacokinetics  Figure 1 Figure 2 Studies in Special Populations:  Exposures of asenapine in special populations are summarized in Figure 3 . Additionally, based on population pharmacokinetic analysis, no effects of sex, race, BMI, and smoking status on asenapine exposure were observed. Exposure in elderly patients is 30-40% higher as compared to adults.  Figure 3: Effect of Intrinsic Factors on Asenapine Pharmacokinetics  Figure 3</Section>
    
    

  
  </Text>
  
  
  <Sentences>
    
    
    <Sentence LabelDrug="SAPHRIS" id="5933" section="34073-7">
      
      

      
      <SentenceText>Antihypertensive Drugs: SAPHRIS may cause hypotension.</SentenceText>
      
      

    
      <Mention code="NO MAP" id="M1" span="42 11" str="hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="0 22" str="Antihypertensive Drugs" type="Precipitant"/>
      <Interaction effect="M1" id="I1" precipitant="M2" trigger="M2" type="Pharmacodynamic interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5934" section="34073-7">
      
      

      
      <SentenceText>Paroxetine (CYP2D6 substrate and inhibitor): Reduce paroxetine by half when used in combination with SAPHRIS.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5935" section="34073-7">
      
      

      
      <SentenceText>Table 12: Clinically Important Drug Interactions with SAPHRIS Concomitant Drug Name or Drug Class Clinical Rationale Clinical Recommendation Antihypertensive Drugs Because of its α1-adrenergic antagonism with potential for inducing hypotension, SAPHRIS may enhance the effects of certain antihypertensive agents.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M3" span="288 23" str="antihypertensive agents" type="Precipitant"/>
      <Mention code="NO MAP" id="M4" span="232 11" str="hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M5" span="257 19" str="enhance the effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M6" span="141 22" str="Antihypertensive Drugs" type="Precipitant"/>
      <Interaction effect="M4;M5" id="I2" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
      <Interaction effect="M4" id="I4" precipitant="M6" trigger="M6" type="Pharmacodynamic interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5936" section="34073-7">
      
      

      
      <SentenceText>Monitor blood pressure and adjust dosage of antihypertensive drug accordingly.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M7" span="0 7" str="Monitor" type="Trigger"/>
      <Mention code="NO MAP" id="M8" span="44 21" str="antihypertensive drug" type="Precipitant"/>
      <Mention code="NO MAP" id="M9" span="27 6" str="adjust" type="Trigger"/>
      <Interaction id="I5" precipitant="M8" trigger="M7" type="Unspecified interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5937" section="34073-7">
      
      

      
      <SentenceText>Strong CYP1A2 Inhibitors (e.g., Fluvoxamine) SAPHRIS is metabolized by CYP1A2.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5938" section="34073-7">
      
      

      
      <SentenceText>Marginal increase of asenapine exposure was observed when SAPHRIS is used with fluvoxamine at 25 mg administered twice daily.</SentenceText>
      
      

    
      <Mention code="NO MAP" id="M10" span="79 11" str="fluvoxamine" type="Precipitant"/>
      <Interaction effect="C54355" id="I6" precipitant="M10" trigger="M10" type="Pharmacokinetic interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5939" section="34073-7">
      
      

      
      <SentenceText>However, the tested fluvoxamine dose was suboptimal.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5940" section="34073-7">
      
      

      
      <SentenceText>Full therapeutic dose of fluvoxamine is expected to cause a greater increase in asenapine exposure.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M11" span="25 11" str="fluvoxamine" type="Precipitant"/>
      <Mention code="NO MAP" id="M12" span="90 8" str="exposure" type="Trigger"/>
      <Interaction effect="C54355" id="I7" precipitant="M11" trigger="M12" type="Pharmacokinetic interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5941" section="34073-7">
      
      

      
      <SentenceText>Dosage reduction for SAPHRIS based on clinical response may be necessary.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M13" span="0 16" str="Dosage reduction" type="Trigger"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5942" section="34073-7">
      
      

      
      <SentenceText>CYP2D6 substrates and inhibitors (e.g., paroxetine) SAPHRIS may enhance the inhibitory effects of paroxetine on its own metabolism.</SentenceText>
      
      

    
      <Mention code="NO MAP" id="M14" span="0 17" str="CYP2D6 substrates" type="Precipitant"/>
      <Mention code="NO MAP" id="M15" span="64 30" str="enhance the inhibitory effects" type="SpecificInteraction"/>
      <Interaction effect="M15" id="I8" precipitant="M14" trigger="M14" type="Pharmacodynamic interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5943" section="34073-7">
      
      

      
      <SentenceText>Concomitant use of paroxetine with SAPHRIS increased the paroxetine exposure by 2-fold as compared to use paroxetine alone.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5944" section="34073-7">
      
      

      
      <SentenceText>Reduce paroxetine dose by half when paroxetine is used in combination with SAPHRIS.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5945" section="34073-7">
      
      

      
      <SentenceText>No dosage adjustment of SAPHRIS is necessary when administered concomitantly with paroxetine.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5946" section="34073-7">
      
      

      
      <SentenceText>In addition, valproic acid and lithium pre-dose serum concentrations collected from an adjunctive therapy study were comparable between asenapine-treated patients and placebo-treated patients indicating a lack of effect of asenapine on valproic and lithium plasma levels.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M16" span="249 7" str="lithium" type="Precipitant"/>
      <Mention code="NO MAP" id="M17" span="236 8" str="valproic" type="Precipitant"/>
      <Mention code="NO MAP" id="M18" span="31 7" str="lithium" type="Precipitant"/>
      <Mention code="NO MAP" id="M19" span="43 25" str="dose serum concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="13 8" str="valproic" type="Precipitant"/>
      <Interaction effect="C54357" id="I9" precipitant="M17" trigger="M19" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54357" id="I10" precipitant="M16" trigger="M19" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54357" id="I11" precipitant="M18" trigger="M19" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54357" id="I12" precipitant="M20" trigger="M19" type="Pharmacokinetic interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5947" section="34090-1">
      
      

      
      <SentenceText>The mechanism of action of asenapine, in schizophrenia and bipolar I disorder, is unknown.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5948" section="34090-1">
      
      

      
      <SentenceText>It has been suggested that the efficacy of asenapine in schizophrenia could be mediated through a combination of antagonist activity at D2 and 5-HT2A receptors.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5949" section="34090-1">
      
      

      
      <SentenceText>Asenapine exhibits high affinity for serotonin 5-HT1A, 5-HT1B, 5-HT2A, 5-HT2B, 5-HT2C, 5-HT5A, 5-HT6, and 5-HT7 receptors (Ki values of 2.5, 2.7, 0.07, 0.18, 0.03, 1.6, 0.25, and 0.11nM, respectively), dopamine D2A, D2B, D3, D4, and D1 receptors (Ki values of 1.3, 1.4, 0.42, 1.1, and 1.4 nM, respectively), α1A, α2A, α2B, and α2C -adrenergic receptors (Ki values of 1.2, 1.2, 0.33 and 1.2 nM, respectively), and histamine H1 receptors (Ki value 1.0 nM), and moderate affinity for H2 receptors (Ki value of 6.2 nM).</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M21" span="202 8" str="dopamine" type="Precipitant"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5950" section="34090-1">
      
      

      
      <SentenceText>In in vitro assays asenapine acts as an antagonist at these receptors.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5951" section="34090-1">
      
      

      
      <SentenceText>Asenapine has no appreciable affinity for muscarinic cholinergic receptors (e.g., Ki value of 8128 nM for M1).</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5952" section="34090-1">
      
      

      
      <SentenceText>Following a single 5 mg dose of SAPHRIS, the mean Cmax was approximately 4 ng/mL and was observed at a mean tmax of 1 hour.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5953" section="34090-1">
      
      

      
      <SentenceText>Elimination of asenapine is primarily through direct glucuronidation by UGT1A4 and oxidative metabolism by cytochrome P450 isoenzymes (predominantly CYP1A2).</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5954" section="34090-1">
      
      

      
      <SentenceText>Following an initial more rapid distribution phase, the mean terminal half-life is approximately 24 hrs.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5955" section="34090-1">
      
      

      
      <SentenceText>With multiple-dose twice-daily dosing, steady-state is attained within 3 days.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5956" section="34090-1">
      
      

      
      <SentenceText>Overall, steady-state asenapine pharmacokinetics are similar to single-dose pharmacokinetics.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5957" section="34090-1">
      
      

      
      <SentenceText>Absorption: Following sublingual administration, asenapine is rapidly absorbed with peak plasma concentrations occurring within 0.5 to 1.5 hours.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5958" section="34090-1">
      
      

      
      <SentenceText>The absolute bioavailability of sublingual asenapine at 5 mg is 35%.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5959" section="34090-1">
      
      

      
      <SentenceText>Increasing the dose from 5 mg to 10 mg twice daily (a two-fold increase) results in less than linear (1.7 times) increases in both the extent of exposure and maximum concentration.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5960" section="34090-1">
      
      

      
      <SentenceText>The absolute bioavailability of asenapine when swallowed is low (&lt;2% with an oral tablet formulation).</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5961" section="34090-1">
      
      

      
      <SentenceText>The intake of water several (2 or 5) minutes after asenapine administration resulted in decreased asenapine exposure.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5962" section="34090-1">
      
      

      
      <SentenceText>Therefore, eating and drinking should be avoided for 10 minutes after administration.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M22" span="41 7" str="avoided" type="Trigger"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5963" section="34090-1">
      
      

      
      <SentenceText>Distribution: Asenapine is rapidly distributed and has a large volume of distribution (approximately 20 - 25 L/kg), indicating extensive extravascular distribution.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5964" section="34090-1">
      
      

      
      <SentenceText>Asenapine is highly bound (95%) to plasma proteins, including albumin and α1-acid glycoprotein.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5965" section="34090-1">
      
      

      
      <SentenceText>Metabolism and Elimination: Direct glucuronidation by UGT1A4 and oxidative metabolism by cytochrome P450 isoenzymes (predominantly CYP1A2) are the primary metabolic pathways for asenapine.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5966" section="34090-1">
      
      

      
      <SentenceText>Asenapine is a high clearance drug with a clearance after intravenous administration of 52 L/h.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5967" section="34090-1">
      
      

      
      <SentenceText>In this circumstance, hepatic clearance is influenced primarily by changes in liver blood flow rather than by changes in the intrinsic clearance, i.e., the metabolizing enzymatic activity.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5968" section="34090-1">
      
      

      
      <SentenceText>Following an initial more rapid distribution phase, the terminal half-life of asenapine is approximately 24 hours.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5969" section="34090-1">
      
      

      
      <SentenceText>Steady-state concentrations of asenapine are reached within 3 days of twice daily dosing.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M23" span="7 20" str="state concentrations" type="Trigger"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5970" section="34090-1">
      
      

      
      <SentenceText>After administration of a single dose of [14C]-labeled asenapine, about 90% of the dose was recovered; approximately 50% was recovered in urine, and 40% recovered in feces.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5971" section="34090-1">
      
      

      
      <SentenceText>About 50% of the circulating species in plasma have been identified.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5972" section="34090-1">
      
      

      
      <SentenceText>The predominant species was asenapine N+-glucuronide; others included N-desmethylasenapine, N-desmethylasenapine N-carbamoyl glucuronide, and unchanged asenapine in smaller amounts.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5973" section="34090-1">
      
      

      
      <SentenceText>SAPHRIS activity is primarily due to the parent drug.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5974" section="34090-1">
      
      

      
      <SentenceText>In vitro studies indicate that asenapine is a substrate for UGT1A4, CYP1A2 and to a lesser extent CYP3A4 and CYP2D6.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5975" section="34090-1">
      
      

      
      <SentenceText>Asenapine does not cause induction of CYP1A2 or CYP3A4 activities in cultured human hepatocytes.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5976" section="34090-1">
      
      

      
      <SentenceText>Coadministration of asenapine with known inhibitors, inducers or substrates of these metabolic pathways has been studied in a number of drug-drug interaction studies.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5977" section="34090-1">
      
      

      
      <SentenceText>Food: A crossover study in 26 healthy adult male subjects was performed to evaluate the effect of food on the pharmacokinetics of a single 5 mg dose of asenapine.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5978" section="34090-1">
      
      

      
      <SentenceText>Consumption of food immediately prior to sublingual administration decreased asenapine exposure by 20%; consumption of food 4 hours after sublingual administration decreased asenapine exposure by about 10%.</SentenceText>
      
      

    
      <Mention code="NO MAP" id="M24" span="164 9;184 8" str="decreased | exposure" type="Trigger"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5979" section="34090-1">
      
      

      
      <SentenceText>These effects are probably due to increased hepatic blood flow.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M25" span="34 28" str="increased hepatic blood flow" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="0 13" str="These effects" type="SpecificInteraction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5980" section="34090-1">
      
      

      
      <SentenceText>In clinical trials establishing the efficacy and safety of SAPHRIS, patients were instructed to avoid eating for 10 minutes following sublingual dosing.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5981" section="34090-1">
      
      

      
      <SentenceText>There were no other restrictions with regard to the timing of meals in these trials.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5982" section="34090-1">
      
      

      
      <SentenceText>Water: In clinical trials establishing the efficacy and safety of SAPHRIS, patients were instructed to avoid drinking for 10 minutes following sublingual dosing.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5983" section="34090-1">
      
      

      
      <SentenceText>The effect of water administration following 10 mg sublingual SAPHRIS dosing was studied at different time points of 2, 5, 10, and 30 minutes in 15 healthy adult male subjects.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5984" section="34090-1">
      
      

      
      <SentenceText>The exposure of asenapine following administration of water 10 minutes after sublingual dosing was equivalent to that when water was administered 30 minutes after dosing.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5985" section="34090-1">
      
      

      
      <SentenceText>Reduced exposure to asenapine was observed following water administration at 2 minutes (19% decrease) and 5 minutes (10% decrease).</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M27" span="0 16" str="Reduced exposure" type="Trigger"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5986" section="34090-1">
      
      

      
      <SentenceText>Drug Interaction Studies: Effects of other drugs on the exposure of asenapine are summarized in Figure 1.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5987" section="34090-1">
      
      

      
      <SentenceText>In addition, a population pharmacokinetic analysis indicated that the concomitant administration of lithium had no effect on the pharmacokinetics of asenapine.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5988" section="34090-1">
      
      

      
      <SentenceText>Figure 1: Effect of Other Drugs on Asenapine Pharmacokinetics The effects of asenapine on the pharmacokinetics of other co-administered drugs are summarized in Figure 2.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5989" section="34090-1">
      
      

      
      <SentenceText>Coadministration of paroxetine with SAPHRIS caused a two-fold increase in the maximum plasma concentrations and systemic exposure of paroxetine.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5990" section="34090-1">
      
      

      
      <SentenceText>Asenapine enhances the inhibitory effects of paroxetine on its own metabolism by CYP2D6.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5991" section="34090-1">
      
      

      
      <SentenceText>Figure 2: Effect of Asenapine on Other Drug Pharmacokinetics Figure 1 Figure 2 Studies in Special Populations: Exposures of asenapine in special populations are summarized in Figure 3.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5992" section="34090-1">
      
      

      
      <SentenceText>Additionally, based on population pharmacokinetic analysis, no effects of sex, race, BMI, and smoking status on asenapine exposure were observed.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5993" section="34090-1">
      
      

      
      <SentenceText>Exposure in elderly patients is 30-40% higher as compared to adults.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="SAPHRIS" id="5994" section="34090-1">
      
      

      
      <SentenceText>Figure 3: Effect of Intrinsic Factors on Asenapine Pharmacokinetics Figure 3</SentenceText>
      
      

    
    </Sentence>
    
    

  
  </Sentences>
  
  
  <LabelInteractions/>
  

</Label>

