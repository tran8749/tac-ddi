<?xml version="1.0" ?>
<Label drug="Rifampin" setid="55816042-946d-4bec-9461-bd998628ff45">
  
  
  <Text>
    
    
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Oral Administration  Rifampin is readily absorbed from the gastrointestinal tract. Peak serum concentrations in healthy adults and pediatric populations vary widely from individual to individual. Following a single 600 mg oral dose of rifampin in healthy adults, the peak serum concentration averages 7 mcg/mL but may vary from 4 to 32 mcg/mL. Absorption of rifampin is reduced by about 30% when the drug is ingested with food.  Rifampin is widely distributed throughout the body. It is present in effective concentrations in many organs and body fluids, including cerebrospinal fluid. Rifampin is about 80% protein bound. Most of the unbound fraction is not ionized and, therefore, diffuses freely into tissues.  In healthy adults, the mean biological half-life of rifampin in serum averages 3.35 ± 0.66 hours after a 600 mg oral dose, with increases up to 5.08 ± 2.45 hours reported after a 900 mg dose. With repeated administration, the half-life decreases and reaches average values of approximately 2 to 3 hours. The half-life does not differ in patients with renal failure at doses not exceeding 600 mg daily, and consequently, no dosage adjustment is required. The half-life of rifampin at a dose of 720 mg daily has not been established in patients with renal failure. Following a single 900 mg oral dose of rifampin in patients with varying degrees of renal insufficiency, the mean half-life increased from 3.6 hours in healthy adults to 5.0, 7.3, and 11.0 hours in patients with glomerular filtration rates of 30 to 50 mL/min, less than 30 mL/min, and in anuric patients, respectively. Refer to the  WARNINGS  section for information regarding patients with hepatic insufficiency.  After absorption, rifampin is rapidly eliminated in the bile, and an enterohepatic circulation ensues. During this process, rifampin undergoes progressive deacetylation so that nearly all the drug in the bile is in this form in about 6 hours. This metabolite has antibacterial activity. Intestinal reabsorption is reduced by deacetylation, and elimination is facilitated. Up to 30% of a dose is excreted in the urine, with about half of this being unchanged drug.  Pediatrics  Oral Administration  In one study, pediatric patients 6 to 58 months old were given rifampin suspended in simple syrup or as dry powder mixed with applesauce at a dose of 10 mg/kg body weight. Peak serum concentrations of 10.7 ± 3.7 and 11.5 ± 5.1 mcg/mL were obtained 1 hour after preprandial ingestion of the drug suspension and the applesauce mixture, respectively. After the administration of either preparation, the t 1/2 of rifampin averaged 2.9 hours. It should be noted that in other studies in pediatric populations, at doses of 10 mg/kg body weight, mean peak serum concentrations of 3.5 mcg/mL to 15 mcg/mL have been reported.  Microbiology  Mechanism of Action  Rifampin inhibits DNA-dependent RNA polymerase activity in susceptible Mycobacterium tuberculosis organisms. Specifically, it interacts with bacterial RNA polymerase but does not inhibit the mammalian enzyme.  Drug Resistance  Organisms resistant to rifampin are likely to be resistant to other rifamycins.  In the treatment of both tuberculosis and the meningococcal carrier state (see INDICATIONS AND USAGE), the small number of resistant cells present within large populations of susceptible cells can rapidly become predominant. In addition, resistance to rifampin has been determined to occur as single-step mutations of the DNA-dependent RNA polymerase. Since resistance can emerge rapidly, appropriate susceptibility tests should be performed in the event of persistent positive cultures.  Activity in vitro and in vivo  Rifampin has bactericidal activity in vitro against slow and intermittently growing M tuberculosis organisms.  Rifampin has been shown to be active against most strains of the following microorganisms, both in vitro and in clinical infections as described in the  INDICATIONS AND USAGE  section.    Aerobic Gram-Negative Microorganisms:  Neisseria meningitidis    “Other” Microorganisms:  Mycobacterium tuberculosis  The following in vitro data are available, but their clinical significance is unknown .  Rifampin exhibits in vitro activity against most strains of the following microorganisms; however, the safety and effectiveness of rifampin in treating clinical infections due to these microorganisms have not been established in adequate and well controlled trials.    Aerobic Gram-Positive Microorganisms:  Staphylococcus aureus (including Methicillin-Resistant S. aureus /MRSA)  Staphylococcus epidermidis    Aerobic Gram-Negative Microorganisms:  Haemophilus influenzae    “Other” Microorganisms:  Mycobacterium leprae  ß-lactamase production should have no effect on rifampin activity.  Susceptibility Testing  Prior to initiation of therapy, appropriate specimens should be collected for identification of the infecting organism and in vitro susceptibility tests.  In vitro testing for Mycobacterium tuberculosis isolates:  Two standardized in vitro susceptibility methods are available for testing rifampin against M tuberculosis organisms. The agar proportion method (CDC or CLSI (1) M24-A) utilizes Middlebrook 7H10 medium impregnated with rifampin at a final concentration of 1.0 mcg/mL to determine drug resistance. After three weeks of incubation MIC 99 values are calculated by comparing the quantity of organisms growing in the medium containing drug to the control cultures. Mycobacterial growth in the presence of drug, of at least 1% of the growth in the control culture, indicates resistance.  The radiometric broth method employs the BACTEC 460 machine to compare the growth index from untreated control cultures to cultures grown in the presence of 2.0 mcg/mL of rifampin. Strict adherence to the manufacturer’s instructions for sample processing and data interpretation is required for this assay.  Susceptibility test results obtained by the two different methods can only be compared if the appropriate rifampin concentration is used for each test method as indicated above. Both procedures require the use of M tuberculosis H37Rv ATCC 27294 as a control organism.  The clinical relevance of in vitro susceptibility test results for mycobacterial species other than M tuberculosis using either the radiometric or the proportion method has not been determined.  In vitro testing for Neisseria meningitidis isolates:  Dilution Techniques: Quantitative methods that are used to determine minimum inhibitory concentrations provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds. One such standardized procedure uses a standardized dilution method 2,4 (broth, agar, or microdilution) or equivalent with rifampin powder. The MIC values obtained should be interpreted according to the following criteria for Neisseria meningitidis:  MIC (mcg/mL)  Interpretation  ≤1  (S) Susceptible  2  (I) Intermediate  ≥4  (R) Resistant  A report of “susceptible” indicates that the pathogen is likely to be inhibited by usually achievable concentrations of the antimicrobial compound in the blood. A report of “intermediate” indicates that the result should be considered equivocal, and if the microorganism is not fully susceptible to alternative, clinically feasible drugs, the test should be repeated. This category implies possible clinical applicability in body sites where the drug is physiologically concentrated or in situations where the maximum acceptable dose of drug can be used. This category also provides a buffer zone that prevents small-uncontrolled technical factors from causing major discrepancies in interpretation. A report of “resistant” indicates that usually achievable concentrations of the antimicrobial compound in the blood are unlikely to be inhibitory and that other therapy should be selected.  Measurement of MIC or minimum bactericidal concentrations (MBC) and achieved antimicrobial compound concentrations may be appropriate to guide therapy in some infections. (See  CLINICAL PHARMACOLOGY  section for further information on drug concentrations achieved in infected body sites and other pharmacokinetic properties of this antimicrobial drug product.)  Standardized susceptibility test procedures require the use of laboratory control microorganisms. The use of these microorganisms does not imply clinical efficacy (see  INDICATIONS AND USAGE  ); they are used to control the technical aspects of the laboratory procedures. Standard rifampin powder should give the following MIC values:  Microorganism  MIC (mcg/mL)  Staphylococcus aureus  ATCC 29213  0.008 - 0.06  Enterococcus faecalis  ATCC 29212    1 - 4  Escherichia coli  ATCC 25922  8 - 32  Pseudomonas aeruginosa  ATCC 27853  32 - 64  Haemophilus influenzae  ATCC 49247  0.25 - 1  Diffusion Techniques: Quantitative methods that require measurement of zone diameters provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds. One such standardized procedure 3,4 that has been recommended for use with disks to test the susceptibility of microorganisms to rifampin uses the 5 mcg rifampin disk. Interpretation involves correlation of the diameter obtained in the disk test with the MIC for rifampin.  Reports from the laboratory providing results of the standard single-disk susceptibility test with a 5 mcg rifampin disk should be interpreted according to the following criteria for Neisseria meningitidis :  Zone Diameter (mm)  Interpretation  ≥ 20  (S) Susceptible  17 - 19  (I) Intermediate  ≤ 16  (R) Resistant  Interpretation should be as stated above for results using dilution techniques.  As with standard dilution techniques, diffusion methods require the use of laboratory control microorganisms. The use of these microorganisms does not imply clinical efficacy (see  INDICATIONS AND USAGE  ); they are used to control the technical aspects of the laboratory procedures. The 5 mcg rifampin disk should provide the following zone diameters in these quality control strains:  Microorganism  Zone Diameter (mm)  S. aureus  ATCC 25923  26 - 34  E. coli  ATCC 25922  8 - 10  H. influenzae  ATCC 49247  22 - 30</Section>
    
    

  
  </Text>
  
  
  <Sentences>
    
    
    <Sentence LabelDrug="Rifampin" id="1000" section="34090-1">
      
      

      
      <SentenceText>Absorption of rifampin is reduced by about 30% when the drug is ingested with food.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1001" section="34090-1">
      
      

      
      <SentenceText>Rifampin is widely distributed throughout the body.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1002" section="34090-1">
      
      

      
      <SentenceText>It is present in effective concentrations in many organs and body fluids, including cerebrospinal fluid.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1003" section="34090-1">
      
      

      
      <SentenceText>Most of the unbound fraction is not ionized and, therefore, diffuses freely into tissues.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1004" section="34090-1">
      
      

      
      <SentenceText>In healthy adults, the mean biological half-life of rifampin in serum averages 3.35 ± 0.66 hours after a 600 mg oral dose, with increases up to 5.08 ± 2.45 hours reported after a 900 mg dose.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M1" span="28 20" str="biological half-life" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="128 12" str="increases up" type="Trigger"/>
      <Mention code="NO MAP" id="M3" span="112 4" str="oral" type="Precipitant"/>
      <Interaction effect="C54611" id="I1" precipitant="M3" trigger="M1" type="Pharmacokinetic interaction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1005" section="34090-1">
      
      

      
      <SentenceText>With repeated administration, the half-life decreases and reaches average values of approximately 2 to 3 hours.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1006" section="34090-1">
      
      

      
      <SentenceText>The half-life does not differ in patients with renal failure at doses not exceeding 600 mg daily, and consequently, no dosage adjustment is required.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1007" section="34090-1">
      
      

      
      <SentenceText>The half-life of rifampin at a dose of 720 mg daily has not been established in patients with renal failure.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1008" section="34090-1">
      
      

      
      <SentenceText>Following a single 900 mg oral dose of rifampin in patients with varying degrees of renal insufficiency, the mean half-life increased from 3.6 hours in healthy adults to 5, 7.3, and 11 hours in patients with glomerular filtration rates of 30 to 50 mL/min, less than 30 mL/min, and in anuric patients, respectively.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M4" span="114 19" str="half-life increased" type="Trigger"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1009" section="34090-1">
      
      

      
      <SentenceText>Refer to the WARNINGS section for information regarding patients with hepatic insufficiency.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1010" section="34090-1">
      
      

      
      <SentenceText>After absorption, rifampin is rapidly eliminated in the bile, and an enterohepatic circulation ensues.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1011" section="34090-1">
      
      

      
      <SentenceText>During this process, rifampin undergoes progressive deacetylation so that nearly all the drug in the bile is in this form in about 6 hours.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1012" section="34090-1">
      
      

      
      <SentenceText>This metabolite has antibacterial activity.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M5" span="20 22" str="antibacterial activity" type="SpecificInteraction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1013" section="34090-1">
      
      

      
      <SentenceText>Intestinal reabsorption is reduced by deacetylation, and elimination is facilitated.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1014" section="34090-1">
      
      

      
      <SentenceText>Up to 30% of a dose is excreted in the urine, with about half of this being unchanged drug.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1015" section="34090-1">
      
      

      
      <SentenceText>In one study, pediatric patients 6 to 58 months old were given rifampin suspended in simple syrup or as dry powder mixed with applesauce at a dose of 10 mg/kg body weight.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1016" section="34090-1">
      
      

      
      <SentenceText>Peak serum concentrations of 10.7 ± 3.7 and 11.5 ± 5.1 mcg/mL were obtained 1 hour after preprandial ingestion of the drug suspension and the applesauce mixture, respectively.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1017" section="34090-1">
      
      

      
      <SentenceText>After the administration of either preparation, the t1/2 of rifampin averaged 2.9 hours.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1018" section="34090-1">
      
      

      
      <SentenceText>It should be noted that in other studies in pediatric populations, at doses of 10 mg/kg body weight, mean peak serum concentrations of 3.5 mcg/mL to 15 mcg/mL have been reported.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1019" section="34090-1">
      
      

      
      <SentenceText>Mechanism of Action Rifampin inhibits DNA-dependent RNA polymerase activity in susceptible Mycobacterium tuberculosis organisms.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1020" section="34090-1">
      
      

      
      <SentenceText>Specifically, it interacts with bacterial RNA polymerase but does not inhibit the mammalian enzyme.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1021" section="34090-1">
      
      

      
      <SentenceText>Drug Resistance Organisms resistant to rifampin are likely to be resistant to other rifamycins.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1022" section="34090-1">
      
      

      
      <SentenceText>In the treatment of both tuberculosis and the meningococcal carrier state , the small number of resistant cells present within large populations of susceptible cells can rapidly become predominant.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1023" section="34090-1">
      
      

      
      <SentenceText>In addition, resistance to rifampin has been determined to occur as single-step mutations of the DNA-dependent RNA polymerase.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1024" section="34090-1">
      
      

      
      <SentenceText>Since resistance can emerge rapidly, appropriate susceptibility tests should be performed in the event of persistent positive cultures.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1025" section="34090-1">
      
      

      
      <SentenceText>Activity in vitro and in vivo Rifampin has bactericidal activity in vitro against slow and intermittently growing M tuberculosis organisms.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1026" section="34090-1">
      
      

      
      <SentenceText>Rifampin has been shown to be active against most strains of the following microorganisms, both in vitro and in clinical infections as described in the INDICATIONS AND USAGE section.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1027" section="34090-1">
      
      

      
      <SentenceText>Aerobic Gram-Negative Microorganisms: Neisseria meningitidis “Other” Microorganisms: Mycobacterium tuberculosis The following in vitro data are available, but their clinical significance is unknown.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1028" section="34090-1">
      
      

      
      <SentenceText>Rifampin exhibits in vitro activity against most strains of the following microorganisms; however, the safety and effectiveness of rifampin in treating clinical infections due to these microorganisms have not been established in adequate and well controlled trials.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1029" section="34090-1">
      
      

      
      <SentenceText>Aerobic Gram-Positive Microorganisms: Staphylococcus aureus (including Methicillin-Resistant S. aureus/MRSA) Staphylococcus epidermis Aerobic Gram-Negative Microorganisms: Haemophilus influenzae “Other” Microorganisms: Mycobacterium leprae ß-lactamase production should have no effect on rifampin activity.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1030" section="34090-1">
      
      

      
      <SentenceText>Prior to initiation of therapy, appropriate specimens should be collected for identification of the infecting organism and in vitro susceptibility tests.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1031" section="34090-1">
      
      

      
      <SentenceText>In vitro testing for Mycobacterium tuberculosis isolates: Two standardized in vitro susceptibility methods are available for testing rifampin against M tuberculosis organisms.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1032" section="34090-1">
      
      

      
      <SentenceText>The agar proportion method (CDC or CLSI(1) M24-A) utilizes Middlebrook 7H10 medium impregnated with rifampin at a final concentration of 1.0 mcg/mL to determine drug resistance.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M6" span="4 4" str="agar" type="Precipitant"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1033" section="34090-1">
      
      

      
      <SentenceText>After three weeks of incubation MIC99 values are calculated by comparing the quantity of organisms growing in the medium containing drug to the control cultures.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1034" section="34090-1">
      
      

      
      <SentenceText>Mycobacterial growth in the presence of drug, of at least 1% of the growth in the control culture, indicates resistance.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1035" section="34090-1">
      
      

      
      <SentenceText>The radiometric broth method employs the BACTEC 460 machine to compare the growth index from untreated control cultures to cultures grown in the presence of 2.0 mcg/mL of rifampin.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1036" section="34090-1">
      
      

      
      <SentenceText>Strict adherence to the manufacturer’s instructions for sample processing and data interpretation is required for this assay.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1037" section="34090-1">
      
      

      
      <SentenceText>Susceptibility test results obtained by the two different methods can only be compared if the appropriate rifampin concentration is used for each test method as indicated above.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1038" section="34090-1">
      
      

      
      <SentenceText>Both procedures require the use of M tuberculosis H37Rv ATCC 27294 as a control organism.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1039" section="34090-1">
      
      

      
      <SentenceText>The clinical relevance of in vitro susceptibility test results for mycobacterial species other than M tuberculosis using either the radiometric or the proportion method has not been determined.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1040" section="34090-1">
      
      

      
      <SentenceText>In vitro testing for Neisseria meningitidis isolates: Dilution Techniques: Quantitative methods that are used to determine minimum inhibitory concentrations provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1041" section="34090-1">
      
      

      
      <SentenceText>One such standardized procedure uses a standardized dilution method2,4 (broth, agar, or microdilution) or equivalent with rifampin powder.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1042" section="34090-1">
      
      

      
      <SentenceText>The MIC values obtained should be interpreted according to the following criteria for Neisseria meningitidis</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1043" section="34090-1">
      
      

      
      <SentenceText>A report of “intermediate” indicates that the result should be considered equivocal, and if the microorganism is not fully susceptible to alternative, clinically feasible drugs, the test should be repeated.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1044" section="34090-1">
      
      

      
      <SentenceText>This category implies possible clinical applicability in body sites where the drug is physiologically concentrated or in situations where the maximum acceptable dose of drug can be used.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1045" section="34090-1">
      
      

      
      <SentenceText>This category also provides a buffer zone that prevents small-uncontrolled technical factors from causing major discrepancies in interpretation.</SentenceText>
      
      

      
      <Mention code="NO MAP" id="M7" span="62 12" str="uncontrolled" type="SpecificInteraction"/>
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1046" section="34090-1">
      
      

      
      <SentenceText>A report of “resistant” indicates that usually achievable concentrations of the antimicrobial compound in the blood are unlikely to be inhibitory and that other therapy should be selected.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1047" section="34090-1">
      
      

      
      <SentenceText>Measurement of MIC or minimum bactericidal concentrations (MBC) and achieved antimicrobial compound concentrations may be appropriate to guide therapy in some infections.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1049" section="34090-1">
      
      

      
      <SentenceText>Standardized susceptibility test procedures require the use of laboratory control microorganisms.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1050" section="34090-1">
      
      

      
      <SentenceText>The use of these microorganisms does not imply clinical efficacy ; they are used to control the technical aspects of the laboratory procedures.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1051" section="34090-1">
      
      

      
      <SentenceText>Standard rifampin powder should give the following MIC values: Microorganism MIC (mcg/mL) Staphylococcus aureus ATCC 29213 0.008 - 0.06 Enterococcus faecalis ATCC 29212 1 - 4 Escherichia coli ATCC 25922 8 - 32 Pseudomonas aeruginosa ATCC 27853 32 - 64 Haemophilus influenzae ATCC 49247 0.25 - 1 Diffusion Techniques: Quantitative methods that require measurement of zone diameters provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1052" section="34090-1">
      
      

      
      <SentenceText>One such standardized procedure3,4 that has been recommended for use with disks to test the susceptibility of microorganisms to rifampin uses the 5 mcg rifampin disk.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1053" section="34090-1">
      
      

      
      <SentenceText>Interpretation involves correlation of the diameter obtained in the disk test with the MIC for rifampin.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1054" section="34090-1">
      
      

      
      <SentenceText>Reports from the laboratory providing results of the standard single-disk susceptibility test with a 5 mcg rifampin disk should be interpreted according to the following criteria for Neisseria meningitides: Zone Diameter (mm) Interpretation ≥ 20 (S) Susceptible 17 - 19 (I) Intermediate ≤ 16 (R) Resistant Interpretation should be as stated above for results using dilution techniques.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1055" section="34090-1">
      
      

      
      <SentenceText>As with standard dilution techniques, diffusion methods require the use of laboratory control microorganisms.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="1057" section="34090-1">
      
      

      
      <SentenceText>The 5 mcg rifampin disk should provide the following zone diameters in these quality control strains: Microorganism Zone Diameter (mm) S. aureus ATCC 25923 26 - 34 E. coli ATCC 25922 8 - 10 H. influenzae ATCC 49247 22 - 30</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="997" section="34090-1">
      
      

      
      <SentenceText>Rifampin is readily absorbed from the gastrointestinal tract.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="998" section="34090-1">
      
      

      
      <SentenceText>Peak serum concentrations in healthy adults and pediatric populations vary widely from individual to individual.</SentenceText>
      
      

    
    </Sentence>
    
    

    
    <Sentence LabelDrug="Rifampin" id="999" section="34090-1">
      
      

      
      <SentenceText>Following a single 600 mg oral dose of rifampin in healthy adults, the peak serum concentration averages 7 mcg/mL but may vary from 4 to 32 mcg/mL.</SentenceText>
      
      

    
    </Sentence>
    
    

  
  </Sentences>
  
  
  <LabelInteractions/>
  

</Label>

