Thiothixene	3064	34073-7	827cf470-485d-4925-85f2-8933a6dea830	19
Hepatic microsomal enzyme inducing agents, such as carbamazepine, were found to significantly increase the clearance of thiothixene.
1	0	6	O	Hepatic
2	8	17	B-K	microsomal
3	19	24	I-K	enzyme
4	26	33	I-K	inducing
5	35	40	I-K	agents
6	41	41	O	,
7	43	46	O	such
8	48	49	O	as
9	51	63	B-K	carbamazepine
10	64	64	O	,
11	66	69	O	were
12	71	75	O	found
13	77	78	O	to
14	80	92	O	significantly
15	94	101	B-T	increase
16	103	105	I-T	the
17	107	115	I-T	clearance
18	117	118	O	of
19	120	130	O	XXXXXXXX
K/2:C54355 K/9:C54355

Thiothixene	3065	34073-7	827cf470-485d-4925-85f2-8933a6dea830	13
Patients receiving these drugs should be observed for signs of reduced thiothixene effectiveness4,5.
1	0	7	O	Patients
2	9	17	O	receiving
3	19	23	O	these
4	25	29	O	drugs
5	31	36	O	should
6	38	39	O	be
7	41	48	O	observed
8	50	52	O	for
9	54	58	O	signs
10	60	61	O	of
11	63	69	O	reduced
12	71	81	O	XXXXXXXX
13	83	98	O	effectiveness4,5
NULL

Thiothixene	3066	34073-7	827cf470-485d-4925-85f2-8933a6dea830	31
Due to a possible additive effect with hypotensive agents, patients receiving these drugs should be observed closely for signs of excessive hypotension when thiothixene is added to their drug regimen6.
1	0	2	O	Due
2	4	5	O	to
3	7	7	O	a
4	9	16	O	possible
5	18	25	B-E	additive
6	27	32	I-E	effect
7	34	37	O	with
8	39	49	B-D	hypotensive
9	51	56	I-D	agents
10	57	57	O	,
11	59	66	O	patients
12	68	76	O	receiving
13	78	82	B-D	these
14	84	88	I-D	drugs
15	90	95	O	should
16	97	98	O	be
17	100	107	O	observed
18	109	115	O	closely
19	117	119	O	for
20	121	125	O	signs
21	127	128	O	of
22	130	138	B-E	excessive
23	140	150	I-E	hypotension
24	152	155	O	when
25	157	167	O	XXXXXXXX
26	169	170	O	is
27	172	176	O	added
28	178	179	O	to
29	181	185	O	their
30	187	190	B-D	drug
31	192	199	I-D	regimen6
D/8:5:1 D/8:22:1 D/13:5:1 D/13:22:1 D/30:5:1 D/30:22:1

Thiothixene	3067	34090-1	827cf470-485d-4925-85f2-8933a6dea830	8
Thiothixene is an antipsychotic of the thioxanthene series.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	16	O	an
4	18	30	O	antipsychotic
5	32	33	O	of
6	35	37	O	the
7	39	50	O	thioxanthene
8	52	57	O	series
NULL

Thiothixene	3068	34090-1	827cf470-485d-4925-85f2-8933a6dea830	19
Thiothixene possesses certain chemical and pharmacological similarities to the piperazine phenothiazines and differences from the aliphatic group of phenothiazines.
1	0	10	O	XXXXXXXX
2	12	20	O	possesses
3	22	28	O	certain
4	30	37	O	chemical
5	39	41	O	and
6	43	57	O	pharmacological
7	59	70	O	similarities
8	72	73	O	to
9	75	77	O	the
10	79	88	O	piperazine
11	90	103	O	phenothiazines
12	105	107	O	and
13	109	119	O	differences
14	121	124	O	from
15	126	128	O	the
16	130	138	O	aliphatic
17	140	144	O	group
18	146	147	O	of
19	149	162	B-D	phenothiazines
NULL

