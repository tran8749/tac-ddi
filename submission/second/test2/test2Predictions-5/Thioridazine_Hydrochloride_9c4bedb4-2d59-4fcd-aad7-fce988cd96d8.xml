<?xml version="1.0" ?>
<Label drug="Thioridazine Hydrochloride" setid="9c4bedb4-2d59-4fcd-aad7-fce988cd96d8">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  The basic pharmacological activity of thioridazine is similar to that of other phenothiazines, but is associated with minimal extrapyramidal stimulation.  However, thioridazine has been shown to prolong the QTc interval in a dose dependent fashion. This effect may increase the risk of serious, potentially fatal, ventricular arrhythmias, such as Torsades de pointes type arrhythmias. Due to this risk, thioridazine is indicated only for schizophrenic patients who have not been responsive to or cannot tolerate other antipsychotic agents (see WARNINGS and CONTRAINDICATIONS ). However, the prescriber should be aware that thioridazine has not been systematically evaluated in controlled trials in treatment refractory schizophrenic patients and its efficacy in such patients is unknown.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1725" section="34073-7">
      

      <SentenceText>Reduced cytochrome P450 2D6 isozyme activity, drugs which inhibit this isozyme (e.g., fluoxetine and paroxetine), and certain other drugs (e.g., fluvoxamine, propranolol, and pindolol) appear to appreciably inhibit the metabolism of thioridazine.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="207 22" str="inhibit the metabolism" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="8 36" str="cytochrome P450 2D6 isozyme activity" type="Precipitant"/>
      <Interaction effect="C54358" id="I1" precipitant="M2" trigger="M1" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1726" section="34073-7">
      

      <SentenceText>The resulting elevated levels of thioridazine would be expected to augment the prolongation of the QTc interval associated with thioridazine and may increase the risk of serious, potentially fatal, cardiac arrhythmias, such as Torsades de pointes type arrhythmias.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="79 12" str="prolongation" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M4" span="95 16" str="the QTc interval" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1727" section="34073-7">
      

      <SentenceText>Such an increased risk may result also from the additive effect of coadministering thioridazine with other agents that prolong the QTc interval.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="48 15" str="additive effect" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M6" span="119 7" str="prolong" type="Precipitant"/>
      <Interaction effect="M5" id="I2" precipitant="M6" trigger="M6" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M7" span="131 12" str="QTc interval" type="Precipitant"/>
      <Interaction effect="M5" id="I3" precipitant="M7" trigger="M7" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1728" section="34073-7">
      

      <SentenceText>Therefore, thioridazine is contraindicated with these drugs as well as in patients, comprising about 7% of the normal population, who are known to have a genetic defect leading to reduced levels of activity of P450 2D6.</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="180 26" str="reduced levels of activity" type="Trigger"/>
      <Mention code="NO MAP" id="M9" span="210 8" str="P450 2D6" type="Precipitant"/>
      <Interaction effect="C54358" id="I4" precipitant="M9" trigger="M8" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1729" section="34073-7">
      

      <SentenceText>In a study of 19 healthy male subjects, which included 6 slow and 13 rapid hydroxylators of debrisoquin, a single 25 mg oral dose of thioridazine produced a 2.4-fold higher Cmax and a 4.5-fold higher AUC for thioridazine in the slow hydroxylators compared to rapid hydroxylators.</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="166 11" str="higher Cmax" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="193 10" str="higher AUC" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1730" section="34073-7">
      

      <SentenceText>The rate of debrisoquin hydroxylation is felt to depend on the level of cytochrome P450 2D6 isozyme activity.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="72 36" str="cytochrome P450 2D6 isozyme activity" type="Precipitant"/>
      <Interaction effect="C54358" id="I5" precipitant="M12" trigger="M12" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1731" section="34073-7">
      

      <SentenceText>Thus, this study suggests that drugs that inhibit P450 2D6 or the presence of reduced activity levels of this isozyme will produce elevated plasma levels of thioridazine.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="78 23" str="reduced activity levels" type="Trigger"/>
      <Mention code="NO MAP" id="M14" span="131 15" str="elevated plasma" type="Trigger"/>
      <Mention code="NO MAP" id="M15" span="31 30" str="drugs that inhibit P450 2D6 or" type="Precipitant"/>
      <Interaction effect="C54355" id="I6" precipitant="M15" trigger="M14" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M16" span="102 15" str="of this isozyme" type="Precipitant"/>
      <Interaction effect="C54358" id="I7" precipitant="M16" trigger="M13" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1732" section="34073-7">
      

      <SentenceText>Therefore, the coadministration of drugs that inhibit P450 2D6 with thioridazine and the use of thioridazine in patients known to have reduced activity of P450 2D6 are contraindicated. for one week) on thioridazine steady-state concentration was evaluated in ten male inpatients with schizophrenia.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="130 21" str="have reduced activity" type="Trigger"/>
      <Mention code="NO MAP" id="M18" span="35 27" str="drugs that inhibit P450 2D6" type="Precipitant"/>
      <Interaction effect="C54356" id="I8" precipitant="M18" trigger="M17" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M19" span="160 3" str="2D6" type="Precipitant"/>
      <Interaction effect="C54358" id="I9" precipitant="M19" trigger="M17" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1733" section="34073-7">
      

      <SentenceText>Concentrations of thioridazine and its two active metabolites, mesoridazine and sulforidazine, increased 3-fold following coadministration of fluvoxamine.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="95 9" str="increased" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="142 11" str="fluvoxamine" type="Precipitant"/>
      <Interaction effect="C54357" id="I10" precipitant="M21" trigger="M20" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1734" section="34073-7">
      

      <SentenceText>Fluvoxamine and thioridazine should not be coadministered.</SentenceText>
      

      <Mention code="NO MAP" id="M22" span="36 21" str="not be coadministered" type="Trigger"/>
      <Mention code="NO MAP" id="M23" span="0 11" str="Fluvoxamine" type="Precipitant"/>
      <Interaction id="I11" precipitant="M23" trigger="M22" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1735" section="34073-7">
      

      <SentenceText>Concurrent administration of propranolol (100 to 800 mg daily) has been reported to produce increases in plasma levels of thioridazine (approximately 50% to 400%) and its metabolites (approximately 80% to 300%).</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="29 11" str="propranolol" type="Precipitant"/>
      <Interaction effect="C54355" id="I12" precipitant="M24" trigger="M24" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1736" section="34073-7">
      

      <SentenceText>Propranolol and thioridazine should not be coadministered.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="29 6" str="should" type="Trigger"/>
      <Mention code="NO MAP" id="M26" span="36 21" str="not be coadministered" type="Trigger"/>
      <Mention code="NO MAP" id="M27" span="0 11" str="Propranolol" type="Precipitant"/>
      <Interaction id="I13" precipitant="M27" trigger="M26" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1737" section="34073-7">
      

      <SentenceText>Concurrent administration of pindolol and thioridazine have resulted in moderate, dose related increases in the serum levels of thioridazine and two of its metabolites, as well as higher than expected serum pindolol levels.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="82 4" str="dose" type="Trigger"/>
      <Mention code="NO MAP" id="M29" span="95 29" str="increases in the serum levels" type="Trigger"/>
      <Mention code="NO MAP" id="M30" span="29 8" str="pindolol" type="Precipitant"/>
      <Interaction effect="C54355" id="I14" precipitant="M30" trigger="M29" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M31" span="72 8" str="moderate" type="Precipitant"/>
      <Interaction effect="C54355" id="I15" precipitant="M31" trigger="M28" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1738" section="34073-7">
      

      <SentenceText>Pindolol and thioridazine should not be coadministered.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="33 6" str="not be" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1739" section="34073-7">
      

      <SentenceText>There are no studies of the coadministration of thioridazine and other drugs that prolong the QTc interval.</SentenceText>
      

      <Mention code="NO MAP" id="M33" span="90 16" str="the QTc interval" type="Precipitant"/>
      <Interaction id="I16" precipitant="M33" trigger="M33" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1740" section="34073-7">
      

      <SentenceText>However, it is expected that such coadministration would produce additive prolongation of the QTc interval and, thus, such use is contraindicated.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="65 21" str="additive prolongation" type="Trigger"/>
      <Mention code="NO MAP" id="M35" span="130 15" str="contraindicated" type="Trigger"/>
      <Mention code="NO MAP" id="M36" span="94 12" str="QTc interval" type="Precipitant"/>
      <Interaction id="I17" precipitant="M36" trigger="M35" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1741" section="34090-1">
      

      <SentenceText>The basic pharmacological activity of thioridazine is similar to that of other phenothiazines, but is associated with minimal extrapyramidal stimulation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1742" section="34090-1">
      

      <SentenceText>However, thioridazine has been shown to prolong the QTc interval in a dose dependent fashion.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1743" section="34090-1">
      

      <SentenceText>This effect may increase the risk of serious, potentially fatal, ventricular arrhythmias, such as Torsades de pointes type arrhythmias.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="5 6" str="effect" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1744" section="34090-1">
      

      <SentenceText>Due to this risk, thioridazine is indicated only for schizophrenic patients who have not been responsive to or cannot tolerate other antipsychotic agents.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Thioridazine Hydrochloride" id="1745" section="34090-1">
      

      <SentenceText>However, the prescriber should be aware that thioridazine has not been systematically evaluated in controlled trials in treatment refractory schizophrenic patients and its efficacy in such patients is unknown.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

