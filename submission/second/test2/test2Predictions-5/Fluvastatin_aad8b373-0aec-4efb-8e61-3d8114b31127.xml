<?xml version="1.0" ?>
<Label drug="Fluvastatin" setid="aad8b373-0aec-4efb-8e61-3d8114b31127">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS  •  Cyclosporine: Combination increases fluvastatin exposure. Limit fluvastatin dose to 20 mg ( 2.4 , 7.1 )  •  Fluconazole: Combination increases fluvastatin exposure. Limit fluvastatin dose to 20 mg ( 2.5 , 7.2 )  •  Concomitant lipid-lowering therapies: Use with fibrates or lipid-modifying doses (≥ 1 g/day) of niacin increases the risk of adverse skeletal muscle effects. Caution should be used when prescribing with fluvastatin ( 5.1 , 7.3 , 7.4 )  •  Glyburide: Monitor blood glucose levels when fluvastatin dose is changed ( 7 )  •  Phenytoin: Monitor plasma phenytoin levels when fluvastatin treatment is initiated or when the dosage is changed ( 7 )  •  Warfarin and coumarin derivates: Monitor prothrombin times when fluvastatin coadministration is initiated, discontinued, or the dosage changed ( 7 )  7.1 Cyclosporine  Cyclosporine coadministration increases fluvastatin exposure. Therefore, in patients taking cyclosporine, therapy should be limited to fluvastatin 20 mg twice daily [ see Warnings and Precautions ( 5.1 ) and Clinical Pharmacology ( 12.3 ) ].  7.2 Fluconazole  Administration of fluvastatin 40 mg single dose to healthy volunteers pre-treated with fluconazole for 4 days results in an increase of fluvastatin exposure. Therefore, in patients taking fluconazole, therapy should be limited to fluvastatin 20 mg twice daily [ see Clinical Pharmacology ( 12.3 ) ] .  7.3 Gemfibrozil  Due to an increased risk of myopathy/rhabdomyolysis when HMG-CoA reductase inhibitors are coadministered with gemfibrozil, concomitant administration of fluvastatin sodium with gemfibrozil should be avoided.  7.4 Other Fibrates  Because it is known that the risk of myopathy during treatment with HMG-CoA reductase inhibitors is increased with concurrent administration of other fibrates, fluvastatin sodium should be administered with caution when used concomitantly with other fibrates [ see Warnings and Precautions ( 5.1 ) and Clinical Pharmacology ( 12.3 ) ].  7.5 Niacin  The risk of skeletal muscle effects may be enhanced when fluvastatin sodium is used in combination with lipid-modifying doses (≥ 1 g/day) of niacin; a reduction in fluvastatin sodium dosage should be considered in this setting [ see Warnings and Precautions ( 5.1 ) ].  7.6 Glyburide  Concomitant administration of fluvastatin and glyburide increased glyburide exposures. Patients on concomitant therapy of glyburide and fluvastatin should continue to be monitored appropriately [ see Clinical Pharmacology ( 12.3 ) ].  7.7 Phenytoin  Concomitant administration of fluvastatin and phenytoin increased phenytoin exposures. Patients should continue to be monitored appropriately when fluvastatin therapy is initiated or when fluvastatin dose is changed [ see Clinical Pharmacology ( 12.3 ) ].  7.8 Warfarin  Bleeding and/or increased prothrombin times have been reported in patients taking coumarin anticoagulants concomitantly with other HMG-CoA reductase inhibitors. Therefore, patients receiving warfarin-type anticoagulants should have their prothrombin times closely monitored when fluvastatin sodium is initiated or the dosage of fluvastatin sodium is changed.  7.9 Colchicine  Cases of myopathy, including rhabdomyolysis, have been reported with fluvastatin coadministered with colchicine, and caution should be exercised when prescribing fluvastatin with colchicine.</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY  12.1 Mechanism of Action  Fluvastatin sodium is a competitive inhibitor of HMG-CoA reductase, the rate limiting enzyme that converts 3-hydroxy-3-methylglutaryl-coenzyme A (HMG-CoA) to mevalonate, a precursor of sterols, including cholesterol. The inhibition of cholesterol biosynthesis reduces the cholesterol in hepatic cells, which stimulates the synthesis of LDL receptors and thereby increases the uptake of LDL particles. The end result of these biochemical processes is a reduction of the plasma cholesterol concentration.  12.3 Pharmacokinetics  Absorption:  Following oral administration of the capsule, fluvastatin reaches peak concentrations in less than 1 hour. The absolute bioavailability is 24% (range 9% to 50%) after administration of a 10 mg dose.  At steady state, administration of fluvastatin with the evening meal results in a 50% decrease in C max , an 11% decrease in AUC, and a more than two-fold increase in t max as compared to administration 4 hours after the evening meal. No significant differences in the lipid-lowering effects were observed between the two administrations. After single or multiple doses above 20 mg, fluvastatin exhibits saturable first-pass metabolism resulting in more than dose proportional plasma fluvastatin concentrations.  Fluvastatin administered as fluvastatin sodium extended-release 80 mg tablets reaches peak concentration in approximately 3 hours under fasting conditions, after a low fat meal, or 2.5 hours after a low fat meal. The mean relative bioavailability of the extended-release tablet is approximately 29% (range: 9% to 66%) compared to that of the fluvastatin immediate-release capsule administered under fasting conditions. Administration of a high fat meal delayed the absorption (T max : 6h) and increased the bioavailability of the extended-release tablet by approximately 50%. However, the maximum concentration of fluvastatin sodium extended-release tablets seen after a high fat meal is less than the peak concentration following a single dose or twice daily dose of the 40 mg fluvastatin capsule.  Distribution:  Fluvastatin is 98% bound to plasma proteins. The mean volume of distribution (VD ss ) is estimated at 0.35 L/kg. At therapeutic concentrations, the protein binding of fluvastatin is not affected by warfarin, salicylic acid and glyburide.  Metabolism:  Fluvastatin is metabolized in the liver, primarily via hydroxylation of the indole ring at the 5 and 6 positions. N-dealkylation and beta-oxidation of the side-chain also occurs. The hydroxy metabolites have some pharmacologic activity, but do not circulate in the blood. Fluvastatin has two enantiomers. Both enantiomers of fluvastatin are metabolized in a similar manner.  In vitro data indicate that fluvastatin metabolism involves multiple Cytochrome P450 (CYP) isozymes. CYP2C9 isoenzyme is primarily involved in the metabolism of fluvastatin (approximately 75%), while CYP2C8 and CYP3A4 isoenzymes are involved to a much less extent, i.e., approximately 5% and approximately 20%, respectively.  Excretion:  Following oral administration, fluvastatin is primarily (about 90%) excreted in the feces as metabolites, with less than 2% present as unchanged drug. Approximately 5% of a radiolabeled oral dose were recovered in urine. The elimination half-life (t 1/2 ) of fluvastatin is approximately 3 hours.  Specific Populations  Renal Impairment:  In patients with moderate to severe renal impairment (CL Cr 10 to 40 mL/min), AUC and C max increased approximately 1.2 fold after administration of a single dose of 40 mg fluvastatin compared to healthy volunteers. In patients with end-stage renal disease on hemodialysis, the AUC increased by approximately 1.5 fold. Fluvastatin sodium extended-release tablets were not evaluated in patients with renal impairment. However, systemic exposures after administration of fluvastatin sodium extended-release tablets are lower than after the 40 mg immediate-release capsule.  Hepatic Impairment:  In patients with hepatic impairment due to liver cirrhosis, fluvastatin AUC and C max increased approximately 2.5 fold compared to healthy subjects after administration of a single 40 mg dose. The enantiomer ratios of the two isomers of fluvastatin in hepatic impairment patients were comparable to those observed in healthy subjects.  Geriatric:  Plasma levels of fluvastatin are not significantly different in patients age &gt; 65 years compared to patients age 21 to 49 years.  Gender:  In a study evaluating the effect of age and gender on fluvastatin pharmacokinetics, there were no significant differences in fluvastatin exposures between males and females, except between younger females and younger males (both ages 21 to 49 years), where there was an approximate 30% increase in AUC in females. Adjusting for body weight decreases the magnitude of the differences seen.  Pediatric:  Pharmacokinetic data in the pediatric population are not available.  Drug-Drug Interactions:  Data from drug-drug interactions studies involving coadministration of gemfibrozil, niacin, itraconazole, erythromycin, tolbutamide or clopidogrel indicate that the PK disposition of fluvastatin is not significantly altered when fluvastatin is coadministered with any of these drugs.  The below listed drug interaction information is derived from studies using fluvastatin capsules.  Table 3: Effect of Coadministered Drugs on Fluvastatin Systemic Exposure  Coadministered drug and dosing regimen  Fluvastatin  Dose (mg)  Single dose unless otherwise noted  Change in AUC  Mean ratio (with/without coadministered drug and no change = 1 fold) or % change (with/without coadministered drug and no change = 0%); symbols of ↑ and ↓ indicate the exposure increase and decrease, respectively.  Change in C max  Cyclosporine - stable dose (b.i.d.) Considered clinically significant [ see Dosage and Administration ( 2 ) and Drug Interactions ( 7 ) ]  20 mg QD for 14 weeks  ↑90%  ↑30%  Fluconazole 400 mg QD day 1,200 mg b.i.d. day 2 to 4  40 mg QD  ↑84%  ↑44%  Cholestyramine 8 g QD  20 mg QD administered 4 hrs after a meal plus cholestyramine  ↓51%  ↓83%  Rifampicin 600 mg QD for 6 days  20 mg QD  ↓53%  ↓42%  Cimetidine 400 mg b.i.d. for 5 days, QD on Day 6  20 mg QD  ↑30%  ↑40%  Ranitidine 150 mg b.i.d. for 5 days, QD on Day 6  20 mg QD  ↑10%  ↑50%  Omeprazole 40 mg QD for 6 days  20 mg QD  ↑20%  ↑37%  Phenytoin 300 mg QD  40 mg b.i.d. for 5 days  ↑40%  ↑27%  Propranolol 40 mg b.i.d. for 3.5 days  40 mg QD  ↓5%  No change  Digoxin 0.1 to 0.5 mg QD for 3 weeks  40 mg QD  No change  ↑11%  Diclofenac 25 mg QD  40 mg QD for 8 days  ↑50%  ↑80%  Glyburide 5 to 20 mg QD for 22 days  40 mg b.i.d. for 14 days  ↑51%  ↑44%  Warfarin 30 mg QD  40 mg QD for 8 days  ↑30%  ↑67%  Clopidogrel 300 mg loading dose on day 10, 75 mg QD on days 11 to 19  fluvastatin sodium extended-release tablets, 80 mg QD for 19 days  ↓2%  ↑27%  Data from drug-drug interaction studies involving fluvastatin and coadministration of either gemfibrozil, tolbutamide or losartan indicate that the PK disposition of either gemfibrozil, tolbutamide or losartan is not significantly altered when coadministered with fluvastatin.  Table 4: Effect of Fluvastatin Coadministration on Systemic Exposure of Other Drugs  Fluvastatin dosage regimen  Coadministered drug  Name and Dose (mg)  Single dose unless otherwise noted  Change in AUC  Mean ratio (with/without coadministered drug and no change = 1 fold) or % change (with/without coadministered drug and no change = 0%); symbols of ↑ and ↓ indicate the exposure increase and decrease, respectively.  Change in C max  40 mg QD for 5 days  Phenytoin 300 mg QD Considered clinically significant [ see Dosage and Administration ( 2 ) and Drug Interactions ( 7 ) ]  ↑20%  ↑5%  40 mg b.i.d. for 21 days  Glyburide 5 to 20 mg QD for 22 days  ↑70%  ↑50%  40 mg QD for 8 days  Diclofenac 25 mg QD  ↑25%  ↑60%  40 mg QD for 8 days  Warfarin 30 mg QD  S-warfarin: ↑7%  S-warfarin: ↑10%  R-warfarin: no change  R-warfarin: ↑6%</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Fluvastatin" id="150" section="34073-7">
      

      <SentenceText>Cyclosporine: Combination increases fluvastatin exposure.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="0 12" str="Cyclosporine" type="Precipitant"/>
      <Interaction effect="C54355" id="I1" precipitant="M1" trigger="M1" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="151" section="34073-7">
      

      <SentenceText>Limit fluvastatin dose to 20 mg (2.4, 7.1) Fluconazole: Combination increases fluvastatin exposure.</SentenceText>
      

      <Mention code="NO MAP" id="M2" span="43 11" str="Fluconazole" type="Precipitant"/>
      <Interaction effect="C54355" id="I2" precipitant="M2" trigger="M2" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="152" section="34073-7">
      

      <SentenceText>Limit fluvastatin dose to 20 mg (2.5, 7.2) Concomitant lipid-lowering therapies: Use with fibrates or lipid-modifying doses (≥ 1 g/day) of niacin increases the risk of adverse skeletal muscle effects.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="0 5" str="Limit" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="165 34" str="of adverse skeletal muscle effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M5" span="90 8" str="fibrates" type="Precipitant"/>
      <Interaction id="I3" precipitant="M5" trigger="M3" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M6" span="139 6" str="niacin" type="Precipitant"/>
      <Interaction effect="M4" id="I4" precipitant="M6" trigger="M3" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="153" section="34073-7">
      

      <SentenceText>Caution should be used when prescribing with fluvastatin (5.1, 7.3, 7.4) Glyburide: Monitor blood glucose levels when fluvastatin dose is changed (7) Phenytoin: Monitor plasma phenytoin levels when fluvastatin treatment is initiated or when the dosage is changed (7) Warfarin and coumarin derivates: Monitor prothrombin times when fluvastatin coadministration is initiated, discontinued, or the dosage changed (7) Cyclosporine coadministration increases fluvastatin exposure.</SentenceText>
      

      <Mention code="NO MAP" id="M7" span="0 7" str="Caution" type="Trigger"/>
      <Mention code="NO MAP" id="M8" span="84 7" str="Monitor" type="Trigger"/>
      <Mention code="NO MAP" id="M9" span="161 7" str="Monitor" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="73 9" str="Glyburide" type="Precipitant"/>
      <Interaction id="I5" precipitant="M10" trigger="M9" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M11" span="150 9" str="Phenytoin" type="Precipitant"/>
      <Interaction id="I6" precipitant="M11" trigger="M8" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M12" span="176 9" str="phenytoin" type="Precipitant"/>
      <Interaction id="I7" precipitant="M12" trigger="M7" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="154" section="34073-7">
      

      <SentenceText>Therefore, in patients taking cyclosporine, therapy should be limited to fluvastatin 20 mg twice daily.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="59 10" str="be limited" type="Trigger"/>
      <Mention code="NO MAP" id="M14" span="30 12" str="cyclosporine" type="Precipitant"/>
      <Interaction id="I8" precipitant="M14" trigger="M13" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="155" section="34073-7">
      

      <SentenceText>Administration of fluvastatin 40 mg single dose to healthy volunteers pre-treated with fluconazole for 4 days results in an increase of fluvastatin exposure.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="87 11" str="fluconazole" type="Precipitant"/>
      <Interaction effect="C54355" id="I9" precipitant="M15" trigger="M15" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="156" section="34073-7">
      

      <SentenceText>Therefore, in patients taking fluconazole, therapy should be limited to fluvastatin 20 mg twice daily.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="51 17" str="should be limited" type="Trigger"/>
      <Mention code="NO MAP" id="M17" span="30 11" str="fluconazole" type="Precipitant"/>
      <Interaction id="I10" precipitant="M17" trigger="M16" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="157" section="34073-7">
      

      <SentenceText>Due to an increased risk of myopathy/rhabdomyolysis when HMG-CoA reductase inhibitors are coadministered with gemfibrozil, concomitant administration of fluvastatin sodium with gemfibrozil should be avoided.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="28 8" str="myopathy" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M19" span="37 14" str="rhabdomyolysis" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M20" span="199 7" str="avoided" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="57 28" str="HMG-CoA reductase inhibitors" type="Precipitant"/>
      <Interaction effect="M18;M19" id="I11" precipitant="M21" trigger="M20" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M22" span="110 11" str="gemfibrozil" type="Precipitant"/>
      <Interaction effect="M18;M19" id="I12" precipitant="M22" trigger="M20" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M23" span="177 11" str="gemfibrozil" type="Precipitant"/>
      <Interaction id="I13" precipitant="M23" trigger="M20" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="158" section="34073-7">
      

      <SentenceText>Because it is known that the risk of myopathy during treatment with HMG-CoA reductase inhibitors is increased with concurrent administration of other fibrates, fluvastatin sodium should be administered with caution when used concomitantly with other fibrates.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="100 9" str="increased" type="Trigger"/>
      <Mention code="NO MAP" id="M25" span="189 12" str="administered" type="Trigger"/>
      <Mention code="NO MAP" id="M26" span="207 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M27" span="68 28" str="HMG-CoA reductase inhibitors" type="Precipitant"/>
      <Interaction id="I14" precipitant="M27" trigger="M26" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M28" span="150 8" str="fibrates" type="Precipitant"/>
      <Interaction id="I15" precipitant="M28" trigger="M25" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M29" span="172 6" str="sodium" type="Precipitant"/>
      <Interaction id="I16" precipitant="M29" trigger="M24" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M30" span="250 8" str="fibrates" type="Precipitant"/>
      <Interaction id="I17" precipitant="M30" trigger="M24" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="159" section="34073-7">
      

      <SentenceText>The risk of skeletal muscle effects may be enhanced when fluvastatin sodium is used in combination with lipid-modifying doses (≥ 1 g/day) of niacin; a reduction in fluvastatin sodium dosage should be considered in this setting.</SentenceText>
      

      <Mention code="NO MAP" id="M31" span="28 7" str="effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M32" span="69 6" str="sodium" type="Precipitant"/>
      <Interaction effect="M31" id="I18" precipitant="M32" trigger="M32" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M33" span="141 6" str="niacin" type="Precipitant"/>
      <Interaction effect="C54355" id="I19" precipitant="M33" trigger="M33" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M34" span="176 6" str="sodium" type="Precipitant"/>
      <Interaction id="I20" precipitant="M34" trigger="M34" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="160" section="34073-7">
      

      <SentenceText>Concomitant administration of fluvastatin and glyburide increased glyburide exposures.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="161" section="34073-7">
      

      <SentenceText>Patients on concomitant therapy of glyburide and fluvastatin should continue to be monitored appropriately.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="162" section="34073-7">
      

      <SentenceText>Concomitant administration of fluvastatin and phenytoin increased phenytoin exposures.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="163" section="34073-7">
      

      <SentenceText>Patients should continue to be monitored appropriately when fluvastatin therapy is initiated or when fluvastatin dose is changed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="164" section="34073-7">
      

      <SentenceText>Bleeding and/or increased prothrombin times have been reported in patients taking coumarin anticoagulants concomitantly with other HMG-CoA reductase inhibitors.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="0 8" str="Bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M36" span="16 27" str="increased prothrombin times" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M37" span="82 23" str="coumarin anticoagulants" type="Precipitant"/>
      <Interaction effect="M35;M36" id="I21" precipitant="M37" trigger="M37" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M38" span="131 28" str="HMG-CoA reductase inhibitors" type="Precipitant"/>
      <Interaction effect="M35;M36" id="I22" precipitant="M38" trigger="M38" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="165" section="34073-7">
      

      <SentenceText>Therefore, patients receiving warfarin-type anticoagulants should have their prothrombin times closely monitored when fluvastatin sodium is initiated or the dosage of fluvastatin sodium is changed.</SentenceText>
      

      <Mention code="NO MAP" id="M39" span="30 28" str="warfarin-type anticoagulants" type="Precipitant"/>
      <Interaction id="I23" precipitant="M39" trigger="M39" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="166" section="34073-7">
      

      <SentenceText>Cases of myopathy, including rhabdomyolysis, have been reported with fluvastatin coadministered with colchicine, and caution should be exercised when prescribing fluvastatin with colchicine.</SentenceText>
      

      <Mention code="NO MAP" id="M40" span="0 5" str="Cases" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M41" span="9 8" str="myopathy" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M42" span="29 14" str="rhabdomyolysis" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M43" span="117 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M44" span="101 10" str="colchicine" type="Precipitant"/>
      <Interaction effect="M40;M41;M42" id="I24" precipitant="M44" trigger="M43" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M45" span="179 10" str="colchicine" type="Precipitant"/>
      <Interaction effect="M40;M41;M42" id="I25" precipitant="M45" trigger="M43" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="167" section="34090-1">
      

      <SentenceText>Fluvastatin sodium is a competitive inhibitor of HMG-CoA reductase, the rate limiting enzyme that converts 3-hydroxy-3-methylglutaryl-coenzyme A (HMG-CoA) to mevalonate, a precursor of sterols, including cholesterol.</SentenceText>
      

      <Mention code="NO MAP" id="M46" span="53 13" str="CoA reductase" type="Precipitant"/>
      <Interaction effect="C54356" id="I26" precipitant="M46" trigger="M46" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="168" section="34090-1">
      

      <SentenceText>The inhibition of cholesterol biosynthesis reduces the cholesterol in hepatic cells, which stimulates the synthesis of LDL receptors and thereby increases the uptake of LDL particles.</SentenceText>
      

      <Mention code="NO MAP" id="M47" span="145 20" str="increases the uptake" type="Trigger"/>
      <Mention code="NO MAP" id="M48" span="119 3" str="LDL" type="Precipitant"/>
      <Interaction effect="C54356" id="I27" precipitant="M48" trigger="M47" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="169" section="34090-1">
      

      <SentenceText>The end result of these biochemical processes is a reduction of the plasma cholesterol concentration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="170" section="34090-1">
      

      <SentenceText>Absorption: Following oral administration of the capsule, fluvastatin reaches peak concentrations in less than 1 hour.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="171" section="34090-1">
      

      <SentenceText>The absolute bioavailability is 24% (range 9% to 50%) after administration of a 10 mg dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="172" section="34090-1">
      

      <SentenceText>At steady state, administration of fluvastatin with the evening meal results in a 50% decrease in Cmax, an 11% decrease in AUC, and a more than two-fold increase in tmax as compared to administration 4 hours after the evening meal.</SentenceText>
      

      <Mention code="NO MAP" id="M49" span="111 15" str="decrease in AUC" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="173" section="34090-1">
      

      <SentenceText>No significant differences in the lipid-lowering effects were observed between the two administrations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="174" section="34090-1">
      

      <SentenceText>After single or multiple doses above 20 mg, fluvastatin exhibits saturable first-pass metabolism resulting in more than dose proportional plasma fluvastatin concentrations.</SentenceText>
      

      <Mention code="NO MAP" id="M50" span="86 10" str="metabolism" type="Trigger"/>
      <Mention code="NO MAP" id="M51" span="115 9" str="than dose" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="175" section="34090-1">
      

      <SentenceText>Fluvastatin administered as fluvastatin sodium extended-release 80 mg tablets reaches peak concentration in approximately 3 hours under fasting conditions, after a low fat meal, or 2.5 hours after a low fat meal.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="176" section="34090-1">
      

      <SentenceText>The mean relative bioavailability of the extended-release tablet is approximately 29% (range: 9% to 66%) compared to that of the fluvastatin immediate-release capsule administered under fasting conditions.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="177" section="34090-1">
      

      <SentenceText>Administration of a high fat meal delayed the absorption (Tmax: 6h) and increased the bioavailability of the extended-release tablet by approximately 50%.</SentenceText>
      

      <Mention code="NO MAP" id="M52" span="72 29" str="increased the bioavailability" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="178" section="34090-1">
      

      <SentenceText>However, the maximum concentration of fluvastatin sodium extended-release tablets seen after a high fat meal is less than the peak concentration following a single dose or twice daily dose of the 40 mg fluvastatin capsule.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="179" section="34090-1">
      

      <SentenceText>Distribution: Fluvastatin is 98% bound to plasma proteins.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="180" section="34090-1">
      

      <SentenceText>The mean volume of distribution (VDss) is estimated at 0.35 L/kg.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="181" section="34090-1">
      

      <SentenceText>At therapeutic concentrations, the protein binding of fluvastatin is not affected by warfarin, salicylic acid and glyburide.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="182" section="34090-1">
      

      <SentenceText>Metabolism: Fluvastatin is metabolized in the liver, primarily via hydroxylation of the indole ring at the 5 and 6 positions.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="183" section="34090-1">
      

      <SentenceText>N-dealkylation and beta-oxidation of the side-chain also occurs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="184" section="34090-1">
      

      <SentenceText>The hydroxy metabolites have some pharmacologic activity, but do not circulate in the blood.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="185" section="34090-1">
      

      <SentenceText>Both enantiomers of fluvastatin are metabolized in a similar manner.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="186" section="34090-1">
      

      <SentenceText>In vitro data indicate that fluvastatin metabolism involves multiple Cytochrome P450 (CYP) isozymes.</SentenceText>
      

      <Mention code="NO MAP" id="M53" span="86 13" str="CYP) isozymes" type="Precipitant"/>
      <Interaction effect="C54356" id="I28" precipitant="M53" trigger="M53" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="187" section="34090-1">
      

      <SentenceText>CYP2C9 isoenzyme is primarily involved in the metabolism of fluvastatin (approximately 75%), while CYP2C8 and CYP3A4 isoenzymes are involved to a much less extent, i.e., approximately 5% and approximately 20%, respectively.</SentenceText>
      

      <Mention code="NO MAP" id="M54" span="99 6" str="CYP2C8" type="Precipitant"/>
      <Interaction effect="C54355" id="I29" precipitant="M54" trigger="M54" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M55" span="110 17" str="CYP3A4 isoenzymes" type="Precipitant"/>
      <Interaction effect="C54355" id="I30" precipitant="M55" trigger="M55" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="188" section="34090-1">
      

      <SentenceText>Excretion: Following oral administration, fluvastatin is primarily (about 90%) excreted in the feces as metabolites, with less than 2% present as unchanged drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="189" section="34090-1">
      

      <SentenceText>Approximately 5% of a radiolabeled oral dose were recovered in urine.</SentenceText>
      

      <Mention code="NO MAP" id="M56" span="22 17" str="radiolabeled oral" type="Precipitant"/>
      <Interaction effect="C54358" id="I31" precipitant="M56" trigger="M56" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="190" section="34090-1">
      

      <SentenceText>The elimination half-life (t1/2) of fluvastatin is approximately 3 hours.</SentenceText>
      

      <Mention code="NO MAP" id="M57" span="16 9" str="half-life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="191" section="34090-1">
      

      <SentenceText>Specific Populations Renal Impairment: In patients with moderate to severe renal impairment (CLCr 10 to 40 mL/min), AUC and Cmax increased approximately 1.2 fold after administration of a single dose of 40 mg fluvastatin compared to healthy volunteers.</SentenceText>
      

      <Mention code="NO MAP" id="M58" span="104 2" str="40" type="Precipitant"/>
      <Interaction effect="C54602" id="I32" precipitant="M58" trigger="M58" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I33" precipitant="M58" trigger="M58" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="192" section="34090-1">
      

      <SentenceText>In patients with end-stage renal disease on hemodialysis, the AUC increased by approximately 1.5 fold.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="193" section="34090-1">
      

      <SentenceText>Fluvastatin sodium extended-release tablets were not evaluated in patients with renal impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="194" section="34090-1">
      

      <SentenceText>However, systemic exposures after administration of fluvastatin sodium extended-release tablets are lower than after the 40 mg immediate-release capsule.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="195" section="34090-1">
      

      <SentenceText>Hepatic Impairment: In patients with hepatic impairment due to liver cirrhosis, fluvastatin AUC and Cmax increased approximately 2.5 fold compared to healthy subjects after administration of a single 40 mg dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="196" section="34090-1">
      

      <SentenceText>The enantiomer ratios of the two isomers of fluvastatin in hepatic impairment patients were comparable to those observed in healthy subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="197" section="34090-1">
      

      <SentenceText>Geriatric: Plasma levels of fluvastatin are not significantly different in patients age &gt; 65 years compared to patients age 21 to 49 years.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="198" section="34090-1">
      

      <SentenceText>Gender: In a study evaluating the effect of age and gender on fluvastatin pharmacokinetics, there were no significant differences in fluvastatin exposures between males and females, except between younger females and younger males (both ages 21 to 49 years), where there was an approximate 30% increase in AUC in females.</SentenceText>
      

      <Mention code="NO MAP" id="M59" span="294 15" str="increase in AUC" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="199" section="34090-1">
      

      <SentenceText>Adjusting for body weight decreases the magnitude of the differences seen.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="200" section="34090-1">
      

      <SentenceText>Pediatric: Pharmacokinetic data in the pediatric population are not available.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="201" section="34090-1">
      

      <SentenceText>Drug-Drug Interactions: Data from drug-drug interactions studies involving coadministration of gemfibrozil, niacin, itraconazole, erythromycin, tolbutamide or clopidogrel indicate that the PK disposition of fluvastatin is not significantly altered when fluvastatin is coadministered with any of these drugs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="202" section="34090-1">
      

      <SentenceText>The below listed drug interaction information is derived from studies using fluvastatin capsules.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="203" section="34090-1">
      

      <SentenceText>Table 3: Effect of Coadministered Drugs on Fluvastatin Systemic Exposure Coadministered drug and dosing regimen Fluvastatin Dose (mg) Single dose unless otherwise noted Change in AUC Mean ratio (with/without coadministered drug and no change = 1 fold) or % change (with/without coadministered drug and no change = 0%); symbols of ↑ and ↓ indicate the exposure increase and decrease, respectively.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="204" section="34090-1">
      

      <SentenceText>Change in Cmax Cyclosporine - stable dose (b.i.d.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="205" section="34090-1">
      

      <SentenceText>)Considered clinically significant 20 mg QD for 14 weeks ↑90% ↑30% Fluconazole 400 mg QD day 1,200 mg b.i.d. day 2 to 4 40 mg QD ↑84% ↑44% Cholestyramine 8 g QD 20 mg QD administered 4 hrs after a meal plus cholestyramine ↓51% ↓83% Rifampicin 600 mg QD for 6 days 20 mg QD ↓53% ↓42% Cimetidine 400 mg b.i.d. for 5 days, QD on Day 6 20 mg QD ↑30% ↑40% Ranitidine 150 mg b.i.d. for 5 days, QD on Day 6 20 mg QD ↑10% ↑50% Omeprazole 40 mg QD for 6 days 20 mg QD ↑20% ↑37% Phenytoin 300 mg QD 40 mg b.i.d. for 5 days ↑40% ↑27% Propranolol 40 mg b.i.d. for 3.5 days 40 mg QD ↓5% No change Digoxin 0.1 to 0.5 mg QD for 3 weeks 40 mg QD No change ↑11% Diclofenac 25 mg QD 40 mg QD for 8 days ↑50% ↑80% Glyburide 5 to 20 mg QD for 22 days 40 mg b.i.d. for 14 days ↑51% ↑44% Warfarin 30 mg QD 40 mg QD for 8 days ↑30% ↑67% Clopidogrel 300 mg loading dose on day 10, 75 mg QD on days 11 to 19 fluvastatin sodium extended-release tablets, 80 mg QD for 19 days ↓2% ↑27% Data from drug-drug interaction studies involving fluvastatin and coadministration of either gemfibrozil, tolbutamide or losartan indicate that the PK disposition of either gemfibrozil, tolbutamide or losartan is not significantly altered when coadministered with fluvastatin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="206" section="34090-1">
      

      <SentenceText>Table 4: Effect of Fluvastatin Coadministration on Systemic Exposure of Other Drugs Fluvastatin dosage regimen Coadministered drug Name and Dose (mg) Single dose unless otherwise noted Change in AUC Mean ratio (with/without coadministered drug and no change = 1 fold) or % change (with/without coadministered drug and no change = 0%); symbols of ↑ and ↓ indicate the exposure increase and decrease, respectively.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fluvastatin" id="207" section="34090-1">
      

      <SentenceText>Change in Cmax 40 mg QD for 5 days Phenytoin 300 mg QDConsidered clinically significant ↑20% ↑5% 40 mg b.i.d. for 21 days Glyburide 5 to 20 mg QD for 22 days ↑70% ↑50% 40 mg QD for 8 days Diclofenac 25 mg QD ↑25% ↑60% 40 mg QD for 8 days Warfarin 30 mg QD S-warfarin: ↑7% S-warfarin: ↑10% R-warfarin: no change R-warfarin: ↑6%</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

