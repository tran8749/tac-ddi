<?xml version="1.0" ?>
<Label drug="Apixaban" setid="e9481622-7cc6-418a-acb6-c5450daae9b0">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS  Apixaban is a substrate of both CYP3A4 and P-gp. Inhibitors of CYP3A4 and P-gp increase exposure to apixaban and increase the risk of bleeding. Inducers of CYP3A4 and P-gp decrease exposure to apixaban and increase the risk of stroke and other thromboembolic events.  • Combined P-gp and strong CYP3A4 inhibitors increase blood levels of apixaban. Reduce ELIQUIS dose or avoid coadministration. (2.5 , 7.1 , 12.3)  • Simultaneous use of combined P-gp and strong CYP3A4 inducers reduces blood levels of apixaban: Avoid concomitant use. (7.2 , 12.3)  7.1 Combined P-gp and Strong CYP3A4 Inhibitors  For patients receiving ELIQUIS 5 mg or 10 mg twice daily, the dose of ELIQUIS should be decreased by 50% when coadministered with drugs that are combined P-gp and strong CYP3A4 inhibitors (e.g., ketoconazole, itraconazole, ritonavir) [see  Dosage and Administration (2.5)  and Clinical Pharmacology (12.3) ] .  For patients receiving ELIQUIS at a dose of 2.5 mg twice daily, avoid coadministration with combined P-gp and strong CYP3A4 inhibitors [see  Dosage and Administration (2.5)  and Clinical Pharmacology (12.3) ] .  Clarithromycin  Although clarithromycin is a combined P-gp and strong CYP3A4 inhibitor, pharmacokinetic data suggest that no dose adjustment is necessary with concomitant administration with ELIQUIS [see Clinical Pharmacology (12.3) ] .  7.2 Combined P-gp and Strong CYP3A4 Inducers  Avoid concomitant use of ELIQUIS with combined P-gp and strong CYP3A4 inducers (e.g., rifampin, carbamazepine, phenytoin, St. John’s wort) because such drugs will decrease exposure to apixaban [see Clinical Pharmacology (12.3) ] .  7.3 Anticoagulants and Antiplatelet Agents  Coadministration of antiplatelet agents, fibrinolytics, heparin, aspirin, and chronic NSAID use increases the risk of bleeding.  APPRAISE-2, a placebo-controlled clinical trial of apixaban in high-risk, post-acute coronary syndrome patients treated with aspirin or the combination of aspirin and clopidogrel, was terminated early due to a higher rate of bleeding with apixaban compared to placebo. The rate of ISTH major bleeding was 2.8% per year with apixaban versus 0.6% per year with placebo in patients receiving single antiplatelet therapy and was 5.9% per year with apixaban versus 2.5% per year with placebo in those receiving dual antiplatelet therapy.  In ARISTOTLE, concomitant use of aspirin increased the bleeding risk on ELIQUIS from 1.8% per year to 3.4% per year and concomitant use of aspirin and warfarin increased the bleeding risk from 2.7% per year to 4.6% per year. In this clinical trial, there was limited (2.3%) use of dual antiplatelet therapy with ELIQUIS.</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY  12.1 Mechanism of Action  Apixaban is a selective inhibitor of FXa. It does not require antithrombin III for antithrombotic activity. Apixaban inhibits free and clot-bound FXa, and prothrombinase activity. Apixaban has no direct effect on platelet aggregation, but indirectly inhibits platelet aggregation induced by thrombin. By inhibiting FXa, apixaban decreases thrombin generation and thrombus development.  12.2 Pharmacodynamics  As a result of FXa inhibition, apixaban prolongs clotting tests such as prothrombin time (PT), INR, and activated partial thromboplastin time (aPTT). Changes observed in these clotting tests at the expected therapeutic dose, however, are small, subject to a high degree of variability, and not useful in monitoring the anticoagulation effect of apixaban.  The Rotachrom ® Heparin chromogenic assay was used to measure the effect of apixaban on FXa activity in humans during the apixaban development program. A concentration-dependent increase in anti-FXa activity was observed in the dose range tested and was similar in healthy subjects and patients with AF.  This test is not recommended for assessing the anticoagulant effect of apixaban.  Effect of PCCs on Pharmacodynamics of ELIQUIS  There is no clinical experience to reverse bleeding with the use of 4-factor PCC products in individuals who have received ELIQUIS.  Effects of 4-factor PCCs on the pharmacodynamics of apixaban were studied in healthy subjects. Following administration of apixaban dosed to steady state, endogenous thrombin potential (ETP) returned to pre-apixaban levels 4 hours after the initiation of a 30-minute PCC infusion, compared to 45 hours with placebo. Mean ETP levels continued to increase and exceeded pre-apixaban levels reaching a maximum (34%-51% increase over pre-apixaban levels) at 21 hours after initiating PCC and remained elevated (21%-27% increase) at the end of the study (69 hours after initiation of PCC). The clinical relevance of this increase in ETP is unknown.  Pharmacodynamic Drug Interaction Studies  Pharmacodynamic drug interaction studies with aspirin, clopidogrel, aspirin and clopidogrel, prasugrel, enoxaparin, and naproxen were conducted. No pharmacodynamic interactions were observed with aspirin, clopidogrel, or prasugrel [see Warnings and Precautions (5.2) ] . A 50% to 60% increase in anti-FXa activity was observed when apixaban was coadministered with enoxaparin or naproxen.  Specific Populations  Renal impairment: Anti-FXa activity adjusted for exposure to apixaban was similar across renal function categories.  Hepatic impairment: Changes in anti-FXa activity were similar in patients with mild-to-moderate hepatic impairment and healthy subjects. However, in patients with moderate hepatic impairment, there is no clear understanding of the impact of this degree of hepatic function impairment on the coagulation cascade and its relationship to efficacy and bleeding. Patients with severe hepatic impairment were not studied.  Cardiac Electrophysiology  Apixaban has no effect on the QTc interval in humans at doses up to 50 mg.  12.3 Pharmacokinetics  Apixaban demonstrates linear pharmacokinetics with dose-proportional increases in exposure for oral doses up to 10 mg.  Absorption  The absolute bioavailability of apixaban is approximately 50% for doses up to 10 mg of ELIQUIS. Food does not affect the bioavailability of apixaban. Maximum concentrations (C max ) of apixaban appear 3 to 4 hours after oral administration of ELIQUIS. At doses ≥25 mg, apixaban displays dissolution-limited absorption with decreased bioavailability. Following oral administration of 10 mg of apixaban as 2 crushed 5 mg tablets suspended in 30 mL of water, exposure was similar to that after oral administration of 2 intact 5 mg tablets. Following oral administration of 10 mg of apixaban as 2 crushed 5 mg tablets mixed with 30 g of applesauce, the C max and AUC were 20% and 16% lower, respectively, when compared to administration of 2 intact 5 mg tablets. Following administration of a crushed 5 mg ELIQUIS tablet that was suspended in 60 mL D5W and delivered through a nasogastric tube, exposure was similar to that seen in other clinical trials involving healthy volunteers receiving a single oral 5 mg tablet dose.  Distribution  Plasma protein binding in humans is approximately 87%. The volume of distribution (Vss) is approximately 21 liters.  Metabolism  Approximately 25% of an orally administered apixaban dose is recovered in urine and feces as metabolites. Apixaban is metabolized mainly via CYP3A4 with minor contributions from CYP1A2, 2C8, 2C9, 2C19, and 2J2. O-demethylation and hydroxylation at the 3-oxopiperidinyl moiety are the major sites of biotransformation.  Unchanged apixaban is the major drug-related component in human plasma; there are no active circulating metabolites.  Elimination  Apixaban is eliminated in both urine and feces. Renal excretion accounts for about 27% of total clearance. Biliary and direct intestinal excretion contributes to elimination of apixaban in the feces.  Apixaban has a total clearance of approximately 3.3 L/hour and an apparent half-life of approximately 12 hours following oral administration.  Apixaban is a substrate of transport proteins: P-gp and breast cancer resistance protein.  Drug Interaction Studies  In i  n vitro apixaban studies at concentrations significantly greater than therapeutic exposures, no inhibitory effect on the activity of CYP1A2, CYP2A6, CYP2B6, CYP2C8, CYP2C9, CYP2D6, CYP3A4/5, or CYP2C19, nor induction effect on the activity of CYP1A2, CYP2B6, or CYP3A4/5 were observed. Therefore, apixaban is not expected to alter the metabolic clearance of coadministered drugs that are metabolized by these enzymes. Apixaban is not a significant inhibitor of P-gp.  The effects of coadministered drugs on the pharmacokinetics of apixaban are summarized in Figure 2 [see also Warnings and Precautions (5.2) and Drug Interactions (7) ] .  Figure 2:      Effect of Coadministered Drugs on the Pharmacokinetics of Apixaban  In dedicated studies conducted in healthy subjects, famotidine, atenolol, prasugrel, and enoxaparin did not meaningfully alter the pharmacokinetics of apixaban.  In studies conducted in healthy subjects, apixaban did not meaningfully alter the pharmacokinetics of digoxin, naproxen, atenolol, prasugrel, or acetylsalicylic acid.  Effect of Coadministered Drugs on Pharmacokinetics of Apixaban  Specific Populations  The effects of level of renal impairment, age, body weight, and level of hepatic impairment on the pharmacokinetics of apixaban are summarized in Figure 3.  Figure 3:      Effect of Specific Populations on the Pharmacokinetics of Apixaban          *  ESRD subjects treated with intermittent hemodialysis; reported PK findings             are following single dose of apixaban post hemodialysis.          †   Results reflect CrCl of 15 mL/min based on regression analysis.          ‡   Dashed vertical lines illustrate pharmacokinetic changes that were used to inform             dosing recommendations.          §   No dose adjustment is recommended for nonvalvular atrial fibrillation patients unless             at least 2 of the following patient characteristics (age greater than or equal to 80 years, body weight less than or equal to 60 kg, or serum creatinine greater than or equal to 1.5 mg/dL) are present.  Gender: A study in healthy subjects comparing the pharmacokinetics in males and females showed no meaningful difference.  Race: The results across pharmacokinetic studies in normal subjects showed no differences in apixaban pharmacokinetics among White/Caucasian, Asian, and Black/African American subjects. No dose adjustment is required based on race/ethnicity.  Hemodialysis in ESRD subjects: Systemic exposure to apixaban administered as a single 5 mg dose in ESRD subjects dosed immediately after the completion of a 4-hour hemodialysis session (post-dialysis) is 36% higher when compared to subjects with normal renal function (Figure 3). The systemic exposure to apixaban administered 2 hours prior to a 4-hour hemodialysis session with a dialysate flow rate of 500 mL/min and a blood flow rate in the range of 350 to 500 mL/min is 17% higher compared to those with normal renal function. The dialysis clearance of apixaban is approximately 18 mL/min. The systemic exposure of apixaban is 14% lower on dialysis when compared to not on dialysis.  Protein binding was similar (92%-94%) between healthy controls and ESRD subjects during the on-dialysis and off-dialysis periods.  O:\E-Submissions\PBO\PRODUCTS\Eliquis 202155\08. PAS ESRD Dosing\01. Orig Submission\Source\eliquis-pkplot-pop-fig3.jpg</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Apixaban" id="1211" section="34073-7">
      

      <SentenceText>Apixaban is a substrate of both CYP3A4 and P-gp.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1212" section="34073-7">
      

      <SentenceText>Inhibitors of CYP3A4 and P-gp increase exposure to apixaban and increase the risk of bleeding.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="30 17" str="increase exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="85 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M3" span="0 20" str="Inhibitors of CYP3A4" type="Precipitant"/>
      <Interaction effect="M2" id="I1" precipitant="M3" trigger="M1" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1213" section="34073-7">
      

      <SentenceText>Inducers of CYP3A4 and P-gp decrease exposure to apixaban and increase the risk of stroke and other thromboembolic events.</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="28 17" str="decrease exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M5" span="0 24" str="Inducers of CYP3A4 and P" type="Precipitant"/>
      <Interaction effect="C54356" id="I2" precipitant="M5" trigger="M4" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1214" section="34073-7">
      

      <SentenceText>•Combined P-gp and strong CYP3A4 inhibitors increase blood levels of apixaban.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="44 21" str="increase blood levels" type="Trigger"/>
      <Mention code="NO MAP" id="M7" span="10 4" str="P-gp" type="Precipitant"/>
      <Interaction effect="C54355" id="I3" precipitant="M7" trigger="M6" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M8" span="26 17" str="CYP3A4 inhibitors" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1215" section="34073-7">
      

      <SentenceText>Reduce ELIQUIS dose or avoid coadministration.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="23 5" str="avoid" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1216" section="34073-7">
      

      <SentenceText>●Simultaneous use of strong dual inducers of CYP3A4 and P-gp reduces blood levels of apixaban: Avoid concomitant use.</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="61 20" str="reduces blood levels" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="95 5" str="Avoid" type="Trigger"/>
      <Mention code="NO MAP" id="M12" span="28 32" str="dual inducers of CYP3A4 and P-gp" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1217" section="34073-7">
      

      <SentenceText>For patients receiving ELIQUIS 5 mg or 10 mg twice daily, the dose of ELIQUIS should be decreased by 50% when coadministered with drugs that are combined P-gp and strong CYP3A4 inhibitors (e.g., ketoconazole, itraconazole, ritonavir).</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="78 19" str="should be decreased" type="Trigger"/>
      <Mention code="NO MAP" id="M14" span="145 42" str="combined P-gp and strong CYP3A4 inhibitors" type="Precipitant"/>
      <Interaction effect="C54355" id="I4" precipitant="M14" trigger="M13" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1218" section="34073-7">
      

      <SentenceText>For patients receiving ELIQUIS at a dose of 2.5 mg twice daily, avoid coadministration with combined P-gp and strong CYP3A4 inhibitors.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="64 5" str="avoid" type="Trigger"/>
      <Mention code="NO MAP" id="M16" span="92 17" str="combined P-gp and" type="Precipitant"/>
      <Interaction id="I5" precipitant="M16" trigger="M15" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M17" span="117 17" str="CYP3A4 inhibitors" type="Precipitant"/>
      <Interaction id="I6" precipitant="M17" trigger="M15" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1219" section="34073-7">
      

      <SentenceText>Although clarithromycin is a combined P-gp and strong CYP3A4 inhibitor, pharmacokinetic data suggest that no dose adjustment is necessary with concomitant administration with ELIQUIS.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="9 14" str="clarithromycin" type="Precipitant"/>
      <Interaction id="I7" precipitant="M18" trigger="M18" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1220" section="34073-7">
      

      <SentenceText>Avoid concomitant use of ELIQUIS with strong dual inducers of CYP3A4 and P-gp (e.g., rifampin, carbamazepine, phenytoin, St. John’s wort) because such drugs will decrease exposure to apixaban.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="0 5" str="Avoid" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="162 17" str="decrease exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="25 7" str="ELIQUIS" type="Precipitant"/>
      <Interaction id="I8" precipitant="M21" trigger="M20" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M22" span="45 13" str="dual inducers" type="Precipitant"/>
      <Interaction id="I9" precipitant="M22" trigger="M19" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M23" span="59 18" str="of CYP3A4 and P-gp" type="Precipitant"/>
      <Interaction effect="C54356" id="I10" precipitant="M23" trigger="M19" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1224" section="34073-7">
      

      <SentenceText>In ARISTOTLE, concomitant use of aspirin increased the bleeding risk on ELIQUIS from 1.8% per year to 3.4% per year and concomitant use of aspirin and warfarin increased the bleeding risk from 2.7% per year to 4.6% per year.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="55 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M25" span="174 13" str="bleeding risk" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="33 7" str="aspirin" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I11" precipitant="M26" trigger="M26" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M27" span="139 7" str="aspirin" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I12" precipitant="M27" trigger="M27" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M28" span="151 8" str="warfarin" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I13" precipitant="M28" trigger="M28" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1225" section="34073-7">
      

      <SentenceText>In this clinical trial, there was limited (2.3%) use of dual antiplatelet therapy with ELIQUIS.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="49 3" str="use" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1230" section="34073-7">
      

      <SentenceText>Coadministration of antiplatelet agents, fibrinolytics, heparin, aspirin, and chronic NSAID use increases the risk of bleeding.</SentenceText>
      

      <Mention code="NO MAP" id="M30" span="96 21" str="increases the risk of" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M31" span="118 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M32" span="20 19" str="antiplatelet agents" type="Precipitant"/>
      <Interaction effect="M30;M31" id="I14" precipitant="M32" trigger="M32" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M33" span="41 13" str="fibrinolytics" type="Precipitant"/>
      <Interaction effect="M30;M31" id="I15" precipitant="M33" trigger="M33" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M34" span="56 7" str="heparin" type="Precipitant"/>
      <Interaction effect="M30;M31" id="I16" precipitant="M34" trigger="M34" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M35" span="65 7" str="aspirin" type="Precipitant"/>
      <Interaction effect="M30;M31" id="I17" precipitant="M35" trigger="M35" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M36" span="86 5" str="NSAID" type="Precipitant"/>
      <Interaction effect="M30;M31" id="I18" precipitant="M36" trigger="M36" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1231" section="34073-7">
      

      <SentenceText>APPRAISE-2, a placebo-controlled clinical trial of apixaban in high-risk, post-acute coronary syndrome patients treated with aspirin or the combination of aspirin and clopidogrel, was terminated early due to a higher rate of bleeding with apixaban compared to placebo.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1232" section="34073-7">
      

      <SentenceText>The rate of ISTH major bleeding was 2.8% per year with apixaban versus 0.6% per year with placebo in patients receiving single antiplatelet therapy and was 5.9% per year with apixaban versus 2.5% per year with placebo in those receiving dual antiplatelet therapy.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="23 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M38" span="12 4" str="ISTH" type="Precipitant"/>
      <Interaction effect="M37" id="I19" precipitant="M38" trigger="M38" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M39" span="127 12" str="antiplatelet" type="Precipitant"/>
      <Interaction effect="M37" id="I20" precipitant="M39" trigger="M39" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M40" span="237 4" str="dual" type="Precipitant"/>
      <Interaction effect="M37" id="I21" precipitant="M40" trigger="M40" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M41" span="242 20" str="antiplatelet therapy" type="Precipitant"/>
      <Interaction effect="M37" id="I22" precipitant="M41" trigger="M41" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1235" section="34090-1">
      

      <SentenceText>Apixaban is a selective inhibitor of FXa.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1236" section="34090-1">
      

      <SentenceText>It does not require antithrombin III for antithrombotic activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1237" section="34090-1">
      

      <SentenceText>Apixaban inhibits free and clot-bound FXa, and prothrombinase activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1238" section="34090-1">
      

      <SentenceText>Apixaban has no direct effect on platelet aggregation, but indirectly inhibits platelet aggregation induced by thrombin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1239" section="34090-1">
      

      <SentenceText>By inhibiting FXa, apixaban decreases thrombin generation and thrombus development.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1240" section="34090-1">
      

      <SentenceText>As a result of FXa inhibition, apixaban prolongs clotting tests such as prothrombin time (PT), INR, and activated partial thromboplastin time (aPTT).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1241" section="34090-1">
      

      <SentenceText>Changes observed in these clotting tests at the expected therapeutic dose, however, are small, subject to a high degree of variability, and not useful in monitoring the anticoagulation effect of apixaban.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1242" section="34090-1">
      

      <SentenceText>The Rotachrom® Heparin chromogenic assay was used to measure the effect of apixaban on FXa activity in humans during the apixaban development program.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1243" section="34090-1">
      

      <SentenceText>A concentration-dependent increase in anti-FXa activity was observed in the dose range tested and was similar in healthy subjects and patients with AF.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1244" section="34090-1">
      

      <SentenceText>This test is not recommended for assessing the anticoagulant effect of apixaban.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1245" section="34090-1">
      

      <SentenceText>There is no clinical experience to reverse bleeding with the use of 4-factor PCC products in individuals who have received ELIQUIS.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1246" section="34090-1">
      

      <SentenceText>Effects of 4-factor PCCs on the pharmacodynamics of apixaban were studied in healthy subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1247" section="34090-1">
      

      <SentenceText>Following administration of apixaban dosed to steady state, endogenous thrombin potential (ETP) returned to pre-apixaban levels 4 hours after the initiation of a 30-minute PCC infusion, compared to 45 hours with placebo.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1248" section="34090-1">
      

      <SentenceText>Mean ETP levels continued to increase and exceeded pre-apixaban levels reaching a maximum (34%-51% increase over pre-apixaban levels) at 21 hours after initiating PCC and remained elevated (21%-27% increase) at the end of the study (69 hours after initiation of PCC).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1249" section="34090-1">
      

      <SentenceText>The clinical relevance of this increase in ETP is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1250" section="34090-1">
      

      <SentenceText>Pharmacodynamic drug interaction studies with aspirin, clopidogrel, aspirin and clopidogrel, prasugrel, enoxaparin, and naproxen were conducted.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1251" section="34090-1">
      

      <SentenceText>No pharmacodynamic interactions were observed with aspirin, clopidogrel, or prasugrel.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1252" section="34090-1">
      

      <SentenceText>A 50% to 60% increase in anti-FXa activity was observed when apixaban was coadministered with enoxaparin or naproxen.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1253" section="34090-1">
      

      <SentenceText>Renal impairment: Anti-FXa activity adjusted for exposure to apixaban was similar across renal function categories.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1254" section="34090-1">
      

      <SentenceText>Hepatic impairment: Changes in anti-FXa activity were similar in patients with mild-to-moderate hepatic impairment and healthy subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1255" section="34090-1">
      

      <SentenceText>However, in patients with moderate hepatic impairment, there is no clear understanding of the impact of this degree of hepatic function impairment on the coagulation cascade and its relationship to efficacy and bleeding.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1256" section="34090-1">
      

      <SentenceText>Patients with severe hepatic impairment were not studied.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1257" section="34090-1">
      

      <SentenceText>Apixaban has no effect on the QTc interval in humans at doses up to 50 mg. Apixaban demonstrates linear pharmacokinetics with dose-proportional increases in exposure for oral doses up to 10 mg.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1258" section="34090-1">
      

      <SentenceText>The absolute bioavailability of apixaban is approximately 50% for doses up to 10 mg of ELIQUIS.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1259" section="34090-1">
      

      <SentenceText>Food does not affect the bioavailability of apixaban.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1260" section="34090-1">
      

      <SentenceText>Maximum concentrations (Cmax) of apixaban appear 3 to 4 hours after oral administration of ELIQUIS.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1261" section="34090-1">
      

      <SentenceText>At doses ≥25 mg, apixaban displays dissolution-limited absorption with decreased bioavailability.</SentenceText>
      

      <Mention code="NO MAP" id="M42" span="71 25" str="decreased bioavailability" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1262" section="34090-1">
      

      <SentenceText>Following oral administration of 10 mg of apixaban as 2 crushed 5 mg tablets suspended in 30 mL of water, exposure was similar to that after oral administration of 2 intact 5 mg tablets.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1263" section="34090-1">
      

      <SentenceText>Following oral administration of 10 mg of apixaban as 2 crushed 5 mg tablets mixed with 30 g of applesauce, the Cmax and AUC were 20% and 16% lower, respectively, when compared to administration of 2 intact 5 mg tablets.</SentenceText>
      

      <Mention code="NO MAP" id="M43" span="96 10" str="applesauce" type="Precipitant"/>
      <Interaction effect="C54355" id="I23" precipitant="M43" trigger="M43" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1264" section="34090-1">
      

      <SentenceText>Following administration of a crushed 5 mg ELIQUIS tablet that was suspended in 60 mL D5W and delivered through a nasogastric tube (NGT), exposure was similar to that seen in other clinical trials involving healthy volunteers receiving a single oral 5 mg tablet dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1265" section="34090-1">
      

      <SentenceText>Plasma protein binding in humans is approximately 87%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1266" section="34090-1">
      

      <SentenceText>The volume of distribution (Vss) is approximately 21 liters.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1267" section="34090-1">
      

      <SentenceText>Approximately 25% of an orally administered apixaban dose is recovered in urine and feces as metabolites.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1268" section="34090-1">
      

      <SentenceText>Apixaban is metabolized mainly via CYP3A4 with minor contributions from CYP1A2, 2C8, 2C9, 2C19, and 2J2.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1269" section="34090-1">
      

      <SentenceText>O-demethylation and hydroxylation at the 3-oxopiperidinyl moiety are the major sites of biotransformation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1270" section="34090-1">
      

      <SentenceText>Unchanged apixaban is the major drug-related component in human plasma; there are no active circulating metabolites.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1271" section="34090-1">
      

      <SentenceText>Apixaban is eliminated in both urine and feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1272" section="34090-1">
      

      <SentenceText>Renal excretion accounts for about 27% of total clearance.</SentenceText>
      

      <Mention code="NO MAP" id="M44" span="0 24" str="Renal excretion accounts" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1273" section="34090-1">
      

      <SentenceText>Biliary and direct intestinal excretion contributes to elimination of apixaban in the feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1274" section="34090-1">
      

      <SentenceText>Apixaban has a total clearance of approximately 3.3 L/hour and an apparent half-life of approximately 12 hours following oral administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1275" section="34090-1">
      

      <SentenceText>Apixaban is a substrate of transport proteins: P-gp and breast cancer resistance protein.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1276" section="34090-1">
      

      <SentenceText>In vitro apixaban studies at concentrations significantly greater than therapeutic exposures, no inhibitory effect on the activity of CYP1A2, CYP2A6, CYP2B6, CYP2C8, CYP2C9, CYP2D6, CYP3A4/5, or CYP2C19, nor induction effect on the activity of CYP1A2, CYP2B6, or CYP3A4/5 were observed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1277" section="34090-1">
      

      <SentenceText>Therefore, apixaban is not expected to alter the metabolic clearance of coadministered drugs that are metabolized by these enzymes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1278" section="34090-1">
      

      <SentenceText>Apixaban is not a significant inhibitor of P-gp.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1279" section="34090-1">
      

      <SentenceText>The effects of coadministered drugs on the pharmacokinetics of apixaban are summarized in Figure 2.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1280" section="34090-1">
      

      <SentenceText>Figure 2: Effect of Coadministered Drugs on the Pharmacokinetics of Apixaban In dedicated studies conducted in healthy subjects, famotidine, atenolol, prasugrel, and enoxaparin did not meaningfully alter the pharmacokinetics of apixaban.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1281" section="34090-1">
      

      <SentenceText>In studies conducted in healthy subjects, apixaban did not meaningfully alter the pharmacokinetics of digoxin, naproxen, atenolol, prasugrel, or acetylsalicylic acid.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1282" section="34090-1">
      

      <SentenceText>Effect of Coadministered Drugs on Pharmacokinetics of Apixaban The effects of level of renal impairment, age, body weight, and level of hepatic impairment on the pharmacokinetics of apixaban are summarized in Figure 3.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1283" section="34090-1">
      

      <SentenceText>Figure 3: Effect of Specific Populations on the Pharmacokinetics of Apixaban * ESRD subjects treated with intermittent hemodialysis; reported PK findings are following single dose of apixaban post hemodialysis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1284" section="34090-1">
      

      <SentenceText>† Results reflect CrCl of 15 mL/min based on regression analysis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1285" section="34090-1">
      

      <SentenceText>‡ Dashed vertical lines illustrate pharmacokinetic changes that were used to inform dosing recommendations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1286" section="34090-1">
      

      <SentenceText>No dose adjustment is recommended for nonvalvular atrial fibrillation patients unless at least 2 of the following patient characteristics are present: age &gt; 79 years, body weight &lt; 61 kg, or serum creatinine &gt; 1.4 mg/dL.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1287" section="34090-1">
      

      <SentenceText>Gender: A study in healthy subjects comparing the pharmacokinetics in males and females showed no meaningful difference.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1288" section="34090-1">
      

      <SentenceText>Race: The results across pharmacokinetic studies in normal subjects showed no differences in apixaban pharmacokinetics among White/Caucasian, Asian, and Black/African American subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1289" section="34090-1">
      

      <SentenceText>No dose adjustment is required based on race/ethnicity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1290" section="34090-1">
      

      <SentenceText>Hemodialysis in ESRD subjects: Systemic exposure to apixaban administered as a single 5 mg dose in ESRD subjects dosed immediately after the completion of a 4-hour hemodialysis session (post-dialysis) is 36% higher when compared to subjects with normal renal function.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1291" section="34090-1">
      

      <SentenceText>The systemic exposure to apixaban administered 2 hours prior to a 4-hour hemodialysis session with a dialysate flow rate of 500 mL/min and a blood flow rate in the range of 350 to 500 mL/min is 17% higher compared to those with normal renal function.</SentenceText>
      

      <Mention code="NO MAP" id="M45" span="198 6" str="higher" type="Trigger"/>
      <Mention code="NO MAP" id="M46" span="241 8" str="function" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1292" section="34090-1">
      

      <SentenceText>The dialysis clearance of apixaban is approximately 18 mL/min.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1293" section="34090-1">
      

      <SentenceText>The systemic exposure of apixaban is 14% lower on dialysis when compared to not on dialysis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1294" section="34090-1">
      

      <SentenceText>Protein binding was similar (92%-94%) between healthy controls and ESRD subjects during the on-dialysis and off-dialysis periods.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1295" section="34090-1">
      

      <SentenceText>O:\E-Submissions\PBO\PRODUCTS\Eliquis 202155\08.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Apixaban" id="1296" section="34090-1">
      

      <SentenceText>Orig Submission\Source\eliquis-pkplot-pop-fig3.jpg</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

