<?xml version="1.0" ?>
<Label drug="Venlafaxine Hydrochloride" setid="4c401522-0108-49cb-8a41-fb5ad4dd0fb1">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS  Serotonergic Drugs (e.g., MAOIs, triptans, SSRIs, other SNRIs, linezolid, lithium, tramadol, or St. John's wort): Potential for serotonin syndrome. Careful patient observation is advised ( 4.2 , 5.2 , 7.3 ).  7.1 Central Nervous System (CNS)-Active Drugs  The risk of using venlafaxine in combination with other CNS-active drugs has not been systematically evaluated. Consequently, caution is advised when venlafaxine hydrochloride extended-release is taken in combination with other CNS-active drugs.  7.2 Monoamine Oxidase Inhibitors  Adverse reactions, some of which were serious, have been reported in patients who have recently been discontinued from an MAOI and started on antidepressants with pharmacological properties similar to venlafaxine hydrochloride extended-release (SNRIs or SSRIs), or who have recently had SNRI or SSRI therapy discontinued prior to initiation of an MAOI [see Dosage and Administration (2.9) , Contraindications (4.2) and Warnings and Precautions (5.2) ] .  7.3 Serotonergic Drugs  Based on the mechanism of action of venlafaxine hydrochloride extended-release and the potential for serotonin syndrome, caution is advised when venlafaxine hydrochloride extended-release is coadministered with other drugs that may affect the serotonergic neurotransmitter systems, such as triptans, SSRIs, other SNRIs, linezolid (an antibiotic which is a reversible non-selective MAOI), lithium, tramadol, or St. John's wort. If concomitant treatment with venlafaxine hydrochloride extended-release and these drugs is clinically warranted, careful observation of the patient is advised, particularly during treatment initiation and dose increases. The concomitant use of venlafaxine hydrochloride extended-release with tryptophan supplements is not recommended [see Dosage and Administration (2.9) , Contraindications (4.2) , and Warnings and Precautions (5.2) ] .  7.4 Drugs that Interfere with Hemostasis (e.g., NSAIDs, Aspirin, and Warfarin)  Serotonin release by platelets plays an important role in hemostasis. The use of psychotropic drugs that interfere with serotonin reuptake is associated with the occurrence of upper gastrointestinal bleeding and concurrent use of an NSAID or aspirin may potentiate this risk of bleeding [see Warnings and Precautions (5.4) ] . Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs and SNRIs are coadministered with warfarin. Patients receiving warfarin therapy should be carefully monitored when venlafaxine hydrochloride extended-release is initiated or discontinued.  7.5 Weight Loss Agents  The safety and efficacy of venlafaxine therapy in combination with weight loss agents, including phentermine, have not been established. Coadministration of venlafaxine hydrochloride extended-release and weight loss agents is not recommended. Venlafaxine hydrochloride extended-release is not indicated for weight loss alone or in combination with other products.  7.6 Effects of Other Drugs on Venlafaxine hydrochloride Extended-Release  Figure 1: Effect of interacting drugs on the pharmacokinetics of venlafaxine and active metabolite O-desmethylvenlafaxine (ODV).  Abbreviations: ODV, O-desmethylvenlafaxine; AUC, area under the curve; C max , peak plasma concentrations; EM's, extensive metabolizers; PM's, poor metabolizers  * No dose adjustment on co-administration with CYP2D6 inhibitors (Fig 3 and Metabolism Section 12.3)  Figure 1  7.7 Effects of Venlafaxine hydrochloride Extended-Release on Other Drugs  Figure 2: Effect of venlafaxine on the pharmacokinetics interacting drugs and their active metabolites.  Abbreviations: AUC, area under the curve; C max , peak plasma concentrations; OH, hydroxyl  * Data for 2-OH desipramine were not plotted to enhance clarity; the fold change and 90% CI for C max and AUC of 2-OH desipramine were 6.6 (5.5, 7.9) and 4.4 (3.8, 5.0), respectively.  Note:  *: Administration of venlafaxine in a stable regimen did not exaggerate the psychomotor and psychometric effects induced by ethanol in these same subjects when they were not receiving venlafaxine.  Figure 2  7.8 Drug-Laboratory Test Interactions  False-positive urine immunoassay screening tests for phencyclidine (PCP) and amphetamine have been reported in patients taking venlafaxine. This is due to lack of specificity of the screening tests. False positive test results may be expected for several days following discontinuation of venlafaxine therapy. Confirmatory tests, such as gas chromatography/mass spectrometry, will distinguish venlafaxine from PCP and amphetamine.</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY  12.1 Mechanism of Action  The exact mechanism of the antidepressant action of venlafaxine in humans is unknown, but is thought to be related to the potentiation of serotonin and norepinephrine in the central nervous system, through inhibition of their reuptake. Non- clinical studies have demonstrated that venlafaxine and its active metabolite, ODV, are potent and selective inhibitors of neuronal serotonin and norepinephrine reuptake and weak inhibitors of dopamine reuptake.  12.2 Pharmacodynamics  Venlafaxine and ODV have no significant affinity for muscarinic-cholinergic, H 1 -histaminergic, or α 1 -adrenergic receptors in vitro . Pharmacologic activity at these receptors is hypothesized to be associated with the various anticholinergic, sedative, and cardiovascular effects seen with other psychotropic drugs. Venlafaxine and ODV do not possess monoamine oxidase (MAO) inhibitory activity.  Cardiac Electrophysiology  The effect of venlafaxine on the QT interval was evaluated in a randomized, double-blind, placebo- and positive-controlled three-period crossover thorough QT study in 54 healthy adult subjects. No significant QT prolongation effect of venlafaxine 450 mg was detected.  12.3 Pharmacokinetics  Steady-state concentrations of venlafaxine and ODV in plasma are attained within 3 days of oral multiple-dose therapy. Venlafaxine and ODV exhibited linear kinetics over the dose range of 75 to 450 mg per day. Mean±SD steady-state plasma clearance of venlafaxine and ODV is 1.3±0.6 and 0.4±0.2 L/h/kg, respectively; apparent elimination half-life is 5±2 and 11±2 hours, respectively; and apparent (steady-state) volume of distribution is 7.5±3.7 and 5.7±1.8 L/kg, respectively. Venlafaxine and ODV are minimally bound at therapeutic concentrations to plasma proteins (27% and 30%, respectively).  Absorption and Distribution  Venlafaxine is well absorbed and extensively metabolized in the liver. ODV is the major active metabolite. On the basis of mass balance studies, at least 92% of a single oral dose of venlafaxine is absorbed. The absolute bioavailability of venlafaxine is approximately 45%.  Administration of venlafaxine hydrochloride extended-release (150 mg once daily) generally resulted in lower C max and later T max values than for venlafaxine hydrochloride immediate-release administered twice daily (Table 16). When equal daily doses of venlafaxine were administered as either an immediate-release tablet or the extended-release capsule, the exposure to both venlafaxine and ODV was similar for the two treatments, and the fluctuation in plasma concentrations was slightly lower with the venlafaxine hydrochloride extended-release capsule. Therefore, venlafaxine hydrochloride extended-release provides a slower rate of absorption, but the same extent of absorption compared with the immediate-release tablet.  Table 16: Comparison of C max and T max Values for Venlafaxine and ODV Following Oral Administration of Venlafaxine hydrochloride Extended-Release and Venlafaxine hydrochloride Immediate-Release)  Venlafaxine  ODV  C max  (ng/mL)  T max  (h)  C max  (ng/mL)  T max  (h)  Venlafaxine hydrochloride Extended-Release (150 mg once daily)  150  5.5  260  9  Venlafaxine hydrochloride Immediate-Release (75 mg twice daily)  225  2  290  3  Food did not affect the bioavailability of venlafaxine or its active metabolite, ODV. Time of administration (AM versus PM) did not affect the pharmacokinetics of venlafaxine and ODV from the 75 mg venlafaxine hydrochloride extended-release capsule.  Venlafaxine is not highly bound to plasma proteins; therefore, administration of venlafaxine hydrochloride extended-release to a patient taking another drug that is highly protein-bound should not cause increased free concentrations of the other drug.  Metabolism and Elimination  Following absorption, venlafaxine undergoes extensive presystemic metabolism in the liver, primarily to ODV, but also to N-desmethylvenlafaxine, N,O-didesmethylvenlafaxine, and other minor metabolites. In vitro studies indicate that the formation of ODV is catalyzed by CYP2D6; this has been confirmed in a clinical study showing that patients with low CYP2D6 levels (poor metabolizers) had increased levels of venlafaxine and reduced levels of ODV compared to people with normal CYP2D6 levels (extensive metabolizers) [see Use in Specific Populations (8.7) ] .  Approximately 87% of a venlafaxine dose is recovered in the urine within 48 hours as unchanged venlafaxine (5%), unconjugated ODV (29%), conjugated ODV (26%), or other minor inactive metabolites (27%). Renal elimination of venlafaxine and its metabolites is thus the primary route of excretion.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5770" section="34073-7">
      

      <SentenceText>Serotonergic Drugs (e.g., MAOIs, triptans, SSRIs, other SNRIs, linezolid, lithium, tramadol, or St. John’s wort): Potential for serotonin syndrome.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5771" section="34073-7">
      

      <SentenceText>Careful patient observation is advised (4.2, 5.2, 7.3).</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="16 11" str="observation" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5772" section="34073-7">
      

      <SentenceText>The risk of using venlafaxine in combination with other CNS-active drugs has not been systematically evaluated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5773" section="34073-7">
      

      <SentenceText>Consequently, caution is advised when venlafaxine hydrochloride extended-release is taken in combination with other CNS-active drugs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5774" section="34073-7">
      

      <SentenceText>Adverse reactions, some of which were serious, have been reported in patients who have recently been discontinued from an MAOI and started on antidepressants with pharmacological properties similar to venlafaxine hydrochloride extended-release (SNRIs or SSRIs), or who have recently had SNRI or SSRI therapy discontinued prior to initiation of an MAOI.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5775" section="34073-7">
      

      <SentenceText>Based on the mechanism of action of venlafaxine hydrochloride extended-release and the potential for serotonin syndrome, caution is advised when venlafaxine hydrochloride extended-release is coadministered with other drugs that may affect the serotonergic neurotransmitter systems, such as triptans, SSRIs, other SNRIs, linezolid (an antibiotic which is a reversible non-selective MAOI), lithium, tramadol, or St. John’s wort.</SentenceText>
      

      <Mention code="NO MAP" id="M2" span="121 7" str="caution" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5776" section="34073-7">
      

      <SentenceText>If concomitant treatment with venlafaxine hydrochloride extended-release and these drugs is clinically warranted, careful observation of the patient is advised, particularly during treatment initiation and dose increases.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5777" section="34073-7">
      

      <SentenceText>The concomitant use of venlafaxine hydrochloride extended-release with tryptophan supplements is not recommended.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="97 15" str="not recommended" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="71 22" str="tryptophan supplements" type="Precipitant"/>
      <Interaction id="I1" precipitant="M4" trigger="M3" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5778" section="34073-7">
      

      <SentenceText>Serotonin release by platelets plays an important role in hemostasis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5779" section="34073-7">
      

      <SentenceText>The use of psychotropic drugs that interfere with serotonin reuptake is associated with the occurrence of upper gastrointestinal bleeding and concurrent use of an NSAID or aspirin may potentiate this risk of bleeding.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="112 25" str="gastrointestinal bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M6" span="184 10" str="potentiate" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M7" span="200 7" str="risk of" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M8" span="208 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M9" span="11 57" str="psychotropic drugs that interfere with serotonin reuptake" type="Precipitant"/>
      <Interaction effect="M5;M6;M7;M8" id="I2" precipitant="M9" trigger="M9" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M10" span="163 5" str="NSAID" type="Precipitant"/>
      <Interaction effect="M5;M6;M7;M8" id="I3" precipitant="M10" trigger="M10" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M11" span="172 7" str="aspirin" type="Precipitant"/>
      <Interaction effect="M5;M6;M7;M8" id="I4" precipitant="M11" trigger="M11" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5780" section="34073-7">
      

      <SentenceText>Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs and SNRIs are coadministered with warfarin.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="51 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M13" span="95 5" str="SNRIs" type="Precipitant"/>
      <Interaction effect="M12" id="I5" precipitant="M13" trigger="M13" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M14" span="125 8" str="warfarin" type="Precipitant"/>
      <Interaction effect="M12" id="I6" precipitant="M14" trigger="M14" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5781" section="34073-7">
      

      <SentenceText>Patients receiving warfarin therapy should be carefully monitored when venlafaxine hydrochloride extended-release is initiated or discontinued.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="56 9" str="monitored" type="Trigger"/>
      <Mention code="NO MAP" id="M16" span="19 8" str="warfarin" type="Precipitant"/>
      <Interaction id="I7" precipitant="M16" trigger="M15" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5782" section="34073-7">
      

      <SentenceText>The safety and efficacy of venlafaxine therapy in combination with weight loss agents, including phentermine, have not been established.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5783" section="34073-7">
      

      <SentenceText>Coadministration of venlafaxine hydrochloride extended-release and weight loss agents is not recommended.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="89 3" str="not" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5784" section="34073-7">
      

      <SentenceText>Venlafaxine hydrochloride extended-release is not indicated for weight loss alone or in combination with other products.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5785" section="34073-7">
      

      <SentenceText>Figure 1: Effect of interacting drugs on the pharmacokinetics of venlafaxine and active metabolite O-desmethylvenlafaxine (ODV).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5786" section="34073-7">
      

      <SentenceText>Abbreviations: ODV, O-desmethylvenlafaxine; AUC, area under the curve; Cmax, peak plasma concentrations; EM's, extensive metabolizers; PM's, poor metabolizers * No dose adjustment on co-administration with CYP2D6 inhibitors (Fig 3 and Metabolism Section 12.3) Figure 1 Figure 2: Effect of venlafaxine on the pharmacokinetics interacting drugs and their active metabolites.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="22 9" str="desmethyl" type="Precipitant"/>
      <Interaction effect="C54355" id="I8" precipitant="M18" trigger="M18" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5787" section="34073-7">
      

      <SentenceText>Abbreviations: AUC, area under the curve; Cmax, peak plasma concentrations; OH, hydroxyl * Data for 2-OH desipramine were not plotted to enhance clarity; the fold change and 90% CI for Cmax and AUC of 2-OH desipramine were 6.6 (5.5, 7.9) and 4.4 (3.8, 5.0), respectively.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="15 3" str="AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="137 15" str="enhance clarity" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="105 11" str="desipramine" type="Precipitant"/>
      <Interaction effect="C54610" id="I9" precipitant="M21" trigger="M20" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54613" id="I10" precipitant="M21" trigger="M20" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M22" span="206 11" str="desipramine" type="Precipitant"/>
      <Interaction effect="C54610" id="I11" precipitant="M22" trigger="M19" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54613" id="I12" precipitant="M22" trigger="M19" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5788" section="34073-7">
      

      <SentenceText>Note: *: Administration of venlafaxine in a stable regimen did not exaggerate the psychomotor and psychometric effects induced by ethanol in these same subjects when they were not receiving venlafaxine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5789" section="34073-7">
      

      <SentenceText>Figure 2 False-positive urine immunoassay screening tests for phencyclidine (PCP) and amphetamine have been reported in patients taking venlafaxine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5790" section="34073-7">
      

      <SentenceText>This is due to lack of specificity of the screening tests.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5791" section="34073-7">
      

      <SentenceText>False positive test results may be expected for several days following discontinuation of venlafaxine therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5792" section="34073-7">
      

      <SentenceText>Confirmatory tests, such as gas chromatography/mass spectrometry, will distinguish venlafaxine from PCP and amphetamine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5793" section="34090-1">
      

      <SentenceText>The exact mechanism of the antidepressant action of venlafaxine in humans is unknown, but is thought to be related to the potentiation of serotonin and norepinephrine in the central nervous system, through inhibition of their reuptake.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5794" section="34090-1">
      

      <SentenceText>Non- clinical studies have demonstrated that venlafaxine and its active metabolite, ODV, are potent and selective inhibitors of neuronal serotonin and norepinephrine reuptake and weak inhibitors of dopamine reuptake.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5795" section="34090-1">
      

      <SentenceText>Venlafaxine and ODV have no significant affinity for muscarinic-cholinergic, H1-histaminergic, or α1-adrenergic receptors in vitro.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5796" section="34090-1">
      

      <SentenceText>Pharmacologic activity at these receptors is hypothesized to be associated with the various anticholinergic, sedative, and cardiovascular effects seen with other psychotropic drugs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5797" section="34090-1">
      

      <SentenceText>Venlafaxine and ODV do not possess monoamine oxidase (MAO) inhibitory activity.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="35 9" str="monoamine" type="Precipitant"/>
      <Interaction id="I13" precipitant="M23" trigger="M23" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5798" section="34090-1">
      

      <SentenceText>Cardiac Electrophysiology The effect of venlafaxine on the QT interval was evaluated in a randomized, double-blind, placebo- and positive-controlled three-period crossover thorough QT study in 54 healthy adult subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5799" section="34090-1">
      

      <SentenceText>No significant QT prolongation effect of venlafaxine 450 mg was detected.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5800" section="34090-1">
      

      <SentenceText>Steady-state concentrations of venlafaxine and ODV in plasma are attained within 3 days of oral multiple-dose therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5801" section="34090-1">
      

      <SentenceText>Venlafaxine and ODV exhibited linear kinetics over the dose range of 75 to 450 mg per day.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5802" section="34090-1">
      

      <SentenceText>Mean±SD steady-state plasma clearance of venlafaxine and ODV is 1.3±0.6 and 0.4±0.2 L/h/kg, respectively; apparent elimination half-life is 5±2 and 11±2 hours, respectively; and apparent (steady-state) volume of distribution is 7.5±3.7 and 5.7±1.8 L/kg, respectively.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5803" section="34090-1">
      

      <SentenceText>Venlafaxine and ODV are minimally bound at therapeutic concentrations to plasma proteins (27% and 30%, respectively).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5804" section="34090-1">
      

      <SentenceText>Absorption and Distribution Venlafaxine is well absorbed and extensively metabolized in the liver.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5805" section="34090-1">
      

      <SentenceText>On the basis of mass balance studies, at least 92% of a single oral dose of venlafaxine is absorbed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5806" section="34090-1">
      

      <SentenceText>The absolute bioavailability of venlafaxine is approximately 45%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5807" section="34090-1">
      

      <SentenceText>Administration of venlafaxine hydrochloride extended-release (150 mg once daily) generally resulted in lower Cmax and later Tmax values than for venlafaxine hydrochloride immediate-release administered twice daily (Table 16).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5808" section="34090-1">
      

      <SentenceText>When equal daily doses of venlafaxine were administered as either an immediate-release tablet or the extended-release capsule, the exposure to both venlafaxine and ODV was similar for the two treatments, and the fluctuation in plasma concentrations was slightly lower with the venlafaxine hydrochloride extended-release capsule.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5809" section="34090-1">
      

      <SentenceText>Therefore, venlafaxine hydrochloride extended-release provides a slower rate of absorption, but the same extent of absorption compared with the immediate-release tablet.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5810" section="34090-1">
      

      <SentenceText>Table 16: Comparison of Cmax and Tmax Values for Venlafaxine and ODV Following Oral Administration of Venlafaxine hydrochloride Extended-Release and Venlafaxine hydrochloride Immediate-Release) Venlafaxine ODV Cmax (ng/mL) Tmax (h) Cmax (ng/mL) Tmax (h) Venlafaxine hydrochloride Extended-Release (150 mg once daily) 150 5.5 260 9 Venlafaxine hydrochloride Immediate-Release (75 mg twice daily) 225 2 290 3 Food did not affect the bioavailability of venlafaxine or its active metabolite, ODV.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5811" section="34090-1">
      

      <SentenceText>Time of administration (AM versus PM) did not affect the pharmacokinetics of venlafaxine and ODV from the 75 mg venlafaxine hydrochloride extended-release capsule.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5812" section="34090-1">
      

      <SentenceText>Venlafaxine is not highly bound to plasma proteins; therefore, administration of venlafaxine hydrochloride extended-release to a patient taking another drug that is highly protein-bound should not cause increased free concentrations of the other drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5813" section="34090-1">
      

      <SentenceText>Metabolism and Elimination Following absorption, venlafaxine undergoes extensive presystemic metabolism in the liver, primarily to ODV, but also to N-desmethylvenlafaxine, N,O-didesmethylvenlafaxine, and other minor metabolites.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5814" section="34090-1">
      

      <SentenceText>In vitro studies indicate that the formation of ODV is catalyzed by CYP2D6; this has been confirmed in a clinical study showing that patients with low CYP2D6 levels (poor metabolizers) had increased levels of venlafaxine and reduced levels of ODV compared to people with normal CYP2D6 levels (extensive metabolizers).</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="189 16" str="increased levels" type="Trigger"/>
      <Mention code="NO MAP" id="M25" span="225 14" str="reduced levels" type="Trigger"/>
      <Mention code="NO MAP" id="M26" span="68 6" str="CYP2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I14" precipitant="M26" trigger="M25" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M27" span="147 10" str="low CYP2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I15" precipitant="M27" trigger="M24" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5815" section="34090-1">
      

      <SentenceText>Approximately 87% of a venlafaxine dose is recovered in the urine within 48 hours as unchanged venlafaxine (5%), unconjugated ODV (29%), conjugated ODV (26%), or other minor inactive metabolites (27%).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Venlafaxine Hydrochloride" id="5816" section="34090-1">
      

      <SentenceText>Renal elimination of venlafaxine and its metabolites is thus the primary route of excretion.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

