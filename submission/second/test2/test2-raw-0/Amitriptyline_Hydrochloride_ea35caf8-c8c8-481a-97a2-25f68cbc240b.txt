Amitriptyline Hydrochloride	1181	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	38
Some patients may experience a large increase in amitriptyline concentration in the presence of topiramate and any adjustments in amitriptyline dose should be made according to the patient's clinical response and not on the basis of plasma levels.
1	0	3	O	Some
2	5	12	O	patients
3	14	16	O	may
4	18	27	O	experience
5	29	29	O	a
6	31	35	O	large
7	37	44	O	increase
8	46	47	O	in
9	49	61	O	XXXXXXXX
10	63	75	O	concentration
11	77	78	O	in
12	80	82	O	the
13	84	91	O	presence
14	93	94	O	of
15	96	105	O	topiramate
16	107	109	O	and
17	111	113	O	any
18	115	125	O	adjustments
19	127	128	O	in
20	130	142	O	XXXXXXXX
21	144	147	O	dose
22	149	154	O	should
23	156	157	O	be
24	159	162	O	made
25	164	172	O	according
26	174	175	O	to
27	177	179	O	the
28	181	189	O	patient's
29	191	198	O	clinical
30	200	207	O	response
31	209	211	O	and
32	213	215	O	not
33	217	218	O	on
34	220	222	O	the
35	224	228	O	basis
36	230	231	O	of
37	233	238	O	plasma
38	240	245	O	levels
NULL

Amitriptyline Hydrochloride	1182	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	56
The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the caucasian population (about 7% to 10% of Caucasians are so called "poor metabolizers"); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African, and other populations are not yet available.
1	0	2	O	The
2	4	14	O	biochemical
3	16	23	O	activity
4	25	26	O	of
5	28	30	O	the
6	32	35	O	drug
7	37	48	O	metabolizing
8	50	56	B-K	isozyme
9	58	67	I-K	cytochrome
10	69	72	I-K	P450
11	74	76	I-K	2D6
12	79	89	I-K	debrisoquin
13	91	101	I-K	hydroxylase
14	104	105	O	is
15	107	113	B-T	reduced
16	115	116	I-T	in
17	118	118	O	a
18	120	125	O	subset
19	127	128	O	of
20	130	132	O	the
21	134	142	O	caucasian
22	144	153	O	population
23	156	160	O	about
24	162	163	O	7%
25	165	166	O	to
26	168	170	O	10%
27	172	173	O	of
28	175	184	O	Caucasians
29	186	188	O	are
30	190	191	O	so
31	193	198	O	called
32	200	204	O	"poor
33	206	218	O	metabolizers"
34	222	229	O	reliable
35	231	239	O	estimates
36	241	242	O	of
37	244	246	O	the
38	248	257	O	prevalence
39	259	260	O	of
40	262	268	O	reduced
41	270	273	O	P450
42	275	277	O	2D6
43	279	285	O	isozyme
44	287	294	O	activity
45	296	300	O	among
46	302	306	O	Asian
47	307	307	O	,
48	309	315	O	African
49	316	316	O	,
50	318	320	O	and
51	322	326	O	other
52	328	338	O	populations
53	340	342	O	are
54	344	346	O	not
55	348	350	O	yet
56	352	360	O	available
K/8:C54358

Amitriptyline Hydrochloride	1183	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	16
Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.
1	0	3	O	Poor
2	5	16	O	metabolizers
3	18	21	O	have
4	23	28	B-T	higher
5	30	33	I-T	than
6	35	42	I-T	expected
7	44	49	I-T	plasma
8	51	64	I-T	concentrations
9	66	67	O	of
10	69	77	B-K	tricyclic
11	79	93	I-K	antidepressants
12	96	99	I-K	TCAs
13	102	105	O	when
14	107	111	O	given
15	113	117	O	usual
16	119	123	O	doses
K/10:C54357

Amitriptyline Hydrochloride	1184	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	32
Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8-fold increase in plasma AUC of the TCA).
1	0	8	O	Depending
2	10	11	O	on
3	13	15	O	the
4	17	24	O	fraction
5	26	27	O	of
6	29	32	B-K	drug
7	34	44	I-K	metabolized
8	46	47	I-K	by
9	49	52	I-K	P450
10	54	56	I-K	2D6
11	57	57	O	,
12	59	61	O	the
13	63	70	B-T	increase
14	72	73	I-T	in
15	75	80	I-T	plasma
16	82	94	I-T	concentration
17	96	98	O	may
18	100	101	O	be
19	103	107	O	small
20	108	108	O	,
21	110	111	O	or
22	113	117	O	quite
23	119	123	O	large
24	126	126	O	8
25	128	131	O	fold
26	133	140	B-T	increase
27	142	143	I-T	in
28	145	150	I-T	plasma
29	152	154	I-T	AUC
30	156	157	O	of
31	159	161	O	the
32	163	165	O	TCA
K/6:C54355

Amitriptyline Hydrochloride	1185	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	18
In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	19	O	certain
5	21	25	O	drugs
6	27	33	B-K	inhibit
7	35	37	I-K	the
8	39	46	B-D	activity
9	48	49	I-D	of
10	51	54	I-D	this
11	56	62	O	isozyme
12	64	66	O	and
13	68	71	O	make
14	73	78	O	normal
15	80	91	O	metabolizers
16	93	100	O	resemble
17	102	105	O	poor
18	107	118	O	metabolizers
K/6:C54356

Amitriptyline Hydrochloride	1186	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	25
An individual who is stable on a given dose of TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.
1	0	1	O	An
2	3	12	O	individual
3	14	16	O	who
4	18	19	O	is
5	21	26	O	stable
6	28	29	O	on
7	31	31	O	a
8	33	37	O	given
9	39	42	O	dose
10	44	45	O	of
11	47	49	O	TCA
12	51	53	O	may
13	55	60	O	become
14	62	69	O	abruptly
15	71	75	O	toxic
16	77	80	O	when
17	82	86	O	given
18	88	90	O	one
19	92	93	O	of
20	95	99	O	these
21	101	110	O	inhibiting
22	112	116	O	drugs
23	118	119	O	as
24	121	131	O	concomitant
25	133	139	O	therapy
NULL

Amitriptyline Hydrochloride	1187	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	40
The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).
1	0	2	O	The
2	4	8	B-K	drugs
3	10	13	I-K	that
4	15	21	I-K	inhibit
5	23	32	I-K	cytochrome
6	34	37	I-K	P450
7	39	41	I-K	2D6
8	43	49	O	include
9	51	54	O	some
10	56	59	O	that
11	61	63	B-T	are
12	65	67	B-T	not
13	69	79	I-T	metabolized
14	81	82	O	by
15	84	86	O	the
16	88	93	O	enzyme
17	96	104	O	quinidine
18	107	116	O	cimetidine
19	119	121	O	and
20	123	126	O	many
21	128	131	O	that
22	133	135	O	are
23	137	146	O	substrates
24	148	150	O	for
25	152	155	O	P450
26	157	159	O	2D6
27	162	165	O	many
28	167	171	O	other
29	173	187	O	antidepressants
30	188	188	O	,
31	190	203	O	phenothiazines
32	204	204	O	,
33	206	208	O	and
34	210	212	O	the
35	214	217	O	Type
36	219	220	O	1C
37	222	236	O	antiarrhythmics
38	238	248	O	propafenone
39	250	252	O	and
40	254	263	O	flecainide
K/2:C54358

Amitriptyline Hydrochloride	1188	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	30
While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.
1	0	4	O	While
2	6	8	O	all
3	10	12	O	the
4	14	22	O	selective
5	24	32	O	serotonin
6	34	41	O	reuptake
7	43	52	O	inhibitors
8	55	59	O	SSRIs
9	61	61	O	,
10	63	65	O	e.g
11	67	67	O	,
12	69	78	O	fluoxetine
13	79	79	O	,
14	81	90	O	sertraline
15	91	91	O	,
16	93	95	O	and
17	97	106	O	paroxetine
18	107	107	O	,
19	109	115	O	inhibit
20	117	120	O	P450
21	122	124	O	2D6
22	125	125	O	,
23	127	130	O	they
24	132	134	O	may
25	136	139	O	vary
26	141	142	O	in
27	144	146	O	the
28	148	153	O	extent
29	155	156	O	of
30	158	167	O	inhibition
NULL

Amitriptyline Hydrochloride	1189	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	25
The extent to which SSRI-TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.
1	0	2	O	The
2	4	9	O	extent
3	11	12	O	to
4	14	18	O	which
5	20	23	O	SSRI
6	25	27	O	TCA
7	29	40	O	interactions
8	42	44	O	may
9	46	49	O	pose
10	51	58	O	clinical
11	60	67	O	problems
12	69	72	O	will
13	74	79	O	depend
14	81	82	O	on
15	84	86	O	the
16	88	93	O	degree
17	95	96	O	of
18	98	107	O	inhibition
19	109	111	O	and
20	113	115	O	the
21	117	132	O	pharmacokinetics
22	134	135	O	of
23	137	139	O	the
24	141	144	O	SSRI
25	146	153	O	involved
NULL

Amitriptyline Hydrochloride	1190	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	25
Nevertheless, caution is indicated in the coadministration of TCAs with any of the SSRIs and also in switching from one class to the other.
1	0	11	O	Nevertheless
2	12	12	O	,
3	14	20	B-T	caution
4	22	23	O	is
5	25	33	O	indicated
6	35	36	O	in
7	38	40	O	the
8	42	57	O	coadministration
9	59	60	O	of
10	62	65	O	TCAs
11	67	70	O	with
12	72	74	O	any
13	76	77	O	of
14	79	81	O	the
15	83	87	O	SSRIs
16	89	91	O	and
17	93	96	O	also
18	98	99	O	in
19	101	109	O	switching
20	111	114	O	from
21	116	118	O	one
22	120	124	O	class
23	126	127	O	to
24	129	131	O	the
25	133	137	O	other
NULL

Amitriptyline Hydrochloride	1191	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	38
Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).
1	0	1	O	Of
2	3	12	O	particular
3	14	23	O	importance
4	24	24	O	,
5	26	35	O	sufficient
6	37	40	O	time
7	42	45	O	must
8	47	52	O	elapse
9	54	59	O	before
10	61	70	O	initiating
11	72	74	O	TCA
12	76	84	O	treatment
13	86	87	O	in
14	89	89	O	a
15	91	97	O	patient
16	99	103	O	being
17	105	113	O	withdrawn
18	115	118	O	from
19	120	129	O	fluoxetine
20	130	130	O	,
21	132	136	O	given
22	138	140	O	the
23	142	145	O	long
24	147	150	O	half
25	152	155	O	life
26	157	158	O	of
27	160	162	O	the
28	164	169	O	parent
29	171	173	O	and
30	175	180	O	active
31	182	191	O	metabolite
32	194	195	O	at
33	197	201	O	least
34	203	203	O	5
35	205	209	O	weeks
36	211	213	O	may
37	215	216	O	be
38	218	226	O	necessary
NULL

Amitriptyline Hydrochloride	1192	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	29
Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	27	B-D	tricyclic
5	29	43	I-D	antidepressants
6	45	48	O	with
7	50	54	O	drugs
8	56	59	B-D	that
9	61	63	B-K	can
10	65	71	B-U	inhibit
11	73	82	I-U	cytochrome
12	84	87	I-U	P450
13	89	91	I-U	2D6
14	93	95	O	may
15	97	103	O	require
16	105	109	B-T	lower
17	111	115	I-T	doses
18	117	120	I-T	than
19	122	128	I-T	usually
20	130	139	I-T	prescribed
21	141	143	O	for
22	145	150	O	either
23	152	154	O	the
24	156	164	B-U	tricyclic
25	166	179	I-U	antidepressant
26	181	182	O	or
27	184	186	O	the
28	188	192	O	other
29	194	197	O	drug
K/9:C54357

Amitriptyline Hydrochloride	1193	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	22
Furthermore, whenever one of these other drugs is withdrawn from cotherapy, an increased dose of tricyclic antidepressant may be required.
1	0	10	O	Furthermore
2	11	11	O	,
3	13	20	O	whenever
4	22	24	O	one
5	26	27	O	of
6	29	33	O	these
7	35	39	O	other
8	41	45	O	drugs
9	47	48	O	is
10	50	58	O	withdrawn
11	60	63	O	from
12	65	73	O	cotherapy
13	74	74	O	,
14	76	77	O	an
15	79	87	O	increased
16	89	92	O	dose
17	94	95	O	of
18	97	105	O	tricyclic
19	107	120	O	antidepressant
20	122	124	O	may
21	126	127	O	be
22	129	136	O	required
NULL

Amitriptyline Hydrochloride	1194	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	27
It is desirable to monitor TCA plasma levels whenever a TCA is going to be coadministered with another drug known to be an inhibitor of P450 2D6.
1	0	1	O	It
2	3	4	O	is
3	6	14	O	desirable
4	16	17	O	to
5	19	25	B-T	monitor
6	27	29	O	TCA
7	31	36	O	plasma
8	38	43	O	levels
9	45	52	O	whenever
10	54	54	O	a
11	56	58	O	TCA
12	60	61	O	is
13	63	67	O	going
14	69	70	O	to
15	72	73	O	be
16	75	88	O	coadministered
17	90	93	O	with
18	95	101	O	another
19	103	106	O	drug
20	108	112	O	known
21	114	115	O	to
22	117	118	O	be
23	120	121	O	an
24	123	131	O	inhibitor
25	133	134	O	of
26	136	139	O	P450
27	141	143	B-U	2D6
NULL

Amitriptyline Hydrochloride	1195	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	16
Guanethidine or similarly acting compounds; thyroid medication; alcohol, barbiturates and other CNS depressants; and disulfiram.
1	0	11	O	Guanethidine
2	13	14	O	or
3	16	24	O	similarly
4	26	31	O	acting
5	33	41	O	compounds
6	44	50	O	thyroid
7	52	61	O	medication
8	64	70	O	alcohol
9	71	71	O	,
10	73	84	O	barbiturates
11	86	88	O	and
12	90	94	O	other
13	96	98	O	CNS
14	100	110	B-D	depressants
15	113	115	O	and
16	117	126	O	disulfiram
NULL

Amitriptyline Hydrochloride	1196	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	27
When amitriptyline hydrochloride is given with anticholinergic agents or sympathomimetic drugs, including epinephrine combined with local anesthetics, close supervision and careful adjustment of dosages are required.
1	0	3	O	When
2	5	31	O	XXXXXXXX
3	33	34	O	is
4	36	40	O	given
5	42	45	O	with
6	47	61	B-U	anticholinergic
7	63	68	I-U	agents
8	70	71	O	or
9	73	87	B-U	sympathomimetic
10	89	93	I-U	drugs
11	94	94	O	,
12	96	104	O	including
13	106	116	B-U	epinephrine
14	118	125	O	combined
15	127	130	O	with
16	132	136	B-U	local
17	138	148	I-U	anesthetics
18	149	149	O	,
19	151	155	O	close
20	157	167	B-T	supervision
21	169	171	O	and
22	173	179	B-T	careful
23	181	190	I-T	adjustment
24	192	193	I-T	of
25	195	201	I-T	dosages
26	203	205	O	are
27	207	214	O	required
NULL

Amitriptyline Hydrochloride	1197	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	20
Hyperpyrexia has been reported when amitriptyline hydrochloride is administered with anticholinergic agents or with neuroleptic drugs, particularly during hot weather.
1	0	11	B-E	Hyperpyrexia
2	13	15	O	has
3	17	20	O	been
4	22	29	O	reported
5	31	34	O	when
6	36	62	O	XXXXXXXX
7	64	65	O	is
8	67	78	O	administered
9	80	83	O	with
10	85	99	B-D	anticholinergic
11	101	106	I-D	agents
12	108	109	O	or
13	111	114	O	with
14	116	126	B-D	neuroleptic
15	128	132	I-D	drugs
16	133	133	O	,
17	135	146	O	particularly
18	148	153	O	during
19	155	157	O	hot
20	159	165	O	weather
D/10:1:1 D/14:1:1

Amitriptyline Hydrochloride	1198	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	15
Paralytic ileus may occur in patients taking tricyclic antidepressants in combination with anticholinergic type drugs.
1	0	8	B-E	Paralytic
2	10	14	B-E	ileus
3	16	18	O	may
4	20	24	O	occur
5	26	27	O	in
6	29	36	O	patients
7	38	43	O	taking
8	45	53	B-D	tricyclic
9	55	69	I-D	antidepressants
10	71	72	O	in
11	74	84	O	combination
12	86	89	O	with
13	91	105	B-D	anticholinergic
14	107	110	I-D	type
15	112	116	I-D	drugs
D/8:1:1 D/8:2:1 D/13:1:1 D/13:2:1

Amitriptyline Hydrochloride	1199	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	23
Cimetidine is reported to reduce hepatic metabolism of certain tricyclic antidepressants, thereby delaying elimination and increasing steady-state concentrations of these drugs.
1	0	9	B-K	Cimetidine
2	11	12	O	is
3	14	21	O	reported
4	23	24	O	to
5	26	31	B-T	reduce
6	33	39	O	hepatic
7	41	50	B-T	metabolism
8	52	53	O	of
9	55	61	O	certain
10	63	71	B-K	tricyclic
11	73	87	I-K	antidepressants
12	88	88	O	,
13	90	96	O	thereby
14	98	105	B-T	delaying
15	107	117	I-T	elimination
16	119	121	O	and
17	123	132	B-T	increasing
18	134	139	I-T	steady
19	141	145	I-T	state
20	147	160	I-T	concentrations
21	162	163	O	of
22	165	169	O	these
23	171	175	O	drugs
K/1:C54357 K/10:C54357

Amitriptyline Hydrochloride	1200	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	15
Clinically significant effects have been reported with the tricyclic antidepressants when used concomitantly with cimetidine.
1	0	9	O	Clinically
2	11	21	O	significant
3	23	29	B-E	effects
4	31	34	O	have
5	36	39	O	been
6	41	48	O	reported
7	50	53	O	with
8	55	57	O	the
9	59	67	B-D	tricyclic
10	69	83	I-D	antidepressants
11	85	88	O	when
12	90	93	O	used
13	95	107	O	concomitantly
14	109	112	O	with
15	114	123	B-D	cimetidine
D/9:3:1 D/15:3:1

Amitriptyline Hydrochloride	1201	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	32
Increases in plasma levels of tricyclic antidepressants, and in the frequency and severity of side effects, particularly anticholinergic, have been reported when cimetidine was added to the drug regimen.
1	0	8	B-T	Increases
2	10	11	I-T	in
3	13	18	I-T	plasma
4	20	25	I-T	levels
5	27	28	O	of
6	30	38	B-D	tricyclic
7	40	54	I-D	antidepressants
8	55	55	O	,
9	57	59	O	and
10	61	62	O	in
11	64	66	O	the
12	68	76	O	frequency
13	78	80	O	and
14	82	89	O	severity
15	91	92	O	of
16	94	97	O	side
17	99	105	O	effects
18	106	106	O	,
19	108	119	O	particularly
20	121	135	B-D	anticholinergic
21	136	136	O	,
22	138	141	O	have
23	143	146	O	been
24	148	155	O	reported
25	157	160	O	when
26	162	171	B-D	cimetidine
27	173	175	O	was
28	177	181	O	added
29	183	184	O	to
30	186	188	O	the
31	190	193	O	drug
32	195	201	O	regimen
NULL

Amitriptyline Hydrochloride	1202	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	22
Discontinuation of cimetidine in well controlled patients receiving tricyclic antidepressants and cimetidine may decrease the plasma levels and efficacy of the antidepressants.
1	0	14	O	Discontinuation
2	16	17	O	of
3	19	28	B-K	cimetidine
4	30	31	O	in
5	33	36	O	well
6	38	47	O	controlled
7	49	56	O	patients
8	58	66	O	receiving
9	68	76	B-K	tricyclic
10	78	92	I-K	antidepressants
11	94	96	O	and
12	98	107	B-K	cimetidine
13	109	111	O	may
14	113	120	B-T	decrease
15	122	124	I-T	the
16	126	131	I-T	plasma
17	133	138	I-T	levels
18	140	142	O	and
19	144	151	O	efficacy
20	153	154	O	of
21	156	158	O	the
22	160	174	B-K	antidepressants
K/3:C54357 K/9:C54357 K/12:C54357 K/22:C54357

Amitriptyline Hydrochloride	1203	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	11
Caution is advised if patients receive large doses of ethchlorvynol concurrently.
1	0	6	B-T	Caution
2	8	9	O	is
3	11	17	O	advised
4	19	20	O	if
5	22	29	O	patients
6	31	37	O	receive
7	39	43	O	large
8	45	49	O	doses
9	51	52	O	of
10	54	66	B-U	ethchlorvynol
11	68	79	O	concurrently
NULL

Amitriptyline Hydrochloride	1204	34073-7	ea35caf8-c8c8-481a-97a2-25f68cbc240b	23
Transient delirium has been reported in patients who were treated with one gram of ethchlorvynol and 75 mg to 150 mg of amitriptyline hydrochloride.
1	0	8	O	Transient
2	10	17	O	delirium
3	19	21	O	has
4	23	26	O	been
5	28	35	O	reported
6	37	38	O	in
7	40	47	O	patients
8	49	51	O	who
9	53	56	O	were
10	58	64	O	treated
11	66	69	O	with
12	71	73	O	one
13	75	78	O	gram
14	80	81	O	of
15	83	95	O	ethchlorvynol
16	97	99	O	and
17	101	102	O	75
18	104	105	O	mg
19	107	108	O	to
20	110	112	O	150
21	114	115	O	mg
22	117	118	O	of
23	120	146	O	XXXXXXXX
NULL

Amitriptyline Hydrochloride	1205	34090-1	ea35caf8-c8c8-481a-97a2-25f68cbc240b	7
Amitriptyline hydrochloride is an antidepressant with sedative effects.
1	0	26	O	XXXXXXXX
2	28	29	O	is
3	31	32	O	an
4	34	47	O	antidepressant
5	49	52	O	with
6	54	61	O	sedative
7	63	69	B-E	effects
NULL

Amitriptyline Hydrochloride	1206	34090-1	ea35caf8-c8c8-481a-97a2-25f68cbc240b	9
Its mechanism of action in man is not known.
1	0	2	O	Its
2	4	12	O	mechanism
3	14	15	O	of
4	17	22	O	action
5	24	25	O	in
6	27	29	O	man
7	31	32	O	is
8	34	36	O	not
9	38	42	O	known
NULL

Amitriptyline Hydrochloride	1207	34090-1	ea35caf8-c8c8-481a-97a2-25f68cbc240b	20
It is not a monoamine oxidase inhibitor and it does not act primarily by stimulation of the central nervous system.
1	0	1	O	It
2	3	4	O	is
3	6	8	O	not
4	10	10	O	a
5	12	20	O	monoamine
6	22	28	O	oxidase
7	30	38	O	inhibitor
8	40	42	O	and
9	44	45	O	it
10	47	50	O	does
11	52	54	O	not
12	56	58	O	act
13	60	68	O	primarily
14	70	71	O	by
15	73	83	O	stimulation
16	85	86	O	of
17	88	90	O	the
18	92	98	O	central
19	100	106	O	nervous
20	108	113	O	system
NULL

Amitriptyline Hydrochloride	1208	34090-1	ea35caf8-c8c8-481a-97a2-25f68cbc240b	18
Amitriptyline inhibits the membrane pump mechanism responsible for uptake of norepinephrine and serotonin in adrenergic and serotonergic neurons.
1	0	12	O	XXXXXXXX
2	14	21	O	inhibits
3	23	25	O	the
4	27	34	O	membrane
5	36	39	O	pump
6	41	49	O	mechanism
7	51	61	O	responsible
8	63	65	O	for
9	67	72	O	uptake
10	74	75	O	of
11	77	90	O	norepinephrine
12	92	94	O	and
13	96	104	O	serotonin
14	106	107	O	in
15	109	118	O	adrenergic
16	120	122	O	and
17	124	135	O	serotonergic
18	137	143	O	neurons
NULL

Amitriptyline Hydrochloride	1209	34090-1	ea35caf8-c8c8-481a-97a2-25f68cbc240b	22
Pharmacologically this action may potentiate or prolong neuronal activity since reuptake of these biogenic amines is important physiologically in terminating transmitting activity.
1	0	16	O	Pharmacologically
2	18	21	O	this
3	23	28	O	action
4	30	32	O	may
5	34	43	O	potentiate
6	45	46	O	or
7	48	54	O	prolong
8	56	63	O	neuronal
9	65	72	O	activity
10	74	78	O	since
11	80	87	O	reuptake
12	89	90	O	of
13	92	96	O	these
14	98	105	O	biogenic
15	107	112	O	amines
16	114	115	O	is
17	117	125	O	important
18	127	141	O	physiologically
19	143	144	O	in
20	146	156	O	terminating
21	158	169	O	transmitting
22	171	178	O	activity
NULL

Amitriptyline Hydrochloride	1210	34090-1	ea35caf8-c8c8-481a-97a2-25f68cbc240b	21
This interference with reuptake of norepinephrine and/or serotonin is believed by some to underlie the antidepressant activity of amitriptyline.
1	0	3	O	This
2	5	16	O	interference
3	18	21	O	with
4	23	30	O	reuptake
5	32	33	O	of
6	35	48	O	norepinephrine
7	50	52	O	and
8	53	53	O	/
9	54	55	O	or
10	57	65	O	serotonin
11	67	68	O	is
12	70	77	O	believed
13	79	80	O	by
14	82	85	O	some
15	87	88	O	to
16	90	97	O	underlie
17	99	101	O	the
18	103	116	O	antidepressant
19	118	125	O	activity
20	127	128	O	of
21	130	142	O	XXXXXXXX
NULL

