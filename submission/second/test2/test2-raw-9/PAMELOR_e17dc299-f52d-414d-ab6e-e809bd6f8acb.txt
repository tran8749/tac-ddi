PAMELOR	6132	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	19
Administration of reserpine during therapy with a tricyclic antidepressant has been shown to produce a "stimulating" effect in some depressed patients.
1	0	13	O	Administration
2	15	16	O	of
3	18	26	O	reserpine
4	28	33	O	during
5	35	41	O	therapy
6	43	58	O	withGGGGGGGGGGGG
7	60	73	O	antidepressant
8	75	77	O	has
9	79	82	O	been
10	84	88	O	shown
11	90	91	O	to
12	93	99	O	produce
13	101	101	O	a
14	103	115	O	"stimulating"
15	117	122	O	effect
16	124	125	O	in
17	127	130	O	some
18	132	140	O	depressed
19	142	149	O	patients
NULL

PAMELOR	6133	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	21
Close supervision and careful adjustment of the dosage are required when Pamelor is used with other anticholinergic drugs and sympathomimetic drugs.
1	0	4	O	Close
2	6	16	B-T	supervision
3	18	20	O	and
4	22	28	O	careful
5	30	39	B-T	adjustment
6	41	42	O	of
7	44	46	O	the
8	48	53	B-T	dosage
9	55	57	O	are
10	59	66	O	required
11	68	71	O	when
12	73	79	O	XXXXXXXX
13	81	82	O	is
14	84	87	O	used
15	89	92	O	with
16	94	98	O	other
17	100	114	B-U	anticholinergic
18	116	120	I-U	drugs
19	122	124	O	and
20	126	140	B-U	sympathomimetic
21	142	146	I-U	drugs
NULL

PAMELOR	6134	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	18
Concurrent administration of cimetidine and tricyclic antidepressants can produce clinically significant increases in the plasma concentrations of the tricyclic antidepressant.
1	0	9	O	Concurrent
2	11	24	O	administration
3	26	27	O	of
4	29	38	B-K	cimetidine
5	40	52	O	aGGGGGGGGGGGG
6	54	68	B-K	antidepressants
7	70	72	O	can
8	74	80	O	produce
9	82	91	O	clinically
10	93	103	O	significant
11	105	113	B-T	increases
12	115	116	I-T	in
13	118	120	I-T	the
14	122	127	I-T	plasma
15	129	142	I-T	concentrations
16	144	145	O	of
17	147	159	O	tGGGGGGGGGGGG
18	161	174	B-K	antidepressant
K/4:C54357 K/6:C54357 K/18:C54357

PAMELOR	6135	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	13
The patient should be informed that the response to alcohol may be exaggerated.
1	0	2	O	The
2	4	10	O	patient
3	12	17	O	should
4	19	20	O	be
5	22	29	O	informed
6	31	34	O	that
7	36	38	O	the
8	40	47	O	response
9	49	50	O	to
10	52	58	B-D	alcohol
11	60	62	O	may
12	64	65	O	be
13	67	77	O	exaggerated
NULL

PAMELOR	6136	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	31
A case of significant hypoglycemia has been reported in a type II diabetic patient maintained on chlorpropamide (250 mg/day), after the addition of nortriptyline (125 mg/day).
1	0	0	O	A
2	2	5	O	case
3	7	8	O	of
4	10	20	O	significant
5	22	33	B-E	hypoglycemia
6	35	37	O	has
7	39	42	O	been
8	44	51	O	reported
9	53	54	O	in
10	56	56	O	a
11	58	61	O	type
12	63	64	O	II
13	66	73	O	diabetic
14	75	81	O	patient
15	83	92	O	maintained
16	94	95	O	on
17	97	110	B-D	chlorpropamide
18	113	115	O	250
19	117	118	O	mg
20	119	119	O	/
21	120	122	O	day
22	124	124	O	,
23	126	130	O	after
24	132	134	O	the
25	136	143	O	addition
26	145	146	O	of
27	148	160	O	XXXXXXXX
28	163	165	O	125
29	167	168	O	mg
30	169	169	O	/
31	170	172	O	day
D/17:5:1

PAMELOR	6137	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	60
Drugs Metabolized by P450 2D6 - The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the Caucasian population (about 7% to 10% of Caucasians are so called "poor metabolizers"); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African and other populations are not yet available.
1	0	4	O	Drugs
2	6	16	B-K	Metabolized
3	18	19	I-K	by
4	21	24	I-K	P450
5	26	28	I-K	2D6
6	32	34	O	The
7	36	46	O	biochemical
8	48	55	O	activity
9	57	58	O	of
10	60	62	O	the
11	64	67	O	drug
12	69	80	B-K	metabolizing
13	82	88	I-K	isozyme
14	90	99	I-K	cytochrome
15	101	104	I-K	P450
16	106	108	I-K	2D6
17	111	121	I-K	debrisoquin
18	123	133	I-K	hydroxylase
19	136	137	O	is
20	139	145	O	reduced
21	147	148	O	in
22	150	150	O	a
23	152	157	O	subset
24	159	160	O	of
25	162	164	O	the
26	166	174	O	Caucasian
27	176	185	O	population
28	188	192	O	about
29	194	195	O	7%
30	197	198	O	to
31	200	202	O	10%
32	204	205	O	of
33	207	216	O	Caucasians
34	218	220	O	are
35	222	223	O	so
36	225	230	O	called
37	232	236	O	"poor
38	238	250	O	metabolizers"
39	254	261	O	reliable
40	263	271	O	estimates
41	273	274	O	of
42	276	278	O	the
43	280	289	O	prevalence
44	291	292	O	of
45	294	300	O	reduced
46	302	305	O	P450
47	307	309	O	2D6
48	311	317	O	isozyme
49	319	326	O	activity
50	328	332	O	among
51	334	338	O	Asian
52	339	339	O	,
53	341	347	O	African
54	349	351	O	and
55	353	357	O	other
56	359	369	O	populations
57	371	373	O	are
58	375	377	O	not
59	379	381	O	yet
60	383	391	O	available
K/2:C54358 K/12:C54356

PAMELOR	6138	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	15
Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.
1	0	3	O	Poor
2	5	16	O	metabolizers
3	18	21	O	have
4	23	28	B-T	higher
5	30	33	I-T	than
6	35	42	I-T	expected
7	44	49	I-T	plasma
8	51	64	I-T	concentrations
9	66	77	O	GGGGGGGG
10	79	93	B-K	antidepressants
11	96	99	I-K	TCAs
12	102	105	O	when
13	107	111	O	given
14	113	117	O	usual
15	119	123	O	doses
K/10:C54357

PAMELOR	6139	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	32
Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8 fold increase in plasma AUC of the TCA).
1	0	8	O	Depending
2	10	11	O	on
3	13	15	O	the
4	17	24	O	fraction
5	26	27	O	of
6	29	32	O	drug
7	34	44	O	metabolized
8	46	47	O	by
9	49	52	O	P450
10	54	56	O	2D6
11	57	57	O	,
12	59	61	O	the
13	63	70	O	increase
14	72	73	O	in
15	75	80	O	plasma
16	82	94	O	concentration
17	96	98	O	may
18	100	101	O	be
19	103	107	O	small
20	108	108	O	,
21	110	111	O	or
22	113	117	O	quite
23	119	123	O	large
24	126	126	O	8
25	128	131	O	fold
26	133	140	B-T	increase
27	142	143	I-T	in
28	145	150	I-T	plasma
29	152	154	I-T	AUC
30	156	157	O	of
31	159	161	O	the
32	163	165	O	TCA
NULL

PAMELOR	6140	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	18
In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	19	O	certain
5	21	25	B-U	drugs
6	27	33	I-U	inhibit
7	35	37	I-U	the
8	39	46	I-U	activity
9	48	49	I-U	of
10	51	54	I-U	this
11	56	62	O	isozyme
12	64	66	O	and
13	68	71	O	make
14	73	78	O	normal
15	80	91	O	metabolizers
16	93	100	O	resemble
17	102	105	O	poor
18	107	118	O	metabolizers
NULL

PAMELOR	6141	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	25
An individual who is stable on a given dose of TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.
1	0	1	O	An
2	3	12	O	individual
3	14	16	O	who
4	18	19	O	is
5	21	26	O	stable
6	28	29	O	on
7	31	31	O	a
8	33	37	O	given
9	39	42	O	dose
10	44	45	O	of
11	47	49	O	TCA
12	51	53	O	may
13	55	60	O	become
14	62	69	O	abruptly
15	71	75	O	toxic
16	77	80	O	when
17	82	86	O	given
18	88	90	O	one
19	92	93	O	of
20	95	99	O	these
21	101	110	O	inhibiting
22	112	116	O	drugs
23	118	119	O	as
24	121	131	O	concomitant
25	133	139	O	therapy
NULL

PAMELOR	6142	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	40
The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).
1	0	2	O	The
2	4	8	B-K	drugs
3	10	13	I-K	that
4	15	21	I-K	inhibit
5	23	32	I-K	cytochrome
6	34	37	I-K	P450
7	39	41	I-K	2D6
8	43	49	O	include
9	51	54	O	some
10	56	59	O	that
11	61	63	O	are
12	65	67	O	not
13	69	79	O	metabolized
14	81	82	O	by
15	84	86	O	the
16	88	93	O	enzyme
17	96	104	O	quinidine
18	107	116	O	cimetidine
19	119	121	O	and
20	123	126	O	many
21	128	131	O	that
22	133	135	O	are
23	137	146	O	substrates
24	148	150	O	for
25	152	155	O	P450
26	157	159	O	2D6
27	162	165	O	many
28	167	171	O	other
29	173	187	O	antidepressants
30	188	188	O	,
31	190	203	O	phenothiazines
32	204	204	O	,
33	206	208	O	and
34	210	212	O	the
35	214	217	O	Type
36	219	220	O	1C
37	222	236	O	antiarrhythmics
38	238	248	O	propafenone
39	250	252	O	and
40	254	263	B-D	flecainide
K/2:C54357

PAMELOR	6143	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	30
While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.
1	0	4	O	While
2	6	8	O	all
3	10	12	O	the
4	14	22	O	selective
5	24	32	O	serotonin
6	34	41	O	reuptake
7	43	52	O	inhibitors
8	55	59	O	SSRIs
9	61	61	O	,
10	63	65	O	e.g
11	67	67	O	,
12	69	78	O	fluoxetine
13	79	79	O	,
14	81	90	B-D	sertraline
15	91	91	O	,
16	93	95	O	and
17	97	106	O	paroxetine
18	107	107	O	,
19	109	115	O	inhibit
20	117	120	O	P450
21	122	124	O	2D6
22	125	125	O	,
23	127	130	O	they
24	132	134	O	may
25	136	139	O	vary
26	141	142	O	in
27	144	146	O	the
28	148	153	O	extent
29	155	156	O	of
30	158	167	O	inhibition
NULL

PAMELOR	6144	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	25
The extent to which SSRI TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.
1	0	2	O	The
2	4	9	O	extent
3	11	12	O	to
4	14	18	O	which
5	20	23	O	SSRI
6	25	27	O	TCA
7	29	40	O	interactions
8	42	44	O	may
9	46	49	O	pose
10	51	58	O	clinical
11	60	67	O	problems
12	69	72	O	will
13	74	79	O	depend
14	81	82	B-E	on
15	84	86	I-E	the
16	88	93	O	degree
17	95	96	O	of
18	98	107	O	inhibition
19	109	111	O	and
20	113	115	O	the
21	117	132	O	pharmacokinetics
22	134	135	O	of
23	137	139	O	the
24	141	144	O	SSRI
25	146	153	O	involved
NULL

PAMELOR	6145	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	26
Nevertheless, caution is indicated in the co-administration of TCAs with any of the SSRIs and also in switching from one class to the other.
1	0	11	O	Nevertheless
2	12	12	O	,
3	14	20	B-T	caution
4	22	23	I-T	is
5	25	33	I-T	indicated
6	35	36	O	in
7	38	40	O	the
8	42	43	O	co
9	45	58	O	administration
10	60	61	O	of
11	63	66	O	TCAs
12	68	71	O	with
13	73	75	O	any
14	77	78	O	of
15	80	82	O	the
16	84	88	B-U	SSRIs
17	90	92	O	and
18	94	97	O	also
19	99	100	O	in
20	102	110	O	switching
21	112	115	O	from
22	117	119	O	one
23	121	125	B-U	class
24	127	128	I-U	to
25	130	132	I-U	the
26	134	138	I-U	other
NULL

PAMELOR	6146	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	38
Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).
1	0	1	O	Of
2	3	12	O	particular
3	14	23	O	importance
4	24	24	O	,
5	26	35	O	sufficient
6	37	40	O	time
7	42	45	O	must
8	47	52	O	elapse
9	54	59	O	before
10	61	70	O	initiating
11	72	74	O	TCA
12	76	84	O	treatment
13	86	87	O	in
14	89	89	O	a
15	91	97	O	patient
16	99	103	O	being
17	105	113	O	withdrawn
18	115	118	O	from
19	120	129	O	fluoxetine
20	130	130	O	,
21	132	136	O	given
22	138	140	O	the
23	142	145	O	long
24	147	150	O	half
25	152	155	O	life
26	157	158	O	of
27	160	162	O	the
28	164	169	O	parent
29	171	173	O	and
30	175	180	O	active
31	182	191	O	metabolite
32	194	195	O	at
33	197	201	O	least
34	203	203	O	5
35	205	209	O	weeks
36	211	213	O	may
37	215	216	O	be
38	218	226	O	necessary
NULL

PAMELOR	6147	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	27
Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	27	O	GGGGGGGG
4	29	43	O	antidepressants
5	45	48	O	with
6	50	54	O	drugs
7	56	59	O	that
8	61	63	O	can
9	65	71	O	inhibit
10	73	82	B-K	cytochrome
11	84	87	I-K	P450
12	89	91	I-K	2D6
13	93	95	O	may
14	97	103	O	require
15	105	109	B-T	lower
16	111	115	I-T	doses
17	117	120	I-T	than
18	122	128	I-T	usually
19	130	139	O	prescribed
20	141	143	O	for
21	145	150	O	either
22	152	164	O	tGGGGGGGGGGGG
23	166	179	O	antidepressant
24	181	182	O	or
25	184	186	O	the
26	188	192	O	other
27	194	197	O	drug
K/10:C54357

PAMELOR	6148	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	22
Furthermore, whenever one of these other drugs is withdrawn from co-therapy, an increased dose of tricyclic antidepressant may be required.
1	0	10	O	Furthermore
2	11	11	O	,
3	13	20	O	whenever
4	22	24	O	one
5	26	27	O	of
6	29	33	O	these
7	35	39	O	other
8	41	45	O	drugs
9	47	48	O	is
10	50	58	O	withdrawn
11	60	63	O	from
12	65	66	O	co
13	68	74	O	therapy
14	75	75	O	,
15	77	78	O	an
16	80	88	O	increased
17	90	93	O	dose
18	95	106	O	GGGGGGGG
19	108	121	O	antidepressant
20	123	125	O	may
21	127	128	O	be
22	130	137	O	required
NULL

PAMELOR	6149	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	28
It is desirable to monitor TCA plasma levels whenever a TCA is going to be co-administered with another drug known to be an inhibitor of P450 2D6.
1	0	1	O	It
2	3	4	O	is
3	6	14	O	desirable
4	16	17	O	to
5	19	25	O	monitor
6	27	29	O	TCA
7	31	36	O	plasma
8	38	43	O	levels
9	45	52	O	whenever
10	54	54	O	a
11	56	58	O	TCA
12	60	61	O	is
13	63	67	O	going
14	69	70	O	to
15	72	73	O	be
16	75	76	O	co
17	78	89	O	administered
18	91	94	O	with
19	96	102	O	another
20	104	107	O	drug
21	109	113	O	known
22	115	116	O	to
23	118	119	O	be
24	121	122	O	an
25	124	132	O	inhibitor
26	134	135	O	of
27	137	140	B-U	P450
28	142	144	I-U	2D6
NULL

PAMELOR	6150	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	4
Monoamine Oxidase Inhibitors (MAOIs)
1	0	8	O	Monoamine
2	10	16	O	Oxidase
3	18	27	O	Inhibitors
4	30	34	O	MAOIs
NULL

PAMELOR	6151	34073-7	e17dc299-f52d-414d-ab6e-e809bd6f8acb	2
Serotonergic Drugs
1	0	11	O	Serotonergic
2	13	17	O	Drugs
NULL

PAMELOR	6152	34090-1	e17dc299-f52d-414d-ab6e-e809bd6f8acb	11
The mechanism of mood elevation by tricyclic antidepressants is at present unknown.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	20	O	mood
5	22	30	O	elevation
6	32	43	O	GGGGGGGG
7	45	59	O	antidepressants
8	61	62	O	is
9	64	65	O	at
10	67	73	O	present
11	75	81	O	unknown
NULL

PAMELOR	6153	34090-1	e17dc299-f52d-414d-ab6e-e809bd6f8acb	7
Pamelor is not a monoamine oxidase inhibitor.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	13	O	not
4	15	15	O	a
5	17	25	O	monoamine
6	27	33	O	oxidase
7	35	43	O	inhibitor
NULL

PAMELOR	6154	34090-1	e17dc299-f52d-414d-ab6e-e809bd6f8acb	16
It inhibits the activity of such diverse agents as histamine, 5-hydroxytryptamine, and acetylcholine.
1	0	1	O	It
2	3	10	O	inhibits
3	12	14	O	the
4	16	23	O	activity
5	25	26	O	of
6	28	31	O	such
7	33	39	O	diverse
8	41	46	O	agents
9	48	49	O	as
10	51	59	O	histamine
11	60	60	O	,
12	62	62	O	5
13	64	80	O	hydroxytryptamine
14	81	81	O	,
15	83	85	O	and
16	87	99	O	acetylcholine
NULL

PAMELOR	6155	34090-1	e17dc299-f52d-414d-ab6e-e809bd6f8acb	14
It increases the pressor effect of norepinephrine but blocks the pressor response of phenethylamine.
1	0	1	O	It
2	3	11	B-E	increases
3	13	15	I-E	the
4	17	23	I-E	pressor
5	25	30	I-E	effect
6	32	33	O	of
7	35	48	B-D	norepinephrine
8	50	52	O	but
9	54	59	B-E	blocks
10	61	63	I-E	the
11	65	71	B-D	pressor
12	73	80	O	response
13	82	83	O	of
14	85	98	B-D	phenethylamine
D/7:2:1 D/7:9:1 D/11:2:1 D/11:9:1 D/14:2:1 D/14:9:1

PAMELOR	6156	34090-1	e17dc299-f52d-414d-ab6e-e809bd6f8acb	15
Studies suggest that Pamelor interferes with the transport, release, and storage of catecholamines.
1	0	6	O	Studies
2	8	14	O	suggest
3	16	19	O	that
4	21	27	O	XXXXXXXX
5	29	38	B-E	interferes
6	40	43	I-E	with
7	45	47	I-E	the
8	49	57	I-E	transport
9	58	58	O	,
10	60	66	O	release
11	67	67	O	,
12	69	71	O	and
13	73	79	O	storage
14	81	82	O	of
15	84	97	O	catecholamines
NULL

PAMELOR	6157	34090-1	e17dc299-f52d-414d-ab6e-e809bd6f8acb	18
Operant conditioning techniques in rats and pigeons suggest that Pamelor has a combination of stimulant and depressant properties.
1	0	6	O	Operant
2	8	19	O	conditioning
3	21	30	O	techniques
4	32	33	O	in
5	35	38	O	rats
6	40	42	O	and
7	44	50	O	pigeons
8	52	58	O	suggest
9	60	63	O	that
10	65	71	O	XXXXXXXX
11	73	75	O	has
12	77	77	O	a
13	79	89	O	combination
14	91	92	B-D	of
15	94	102	I-D	stimulant
16	104	106	I-D	and
17	108	117	I-D	depressant
18	119	128	I-D	properties
NULL

