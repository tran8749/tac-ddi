<?xml version="1.0" ?>
<Label drug="Fanapt" setid="33f60b40-3fca-11de-8f56-0002a5d5c51b">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7     DRUG INTERACTIONS  Given the primary CNS effects of FANAPT, caution should be used when it is taken in combination with other centrally acting drugs and alcohol. Due to its - alpha1-adrenergic receptor antagonism, FANAPT has the potential to enhance the effect of certain antihypertensive agents.  The dose of FANAPT should be reduced in patients co-administered a strong CYP2D6 or CYP3A4 inhibitor. ( 2.2 , 7.1 )  7.1     Potential for Other Drugs to Affect FANAPT  Iloperidone is not a substrate for CYP1A1, CYP1A2, CYP2A6, CYP2B6, CYP2C8, CYP2C9, CYP2C19, or CYP2E1 enzymes. This suggests that an interaction of iloperidone with inhibitors or inducers of these enzymes, or other factors, like smoking, is unlikely.  Both CYP3A4 and CYP2D6 are responsible for iloperidone metabolism. Inhibitors of CYP3A4 (e.g., ketoconazole) or CYP2D6 (e.g., fluoxetine, paroxetine) can inhibit iloperidone elimination and cause increased blood levels.   Ketoconazole: Co-administration of ketoconazole (200 mg twice daily for 4 days), a potent inhibitor of CYP3A4, with a 3 mg single dose of iloperidone to 19 healthy volunteers, ages 18-45 years, increased the area under the curve (AUC) of iloperidone and its metabolites P88 and P95 by 57%, 55% and 35%, respectively. Iloperidone doses should be reduced by about one-half when administered with ketoconazole or other strong inhibitors of CYP3A4 (e.g., itraconazole). Weaker inhibitors (e.g., erythromycin, grapefruit juice) have not been studied. When the CYP3A4 inhibitor is withdrawn from the combination therapy, the iloperidone dose should be returned to the previous level.  Fluoxetine : Coadministration of fluoxetine (20 mg twice daily for 21 days), a potent inhibitor of CYP2D6, with a single 3 mg dose of iloperidone to 23 healthy volunteers, ages 29-44 years, who were classified as CYP2D6 extensive metabolizers, increased the AUC of iloperidone and its metabolite P88, by about 2- to 3-fold, and decreased the AUC of its metabolite P95 by one-half. Iloperidone doses should be reduced by one-half when administered with fluoxetine. When fluoxetine is withdrawn from the combination therapy, the iloperidone dose should be returned to the previous level. Other strong inhibitors of CYP2D6 would be expected to have similar effects and would need appropriate dose reductions. When the CYP2D6 inhibitor is withdrawn from the combination therapy, iloperidone dose could then be increased to the previous level.  Paroxetine: Coadministration of paroxetine (20 mg/day for 5-8 days), a potent inhibitor of CYP2D6, with multiple doses of iloperidone (8 or 12 mg twice daily) to patients with schizophrenia ages 18-65 years resulted in increased mean steady-state peak concentrations of iloperidone and its metabolite P88, by about 1.6 fold, and decreased mean steady-state peak concentrations of its metabolite P95 by one-half. Iloperidone doses should be reduced by one-half when administered with paroxetine. When paroxetine is withdrawn from the combination therapy, the iloperidone dose should be returned to the previous level. Other strong inhibitors of CYP2D6 would be expected to have similar effects and would need appropriate dose reductions. When the CYP2D6 inhibitor is withdrawn from the combination therapy, iloperidone dose could then be increased to previous levels.  Paroxetine and Ketoconazole:    Coadministration of paroxetine (20 mg once daily for 10 days), a CYP2D6 inhibitor, and ketoconazole (200 mg twice daily) with multiple doses of iloperidone (8 or 12 mg twice daily) to patients with schizophrenia ages 18-65 years resulted in a 1.4 fold increase in steady-state concentrations of iloperidone and its metabolite P88 and a 1.4 fold decrease in the P95 in the presence of paroxetine. So giving iloperidone with inhibitors of both of its metabolic pathways did not add to the effect of either inhibitor given alone. Iloperidone doses should therefore be reduced by about one-half if administered concomitantly with both a CYP2D6 and CYP3A4 inhibitor.  7.2     Potential for FANAPT to Affect Other Drugs  In vitro studies in human liver microsomes showed that iloperidone does not substantially inhibit the metabolism of drugs metabolized by the following cytochrome P450 isozymes: CYP1A1, CYP1A2, CYP2A6, CYP2B6, CYP2C8, CYP2C9, or CYP2E1. Furthermore, in vitro studies in human liver microsomes showed that iloperidone does not have enzyme inducing properties, specifically for the following cytochrome P450 isozymes: CYP1A2, CYP2C8, CYP2C9, CYP2C19, CYP3A4 and CYP3A5.  Dextromethorphan: A study in healthy volunteers showed that changes in the pharmacokinetics of dextromethorphan (80 mg dose) when a 3 mg dose of iloperidone was co-administered resulted in a 17% increase in total exposure and a 26% increase in the maximum plasma concentrations C max of dextromethorphan. Thus, an interaction between iloperidone and other CYP2D6 substrates is unlikely.  Fluoxetine:  A single 3 mg dose of iloperidone had no effect on the pharmacokinetics of fluoxetine (20 mg twice daily).   Midazolam (a sensitive CYP 3A4 substrate):  A study in patients with schizophrenia showed a less than 50% increase in midazolam total exposure at iloperidone steady state (14 days of oral dosing at up to 10 mg iloperidone twice daily) and no effect on midazolam C max . Thus, an interaction between iloperidone and other CYP3A4 substrates is unlikely.  7.3     Drugs that Prolong the QT Interval  FANAPT should not be used with any other drugs that prolong the QT interval [see Warnings and Precautions ( 5.2 )] .</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12     CLINICAL PHARMACOLOGY  12.1     Mechanism of Action  The mechanism of action of FANAPT, as with other drugs having efficacy in schizophrenia, is unknown. However it is proposed that the efficacy of FANAPT is mediated through a combination of dopamine type 2 (D 2 ) and serotonin type 2 (5-HT 2 ) antagonisms.   12.2     Pharmacodynamics  FANAPT exhibits high (nM) affinity binding to serotonin 5-HT 2A dopamine D 2 and D 3 receptors, and norepinephrine NEα1 receptors (K i values of 5.6, 6.3, 7.1, and 0.36 nM, respectively). FANAPT has moderate affinity for dopamine D 4 , and serotonin 5-HT 6 and 5-HT 7 receptors (K i values of 25, 43, and 22, nM respectively), and low affinity for the serotonin 5-HT 1A , dopamine D 1 , and histamine H 1 receptors (K i values of 168, 216 and 437 nM, respectively). FANAPT has no appreciable affinity (K i &gt;1000 nM) for cholinergic muscarinic receptors. FANAPT functions as an antagonist at the dopamine D 2 , D 3 , serotonin 5-HT 1A and norepinephrine α 1 /α 2C receptors. The affinity of the FANAPT metabolite P88 is generally equal or less than that of the parent compound. In contrast, the metabolite P95 only shows affinity for 5-HT 2A (K i value of 3.91) and the NE α1A , NE α1B , NE α1D , and NE α2  C receptors (K i values of 4.7, 2.7, 8.8 and 4.7 nM respectively).   12.3     Pharmacokinetics  The observed mean elimination half-lives for iloperidone, P88 and P95 in CYP2D6 extensive metabolizers (EM) are 18, 26, and 23 hours, respectively, and in poor metabolizers (PM) are 33, 37 and 31 hours, respectively. Steady-state concentrations are attained within 3-4 days of dosing. Iloperidone accumulation is predictable from single-dose pharmacokinetics. The pharmacokinetics of iloperidone is more than dose proportional. Elimination of iloperidone is mainly through hepatic metabolism involving 2 P450 isozymes, CYP2D6 and CYP3A4.  Absorption:  Iloperidone is well absorbed after administration of the tablet with peak plasma concentrations occurring within 2 to 4 hours; while the relative bioavailability of the tablet formulation compared to oral solution is 96%. Administration of iloperidone with a standard high-fat meal did not significantly affect the C max or AUC of iloperidone, P88, or P95, but delayed T max by 1 hour for iloperidone, 2 hours for P88 and 6 hours for P95. FANAPT can be administered without regard to meals.   Distribution:  Iloperidone has an apparent clearance (clearance/bioavailability) of 47 to 102 L/h, with an apparent volume of distribution of 1340-2800 L. At therapeutic concentrations, the unbound fraction of iloperidone in plasma is ~3% and of each metabolite (P88 and P95) it is ~8%.  Metabolism and Elimination:    Iloperidone is metabolized primarily by 3 biotransformation pathways: carbonyl reduction, hydroxylation (mediated by CYP2D6) and O -demethylation (mediated by CYP3A4). There are 2 predominant iloperidone metabolites, P95 and P88. The iloperidone metabolite P95 represents 47.9% of the AUC of iloperidone and its metabolites in plasma at steady-state for extensive metabolizers (EM) and 25% for poor metabolizers (PM). The active metabolite P88 accounts for 19.5% and 34.0% of total plasma exposure in EM and PM, respectively.  Approximately 7% - 10% of Caucasians and 3% - 8% of black/African Americans lack the capacity to metabolize CYP2D6 substrates and are classified as poor metabolizers (PM), whereas the rest are intermediate, extensive or ultrarapid metabolizers. Coadministration of FANAPT with known strong inhibitors of CYP2D6 like fluoxetine results in a 2.3-fold increase in iloperidone plasma exposure, and therefore one-half of the FANAPT dose should be administered.  Similarly, PMs of CYP2D6 have higher exposure to iloperidone compared with EMs and PMs should have their dose reduced by one-half. Laboratory tests are available to identify CYP2D6 PMs.  The bulk of the radioactive materials were recovered in the urine (mean 58.2% and 45.1% in EM and PM, respectively), with feces accounting for 19.9% (EM) to 22.1% (PM) of the dosed radioactivity.  Transporter Interaction: Iloperidone and P88 are not substrates of P-gp and iloperidone is a weak P-gp inhibitor.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Fanapt" id="3967" section="34073-7">
      

      <SentenceText>Given the primary CNS effects of FANAPT, caution should be used when it is taken in combination with other centrally acting drugs and alcohol.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="41 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="107 22" str="centrally acting drugs" type="Precipitant"/>
      <Mention code="NO MAP" id="M3" span="134 7" str="alcohol" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3968" section="34073-7">
      

      <SentenceText>Due to its - alpha1-adrenergic receptor antagonism, FANAPT has the potential to enhance the effect of certain antihypertensive agents.</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="80 18" str="enhance the effect" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M5" span="110 23" str="antihypertensive agents" type="Precipitant"/>
      <Interaction effect="M4" id="I1" precipitant="M5" trigger="M5" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3969" section="34073-7">
      

      <SentenceText>The dose of FANAPT should be reduced in patients co-administered a strong CYP2D6 or CYP3A4 inhibitor.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="4 4" str="dose" type="Trigger"/>
      <Mention code="NO MAP" id="M7" span="19 17" str="should be reduced" type="Trigger"/>
      <Mention code="NO MAP" id="M8" span="74 26" str="CYP2D6 or CYP3A4 inhibitor" type="Precipitant"/>
      <Interaction id="I2" precipitant="M8" trigger="M7" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3970" section="34073-7">
      

      <SentenceText>Iloperidone is not a substrate for CYP1A1, CYP1A2, CYP2A6, CYP2B6, CYP2C8, CYP2C9, CYP2C19, or CYP2E1 enzymes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3971" section="34073-7">
      

      <SentenceText>This suggests that an interaction of iloperidone with inhibitors or inducers of these enzymes, or other factors, like smoking, is unlikely.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3972" section="34073-7">
      

      <SentenceText>Both CYP3A4 and CYP2D6 are responsible for iloperidone metabolism.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3973" section="34073-7">
      

      <SentenceText>Inhibitors of CYP3A4 (e.g., ketoconazole) or CYP2D6 (e.g., fluoxetine, paroxetine) can inhibit iloperidone elimination and cause increased blood levels.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="129 22" str="increased blood levels" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="0 20" str="Inhibitors of CYP3A4" type="Precipitant"/>
      <Interaction effect="C54355" id="I3" precipitant="M10" trigger="M9" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M11" span="28 12" str="ketoconazole" type="Precipitant"/>
      <Interaction effect="C54355" id="I4" precipitant="M11" trigger="M9" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3974" section="34073-7">
      

      <SentenceText>Ketoconazole: Co-administration of ketoconazole (200 mg twice daily for 4 days), a potent inhibitor of CYP3A4, with a 3 mg single dose of iloperidone to 19 healthy volunteers, ages 18 to 45 years, increased the area under the curve (AUC) of iloperidone and its metabolites P88 and P95 by 57%, 55% and 35%, respectively.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="0 12" str="Ketoconazole" type="Precipitant"/>
      <Interaction effect="C54602" id="I5" precipitant="M12" trigger="M12" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I6" precipitant="M12" trigger="M12" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M13" span="35 12" str="ketoconazole" type="Precipitant"/>
      <Interaction effect="C54602" id="I7" precipitant="M13" trigger="M13" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I8" precipitant="M13" trigger="M13" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M14" span="90 19" str="inhibitor of CYP3A4" type="Precipitant"/>
      <Interaction effect="C54602" id="I9" precipitant="M14" trigger="M14" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I10" precipitant="M14" trigger="M14" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3975" section="34073-7">
      

      <SentenceText>Iloperidone doses should be reduced by about one-half when administered with ketoconazole or other strong inhibitors of CYP3A4 (e.g., itraconazole).</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="12 23" str="doses should be reduced" type="Trigger"/>
      <Mention code="NO MAP" id="M16" span="77 12" str="ketoconazole" type="Precipitant"/>
      <Interaction id="I11" precipitant="M16" trigger="M15" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M17" span="106 20" str="inhibitors of CYP3A4" type="Precipitant"/>
      <Interaction id="I12" precipitant="M17" trigger="M15" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M18" span="134 12" str="itraconazole" type="Precipitant"/>
      <Interaction id="I13" precipitant="M18" trigger="M15" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3976" section="34073-7">
      

      <SentenceText>Weaker inhibitors (e.g., erythromycin, grapefruit juice) have not been studied.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3977" section="34073-7">
      

      <SentenceText>When the CYP3A4 inhibitor is withdrawn from the combination therapy, the iloperidone dose should be returned to the previous level.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="9 16" str="CYP3A4 inhibitor" type="Precipitant"/>
      <Interaction id="I14" precipitant="M19" trigger="M19" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3978" section="34073-7">
      

      <SentenceText>Fluoxetine: Co-administration of fluoxetine (20 mg twice daily for 21 days), a potent inhibitor of CYP2D6, with a single 3 mg dose of iloperidone to 23 healthy volunteers, ages 29 to 44 years, who were classified as CYP2D6 extensive metabolizers, increased the AUC of iloperidone and its metabolite P88, by about 2- to 3-fold, and decreased the AUC of its metabolite P95 by one-half.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="247 17" str="increased the AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="331 17" str="decreased the AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M22" span="33 10" str="fluoxetine" type="Precipitant"/>
      <Interaction effect="C54605" id="I15" precipitant="M22" trigger="M21" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M23" span="86 19" str="inhibitor of CYP2D6" type="Precipitant"/>
      <Mention code="NO MAP" id="M24" span="288 14" str="metabolite P88" type="Precipitant"/>
      <Interaction effect="C54605" id="I16" precipitant="M24" trigger="M20" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3979" section="34073-7">
      

      <SentenceText>Iloperidone doses should be reduced by one-half when administered with fluoxetine.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="12 23" str="doses should be reduced" type="Trigger"/>
      <Mention code="NO MAP" id="M26" span="71 10" str="fluoxetine" type="Precipitant"/>
      <Interaction id="I17" precipitant="M26" trigger="M25" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3980" section="34073-7">
      

      <SentenceText>When fluoxetine is withdrawn from the combination therapy, the iloperidone dose should be returned to the previous level.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="5 10" str="fluoxetine" type="Precipitant"/>
      <Interaction id="I18" precipitant="M27" trigger="M27" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3981" section="34073-7">
      

      <SentenceText>Other strong inhibitors of CYP2D6 would be expected to have similar effects and would need appropriate dose reductions.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="103 4" str="dose" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3982" section="34073-7">
      

      <SentenceText>When the CYP2D6 inhibitor is withdrawn from the combination therapy, iloperidone dose could then be increased to the previous level.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="9 16" str="CYP2D6 inhibitor" type="Precipitant"/>
      <Interaction id="I19" precipitant="M29" trigger="M29" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3983" section="34073-7">
      

      <SentenceText>Paroxetine: Co-administration of paroxetine (20 mg/day for 5-8 days), a potent inhibitor of CYP2D6, with multiple doses of iloperidone (8 or 12 mg twice daily) to patients with schizophrenia ages 18-65 resulted in increased mean steady-state peak concentrations of iloperidone and its metabolite P88, by about 1.6 fold, and decreased mean steady-state peak concentrations of its metabolite P95 by one-half.</SentenceText>
      

      <Mention code="NO MAP" id="M30" span="214 47" str="increased mean steady-state peak concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M31" span="324 47" str="decreased mean steady-state peak concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M32" span="79 19" str="inhibitor of CYP2D6" type="Precipitant"/>
      <Mention code="NO MAP" id="M33" span="285 14" str="metabolite P88" type="Precipitant"/>
      <Interaction effect="C54602" id="I20" precipitant="M33" trigger="M30" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I21" precipitant="M33" trigger="M30" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3984" section="34073-7">
      

      <SentenceText>Iloperidone doses should be reduced by one-half when administered with paroxetine.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="12 23" str="doses should be reduced" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3985" section="34073-7">
      

      <SentenceText>When paroxetine is withdrawn from the combination therapy, the iloperidone dose should be returned to the previous level.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3987" section="34073-7">
      

      <SentenceText>When the CYP2D6 inhibitor is withdrawn from the combination therapy, iloperidone dose could then be increased to previous levels.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="9 16" str="CYP2D6 inhibitor" type="Precipitant"/>
      <Interaction id="I22" precipitant="M35" trigger="M35" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3988" section="34073-7">
      

      <SentenceText>Paroxetine and Ketoconazole: Co-administration of paroxetine (20 mg once daily for 10 days), a CYP2D6 inhibitor, and ketoconazole (200 mg twice daily) with multiple doses of iloperidone (8 or 12 mg twice daily) to patients with schizophrenia ages 18-65 resulted in a 1.4 fold increase in steady-state concentrations of iloperidone and its metabolite P88 and a 1.4 fold decrease in the P95 in the presence of paroxetine.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="276 39" str="increase in steady-state concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M37" span="369 19" str="decrease in the P95" type="Trigger"/>
      <Mention code="NO MAP" id="M38" span="95 16" str="CYP2D6 inhibitor" type="Precipitant"/>
      <Interaction effect="C54355" id="I23" precipitant="M38" trigger="M37" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M39" span="117 12" str="ketoconazole" type="Precipitant"/>
      <Interaction effect="C54355" id="I24" precipitant="M39" trigger="M36" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M40" span="339 18" str="metabolite P88 and" type="Precipitant"/>
      <Interaction effect="C54355" id="I25" precipitant="M40" trigger="M36" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M41" span="408 10" str="paroxetine" type="Precipitant"/>
      <Interaction effect="C54355" id="I26" precipitant="M41" trigger="M36" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3989" section="34073-7">
      

      <SentenceText>So giving iloperidone with inhibitors of both of its metabolic pathways did not add to the effect of either inhibitor given alone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3990" section="34073-7">
      

      <SentenceText>Iloperidone doses should therefore be reduced by about one-half if administered concomitantly with both a CYP2D6 and CYP3A4 inhibitor.</SentenceText>
      

      <Mention code="NO MAP" id="M42" span="12 5" str="doses" type="Trigger"/>
      <Mention code="NO MAP" id="M43" span="18 27" str="should therefore be reduced" type="Trigger"/>
      <Mention code="NO MAP" id="M44" span="106 6" str="CYP2D6" type="Precipitant"/>
      <Interaction id="I27" precipitant="M44" trigger="M43" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M45" span="117 16" str="CYP3A4 inhibitor" type="Precipitant"/>
      <Interaction id="I28" precipitant="M45" trigger="M42" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3991" section="34073-7">
      

      <SentenceText>In vitro studies in human liver microsomes showed that iloperidone does not substantially inhibit the metabolism of drugs metabolized by the following cytochrome P450 isozymes: CYP1A1, CYP1A2, CYP2A6, CYP2B6, CYP2C8, CYP2C9, or CYP2E1.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3992" section="34073-7">
      

      <SentenceText>Furthermore, in vitro studies in human liver microsomes showed that iloperidone does not have enzyme inducing properties, specifically for the following cytochrome P450 isozymes: CYP1A2, CYP2C8, CYP2C9, CYP2C19, CYP3A4 and CYP3A5.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3993" section="34073-7">
      

      <SentenceText>Dextromethorphan: A study in healthy volunteers showed that changes in the pharmacokinetics of dextromethorphan (80 mg dose) when a 3 mg dose of iloperidone was co-administered resulted in a 17% increase in total exposure and a 26% increase in the maximum plasma concentrations (Cmax)of dextromethorphan.</SentenceText>
      

      <Mention code="NO MAP" id="M46" span="60 7" str="changes" type="Trigger"/>
      <Mention code="NO MAP" id="M47" span="75 16" str="pharmacokinetics" type="Trigger"/>
      <Mention code="NO MAP" id="M48" span="195 26" str="increase in total exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M49" span="263 14" str="concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M50" span="95 16" str="dextromethorphan" type="Precipitant"/>
      <Interaction effect="C54357" id="I29" precipitant="M50" trigger="M49" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M51" span="287 16" str="dextromethorphan" type="Precipitant"/>
      <Interaction effect="C54357" id="I30" precipitant="M51" trigger="M48" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3994" section="34073-7">
      

      <SentenceText>Thus, an interaction between iloperidone and other CYP2D6 substrates is unlikely.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3995" section="34073-7">
      

      <SentenceText>Fluoxetine: A single 3 mg dose of iloperidone had no effect on the pharmacokinetics of fluoxetine (20 mg twice daily).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3996" section="34073-7">
      

      <SentenceText>Midazolam (a sensitive CYP 3A4 substrate):A study in patients with schizophrenia showed a less than 50% increase in midazolam total exposure at iloperidone steady state (14 days of oral dosing at up to 10 mg iloperidone twice daily) and no effect on midazolam Cmax.</SentenceText>
      

      <Mention code="NO MAP" id="M52" span="132 8" str="exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M53" span="0 9" str="Midazolam" type="Precipitant"/>
      <Interaction effect="C54355" id="I31" precipitant="M53" trigger="M52" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M54" span="23 3" str="CYP" type="Precipitant"/>
      <Interaction effect="C54355" id="I32" precipitant="M54" trigger="M52" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M55" span="116 9" str="midazolam" type="Precipitant"/>
      <Interaction effect="C54355" id="I33" precipitant="M55" trigger="M52" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M56" span="181 4" str="oral" type="Precipitant"/>
      <Interaction effect="C54355" id="I34" precipitant="M56" trigger="M52" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M57" span="250 9" str="midazolam" type="Precipitant"/>
      <Interaction effect="C54355" id="I35" precipitant="M57" trigger="M52" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3997" section="34073-7">
      

      <SentenceText>Thus, an interaction between iloperidone and other CYP3A4 substrates is unlikely.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3998" section="34073-7">
      

      <SentenceText>FANAPT should not be used with any other drugs that prolong the QT interval.</SentenceText>
      

      <Mention code="NO MAP" id="M58" span="7 6" str="should" type="Trigger"/>
      <Mention code="NO MAP" id="M59" span="14 11" str="not be used" type="Trigger"/>
      <Mention code="NO MAP" id="M60" span="41 34" str="drugs that prolong the QT interval" type="Precipitant"/>
      <Interaction id="I36" precipitant="M60" trigger="M59" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="3999" section="34090-1">
      

      <SentenceText>The mechanism of action of FANAPT, as with other drugs having efficacy in schizophrenia, is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4000" section="34090-1">
      

      <SentenceText>However it is proposed that the efficacy of FANAPTis mediated through a combination of dopamine type 2 (D2) and serotonin type 2 (5-HT2) antagonisms.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4001" section="34090-1">
      

      <SentenceText>FANAPT exhibits high (nM) affinity binding to serotonin 5-HT2A dopamine D2 and D3 receptors, and norepinephrine NEα1 receptors (Ki values of 5.6, 6.3, 7.1, and 0.36 nM, respectively).</SentenceText>
      

      <Mention code="NO MAP" id="M61" span="22 20" str="nM) affinity binding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M62" span="46 9" str="serotonin" type="Precipitant"/>
      <Interaction effect="C54610" id="I37" precipitant="M62" trigger="M62" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54613" id="I38" precipitant="M62" trigger="M62" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M63" span="63 11" str="dopamine D2" type="Precipitant"/>
      <Interaction effect="C54602" id="I39" precipitant="M63" trigger="M63" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I40" precipitant="M63" trigger="M63" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M64" span="97 19" str="norepinephrine NEα1" type="Precipitant"/>
      <Interaction effect="C54610" id="I41" precipitant="M64" trigger="M64" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54613" id="I42" precipitant="M64" trigger="M64" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M65" span="128 2" str="Ki" type="Precipitant"/>
      <Interaction effect="C54610" id="I43" precipitant="M65" trigger="M65" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54613" id="I44" precipitant="M65" trigger="M65" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4002" section="34090-1">
      

      <SentenceText>FANAPT has moderate affinity for dopamine D4, and serotonin 5-HT6 and 5-HT7 receptors (Ki values of 25, 43, and 22, nM respectively), and low affinity for the serotonin 5-HT1A, dopamine D1, and histamine H1 receptors (Ki values of 168, 216 and 437 nM, respectively).</SentenceText>
      

      <Mention code="NO MAP" id="M66" span="159 9" str="serotonin" type="Precipitant"/>
      <Interaction effect="C54357" id="I45" precipitant="M66" trigger="M66" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M67" span="177 8" str="dopamine" type="Precipitant"/>
      <Interaction effect="C54357" id="I46" precipitant="M67" trigger="M67" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4003" section="34090-1">
      

      <SentenceText>FANAPT has no appreciable affinity (Ki &gt;1000 nM) for cholinergic muscarinic receptors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4004" section="34090-1">
      

      <SentenceText>FANAPT functions as an antagonist at the dopamine D2, D3, serotonin 5-HT1A and norepinephrine α1/α2C receptors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4005" section="34090-1">
      

      <SentenceText>The affinity of the FANAPT metabolite P88 is generally equal or less than that of the parent compound.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4006" section="34090-1">
      

      <SentenceText>In contrast, the metabolite P95 only shows affinity for 5-HT2A (Ki value of 3.91) and the NEα1A, NEα1B, NEα1D, and NEα2 C receptors (Ki values of 4.7, 2.7, 8.8, and 4.7 nM respectively).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4007" section="34090-1">
      

      <SentenceText>The observed mean elimination half-lives for iloperidone, P88 and P95 in CYP2D6 extensive metabolizers (EM)are 18, 26, and 23 hours, respectively, and in poor metabolizers (PM) are 33, 37, and 31 hours, respectively.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4008" section="34090-1">
      

      <SentenceText>Steady-state concentrations are attained within 3-4 days of dosing.</SentenceText>
      

      <Mention code="NO MAP" id="M68" span="7 20" str="state concentrations" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4009" section="34090-1">
      

      <SentenceText>Iloperidone accumulation is predictable from single-dose pharmacokinetics.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4010" section="34090-1">
      

      <SentenceText>The pharmacokinetics of iloperidone is more than dose proportional.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4011" section="34090-1">
      

      <SentenceText>Elimination of iloperidone is mainly through hepatic metabolism involving 2 P450 isozymes, CYP2D6 and CYP3A4.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4012" section="34090-1">
      

      <SentenceText>Absorption:Iloperidone is well absorbed after administration of the tablet with peak plasma concentrations occurring within 2 to 4 hours; while the relative bioavailability of the tablet formulation compared to oral solution is 96%.</SentenceText>
      

      <Mention code="NO MAP" id="M69" span="211 13" str="oral solution" type="Precipitant"/>
      <Interaction effect="C54358" id="I47" precipitant="M69" trigger="M69" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4013" section="34090-1">
      

      <SentenceText>Administration of iloperidone with a standard high-fat meal did not significantly affect the Cmax or AUC of iloperidone, P88, or P95, but delayed Tmax by 1 hour for iloperidone, 2 hours for P88 and 6 hours for P95.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4014" section="34090-1">
      

      <SentenceText>FANAPT can be administered without regard to meals.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4015" section="34090-1">
      

      <SentenceText>Distribution:Iloperidone has an apparent clearance (clearance/bioavailability) of 47 to 102 L/h, with an apparent volume of distribution of 1340 to 2800 L. At therapeutic concentrations, the unbound fraction of iloperidone in plasma is ~3% and of each metabolite (P88 and P95) it is ~8%.</SentenceText>
      

      <Mention code="NO MAP" id="M70" span="41 36" str="clearance (clearance/bioavailability" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4016" section="34090-1">
      

      <SentenceText>Metabolism and Elimination: Iloperidone is metabolized primarily by 3 biotransformation pathways: carbonyl reduction, hydroxylation (mediated by CYP2D6) and O-demethylation (mediated by CYP3A4).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4017" section="34090-1">
      

      <SentenceText>There are 2 predominant iloperidone metabolites, P95 and P88.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4018" section="34090-1">
      

      <SentenceText>The iloperidone metabolite P95 represents 47.9% of the AUC of iloperidone and its metabolites in plasma at steady-state for extensive metabolizers (EM) and 25% for poor metabolizers (PM).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4019" section="34090-1">
      

      <SentenceText>The active metabolite P88 accounts for 19.5% and 34.0% of total plasma exposure in EM and PM, respectively.</SentenceText>
      

      <Mention code="NO MAP" id="M71" span="58 21" str="total plasma exposure" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4020" section="34090-1">
      

      <SentenceText>Approximately 7% - 10% of Caucasians and 3% - 8% of black/African Americans lack the capacity to metabolize CYP2D6 substrates and are classified as poor metabolizers (PM), whereas the rest are intermediate, extensive or ultrarapid metabolizers.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4021" section="34090-1">
      

      <SentenceText>Co-administration of FANAPT with known strong inhibitors of CYP2D6 like fluoxetine results in a 2.3-fold increase in iloperidone plasma exposure, andtherefore one-half of the FANAPT dose should be administered.</SentenceText>
      

      <Mention code="NO MAP" id="M72" span="136 8" str="exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M73" span="182 27" str="dose should be administered" type="Trigger"/>
      <Mention code="NO MAP" id="M74" span="46 36" str="inhibitors of CYP2D6 like fluoxetine" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4022" section="34090-1">
      

      <SentenceText>Similarly, PMs of CYP2D6 have higher exposure to iloperidone compared with EMsand PMs should have their dose reduced by one-half.</SentenceText>
      

      <Mention code="NO MAP" id="M75" span="30 15" str="higher exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M76" span="86 30" str="should have their dose reduced" type="Trigger"/>
      <Mention code="NO MAP" id="M77" span="75 10" str="EMsand PMs" type="Precipitant"/>
      <Interaction effect="C54357" id="I48" precipitant="M77" trigger="M76" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4023" section="34090-1">
      

      <SentenceText>Laboratory tests are available to identify CYP2D6 PMs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4024" section="34090-1">
      

      <SentenceText>The bulk of the radioactive materials were recovered in the urine (mean 58.2% and 45.1% in EM and PM, respectively), with feces accounting for 19.9% (EM) to 22.1% (PM) of the dosed radioactivity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fanapt" id="4025" section="34090-1">
      

      <SentenceText>Transporter Interaction: Iloperidone and P88 are not substrates of P-gp and iloperidone is a weak P-gp inhibitor.</SentenceText>
      

      <Mention code="NO MAP" id="M78" span="100 12" str="gp inhibitor" type="Precipitant"/>
      <Interaction effect="C54358" id="I49" precipitant="M78" trigger="M78" type="Pharmacokinetic interaction"/>
    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

