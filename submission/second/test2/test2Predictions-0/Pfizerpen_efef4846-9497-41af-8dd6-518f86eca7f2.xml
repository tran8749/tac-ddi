<?xml version="1.0" ?>
<Label drug="Pfizerpen" setid="efef4846-9497-41af-8dd6-518f86eca7f2">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Aqueous penicillin G is rapidly absorbed following both intramuscular and subcutaneous injection. Initial blood levels following parenteral administration are high but transient. Penicillins bind to serum proteins, mainly albumin. Therapeutic levels of the penicillins are easily achieved under normal circumstances in extracellular fluid and most other body tissues. Penicillins are distributed in varying degrees into pleural, pericardial, peritoneal, ascitic, synovial, and interstitial fluids. Penicillins are excreted in breast milk. Penetration into the cerebrospinal fluid, eyes, and prostate is poor. Penicillins are rapidly excreted in the urine by glomerular filtration and active tubular secretion, primarily as unchanged drug. Approximately 60 percent of the total dose of 300,000 units is excreted in the urine within this 5-hour period. For this reason, high and frequent doses are required to maintain the elevated serum levels desirable in treating certain severe infections in individuals with normal kidney function. In neonates and young infants, and in individuals with impaired kidney function, excretion is considerably delayed.  After an intravenous infusion of penicillin G, peak serum concentrations are attained immediately after completion of the infusion. In a study of ten patients administered a single 5 million unit dose of penicillin G intravenously over 3–5 minutes, the mean serum concentrations were 400 mcg/mL, 273 mcg/mL and 3.0 mcg/mL at 5–6 minutes, 10 minutes and 4 hours after completion of the injection, respectively. In a separate study, five healthy adults were administered one million units of penicillin G intravenously, either as a bolus over 4 minutes or as an infusion over 60 minutes. The mean serum concentration eight minutes after completion of the bolus was 45 mcg/mL and eight minutes after completion of the infusion was 14.4 mcg/mL. The mean β-phase serum half-life of penicillin G administered by the intravenous route in ten patients with normal renal function was 42 minutes, with a range of 31–50 minutes.  The clearance of penicillin G in normal individuals is predominantly via the kidney. The renal clearance, which is extremely rapid, is the result of glomerular filtration and active tubular transport, with the latter route predominating. Urinary recovery is reported to be 58–85% of the administered dose. Renal clearance of penicillin is delayed in premature infants, neonates and in the elderly due to decreased renal function. The serum half-life of penicillin G correlates inversely with age and clearance of creatinine and ranges from 3.2 hours in infants 0 to 6 days of age to 1.4 hours in infants 14 days of age or older.  Nonrenal clearance includes hepatic metabolism and, to a lesser extent, biliary excretion. The latter routes become more important with renal impairment.  Probenecid blocks the renal tubular secretion of penicillin. Therefore, the concurrent administration of probenecid prolongs the elimination of penicillin G and, consequently, increases the serum concentrations.  Penicillin G is distributed to most areas of the body including lung, liver, kidney, muscle, bone and placenta. In the presence of inflammation, levels of penicillin in abscesses, middle ear, pleural, peritoneal and synovial fluids are sufficient to inhibit most susceptible bacteria. Penetration in the eye, brain, cerebrospinal fluid (CSF) or prostate is poor in the absence of inflammation. With inflamed meninges, the penetration of penicillin G into the CSF improves, such that the CSF/serum ratio is 2–6%. Inflammation also enhances its penetration into the pericardial fluid. Penicillin G is actively secreted into the bile resulting in levels at least 10 times those achieved simultaneously in serum. Penicillin G penetrates poorly into human polymorphonuclear leukocytes.  In the presence of impaired renal function, the β-phase serum half-life of penicillin G is prolonged. β-phase serum half-lives of one to two hours were observed in azotemic patients with serum creatinine concentrations &lt;3 mg/100 mL and ranged as high as 20 hours in anuric patients. A linear relationship, including the lowest range of renal function, is found between the serum elimination rate constant and renal function as measured by creatinine clearance.  In patients with altered renal function, the presence of hepatic insufficiency further alters the elimination of penicillin G. In one study, the serum half-lives in two anuric patients (excreting &lt;400 mL urine/day) were 7.2 and 10.1 hours. A totally anuric patient with terminal hepatic cirrhosis had a penicillin half-life of 30.5 hours, while another patient with anuria and liver disease had a serum half-life of 16.4 hours. The dosage of penicillin G should be reduced in patients with severe renal impairment, with additional modifications when hepatic disease accompanies the renal impairment. Hemodialysis has been shown to reduce penicillin G serum levels.  Microbiology  Penicillin G exerts a bactericidal action against penicillin-susceptible microorganisms during the stage of active multiplication. It acts through the inhibition of biosynthesis of cell wall peptidoglycan rendering the cell wall osmotically unstable. It is not active against the penicillinase-producing bacteria, which include many strains of staphylococci or against organisms resistant to beta-lactams because of alterations in the penicillin-binding proteins, such as methicillin-resistant staphylococci. Penicillin G is highly active in vitro against streptococci (groups A, B, C, G, H, L, and M), and Neisseria meningitidis .  Other organisms susceptible to penicillin G are N. gonorrhoeae , Corynebacterium diphtheriae , Bacillus anthracis , Clostridium spp, Actinomyces species, &quot; Spirillum minus &quot;, Streptobacillus moniliformis , Listeria monocytogenes and Leptospira spp; Treponema pallidum is extremely sensitive to the bactericidal action of penicillin G.  Some species of gram-negative bacilli were previously considered susceptible to very high intravenous doses of penicillin G (up to 80 million units/day) including some strains of Escherichia coli,  Proteus mirabilis , Salmonella spp. and Shigella spp.; Enterobacter aerogenes (formerly Aerobacter aerogenes) and Alcaligenes faecalis . Penicillin G is no longer considered a drug of choice for infections caused by these organisms.  Susceptibility Test Methods  When available, the clinical microbiology laboratory should provide the results of in vitro susceptibility test results for antimicrobial drugs used in local hospitals and practice areas to the physician as periodic reports that describe the susceptibility profile of nosocomial and community-acquired pathogens. These reports should aid the physician in selecting the most effective antimicrobial.  Dilution Techniques  Quantitative methods are used to determine antimicrobial minimum inhibitory concentrations (MICs). These MICs provide estimates of the susceptibility of bacteria to antimicrobial compounds. The MICs should be determined using a standardized procedure. Standardized procedures are based on dilution method 1, 2, 3 (broth or agar dilution) or equivalent using standardized inoculum and concentrations of penicillin. The MIC values should be interpreted according to the criteria in Table 1.  Diffusion Techniques  Quantitative methods that require measurement of zone diameters also provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds. One such standardized procedure 2, 4 requires the use of standardized inoculum concentrations. This procedure uses paper disks impregnated with 10 units of penicillin to test the susceptibility of microorganisms to penicillin. Interpretation involves correlation of the diameter obtained in the disk test with the MIC for penicillin. Reports from the laboratory providing results of the standard single-disk susceptibility test with a 10 unit penicillin disk should be interpreted according to the following criteria in Table 1.  Table 1: Susceptibility Test Interpretive Criteria for Penicillin  MIC (mcg/mL)  Disk Diffusion (zone diameter in mm) Organisms for which no values for disk susceptibility appear cannot be reliably tested with this method.  Pathogen  Susceptible (S)  Intermediate (I)  Resistant (R)  Susceptible (S)  Intermediate (I)  Resistant (R)  Staphylococcus spp.  ≤0.12 Penicillin-resistant strains of staphylococci produce β-lactamase. An induced β-lactamase test should be performed on all S. aureus isolates for which the penicillin MIC is ≤0.12 mcg/mL or zone diameter is ≥29 mm before reporting as penicillin susceptible. Rare isolates of staphylococci that contains genes for β-lactamase production may not produce a positive induced β-lactamase test. For serious infections requiring penicillin therapy, laboratories should perform MIC tests and induced β-lactamase testing on all subsequent isolates from the same patient. 2  −  ≥0.25  ≥29  −  ≤28  Neisseria Gonorrhoeae  A positive N. gonorrhoeae β-lactamase test predicts one form of resistance to penicillin. Strains with chromosomally mediated resistance can be detected only by agar dilution or disk diffusion susceptibility test methods. 2 Isolates with zone diameters ≤19 mm generally produced β-lactamase. 2,4  ≤0.06  0.12 – 1  ≥2  ≥47  27 − 46  ≤26  Listeria Monocytogenes  ≤2 The current absence of resistant isolates precludes defining results other than &quot;Susceptible&quot;. Isolates yielding results suggestive of &quot;Non-susceptible&quot; should be submitted to a reference laboratory for further testing.  −  −  −  −  −  Streptococcus pneumoniae  ( meningitis)  ≤0.06  −  ≥0.12  −  −  −  Streptococcus pneumoniae  (non-meningitis)  ≤2  4  ≥8  −  −  −  Streptococcus spp., Beta-hemolytic Group Susceptibility testing of penicillins for treatment of β-hemolytic streptococcal infections need not be performed routinely, because non-susceptible isolates are extremely rare in any β-hemolytic streptococcus and have not been reported from Streptococcus pyogenes . Any β-hemolytic streptococcal isolate found to be non-susceptible to penicillin should be re-identified, retested, and, if confirmed, submitted to a public health authority. 2,4  ≤0.12  −  −  ≥24  −  −  Streptococcus spp., Viridans Group  ≤0.12  0.25–2  ≥4  −  −  −  Bacillus anthracis  B. anthracis strains may contain inducible β-lactamases. In vitro penicillinase induction studies suggest that penicillin MICs may increase during therapy. However, β-lactamase testing of clinical isolates of B. anthracis is unreliable and should not be performed. 3  ≤0.12  −  ≥0.25  −  −  −  Quality Control  Standardized susceptibility test procedures require the use of laboratory control microorganisms to monitor and ensure the accuracy and precision of the supplies and reagents used in the assay, and the techniques of the individuals performing the test. Standard penicillin powder should provide MIC values provided below. For the diffusion technique, the 10 unit penicillin disk should provide the following zone diameters with the quality control strains:  Table 2. In Vitro Susceptibility Test Quality Control Ranges for Penicillin  Organism (ATTC #)  MIC range mcg/mL  Disk Diffusion range (mm)  Staphylococcus aureus  (29213)  0.25 − 2  Not applicable  Staphylococcus aureus ( 25923)  Not applicable  26 − 37  Streptococcus pneumoniae  (49619)  0.25 − 1  24 − 30  Neisseria gonorrhoeae  (49226)  0.25 – 1 Using agar dilution method only. No criteria for broth microdilution are available. 2  26 − 34</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Pfizerpen" id="6476" section="34073-7">
      

      <SentenceText>Bacteriostatic antibacterials (i.e., chloramphenicol, erythromycins, sulfonamides or tetracyclines) may antagonize the bactericidal effect of penicillin, and concurrent use of these drugs should be avoided.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="104 48" str="antagonize the bactericidal effect of penicillin" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="0 29" str="Bacteriostatic antibacterials" type="Precipitant"/>
      <Interaction effect="M1" id="I1" precipitant="M2" trigger="M2" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M3" span="37 15" str="chloramphenicol" type="Precipitant"/>
      <Interaction effect="M1" id="I2" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M4" span="54 13" str="erythromycins" type="Precipitant"/>
      <Interaction effect="M1" id="I3" precipitant="M4" trigger="M4" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M5" span="85 13" str="tetracyclines" type="Precipitant"/>
      <Interaction effect="M1" id="I4" precipitant="M5" trigger="M5" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6477" section="34073-7">
      

      <SentenceText>This has been documented in vitro; however, the clinical significance of this interaction is not well-documented.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6478" section="34073-7">
      

      <SentenceText>Penicillin blood levels may be prolonged by concurrent administration of probenecid which blocks the renal tubular secretion of penicillins.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="90 34" str="blocks the renal tubular secretion" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M7" span="73 16" str="probenecid which" type="Precipitant"/>
      <Interaction effect="M6" id="I5" precipitant="M7" trigger="M7" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M8" span="128 11" str="penicillins" type="Precipitant"/>
      <Interaction effect="M6" id="I6" precipitant="M8" trigger="M8" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6479" section="34073-7">
      

      <SentenceText>Other drugs may compete with penicillin G for renal tubular secretion and thus prolong the serum half-life of penicillin.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="79 27" str="prolong the serum half-life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6480" section="34073-7">
      

      <SentenceText>These drugs include: aspirin, phenylbutazone, sulfonamides, indomethacin, thiazide diuretics, furosemide and ethacrynic acid.</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="109 15" str="ethacrynic acid" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6481" section="34090-1">
      

      <SentenceText>Aqueous penicillin G is rapidly absorbed following both intramuscular and subcutaneous injection.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6482" section="34090-1">
      

      <SentenceText>Initial blood levels following parenteral administration are high but transient.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6483" section="34090-1">
      

      <SentenceText>Penicillins bind to serum proteins, mainly albumin.</SentenceText>
      

      <Mention code="NO MAP" id="M11" span="0 11" str="Penicillins" type="Precipitant"/>
      <Interaction id="I7" precipitant="M11" trigger="M11" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6484" section="34090-1">
      

      <SentenceText>Therapeutic levels of the penicillins are easily achieved under normal circumstances in extracellular fluid and most other body tissues.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6485" section="34090-1">
      

      <SentenceText>Penicillins are distributed in varying degrees into pleural, pericardial, peritoneal, ascitic, synovial, and interstitial fluids.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="0 11" str="Penicillins" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6486" section="34090-1">
      

      <SentenceText>Penetration into the cerebrospinal fluid, eyes, and prostate is poor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6487" section="34090-1">
      

      <SentenceText>Penicillins are rapidly excreted in the urine by glomerular filtration and active tubular secretion, primarily as unchanged drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6488" section="34090-1">
      

      <SentenceText>Approximately 60 percent of the total dose of 300,000 units is excreted in the urine within this 5-hour period.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6489" section="34090-1">
      

      <SentenceText>For this reason, high and frequent doses are required to maintain the elevated serum levels desirable in treating certain severe infections in individuals with normal kidney function.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6490" section="34090-1">
      

      <SentenceText>In neonates and young infants, and in individuals with impaired kidney function, excretion is considerably delayed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6491" section="34090-1">
      

      <SentenceText>After an intravenous infusion of penicillin G, peak serum concentrations are attained immediately after completion of the infusion.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6492" section="34090-1">
      

      <SentenceText>In a study of ten patients administered a single 5 million unit dose of penicillin G intravenously over 3–5 minutes, the mean serum concentrations were 400 mcg/mL, 273 mcg/mL and 3.0 mcg/mL at 5–6 minutes, 10 minutes and 4 hours after completion of the injection, respectively.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6493" section="34090-1">
      

      <SentenceText>In a separate study, five healthy adults were administered one million units of penicillin G intravenously, either as a bolus over 4 minutes or as an infusion over 60 minutes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6494" section="34090-1">
      

      <SentenceText>The mean serum concentration eight minutes after completion of the bolus was 45 mcg/mL and eight minutes after completion of the infusion was 14.4 mcg/mL.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6495" section="34090-1">
      

      <SentenceText>The mean β-phase serum half-life of penicillin G administered by the intravenous route in ten patients with normal renal function was 42 minutes, with a range of 31–50 minutes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6496" section="34090-1">
      

      <SentenceText>The clearance of penicillin G in normal individuals is predominantly via the kidney.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6497" section="34090-1">
      

      <SentenceText>The renal clearance, which is extremely rapid, is the result of glomerular filtration and active tubular transport, with the latter route predominating.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6498" section="34090-1">
      

      <SentenceText>Urinary recovery is reported to be 58–85% of the administered dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6499" section="34090-1">
      

      <SentenceText>Renal clearance of penicillin is delayed in premature infants, neonates and in the elderly due to decreased renal function.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="98 24" str="decreased renal function" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6500" section="34090-1">
      

      <SentenceText>The serum half-life of penicillin G correlates inversely with age and clearance of creatinine and ranges from 3.2 hours in infants 0 to 6 days of age to 1.4 hours in infants 14 days of age or older.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6501" section="34090-1">
      

      <SentenceText>Nonrenal clearance includes hepatic metabolism and, to a lesser extent, biliary excretion.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6502" section="34090-1">
      

      <SentenceText>The latter routes become more important with renal impairment.</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="45 16" str="renal impairment" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6503" section="34090-1">
      

      <SentenceText>Probenecid blocks the renal tubular secretion of penicillin.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="11 6" str="blocks" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M16" span="28 7" str="tubular" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M17" span="49 10" str="penicillin" type="Precipitant"/>
      <Interaction effect="M15;M16" id="I8" precipitant="M17" trigger="M17" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6504" section="34090-1">
      

      <SentenceText>Therefore, the concurrent administration of probenecid prolongs the elimination of penicillin G and, consequently, increases the serum concentrations.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="55 24" str="prolongs the elimination" type="Trigger"/>
      <Mention code="NO MAP" id="M19" span="115 34" str="increases the serum concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="44 10" str="probenecid" type="Precipitant"/>
      <Interaction effect="C54357" id="I9" precipitant="M20" trigger="M19" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M21" span="83 10" str="penicillin" type="Precipitant"/>
      <Interaction effect="C54357" id="I10" precipitant="M21" trigger="M18" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M22" span="94 1" str="G" type="Precipitant"/>
      <Interaction effect="C54357" id="I11" precipitant="M22" trigger="M18" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6505" section="34090-1">
      

      <SentenceText>Penicillin G is distributed to most areas of the body including lung, liver, kidney, muscle, bone and placenta.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6506" section="34090-1">
      

      <SentenceText>In the presence of inflammation, levels of penicillin in abscesses, middle ear, pleural, peritoneal and synovial fluids are sufficient to inhibit most susceptible bacteria.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6507" section="34090-1">
      

      <SentenceText>Penetration in the eye, brain, cerebrospinal fluid (CSF) or prostate is poor in the absence of inflammation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6508" section="34090-1">
      

      <SentenceText>With inflamed meninges, the penetration of penicillin G into the CSF improves, such that the CSF/serum ratio is 2–6%.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="5 17" str="inflamed meninges" type="Precipitant"/>
      <Mention code="NO MAP" id="M24" span="43 12" str="penicillin G" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6509" section="34090-1">
      

      <SentenceText>Inflammation also enhances its penetration into the pericardial fluid.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6510" section="34090-1">
      

      <SentenceText>Penicillin G is actively secreted into the bile resulting in levels at least 10 times those achieved simultaneously in serum.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6511" section="34090-1">
      

      <SentenceText>Penicillin G penetrates poorly into human polymorphonuclear leukocytes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6512" section="34090-1">
      

      <SentenceText>In the presence of impaired renal function, the β-phase serum half-life of penicillin G is prolonged.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6513" section="34090-1">
      

      <SentenceText>β-phase serum half-lives of one to two hours were observed in azotemic patients with serum creatinine concentrations &lt;3 mg/100 mL and ranged as high as 20 hours in anuric patients.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6514" section="34090-1">
      

      <SentenceText>A linear relationship, including the lowest range of renal function, is found between the serum elimination rate constant and renal function as measured by creatinine clearance.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6515" section="34090-1">
      

      <SentenceText>In patients with altered renal function, the presence of hepatic insufficiency further alters the elimination of penicillin G. In one study, the serum half-lives in two anuric patients (excreting &lt;400 mL urine/day) were 7.2 and 10.1 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6516" section="34090-1">
      

      <SentenceText>A totally anuric patient with terminal hepatic cirrhosis had a penicillin half-life of 30.5 hours, while another patient with anuria and liver disease had a serum half-life of 16.4 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6517" section="34090-1">
      

      <SentenceText>The dosage of penicillin G should be reduced in patients with severe renal impairment, with additional modifications when hepatic disease accompanies the renal impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6518" section="34090-1">
      

      <SentenceText>Hemodialysis has been shown to reduce penicillin G serum levels.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6519" section="34090-1">
      

      <SentenceText>Penicillin G exerts a bactericidal action against penicillin-susceptible microorganisms during the stage of active multiplication.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6520" section="34090-1">
      

      <SentenceText>It acts through the inhibition of biosynthesis of cell wall peptidoglycan rendering the cell wall osmotically unstable.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6521" section="34090-1">
      

      <SentenceText>It is not active against the penicillinase-producing bacteria, which include many strains of staphylococci or against organisms resistant to beta-lactams because of alterations in the penicillin-binding proteins, such as methicillin-resistant staphylococci.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6522" section="34090-1">
      

      <SentenceText>Penicillin G is highly active in vitro against streptococci (groups A, B, C, G, H, L, and M), and Neisseria meningitidis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6523" section="34090-1">
      

      <SentenceText>Other organisms susceptible to penicillin G are N. gonorrhoeae, Corynebacterium diphtheriae, Bacillus anthracis, Clostridium spp, Actinomyces species, &quot;Spirillum minus&quot;, Streptobacillus moniliformis, Listeria monocytogenes and Leptospira spp; Treponema pallidum is extremely sensitive to the bactericidal action of penicillin G. Some species of gram-negative bacilli were previously considered susceptible to very high intravenous doses of penicillin G (up to 80 million units/day) including some strains of Escherichia coli, Proteus mirabilis, Salmonella spp.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6524" section="34090-1">
      

      <SentenceText>; Enterobacter aerogenes (formerly Aerobacter aerogenes) and Alcaligenes faecalis.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="2 12" str="Enterobacter" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="15 9" str="aerogenes" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6525" section="34090-1">
      

      <SentenceText>Penicillin G is no longer considered a drug of choice for infections caused by these organisms.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6526" section="34090-1">
      

      <SentenceText>When available, the clinical microbiology laboratory should provide the results of in vitro susceptibility test results for antimicrobial drugs used in local hospitals and practice areas to the physician as periodic reports that describe the susceptibility profile of nosocomial and community-acquired pathogens.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6527" section="34090-1">
      

      <SentenceText>These reports should aid the physician in selecting the most effective antimicrobial.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6528" section="34090-1">
      

      <SentenceText>Quantitative methods are used to determine antimicrobial minimum inhibitory concentrations (MICs).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6529" section="34090-1">
      

      <SentenceText>These MICs provide estimates of the susceptibility of bacteria to antimicrobial compounds.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6530" section="34090-1">
      

      <SentenceText>The MICs should be determined using a standardized procedure.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6531" section="34090-1">
      

      <SentenceText>Standardized procedures are based on dilution method 1, 2, 3 (broth or agar dilution) or equivalent using standardized inoculum and concentrations of penicillin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6532" section="34090-1">
      

      <SentenceText>The MIC values should be interpreted according to the criteria in Table 1.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6533" section="34090-1">
      

      <SentenceText>Quantitative methods that require measurement of zone diameters also provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="151 9" str="compounds" type="Precipitant"/>
      <Interaction effect="C54358" id="I12" precipitant="M27" trigger="M27" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6534" section="34090-1">
      

      <SentenceText>One such standardized procedure 2, 4 requires the use of standardized inoculum concentrations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6535" section="34090-1">
      

      <SentenceText>This procedure uses paper disks impregnated with 10 units of penicillin to test the susceptibility of microorganisms to penicillin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6536" section="34090-1">
      

      <SentenceText>Interpretation involves correlation of the diameter obtained in the disk test with the MIC for penicillin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6537" section="34090-1">
      

      <SentenceText>Reports from the laboratory providing results of the standard single-disk susceptibility test with a 10 unit penicillin disk should be interpreted according to the following criteria in Table 1.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6538" section="34090-1">
      

      <SentenceText>Table 1: Susceptibility Test Interpretive Criteria for Penicillin MIC (mcg/mL) Disk Diffusion (zone diameter in mm)Organisms for which no values for disk susceptibility appear cannot be reliably tested with this method.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6539" section="34090-1">
      

      <SentenceText>Pathogen Susceptible(S) Intermediate(I) Resistant(R) Susceptible(S) Intermediate(I) Resistant(R) Staphylococcus spp.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6540" section="34090-1">
      

      <SentenceText>≤0.12Penicillin-resistant strains of staphylococci produce β-lactamase.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6541" section="34090-1">
      

      <SentenceText>An induced β-lactamase test should be performed on all S. aureus isolates for which the penicillin MIC is ≤0.12 mcg/mL or zone diameter is ≥29 mm before reporting as penicillin susceptible.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6542" section="34090-1">
      

      <SentenceText>Rare isolates of staphylococci that contains genes for β-lactamase production may not produce a positive induced β-lactamase test.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6543" section="34090-1">
      

      <SentenceText>For serious infections requiring penicillin therapy, laboratories should perform MIC tests and induced β-lactamase testing on all subsequent isolates from the same patient.2 − ≥0.25 ≥29 − ≤28 Neisseria Gonorrhoeae A positive N. gonorrhoeae β-lactamase test predicts one form of resistance to penicillin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6544" section="34090-1">
      

      <SentenceText>Strains with chromosomally mediated resistance can be detected only by agar dilution or disk diffusion susceptibility test methods.2 Isolates with zone diameters ≤19 mm generally produced β-lactamase.2,4 ≤0.06 0.12 – 1 ≥2 ≥47 27 − 46 ≤26 Listeria Monocytogenes ≤2The current absence of resistant isolates precludes defining results other than &quot;Susceptible&quot;.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6545" section="34090-1">
      

      <SentenceText>Isolates yielding results suggestive of &quot;Non-susceptible&quot; should be submitted to a reference laboratory for further testing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6546" section="34090-1">
      

      <SentenceText>− − − − − Streptococcus pneumoniae (meningitis) ≤0.06 − ≥0.12 − − − Streptococcus pneumoniae (non-meningitis) ≤2 4 ≥8 − − − Streptococcus spp., Beta-hemolytic GroupSusceptibility testing of penicillins for treatment of β-hemolytic streptococcal infections need not be performed routinely, because non-susceptible isolates are extremely rare in any β-hemolytic streptococcus and have not been reported from Streptococcus pyogenes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6547" section="34090-1">
      

      <SentenceText>Any β-hemolytic streptococcal isolate found to be non-susceptible to penicillin should be re-identified, retested, and, if confirmed, submitted to a public health authority.2,4 ≤0.12 − − ≥24 − − Streptococcus spp., Viridans Group ≤0.12 0.25–2 ≥4 − − − Bacillus anthracis B. anthracis strains may contain inducible β-lactamases.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6548" section="34090-1">
      

      <SentenceText>In vitro penicillinase induction studies suggest that penicillin MICs may increase during therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6549" section="34090-1">
      

      <SentenceText>However, β-lactamase testing of clinical isolates of B. anthracis is unreliable and should not be performed.3 ≤0.12 − ≥0.25 − − − Standardized susceptibility test procedures require the use of laboratory control microorganisms to monitor and ensure the accuracy and precision of the supplies and reagents used in the assay, and the techniques of the individuals performing the test.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="91 18" str="not be performed.3" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6550" section="34090-1">
      

      <SentenceText>Standard penicillin powder should provide MIC values provided below.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6551" section="34090-1">
      

      <SentenceText>For the diffusion technique, the 10 unit penicillin disk should provide the following zone diameters with the quality control strains: Table 2.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6552" section="34090-1">
      

      <SentenceText>In Vitro Susceptibility Test Quality Control Ranges for Penicillin Organism(ATTC #) MIC range mcg/mL Disk Diffusion range (mm) Staphylococcus aureus (29213) 0.25 − 2 Not applicable Staphylococcus aureus (25923) Not applicable 26 − 37 Streptococcus pneumoniae (49619) 0.25 − 1 24 − 30 Neisseria gonorrhoeae (49226) 0.25 – 1Using agar dilution method only.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pfizerpen" id="6553" section="34090-1">
      

      <SentenceText>No criteria for broth microdilution are available.2 26 − 34</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

