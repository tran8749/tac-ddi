Ziprasidone	4566	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	15
Drug-drug interactions can be pharmacodynamic (combined pharmacologic effects) or pharmacokinetic (alteration of plasma levels).
1	0	3	O	Drug
2	5	8	O	drug
3	10	21	O	interactions
4	23	25	O	can
5	27	28	O	be
6	30	44	O	pharmacodynamic
7	47	54	O	combined
8	56	68	O	pharmacologic
9	70	76	O	effects
10	79	80	O	or
11	82	96	O	pharmacokinetic
12	99	108	O	alteration
13	110	111	O	of
14	113	118	O	plasma
15	120	125	O	levels
NULL

Ziprasidone	4567	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	16
The risks of using ziprasidone in combination with other drugs have been evaluated as described below.
1	0	2	O	The
2	4	8	O	risks
3	10	11	O	of
4	13	17	O	using
5	19	29	O	XXXXXXXX
6	31	32	O	in
7	34	44	O	combination
8	46	49	O	with
9	51	55	O	other
10	57	61	O	drugs
11	63	66	O	have
12	68	71	O	been
13	73	81	O	evaluated
14	83	84	O	as
15	86	94	O	described
16	96	100	O	below
NULL

Ziprasidone	4568	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	9
All interactions studies have been conducted with oral ziprasidone.
1	0	2	O	All
2	4	15	O	interactions
3	17	23	O	studies
4	25	28	O	have
5	30	33	O	been
6	35	43	O	conducted
7	45	48	O	with
8	50	53	O	oral
9	55	65	O	XXXXXXXX
NULL

Ziprasidone	4569	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	37
Based upon the pharmacodynamic and pharmacokinetic profile of ziprasidone, possible interactions could be anticipated: Approximately two-thirds of ziprasidone is metabolized via a combination of chemical reduction by glutathione and enzymatic reduction by aldehyde oxidase.
1	0	4	O	Based
2	6	9	O	upon
3	11	13	O	the
4	15	29	O	pharmacodynamic
5	31	33	O	and
6	35	49	O	pharmacokinetic
7	51	57	O	profile
8	59	60	O	of
9	62	72	O	XXXXXXXX
10	73	73	O	,
11	75	82	O	possible
12	84	95	O	interactions
13	97	101	O	could
14	103	104	O	be
15	106	116	O	anticipated
16	117	117	O	:
17	119	131	O	Approximately
18	133	135	O	two
19	137	142	O	thirds
20	144	145	O	of
21	147	157	O	XXXXXXXX
22	159	160	O	is
23	162	172	O	metabolized
24	174	176	O	via
25	178	178	O	a
26	180	190	O	combination
27	192	193	O	of
28	195	202	O	chemical
29	204	212	O	reduction
30	214	215	O	by
31	217	227	O	glutathione
32	229	231	O	and
33	233	241	O	enzymatic
34	243	251	O	reduction
35	253	254	O	by
36	256	263	B-K	aldehyde
37	265	271	I-K	oxidase
K/36:C54357

Ziprasidone	4570	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	12
There are no known clinically relevant inhibitors or inducers of aldehyde oxidase.
1	0	4	O	There
2	6	8	O	are
3	10	11	O	no
4	13	17	O	known
5	19	28	O	clinically
6	30	37	O	relevant
7	39	48	O	inhibitors
8	50	51	O	or
9	53	60	O	inducers
10	62	63	O	of
11	65	72	O	aldehyde
12	74	80	O	oxidase
NULL

Ziprasidone	4571	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	15
Less than one-third of ziprasidone metabolic clearance is mediated by cytochrome P450 catalyzed oxidation.
1	0	3	O	Less
2	5	8	O	than
3	10	12	O	one
4	14	18	O	third
5	20	21	O	of
6	23	33	O	XXXXXXXX
7	35	43	O	metabolic
8	45	53	O	clearance
9	55	56	O	is
10	58	65	O	mediated
11	67	68	O	by
12	70	79	O	cytochrome
13	81	84	O	P450
14	86	94	O	catalyzed
15	96	104	O	oxidation
NULL

Ziprasidone	4572	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	44
An in vitro enzyme inhibition study utilizing human liver microsomes showed that ziprasidone had little inhibitory effect on CYP1A2, CYP2C9, CYP2C19, CYP2D6 and CYP3A4, and thus would not likely interfere with the metabolism of drugs primarily metabolized by these enzymes.
1	0	1	O	An
2	3	4	O	in
3	6	10	O	vitro
4	12	17	O	enzyme
5	19	28	O	inhibition
6	30	34	O	study
7	36	44	O	utilizing
8	46	50	O	human
9	52	56	O	liver
10	58	67	O	microsomes
11	69	74	O	showed
12	76	79	O	that
13	81	91	O	XXXXXXXX
14	93	95	O	had
15	97	102	O	little
16	104	113	O	inhibitory
17	115	120	O	effect
18	122	123	O	on
19	125	130	O	CYP1A2
20	131	131	O	,
21	133	138	O	CYP2C9
22	139	139	O	,
23	141	147	O	CYP2C19
24	148	148	O	,
25	150	155	O	CYP2D6
26	157	159	O	and
27	161	166	O	CYP3A4
28	167	167	O	,
29	169	171	O	and
30	173	176	O	thus
31	178	182	O	would
32	184	186	O	not
33	188	193	O	likely
34	195	203	O	interfere
35	205	208	O	with
36	210	212	O	the
37	214	223	O	metabolism
38	225	226	O	of
39	228	232	O	drugs
40	234	242	O	primarily
41	244	254	O	metabolized
42	256	257	O	by
43	259	263	O	these
44	265	271	O	enzymes
NULL

Ziprasidone	4573	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	12
There is little potential for drug interactions with ziprasidone due to displacement.
1	0	4	O	There
2	6	7	O	is
3	9	14	O	little
4	16	24	O	potential
5	26	28	O	for
6	30	33	O	drug
7	35	46	O	interactions
8	48	51	O	with
9	53	63	O	XXXXXXXX
10	65	67	O	due
11	69	70	O	to
12	72	83	O	displacement
NULL

Ziprasidone	4574	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	13
Ziprasidone should not be used with any drug that prolongs the QT interval.
1	0	10	O	XXXXXXXX
2	12	17	B-T	should
3	19	21	B-T	not
4	23	24	I-T	be
5	26	29	I-T	used
6	31	34	O	with
7	36	38	O	any
8	40	43	B-U	drug
9	45	48	I-U	that
10	50	57	I-U	prolongs
11	59	61	I-U	the
12	63	64	I-U	QT
13	66	73	I-U	interval
NULL

Ziprasidone	4575	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	23
Given the primary CNS effects of ziprasidone, caution should be used when it is taken in combination with other centrally acting drugs.
1	0	4	O	Given
2	6	8	O	the
3	10	16	O	primary
4	18	20	O	CNS
5	22	28	O	effects
6	30	31	O	of
7	33	43	O	XXXXXXXX
8	44	44	O	,
9	46	52	B-T	caution
10	54	59	O	should
11	61	62	O	be
12	64	67	O	used
13	69	72	O	when
14	74	75	O	it
15	77	78	O	is
16	80	84	O	taken
17	86	87	O	in
18	89	99	O	combination
19	101	104	O	with
20	106	110	O	other
21	112	120	B-D	centrally
22	122	127	I-D	acting
23	129	133	I-D	drugs
NULL

Ziprasidone	4576	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	17
Because of its potential for inducing hypotension, ziprasidone may enhance the effects of certain antihypertensive agents.
1	0	6	O	Because
2	8	9	O	of
3	11	13	O	its
4	15	23	O	potential
5	25	27	O	for
6	29	36	O	inducing
7	38	48	B-E	hypotension
8	49	49	O	,
9	51	61	O	XXXXXXXX
10	63	65	O	may
11	67	73	B-E	enhance
12	75	77	I-E	the
13	79	85	I-E	effects
14	87	88	O	of
15	90	96	O	certain
16	98	113	B-D	antihypertensive
17	115	120	I-D	agents
D/16:7:1 D/16:11:1

Ziprasidone	4577	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	10
Ziprasidone may antagonize the effects of levodopa and dopamine agonists.
1	0	10	O	XXXXXXXX
2	12	14	O	may
3	16	25	B-E	antagonize
4	27	29	I-E	the
5	31	37	I-E	effects
6	39	40	O	of
7	42	49	B-D	levodopa
8	51	53	O	and
9	55	62	B-D	dopamine
10	64	71	I-D	agonists
D/7:3:1 D/9:3:1

Ziprasidone	4578	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	28
Carbamazepine Carbamazepine is an inducer of CYP3A4; administration of 200 mg twice daily for 21 days resulted in a decrease of approximately 35% in the AUC of ziprasidone.
1	0	12	O	Carbamazepine
2	14	26	O	Carbamazepine
3	28	29	O	is
4	31	32	O	an
5	34	40	O	inducer
6	42	43	O	of
7	45	50	O	CYP3A4
8	53	66	O	administration
9	68	69	O	of
10	71	73	O	200
11	75	76	O	mg
12	78	82	O	twice
13	84	88	O	daily
14	90	92	O	for
15	94	95	O	21
16	97	100	O	days
17	102	109	O	resulted
18	111	112	O	in
19	114	114	O	a
20	116	123	O	decrease
21	125	126	O	of
22	128	140	O	approximately
23	142	144	O	35%
24	146	147	O	in
25	149	151	O	the
26	153	155	B-T	AUC
27	157	158	O	of
28	160	170	O	XXXXXXXX
NULL

Ziprasidone	4579	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	12
This effect may be greater when higher doses of carbamazepine are administered.
1	0	3	B-E	This
2	5	10	B-E	effect
3	12	14	B-T	may
4	16	17	I-T	be
5	19	25	O	greater
6	27	30	O	when
7	32	37	O	higher
8	39	43	B-T	doses
9	45	46	O	of
10	48	60	B-K	carbamazepine
11	62	64	O	are
12	66	77	O	administered
K/10:C54358

Ziprasidone	4580	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	31
Ketoconazole Ketoconazole, a potent inhibitor of CYP3A4, at a dose of 400 mg QD for 5 days, increased the AUC and Cmax of ziprasidone by about 35-40%.
1	0	11	B-K	Ketoconazole
2	13	24	B-K	Ketoconazole
3	25	25	O	,
4	27	27	O	a
5	29	34	B-K	potent
6	36	44	I-K	inhibitor
7	46	47	I-K	of
8	49	54	I-K	CYP3A4
9	55	55	O	,
10	57	58	O	at
11	60	60	O	a
12	62	65	O	dose
13	67	68	O	of
14	70	72	O	400
15	74	75	O	mg
16	77	78	B-K	QD
17	80	82	O	for
18	84	84	O	5
19	86	89	O	days
20	90	90	O	,
21	92	100	B-T	increased
22	102	104	I-T	the
23	106	108	I-T	AUC
24	110	112	O	and
25	114	117	O	Cmax
26	119	120	O	of
27	122	132	O	XXXXXXXX
28	134	135	O	by
29	137	141	O	about
30	143	144	O	35
31	146	148	O	40%
K/1:C54602 K/2:C54602 K/5:C54602 K/16:C54602

Ziprasidone	4581	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	11
Other inhibitors of CYP3A4 would be expected to have similar effects.
1	0	4	O	Other
2	6	15	B-D	inhibitors
3	17	18	O	of
4	20	25	B-D	CYP3A4
5	27	31	O	would
6	33	34	O	be
7	36	43	O	expected
8	45	46	O	to
9	48	51	O	have
10	53	59	O	similar
11	61	67	B-E	effects
D/2:11:1 D/4:11:1

Ziprasidone	4582	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	17
Cimetidine Cimetidine at a dose of 800 mg QD for 2 days did not affect ziprasidone pharmacokinetics.
1	0	9	O	Cimetidine
2	11	20	O	Cimetidine
3	22	23	O	at
4	25	25	O	a
5	27	30	O	dose
6	32	33	O	of
7	35	37	O	800
8	39	40	O	mg
9	42	43	O	QD
10	45	47	O	for
11	49	49	O	2
12	51	54	O	days
13	56	58	O	did
14	60	62	O	not
15	64	69	O	affect
16	71	81	O	XXXXXXXX
17	83	98	O	pharmacokinetics
NULL

Ziprasidone	4583	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	18
Antacid The co-administration of 30 mL of Maalox* with ziprasidone did not affect the pharmacokinetics of ziprasidone.
1	0	6	O	Antacid
2	8	10	O	The
3	12	13	O	co
4	15	28	O	administration
5	30	31	O	of
6	33	34	O	30
7	36	37	O	mL
8	39	40	O	of
9	42	48	O	Maalox*
10	50	53	O	with
11	55	65	O	XXXXXXXX
12	67	69	O	did
13	71	73	O	not
14	75	80	O	affect
15	82	84	O	the
16	86	101	O	pharmacokinetics
17	103	104	O	of
18	106	116	O	XXXXXXXX
NULL

Ziprasidone	4584	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	36
Ziprasidone at a dose of 40 mg twice daily administered concomitantly with lithium at a dose of 450 mg twice daily for 7 days did not affect the steady-state level or renal clearance of lithium.
1	0	10	O	XXXXXXXX
2	12	13	O	at
3	15	15	O	a
4	17	20	O	dose
5	22	23	O	of
6	25	26	O	40
7	28	29	O	mg
8	31	35	O	twice
9	37	41	O	daily
10	43	54	O	administered
11	56	68	O	concomitantly
12	70	73	O	with
13	75	81	B-U	lithium
14	83	84	O	at
15	86	86	O	a
16	88	91	O	dose
17	93	94	O	of
18	96	98	O	450
19	100	101	O	mg
20	103	107	O	twice
21	109	113	O	daily
22	115	117	O	for
23	119	119	O	7
24	121	124	O	days
25	126	128	O	did
26	130	132	O	not
27	134	139	O	affect
28	141	143	O	the
29	145	150	O	steady
30	152	156	O	state
31	158	162	O	level
32	164	165	O	or
33	167	171	O	renal
34	173	181	O	clearance
35	183	184	O	of
36	186	192	O	lithium
NULL

Ziprasidone	4585	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	19
Ziprasidone dosed adjunctively to lithium in a maintenance trial of bipolar patients did not affect mean therapeutic lithium levels.
1	0	10	O	XXXXXXXX
2	12	16	O	dosed
3	18	29	O	adjunctively
4	31	32	O	to
5	34	40	O	lithium
6	42	43	O	in
7	45	45	O	a
8	47	57	O	maintenance
9	59	63	O	trial
10	65	66	O	of
11	68	74	O	bipolar
12	76	83	O	patients
13	85	87	O	did
14	89	91	O	not
15	93	98	O	affect
16	100	103	O	mean
17	105	115	O	therapeutic
18	117	123	O	lithium
19	125	130	O	levels
NULL

Ziprasidone	4586	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	17
In vivo studies have revealed no effect of ziprasidone on the pharmacokinetics of estrogen or progesterone components.
1	0	1	O	In
2	3	6	O	vivo
3	8	14	O	studies
4	16	19	O	have
5	21	28	O	revealed
6	30	31	O	no
7	33	38	O	effect
8	40	41	O	of
9	43	53	O	XXXXXXXX
10	55	56	O	on
11	58	60	O	the
12	62	77	O	pharmacokinetics
13	79	80	O	of
14	82	89	O	estrogen
15	91	92	O	or
16	94	105	O	progesterone
17	107	116	O	components
NULL

Ziprasidone	4587	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	28
Ziprasidone at a dose of 20 mg twice daily did not affect the pharmacokinetics of concomitantly administered oral contraceptives, ethinyl estradiol (0.03 mg) and levonorgestrel (0.15 mg).
1	0	10	O	XXXXXXXX
2	12	13	O	at
3	15	15	O	a
4	17	20	O	dose
5	22	23	O	of
6	25	26	O	20
7	28	29	O	mg
8	31	35	O	twice
9	37	41	O	daily
10	43	45	O	did
11	47	49	O	not
12	51	56	O	affect
13	58	60	O	the
14	62	77	O	pharmacokinetics
15	79	80	O	of
16	82	94	O	concomitantly
17	96	107	O	administered
18	109	112	O	oral
19	114	127	B-K	contraceptives
20	128	128	O	,
21	130	136	O	ethinyl
22	138	146	O	estradiol
23	149	152	O	0.03
24	154	155	O	mg
25	158	160	O	and
26	162	175	O	levonorgestrel
27	178	181	O	0.15
28	183	184	O	mg
K/19:C54358

Ziprasidone	4588	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	34
Consistent with in vitro results, a study in normal healthy volunteers showed that ziprasidone did not alter the metabolism of dextromethorphan, a CYP2D6 model substrate, to its major metabolite, dextrorphan.
1	0	9	O	Consistent
2	11	14	O	with
3	16	17	O	in
4	19	23	O	vitro
5	25	31	O	results
6	32	32	O	,
7	34	34	O	a
8	36	40	O	study
9	42	43	O	in
10	45	50	O	normal
11	52	58	O	healthy
12	60	69	O	volunteers
13	71	76	O	showed
14	78	81	O	that
15	83	93	O	XXXXXXXX
16	95	97	O	did
17	99	101	O	not
18	103	107	O	alter
19	109	111	O	the
20	113	122	O	metabolism
21	124	125	O	of
22	127	142	O	dextromethorphan
23	143	143	O	,
24	145	145	O	a
25	147	152	O	CYP2D6
26	154	158	O	model
27	160	168	O	substrate
28	169	169	O	,
29	171	172	O	to
30	174	176	O	its
31	178	182	O	major
32	184	193	O	metabolite
33	194	194	O	,
34	196	206	O	dextrorphan
NULL

Ziprasidone	4589	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	13
There was no statistically significant change in the urinary dextromethorphan/dextrorphan ratio.
1	0	4	O	There
2	6	8	O	was
3	10	11	O	no
4	13	25	O	statistically
5	27	37	O	significant
6	39	44	O	change
7	46	47	O	in
8	49	51	O	the
9	53	59	O	urinary
10	61	76	O	dextromethorphan
11	77	77	O	/
12	78	88	O	dextrorphan
13	90	94	O	ratio
NULL

Ziprasidone	4590	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	21
A pharmacokinetic interaction of ziprasidone with valproate is unlikely due to the lack of common metabolic pathways for the two drugs.
1	0	0	O	A
2	2	16	O	pharmacokinetic
3	18	28	O	interaction
4	30	31	O	of
5	33	43	O	XXXXXXXX
6	45	48	O	with
7	50	58	O	valproate
8	60	61	O	is
9	63	70	O	unlikely
10	72	74	O	due
11	76	77	O	to
12	79	81	O	the
13	83	86	O	lack
14	88	89	O	of
15	91	96	O	common
16	98	106	O	metabolic
17	108	115	O	pathways
18	117	119	O	for
19	121	123	O	the
20	125	127	O	two
21	129	133	O	drugs
NULL

Ziprasidone	4591	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	19
Ziprasidone dosed adjunctively to valproate in a maintenance trial of bipolar patients did not affect mean therapeutic valproate levels.
1	0	10	O	XXXXXXXX
2	12	16	O	dosed
3	18	29	O	adjunctively
4	31	32	O	to
5	34	42	O	valproate
6	44	45	O	in
7	47	47	O	a
8	49	59	O	maintenance
9	61	65	O	trial
10	67	68	O	of
11	70	76	O	bipolar
12	78	85	O	patients
13	87	89	O	did
14	91	93	O	not
15	95	100	O	affect
16	102	105	O	mean
17	107	117	O	therapeutic
18	119	127	O	valproate
19	129	134	O	levels
NULL

Ziprasidone	4592	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	28
Population pharmacokinetic analysis of schizophrenic patients enrolled in controlled clinical trials has not revealed evidence of any clinically significant pharmacokinetic interactions with benztropine, propranolol, or lorazepam.
1	0	9	O	Population
2	11	25	O	pharmacokinetic
3	27	34	O	analysis
4	36	37	O	of
5	39	51	O	schizophrenic
6	53	60	O	patients
7	62	69	O	enrolled
8	71	72	O	in
9	74	83	O	controlled
10	85	92	O	clinical
11	94	99	O	trials
12	101	103	O	has
13	105	107	O	not
14	109	116	O	revealed
15	118	125	O	evidence
16	127	128	O	of
17	130	132	O	any
18	134	143	O	clinically
19	145	155	O	significant
20	157	171	O	pharmacokinetic
21	173	184	O	interactions
22	186	189	O	with
23	191	201	O	benztropine
24	202	202	O	,
25	204	214	O	propranolol
26	215	215	O	,
27	217	218	O	or
28	220	228	O	lorazepam
NULL

Ziprasidone	4593	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	14
The absolute bioavailability of a 20 mg dose under fed conditions is approximately 60%.
1	0	2	O	The
2	4	11	O	absolute
3	13	27	O	bioavailability
4	29	30	O	of
5	32	32	O	a
6	34	35	O	20
7	37	38	O	mg
8	40	43	O	dose
9	45	49	O	under
10	51	53	O	fed
11	55	64	O	conditions
12	66	67	O	is
13	69	81	O	approximately
14	83	85	O	60%
NULL

Ziprasidone	4594	34073-7	036db1f2-52b3-42a0-acf9-817b7ba8c724	15
The absorption of ziprasidone is increased up to two-fold in the presence of food.
1	0	2	O	The
2	4	13	O	absorption
3	15	16	O	of
4	18	28	O	XXXXXXXX
5	30	31	O	is
6	33	41	O	increased
7	43	44	O	up
8	46	47	O	to
9	49	51	O	two
10	53	56	O	fold
11	58	59	O	in
12	61	63	O	the
13	65	72	O	presence
14	74	75	O	of
15	77	80	O	food
NULL

Ziprasidone	4595	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	18
The mechanism of action of ziprasidone, as with other drugs having efficacy in schizophrenia, is unknown.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	22	O	action
5	24	25	O	of
6	27	37	O	XXXXXXXX
7	38	38	O	,
8	40	41	O	as
9	43	46	O	with
10	48	52	O	other
11	54	58	O	drugs
12	60	65	O	having
13	67	74	O	efficacy
14	76	77	O	in
15	79	91	O	schizophrenia
16	92	92	O	,
17	94	95	O	is
18	97	103	O	unknown
NULL

Ziprasidone	4596	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	30
However, it has been proposed that this drug's efficacy in schizophrenia is mediated through a combination of dopamine type 2 (D 2) and serotonin type 2 (5HT 2) antagonism.
1	0	6	O	However
2	7	7	O	,
3	9	10	O	it
4	12	14	O	has
5	16	19	O	been
6	21	28	O	proposed
7	30	33	O	that
8	35	38	O	this
9	40	45	O	drug's
10	47	54	O	efficacy
11	56	57	O	in
12	59	71	O	schizophrenia
13	73	74	O	is
14	76	83	O	mediated
15	85	91	O	through
16	93	93	O	a
17	95	105	O	combination
18	107	108	O	of
19	110	117	O	dopamine
20	119	122	O	type
21	124	124	O	2
22	127	127	O	D
23	129	129	O	2
24	132	134	O	and
25	136	144	O	serotonin
26	146	149	O	type
27	151	151	O	2
28	154	156	O	5HT
29	158	158	O	2
30	161	170	O	antagonism
NULL

Ziprasidone	4597	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	21
As with other drugs having efficacy in bipolar disorder, the mechanism of action of ziprasidone in bipolar disorder is unknown.
1	0	1	O	As
2	3	6	O	with
3	8	12	O	other
4	14	18	O	drugs
5	20	25	O	having
6	27	34	O	efficacy
7	36	37	O	in
8	39	45	O	bipolar
9	47	54	O	disorder
10	55	55	O	,
11	57	59	O	the
12	61	69	O	mechanism
13	71	72	O	of
14	74	79	O	action
15	81	82	O	of
16	84	94	O	XXXXXXXX
17	96	97	O	in
18	99	105	O	bipolar
19	107	114	O	disorder
20	116	117	O	is
21	119	125	O	unknown
NULL

Ziprasidone	4598	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	59
Ziprasidone exhibited high in vitro binding affinity for the dopamine D2 and D3, the serotonin 5HT2A, 5HT2C, 5HT1A, 5HT1D, and A1-adrenergic receptors (Ki s of 4.8, 7.2, 0.4, 1.3, 3.4, 2, and 10 nM, respectively), and moderate affinity for the histamine H1 receptor (Ki=47 nM).
1	0	10	O	XXXXXXXX
2	12	20	O	exhibited
3	22	25	O	high
4	27	28	O	in
5	30	34	O	vitro
6	36	42	O	binding
7	44	51	O	affinity
8	53	55	O	for
9	57	59	O	the
10	61	68	O	dopamine
11	70	71	O	D2
12	73	75	O	and
13	77	78	O	D3
14	79	79	O	,
15	81	83	O	the
16	85	93	O	serotonin
17	95	99	O	5HT2A
18	100	100	O	,
19	102	106	O	5HT2C
20	107	107	O	,
21	109	113	O	5HT1A
22	114	114	O	,
23	116	120	O	5HT1D
24	121	121	O	,
25	123	125	O	and
26	127	128	O	A1
27	130	139	O	adrenergic
28	141	149	O	receptors
29	152	153	O	Ki
30	155	155	O	s
31	157	158	O	of
32	160	162	O	4.8
33	163	163	O	,
34	165	167	O	7.2
35	168	168	O	,
36	170	172	O	0.4
37	173	173	O	,
38	175	177	O	1.3
39	178	178	O	,
40	180	182	O	3.4
41	183	183	O	,
42	185	185	O	2
43	186	186	O	,
44	188	190	O	and
45	192	193	O	10
46	195	196	O	nM
47	197	197	O	,
48	199	210	O	respectively
49	212	212	O	,
50	214	216	O	and
51	218	225	O	moderate
52	227	234	O	affinity
53	236	238	O	for
54	240	242	O	the
55	244	252	O	histamine
56	254	255	O	H1
57	257	264	O	receptor
58	267	271	O	Ki=47
59	273	274	O	nM
NULL

Ziprasidone	4599	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	27
Ziprasidone functioned as an antagonist at the D 2, 5HT 2A, and 5HT 1D receptors, and as an agonist at the 5HT 1A receptor.
1	0	10	O	XXXXXXXX
2	12	21	O	functioned
3	23	24	O	as
4	26	27	O	an
5	29	38	O	antagonist
6	40	41	O	at
7	43	45	O	the
8	47	47	O	D
9	49	49	O	2
10	50	50	O	,
11	52	54	O	5HT
12	56	57	O	2A
13	58	58	O	,
14	60	62	O	and
15	64	66	O	5HT
16	68	69	O	1D
17	71	79	O	receptors
18	80	80	O	,
19	82	84	O	and
20	86	87	O	as
21	89	90	O	an
22	92	98	O	agonist
23	100	101	O	at
24	103	105	O	the
25	107	109	O	5HT
26	111	112	O	1A
27	114	121	O	receptor
NULL

Ziprasidone	4600	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	8
Ziprasidone inhibited synaptic reuptake of serotonin and norepinephrine.
1	0	10	O	XXXXXXXX
2	12	20	O	inhibited
3	22	29	O	synaptic
4	31	38	O	reuptake
5	40	41	O	of
6	43	51	O	serotonin
7	53	55	O	and
8	57	70	O	norepinephrine
NULL

Ziprasidone	4601	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	21
No appreciable affinity was exhibited for other receptor/binding sites tested, including the cholinergic muscarinic receptor (IC50 >1 uM).
1	0	1	O	No
2	3	13	O	appreciable
3	15	22	O	affinity
4	24	26	O	was
5	28	36	O	exhibited
6	38	40	O	for
7	42	46	O	other
8	48	55	O	receptor
9	56	56	O	/
10	57	63	O	binding
11	65	69	O	sites
12	71	76	O	tested
13	77	77	O	,
14	79	87	O	including
15	89	91	O	the
16	93	103	O	cholinergic
17	105	114	O	muscarinic
18	116	123	O	receptor
19	126	129	O	IC50
20	131	132	O	>1
21	134	135	O	uM
NULL

Ziprasidone	4602	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	25
Antagonism at receptors other than dopamine and 5HT 2 with similar receptor affinities may explain some of the other therapeutic and side effects of ziprasidone.
1	0	9	O	Antagonism
2	11	12	O	at
3	14	22	O	receptors
4	24	28	O	other
5	30	33	O	than
6	35	42	B-D	dopamine
7	44	46	O	and
8	48	50	O	5HT
9	52	52	O	2
10	54	57	O	with
11	59	65	O	similar
12	67	74	B-D	receptor
13	76	85	I-D	affinities
14	87	89	O	may
15	91	97	O	explain
16	99	102	O	some
17	104	105	O	of
18	107	109	O	the
19	111	115	O	other
20	117	127	O	therapeutic
21	129	131	O	and
22	133	136	O	side
23	138	144	O	effects
24	146	147	O	of
25	149	159	O	XXXXXXXX
NULL

Ziprasidone	4603	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	16
Ziprasidone's antagonism of histamine H 1 receptors may explain the somnolence observed with this drug.
1	0	10	O	XXXXXXXX
2	11	12	O	's
3	14	23	O	antagonism
4	25	26	O	of
5	28	36	O	histamine
6	38	38	O	H
7	40	40	O	1
8	42	50	O	receptors
9	52	54	O	may
10	56	62	O	explain
11	64	66	O	the
12	68	77	B-E	somnolence
13	79	86	O	observed
14	88	91	O	with
15	93	96	O	this
16	98	101	O	drug
NULL

Ziprasidone	4604	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	16
Ziprasidone's antagonism of A1-adrenergic receptors may explain the orthostatic hypotension observed with this drug.
1	0	10	O	XXXXXXXX
2	11	12	O	's
3	14	23	O	antagonism
4	25	26	O	of
5	28	29	O	A1
6	31	40	O	adrenergic
7	42	50	O	receptors
8	52	54	O	may
9	56	62	B-E	explain
10	64	66	I-E	the
11	68	78	I-E	orthostatic
12	80	90	I-E	hypotension
13	92	99	O	observed
14	101	104	O	with
15	106	109	O	this
16	111	114	O	drug
NULL

Ziprasidone	4605	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	12
Oral Pharmacokinetics Ziprasidone's activity is primarily due to the parent drug.
1	0	3	O	Oral
2	5	20	O	Pharmacokinetics
3	22	32	O	XXXXXXXX
4	33	34	O	's
5	36	43	O	activity
6	45	46	O	is
7	48	56	O	primarily
8	58	60	O	due
9	62	63	O	to
10	65	67	O	the
11	69	74	O	parent
12	76	79	O	drug
NULL

Ziprasidone	4606	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	24
The multiple-dose pharmacokinetics of ziprasidone are dose-proportional within the proposed clinical dose range, and ziprasidone accumulation is predictable with multiple dosing.
1	0	2	O	The
2	4	11	O	multiple
3	13	16	O	dose
4	18	33	O	pharmacokinetics
5	35	36	O	of
6	38	48	O	XXXXXXXX
7	50	52	O	are
8	54	57	O	dose
9	59	70	O	proportional
10	72	77	O	within
11	79	81	O	the
12	83	90	O	proposed
13	92	99	O	clinical
14	101	104	O	dose
15	106	110	O	range
16	111	111	O	,
17	113	115	O	and
18	117	127	O	XXXXXXXX
19	129	140	O	accumulation
20	142	143	O	is
21	145	155	O	predictable
22	157	160	O	with
23	162	169	O	multiple
24	171	176	O	dosing
NULL

Ziprasidone	4607	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	24
Elimination of ziprasidone is mainly via hepatic metabolism with a mean terminal half-life of about 7 hours within the proposed clinical dose range.
1	0	10	O	Elimination
2	12	13	O	of
3	15	25	O	XXXXXXXX
4	27	28	O	is
5	30	35	O	mainly
6	37	39	O	via
7	41	47	O	hepatic
8	49	58	O	metabolism
9	60	63	O	with
10	65	65	O	a
11	67	70	O	mean
12	72	79	O	terminal
13	81	84	O	half
14	86	89	O	life
15	91	92	O	of
16	94	98	O	about
17	100	100	O	7
18	102	106	O	hours
19	108	113	O	within
20	115	117	O	the
21	119	126	O	proposed
22	128	135	O	clinical
23	137	140	O	dose
24	142	146	O	range
NULL

Ziprasidone	4608	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	12
Steady-state concentrations are achieved within one to three days of dosing.
1	0	5	O	Steady
2	7	11	O	state
3	13	26	O	concentrations
4	28	30	O	are
5	32	39	O	achieved
6	41	46	O	within
7	48	50	O	one
8	52	53	O	to
9	55	59	O	three
10	61	64	O	days
11	66	67	O	of
12	69	74	O	dosing
NULL

Ziprasidone	4609	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	12
The mean apparent systemic clearance is 7.5 mL/min/kg.
1	0	2	O	The
2	4	7	O	mean
3	9	16	O	apparent
4	18	25	O	systemic
5	27	35	O	clearance
6	37	38	O	is
7	40	42	O	7.5
8	44	45	O	mL
9	46	46	O	/
10	47	49	O	min
11	50	50	O	/
12	51	52	O	kg
NULL

Ziprasidone	4610	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	15
Ziprasidone is unlikely to interfere with the metabolism of drugs metabolized by cytochrome P450 enzymes.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	22	O	unlikely
4	24	25	O	to
5	27	35	O	interfere
6	37	40	O	with
7	42	44	O	the
8	46	55	O	metabolism
9	57	58	O	of
10	60	64	O	drugs
11	66	76	O	metabolized
12	78	79	O	by
13	81	90	O	cytochrome
14	92	95	O	P450
15	97	103	O	enzymes
NULL

Ziprasidone	4611	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	19
Absorption: Ziprasidone is well absorbed after oral administration, reaching peak plasma concentrations in 6 to 8 hours.
1	0	9	O	Absorption
2	10	10	O	:
3	12	22	O	XXXXXXXX
4	24	25	O	is
5	27	30	O	well
6	32	39	O	absorbed
7	41	45	O	after
8	47	50	O	oral
9	52	65	O	administration
10	66	66	O	,
11	68	75	O	reaching
12	77	80	O	peak
13	82	87	O	plasma
14	89	102	O	concentrations
15	104	105	O	in
16	107	107	O	6
17	109	110	O	to
18	112	112	O	8
19	114	118	O	hours
NULL

Ziprasidone	4614	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	15
Distribution: Ziprasidone has a mean apparent volume of distribution of 1.5 L/kg.
1	0	11	O	Distribution
2	12	12	O	:
3	14	24	O	XXXXXXXX
4	26	28	O	has
5	30	30	O	a
6	32	35	O	mean
7	37	44	O	apparent
8	46	51	O	volume
9	53	54	O	of
10	56	67	O	distribution
11	69	70	O	of
12	72	74	O	1.5
13	76	76	O	L
14	77	77	O	/
15	78	79	O	kg
NULL

Ziprasidone	4615	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	18
It is greater than 99% bound to plasma proteins, binding primarily to albumin and A1-acid glycoprotein.
1	0	1	O	It
2	3	4	O	is
3	6	12	O	greater
4	14	17	O	than
5	19	21	O	99%
6	23	27	O	bound
7	29	30	O	to
8	32	37	O	plasma
9	39	46	O	proteins
10	47	47	O	,
11	49	55	O	binding
12	57	65	O	primarily
13	67	68	O	to
14	70	76	O	albumin
15	78	80	O	and
16	82	83	O	A1
17	85	88	O	acid
18	90	101	O	glycoprotein
NULL

Ziprasidone	4616	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	34
The in vitro plasma protein binding of ziprasidone was not altered by warfarin or propranolol, two highly protein-bound drugs, nor did ziprasidone alter the binding of these drugs in human plasma.
1	0	2	O	The
2	4	5	O	in
3	7	11	O	vitro
4	13	18	O	plasma
5	20	26	O	protein
6	28	34	O	binding
7	36	37	O	of
8	39	49	O	XXXXXXXX
9	51	53	O	was
10	55	57	O	not
11	59	65	O	altered
12	67	68	O	by
13	70	77	O	warfarin
14	79	80	O	or
15	82	92	O	propranolol
16	93	93	O	,
17	95	97	O	two
18	99	104	O	highly
19	106	112	O	protein
20	114	118	O	bound
21	120	124	O	drugs
22	125	125	O	,
23	127	129	O	nor
24	131	133	O	did
25	135	145	O	XXXXXXXX
26	147	151	O	alter
27	153	155	O	the
28	157	163	O	binding
29	165	166	O	of
30	168	172	O	these
31	174	178	O	drugs
32	180	181	O	in
33	183	187	O	human
34	189	194	O	plasma
NULL

Ziprasidone	4617	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	14
Thus, the potential for drug interactions with ziprasidone due to displacement is minimal.
1	0	3	O	Thus
2	4	4	O	,
3	6	8	O	the
4	10	18	O	potential
5	20	22	O	for
6	24	27	O	drug
7	29	40	O	interactions
8	42	45	O	with
9	47	57	O	XXXXXXXX
10	59	61	O	due
11	63	64	O	to
12	66	77	O	displacement
13	79	80	O	is
14	82	88	O	minimal
NULL

Ziprasidone	4618	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	27
Metabolism and Elimination: Ziprasidone is extensively metabolized after oral administration with only a small amount excreted in the urine (<1%) or feces (<4%) as unchanged drug.
1	0	9	O	Metabolism
2	11	13	O	and
3	15	25	O	Elimination
4	26	26	O	:
5	28	38	O	XXXXXXXX
6	40	41	O	is
7	43	53	O	extensively
8	55	65	O	metabolized
9	67	71	O	after
10	73	76	O	oral
11	78	91	O	administration
12	93	96	O	with
13	98	101	O	only
14	103	103	O	a
15	105	109	O	small
16	111	116	O	amount
17	118	125	O	excreted
18	127	128	O	in
19	130	132	O	the
20	134	138	O	urine
21	141	143	O	<1%
22	146	147	O	or
23	149	153	O	feces
24	156	158	O	<4%
25	161	162	O	as
26	164	172	O	unchanged
27	174	177	O	drug
NULL

Ziprasidone	4619	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	29
Ziprasidone is primarily cleared via three metabolic routes to yield four major circulating metabolites, benzisothiazole (BITP) sulphoxide, BITP-sulphone, ziprasidone sulphoxide, and S-methyldihydroziprasidone.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	23	O	primarily
4	25	31	O	cleared
5	33	35	O	via
6	37	41	O	three
7	43	51	O	metabolic
8	53	58	O	routes
9	60	61	O	to
10	63	67	O	yield
11	69	72	O	four
12	74	78	O	major
13	80	90	O	circulating
14	92	102	O	metabolites
15	103	103	O	,
16	105	119	O	benzisothiazole
17	122	125	O	BITP
18	128	137	O	sulphoxide
19	138	138	O	,
20	140	143	O	BITP
21	145	152	O	sulphone
22	153	153	O	,
23	155	165	O	XXXXXXXX
24	167	176	O	sulphoxide
25	177	177	O	,
26	179	181	O	and
27	183	183	O	S
28	185	197	O	methyldihydro
29	198	208	O	XXXXXXXX
NULL

Ziprasidone	4620	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	19
Approximately 20% of the dose is excreted in the urine, with approximately 66% being eliminated in the feces.
1	0	12	O	Approximately
2	14	16	O	20%
3	18	19	O	of
4	21	23	O	the
5	25	28	O	dose
6	30	31	O	is
7	33	40	O	excreted
8	42	43	O	in
9	45	47	O	the
10	49	53	O	urine
11	54	54	O	,
12	56	59	O	with
13	61	73	O	approximately
14	75	77	O	66%
15	79	83	O	being
16	85	94	O	eliminated
17	96	97	O	in
18	99	101	O	the
19	103	107	O	feces
NULL

Ziprasidone	4621	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	12
Unchanged ziprasidone represents about 44% of total drug-related material in serum.
1	0	8	O	Unchanged
2	10	20	O	XXXXXXXX
3	22	31	O	represents
4	33	37	O	about
5	39	41	O	44%
6	43	44	O	of
7	46	50	O	total
8	52	55	O	drug
9	57	63	O	related
10	65	72	O	material
11	74	75	O	in
12	77	81	O	serum
NULL

Ziprasidone	4622	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	18
In vitro studies using human liver subcellular fractions indicate that S-methyldihydroziprasidone is generated in two steps.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	21	O	using
5	23	27	O	human
6	29	33	O	liver
7	35	45	O	subcellular
8	47	55	O	fractions
9	57	64	O	indicate
10	66	69	O	that
11	71	71	O	S
12	73	85	O	methyldihydro
13	86	96	O	XXXXXXXX
14	98	99	O	is
15	101	109	O	generated
16	111	112	O	in
17	114	116	O	two
18	118	122	O	steps
NULL

Ziprasidone	4623	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	33
These studies indicate that the reduction reaction is mediated primarily by chemical reduction by glutathione as well as by enzymatic reduction by aldehyde oxidase and the subsequent methylation is mediated by thiol methyltransferase.
1	0	4	O	These
2	6	12	O	studies
3	14	21	O	indicate
4	23	26	O	that
5	28	30	O	the
6	32	40	O	reduction
7	42	49	O	reaction
8	51	52	O	is
9	54	61	O	mediated
10	63	71	O	primarily
11	73	74	O	by
12	76	83	O	chemical
13	85	93	O	reduction
14	95	96	O	by
15	98	108	O	glutathione
16	110	111	O	as
17	113	116	O	well
18	118	119	O	as
19	121	122	O	by
20	124	132	O	enzymatic
21	134	142	O	reduction
22	144	145	O	by
23	147	154	O	aldehyde
24	156	162	O	oxidase
25	164	166	O	and
26	168	170	O	the
27	172	181	O	subsequent
28	183	193	O	methylation
29	195	196	O	is
30	198	205	O	mediated
31	207	208	O	by
32	210	214	O	thiol
33	216	232	O	methyltransferase
NULL

Ziprasidone	4624	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	24
In vitro studies using human liver microsomes and recombinant enzymes indicate that CYP3A4 is the major CYP contributing to the oxidative metabolism of ziprasidone.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	21	O	using
5	23	27	O	human
6	29	33	O	liver
7	35	44	O	microsomes
8	46	48	O	and
9	50	60	O	recombinant
10	62	68	O	enzymes
11	70	77	O	indicate
12	79	82	O	that
13	84	89	O	CYP3A4
14	91	92	O	is
15	94	96	O	the
16	98	102	O	major
17	104	106	O	CYP
18	108	119	O	contributing
19	121	122	O	to
20	124	126	O	the
21	128	136	O	oxidative
22	138	147	O	metabolism
23	149	150	O	of
24	152	162	O	XXXXXXXX
NULL

Ziprasidone	4625	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	8
CYP1A2 may contribute to a much lesser extent.
1	0	5	O	CYP1A2
2	7	9	O	may
3	11	20	O	contribute
4	22	23	O	to
5	25	25	O	a
6	27	30	O	much
7	32	37	O	lesser
8	39	44	O	extent
NULL

Ziprasidone	4626	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	30
Based on in vivo abundance of excretory metabolites, less than one-third of ziprasidone metabolic clearance is mediated by cytochrome P450 catalyzed oxidation and approximately two-thirds via reduction.
1	0	4	O	Based
2	6	7	O	on
3	9	10	O	in
4	12	15	O	vivo
5	17	25	O	abundance
6	27	28	O	of
7	30	38	O	excretory
8	40	50	O	metabolites
9	51	51	O	,
10	53	56	O	less
11	58	61	O	than
12	63	65	O	one
13	67	71	O	third
14	73	74	O	of
15	76	86	O	XXXXXXXX
16	88	96	O	metabolic
17	98	106	O	clearance
18	108	109	O	is
19	111	118	O	mediated
20	120	121	O	by
21	123	132	B-K	cytochrome
22	134	137	I-K	P450
23	139	147	I-K	catalyzed
24	149	157	O	oxidation
25	159	161	O	and
26	163	175	O	approximately
27	177	179	O	two
28	181	186	O	thirds
29	188	190	O	via
30	192	200	B-T	reduction
K/21:C54355

Ziprasidone	4628	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	13
Intramuscular Pharmacokinetics Systemic Bioavailability: The bioavailability of ziprasidone administered intramuscularly is 100%.
1	0	12	O	Intramuscular
2	14	29	O	Pharmacokinetics
3	31	38	O	Systemic
4	40	54	O	Bioavailability
5	55	55	O	:
6	57	59	O	The
7	61	75	O	bioavailability
8	77	78	O	of
9	80	90	O	XXXXXXXX
10	92	103	O	administered
11	105	119	O	intramuscularly
12	121	122	O	is
13	124	127	O	100%
NULL

Ziprasidone	4629	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	32
After intramuscular administration of single doses, peak serum concentrations typically occur at approximately 60 minutes post-dose or earlier and the mean half-life (T%) ranges from two to five hours.
1	0	4	O	After
2	6	18	O	intramuscular
3	20	33	O	administration
4	35	36	O	of
5	38	43	O	single
6	45	49	O	doses
7	50	50	O	,
8	52	55	O	peak
9	57	61	O	serum
10	63	76	O	concentrations
11	78	86	O	typically
12	88	92	O	occur
13	94	95	O	at
14	97	109	O	approximately
15	111	112	O	60
16	114	120	O	minutes
17	122	125	O	post
18	127	130	O	dose
19	132	133	O	or
20	135	141	O	earlier
21	143	145	O	and
22	147	149	O	the
23	151	154	O	mean
24	156	159	O	half
25	161	164	O	life
26	167	168	O	T%
27	171	176	O	ranges
28	178	181	O	from
29	183	185	O	two
30	187	188	O	to
31	190	193	O	five
32	195	199	O	hours
NULL

Ziprasidone	4630	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	19
Exposure increases in a dose-related manner and following three days of intramuscular dosing, little accumulation is observed.
1	0	7	O	Exposure
2	9	17	O	increases
3	19	20	O	in
4	22	22	O	a
5	24	27	O	dose
6	29	35	O	related
7	37	42	O	manner
8	44	46	O	and
9	48	56	O	following
10	58	62	O	three
11	64	67	O	days
12	69	70	O	of
13	72	84	O	intramuscular
14	86	91	O	dosing
15	92	92	O	,
16	94	99	O	little
17	101	112	O	accumulation
18	114	115	O	is
19	117	124	O	observed
NULL

Ziprasidone	4631	34090-1	036db1f2-52b3-42a0-acf9-817b7ba8c724	32
Metabolism and Elimination: Although the metabolism and elimination of IM ziprasidone have not been systematically evaluated, the intramuscular route of administration would not be expected to alter the metabolic pathways.
1	0	9	O	Metabolism
2	11	13	O	and
3	15	25	O	Elimination
4	26	26	O	:
5	28	35	O	Although
6	37	39	O	the
7	41	50	O	metabolism
8	52	54	O	and
9	56	66	O	elimination
10	68	69	O	of
11	71	72	O	IM
12	74	84	O	XXXXXXXX
13	86	89	O	have
14	91	93	O	not
15	95	98	O	been
16	100	113	O	systematically
17	115	123	O	evaluated
18	124	124	O	,
19	126	128	O	the
20	130	142	O	intramuscular
21	144	148	O	route
22	150	151	O	of
23	153	166	O	administration
24	168	172	O	would
25	174	176	O	not
26	178	179	O	be
27	181	188	O	expected
28	190	191	O	to
29	193	197	O	alter
30	199	201	O	the
31	203	211	O	metabolic
32	213	220	O	pathways
NULL

