Eszopiclone	268	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	10
CNS Depressants: Additive CNS-depressant effects with combination use.
1	0	2	B-D	CNS
2	4	14	I-D	Depressants
3	15	15	O	:
4	17	24	B-E	Additive
5	26	28	I-E	CNS
6	30	39	I-E	depressant
7	41	47	I-E	effects
8	49	52	O	with
9	54	64	O	combination
10	66	68	O	use
D/1:4:1

Eszopiclone	269	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	30
Use with ethanol causes additive psychomotor impairment (7.1) Rifampicin: Combination use may decrease exposure and effects of LUNESTA (7.2) Ketoconazole: Combination use increases exposure and effect of LUNESTA.
1	0	2	O	Use
2	4	7	O	with
3	9	15	B-D	ethanol
4	17	22	O	causes
5	24	31	B-E	additive
6	33	43	I-E	psychomotor
7	45	54	I-E	impairment
8	57	59	O	7.1
9	62	71	O	Rifampicin
10	72	72	O	:
11	74	84	O	Combination
12	86	88	O	use
13	90	92	O	may
14	94	101	B-T	decrease
15	103	110	I-T	exposure
16	112	114	O	and
17	116	122	O	effects
18	124	125	O	of
19	127	133	B-K	LUNESTA
20	136	138	O	7.2
21	141	152	B-K	Ketoconazole
22	153	153	O	:
23	155	165	O	Combination
24	167	169	O	use
25	171	179	B-T	increases
26	181	188	I-T	exposure
27	190	192	O	and
28	194	199	O	effect
29	201	202	O	of
30	204	210	O	LUNESTA
D/3:5:1K/19:C54357 K/21:C54357

Eszopiclone	270	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	23
Dose reduction of LUNESTA is needed (7.2) Ethanol: An additive effect on psychomotor performance was seen with coadministration of eszopiclone and ethanol.
1	0	3	B-T	Dose
2	5	13	I-T	reduction
3	15	16	O	of
4	18	24	O	LUNESTA
5	26	27	O	is
6	29	34	O	needed
7	37	39	O	7.2
8	42	48	O	Ethanol
9	49	49	O	:
10	51	52	O	An
11	54	61	B-E	additive
12	63	68	I-E	effect
13	70	71	I-E	on
14	73	83	I-E	psychomotor
15	85	95	O	performance
16	97	99	O	was
17	101	104	O	seen
18	106	109	O	with
19	111	126	O	coadministration
20	128	129	O	of
21	131	141	O	XXXXXXXX
22	143	145	O	and
23	147	153	B-D	ethanol
D/23:11:1

Eszopiclone	271	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	13
Olanzapine: Coadministration of eszopiclone and olanzapine produced a decrease in DSST scores.
1	0	9	O	Olanzapine
2	10	10	O	:
3	12	27	O	Coadministration
4	29	30	O	of
5	32	42	O	XXXXXXXX
6	44	46	O	and
7	48	57	B-K	olanzapine
8	59	66	O	produced
9	68	68	O	a
10	70	77	B-T	decrease
11	79	80	O	in
12	82	85	B-T	DSST
13	87	92	I-T	scores
K/7:C54355

Eszopiclone	272	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	14
The interaction was pharmacodynamic; there was no alteration in the pharmacokinetics of either drug.
1	0	2	O	The
2	4	14	O	interaction
3	16	18	O	was
4	20	34	O	pharmacodynamic
5	37	41	O	there
6	43	45	O	was
7	47	48	O	no
8	50	59	O	alteration
9	61	62	O	in
10	64	66	O	the
11	68	83	O	pharmacokinetics
12	85	86	O	of
13	88	93	O	either
14	95	98	O	drug
NULL

Eszopiclone	273	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	15
Drugs That Inhibit CYP3A4 (Ketoconazole) CYP3A4 is a major metabolic pathway for elimination of eszopiclone.
1	0	4	O	Drugs
2	6	9	O	That
3	11	17	O	Inhibit
4	19	24	O	CYP3A4
5	27	38	O	Ketoconazole
6	41	46	O	CYP3A4
7	48	49	O	is
8	51	51	O	a
9	53	57	O	major
10	59	67	O	metabolic
11	69	75	O	pathway
12	77	79	O	for
13	81	91	O	elimination
14	93	94	O	of
15	96	106	O	XXXXXXXX
NULL

Eszopiclone	274	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	16
The exposure of eszopiclone was increased by coadministration of ketoconazole, a potent inhibitor of CYP3A4.
1	0	2	O	The
2	4	11	O	exposure
3	13	14	O	of
4	16	26	O	XXXXXXXX
5	28	30	O	was
6	32	40	O	increased
7	42	43	O	by
8	45	60	O	coadministration
9	62	63	O	of
10	65	76	B-K	ketoconazole
11	77	77	O	,
12	79	79	O	a
13	81	86	O	potent
14	88	96	B-K	inhibitor
15	98	99	I-K	of
16	101	106	I-K	CYP3A4
K/10:C54355 K/14:C54355

Eszopiclone	275	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	24
Other strong inhibitors of CYP3A4 (e.g., itraconazole, clarithromycin, nefazodone, troleandomycin, ritonavir, nelfinavir) would be expected to behave similarly.
1	0	4	O	Other
2	6	11	O	strong
3	13	22	B-U	inhibitors
4	24	25	I-U	of
5	27	32	I-U	CYP3A4
6	35	37	O	e.g
7	39	39	O	,
8	41	52	O	itraconazole
9	53	53	O	,
10	55	68	O	clarithromycin
11	69	69	O	,
12	71	80	O	nefazodone
13	81	81	O	,
14	83	96	O	troleandomycin
15	97	97	O	,
16	99	107	O	ritonavir
17	108	108	O	,
18	110	119	O	nelfinavir
19	122	126	O	would
20	128	129	O	be
21	131	138	O	expected
22	140	141	O	to
23	143	148	O	behave
24	150	158	O	similarly
NULL

Eszopiclone	276	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	15
Dose reduction of LUNESTA is needed for patient co-administered LUNESTA with potent CYP3A4 inhibitors.
1	0	3	B-T	Dose
2	5	13	I-T	reduction
3	15	16	O	of
4	18	24	O	LUNESTA
5	26	27	O	is
6	29	34	O	needed
7	36	38	O	for
8	40	46	O	patient
9	48	49	O	co
10	51	62	O	administered
11	64	70	O	LUNESTA
12	72	75	O	with
13	77	82	B-U	potent
14	84	89	I-U	CYP3A4
15	91	100	I-U	inhibitors
NULL

Eszopiclone	277	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	22
Drugs that Induce CYP3A4 (Rifampicin) Racemic zopiclone exposure was decreased 80% by concomitant use of rifampicin, a potent inducer of CYP3A4.
1	0	4	O	Drugs
2	6	9	B-K	that
3	11	16	O	Induce
4	18	23	O	CYP3A4
5	26	35	O	Rifampicin
6	38	44	O	Racemic
7	46	54	O	zopiclone
8	56	63	O	exposure
9	65	67	O	was
10	69	77	O	decreased
11	79	81	O	80%
12	83	84	O	by
13	86	96	O	concomitant
14	98	100	O	use
15	102	103	O	of
16	105	114	O	rifampicin
17	115	115	O	,
18	117	117	O	a
19	119	124	O	potent
20	126	132	B-K	inducer
21	134	135	I-K	of
22	137	142	I-K	CYP3A4
K/2:C54358 K/20:C54358

Eszopiclone	278	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	8
A similar effect would be expected with eszopiclone.
1	0	0	O	A
2	2	8	O	similar
3	10	15	O	effect
4	17	21	O	would
5	23	24	O	be
6	26	33	O	expected
7	35	38	O	with
8	40	50	O	XXXXXXXX
NULL

Eszopiclone	279	34073-7	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	13
Combination use with CYP3A4 inducer may decrease the exposure and effects of LUNESTA.
1	0	10	O	Combination
2	12	14	O	use
3	16	19	O	with
4	21	26	B-D	CYP3A4
5	28	34	I-D	inducer
6	36	38	O	may
7	40	47	B-T	decrease
8	49	51	I-T	the
9	53	60	I-T	exposure
10	62	64	O	and
11	66	72	O	effects
12	74	75	O	of
13	77	83	O	LUNESTA
NULL

Eszopiclone	280	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	39
The precise mechanism of action of eszopiclone as a hypnotic is unknown, but its effect is believed to result from its interaction with GABA-receptor complexes at binding domains located close to or allosterically coupled to benzodiazepine receptors.
1	0	2	O	The
2	4	10	O	precise
3	12	20	O	mechanism
4	22	23	O	of
5	25	30	O	action
6	32	33	O	of
7	35	45	O	XXXXXXXX
8	47	48	O	as
9	50	50	O	a
10	52	59	O	hypnotic
11	61	62	O	is
12	64	70	O	unknown
13	71	71	O	,
14	73	75	O	but
15	77	79	O	its
16	81	86	O	effect
17	88	89	O	is
18	91	98	O	believed
19	100	101	O	to
20	103	108	O	result
21	110	113	O	from
22	115	117	O	its
23	119	129	O	interaction
24	131	134	O	with
25	136	139	O	GABA
26	141	148	O	receptor
27	150	158	O	complexes
28	160	161	O	at
29	163	169	O	binding
30	171	177	O	domains
31	179	185	O	located
32	187	191	O	close
33	193	194	O	to
34	196	197	O	or
35	199	212	O	allosterically
36	214	220	O	coupled
37	222	223	O	to
38	225	238	O	benzodiazepine
39	240	248	O	receptors
NULL

Eszopiclone	281	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	35
Eszopiclone is a nonbenzodiazepine hypnotic that is a pyrrolopyrazine derivative of the cyclopyrrolone class with a chemical structure unrelated to pyrazolopyrimidines, imidazopyridines, benzodiazepines, barbiturates, or other drugs with known hypnotic properties.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	15	O	a
4	17	33	O	nonbenzodiazepine
5	35	42	O	hypnotic
6	44	47	O	that
7	49	50	O	is
8	52	52	O	a
9	54	68	O	pyrrolopyrazine
10	70	79	O	derivative
11	81	82	O	of
12	84	86	O	the
13	88	101	O	cyclopyrrolone
14	103	107	O	class
15	109	112	O	with
16	114	114	O	a
17	116	123	O	chemical
18	125	133	O	structure
19	135	143	O	unrelated
20	145	146	O	to
21	148	166	O	pyrazolopyrimidines
22	167	167	O	,
23	169	184	O	imidazopyridines
24	185	185	O	,
25	187	201	O	benzodiazepines
26	202	202	O	,
27	204	215	O	barbiturates
28	216	216	O	,
29	218	219	O	or
30	221	225	O	other
31	227	231	O	drugs
32	233	236	O	with
33	238	242	O	known
34	244	251	O	hypnotic
35	253	262	O	properties
NULL

Eszopiclone	282	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	22
The pharmacokinetics of eszopiclone have been investigated in healthy subjects (adult and elderly) and in patients with hepatic disease or renal disease.
1	0	2	O	The
2	4	19	O	pharmacokinetics
3	21	22	O	of
4	24	34	O	XXXXXXXX
5	36	39	O	have
6	41	44	O	been
7	46	57	O	investigated
8	59	60	O	in
9	62	68	O	healthy
10	70	77	O	subjects
11	80	84	O	adult
12	86	88	O	and
13	90	96	O	elderly
14	99	101	O	and
15	103	104	O	in
16	106	113	O	patients
17	115	118	O	with
18	120	126	O	hepatic
19	128	134	O	disease
20	136	137	O	or
21	139	143	O	renal
22	145	151	O	disease
NULL

Eszopiclone	283	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	33
In healthy subjects, the pharmacokinetic profile was examined after single doses of up to 7.5 mg and after once-daily administration of 1, 3, and 6 mg for 7 days.
1	0	1	O	In
2	3	9	O	healthy
3	11	18	O	subjects
4	19	19	O	,
5	21	23	O	the
6	25	39	O	pharmacokinetic
7	41	47	O	profile
8	49	51	O	was
9	53	60	O	examined
10	62	66	O	after
11	68	73	O	single
12	75	79	O	doses
13	81	82	O	of
14	84	85	O	up
15	87	88	O	to
16	90	92	O	7.5
17	94	95	O	mg
18	97	99	O	and
19	101	105	O	after
20	107	110	O	once
21	112	116	O	daily
22	118	131	O	administration
23	133	134	O	of
24	136	136	O	1
25	137	137	O	,
26	139	139	O	3
27	140	140	O	,
28	142	144	O	and
29	146	146	O	6
30	148	149	O	mg
31	151	153	O	for
32	155	155	O	7
33	157	160	O	days
NULL

Eszopiclone	284	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	30
Eszopiclone is rapidly absorbed, with a time to peak concentration (tmax) of approximately 1 hour and a terminal-phase elimination half-life (t1/2) of approximately 6 hours.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	21	O	rapidly
4	23	30	O	absorbed
5	31	31	O	,
6	33	36	O	with
7	38	38	O	a
8	40	43	O	time
9	45	46	O	to
10	48	51	O	peak
11	53	65	O	concentration
12	68	71	O	tmax
13	74	75	O	of
14	77	89	O	approximately
15	91	91	O	1
16	93	96	O	hour
17	98	100	O	and
18	102	102	O	a
19	104	111	O	terminal
20	113	117	O	phase
21	119	129	B-T	elimination
22	131	134	I-T	half
23	136	139	I-T	life
24	142	143	O	t1
25	144	144	O	/
26	145	145	O	2
27	148	149	O	of
28	151	163	O	approximately
29	165	165	O	6
30	167	171	O	hours
NULL

Eszopiclone	285	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	37
In healthy adults, LUNESTA does not accumulate with once-daily administration, and its exposure is dose-proportional over the range of 1 to 6 mg. Absorption and Distribution Eszopiclone is rapidly absorbed following oral administration.
1	0	1	O	In
2	3	9	O	healthy
3	11	16	O	adults
4	17	17	O	,
5	19	25	O	LUNESTA
6	27	30	O	does
7	32	34	O	not
8	36	45	O	accumulate
9	47	50	O	with
10	52	55	O	once
11	57	61	O	daily
12	63	76	O	administration
13	77	77	O	,
14	79	81	O	and
15	83	85	O	its
16	87	94	O	exposure
17	96	97	O	is
18	99	102	O	dose
19	104	115	O	proportional
20	117	120	O	over
21	122	124	O	the
22	126	130	O	range
23	132	133	O	of
24	135	135	O	1
25	137	138	O	to
26	140	140	O	6
27	142	143	O	mg
28	146	155	O	Absorption
29	157	159	O	and
30	161	172	O	Distribution
31	174	184	O	XXXXXXXX
32	186	187	O	is
33	189	195	O	rapidly
34	197	204	O	absorbed
35	206	214	O	following
36	216	219	O	oral
37	221	234	O	administration
NULL

Eszopiclone	286	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	12
Peak plasma concentrations are achieved within approximately 1 hour after oral administration.
1	0	3	O	Peak
2	5	10	O	plasma
3	12	25	O	concentrations
4	27	29	O	are
5	31	38	O	achieved
6	40	45	O	within
7	47	59	O	approximately
8	61	61	O	1
9	63	66	O	hour
10	68	72	O	after
11	74	77	O	oral
12	79	92	O	administration
NULL

Eszopiclone	287	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	9
Eszopiclone is weakly bound to plasma protein (52-59%).
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	20	O	weakly
4	22	26	O	bound
5	28	29	O	to
6	31	36	O	plasma
7	38	44	O	protein
8	47	48	O	52
9	50	52	O	59%
NULL

Eszopiclone	288	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	20
The large free fraction suggests that eszopiclone disposition should not be affected by drug-drug interactions caused by protein binding.
1	0	2	O	The
2	4	8	O	large
3	10	13	O	free
4	15	22	O	fraction
5	24	31	O	suggests
6	33	36	O	that
7	38	48	O	XXXXXXXX
8	50	60	O	disposition
9	62	67	O	should
10	69	71	O	not
11	73	74	O	be
12	76	83	O	affected
13	85	86	O	by
14	88	91	O	drug
15	93	96	O	drug
16	98	109	O	interactions
17	111	116	O	caused
18	118	119	O	by
19	121	127	O	protein
20	129	135	O	binding
NULL

Eszopiclone	289	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	20
The blood-to-plasma ratio for eszopiclone is less than one, indicating no selective uptake by red blood cells.
1	0	2	O	The
2	4	8	O	blood
3	10	11	O	to
4	13	18	O	plasma
5	20	24	O	ratio
6	26	28	O	for
7	30	40	O	XXXXXXXX
8	42	43	O	is
9	45	48	O	less
10	50	53	O	than
11	55	57	O	one
12	58	58	O	,
13	60	69	O	indicating
14	71	72	O	no
15	74	82	O	selective
16	84	89	O	uptake
17	91	92	O	by
18	94	96	O	red
19	98	102	O	blood
20	104	108	O	cells
NULL

Eszopiclone	290	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	13
Metabolism Following oral administration, eszopiclone is extensively metabolized by oxidation and demethylation.
1	0	9	O	Metabolism
2	11	19	O	Following
3	21	24	O	oral
4	26	39	O	administration
5	40	40	O	,
6	42	52	O	XXXXXXXX
7	54	55	O	is
8	57	67	O	extensively
9	69	79	O	metabolized
10	81	82	O	by
11	84	92	O	oxidation
12	94	96	O	and
13	98	110	O	demethylation
NULL

Eszopiclone	291	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	39
The primary plasma metabolites are (S)-zopiclone-N-oxide and (S)-N-desmethyl zopiclone; the latter compound binds to GABA receptors with substantially lower potency than eszopiclone, and the former compound shows no significant binding to this receptor.
1	0	2	O	The
2	4	10	O	primary
3	12	17	O	plasma
4	19	29	O	metabolites
5	31	33	O	are
6	36	36	O	S
7	39	47	O	zopiclone
8	49	49	O	N
9	51	55	O	oxide
10	57	59	O	and
11	62	62	O	S
12	65	65	O	N
13	67	75	O	desmethyl
14	77	85	O	zopiclone
15	88	90	O	the
16	92	97	O	latter
17	99	106	O	compound
18	108	112	O	binds
19	114	115	O	to
20	117	120	O	GABA
21	122	130	O	receptors
22	132	135	O	with
23	137	149	O	substantially
24	151	155	O	lower
25	157	163	O	potency
26	165	168	O	than
27	170	180	O	XXXXXXXX
28	181	181	O	,
29	183	185	O	and
30	187	189	O	the
31	191	196	O	former
32	198	205	O	compound
33	207	211	O	shows
34	213	214	O	no
35	216	226	O	significant
36	228	234	O	binding
37	236	237	O	to
38	239	242	O	this
39	244	251	O	receptor
NULL

Eszopiclone	292	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	17
In vitro studies have shown that CYP3A4 and CYP2E1 enzymes are involved in the metabolism of eszopiclone.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	20	O	have
5	22	26	O	shown
6	28	31	O	that
7	33	38	O	CYP3A4
8	40	42	O	and
9	44	49	O	CYP2E1
10	51	57	O	enzymes
11	59	61	O	are
12	63	70	O	involved
13	72	73	O	in
14	75	77	O	the
15	79	88	O	metabolism
16	90	91	O	of
17	93	103	O	XXXXXXXX
NULL

Eszopiclone	293	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	27
Eszopiclone did not show any inhibitory potential on CYP450 1A2, 2A6, 2C9, 2C19, 2D6, 2E1, and 3A4 in cryopreserved human hepatocytes.
1	0	10	O	XXXXXXXX
2	12	14	O	did
3	16	18	O	not
4	20	23	O	show
5	25	27	O	any
6	29	38	O	inhibitory
7	40	48	O	potential
8	50	51	O	on
9	53	58	O	CYP450
10	60	62	O	1A2
11	63	63	O	,
12	65	67	O	2A6
13	68	68	O	,
14	70	72	O	2C9
15	73	73	O	,
16	75	78	O	2C19
17	79	79	O	,
18	81	83	O	2D6
19	84	84	O	,
20	86	88	O	2E1
21	89	89	O	,
22	91	93	O	and
23	95	97	O	3A4
24	99	100	O	in
25	102	114	O	cryopreserved
26	116	120	O	human
27	122	132	O	hepatocytes
NULL

Eszopiclone	294	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	18
Elimination After oral administration, eszopiclone is eliminated with a mean t1/2 of approximately 6 hours.
1	0	10	O	Elimination
2	12	16	O	After
3	18	21	O	oral
4	23	36	O	administration
5	37	37	O	,
6	39	49	O	XXXXXXXX
7	51	52	O	is
8	54	63	O	eliminated
9	65	68	O	with
10	70	70	O	a
11	72	75	O	mean
12	77	78	O	t1
13	79	79	O	/
14	80	80	O	2
15	82	83	O	of
16	85	97	O	approximately
17	99	99	O	6
18	101	105	O	hours
NULL

Eszopiclone	295	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	19
Up to 75% of an oral dose of racemic zopiclone is excreted in the urine, primarily as metabolites.
1	0	1	O	Up
2	3	4	O	to
3	6	8	O	75%
4	10	11	O	of
5	13	14	O	an
6	16	19	O	oral
7	21	24	O	dose
8	26	27	O	of
9	29	35	O	racemic
10	37	45	O	zopiclone
11	47	48	O	is
12	50	57	O	excreted
13	59	60	O	in
14	62	64	O	the
15	66	70	O	urine
16	71	71	O	,
17	73	81	O	primarily
18	83	84	O	as
19	86	96	O	metabolites
NULL

Eszopiclone	296	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	16
A similar excretion profile would be expected for eszopiclone, the S-isomer of racemic zopiclone.
1	0	0	O	A
2	2	8	O	similar
3	10	18	O	excretion
4	20	26	O	profile
5	28	32	O	would
6	34	35	O	be
7	37	44	O	expected
8	46	48	O	for
9	50	60	O	XXXXXXXX
10	61	61	O	,
11	63	65	O	the
12	67	67	O	S
13	69	74	O	isomer
14	76	77	O	of
15	79	85	O	racemic
16	87	95	O	zopiclone
NULL

Eszopiclone	297	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	17
Less than 10% of the orally administered eszopiclone dose is excreted in the urine as parent drug.
1	0	3	O	Less
2	5	8	O	than
3	10	12	O	10%
4	14	15	O	of
5	17	19	O	the
6	21	26	O	orally
7	28	39	O	administered
8	41	51	O	XXXXXXXX
9	53	56	O	dose
10	58	59	O	is
11	61	68	O	excreted
12	70	71	O	in
13	73	75	O	the
14	77	81	O	urine
15	83	84	O	as
16	86	91	O	parent
17	93	96	O	drug
NULL

Eszopiclone	298	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	42
Effect of Food In healthy adults, administration of a 3 mg dose of eszopiclone after a high-fat meal resulted in no change in AUC, a reduction in mean Cmax of 21%, and delayed tmax by approximately 1 hour.
1	0	5	O	Effect
2	7	8	O	of
3	10	13	O	Food
4	15	16	O	In
5	18	24	O	healthy
6	26	31	O	adults
7	32	32	O	,
8	34	47	O	administration
9	49	50	O	of
10	52	52	O	a
11	54	54	O	3
12	56	57	O	mg
13	59	62	O	dose
14	64	65	O	of
15	67	77	O	XXXXXXXX
16	79	83	O	after
17	85	85	O	a
18	87	90	O	high
19	92	94	O	fat
20	96	99	O	meal
21	101	108	O	resulted
22	110	111	O	in
23	113	114	O	no
24	116	121	O	change
25	123	124	O	in
26	126	128	O	AUC
27	129	129	O	,
28	131	131	O	a
29	133	141	O	reduction
30	143	144	O	in
31	146	149	O	mean
32	151	154	O	Cmax
33	156	157	O	of
34	159	161	O	21%
35	162	162	O	,
36	164	166	O	and
37	168	174	O	delayed
38	176	179	O	tmax
39	181	182	O	by
40	184	196	O	approximately
41	198	198	O	1
42	200	203	O	hour
NULL

Eszopiclone	299	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	9
The half-life remained unchanged, approximately 6 hours.
1	0	2	O	The
2	4	7	O	half
3	9	12	O	life
4	14	21	O	remained
5	23	31	O	unchanged
6	32	32	O	,
7	34	46	O	approximately
8	48	48	O	6
9	50	54	O	hours
NULL

Eszopiclone	300	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	24
The effects of LUNESTA on sleep onset may be reduced if it is taken with or immediately after a high-fat/heavy meal.
1	0	2	O	The
2	4	10	O	effects
3	12	13	O	of
4	15	21	O	LUNESTA
5	23	24	O	on
6	26	30	O	sleep
7	32	36	O	onset
8	38	40	O	may
9	42	43	O	be
10	45	51	O	reduced
11	53	54	O	if
12	56	57	O	it
13	59	60	O	is
14	62	66	O	taken
15	68	71	O	with
16	73	74	O	or
17	76	86	O	immediately
18	88	92	O	after
19	94	94	O	a
20	96	99	O	high
21	101	103	O	fat
22	104	104	O	/
23	105	109	O	heavy
24	111	114	O	meal
NULL

Eszopiclone	301	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	35
Specific Populations Age Compared with non-elderly adults, subjects 65 years and older had an increase of 41% in total exposure (AUC) and a slightly prolonged elimination of eszopiclone (t1/2 approximately 9hours).
1	0	7	O	Specific
2	9	19	O	Populations
3	21	23	O	Age
4	25	32	O	Compared
5	34	37	O	with
6	39	41	O	non
7	43	49	O	elderly
8	51	56	O	adults
9	57	57	O	,
10	59	66	O	subjects
11	68	69	O	65
12	71	75	O	years
13	77	79	O	and
14	81	85	O	older
15	87	89	O	had
16	91	92	O	an
17	94	101	O	increase
18	103	104	O	of
19	106	108	O	41%
20	110	111	O	in
21	113	117	O	total
22	119	126	B-T	exposure
23	129	131	O	AUC
24	134	136	O	and
25	138	138	O	a
26	140	147	O	slightly
27	149	157	B-T	prolonged
28	159	169	I-T	elimination
29	171	172	O	of
30	174	184	O	XXXXXXXX
31	187	188	O	t1
32	189	189	O	/
33	190	190	O	2
34	192	204	O	approximately
35	206	211	O	9hours
NULL

Eszopiclone	302	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	12
Therefore, in elderly patients the dose should not exceed 2 mg.
1	0	8	O	Therefore
2	9	9	O	,
3	11	12	O	in
4	14	20	O	elderly
5	22	29	O	patients
6	31	33	O	the
7	35	38	O	dose
8	40	45	O	should
9	47	49	O	not
10	51	56	B-T	exceed
11	58	58	O	2
12	60	61	O	mg
NULL

Eszopiclone	303	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	11
Gender The pharmacokinetics of eszopiclone in men and women are similar.
1	0	5	O	Gender
2	7	9	O	The
3	11	26	O	pharmacokinetics
4	28	29	O	of
5	31	41	O	XXXXXXXX
6	43	44	O	in
7	46	48	O	men
8	50	52	O	and
9	54	58	O	women
10	60	62	O	are
11	64	70	O	similar
NULL

Eszopiclone	304	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	25
Race In an analysis of data on all subjects participating in Phase 1 studies of eszopiclone, the pharmacokinetics for all races studied appeared similar.
1	0	3	O	Race
2	5	6	O	In
3	8	9	O	an
4	11	18	O	analysis
5	20	21	O	of
6	23	26	O	data
7	28	29	O	on
8	31	33	O	all
9	35	42	O	subjects
10	44	56	O	participating
11	58	59	O	in
12	61	65	O	Phase
13	67	67	O	1
14	69	75	O	studies
15	77	78	O	of
16	80	90	O	XXXXXXXX
17	91	91	O	,
18	93	95	O	the
19	97	112	O	pharmacokinetics
20	114	116	O	for
21	118	120	O	all
22	122	126	O	races
23	128	134	O	studied
24	136	143	O	appeared
25	145	151	O	similar
NULL

Eszopiclone	305	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	27
Hepatic Impairment Pharmacokinetics of a 2 mg eszopiclone dose were assessed in 16 healthy volunteers and in 8subjects with mild, moderate, and severe liver disease.
1	0	6	O	Hepatic
2	8	17	O	Impairment
3	19	34	O	Pharmacokinetics
4	36	37	O	of
5	39	39	O	a
6	41	41	O	2
7	43	44	O	mg
8	46	56	O	XXXXXXXX
9	58	61	O	dose
10	63	66	O	were
11	68	75	O	assessed
12	77	78	O	in
13	80	81	O	16
14	83	89	O	healthy
15	91	100	O	volunteers
16	102	104	O	and
17	106	107	O	in
18	109	117	O	8subjects
19	119	122	O	with
20	124	127	O	mild
21	128	128	O	,
22	130	137	O	moderate
23	138	138	O	,
24	140	142	O	and
25	144	149	O	severe
26	151	155	O	liver
27	157	163	O	disease
NULL

Eszopiclone	306	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	14
Exposure was increased 2-fold in severely impaired patients compared with the healthy volunteers.
1	0	7	O	Exposure
2	9	11	O	was
3	13	21	O	increased
4	23	23	O	2
5	25	28	O	fold
6	30	31	O	in
7	33	40	O	severely
8	42	49	O	impaired
9	51	58	O	patients
10	60	67	O	compared
11	69	72	O	with
12	74	76	O	the
13	78	84	O	healthy
14	86	95	O	volunteers
NULL

Eszopiclone	307	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	13
No dose adjustment is necessary for patients with mild-to-moderate hepatic impairment.
1	0	1	O	No
2	3	6	O	dose
3	8	17	O	adjustment
4	19	20	O	is
5	22	30	O	necessary
6	32	34	O	for
7	36	43	O	patients
8	45	48	O	with
9	50	53	O	mild
10	55	56	O	to
11	58	65	O	moderate
12	67	73	O	hepatic
13	75	84	O	impairment
NULL

Eszopiclone	308	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	10
Dose reduction is recommended for patients with severe hepatic impairment.
1	0	3	B-T	Dose
2	5	13	I-T	reduction
3	15	16	O	is
4	18	28	O	recommended
5	30	32	O	for
6	34	41	O	patients
7	43	46	O	with
8	48	53	O	severe
9	55	61	O	hepatic
10	63	72	O	impairment
NULL

Eszopiclone	309	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	11
LUNESTA should be used with caution in patients with hepatic impairment.
1	0	6	O	LUNESTA
2	8	13	O	should
3	15	16	O	be
4	18	21	O	used
5	23	26	O	with
6	28	34	O	caution
7	36	37	O	in
8	39	46	O	patients
9	48	51	O	with
10	53	59	O	hepatic
11	61	70	O	impairment
NULL

Eszopiclone	310	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	20
Renal Impairment The pharmacokinetics of eszopiclone were studied in 24 patients with mild, moderate, or severe renal impairment.
1	0	4	O	Renal
2	6	15	O	Impairment
3	17	19	O	The
4	21	36	O	pharmacokinetics
5	38	39	O	of
6	41	51	O	XXXXXXXX
7	53	56	O	were
8	58	64	O	studied
9	66	67	O	in
10	69	70	O	24
11	72	79	O	patients
12	81	84	O	with
13	86	89	O	mild
14	90	90	O	,
15	92	99	O	moderate
16	100	100	O	,
17	102	103	O	or
18	105	110	O	severe
19	112	116	O	renal
20	118	127	O	impairment
NULL

Eszopiclone	311	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	15
AUC and Cmax were similar in the patients compared with demographically matched healthy control subjects.
1	0	2	O	AUC
2	4	6	O	and
3	8	11	O	Cmax
4	13	16	O	were
5	18	24	O	similar
6	26	27	O	in
7	29	31	O	the
8	33	40	O	patients
9	42	49	O	compared
10	51	54	O	with
11	56	70	O	demographically
12	72	78	O	matched
13	80	86	O	healthy
14	88	94	O	control
15	96	103	O	subjects
NULL

Eszopiclone	312	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	29
No dose adjustment is necessary in patients with renal impairment, since less than 10% of the orally administered eszopiclone dose is excreted in the urine as parent drug.
1	0	1	O	No
2	3	6	O	dose
3	8	17	O	adjustment
4	19	20	O	is
5	22	30	O	necessary
6	32	33	O	in
7	35	42	O	patients
8	44	47	O	with
9	49	53	O	renal
10	55	64	O	impairment
11	65	65	O	,
12	67	71	O	since
13	73	76	O	less
14	78	81	O	than
15	83	85	O	10%
16	87	88	O	of
17	90	92	O	the
18	94	99	O	orally
19	101	112	O	administered
20	114	124	O	XXXXXXXX
21	126	129	O	dose
22	131	132	O	is
23	134	141	O	excreted
24	143	144	O	in
25	146	148	O	the
26	150	154	O	urine
27	156	157	O	as
28	159	164	O	parent
29	166	169	O	drug
NULL

Eszopiclone	313	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	13
Drug Interactions Eszopiclone is metabolized by CYP3A4 and CYP2E1 via demethylation and oxidation.
1	0	3	O	Drug
2	5	16	O	Interactions
3	18	28	O	XXXXXXXX
4	30	31	O	is
5	33	43	O	metabolized
6	45	46	O	by
7	48	53	O	CYP3A4
8	55	57	O	and
9	59	64	O	CYP2E1
10	66	68	O	via
11	70	82	O	demethylation
12	84	86	O	and
13	88	96	O	oxidation
NULL

Eszopiclone	314	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	11
There were no pharmacokinetic or pharmacodynamic interactions between eszopiclone and paroxetine.
1	0	4	O	There
2	6	9	O	were
3	11	12	O	no
4	14	28	O	pharmacokinetic
5	30	31	O	or
6	33	47	O	pharmacodynamic
7	49	60	O	interactions
8	62	68	O	between
9	70	80	O	XXXXXXXX
10	82	84	O	and
11	86	95	O	paroxetine
NULL

Eszopiclone	315	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	31
When eszopiclone was coadministered with olanzapine, no pharmacokinetic interaction was detected in levels of eszopiclone or olanzapine, but a pharmacodynamic interaction was seen on a measure of psychomotor function.
1	0	3	O	When
2	5	15	O	XXXXXXXX
3	17	19	O	was
4	21	34	O	coadministered
5	36	39	O	with
6	41	50	B-D	olanzapine
7	51	51	O	,
8	53	54	O	no
9	56	70	O	pharmacokinetic
10	72	82	O	interaction
11	84	86	O	was
12	88	95	O	detected
13	97	98	O	in
14	100	105	O	levels
15	107	108	O	of
16	110	120	O	XXXXXXXX
17	122	123	O	or
18	125	134	O	olanzapine
19	135	135	O	,
20	137	139	O	but
21	141	141	O	a
22	143	157	O	pharmacodynamic
23	159	169	O	interaction
24	171	173	O	was
25	175	178	O	seen
26	180	181	O	on
27	183	183	O	a
28	185	191	O	measure
29	193	194	O	of
30	196	206	O	psychomotor
31	208	215	O	function
NULL

Eszopiclone	316	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	9
Eszopiclone and lorazepam decreased each other's Cmax by 22%.
1	0	10	O	XXXXXXXX
2	12	14	O	and
3	16	24	B-K	lorazepam
4	26	34	B-T	decreased
5	36	39	I-T	each
6	41	47	I-T	other's
7	49	52	I-T	Cmax
8	54	55	O	by
9	57	59	O	22%
K/3:C54602

Eszopiclone	317	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	32
Coadministration of eszopiclone 3 mg to subjects receiving ketoconazole, a potent inhibitor of CYP3A4, 400mg daily for 5 days, resulted in a 2.2-fold increase in exposure to eszopiclone.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	30	O	XXXXXXXX
4	32	32	O	3
5	34	35	O	mg
6	37	38	O	to
7	40	47	O	subjects
8	49	57	O	receiving
9	59	70	B-K	ketoconazole
10	71	71	O	,
11	73	73	O	a
12	75	80	B-K	potent
13	82	90	I-K	inhibitor
14	92	93	I-K	of
15	95	100	I-K	CYP3A4
16	101	101	O	,
17	103	107	O	400mg
18	109	113	O	daily
19	115	117	O	for
20	119	119	O	5
21	121	124	O	days
22	125	125	O	,
23	127	134	O	resulted
24	136	137	O	in
25	139	139	O	a
26	141	143	O	2.2
27	145	148	O	fold
28	150	157	B-T	increase
29	159	160	I-T	in
30	162	169	I-T	exposure
31	171	172	O	to
32	174	184	O	XXXXXXXX
K/9:C54355 K/12:C54355

Eszopiclone	318	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	14
Cmax and t1/2 were increased 1.4-fold and 1.3-fold, respectively.
1	0	3	O	Cmax
2	5	7	O	and
3	9	10	O	t1
4	11	11	O	/
5	12	12	O	2
6	14	17	O	were
7	19	27	O	increased
8	29	31	O	1.4
9	33	36	O	fold
10	38	40	O	and
11	42	44	O	1.3
12	46	49	O	fold
13	50	50	O	,
14	52	63	O	respectively
NULL

Eszopiclone	319	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	16
LUNESTA would not be expected to alter the clearance of drugs metabolized by common CYP450 enzymes.
1	0	6	O	LUNESTA
2	8	12	O	would
3	14	16	O	not
4	18	19	O	be
5	21	28	O	expected
6	30	31	O	to
7	33	37	O	alter
8	39	41	O	the
9	43	51	O	clearance
10	53	54	O	of
11	56	60	O	drugs
12	62	72	O	metabolized
13	74	75	O	by
14	77	82	O	common
15	84	89	O	CYP450
16	91	97	O	enzymes
NULL

Eszopiclone	320	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	16
Paroxetine: Coadministration of single dose of eszopiclone and paroxetine produced no pharmacokinetic or pharmacodynamic interaction.
1	0	9	O	Paroxetine
2	10	10	O	:
3	12	27	O	Coadministration
4	29	30	O	of
5	32	37	O	single
6	39	42	O	dose
7	44	45	O	of
8	47	57	O	XXXXXXXX
9	59	61	O	and
10	63	72	O	paroxetine
11	74	81	O	produced
12	83	84	O	no
13	86	100	O	pharmacokinetic
14	102	103	O	or
15	105	119	O	pharmacodynamic
16	121	131	O	interaction
NULL

Eszopiclone	321	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	23
The lack of a drug interaction following single-dose administration does not predict the complete absence of a pharmacodynamic effect following chronic administration.
1	0	2	O	The
2	4	7	O	lack
3	9	10	O	of
4	12	12	O	a
5	14	17	O	drug
6	19	29	O	interaction
7	31	39	O	following
8	41	46	O	single
9	48	51	O	dose
10	53	66	O	administration
11	68	71	O	does
12	73	75	O	not
13	77	83	O	predict
14	85	87	O	the
15	89	96	O	complete
16	98	104	O	absence
17	106	107	O	of
18	109	109	O	a
19	111	125	O	pharmacodynamic
20	127	132	O	effect
21	134	142	O	following
22	144	150	O	chronic
23	152	165	O	administration
NULL

Eszopiclone	322	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	24
Lorazepam: Coadministration of single doses of eszopiclone and lorazepam did not have clinically relevant effects on the pharmacodynamics or pharmacokinetics of either drug.
1	0	8	O	Lorazepam
2	9	9	O	:
3	11	26	O	Coadministration
4	28	29	O	of
5	31	36	O	single
6	38	42	O	doses
7	44	45	O	of
8	47	57	O	XXXXXXXX
9	59	61	O	and
10	63	71	O	lorazepam
11	73	75	O	did
12	77	79	O	not
13	81	84	O	have
14	86	95	O	clinically
15	97	104	O	relevant
16	106	112	O	effects
17	114	115	O	on
18	117	119	O	the
19	121	136	O	pharmacodynamics
20	138	139	O	or
21	141	156	O	pharmacokinetics
22	158	159	O	of
23	161	166	O	either
24	168	171	O	drug
NULL

Eszopiclone	324	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	45
Drugs with a Narrow Therapeutic Index Digoxin: A single dose of eszopiclone 3 mg did not affect the pharmacokinetics of digoxin measured at steady state following dosing of 0.5 mg twice daily for one day and 0.25 mg daily for the next 6 days.
1	0	4	O	Drugs
2	6	9	O	with
3	11	11	O	a
4	13	18	O	Narrow
5	20	30	O	Therapeutic
6	32	36	O	Index
7	38	44	O	Digoxin
8	45	45	O	:
9	47	47	O	A
10	49	54	O	single
11	56	59	O	dose
12	61	62	O	of
13	64	74	O	XXXXXXXX
14	76	76	O	3
15	78	79	O	mg
16	81	83	O	did
17	85	87	O	not
18	89	94	O	affect
19	96	98	O	the
20	100	115	O	pharmacokinetics
21	117	118	O	of
22	120	126	O	digoxin
23	128	135	O	measured
24	137	138	O	at
25	140	145	O	steady
26	147	151	O	state
27	153	161	O	following
28	163	168	O	dosing
29	170	171	O	of
30	173	175	O	0.5
31	177	178	O	mg
32	180	184	O	twice
33	186	190	O	daily
34	192	194	O	for
35	196	198	O	one
36	200	202	O	day
37	204	206	O	and
38	208	211	O	0.25
39	213	214	O	mg
40	216	220	O	daily
41	222	224	O	for
42	226	228	O	the
43	230	233	O	next
44	235	235	O	6
45	237	240	O	days
NULL

Eszopiclone	325	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	41
Warfarin: Eszopiclone 3 mg administered daily for 5 days did not affect the pharmacokinetics of (R)- or (S)-warfarin, nor were there any changes in the pharmacodynamic profile (prothrombin time) following a single 25 mg oral dose of warfarin.
1	0	7	O	Warfarin
2	8	8	O	:
3	10	20	O	XXXXXXXX
4	22	22	O	3
5	24	25	O	mg
6	27	38	O	administered
7	40	44	O	daily
8	46	48	O	for
9	50	50	O	5
10	52	55	O	days
11	57	59	O	did
12	61	63	O	not
13	65	70	O	affect
14	72	74	O	the
15	76	91	O	pharmacokinetics
16	93	94	O	of
17	97	97	O	R
18	101	102	O	or
19	105	105	O	S
20	108	115	O	warfarin
21	116	116	O	,
22	118	120	O	nor
23	122	125	O	were
24	127	131	O	there
25	133	135	O	any
26	137	143	O	changes
27	145	146	O	in
28	148	150	O	the
29	152	166	O	pharmacodynamic
30	168	174	O	profile
31	177	187	O	prothrombin
32	189	192	O	time
33	195	203	O	following
34	205	205	O	a
35	207	212	O	single
36	214	215	O	25
37	217	218	O	mg
38	220	223	O	oral
39	225	228	O	dose
40	230	231	O	of
41	233	240	O	warfarin
NULL

Eszopiclone	326	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	34
Drugs Highly Bound to Plasma Protein Eszopiclone is not highly bound to plasma proteins (52-59% bound); therefore, the disposition of eszopiclone is not expected to be sensitive to alterations in protein binding.
1	0	4	O	Drugs
2	6	11	O	Highly
3	13	17	O	Bound
4	19	20	O	to
5	22	27	O	Plasma
6	29	35	O	Protein
7	37	47	O	XXXXXXXX
8	49	50	O	is
9	52	54	O	not
10	56	61	O	highly
11	63	67	O	bound
12	69	70	O	to
13	72	77	O	plasma
14	79	86	O	proteins
15	89	90	O	52
16	92	94	O	59%
17	96	100	O	bound
18	104	112	O	therefore
19	113	113	O	,
20	115	117	O	the
21	119	129	O	disposition
22	131	132	O	of
23	134	144	O	XXXXXXXX
24	146	147	O	is
25	149	151	O	not
26	153	160	O	expected
27	162	163	O	to
28	165	166	O	be
29	168	176	O	sensitive
30	178	179	O	to
31	181	191	O	alterations
32	193	194	O	in
33	196	202	O	protein
34	204	210	O	binding
NULL

Eszopiclone	327	34090-1	39a5dae2-49f7-4662-9eac-aa7b4c7807a4	31
Administration of eszopiclone 3 mg to a patient taking another drug that is highly protein-bound would not be expected to cause an alteration in the free concentration of either drug.
1	0	13	O	Administration
2	15	16	O	of
3	18	28	O	XXXXXXXX
4	30	30	O	3
5	32	33	O	mg
6	35	36	O	to
7	38	38	O	a
8	40	46	O	patient
9	48	53	O	taking
10	55	61	O	another
11	63	66	O	drug
12	68	71	O	that
13	73	74	O	is
14	76	81	O	highly
15	83	89	O	protein
16	91	95	O	bound
17	97	101	O	would
18	103	105	O	not
19	107	108	O	be
20	110	117	O	expected
21	119	120	O	to
22	122	126	O	cause
23	128	129	O	an
24	131	140	O	alteration
25	142	143	O	in
26	145	147	O	the
27	149	152	O	free
28	154	166	O	concentration
29	168	169	O	of
30	171	176	O	either
31	178	181	O	drug
NULL

