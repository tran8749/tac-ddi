Latuda	3677	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	35
Table 34: Clinically Important Drug Interactions with LATUDA Strong CYP3A4 Inhibitors Clinical Impact: Concomitant use of LATUDA with strong CYP3A4 inhibitors increased the exposure of lurasidone compared to the use of LATUDA alone.
1	0	4	O	Table
2	6	7	O	34
3	8	8	O	:
4	10	19	O	Clinically
5	21	29	O	Important
6	31	34	O	Drug
7	36	47	O	Interactions
8	49	52	O	with
9	54	59	O	XXXXXXXX
10	61	66	O	Strong
11	68	73	O	CYP3A4
12	75	84	O	Inhibitors
13	86	93	O	Clinical
14	95	100	O	Impact
15	101	101	O	:
16	103	113	O	Concomitant
17	115	117	O	use
18	119	120	O	of
19	122	127	O	XXXXXXXX
20	129	132	O	with
21	134	139	B-K	strong
22	141	146	I-K	CYP3A4
23	148	157	I-K	inhibitors
24	159	167	B-T	increased
25	169	171	I-T	the
26	173	180	I-T	exposure
27	182	183	O	of
28	185	194	O	XXXXXXXX
29	196	203	O	compared
30	205	206	O	to
31	208	210	O	the
32	212	214	O	use
33	216	217	O	of
34	219	224	O	XXXXXXXX
35	226	230	O	alone
K/21:C54355

Latuda	3678	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	12
Intervention: LATUDA should not be used concomitantly with strong CYP3A4 inhibitors.
1	0	11	O	Intervention
2	12	12	O	:
3	14	19	O	XXXXXXXX
4	21	26	B-T	should
5	28	30	B-T	not
6	32	33	I-T	be
7	35	38	I-T	used
8	40	52	O	concomitantly
9	54	57	O	with
10	59	64	B-U	strong
11	66	71	I-U	CYP3A4
12	73	82	I-U	inhibitors
NULL

Latuda	3679	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	37
Examples: Ketoconazole, clarithromycin, ritonavir, voriconazole, mibefradil Moderate CYP3A4 Inhibitors Clinical Impact: Concomitant use of LATUDA with moderate CYP3A4 inhibitors increased the exposure of lurasidone compared to the use of LATUDA alone.
1	0	7	O	Examples
2	8	8	O	:
3	10	21	B-K	Ketoconazole
4	22	22	O	,
5	24	37	B-K	clarithromycin
6	38	38	O	,
7	40	48	O	ritonavir
8	49	49	O	,
9	51	62	O	voriconazole
10	63	63	O	,
11	65	74	B-K	mibefradil
12	76	83	O	Moderate
13	85	90	B-K	CYP3A4
14	92	101	O	Inhibitors
15	103	110	O	Clinical
16	112	117	O	Impact
17	118	118	O	:
18	120	130	O	Concomitant
19	132	134	O	use
20	136	137	O	of
21	139	144	O	XXXXXXXX
22	146	149	O	with
23	151	158	B-K	moderate
24	160	165	I-K	CYP3A4
25	167	176	I-K	inhibitors
26	178	186	B-T	increased
27	188	190	I-T	the
28	192	199	I-T	exposure
29	201	202	O	of
30	204	213	O	XXXXXXXX
31	215	222	O	compared
32	224	225	O	to
33	227	229	O	the
34	231	233	O	use
35	235	236	O	of
36	238	243	O	XXXXXXXX
37	245	249	O	alone
K/3:C54355 K/5:C54355 K/11:C54355 K/13:C54355 K/23:C54355

Latuda	3680	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	21
Intervention: LATUDA dose should be reduced to half of the original level when used concomitantly with moderate inhibitors of CYP3A4.
1	0	11	O	Intervention
2	12	12	O	:
3	14	19	O	XXXXXXXX
4	21	24	B-T	dose
5	26	31	O	should
6	33	34	B-T	be
7	36	42	B-T	reduced
8	44	45	I-T	to
9	47	50	I-T	half
10	52	53	O	of
11	55	57	B-T	the
12	59	66	I-T	original
13	68	72	I-T	level
14	74	77	O	when
15	79	82	O	used
16	84	96	O	concomitantly
17	98	101	O	with
18	103	110	B-U	moderate
19	112	121	I-U	inhibitors
20	123	124	I-U	of
21	126	131	I-U	CYP3A4
NULL

Latuda	3681	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	37
Examples: Diltiazem, atazanavir, erythromycin, fluconazole, verapamil Strong CYP3A4 Inducers Clinical Impact: Concomitant use of LATUDA with strong CYP3A4 inducers decreased the exposure of lurasidone compared to the use of LATUDA alone.
1	0	7	O	Examples
2	8	8	O	:
3	10	18	B-K	Diltiazem
4	19	19	O	,
5	21	30	O	atazanavir
6	31	31	O	,
7	33	44	B-K	erythromycin
8	45	45	O	,
9	47	57	O	fluconazole
10	58	58	O	,
11	60	68	O	verapamil
12	70	75	O	Strong
13	77	82	B-K	CYP3A4
14	84	91	O	Inducers
15	93	100	O	Clinical
16	102	107	B-T	Impact
17	108	108	I-T	:
18	110	120	O	Concomitant
19	122	124	O	use
20	126	127	O	of
21	129	134	O	XXXXXXXX
22	136	139	O	with
23	141	146	B-K	strong
24	148	153	I-K	CYP3A4
25	155	162	I-K	inducers
26	164	172	B-T	decreased
27	174	176	I-T	the
28	178	185	I-T	exposure
29	187	188	O	of
30	190	199	O	XXXXXXXX
31	201	208	O	compared
32	210	211	O	to
33	213	215	O	the
34	217	219	O	use
35	221	222	O	of
36	224	229	O	XXXXXXXX
37	231	235	O	alone
K/3:C54356 K/7:C54356 K/13:C54356 K/23:C54356

Latuda	3682	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	12
Intervention: LATUDA should not be used concomitantly with strong CYP3A4 inducers.
1	0	11	O	Intervention
2	12	12	O	:
3	14	19	O	XXXXXXXX
4	21	26	B-T	should
5	28	30	B-T	not
6	32	33	I-T	be
7	35	38	I-T	used
8	40	52	O	concomitantly
9	54	57	O	with
10	59	64	B-U	strong
11	66	71	I-U	CYP3A4
12	73	80	I-U	inducers
NULL

Latuda	3683	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	39
Examples: Rifampin, avasimibe, St. John's wort, phenytoin, carbamazepine Moderate CYP3A4 Inducers Clinical Impact: Concomitant use of LATUDA with moderate CYP3A4 inducers decreased the exposure of lurasidone compared to the use of LATUDA alone.
1	0	7	O	Examples
2	8	8	O	:
3	10	17	B-K	Rifampin
4	18	18	O	,
5	20	28	B-K	avasimibe
6	29	29	O	,
7	31	32	B-K	St
8	35	40	I-K	John's
9	42	45	O	wort
10	46	46	O	,
11	48	56	O	phenytoin
12	57	57	O	,
13	59	71	O	carbamazepine
14	73	80	O	Moderate
15	82	87	O	CYP3A4
16	89	96	O	Inducers
17	98	105	O	Clinical
18	107	112	O	Impact
19	113	113	O	:
20	115	125	O	Concomitant
21	127	129	O	use
22	131	132	O	of
23	134	139	O	XXXXXXXX
24	141	144	O	with
25	146	153	B-K	moderate
26	155	160	I-K	CYP3A4
27	162	169	I-K	inducers
28	171	179	B-T	decreased
29	181	183	I-T	the
30	185	192	I-T	exposure
31	194	195	O	of
32	197	206	O	XXXXXXXX
33	208	215	O	compared
34	217	218	O	to
35	220	222	O	the
36	224	226	O	use
37	228	229	O	of
38	231	236	O	XXXXXXXX
39	238	242	O	alone
K/3:C54356 K/5:C54356 K/7:C54356 K/25:C54356

Latuda	3684	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	15
Intervention: LATUDA dose should be increased when used concomitantly with moderate inducers of CYP3A4.
1	0	11	O	Intervention
2	12	12	O	:
3	14	19	O	XXXXXXXX
4	21	24	B-T	dose
5	26	31	I-T	should
6	33	34	I-T	be
7	36	44	I-T	increased
8	46	49	O	when
9	51	54	O	used
10	56	68	O	concomitantly
11	70	73	O	with
12	75	82	B-U	moderate
13	84	91	I-U	inducers
14	93	94	I-U	of
15	96	101	I-U	CYP3A4
NULL

Latuda	3685	34073-7	afad3051-9df2-4c54-9684-e8262a133af8	38
Examples: Bosentan, efavirenz, etravirine, modafinil, nafcillin Based on pharmacokinetic studies, no dosage adjustment of LATUDA is required when administered concomitantly with lithium, valproate, or substrates of P-gp or CYP3A4.
1	0	7	O	Examples
2	8	8	O	:
3	10	17	O	Bosentan
4	18	18	O	,
5	20	28	O	efavirenz
6	29	29	O	,
7	31	40	O	etravirine
8	41	41	O	,
9	43	51	O	modafinil
10	52	52	O	,
11	54	62	O	nafcillin
12	64	68	O	Based
13	70	71	O	on
14	73	87	O	pharmacokinetic
15	89	95	O	studies
16	96	96	O	,
17	98	99	O	no
18	101	106	O	dosage
19	108	117	O	adjustment
20	119	120	O	of
21	122	127	O	XXXXXXXX
22	129	130	O	is
23	132	139	O	required
24	141	144	O	when
25	146	157	O	administered
26	159	171	O	concomitantly
27	173	176	O	with
28	178	184	B-U	lithium
29	185	185	O	,
30	187	195	B-U	valproate
31	196	196	O	,
32	198	199	O	or
33	201	210	B-U	substrates
34	212	213	I-U	of
35	215	215	I-U	P
36	217	218	I-U	gp
37	220	221	O	or
38	223	228	B-U	CYP3A4
NULL

Latuda	3686	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	16
The mechanism of action of lurasidone in the treatment of schizophrenia and bipolar depression is unclear.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	22	O	action
5	24	25	O	of
6	27	36	O	XXXXXXXX
7	38	39	O	in
8	41	43	O	the
9	45	53	O	treatment
10	55	56	O	of
11	58	70	O	schizophrenia
12	72	74	O	and
13	76	82	O	bipolar
14	84	93	O	depression
15	95	96	O	is
16	98	104	O	unclear
NULL

Latuda	3687	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	26
However, its efficacy in schizophrenia and bipolar depression could be mediated through a combination of central dopamine D2 and serotonin Type 2 (5HT2A) receptor antagonism.
1	0	6	O	However
2	7	7	O	,
3	9	11	O	its
4	13	20	O	efficacy
5	22	23	O	in
6	25	37	O	schizophrenia
7	39	41	O	and
8	43	49	O	bipolar
9	51	60	O	depression
10	62	66	O	could
11	68	69	O	be
12	71	78	O	mediated
13	80	86	O	through
14	88	88	O	a
15	90	100	O	combination
16	102	103	O	of
17	105	111	O	central
18	113	120	O	dopamine
19	122	123	O	D2
20	125	127	O	and
21	129	137	O	serotonin
22	139	142	O	Type
23	144	144	O	2
24	147	151	O	5HT2A
25	154	161	O	receptor
26	163	172	O	antagonism
NULL

Latuda	3688	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	34
Lurasidone is an antagonist with high affinity binding at the dopamine D2 receptors (Ki of 1 nM) and the serotonin 5-HT2A (Ki of 0.5 nM) and 5-HT7 (Ki of 0.5 nM) receptors.
1	0	9	O	XXXXXXXX
2	11	12	O	is
3	14	15	O	an
4	17	26	O	antagonist
5	28	31	O	with
6	33	36	O	high
7	38	45	B-D	affinity
8	47	53	O	binding
9	55	56	O	at
10	58	60	O	the
11	62	69	B-D	dopamine
12	71	72	B-K	D2
13	74	82	O	receptors
14	85	86	O	Ki
15	88	89	O	of
16	91	91	O	1
17	93	94	O	nM
18	97	99	O	and
19	101	103	O	the
20	105	113	O	serotonin
21	115	115	O	5
22	117	120	O	HT2A
23	123	124	O	Ki
24	126	127	O	of
25	129	131	O	0.5
26	133	134	O	nM
27	137	139	O	and
28	141	141	O	5
29	143	145	O	HT7
30	148	149	B-K	Ki
31	151	152	O	of
32	154	156	O	0.5
33	158	159	O	nM
34	162	170	O	receptors
K/12:C54357 K/30:C54357

Latuda	3689	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	38
It also binds with moderate affinity to the human A2C adrenergic receptors (Ki=11 nM), is a partial agonist at serotonin 5-HT1A (Ki=6.4 nM) receptors, and is an antagonist at the A2A adrenergic receptors (Ki=41 nM).
1	0	1	O	It
2	3	6	O	also
3	8	12	O	binds
4	14	17	O	with
5	19	26	O	moderate
6	28	35	O	affinity
7	37	38	O	to
8	40	42	O	the
9	44	48	O	human
10	50	52	O	A2C
11	54	63	O	adrenergic
12	65	73	O	receptors
13	76	80	O	Ki=11
14	82	83	O	nM
15	85	85	O	,
16	87	88	O	is
17	90	90	O	a
18	92	98	O	partial
19	100	106	O	agonist
20	108	109	O	at
21	111	119	O	serotonin
22	121	121	O	5
23	123	126	O	HT1A
24	129	134	O	Ki=6.4
25	136	137	O	nM
26	140	148	O	receptors
27	149	149	O	,
28	151	153	O	and
29	155	156	O	is
30	158	159	O	an
31	161	170	O	antagonist
32	172	173	O	at
33	175	177	O	the
34	179	181	O	A2A
35	183	192	O	adrenergic
36	194	202	O	receptors
37	205	209	O	Ki=41
38	211	212	O	nM
NULL

Latuda	3690	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	17
Lurasidone exhibits little or no affinity for histamine H1 and muscarinic M1 receptors (IC50 > 1,000 nM).
1	0	9	O	XXXXXXXX
2	11	18	O	exhibits
3	20	25	O	little
4	27	28	O	or
5	30	31	O	no
6	33	40	O	affinity
7	42	44	O	for
8	46	54	O	histamine
9	56	57	O	H1
10	59	61	O	and
11	63	72	O	muscarinic
12	74	75	O	M1
13	77	85	O	receptors
14	88	91	O	IC50
15	93	93	O	>
16	95	99	O	1,000
17	101	102	O	nM
NULL

Latuda	3691	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	54
ECG Changes The effects of LATUDA on the QTc interval were evaluated in a randomized, double-blind, multiple-dose, parallel-dedicated thorough QT study in 43 patients with schizophrenia or schizoaffective disorder, who were treated with LATUDA doses of 120 mg daily, 600 mg daily and completed the study.
1	0	2	O	ECG
2	4	10	O	Changes
3	12	14	O	The
4	16	22	O	effects
5	24	25	O	of
6	27	32	O	XXXXXXXX
7	34	35	O	on
8	37	39	O	the
9	41	43	O	QTc
10	45	52	O	interval
11	54	57	O	were
12	59	67	O	evaluated
13	69	70	O	in
14	72	72	O	a
15	74	83	O	randomized
16	84	84	O	,
17	86	91	O	double
18	93	97	O	blind
19	98	98	O	,
20	100	107	O	multiple
21	109	112	O	dose
22	113	113	O	,
23	115	122	O	parallel
24	124	132	O	dedicated
25	134	141	O	thorough
26	143	144	O	QT
27	146	150	O	study
28	152	153	O	in
29	155	156	O	43
30	158	165	O	patients
31	167	170	O	with
32	172	184	O	schizophrenia
33	186	187	O	or
34	189	203	O	schizoaffective
35	205	212	O	disorder
36	213	213	O	,
37	215	217	O	who
38	219	222	O	were
39	224	230	O	treated
40	232	235	O	with
41	237	242	O	XXXXXXXX
42	244	248	O	doses
43	250	251	O	of
44	253	255	O	120
45	257	258	O	mg
46	260	264	O	daily
47	265	265	O	,
48	267	269	O	600
49	271	272	O	mg
50	274	278	O	daily
51	280	282	O	and
52	284	292	O	completed
53	294	296	O	the
54	298	302	O	study
NULL

Latuda	3692	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	49
The maximum mean (upper 1-sided, 95% CI) increase in baseline-adjusted QTc intervals based on individual correction method (QTcI) was 7.5 (11.7) ms and 4.6 (9.5) ms, for the 120 mg and 600 mg dose groups respectively, observed at 2 to 4 hours after dosing.
1	0	2	O	The
2	4	10	O	maximum
3	12	15	O	mean
4	18	22	O	upper
5	24	24	O	1
6	26	30	O	sided
7	31	31	O	,
8	33	35	O	95%
9	37	38	O	CI
10	41	48	O	increase
11	50	51	O	in
12	53	60	O	baseline
13	62	69	O	adjusted
14	71	73	O	QTc
15	75	83	O	intervals
16	85	89	O	based
17	91	92	O	on
18	94	103	O	individual
19	105	114	O	correction
20	116	121	O	method
21	124	127	O	QTcI
22	130	132	O	was
23	134	136	O	7.5
24	139	142	O	11.7
25	145	146	O	ms
26	148	150	O	and
27	152	154	O	4.6
28	157	159	O	9.5
29	162	163	O	ms
30	164	164	O	,
31	166	168	O	for
32	170	172	O	the
33	174	176	O	120
34	178	179	O	mg
35	181	183	O	and
36	185	187	O	600
37	189	190	O	mg
38	192	195	O	dose
39	197	202	O	groups
40	204	215	O	respectively
41	216	216	O	,
42	218	225	O	observed
43	227	228	O	at
44	230	230	O	2
45	232	233	O	to
46	235	235	O	4
47	237	241	O	hours
48	243	247	O	after
49	249	254	O	dosing
NULL

Latuda	3693	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	12
In this study, there was no apparent dose (exposure)-response relationship.
1	0	1	O	In
2	3	6	O	this
3	8	12	O	study
4	13	13	O	,
5	15	19	O	there
6	21	23	O	was
7	25	26	O	no
8	28	35	O	apparent
9	37	40	O	dose
10	43	50	O	exposure
11	53	60	O	response
12	62	73	O	relationship
NULL

Latuda	3694	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	30
In short-term, placebo-controlled studies in schizophrenia and bipolar depression, no post-baseline QT prolongations exceeding 500 msec were reported in patients treated with LATUDA or placebo.
1	0	1	O	In
2	3	7	O	short
3	9	12	O	term
4	13	13	O	,
5	15	21	O	placebo
6	23	32	O	controlled
7	34	40	O	studies
8	42	43	O	in
9	45	57	O	schizophrenia
10	59	61	O	and
11	63	69	O	bipolar
12	71	80	O	depression
13	81	81	O	,
14	83	84	O	no
15	86	89	O	post
16	91	98	O	baseline
17	100	101	O	QT
18	103	115	O	prolongations
19	117	125	O	exceeding
20	127	129	O	500
21	131	134	O	msec
22	136	139	O	were
23	141	148	O	reported
24	150	151	O	in
25	153	160	O	patients
26	162	168	O	treated
27	170	173	O	with
28	175	180	O	XXXXXXXX
29	182	183	O	or
30	185	191	O	placebo
NULL

Latuda	3695	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	12
Adults The activity of LATUDA is primarily due to the parent drug.
1	0	5	O	Adults
2	7	9	O	The
3	11	18	O	activity
4	20	21	O	of
5	23	28	O	XXXXXXXX
6	30	31	O	is
7	33	41	O	primarily
8	43	45	O	due
9	47	48	O	to
10	50	52	O	the
11	54	59	O	parent
12	61	64	O	drug
NULL

Latuda	3696	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	32
The pharmacokinetics of LATUDA is dose-proportional within a total daily dose range of 20 mg to 160 mg. Steady-state concentrations of LATUDA are reached within 7 days of starting LATUDA.
1	0	2	O	The
2	4	19	O	pharmacokinetics
3	21	22	O	of
4	24	29	O	XXXXXXXX
5	31	32	O	is
6	34	37	O	dose
7	39	50	O	proportional
8	52	57	O	within
9	59	59	O	a
10	61	65	O	total
11	67	71	O	daily
12	73	76	O	dose
13	78	82	O	range
14	84	85	O	of
15	87	88	O	20
16	90	91	O	mg
17	93	94	O	to
18	96	98	O	160
19	100	101	O	mg
20	104	109	O	Steady
21	111	115	O	state
22	117	130	O	concentrations
23	132	133	O	of
24	135	140	O	XXXXXXXX
25	142	144	O	are
26	146	152	O	reached
27	154	159	O	within
28	161	161	O	7
29	163	166	O	days
30	168	169	O	of
31	171	178	O	starting
32	180	185	O	XXXXXXXX
NULL

Latuda	3697	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	18
Following administration of 40 mg of LATUDA, the mean (%CV) elimination half-life was 18 (7) hours.
1	0	8	O	Following
2	10	23	O	administration
3	25	26	O	of
4	28	29	O	40
5	31	32	O	mg
6	34	35	O	of
7	37	42	O	XXXXXXXX
8	43	43	O	,
9	45	47	O	the
10	49	52	O	mean
11	55	57	O	%CV
12	60	70	B-T	elimination
13	72	75	I-T	half
14	77	80	I-T	life
15	82	84	O	was
16	86	87	O	18
17	90	90	O	7
18	93	97	O	hours
NULL

Latuda	3698	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	17
Absorption and Distribution: LATUDA is absorbed and reaches peak serum concentrations in approximately 1-3 hours.
1	0	9	O	Absorption
2	11	13	O	and
3	15	26	O	Distribution
4	27	27	O	:
5	29	34	O	XXXXXXXX
6	36	37	O	is
7	39	46	O	absorbed
8	48	50	O	and
9	52	58	O	reaches
10	60	63	O	peak
11	65	69	O	serum
12	71	84	O	concentrations
13	86	87	O	in
14	89	101	O	approximately
15	103	103	O	1
16	105	105	O	3
17	107	111	O	hours
NULL

Latuda	3699	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	12
It is estimated that 9-19% of an administered dose is absorbed.
1	0	1	O	It
2	3	4	O	is
3	6	14	O	estimated
4	16	19	O	that
5	21	21	O	9
6	23	25	O	19%
7	27	28	O	of
8	30	31	O	an
9	33	44	O	administered
10	46	49	O	dose
11	51	52	O	is
12	54	61	O	absorbed
NULL

Latuda	3700	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	27
Following administration of 40 mg of LATUDA, the mean (%CV) apparent volume of distribution was 6173 (17.2) L. LATUDA is highly bound (~99%) to serum proteins.
1	0	8	O	Following
2	10	23	O	administration
3	25	26	O	of
4	28	29	O	40
5	31	32	O	mg
6	34	35	O	of
7	37	42	O	XXXXXXXX
8	43	43	O	,
9	45	47	O	the
10	49	52	O	mean
11	55	57	O	%CV
12	60	67	O	apparent
13	69	74	O	volume
14	76	77	O	of
15	79	90	O	distribution
16	92	94	O	was
17	96	99	O	6173
18	102	105	O	17.2
19	108	108	O	L
20	111	116	O	XXXXXXXX
21	118	119	O	is
22	121	126	O	highly
23	128	132	O	bound
24	135	138	O	~99%
25	141	142	O	to
26	144	148	O	serum
27	150	157	O	proteins
NULL

Latuda	3701	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	33
In a food effect study, LATUDA mean Cmax and AUC were about 3-times and 2-times, respectively, when administered with food compared to the levels observed under fasting conditions.
1	0	1	O	In
2	3	3	O	a
3	5	8	O	food
4	10	15	O	effect
5	17	21	O	study
6	22	22	O	,
7	24	29	O	XXXXXXXX
8	31	34	O	mean
9	36	39	O	Cmax
10	41	43	O	and
11	45	47	O	AUC
12	49	52	O	were
13	54	58	O	about
14	60	60	O	3
15	62	66	O	times
16	68	70	O	and
17	72	72	O	2
18	74	78	O	times
19	79	79	O	,
20	81	92	O	respectively
21	93	93	O	,
22	95	98	O	when
23	100	111	O	administered
24	113	116	O	with
25	118	121	O	food
26	123	130	O	compared
27	132	133	O	to
28	135	137	O	the
29	139	144	O	levels
30	146	153	O	observed
31	155	159	O	under
32	161	167	O	fasting
33	169	178	O	conditions
NULL

Latuda	3702	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	22
LATUDA exposure was not affected as meal size was increased from 350 to 1000 calories and was independent of meal fat content.
1	0	5	O	XXXXXXXX
2	7	14	O	exposure
3	16	18	O	was
4	20	22	O	not
5	24	31	O	affected
6	33	34	O	as
7	36	39	O	meal
8	41	44	O	size
9	46	48	O	was
10	50	58	O	increased
11	60	63	O	from
12	65	67	O	350
13	69	70	O	to
14	72	75	O	1000
15	77	84	O	calories
16	86	88	O	and
17	90	92	O	was
18	94	104	O	independent
19	106	107	O	of
20	109	112	O	meal
21	114	116	O	fat
22	118	124	O	content
NULL

Latuda	3703	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	22
In clinical studies, establishing the safety and efficacy of LATUDA, patients were instructed to take their daily dose with food.
1	0	1	O	In
2	3	10	O	clinical
3	12	18	O	studies
4	19	19	O	,
5	21	32	O	establishing
6	34	36	O	the
7	38	43	O	safety
8	45	47	O	and
9	49	56	O	efficacy
10	58	59	O	of
11	61	66	O	XXXXXXXX
12	67	67	O	,
13	69	76	O	patients
14	78	81	O	were
15	83	92	O	instructed
16	94	95	O	to
17	97	100	O	take
18	102	106	O	their
19	108	112	O	daily
20	114	117	O	dose
21	119	122	O	with
22	124	127	O	food
NULL

Latuda	3704	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	10
Metabolism and Elimination: LATUDA is metabolized mainly via CYP3A4.
1	0	9	O	Metabolism
2	11	13	O	and
3	15	25	O	Elimination
4	26	26	O	:
5	28	33	O	XXXXXXXX
6	35	36	O	is
7	38	48	O	metabolized
8	50	55	O	mainly
9	57	59	O	via
10	61	66	O	CYP3A4
NULL

Latuda	3705	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	17
The major biotransformation pathways are oxidative N-dealkylation, hydroxylation of norbornane ring, and S-oxidation.
1	0	2	O	The
2	4	8	O	major
3	10	26	O	biotransformation
4	28	35	O	pathways
5	37	39	O	are
6	41	49	O	oxidative
7	51	51	O	N
8	53	64	O	dealkylation
9	65	65	O	,
10	67	79	O	hydroxylation
11	81	82	O	of
12	84	93	O	norbornane
13	95	98	O	ring
14	99	99	O	,
15	101	103	O	and
16	105	105	O	S
17	107	115	O	oxidation
NULL

Latuda	3706	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	23
LATUDA is metabolized into two active metabolites (ID-14283 and ID-14326) and two major non-active metabolites (ID-20219 and ID-20220).
1	0	5	O	XXXXXXXX
2	7	8	O	is
3	10	20	O	metabolized
4	22	25	O	into
5	27	29	O	two
6	31	36	O	active
7	38	48	O	metabolites
8	51	52	O	ID
9	54	58	O	14283
10	60	62	O	and
11	64	65	O	ID
12	67	71	O	14326
13	74	76	O	and
14	78	80	O	two
15	82	86	O	major
16	88	90	O	non
17	92	97	O	active
18	99	109	O	metabolites
19	112	113	O	ID
20	115	119	O	20219
21	121	123	O	and
22	125	126	O	ID
23	128	132	O	20220
NULL

Latuda	3707	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	32
Based on in vitro studies, LATUDA is not a substrate of CYP1A1, CYP1A2, CYP2A6, CYP4A11, CYP2B6, CYP2C8, CYP2C9, CYP2C19, CYP2D6 or CYP2E1 enzymes.
1	0	4	O	Based
2	6	7	O	on
3	9	10	O	in
4	12	16	O	vitro
5	18	24	O	studies
6	25	25	O	,
7	27	32	O	XXXXXXXX
8	34	35	O	is
9	37	39	O	not
10	41	41	O	a
11	43	51	O	substrate
12	53	54	O	of
13	56	61	O	CYP1A1
14	62	62	O	,
15	64	69	O	CYP1A2
16	70	70	O	,
17	72	77	O	CYP2A6
18	78	78	O	,
19	80	86	O	CYP4A11
20	87	87	O	,
21	89	94	O	CYP2B6
22	95	95	O	,
23	97	102	O	CYP2C8
24	103	103	O	,
25	105	110	O	CYP2C9
26	111	111	O	,
27	113	119	O	CYP2C19
28	120	120	O	,
29	122	127	O	CYP2D6
30	129	130	O	or
31	132	137	O	CYP2E1
32	139	145	O	enzymes
NULL

Latuda	3708	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	22
Because LATUDA is not a substrate for CYP1A2, smoking is not expected to have an effect on the pharmacokinetics of LATUDA.
1	0	6	O	Because
2	8	13	O	XXXXXXXX
3	15	16	O	is
4	18	20	O	not
5	22	22	O	a
6	24	32	O	substrate
7	34	36	O	for
8	38	43	O	CYP1A2
9	44	44	O	,
10	46	52	O	smoking
11	54	55	O	is
12	57	59	O	not
13	61	68	O	expected
14	70	71	O	to
15	73	76	O	have
16	78	79	O	an
17	81	86	O	effect
18	88	89	O	on
19	91	93	O	the
20	95	110	O	pharmacokinetics
21	112	113	O	of
22	115	120	O	XXXXXXXX
NULL

Latuda	3709	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	28
Transporter proteins: In vitro studies suggest LATUDA is not a substrate of OATP1B1 or OATP1B3, however, is probably a substrate of P-gp and BCRP.
1	0	10	O	Transporter
2	12	19	O	proteins
3	20	20	O	:
4	22	23	O	In
5	25	29	O	vitro
6	31	37	O	studies
7	39	45	O	suggest
8	47	52	O	XXXXXXXX
9	54	55	O	is
10	57	59	O	not
11	61	61	O	a
12	63	71	O	substrate
13	73	74	O	of
14	76	82	O	OATP1B1
15	84	85	O	or
16	87	93	O	OATP1B3
17	94	94	O	,
18	96	102	O	however
19	103	103	O	,
20	105	106	O	is
21	108	115	O	probably
22	117	117	O	a
23	119	127	O	substrate
24	129	130	O	of
25	132	132	O	P
26	134	135	O	gp
27	137	139	O	and
28	141	144	O	BCRP
NULL

Latuda	3710	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	34
In vitro studies indicate that LATUDA is not expected to inhibit transporters OATP1B1, OATP1B3, OCT1, OCT2, OAT1, OAT3, MATE1, MATE2-K and BSEP at clinically relevant concentrations.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	24	O	indicate
5	26	29	O	that
6	31	36	O	XXXXXXXX
7	38	39	O	is
8	41	43	O	not
9	45	52	O	expected
10	54	55	O	to
11	57	63	O	inhibit
12	65	76	O	transporters
13	78	84	O	OATP1B1
14	85	85	O	,
15	87	93	O	OATP1B3
16	94	94	O	,
17	96	99	O	OCT1
18	100	100	O	,
19	102	105	O	OCT2
20	106	106	O	,
21	108	111	O	OAT1
22	112	112	O	,
23	114	117	O	OAT3
24	118	118	O	,
25	120	124	O	MATE1
26	125	125	O	,
27	127	131	O	MATE2
28	133	133	O	K
29	135	137	O	and
30	139	142	O	BSEP
31	144	145	O	at
32	147	156	O	clinically
33	158	165	O	relevant
34	167	180	O	concentrations
NULL

Latuda	3711	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	10
LATUDA is not a clinically significant inhibitor of P-gp.
1	0	5	O	XXXXXXXX
2	7	8	O	is
3	10	12	O	not
4	14	14	O	a
5	16	25	O	clinically
6	27	37	O	significant
7	39	47	O	inhibitor
8	49	50	O	of
9	52	52	O	P
10	54	55	O	gp
NULL

Latuda	3712	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	33
Total excretion of radioactivity in urine and feces combined was approximately 89%, with about 80% recovered in feces and 9% recovered in urine, after a single dose of [14C]-labeled LATUDA.
1	0	4	O	Total
2	6	14	O	excretion
3	16	17	O	of
4	19	31	O	radioactivity
5	33	34	O	in
6	36	40	O	urine
7	42	44	O	and
8	46	50	O	feces
9	52	59	O	combined
10	61	63	O	was
11	65	77	O	approximately
12	79	81	O	89%
13	82	82	O	,
14	84	87	O	with
15	89	93	O	about
16	95	97	O	80%
17	99	107	O	recovered
18	109	110	O	in
19	112	116	O	feces
20	118	120	O	and
21	122	123	O	9%
22	125	133	O	recovered
23	135	136	O	in
24	138	142	O	urine
25	143	143	O	,
26	145	149	O	after
27	151	151	O	a
28	153	158	O	single
29	160	163	O	dose
30	165	166	O	of
31	169	171	O	14C
32	174	180	O	labeled
33	182	187	O	XXXXXXXX
NULL

Latuda	3713	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	19
Following administration of 40 mg of LATUDA, the mean (%CV) apparent clearance was 3902 (18.0) mL/min.
1	0	8	O	Following
2	10	23	O	administration
3	25	26	O	of
4	28	29	O	40
5	31	32	O	mg
6	34	35	O	of
7	37	42	O	XXXXXXXX
8	43	43	O	,
9	45	47	O	the
10	49	52	O	mean
11	55	57	O	%CV
12	60	67	O	apparent
13	69	77	O	clearance
14	79	81	O	was
15	83	86	O	3902
16	89	92	O	18.0
17	95	96	O	mL
18	97	97	O	/
19	98	100	O	min
NULL

Latuda	3714	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	18
Drug Interaction Studies: Effects of other drugs on the exposure of asenapine are summarized in Figure 1.
1	0	3	O	Drug
2	5	15	O	Interaction
3	17	23	O	Studies
4	24	24	O	:
5	26	32	O	Effects
6	34	35	O	of
7	37	41	O	other
8	43	47	O	drugs
9	49	50	O	on
10	52	54	O	the
11	56	63	O	exposure
12	65	66	O	of
13	68	76	B-D	asenapine
14	78	80	O	are
15	82	91	O	summarized
16	93	94	O	in
17	96	101	O	Figure
18	103	103	O	1
NULL

Latuda	3715	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	34
A population PK analyses concluded that coadministration of lithium 300-2400 mg/day or valproate 300-2000 mg/day with lurasidone for up to 6 weeks has minimal effect on lurasidone exposure.
1	0	0	O	A
2	2	11	O	population
3	13	14	O	PK
4	16	23	O	analyses
5	25	33	O	concluded
6	35	38	O	that
7	40	55	O	coadministration
8	57	58	O	of
9	60	66	O	lithium
10	68	70	O	300
11	72	75	O	2400
12	77	78	O	mg
13	79	79	O	/
14	80	82	O	day
15	84	85	O	or
16	87	95	O	valproate
17	97	99	O	300
18	101	104	O	2000
19	106	107	O	mg
20	108	108	O	/
21	109	111	O	day
22	113	116	O	with
23	118	127	O	XXXXXXXX
24	129	131	O	for
25	133	134	O	up
26	136	137	O	to
27	139	139	O	6
28	141	145	O	weeks
29	147	149	O	has
30	151	157	O	minimal
31	159	164	O	effect
32	166	167	O	on
33	169	178	O	XXXXXXXX
34	180	187	O	exposure
NULL

Latuda	3716	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	15
And the effects of Latuda on the exposures of other drugs are summarized in Figure2.
1	0	2	O	And
2	4	6	O	the
3	8	14	O	effects
4	16	17	O	of
5	19	24	O	XXXXXXXX
6	26	27	O	on
7	29	31	O	the
8	33	41	O	exposures
9	43	44	O	of
10	46	50	O	other
11	52	56	O	drugs
12	58	60	O	are
13	62	71	O	summarized
14	73	74	O	in
15	76	82	O	Figure2
NULL

Latuda	3717	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	35
A population PK analyses concluded that coadministration of lurasidone has minimal effect on lithium and valproate exposure when it is coadministered with lithium 300-2400 mg/day or valproate 300-2000 mg/day.
1	0	0	O	A
2	2	11	O	population
3	13	14	O	PK
4	16	23	O	analyses
5	25	33	O	concluded
6	35	38	O	that
7	40	55	O	coadministration
8	57	58	O	of
9	60	69	O	XXXXXXXX
10	71	73	O	has
11	75	81	O	minimal
12	83	88	O	effect
13	90	91	O	on
14	93	99	B-K	lithium
15	101	103	O	and
16	105	113	B-K	valproate
17	115	122	O	exposure
18	124	127	O	when
19	129	130	O	it
20	132	133	O	is
21	135	148	O	coadministered
22	150	153	O	with
23	155	161	O	lithium
24	163	165	O	300
25	167	170	O	2400
26	172	173	O	mg
27	174	174	O	/
28	175	177	O	day
29	179	180	O	or
30	182	190	O	valproate
31	192	194	O	300
32	196	199	O	2000
33	201	202	O	mg
34	203	203	O	/
35	204	206	O	day
K/14:C54357 K/16:C54357

Latuda	3718	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	9
Figure1: Impact of Other Drugs on LATUDA Pharmacokinetics
1	0	6	O	Figure1
2	7	7	O	:
3	9	14	O	Impact
4	16	17	O	of
5	19	23	O	Other
6	25	29	O	Drugs
7	31	32	O	on
8	34	39	O	XXXXXXXX
9	41	56	O	Pharmacokinetics
NULL

Latuda	3719	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	44
Pediatric Patients LATUDA exposure (i.e., steady-state Cmax and AUC) in children and adolescent patients (10 to 17 years of age) was generally similar to that in adults across the dose range from 40 to 160 mg, without adjusting for body weight.
1	0	8	O	Pediatric
2	10	17	O	Patients
3	19	24	O	XXXXXXXX
4	26	33	O	exposure
5	36	38	O	i.e
6	40	40	O	,
7	42	47	O	steady
8	49	53	O	state
9	55	58	O	Cmax
10	60	62	O	and
11	64	66	O	AUC
12	69	70	O	in
13	72	79	O	children
14	81	83	O	and
15	85	94	O	adolescent
16	96	103	O	patients
17	106	107	O	10
18	109	110	O	to
19	112	113	O	17
20	115	119	O	years
21	121	122	O	of
22	124	126	O	age
23	129	131	O	was
24	133	141	O	generally
25	143	149	O	similar
26	151	152	O	to
27	154	157	O	that
28	159	160	O	in
29	162	167	O	adults
30	169	174	O	across
31	176	178	O	the
32	180	183	O	dose
33	185	189	O	range
34	191	194	O	from
35	196	197	O	40
36	199	200	O	to
37	202	204	O	160
38	206	207	O	mg
39	208	208	O	,
40	210	216	O	without
41	218	226	O	adjusting
42	228	230	O	for
43	232	235	O	body
44	237	242	O	weight
NULL

Latuda	3720	34090-1	afad3051-9df2-4c54-9684-e8262a133af8	11
Figure 3: Impact of Other Patient Factors on Latuda Pharmacokinetics
1	0	5	O	Figure
2	7	7	O	3
3	8	8	O	:
4	10	15	O	Impact
5	17	18	O	of
6	20	24	O	Other
7	26	32	O	Patient
8	34	40	O	Factors
9	42	43	O	on
10	45	50	O	XXXXXXXX
11	52	67	O	Pharmacokinetics
NULL

