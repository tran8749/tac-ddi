Flurazepam Hydrochloride	3039	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	27
Benzodiazepines, including flurazepam, produce additive CNS depressant effects when co-administered with ethanol or other CNS depressants (e.g., psychotropic medications, anticonvulsants, antihistamines).
1	0	14	O	Benzodiazepines
2	15	15	O	,
3	17	25	O	including
4	27	36	O	XXXXXXXX
5	37	37	O	,
6	39	45	O	produce
7	47	54	B-E	additive
8	56	58	I-E	CNS
9	60	69	I-E	depressant
10	71	77	I-E	effects
11	79	82	O	when
12	84	85	O	co
13	87	98	O	administered
14	100	103	O	with
15	105	111	B-D	ethanol
16	113	114	O	or
17	116	120	O	other
18	122	124	B-D	CNS
19	126	136	I-D	depressants
20	139	141	O	e.g
21	143	143	O	,
22	145	156	B-D	psychotropic
23	158	168	I-D	medications
24	169	169	O	,
25	171	185	O	anticonvulsants
26	186	186	O	,
27	188	201	O	antihistamines
D/15:7:1 D/18:7:1 D/22:7:1

Flurazepam Hydrochloride	3040	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	18
Downward dose adjustment of flurazepam and/or concomitant CNS depressants may be necessary because of additive effects.
1	0	7	B-T	Downward
2	9	12	I-T	dose
3	14	23	I-T	adjustment
4	25	26	O	of
5	28	37	O	XXXXXXXX
6	39	41	O	and
7	42	42	O	/
8	43	44	O	or
9	46	56	O	concomitant
10	58	60	B-D	CNS
11	62	72	I-D	depressants
12	74	76	O	may
13	78	79	O	be
14	81	89	O	necessary
15	91	97	O	because
16	99	100	O	of
17	102	109	B-E	additive
18	111	117	I-E	effects
D/10:17:1

Flurazepam Hydrochloride	3041	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	13
*CNS Depressants: Downward dose adjustment may be necessary due to additive effects.
1	0	3	B-D	*CNS
2	5	15	I-D	Depressants
3	16	16	O	:
4	18	25	O	Downward
5	27	30	B-T	dose
6	32	41	O	adjustment
7	43	45	O	may
8	47	48	O	be
9	50	58	O	necessary
10	60	62	O	due
11	64	65	O	to
12	67	74	B-E	additive
13	76	82	I-E	effects
D/1:12:1

Flurazepam Hydrochloride	3042	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	16
Opioids: Limit dosage and duration, and follow patients closely for respiratory depression and sedation.
1	0	6	B-D	Opioids
2	7	7	O	:
3	9	13	B-T	Limit
4	15	20	I-T	dosage
5	22	24	O	and
6	26	33	O	duration
7	34	34	O	,
8	36	38	O	and
9	40	45	O	follow
10	47	54	O	patients
11	56	62	O	closely
12	64	66	O	for
13	68	78	B-E	respiratory
14	80	89	O	depression
15	91	93	O	and
16	95	102	O	sedation
D/1:13:1

Flurazepam Hydrochloride	3043	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	26
The concomitant use of benzodiazepines and opioids increases the risk of respiratory depression because of actions at different receptor sites in the CNS that control respiration.
1	0	2	O	The
2	4	14	O	concomitant
3	16	18	O	use
4	20	21	O	of
5	23	37	B-D	benzodiazepines
6	39	41	O	and
7	43	49	B-D	opioids
8	51	59	O	increases
9	61	63	O	the
10	65	68	O	risk
11	70	71	O	of
12	73	83	B-E	respiratory
13	85	94	I-E	depression
14	96	102	O	because
15	104	105	O	of
16	107	113	O	actions
17	115	116	O	at
18	118	126	O	different
19	128	135	O	receptor
20	137	141	B-E	sites
21	143	144	O	in
22	146	148	O	the
23	150	152	B-D	CNS
24	154	157	I-D	that
25	159	165	O	control
26	167	177	B-D	respiration
D/5:12:1 D/5:20:1 D/7:12:1 D/7:20:1 D/23:12:1 D/23:20:1 D/26:12:1 D/26:20:1

Flurazepam Hydrochloride	3044	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	13
Benzodiazepines interact at GABAA sites, and opioids interact primarily at mu receptors.
1	0	14	O	Benzodiazepines
2	16	23	O	interact
3	25	26	O	at
4	28	32	O	GABAA
5	34	38	O	sites
6	39	39	O	,
7	41	43	O	and
8	45	51	B-D	opioids
9	53	60	I-D	interact
10	62	70	I-D	primarily
11	72	73	O	at
12	75	76	B-D	mu
13	78	86	I-D	receptors
NULL

Flurazepam Hydrochloride	3045	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	19
When benzodiazepines and opioids are combined, the potential for benzodiazepines to significantly worsen opioid-related respiratory depression exists.
1	0	3	O	When
2	5	19	O	benzodiazepines
3	21	23	O	and
4	25	31	B-D	opioids
5	33	35	O	are
6	37	44	O	combined
7	45	45	O	,
8	47	49	O	the
9	51	59	O	potential
10	61	63	O	for
11	65	79	O	benzodiazepines
12	81	82	O	to
13	84	96	O	significantly
14	98	103	O	worsen
15	105	110	O	opioid
16	112	118	O	related
17	120	130	O	respiratory
18	132	141	B-E	depression
19	143	148	O	exists
D/4:18:1

Flurazepam Hydrochloride	3046	34073-7	2f2db2f5-49d3-4d47-a08a-628df49d2120	21
Limit dosage and duration of concomitant use of benzodiazepines and opioids, and follow patients closely for respiratory depression and sedation.
1	0	4	B-T	Limit
2	6	11	I-T	dosage
3	13	15	O	and
4	17	24	O	duration
5	26	27	O	of
6	29	39	O	concomitant
7	41	43	O	use
8	45	46	O	of
9	48	62	O	benzodiazepines
10	64	66	O	and
11	68	74	B-D	opioids
12	75	75	O	,
13	77	79	O	and
14	81	86	O	follow
15	88	95	O	patients
16	97	103	O	closely
17	105	107	O	for
18	109	119	B-E	respiratory
19	121	130	I-E	depression
20	132	134	I-E	and
21	136	143	B-E	sedation
D/11:18:1 D/11:21:1

Flurazepam Hydrochloride	3047	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	33
Flurazepam, like other central nervous system agents of the 1,4-benzodiazepine class, presumably exerts its effects by binding to stereo-specific receptors at several sites within the central nervous system (CNS).
1	0	9	O	XXXXXXXX
2	10	10	O	,
3	12	15	O	like
4	17	21	O	other
5	23	29	O	central
6	31	37	O	nervous
7	39	44	O	system
8	46	51	O	agents
9	53	54	O	of
10	56	58	O	the
11	60	62	O	1,4
12	64	77	O	benzodiazepine
13	79	83	O	class
14	84	84	O	,
15	86	95	O	presumably
16	97	102	O	exerts
17	104	106	O	its
18	108	114	O	effects
19	116	117	O	by
20	119	125	O	binding
21	127	128	O	to
22	130	135	O	stereo
23	137	144	O	specific
24	146	154	O	receptors
25	156	157	O	at
26	159	165	O	several
27	167	171	O	sites
28	173	178	O	within
29	180	182	O	the
30	184	190	O	central
31	192	198	O	nervous
32	200	205	O	system
33	208	210	O	CNS
NULL

Flurazepam Hydrochloride	3048	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	7
The exact mechanism of action is unknown.
1	0	2	O	The
2	4	8	O	exact
3	10	18	O	mechanism
4	20	21	O	of
5	23	28	O	action
6	30	31	O	is
7	33	39	O	unknown
NULL

Flurazepam Hydrochloride	3049	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	9
Flurazepam hydrochloride is rapidly absorbed from the gastro-intestinal tract.
1	0	23	O	XXXXXXXX
2	25	26	O	is
3	28	34	O	rapidly
4	36	43	O	absorbed
5	45	48	O	from
6	50	52	O	the
7	54	59	O	gastro
8	61	70	O	intestinal
9	72	76	O	tract
NULL

Flurazepam Hydrochloride	3050	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	11
Flurazepam is rapidly metabolized and is excreted primarily in the urine.
1	0	9	O	XXXXXXXX
2	11	12	O	is
3	14	20	O	rapidly
4	22	32	O	metabolized
5	34	36	O	and
6	38	39	O	is
7	41	48	O	excreted
8	50	58	O	primarily
9	60	61	O	in
10	63	65	O	the
11	67	71	O	urine
NULL

Flurazepam Hydrochloride	3051	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	26
Following a single oral dose, peak flurazepam plasma concentrations ranging from 0.5 to 4.0 ng/mL occur at 30 to 60 minutes post-dosing.
1	0	8	O	Following
2	10	10	O	a
3	12	17	O	single
4	19	22	O	oral
5	24	27	O	dose
6	28	28	O	,
7	30	33	O	peak
8	35	44	O	XXXXXXXX
9	46	51	O	plasma
10	53	66	O	concentrations
11	68	74	O	ranging
12	76	79	O	from
13	81	83	O	0.5
14	85	86	O	to
15	88	90	O	4.0
16	92	93	O	ng
17	94	94	O	/
18	95	96	O	mL
19	98	102	O	occur
20	104	105	O	at
21	107	108	O	30
22	110	111	O	to
23	113	114	O	60
24	116	122	O	minutes
25	124	127	O	post
26	129	134	O	dosing
NULL

Flurazepam Hydrochloride	3052	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	11
The harmonic mean apparent half-life of flurazepam is 2.3 hours.
1	0	2	O	The
2	4	11	O	harmonic
3	13	16	O	mean
4	18	25	O	apparent
5	27	30	O	half
6	32	35	O	life
7	37	38	O	of
8	40	49	O	XXXXXXXX
9	51	52	O	is
10	54	56	O	2.3
11	58	62	O	hours
NULL

Flurazepam Hydrochloride	3053	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	25
The blood level profile of flurazepam and its major metabolites was determined in man following the oral administration of 30 mg daily for 2 weeks.
1	0	2	O	The
2	4	8	O	blood
3	10	14	O	level
4	16	22	O	profile
5	24	25	O	of
6	27	36	O	XXXXXXXX
7	38	40	O	and
8	42	44	O	its
9	46	50	O	major
10	52	62	O	metabolites
11	64	66	O	was
12	68	77	O	determined
13	79	80	O	in
14	82	84	O	man
15	86	94	O	following
16	96	98	O	the
17	100	103	O	oral
18	105	118	O	administration
19	120	121	O	of
20	123	124	O	30
21	126	127	O	mg
22	129	133	O	daily
23	135	137	O	for
24	139	139	O	2
25	141	145	O	weeks
NULL

Flurazepam Hydrochloride	3054	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	23
The N1-hydroxyethyl-flurazepam was measurable only during the early hours after a 30 mg dose and was not detectable after 24 hours.
1	0	2	O	The
2	4	5	O	N1
3	7	18	O	hydroxyethyl
4	20	29	O	XXXXXXXX
5	31	33	O	was
6	35	44	O	measurable
7	46	49	O	only
8	51	56	O	during
9	58	60	O	the
10	62	66	O	early
11	68	72	O	hours
12	74	78	O	after
13	80	80	O	a
14	82	83	O	30
15	85	86	O	mg
16	88	91	O	dose
17	93	95	O	and
18	97	99	O	was
19	101	103	O	not
20	105	114	O	detectable
21	116	120	O	after
22	122	123	O	24
23	125	129	O	hours
NULL

Flurazepam Hydrochloride	3055	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	41
The major metabolite in blood was N1-desalkyl-flurazepam, which reached steady-state (plateau) levels after 7 to 10 days of dosing, at levels approximately 5- to 6-fold greater than the 24-hour levels observed on Day 1.
1	0	2	O	The
2	4	8	O	major
3	10	19	O	metabolite
4	21	22	O	in
5	24	28	O	blood
6	30	32	O	was
7	34	35	O	N1
8	37	44	O	desalkyl
9	46	55	O	XXXXXXXX
10	56	56	O	,
11	58	62	O	which
12	64	70	O	reached
13	72	77	O	steady
14	79	83	O	state
15	86	92	O	plateau
16	95	100	B-T	levels
17	102	106	O	after
18	108	108	O	7
19	110	111	O	to
20	113	114	O	10
21	116	119	O	days
22	121	122	O	of
23	124	129	O	dosing
24	130	130	O	,
25	132	133	O	at
26	135	140	O	levels
27	142	154	O	approximately
28	156	156	O	5
29	159	160	O	to
30	162	162	O	6
31	164	167	O	fold
32	169	175	O	greater
33	177	180	O	than
34	182	184	O	the
35	186	187	O	24
36	189	192	O	hour
37	194	199	O	levels
38	201	208	O	observed
39	210	211	O	on
40	213	215	O	Day
41	217	217	O	1
NULL

Flurazepam Hydrochloride	3056	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	15
The half-life of elimination of N1-desalkyl-flurazepam ranged from 47 to 100 hours.
1	0	2	O	The
2	4	7	O	half
3	9	12	B-T	life
4	14	15	O	of
5	17	27	O	elimination
6	29	30	O	of
7	32	33	B-K	N1
8	35	42	O	desalkyl
9	44	53	O	XXXXXXXX
10	55	60	O	ranged
11	62	65	O	from
12	67	68	O	47
13	70	71	O	to
14	73	75	O	100
15	77	81	O	hours
K/7:C54611

Flurazepam Hydrochloride	3057	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	18
The major urinary metabolite is conjugated N1-hydroxyethyl-flurazepam which accounts for 22% to 55% of the dose.
1	0	2	O	The
2	4	8	O	major
3	10	16	O	urinary
4	18	27	O	metabolite
5	29	30	O	is
6	32	41	O	conjugated
7	43	44	O	N1
8	46	57	O	hydroxyethyl
9	59	68	O	XXXXXXXX
10	70	74	O	which
11	76	83	O	accounts
12	85	87	O	for
13	89	91	O	22%
14	93	94	O	to
15	96	98	O	55%
16	100	101	O	of
17	103	105	O	the
18	107	110	O	dose
NULL

Flurazepam Hydrochloride	3058	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	15
Less than 1% of the dose is excreted in the urine as N1-desalkyl-flurazepam.
1	0	3	O	Less
2	5	8	O	than
3	10	11	O	1%
4	13	14	O	of
5	16	18	O	the
6	20	23	O	dose
7	25	26	O	is
8	28	35	O	excreted
9	37	38	O	in
10	40	42	O	the
11	44	48	O	urine
12	50	51	O	as
13	53	54	O	N1
14	56	63	O	desalkyl
15	65	74	O	XXXXXXXX
NULL

Flurazepam Hydrochloride	3059	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	47
This pharmacokinetic profile may be responsible for the clinical observation that flurazepam is increasingly effective on the second or third night of consecutive use and that for 1 or 2 nights after the drug is discontinued both sleep latency and total wake time may still be decreased.
1	0	3	O	This
2	5	19	O	pharmacokinetic
3	21	27	O	profile
4	29	31	O	may
5	33	34	O	be
6	36	46	O	responsible
7	48	50	O	for
8	52	54	O	the
9	56	63	O	clinical
10	65	75	O	observation
11	77	80	O	that
12	82	91	O	XXXXXXXX
13	93	94	O	is
14	96	107	O	increasingly
15	109	117	O	effective
16	119	120	O	on
17	122	124	O	the
18	126	131	O	second
19	133	134	O	or
20	136	140	O	third
21	142	146	O	night
22	148	149	O	of
23	151	161	O	consecutive
24	163	165	O	use
25	167	169	O	and
26	171	174	O	that
27	176	178	O	for
28	180	180	O	1
29	182	183	O	or
30	185	185	O	2
31	187	192	O	nights
32	194	198	O	after
33	200	202	O	the
34	204	207	O	drug
35	209	210	O	is
36	212	223	O	discontinued
37	225	228	O	both
38	230	234	O	sleep
39	236	242	O	latency
40	244	246	O	and
41	248	252	O	total
42	254	257	O	wake
43	259	262	O	time
44	264	266	O	may
45	268	272	O	still
46	274	275	O	be
47	277	285	O	decreased
NULL

Flurazepam Hydrochloride	3060	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	18
The single dose pharmacokinetics of flurazepam were studied in 12 healthy geriatric subjects (aged 61 to 85 years).
1	0	2	O	The
2	4	9	O	single
3	11	14	O	dose
4	16	31	O	pharmacokinetics
5	33	34	O	of
6	36	45	O	XXXXXXXX
7	47	50	O	were
8	52	58	O	studied
9	60	61	O	in
10	63	64	O	12
11	66	72	O	healthy
12	74	82	O	geriatric
13	84	91	O	subjects
14	94	97	O	aged
15	99	100	O	61
16	102	103	O	to
17	105	106	O	85
18	108	112	O	years
NULL

Flurazepam Hydrochloride	3061	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	43
The mean elimination half-life of desalkyl-flurazepam was longer in elderly male subjects (160 hours) compared with younger male subjects (74 hours), while mean elimination half-life was similar in geriatric female subjects (120 hours) and younger female subjects (90 hours).
1	0	2	O	The
2	4	7	O	mean
3	9	19	O	elimination
4	21	24	O	half
5	26	29	O	life
6	31	32	O	of
7	34	41	O	desalkyl
8	43	52	O	XXXXXXXX
9	54	56	O	was
10	58	63	O	longer
11	65	66	O	in
12	68	74	O	elderly
13	76	79	O	male
14	81	88	O	subjects
15	91	93	O	160
16	95	99	O	hours
17	102	109	O	compared
18	111	114	O	with
19	116	122	O	younger
20	124	127	O	male
21	129	136	O	subjects
22	139	140	O	74
23	142	146	O	hours
24	148	148	O	,
25	150	154	O	while
26	156	159	O	mean
27	161	171	O	elimination
28	173	176	O	half
29	178	181	O	life
30	183	185	O	was
31	187	193	O	similar
32	195	196	O	in
33	198	206	O	geriatric
34	208	213	O	female
35	215	222	O	subjects
36	225	227	O	120
37	229	233	O	hours
38	236	238	O	and
39	240	246	O	younger
40	248	253	O	female
41	255	262	O	subjects
42	265	266	O	90
43	268	272	O	hours
NULL

Flurazepam Hydrochloride	3062	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	52
After multiple dosing, mean steady-state plasma levels of desalkyl-flurazepam were higher in elderly male subjects (81 ng/mL) compared with younger male subjects (53 ng/mL), while values were similar between elderly female subjects (85 ng/mL) and younger female subjects (86 ng/mL).
1	0	4	O	After
2	6	13	O	multiple
3	15	20	O	dosing
4	21	21	O	,
5	23	26	O	mean
6	28	33	O	steady
7	35	39	O	state
8	41	46	O	plasma
9	48	53	O	levels
10	55	56	O	of
11	58	65	O	desalkyl
12	67	76	O	XXXXXXXX
13	78	81	O	were
14	83	88	O	higher
15	90	91	O	in
16	93	99	O	elderly
17	101	104	O	male
18	106	113	O	subjects
19	116	117	O	81
20	119	120	O	ng
21	121	121	O	/
22	122	123	O	mL
23	126	133	O	compared
24	135	138	O	with
25	140	146	O	younger
26	148	151	O	male
27	153	160	O	subjects
28	163	164	O	53
29	166	167	O	ng
30	168	168	O	/
31	169	170	O	mL
32	172	172	O	,
33	174	178	O	while
34	180	185	O	values
35	187	190	O	were
36	192	198	O	similar
37	200	206	O	between
38	208	214	O	elderly
39	216	221	O	female
40	223	230	O	subjects
41	233	234	O	85
42	236	237	O	ng
43	238	238	O	/
44	239	240	O	mL
45	243	245	O	and
46	247	253	O	younger
47	255	260	O	female
48	262	269	O	subjects
49	272	273	O	86
50	275	276	O	ng
51	277	277	O	/
52	278	279	O	mL
NULL

Flurazepam Hydrochloride	3063	34090-1	2f2db2f5-49d3-4d47-a08a-628df49d2120	62
The mean washout half-life of desalkyl-flurazepam was longer in elderly male and female subjects (126 and 158 hours, respectively) compared with younger male and female subjects (111 and 113 hours, respectively).1 1 Greenblatt DJ, Divoll M, Hammatz JS, MacLauglin DS, Shader RI: Kinetics and clinical effects of flurazepam in young and elderly noninsomniacs.
1	0	2	O	The
2	4	7	O	mean
3	9	15	O	washout
4	17	20	O	half
5	22	25	O	life
6	27	28	O	of
7	30	37	O	desalkyl
8	39	48	O	XXXXXXXX
9	50	52	O	was
10	54	59	O	longer
11	61	62	O	in
12	64	70	O	elderly
13	72	75	O	male
14	77	79	O	and
15	81	86	O	female
16	88	95	O	subjects
17	98	100	O	126
18	102	104	O	and
19	106	108	O	158
20	110	114	O	hours
21	115	115	O	,
22	117	128	O	respectively
23	131	138	O	compared
24	140	143	O	with
25	145	151	O	younger
26	153	156	O	male
27	158	160	O	and
28	162	167	O	female
29	169	176	O	subjects
30	179	181	O	111
31	183	185	O	and
32	187	189	O	113
33	191	195	O	hours
34	196	196	O	,
35	198	212	O	respectively).1
36	214	214	O	1
37	216	225	O	Greenblatt
38	227	228	O	DJ
39	229	229	O	,
40	231	236	O	Divoll
41	238	238	O	M
42	239	239	O	,
43	241	247	O	Hammatz
44	249	250	O	JS
45	251	251	O	,
46	253	262	O	MacLauglin
47	264	265	O	DS
48	266	266	O	,
49	268	273	O	Shader
50	275	276	O	RI
51	277	277	O	:
52	279	286	B-E	Kinetics
53	288	290	O	and
54	292	299	O	clinical
55	301	307	B-E	effects
56	309	310	O	of
57	312	321	O	XXXXXXXX
58	323	324	O	in
59	326	330	O	young
60	332	334	O	and
61	336	342	O	elderly
62	344	356	O	noninsomniacs
NULL

