<?xml version="1.0" ?>
<Label drug="ONDANSETRON" setid="d89017aa-b8c3-4fd2-8e68-44e2cfd6f290">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS Apomorphine – profound hypotension and loss of consciousness. Concomitant use with ondansetron is contraindicated. ( 7.2 ) 7.1 Drugs Affecting Cytochrome P-450 Enzymes Ondansetron does not appear to induce or inhibit the cytochrome P-450 drug-metabolizing enzyme system of the liver. Because ondansetron is metabolized by hepatic cytochrome P-450 drug-metabolizing enzymes (CYP3A4, CYP2D6, CYP1A2), inducers or inhibitors of these enzymes may change the clearance and, hence, the half-life of ondansetron [see Clinical Pharmacology ( 12.3 )] . On the basis of limited available data, no dosage adjustment is recommended for patients on these drugs. 7.2 Apomorphine Based on reports of profound hypotension and loss of consciousness when apomorphine was administered with ondansetron, concomitant use of apomorphine with ondansetron is contraindicated [see Contraindications ( 4 )].  7.3 Phenytoin, Carbamazepine, and Rifampin In patients treated with potent inducers of CYP3A4 (i.e., phenytoin, carbamazepine, and rifampin), the clearance of ondansetron was significantly increased and ondansetron blood concentrations were decreased. However, on the basis of available data, no dosage adjustment for ondansetron is recommended for patients on these drugs [see Clinical Pharmacology ( 12.3 )] . 7.4 Tramadol Although there are no data on pharmacokinetic drug interactions between ondansetron and tramadol, data from two small studies indicate that concomitant use of ondansetron may result in reduced analgesic activity of tramadol. Patients on concomitant ondansetron self administered tramadol more frequently in these studies, leading to an increased cumulative dose in patient controlled administration (PCA) of tramadol. 7.5 Serotonergic Drugs Serotonin syndrome (including altered mental status, autonomic instability, and neuromuscular symptoms) has been described following the concomitant use of 5-HT3 receptor antagonists and other serotonergic drugs, including selective serotonin reuptake inhibitors (SSRIs) and serotonin and noradrenaline reuptake inhibitors (SNRIs) [see Warnings and Precautions ( 5.3 )] . 7.6 Chemotherapy In humans, carmustine, etoposide, and cisplatin do not affect the pharmacokinetics of ondansetron. In a crossover study in 76 pediatric patients, intravenous ondansetron did not increase blood levels of high-dose methotrexate. 7.7 Temazepam The coadministration of ondansetron had no effect on the pharmacokinetics and pharmacodynamics of temazepam. 7.8 Alfentanil and Atracurium Ondansetron does not alter the respiratory depressant effects produced by alfentanil or the degree of neuromuscular blockade produced by atracurium. Interactions with general or local anesthetics have not been studied.</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY 12.1 Mechanism of Action Ondansetron is a selective 5-HT 3 receptor antagonist. While ondansetron's mechanism of action has not been fully characterized, it is not a dopamine-receptor antagonist. 12.2 Pharmacodynamics QTc interval prolongation was studied in a double blind, single intravenous dose, placebo- and positive-controlled, crossover study in 58 healthy subjects. The maximum mean (95% upper confidence bound) difference in QTcF from placebo after baseline-correction was 19.5 (21.8) ms and 5.6 (7.4) ms after 15 minute intravenous infusions of 32 mg and 8 mg Ondansetron, respectively. A significant exposure-response relationship was identified between ondansetron concentration and ΔΔQTcF. Using the established exposure-response relationship, 24 mg infused intravenously over 15 min had a mean predicted (95% upper prediction interval) ΔΔQTcF of 14.0 (16.3) ms. In contrast, 16 mg infused intravenously over 15 min using the same model had a mean predicted (95% upper prediction interval) ΔΔQTcF of 9.1 (11.2) ms. In normal volunteers, single intravenous doses of 0.15 mg/kg of ondansetron had no effect on esophageal motility, gastric motility, lower esophageal sphincter pressure, or small intestinal transit time. In another study in six normal male volunteers, a 16-mg dose infused over 5 minutes showed no effect of the drug on cardiac output, heart rate, stroke volume, blood pressure, or electrocardiogram (ECG). Multiday administration of ondansetron has been shown to slow colonic transit in normal volunteers. Ondansetron has no effect on plasma prolactin concentrations. In a gender-balanced pharmacodynamic study (n = 56), ondansetron 4 mg administered intravenously or intramuscularly was dynamically similar in the prevention of nausea and vomiting using the ipecacuanha model of emesis. 12.3 Pharmacokinetics In normal adult volunteers, the following mean pharmacokinetic data have been determined following a single 0.15-mg/kg intravenous dose. Table 3. Pharmacokinetics in Normal Adult Volunteers Age-group (years) n Peak Plasma Concentration (ng/mL) Mean Elimination Half-life (h) Plasma Clearance (L/h/kg) 19-40 11 102 3.5 0.381 61-74 12 106 4.7 0.319 ≥ 75 11 170 5.5 0.262 Absorption : A study was performed in normal volunteers (n = 56) to evaluate the pharmacokinetics of a single 4-mg dose administered as a 5-minute infusion compared to a single intramuscular injection. Systemic exposure as measured by mean AUC were equivalent, with values of 156 [95% CI 136, 180] and 161 [95% CI 137, 190] ng•h/mL for intravenous and intramuscular groups, respectively. Mean peak plasma concentrations were 42.9 [95% CI 33.8, 54.4] ng/mL at 10 minutes after intravenous infusion and 31.9 [95% CI 26.3, 38.6] ng/mL at 41 minutes after intramuscular injection. Distribution : Plasma protein binding of ondansetron as measured in vitro was 70% to 76%, over the pharmacologic concentration range of 10 to 500 ng/mL. Circulating drug also distributes into erythrocytes. Metabolism: Ondansetron is extensively metabolized in humans, with approximately 5% of a radiolabeled dose recovered as the parent compound from the urine. The primary metabolic pathway is hydroxylation on the indole ring followed by subsequent glucuronide or sulfate conjugation. Although some nonconjugated metabolites have pharmacologic activity, these are not found in plasma at concentrations likely to significantly contribute to the biological activity of ondansetron. The metabolites are observed in the urine. In vitro metabolism studies have shown that ondansetron is a substrate for multiple human hepatic cytochrome P-450 enzymes, including CYP1A2, CYP2D6, and CYP3A4. In terms of overall ondansetron turnover, CYP3A4 plays a predominant role while formation of the major in vivo metabolites is apparently mediated by CYP1A2. The role of CYP2D6 in ondansetron in vivo metabolism is relatively minor. The pharmacokinetics of intravenous ondansetron did not differ between subjects who were poor metabolisers of CYP2D6 and those who were extensive metabolisers of CYP2D6, further supporting the limited role of CYP2D6 in ondansetron disposition in vivo. Elimination: In adult cancer patients, the mean ondansetron elimination half-life was 4.0 hours, and there was no difference in the multidose pharmacokinetics over a 4-day period. In a dose proportionality study, systemic exposure to 32 mg of ondansetron was not proportional to dose as measured by comparing dose-normalized AUC values to an 8-mg dose. This is consistent with a small decrease in systemic clearance with increasing plasma concentrations. Geriatrics: A reduction in clearance and increase in elimination half-life are seen in patients over 75 years of age. In clinical trials with cancer patients, safety and efficacy were similar in patients over 65 years of age and those under 65 years of age; there was an insufficient number of patients over 75 years of age to permit conclusions in that age-group. No dosage adjustment is recommended in the elderly. Pediatrics: Pharmacokinetic samples were collected from 74 cancer patients 6 to 48 months of age, who received a dose of 0.15 mg/kg of intravenous ondansetron every 4 hours for 3 doses during a safety and efficacy trial. These data were combined with sequential pharmacokinetics data from 41 surgery patients 1 month to 24 months of age, who received a single dose of 0.1 mg/kg of intravenous ondansetron prior to surgery with general anesthesia, and a population pharmacokinetic analysis was performed on the combined data set. The results of this analysis are included in Table 4 and are compared to the pharmacokinetic results in cancer patients 4 to 18 years of age. Table 4. Pharmacokinetics in Pediatric Cancer Patients 1 Month to 18 Years of Age a Population PK (Pharmacokinetic) Patients: 64% cancer patients and 36% surgery patients. Subjects and Age Group N CL (L/h/kg) Vd ss  (L/kg) T ½  (h) Geometric Mean Mean Pediatric Cancer Patients 4 to 18 years of age N = 21 0.599 1.9 2.8 Population PK Patients a  1 month to 48 months of age N = 115 0.582 3.65 4.9 Based on the population pharmacokinetic analysis, cancer patients 6 to 48 months of age who receive a dose of 0.15 mg/kg of intravenous ondansetron every 4 hours for 3 doses would be expected to achieve a systemic exposure (AUC) consistent with the exposure achieved in previous pediatric studies in cancer patients (4 to 18 years of age) at similar doses. In a study of 21 pediatric patients (3 to 12 years of age) who were undergoing surgery requiring anesthesia for a duration of 45 minutes to 2 hours, a single intravenous dose of ondansetron, 2 mg (3 to 7 years) or 4 mg (8 to 12 years), was administered immediately prior to anesthesia induction. Mean weight-normalized clearance and volume of distribution values in these pediatric surgical patients were similar to those previously reported for young adults. Mean terminal half-life was slightly reduced in pediatric patients (range, 2.5 to 3 hours) in comparison with adults (range, 3 to 3.5 hours). In a study of 51 pediatric patients (1 month to 24 months of age) who were undergoing surgery requiring general anesthesia, a single intravenous dose of ondansetron, 0.1 or 0.2 mg/kg, was administered prior to surgery. As shown in Table 5 , the 41 patients with pharmacokinetic data were divided into 2 groups, patients 1 month to 4 months of age and patients 5 to 24 months of age, and are compared to pediatric patients 3 to 12 years of age. Table 5. Pharmacokinetics in Pediatric Surgery Patients 1 Month to 12 Years of Age Subjects and Age Group N CL (L/h/kg) Vd ss  (L/kg) T ½  (h) Geometric Mean Mean Pediatric Surgery Patients 3 to 12 years of age N = 21 0.439 1.65 2.9 Pediatric Surgery Patients 5 to 24 months of age N = 22 0.581 2.3 2.9 Pediatric Surgery Patients 1 month to 4 months of age N = 19 0.401 3.5 6.7 In general, surgical and cancer pediatric patients younger than 18 years tend to have a higher ondansetron clearance compared to adults leading to a shorter half-life in most pediatric patients. In patients 1 month to 4 months of age, a longer half-life was observed due to the higher volume of distribution in this age group. In a study of 21 pediatric cancer patients (4 to 18 years of age) who received three intravenous doses of 0.15 mg/kg of ondansetron at 4-hour intervals, patients older than 15 years of age exhibited ondansetron pharmacokinetic parameters similar to those of adults. Renal Impairment : Due to the very small contribution (5%) of renal clearance to the overall clearance, renal impairment was not expected to significantly influence the total clearance of ondansetron. However, ondansetron mean plasma clearance was reduced by about 41% in patients with severe renal impairment (creatinine clearance &lt; 30 mL/min). This reduction in clearance is variable and was not consistent with an increase in half-life. No reduction in dose or dosing frequency in these patients is warranted. Hepatic Impairment : In patients with mild-to-moderate hepatic impairment, clearance is reduced 2-fold and mean half-life is increased to 11.6 hours compared to 5.7 hours in those without hepatic impairment. In patients with severe hepatic impairment (Child-Pugh score of 10 or greater), clearance is reduced 2-fold to 3-fold and apparent volume of distribution is increased with a resultant increase in half-life to 20 hours. In patients with severe hepatic impairment, a total daily dose of 8 mg should not be exceeded.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="ONDANSETRON" id="5522" section="34073-7">
      

      <SentenceText>Apomorphine – profound hypotension and loss of consciousness.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="14 20" str="profound hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="39 4" str="loss" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M3" span="0 11" str="Apomorphine" type="Precipitant"/>
      <Interaction effect="M1;M2" id="I1" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5523" section="34073-7">
      

      <SentenceText>Concomitant use with ondansetron is contraindicated.</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="36 15" str="contraindicated" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5524" section="34073-7">
      

      <SentenceText>Ondansetron does not appear to induce or inhibit the cytochrome P-450 drug-metabolizing enzyme system of the liver.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5525" section="34073-7">
      

      <SentenceText>Because ondansetron is metabolized by hepatic cytochrome P-450 drug-metabolizing enzymes (CYP3A4, CYP2D6, CYP1A2), inducers or inhibitors of these enzymes may change the clearance and, hence, the half-life of ondansetron.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5526" section="34073-7">
      

      <SentenceText>On the basis of limited available data, no dosage adjustment is recommended for patients on these drugs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5527" section="34073-7">
      

      <SentenceText>Based on reports of profound hypotension and loss of consciousness when apomorphine was administered with ondansetron, concomitant use of apomorphine with ondansetron is contraindicated.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="20 8" str="profound" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M6" span="29 11" str="hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M7" span="45 4" str="loss" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M8" span="170 15" str="contraindicated" type="Trigger"/>
      <Mention code="NO MAP" id="M9" span="72 11" str="apomorphine" type="Precipitant"/>
      <Interaction effect="M5;M6;M7" id="I2" precipitant="M9" trigger="M8" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M10" span="138 11" str="apomorphine" type="Precipitant"/>
      <Interaction effect="M5;M6;M7" id="I3" precipitant="M10" trigger="M8" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5528" section="34073-7">
      

      <SentenceText>In patients treated with potent inducers of CYP3A4 (i.e., phenytoin, carbamazepine, and rifampin), the clearance of ondansetron was significantly increased and ondansetron blood concentrations were decreased.</SentenceText>
      

      <Mention code="NO MAP" id="M11" span="32 18" str="inducers of CYP3A4" type="Precipitant"/>
      <Mention code="NO MAP" id="M12" span="58 9" str="phenytoin" type="Precipitant"/>
      <Interaction effect="C54355" id="I4" precipitant="M12" trigger="M12" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M13" span="69 13" str="carbamazepine" type="Precipitant"/>
      <Interaction effect="C54355" id="I5" precipitant="M13" trigger="M13" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M14" span="88 8" str="rifampin" type="Precipitant"/>
      <Interaction effect="C54355" id="I6" precipitant="M14" trigger="M14" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5529" section="34073-7">
      

      <SentenceText>However, on the basis of available data, no dosage adjustment for ondansetron is recommended for patients on these drugs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5530" section="34073-7">
      

      <SentenceText>Although there are no data on pharmacokinetic drug interactions between ondansetron and tramadol, data from two small studies indicate that concomitant use of ondansetron may result in reduced analgesic activity of tramadol.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="185 26" str="reduced analgesic activity" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M16" span="215 8" str="tramadol" type="Precipitant"/>
      <Interaction effect="M15" id="I7" precipitant="M16" trigger="M16" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5531" section="34073-7">
      

      <SentenceText>Patients on concomitant ondansetron self administered tramadol more frequently in these studies, leading to an increased cumulative dose in patient controlled administration (PCA) of tramadol.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="111 25" str="increased cumulative dose" type="Trigger"/>
      <Mention code="NO MAP" id="M18" span="54 8" str="tramadol" type="Precipitant"/>
      <Interaction effect="C54355" id="I8" precipitant="M18" trigger="M17" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M19" span="183 8" str="tramadol" type="Precipitant"/>
      <Interaction effect="C54355" id="I9" precipitant="M19" trigger="M17" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5532" section="34073-7">
      

      <SentenceText>Serotonin syndrome (including altered mental status, autonomic instability, and neuromuscular symptoms) has been described following the concomitant use of 5-HT3 receptor antagonists and other serotonergic drugs, including selective serotonin reuptake inhibitors (SSRIs) and serotonin and noradrenaline reuptake inhibitors (SNRIs).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5533" section="34073-7">
      

      <SentenceText>In humans, carmustine, etoposide, and cisplatin do not affect the pharmacokinetics of ondansetron.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="48 34" str="do not affect the pharmacokinetics" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5534" section="34073-7">
      

      <SentenceText>In a crossover study in 76 pediatric patients, intravenous ondansetron did not increase blood levels of high-dose methotrexate.</SentenceText>
      

      <Mention code="NO MAP" id="M21" span="79 21" str="increase blood levels" type="Trigger"/>
      <Mention code="NO MAP" id="M22" span="114 12" str="methotrexate" type="Precipitant"/>
      <Interaction effect="C54357" id="I10" precipitant="M22" trigger="M21" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5535" section="34073-7">
      

      <SentenceText>The coadministration of ondansetron had no effect on the pharmacokinetics and pharmacodynamics of temazepam.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5536" section="34073-7">
      

      <SentenceText>Ondansetron does not alter the respiratory depressant effects produced by alfentanil or the degree of neuromuscular blockade produced by atracurium.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5537" section="34073-7">
      

      <SentenceText>Interactions with general or local anesthetics have not been studied.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5538" section="34090-1">
      

      <SentenceText>Ondansetron is a selective 5-HT3 receptor antagonist.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5539" section="34090-1">
      

      <SentenceText>While ondansetron's mechanism of action has not been fully characterized, it is not a dopamine-receptor antagonist.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5540" section="34090-1">
      

      <SentenceText>QTc interval prolongation was studied in a double blind, single intravenous dose, placebo- and positive-controlled, crossover study in 58 healthy subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5541" section="34090-1">
      

      <SentenceText>The maximum mean (95% upper confidence bound) difference in QTcF from placebo after baseline-correction was 19.5 (21.8) ms and 5.6 (7.4) ms after 15 minute intravenous infusions of 32 mg and 8 mg Ondansetron, respectively.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5542" section="34090-1">
      

      <SentenceText>A significant exposure-response relationship was identified between ondansetron concentration and ΔΔQTcF.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="14 8" str="exposure" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5543" section="34090-1">
      

      <SentenceText>Using the established exposure-response relationship, 24 mg infused intravenously over 15 min had a mean predicted (95% upper prediction interval) ΔΔQTcF of 14.0 (16.3) ms.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5544" section="34090-1">
      

      <SentenceText>In contrast, 16 mg infused intravenously over 15 min using the same model had a mean predicted (95% upper prediction interval) ΔΔQTcF of 9.1 (11.2) ms.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5545" section="34090-1">
      

      <SentenceText>In normal volunteers, single intravenous doses of 0.15 mg/kg of ondansetron had no effect on esophageal motility, gastric motility, lower esophageal sphincter pressure, or small intestinal transit time.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5546" section="34090-1">
      

      <SentenceText>In another study in six normal male volunteers, a 16-mg dose infused over 5 minutes showed no effect of the drug on cardiac output, heart rate, stroke volume, blood pressure, or electrocardiogram (ECG).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5547" section="34090-1">
      

      <SentenceText>Multiday administration of ondansetron has been shown to slow colonic transit in normal volunteers.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5548" section="34090-1">
      

      <SentenceText>Ondansetron has no effect on plasma prolactin concentrations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5549" section="34090-1">
      

      <SentenceText>In a gender-balanced pharmacodynamic study (n = 56), ondansetron 4 mg administered intravenously or intramuscularly was dynamically similar in the prevention of nausea and vomiting using the ipecacuanha model of emesis.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="161 6" str="nausea" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M25" span="172 8" str="vomiting" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="212 6" str="emesis" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5550" section="34090-1">
      

      <SentenceText>In normal adult volunteers, the following mean pharmacokinetic data have been determined following a single 0.15-mg/kg intravenous dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5551" section="34090-1">
      

      <SentenceText>Pharmacokinetics in Normal Adult Volunteers Age-group(years) n Peak Plasma Concentration (ng/mL) Mean EliminationHalf-life (h) Plasma Clearance(L/h/kg) 19-40 11 102 3.5 0.381 61-74 12 106 4.7 0.319 ≥ 75 11 170 5.5 0.262 Absorption: A study was performed in normal volunteers (n = 56) to evaluate the pharmacokinetics of a single 4-mg dose administered as a 5-minute infusion compared to a single intramuscular injection.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5552" section="34090-1">
      

      <SentenceText>Systemic exposure as measured by mean AUC were equivalent, with values of 156 [95% CI 136, 180] and 161 [95% CI 137, 190] ng•h/mL for intravenous and intramuscular groups, respectively.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5553" section="34090-1">
      

      <SentenceText>Mean peak plasma concentrations were 42.9 [95% CI 33.8, 54.4] ng/mL at 10 minutes after intravenous infusion and 31.9 [95% CI 26.3, 38.6] ng/mL at 41 minutes after intramuscular injection.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5554" section="34090-1">
      

      <SentenceText>Distribution: Plasma protein binding of ondansetron as measured in vitro was 70% to 76%, over the pharmacologic concentration range of 10 to 500 ng/mL.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5555" section="34090-1">
      

      <SentenceText>Circulating drug also distributes into erythrocytes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5556" section="34090-1">
      

      <SentenceText>Metabolism: Ondansetron is extensively metabolized in humans, with approximately 5% of a radiolabeled dose recovered as the parent compound from the urine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5557" section="34090-1">
      

      <SentenceText>The primary metabolic pathway is hydroxylation on the indole ring followed by subsequent glucuronide or sulfate conjugation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5558" section="34090-1">
      

      <SentenceText>Although some nonconjugated metabolites have pharmacologic activity, these are not found in plasma at concentrations likely to significantly contribute to the biological activity of ondansetron.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="9 30" str="some nonconjugated metabolites" type="Precipitant"/>
      <Interaction effect="C54355" id="I11" precipitant="M27" trigger="M27" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5559" section="34090-1">
      

      <SentenceText>The metabolites are observed in the urine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5560" section="34090-1">
      

      <SentenceText>In vitro metabolism studies have shown that ondansetron is a substrate for multiple human hepatic cytochrome P-450 enzymes, including CYP1A2, CYP2D6, and CYP3A4.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5561" section="34090-1">
      

      <SentenceText>In terms of overall ondansetron turnover, CYP3A4 plays a predominant role while formation of the major in vivo metabolites is apparently mediated by CYP1A2.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5562" section="34090-1">
      

      <SentenceText>The role of CYP2D6 in ondansetron in vivo metabolism is relatively minor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5563" section="34090-1">
      

      <SentenceText>The pharmacokinetics of intravenous ondansetron did not differ between subjects who were poor metabolisers of CYP2D6 and those who were extensive metabolisers of CYP2D6, further supporting the limited role of CYP2D6 in ondansetron disposition in vivo.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5564" section="34090-1">
      

      <SentenceText>Elimination: In adult cancer patients, the mean ondansetron elimination half-life was 4.0 hours, and there was no difference in the multidose pharmacokinetics over a 4-day period.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5565" section="34090-1">
      

      <SentenceText>In a dose proportionality study, systemic exposure to 32 mg of ondansetron was not proportional to dose as measured by comparing dose-normalized AUC values to an 8-mg dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5566" section="34090-1">
      

      <SentenceText>This is consistent with a small decrease in systemic clearance with increasing plasma concentrations.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="44 18" str="systemic clearance" type="Trigger"/>
      <Mention code="NO MAP" id="M29" span="68 32" str="increasing plasma concentrations" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5567" section="34090-1">
      

      <SentenceText>Geriatrics: A reduction in clearance and increase in elimination half-life are seen in patients over 75 years of age.</SentenceText>
      

      <Mention code="NO MAP" id="M30" span="24 12" str="in clearance" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5568" section="34090-1">
      

      <SentenceText>In clinical trials with cancer patients, safety and efficacy were similar in patients over 65 years of age and those under 65 years of age; there was an insufficient number of patients over 75 years of age to permit conclusions in that age-group.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5569" section="34090-1">
      

      <SentenceText>No dosage adjustment is recommended in the elderly.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5570" section="34090-1">
      

      <SentenceText>Pediatrics: Pharmacokinetic samples were collected from 74 cancer patients 6 to 48 months of age, who received a dose of 0.15 mg/kg of intravenous ondansetron every 4 hours for 3 doses during a safety and efficacy trial.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5571" section="34090-1">
      

      <SentenceText>These data were combined with sequential pharmacokinetics data from 41 surgery patients 1 month to 24 months of age, who received a single dose of 0.1 mg/kg of intravenous ondansetron prior to surgery with general anesthesia, and a population pharmacokinetic analysis was performed on the combined data set.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5572" section="34090-1">
      

      <SentenceText>The results of this analysis are included in Table 4 and are compared to the pharmacokinetic results in cancer patients 4 to 18 years of age.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5573" section="34090-1">
      

      <SentenceText>Pharmacokinetics in Pediatric Cancer Patients 1 Month to 18 Years of Age a Population PK (Pharmacokinetic) Patients: 64% cancer patients and 36% surgery patients.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5574" section="34090-1">
      

      <SentenceText>Subjects and Age Group N CL(L/h/kg) Vdss (L/kg) T½ (h) Geometric Mean Mean Pediatric Cancer Patients4 to 18 years of age N = 21 0.599 1.9 2.8 Population PK Patientsa 1 month to 48 months of age N = 115 0.582 3.65 4.9 Based on the population pharmacokinetic analysis, cancer patients 6 to 48 months of age who receive a dose of 0.15 mg/kg of intravenous ondansetron every 4 hours for 3 doses would be expected to achieve a systemic exposure (AUC) consistent with the exposure achieved in previous pediatric studies in cancer patients (4 to 18 years of age) at similar doses.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5575" section="34090-1">
      

      <SentenceText>In a study of 21 pediatric patients (3 to 12 years of age) who were undergoing surgery requiring anesthesia for a duration of 45 minutes to 2 hours, a single intravenous dose of ondansetron, 2 mg (3 to 7 years) or 4 mg (8 to 12 years), was administered immediately prior to anesthesia induction.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5576" section="34090-1">
      

      <SentenceText>Mean weight-normalized clearance and volume of distribution values in these pediatric surgical patients were similar to those previously reported for young adults.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5577" section="34090-1">
      

      <SentenceText>Mean terminal half-life was slightly reduced in pediatric patients (range, 2.5 to 3 hours) in comparison with adults (range, 3 to 3.5 hours).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5578" section="34090-1">
      

      <SentenceText>In a study of 51 pediatric patients (1 month to 24 months of age) who were undergoing surgery requiring general anesthesia, a single intravenous dose of ondansetron, 0.1 or 0.2 mg/kg, was administered prior to surgery.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5579" section="34090-1">
      

      <SentenceText>As shown in Table 5, the 41 patients with pharmacokinetic data were divided into 2 groups, patients 1 month to 4 months of age and patients 5 to 24 months of age, and are compared to pediatric patients 3 to 12 years of age.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5580" section="34090-1">
      

      <SentenceText>Pharmacokinetics in Pediatric Surgery Patients 1 Month to 12 Years of Age Subjects and Age Group N CL(L/h/kg) Vdss (L/kg) T½ (h) Geometric Mean Mean Pediatric Surgery Patients3 to 12 years of age N = 21 0.439 1.65 2.9 Pediatric Surgery Patients5 to 24 months of age N = 22 0.581 2.3 2.9 Pediatric Surgery Patients1 month to 4 months of age N = 19 0.401 3.5 6.7 In general, surgical and cancer pediatric patients younger than 18 years tend to have a higher ondansetron clearance compared to adults leading to a shorter half-life in most pediatric patients.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5581" section="34090-1">
      

      <SentenceText>In patients 1 month to 4 months of age, a longer half-life was observed due to the higher volume of distribution in this age group.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5582" section="34090-1">
      

      <SentenceText>In a study of 21 pediatric cancer patients (4 to 18 years of age) who received three intravenous doses of 0.15 mg/kg of ondansetron at 4-hour intervals, patients older than 15 years of age exhibited ondansetron pharmacokinetic parameters similar to those of adults.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5583" section="34090-1">
      

      <SentenceText>Renal Impairment: Due to the very small contribution (5%) of renal clearance to the overall clearance, renal impairment was not expected to significantly influence the total clearance of ondansetron.</SentenceText>
      

      <Mention code="NO MAP" id="M31" span="67 9" str="clearance" type="Trigger"/>
      <Mention code="NO MAP" id="M32" span="154 29" str="influence the total clearance" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5584" section="34090-1">
      

      <SentenceText>However, ondansetron mean plasma clearance was reduced by about 41% in patients with severe renal impairment (creatinine clearance &lt; 30 mL/min).</SentenceText>
      

      <Mention code="NO MAP" id="M33" span="21 33" str="mean plasma clearance was reduced" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5585" section="34090-1">
      

      <SentenceText>This reduction in clearance is variable and was not consistent with an increase in half-life.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="0 4" str="This" type="Trigger"/>
      <Mention code="NO MAP" id="M35" span="5 22" str="reduction in clearance" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5586" section="34090-1">
      

      <SentenceText>No reduction in dose or dosing frequency in these patients is warranted.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5587" section="34090-1">
      

      <SentenceText>Hepatic Impairment: In patients with mild-to-moderate hepatic impairment, clearance is reduced 2-fold and mean half-life is increased to 11.6 hours compared to 5.7 hours in those without hepatic impairment.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="116 4" str="life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5588" section="34090-1">
      

      <SentenceText>In patients with severe hepatic impairment (Child-Pugh score of 10 or greater), clearance is reduced 2-fold to 3-fold and apparent volume of distribution is increased with a resultant increase in half-life to 20 hours.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="90 10" str="is reduced" type="Trigger"/>
      <Mention code="NO MAP" id="M38" span="141 25" str="distribution is increased" type="Trigger"/>
      <Mention code="NO MAP" id="M39" span="184 8" str="increase" type="Trigger"/>
      <Mention code="NO MAP" id="M40" span="201 4" str="life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ONDANSETRON" id="5589" section="34090-1">
      

      <SentenceText>In patients with severe hepatic impairment, a total daily dose of 8 mg should not be exceeded.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

