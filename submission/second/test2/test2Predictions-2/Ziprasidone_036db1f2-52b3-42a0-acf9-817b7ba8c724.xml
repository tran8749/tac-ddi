<?xml version="1.0" ?>
<Label drug="Ziprasidone" setid="036db1f2-52b3-42a0-acf9-817b7ba8c724">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
DRUG INTERACTIONS  Drug-drug interactions can be pharmacodynamic (combined pharmacologic effects) or pharmacokinetic (alteration of plasma levels). The risks of using ziprasidone in combination with other drugs have been evaluated as described below. All interactions studies have been conducted with oral ziprasidone. Based upon the pharmacodynamic and pharmacokinetic profile of ziprasidone, possible interactions could be anticipated:  Approximately two-thirds of ziprasidone is metabolized via a combination of chemical reduction by glutathione and enzymatic reduction by aldehyde oxidase. There are no known clinically relevant inhibitors or inducers of aldehyde oxidase. Less than one-third of ziprasidone metabolic clearance is mediated by cytochrome P450 catalyzed oxidation.  An  in vitro enzyme inhibition study utilizing human liver microsomes showed that ziprasidone had little inhibitory effect on CYP1A2, CYP2C9, CYP2C19, CYP2D6 and CYP3A4, and thus would not likely interfere with the metabolism of drugs primarily metabolized by these enzymes. There is little potential for drug interactions with ziprasidone due to displacement  [see  Clinical Pharmacology (12.3) ] . Ziprasidone should not be used with any drug that prolongs the QT interval  [see  Contraindications (4.1) ] . Given the primary CNS effects of ziprasidone, caution should be used when it is taken in combination with other centrally acting drugs.  Because of its potential for inducing hypotension, ziprasidone may enhance the effects of certain antihypertensive agents.  Ziprasidone may antagonize the effects of levodopa and dopamine agonists.  Carbamazepine  Carbamazepine is an inducer of CYP3A4; administration of 200 mg twice daily for 21 days resulted in a decrease of approximately 35% in the AUC of ziprasidone. This effect may be greater when higher doses of carbamazepine are administered.  Ketoconazole  Ketoconazole, a potent inhibitor of CYP3A4, at a dose of 400 mg QD for 5 days, increased the AUC and Cmax of ziprasidone by about 35–40%. Other inhibitors of CYP3A4 would be expected to have similar effects.  Cimetidine  Cimetidine at a dose of 800 mg QD for 2 days did not affect ziprasidone pharmacokinetics.  Antacid  The co-administration of 30 mL of Maalox® with ziprasidone did not affect the pharmacokinetics of ziprasidone.  Ziprasidone at a dose of 40 mg twice daily administered concomitantly with lithium at a dose of 450 mg twice daily for 7 days did not affect the steady-state level or renal clearance of lithium. Ziprasidone dosed adjunctively to lithium in a maintenance trial of bipolar patients did not affect mean therapeutic lithium levels.  In vivo studies have revealed no effect of ziprasidone on the pharmacokinetics of estrogen or progesterone components. Ziprasidone at a dose of 20 mg twice daily did not affect the pharmacokinetics of concomitantly administered oral contraceptives, ethinyl estradiol (0.03 mg) and levonorgestrel (0.15 mg). Consistent with  in vitro results, a study in normal healthy volunteers showed that ziprasidone did not alter the metabolism of dextromethorphan, a CYP2D6 model substrate, to its major metabolite, dextrorphan. There was no statistically significant change in the urinary dextromethorphan/dextrorphan ratio. A pharmacokinetic interaction of ziprasidone with valproate is unlikely due to the lack of common metabolic pathways for the two drugs. Ziprasidone dosed adjunctively to valproate in a maintenance trial of bipolar patients did not affect mean therapeutic valproate levels.  Population pharmacokinetic analysis of schizophrenic patients enrolled in controlled clinical trials has not revealed evidence of any clinically significant pharmacokinetic interactions with benztropine, propranolol, or lorazepam.  The absolute bioavailability of a 20 mg dose under fed conditions is approximately 60%. The absorption of ziprasidone is increased up to two-fold in the presence of food  [see  Clinical Pharmacology (12.3) ] .</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  The mechanism of action of ziprasidone, as with other drugs having efficacy in schizophrenia, is unknown. However, it has been proposed that this drug's efficacy in schizophrenia is mediated through a combination of dopamine type 2 (D 2 ) and serotonin type 2 (5HT 2 ) antagonism. As with other drugs having efficacy in bipolar disorder, the mechanism of action of ziprasidone in bipolar disorder is unknown. Ziprasidone exhibited high  in vitro binding affinity for the dopamine D 2 and D 3 , the serotonin 5HT 2A , 5HT 2C , 5HT 1A , 5HT 1D , and α 1 -adrenergic receptors (K i s of 4.8, 7.2, 0.4, 1.3, 3.4, 2, and 10 nM, respectively), and moderate affinity for the histamine H 1 receptor (K i =47 nM). Ziprasidone functioned as an antagonist at the D 2, 5HT 2A , and 5HT 1D receptors, and as an agonist at the 5HT 1A receptor. Ziprasidone inhibited synaptic reuptake of serotonin and norepinephrine. No appreciable affinity was exhibited for other receptor/binding sites tested, including the cholinergic muscarinic receptor (IC 50 &gt;1 µM). Antagonism at receptors other than dopamine and 5HT 2 with similar receptor affinities may explain some of the other therapeutic and side effects of ziprasidone. Ziprasidone's antagonism of histamine H 1 receptors may explain the somnolence observed with this drug. Ziprasidone's antagonism of α 1 -adrenergic receptors may explain the orthostatic hypotension observed with this drug. Oral Pharmacokinetics  Ziprasidone's activity is primarily due to the parent drug. The multiple-dose pharmacokinetics of ziprasidone are dose-proportional within the proposed clinical dose range, and ziprasidone accumulation is predictable with multiple dosing. Elimination of ziprasidone is mainly via hepatic metabolism with a mean terminal half-life of about 7 hours within the proposed clinical dose range. Steady-state concentrations are achieved within one to three days of dosing. The mean apparent systemic clearance is 7.5 mL/min/kg. Ziprasidone is unlikely to interfere with the metabolism of drugs metabolized by cytochrome P450 enzymes.  Absorption : Ziprasidone is well absorbed after oral administration, reaching peak plasma concentrations in 6 to 8 hours. The absolute bioavailability of a 20 mg dose under fed conditions is approximately 60%. The absorption of ziprasidone is increased up to two-fold in the presence of food. Distribution : Ziprasidone has a mean apparent volume of distribution of 1.5 L/kg. It is greater than 99% bound to plasma proteins, binding primarily to albumin and α 1 -acid glycoprotein. The  in vitro plasma protein binding of ziprasidone was not altered by warfarin or propranolol, two highly protein-bound drugs, nor did ziprasidone alter the binding of these drugs in human plasma. Thus, the potential for drug interactions with ziprasidone due to displacement is minimal. Metabolism and Elimination : Ziprasidone is extensively metabolized after oral administration with only a small amount excreted in the urine (&lt;1%) or feces (&lt;4%) as unchanged drug. Ziprasidone is primarily cleared via three metabolic routes to yield four major circulating metabolites, benzisothiazole (BITP) sulphoxide, BITP-sulphone, ziprasidone sulphoxide, and S-methyldihydroziprasidone. Approximately 20% of the dose is excreted in the urine, with approximately 66% being eliminated in the feces. Unchanged ziprasidone represents about 44% of total drug-related material in serum.  In vitro studies using human liver subcellular fractions indicate that S-methyldihydroziprasidone is generated in two steps. These studies indicate that the reduction reaction is mediated primarily by chemical reduction by glutathione as well as by enzymatic reduction by aldehyde oxidase and the subsequent methylation is mediated by thiol methyltransferase.  In vitro studies using human liver microsomes and recombinant enzymes indicate that CYP3A4 is the major CYP contributing to the oxidative metabolism of ziprasidone. CYP1A2 may contribute to a much lesser extent. Based on  in vivo abundance of excretory metabolites, less than one-third of ziprasidone metabolic clearance is mediated by cytochrome P450 catalyzed oxidation and approximately two-thirds via reduction. There are no known clinically relevant inhibitors or inducers of aldehyde oxidase. Intramuscular Pharmacokinetics  Systemic Bioavailability : The bioavailability of ziprasidone administered intramuscularly is 100%. After intramuscular administration of single doses, peak serum concentrations typically occur at approximately 60 minutes post-dose or earlier and the mean half-life (T ½ ) ranges from two to five hours. Exposure increases in a dose-related manner and following three days of intramuscular dosing, little accumulation is observed. Metabolism and Elimination : Although the metabolism and elimination of IM ziprasidone have not been systematically evaluated, the intramuscular route of administration would not be expected to alter the metabolic pathways.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Ziprasidone" id="4566" section="34073-7">
      

      <SentenceText>Drug-drug interactions can be pharmacodynamic (combined pharmacologic effects) or pharmacokinetic (alteration of plasma levels).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4567" section="34073-7">
      

      <SentenceText>The risks of using ziprasidone in combination with other drugs have been evaluated as described below.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4568" section="34073-7">
      

      <SentenceText>All interactions studies have been conducted with oral ziprasidone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4569" section="34073-7">
      

      <SentenceText>Based upon the pharmacodynamic and pharmacokinetic profile of ziprasidone, possible interactions could be anticipated: Approximately two-thirds of ziprasidone is metabolized via a combination of chemical reduction by glutathione and enzymatic reduction by aldehyde oxidase.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="256 16" str="aldehyde oxidase" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4570" section="34073-7">
      

      <SentenceText>There are no known clinically relevant inhibitors or inducers of aldehyde oxidase.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4571" section="34073-7">
      

      <SentenceText>Less than one-third of ziprasidone metabolic clearance is mediated by cytochrome P450 catalyzed oxidation.</SentenceText>
      

      <Mention code="NO MAP" id="M2" span="81 4" str="P450" type="Precipitant"/>
      <Mention code="NO MAP" id="M3" span="96 9" str="oxidation" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4572" section="34073-7">
      

      <SentenceText>An in vitro enzyme inhibition study utilizing human liver microsomes showed that ziprasidone had little inhibitory effect on CYP1A2, CYP2C9, CYP2C19, CYP2D6 and CYP3A4, and thus would not likely interfere with the metabolism of drugs primarily metabolized by these enzymes.</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="228 27" str="drugs primarily metabolized" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4573" section="34073-7">
      

      <SentenceText>There is little potential for drug interactions with ziprasidone due to displacement.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4574" section="34073-7">
      

      <SentenceText>Ziprasidone should not be used with any drug that prolongs the QT interval.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="12 18" str="should not be used" type="Trigger"/>
      <Mention code="NO MAP" id="M6" span="66 8" str="interval" type="Precipitant"/>
      <Interaction id="I1" precipitant="M6" trigger="M5" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4575" section="34073-7">
      

      <SentenceText>Given the primary CNS effects of ziprasidone, caution should be used when it is taken in combination with other centrally acting drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M7" span="46 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M8" span="112 22" str="centrally acting drugs" type="Precipitant"/>
      <Interaction id="I2" precipitant="M8" trigger="M7" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4576" section="34073-7">
      

      <SentenceText>Because of its potential for inducing hypotension, ziprasidone may enhance the effects of certain antihypertensive agents.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="38 11" str="hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M10" span="67 19" str="enhance the effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M11" span="98 23" str="antihypertensive agents" type="Precipitant"/>
      <Interaction effect="M10;M9" id="I3" precipitant="M11" trigger="M11" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4577" section="34073-7">
      

      <SentenceText>Ziprasidone may antagonize the effects of levodopa and dopamine agonists.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="16 22" str="antagonize the effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M13" span="42 8" str="levodopa" type="Precipitant"/>
      <Interaction effect="M12" id="I4" precipitant="M13" trigger="M13" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M14" span="55 17" str="dopamine agonists" type="Precipitant"/>
      <Interaction effect="M12" id="I5" precipitant="M14" trigger="M14" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4578" section="34073-7">
      

      <SentenceText>Carbamazepine Carbamazepine is an inducer of CYP3A4; administration of 200 mg twice daily for 21 days resulted in a decrease of approximately 35% in the AUC of ziprasidone.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="149 7" str="the AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M16" span="0 13" str="Carbamazepine" type="Precipitant"/>
      <Interaction effect="C54602" id="I6" precipitant="M16" trigger="M15" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I7" precipitant="M16" trigger="M15" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4579" section="34073-7">
      

      <SentenceText>This effect may be greater when higher doses of carbamazepine are administered.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="5 21" str="effect may be greater" type="Trigger"/>
      <Mention code="NO MAP" id="M18" span="48 13" str="carbamazepine" type="Precipitant"/>
      <Interaction id="I8" precipitant="M18" trigger="M17" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4580" section="34073-7">
      

      <SentenceText>Ketoconazole Ketoconazole, a potent inhibitor of CYP3A4, at a dose of 400 mg QD for 5 days, increased the AUC and Cmax of ziprasidone by about 35–40%.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="92 21" str="increased the AUC and" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="0 12" str="Ketoconazole" type="Precipitant"/>
      <Interaction effect="C54602" id="I9" precipitant="M20" trigger="M19" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I10" precipitant="M20" trigger="M19" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M21" span="13 12" str="Ketoconazole" type="Precipitant"/>
      <Interaction effect="C54602" id="I11" precipitant="M21" trigger="M19" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I12" precipitant="M21" trigger="M19" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M22" span="36 19" str="inhibitor of CYP3A4" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4581" section="34073-7">
      

      <SentenceText>Other inhibitors of CYP3A4 would be expected to have similar effects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4582" section="34073-7">
      

      <SentenceText>Cimetidine Cimetidine at a dose of 800 mg QD for 2 days did not affect ziprasidone pharmacokinetics.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4583" section="34073-7">
      

      <SentenceText>Antacid The co-administration of 30 mL of Maalox® with ziprasidone did not affect the pharmacokinetics of ziprasidone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4584" section="34073-7">
      

      <SentenceText>Ziprasidone at a dose of 40 mg twice daily administered concomitantly with lithium at a dose of 450 mg twice daily for 7 days did not affect the steady-state level or renal clearance of lithium.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="75 7" str="lithium" type="Precipitant"/>
      <Interaction id="I13" precipitant="M23" trigger="M23" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4585" section="34073-7">
      

      <SentenceText>Ziprasidone dosed adjunctively to lithium in a maintenance trial of bipolar patients did not affect mean therapeutic lithium levels.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="117 7" str="lithium" type="Precipitant"/>
      <Interaction effect="C54357" id="I14" precipitant="M24" trigger="M24" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4586" section="34073-7">
      

      <SentenceText>In vivo studies have revealed no effect of ziprasidone on the pharmacokinetics of estrogen or progesterone components.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4587" section="34073-7">
      

      <SentenceText>Ziprasidone at a dose of 20 mg twice daily did not affect the pharmacokinetics of concomitantly administered oral contraceptives, ethinyl estradiol (0.03 mg) and levonorgestrel (0.15 mg).</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="51 6" str="affect" type="Trigger"/>
      <Mention code="NO MAP" id="M26" span="114 14" str="contraceptives" type="Precipitant"/>
      <Interaction effect="C54358" id="I15" precipitant="M26" trigger="M25" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4588" section="34073-7">
      

      <SentenceText>Consistent with in vitro results, a study in normal healthy volunteers showed that ziprasidone did not alter the metabolism of dextromethorphan, a CYP2D6 model substrate, to its major metabolite, dextrorphan.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4589" section="34073-7">
      

      <SentenceText>There was no statistically significant change in the urinary dextromethorphan/dextrorphan ratio.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4590" section="34073-7">
      

      <SentenceText>A pharmacokinetic interaction of ziprasidone with valproate is unlikely due to the lack of common metabolic pathways for the two drugs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4591" section="34073-7">
      

      <SentenceText>Ziprasidone dosed adjunctively to valproate in a maintenance trial of bipolar patients did not affect mean therapeutic valproate levels.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="119 9" str="valproate" type="Precipitant"/>
      <Interaction effect="C54357" id="I16" precipitant="M27" trigger="M27" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4592" section="34073-7">
      

      <SentenceText>Population pharmacokinetic analysis of schizophrenic patients enrolled in controlled clinical trials has not revealed evidence of any clinically significant pharmacokinetic interactions with benztropine, propranolol, or lorazepam.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4593" section="34073-7">
      

      <SentenceText>The absolute bioavailability of a 20 mg dose under fed conditions is approximately 60%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4594" section="34073-7">
      

      <SentenceText>The absorption of ziprasidone is increased up to two-fold in the presence of food.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="0 3" str="The" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4595" section="34090-1">
      

      <SentenceText>The mechanism of action of ziprasidone, as with other drugs having efficacy in schizophrenia, is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4596" section="34090-1">
      

      <SentenceText>However, it has been proposed that this drug's efficacy in schizophrenia is mediated through a combination of dopamine type 2 (D 2) and serotonin type 2 (5HT 2) antagonism.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4597" section="34090-1">
      

      <SentenceText>As with other drugs having efficacy in bipolar disorder, the mechanism of action of ziprasidone in bipolar disorder is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4598" section="34090-1">
      

      <SentenceText>Ziprasidone exhibited high in vitro binding affinity for the dopamine D2 and D3, the serotonin 5HT2A, 5HT2C, 5HT1A, 5HT1D, and α1-adrenergic receptors (Ki s of 4.8, 7.2, 0.4, 1.3, 3.4, 2, and 10 nM, respectively), and moderate affinity for the histamine H1 receptor (Ki=47 nM).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4599" section="34090-1">
      

      <SentenceText>Ziprasidone functioned as an antagonist at the D 2, 5HT 2A, and 5HT 1D receptors, and as an agonist at the 5HT 1A receptor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4600" section="34090-1">
      

      <SentenceText>Ziprasidone inhibited synaptic reuptake of serotonin and norepinephrine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4601" section="34090-1">
      

      <SentenceText>No appreciable affinity was exhibited for other receptor/binding sites tested, including the cholinergic muscarinic receptor (IC50 &gt;1 µM).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4602" section="34090-1">
      

      <SentenceText>Antagonism at receptors other than dopamine and 5HT 2 with similar receptor affinities may explain some of the other therapeutic and side effects of ziprasidone.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="0 10" str="Antagonism" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M30" span="35 8" str="dopamine" type="Precipitant"/>
      <Interaction effect="M29" id="I17" precipitant="M30" trigger="M30" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M31" span="67 19" str="receptor affinities" type="Precipitant"/>
      <Interaction effect="M29" id="I18" precipitant="M31" trigger="M31" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4603" section="34090-1">
      

      <SentenceText>Ziprasidone's antagonism of histamine H 1 receptors may explain the somnolence observed with this drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4604" section="34090-1">
      

      <SentenceText>Ziprasidone's antagonism of α1-adrenergic receptors may explain the orthostatic hypotension observed with this drug.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="68 23" str="orthostatic hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M33" span="31 10" str="adrenergic" type="Precipitant"/>
      <Interaction effect="M32" id="I19" precipitant="M33" trigger="M33" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4605" section="34090-1">
      

      <SentenceText>Oral Pharmacokinetics Ziprasidone's activity is primarily due to the parent drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4606" section="34090-1">
      

      <SentenceText>The multiple-dose pharmacokinetics of ziprasidone are dose-proportional within the proposed clinical dose range, and ziprasidone accumulation is predictable with multiple dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4607" section="34090-1">
      

      <SentenceText>Elimination of ziprasidone is mainly via hepatic metabolism with a mean terminal half-life of about 7 hours within the proposed clinical dose range.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4608" section="34090-1">
      

      <SentenceText>Steady-state concentrations are achieved within one to three days of dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4609" section="34090-1">
      

      <SentenceText>The mean apparent systemic clearance is 7.5 mL/min/kg.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4610" section="34090-1">
      

      <SentenceText>Ziprasidone is unlikely to interfere with the metabolism of drugs metabolized by cytochrome P450 enzymes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4611" section="34090-1">
      

      <SentenceText>Absorption: Ziprasidone is well absorbed after oral administration, reaching peak plasma concentrations in 6 to 8 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4614" section="34090-1">
      

      <SentenceText>Distribution: Ziprasidone has a mean apparent volume of distribution of 1.5 L/kg.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4615" section="34090-1">
      

      <SentenceText>It is greater than 99% bound to plasma proteins, binding primarily to albumin and α1-acid glycoprotein.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4616" section="34090-1">
      

      <SentenceText>The in vitro plasma protein binding of ziprasidone was not altered by warfarin or propranolol, two highly protein-bound drugs, nor did ziprasidone alter the binding of these drugs in human plasma.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4617" section="34090-1">
      

      <SentenceText>Thus, the potential for drug interactions with ziprasidone due to displacement is minimal.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4618" section="34090-1">
      

      <SentenceText>Metabolism and Elimination: Ziprasidone is extensively metabolized after oral administration with only a small amount excreted in the urine (&lt;1%) or feces (&lt;4%) as unchanged drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4619" section="34090-1">
      

      <SentenceText>Ziprasidone is primarily cleared via three metabolic routes to yield four major circulating metabolites, benzisothiazole (BITP) sulphoxide, BITP-sulphone, ziprasidone sulphoxide, and S-methyldihydroziprasidone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4620" section="34090-1">
      

      <SentenceText>Approximately 20% of the dose is excreted in the urine, with approximately 66% being eliminated in the feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4621" section="34090-1">
      

      <SentenceText>Unchanged ziprasidone represents about 44% of total drug-related material in serum.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4622" section="34090-1">
      

      <SentenceText>In vitro studies using human liver subcellular fractions indicate that S-methyldihydroziprasidone is generated in two steps.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4623" section="34090-1">
      

      <SentenceText>These studies indicate that the reduction reaction is mediated primarily by chemical reduction by glutathione as well as by enzymatic reduction by aldehyde oxidase and the subsequent methylation is mediated by thiol methyltransferase.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="156 7" str="oxidase" type="Precipitant"/>
      <Interaction effect="C54358" id="I20" precipitant="M34" trigger="M34" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M35" span="210 23" str="thiol methyltransferase" type="Precipitant"/>
      <Interaction effect="C54358" id="I21" precipitant="M35" trigger="M35" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4624" section="34090-1">
      

      <SentenceText>In vitro studies using human liver microsomes and recombinant enzymes indicate that CYP3A4 is the major CYP contributing to the oxidative metabolism of ziprasidone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4625" section="34090-1">
      

      <SentenceText>CYP1A2 may contribute to a much lesser extent.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4626" section="34090-1">
      

      <SentenceText>Based on in vivo abundance of excretory metabolites, less than one-third of ziprasidone metabolic clearance is mediated by cytochrome P450 catalyzed oxidation and approximately two-thirds via reduction.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="188 13" str="via reduction" type="Trigger"/>
      <Mention code="NO MAP" id="M37" span="123 35" str="cytochrome P450 catalyzed oxidation" type="Precipitant"/>
      <Interaction effect="C54357" id="I22" precipitant="M37" trigger="M36" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4628" section="34090-1">
      

      <SentenceText>Intramuscular Pharmacokinetics Systemic Bioavailability: The bioavailability of ziprasidone administered intramuscularly is 100%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4629" section="34090-1">
      

      <SentenceText>After intramuscular administration of single doses, peak serum concentrations typically occur at approximately 60 minutes post-dose or earlier and the mean half-life (T½) ranges from two to five hours.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="161 4" str="life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4630" section="34090-1">
      

      <SentenceText>Exposure increases in a dose-related manner and following three days of intramuscular dosing, little accumulation is observed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Ziprasidone" id="4631" section="34090-1">
      

      <SentenceText>Metabolism and Elimination: Although the metabolism and elimination of IM ziprasidone have not been systematically evaluated, the intramuscular route of administration would not be expected to alter the metabolic pathways.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

