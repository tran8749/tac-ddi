Fetzima	5338	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	30
Other than CYP3A4 drug interactions, FETZIMA is predicted, based on in vitro studies, to have a low potential to be involved in clinically significant pharmacokinetic drug interactions.
1	0	4	O	Other
2	6	9	O	than
3	11	16	O	CYP3A4
4	18	21	O	drug
5	23	34	O	interactions
6	35	35	O	,
7	37	43	O	XXXXXXXX
8	45	46	O	is
9	48	56	O	predicted
10	57	57	O	,
11	59	63	O	based
12	65	66	O	on
13	68	69	O	in
14	71	75	O	vitro
15	77	83	O	studies
16	84	84	O	,
17	86	87	O	to
18	89	92	O	have
19	94	94	O	a
20	96	98	O	low
21	100	108	O	potential
22	110	111	O	to
23	113	114	O	be
24	116	123	O	involved
25	125	126	O	in
26	128	137	O	clinically
27	139	149	O	significant
28	151	165	O	pharmacokinetic
29	167	170	O	drug
30	172	183	O	interactions
NULL

Fetzima	5339	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	15
Strong CYP3A4 inhibitors such as ketoconazole: Do not exceed 80 mg once daily (7).
1	0	5	O	Strong
2	7	12	O	CYP3A4
3	14	23	O	inhibitors
4	25	28	O	such
5	30	31	O	as
6	33	44	O	ketoconazole
7	45	45	O	:
8	47	48	O	Do
9	50	52	O	not
10	54	59	O	exceed
11	61	62	O	80
12	64	65	O	mg
13	67	70	O	once
14	72	76	O	daily
15	79	79	O	7
NULL

Fetzima	5340	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	10
Serotonin release by platelets plays an important role in hemostasis.
1	0	8	O	Serotonin
2	10	16	O	release
3	18	19	O	by
4	21	29	O	platelets
5	31	35	O	plays
6	37	38	O	an
7	40	48	O	important
8	50	53	O	role
9	55	56	O	in
10	58	67	O	hemostasis
NULL

Fetzima	5341	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	29
Epidemiological studies of case-control and cohort design have demonstrated an association between use of psychotropic drugs that interfere with serotonin reuptake and the occurrence of upper gastrointestinal bleeding.
1	0	14	O	Epidemiological
2	16	22	O	studies
3	24	25	O	of
4	27	30	O	case
5	32	38	O	control
6	40	42	O	and
7	44	49	O	cohort
8	51	56	O	design
9	58	61	O	have
10	63	74	O	demonstrated
11	76	77	O	an
12	79	89	O	association
13	91	97	O	between
14	99	101	O	use
15	103	104	O	of
16	106	117	B-D	psychotropic
17	119	123	I-D	drugs
18	125	128	I-D	that
19	130	138	I-D	interfere
20	140	143	I-D	with
21	145	153	I-D	serotonin
22	155	162	I-D	reuptake
23	164	166	O	and
24	168	170	O	the
25	172	181	O	occurrence
26	183	184	O	of
27	186	190	B-E	upper
28	192	207	I-E	gastrointestinal
29	209	216	I-E	bleeding
D/16:27:1

Fetzima	5342	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	19
These studies have also shown that concurrent use of an NSAID or aspirin may potentiate this risk of bleeding.
1	0	4	O	These
2	6	12	O	studies
3	14	17	O	have
4	19	22	O	also
5	24	28	O	shown
6	30	33	O	that
7	35	44	O	concurrent
8	46	48	O	use
9	50	51	O	of
10	53	54	O	an
11	56	60	B-D	NSAID
12	62	63	O	or
13	65	71	B-D	aspirin
14	73	75	O	may
15	77	86	B-E	potentiate
16	88	91	I-E	this
17	93	96	I-E	risk
18	98	99	I-E	of
19	101	108	B-E	bleeding
D/11:15:1 D/11:19:1 D/13:15:1 D/13:19:1

Fetzima	5343	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	20
Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs and SNRIs are co-administered with warfarin.
1	0	6	O	Altered
2	8	20	O	anticoagulant
3	22	28	O	effects
4	29	29	O	,
5	31	39	O	including
6	41	49	B-E	increased
7	51	58	I-E	bleeding
8	59	59	O	,
9	61	64	O	have
10	66	69	O	been
11	71	78	O	reported
12	80	83	O	when
13	85	89	B-D	SSRIs
14	91	93	O	and
15	95	99	O	SNRIs
16	101	103	O	are
17	105	106	O	co
18	108	119	O	administered
19	121	124	O	with
20	126	133	B-D	warfarin
D/13:6:1 D/20:6:1

Fetzima	5344	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	14
Patients receiving warfarin therapy should be carefully monitored when FETZIMA is initiated or discontinued.
1	0	7	O	Patients
2	9	17	O	receiving
3	19	26	B-U	warfarin
4	28	34	O	therapy
5	36	41	O	should
6	43	44	O	be
7	46	54	O	carefully
8	56	64	B-T	monitored
9	66	69	O	when
10	71	77	O	XXXXXXXX
11	79	80	O	is
12	82	90	O	initiated
13	92	93	O	or
14	95	106	O	discontinued
NULL

Fetzima	5345	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	16
Dose adjustment is recommended when FETZIMA is co-administered with strong inhibitors of CYP3A4 (e.g. ketoconazole).
1	0	3	B-T	Dose
2	5	14	O	adjustment
3	16	17	O	is
4	19	29	O	recommended
5	31	34	O	when
6	36	42	O	XXXXXXXX
7	44	45	O	is
8	47	48	O	co
9	50	61	O	administered
10	63	66	O	with
11	68	73	B-U	strong
12	75	84	I-U	inhibitors
13	86	87	I-U	of
14	89	94	I-U	CYP3A4
15	97	99	O	e.g
16	102	113	B-U	ketoconazole
NULL

Fetzima	5346	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	22
An in vivo study showed a clinically meaningful increase in levomilnacipran exposure when FETZIMA was co-administered with the CYP3A4 inhibitor ketoconazole.
1	0	1	O	An
2	3	4	O	in
3	6	9	O	vivo
4	11	15	O	study
5	17	22	O	showed
6	24	24	O	a
7	26	35	O	clinically
8	37	46	O	meaningful
9	48	55	O	increase
10	57	58	O	in
11	60	74	O	XXXXXXXX
12	76	83	O	exposure
13	85	88	O	when
14	90	96	O	XXXXXXXX
15	98	100	O	was
16	102	103	O	co
17	105	116	O	administered
18	118	121	O	with
19	123	125	O	the
20	127	132	O	CYP3A4
21	134	142	O	inhibitor
22	144	155	O	ketoconazole
NULL

Fetzima	5347	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	16
No dose adjustment of FETZIMA is needed when co-administered with a CYP3A4 inducer or substrate.
1	0	1	O	No
2	3	6	O	dose
3	8	17	O	adjustment
4	19	20	O	of
5	22	28	O	XXXXXXXX
6	30	31	O	is
7	33	38	O	needed
8	40	43	O	when
9	45	46	O	co
10	48	59	O	administered
11	61	64	O	with
12	66	66	O	a
13	68	73	B-U	CYP3A4
14	75	81	I-U	inducer
15	83	84	O	or
16	86	94	B-U	substrate
NULL

Fetzima	5348	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	24
In vivo studies showed no clinically meaningful change in levomilnacipran exposure when co-administered with the CYP3A4 inducer carbamazepine or the CYP3A4 substrate alprazolam.
1	0	1	O	In
2	3	6	O	vivo
3	8	14	O	studies
4	16	21	O	showed
5	23	24	O	no
6	26	35	O	clinically
7	37	46	O	meaningful
8	48	53	O	change
9	55	56	O	in
10	58	72	O	XXXXXXXX
11	74	81	O	exposure
12	83	86	O	when
13	88	89	O	co
14	91	102	O	administered
15	104	107	O	with
16	109	111	O	the
17	113	118	O	CYP3A4
18	120	126	O	inducer
19	128	140	O	carbamazepine
20	142	143	O	or
21	145	147	O	the
22	149	154	O	CYP3A4
23	156	164	O	substrate
24	166	175	B-K	alprazolam
K/24:C54358

Fetzima	5349	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	36
No dose adjustment of FETZIMA is needed when co-administered with inhibitors of CYP2C8 , CYP2C19, CYP2D6, CYP2J2, P-glycoprotein, BCRP, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2.
1	0	1	O	No
2	3	6	O	dose
3	8	17	O	adjustment
4	19	20	O	of
5	22	28	O	XXXXXXXX
6	30	31	O	is
7	33	38	O	needed
8	40	43	O	when
9	45	46	O	co
10	48	59	O	administered
11	61	64	O	with
12	66	75	B-U	inhibitors
13	77	78	O	of
14	80	85	O	CYP2C8
15	87	87	O	,
16	89	95	O	CYP2C19
17	96	96	O	,
18	98	103	O	CYP2D6
19	104	104	O	,
20	106	111	O	CYP2J2
21	112	112	O	,
22	114	114	O	P
23	116	127	O	glycoprotein
24	128	128	O	,
25	130	133	O	BCRP
26	134	134	O	,
27	136	142	O	OATP1B1
28	143	143	O	,
29	145	151	O	OATP1B3
30	152	152	O	,
31	154	157	O	OAT1
32	158	158	O	,
33	160	163	O	OAT3
34	164	164	O	,
35	166	167	O	or
36	169	172	O	OCT2
NULL

Fetzima	5350	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	20
In vitro studies suggested that CYP2C8, CYP2C19, CYP2D6, and CYP2J2 had minimal contributions to metabolism of levomilnacipran.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	25	O	suggested
5	27	30	O	that
6	32	37	O	CYP2C8
7	38	38	O	,
8	40	46	O	CYP2C19
9	47	47	O	,
10	49	54	O	CYP2D6
11	55	55	O	,
12	57	59	O	and
13	61	66	O	CYP2J2
14	68	70	O	had
15	72	78	O	minimal
16	80	92	O	contributions
17	94	95	O	to
18	97	106	O	metabolism
19	108	109	O	of
20	111	125	O	XXXXXXXX
NULL

Fetzima	5351	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	29
In addition, levomilnacipran is not a substrate of BCRP, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2 and is a weak substrate of P-gp.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	27	O	XXXXXXXX
5	29	30	O	is
6	32	34	O	not
7	36	36	O	a
8	38	46	O	substrate
9	48	49	O	of
10	51	54	O	BCRP
11	55	55	O	,
12	57	63	O	OATP1B1
13	64	64	O	,
14	66	72	O	OATP1B3
15	73	73	O	,
16	75	78	O	OAT1
17	79	79	O	,
18	81	84	O	OAT3
19	85	85	O	,
20	87	88	O	or
21	90	93	O	OCT2
22	95	97	O	and
23	99	100	O	is
24	102	102	O	a
25	104	107	O	weak
26	109	117	O	substrate
27	119	120	O	of
28	122	122	O	P
29	124	125	B-K	gp
K/29:C54358

Fetzima	5352	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	58
Figure 1 PK Interactions between Levomilnacipran (LVM) and Other Drugs Figure 1 No dose adjustment of the concomitant medication is recommended when FETZIMA is administered with a substrate of CYP3A4, CYP1A2, CYP2A6, CYP2C8, CYP2C9, CYP2C19, CYP2D6, CYP2E1, P-gp, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2.
1	0	5	O	Figure
2	7	7	O	1
3	9	10	O	PK
4	12	23	O	Interactions
5	25	31	O	between
6	33	47	O	XXXXXXXX
7	50	52	O	LVM
8	55	57	O	and
9	59	63	O	Other
10	65	69	O	Drugs
11	71	76	O	Figure
12	78	78	O	1
13	80	81	O	No
14	83	86	O	dose
15	88	97	O	adjustment
16	99	100	O	of
17	102	104	O	the
18	106	116	O	concomitant
19	118	127	O	medication
20	129	130	O	is
21	132	142	O	recommended
22	144	147	O	when
23	149	155	O	XXXXXXXX
24	157	158	O	is
25	160	171	O	administered
26	173	176	O	with
27	178	178	O	a
28	180	188	O	substrate
29	190	191	O	of
30	193	198	O	CYP3A4
31	199	199	O	,
32	201	206	O	CYP1A2
33	207	207	O	,
34	209	214	O	CYP2A6
35	215	215	O	,
36	217	222	O	CYP2C8
37	223	223	O	,
38	225	230	O	CYP2C9
39	231	231	O	,
40	233	239	O	CYP2C19
41	240	240	O	,
42	242	247	O	CYP2D6
43	248	248	O	,
44	250	255	O	CYP2E1
45	256	256	O	,
46	258	258	O	P
47	260	261	O	gp
48	262	262	O	,
49	264	270	O	OATP1B1
50	271	271	O	,
51	273	279	O	OATP1B3
52	280	280	O	,
53	282	285	O	OAT1
54	286	286	O	,
55	288	291	O	OAT3
56	292	292	O	,
57	294	295	O	or
58	297	300	O	OCT2
NULL

Fetzima	5353	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	39
In vitro studies have shown that levomilnacipran is not an inhibitor of CYP1A2, CYP2A6, CYP2C8, CYP2C9, CYP2C19, CYP2D6, CYP2E1, P-gp, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	20	O	have
5	22	26	O	shown
6	28	31	O	that
7	33	47	O	XXXXXXXX
8	49	50	O	is
9	52	54	O	not
10	56	57	O	an
11	59	67	O	inhibitor
12	69	70	O	of
13	72	77	O	CYP1A2
14	78	78	O	,
15	80	85	O	CYP2A6
16	86	86	O	,
17	88	93	O	CYP2C8
18	94	94	O	,
19	96	101	O	CYP2C9
20	102	102	O	,
21	104	110	O	CYP2C19
22	111	111	O	,
23	113	118	O	CYP2D6
24	119	119	O	,
25	121	126	O	CYP2E1
26	127	127	O	,
27	129	129	O	P
28	131	132	O	gp
29	133	133	O	,
30	135	141	O	OATP1B1
31	142	142	O	,
32	144	150	O	OATP1B3
33	151	151	O	,
34	153	156	O	OAT1
35	157	157	O	,
36	159	162	O	OAT3
37	163	163	O	,
38	165	166	O	or
39	168	171	O	OCT2
NULL

Fetzima	5354	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	23
Concomitant use of FETZIMA with alprazolam or carbamazepine, substrates of CYP3A4, had no significant effect on alprazolam or carbamazepine plasma concentrations.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	25	O	XXXXXXXX
5	27	30	O	with
6	32	41	B-K	alprazolam
7	43	44	O	or
8	46	58	B-K	carbamazepine
9	59	59	O	,
10	61	70	O	substrates
11	72	73	O	of
12	75	80	O	CYP3A4
13	81	81	O	,
14	83	85	O	had
15	87	88	O	no
16	90	100	O	significant
17	102	107	O	effect
18	109	110	O	on
19	112	121	B-K	alprazolam
20	123	124	O	or
21	126	138	B-K	carbamazepine
22	140	145	O	plasma
23	147	160	O	concentrations
K/6:C54355 K/8:C54355 K/19:C54355 K/21:C54355

Fetzima	5355	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	17
The risk of using FETZIMA in combination with other CNS-active drugs has not been systematically evaluated.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	16	O	using
5	18	24	O	XXXXXXXX
6	26	27	O	in
7	29	39	O	combination
8	41	44	O	with
9	46	50	O	other
10	52	54	O	CNS
11	56	61	O	active
12	63	67	O	drugs
13	69	71	O	has
14	73	75	O	not
15	77	80	O	been
16	82	95	O	systematically
17	97	105	O	evaluated
NULL

Fetzima	5356	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	25
Consequently, caution is advised when FETZIMA is prescribed in combination with other CNS-active drugs, including those with a similar mechanism of action.
1	0	11	O	Consequently
2	12	12	O	,
3	14	20	B-T	caution
4	22	23	O	is
5	25	31	O	advised
6	33	36	O	when
7	38	44	O	XXXXXXXX
8	46	47	O	is
9	49	58	O	prescribed
10	60	61	O	in
11	63	73	O	combination
12	75	78	O	with
13	80	84	O	other
14	86	88	B-U	CNS
15	90	95	I-U	active
16	97	101	I-U	drugs
17	102	102	O	,
18	104	112	O	including
19	114	118	O	those
20	120	123	O	with
21	125	125	O	a
22	127	133	O	similar
23	135	143	O	mechanism
24	145	146	O	of
25	148	153	O	action
NULL

Fetzima	5357	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	16
Alcohol In an in vitro study, alcohol interacted with the extended-release properties of FETZIMA.
1	0	6	B-D	Alcohol
2	8	9	O	In
3	11	12	O	an
4	14	15	O	in
5	17	21	O	vitro
6	23	27	O	study
7	28	28	O	,
8	30	36	B-D	alcohol
9	38	47	O	interacted
10	49	52	O	with
11	54	56	O	the
12	58	65	O	extended
13	67	73	O	release
14	75	84	O	properties
15	86	87	O	of
16	89	95	O	XXXXXXXX
NULL

Fetzima	5358	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	14
If FETZIMA is taken with alcohol, a pronounced accelerated drug release may occur.
1	0	1	O	If
2	3	9	O	XXXXXXXX
3	11	12	O	is
4	14	18	O	taken
5	20	23	O	with
6	25	31	B-D	alcohol
7	32	32	O	,
8	34	34	O	a
9	36	45	O	pronounced
10	47	57	O	accelerated
11	59	62	O	drug
12	64	70	O	release
13	72	74	O	may
14	76	80	O	occur
NULL

Fetzima	5359	34073-7	f371258d-91b3-4b6a-ac99-434a1964c3af	13
It is recommended that FETZIMA extended-release capsules not be taken with alcohol.
1	0	1	O	It
2	3	4	O	is
3	6	16	O	recommended
4	18	21	O	that
5	23	29	O	XXXXXXXX
6	31	38	O	extended
7	40	46	O	release
8	48	55	O	capsules
9	57	59	O	not
10	61	62	O	be
11	64	68	O	taken
12	70	73	O	with
13	75	81	O	alcohol
NULL

Fetzima	5360	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	40
The exact mechanism of the antidepressant action of levomilnacipran is unknown, but is thought to be related to the potentiation of serotonin and norepinephrine in the central nervous system, through inhibition of reuptake at serotonin and norepinephrine transporters.
1	0	2	O	The
2	4	8	O	exact
3	10	18	O	mechanism
4	20	21	O	of
5	23	25	O	the
6	27	40	O	antidepressant
7	42	47	O	action
8	49	50	O	of
9	52	66	O	XXXXXXXX
10	68	69	O	is
11	71	77	O	unknown
12	78	78	O	,
13	80	82	O	but
14	84	85	O	is
15	87	93	O	thought
16	95	96	O	to
17	98	99	O	be
18	101	107	O	related
19	109	110	O	to
20	112	114	O	the
21	116	127	O	potentiation
22	129	130	O	of
23	132	140	O	serotonin
24	142	144	O	and
25	146	159	O	norepinephrine
26	161	162	O	in
27	164	166	O	the
28	168	174	O	central
29	176	182	O	nervous
30	184	189	O	system
31	190	190	O	,
32	192	198	O	through
33	200	209	O	inhibition
34	211	212	O	of
35	214	221	O	reuptake
36	223	224	O	at
37	226	234	O	serotonin
38	236	238	O	and
39	240	253	O	norepinephrine
40	255	266	O	transporters
NULL

Fetzima	5361	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	18
Non-clinical studies have shown that levomilnacipran is a potent and selective serotonin and norepinephrine reuptake inhibitor (SNRI).
1	0	2	O	Non
2	4	11	O	clinical
3	13	19	O	studies
4	21	24	O	have
5	26	30	O	shown
6	32	35	O	that
7	37	51	O	XXXXXXXX
8	53	54	O	is
9	56	56	O	a
10	58	63	O	potent
11	65	67	O	and
12	69	77	O	selective
13	79	87	O	serotonin
14	89	91	O	and
15	93	106	O	norepinephrine
16	108	115	O	reuptake
17	117	125	O	inhibitor
18	128	131	O	SNRI
NULL

Fetzima	5362	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	40
Levomilnacipran binds with high affinity to the human serotonin (5-HT) and norepinephrine (NE) transporters (Ki = 11 and 91 nM, respectively) and potently inhibits 5-HT and NE reuptake (IC50 = 16-19 and 11 nM, respectively).
1	0	14	O	XXXXXXXX
2	16	20	O	binds
3	22	25	O	with
4	27	30	O	high
5	32	39	O	affinity
6	41	42	O	to
7	44	46	O	the
8	48	52	O	human
9	54	62	O	serotonin
10	65	65	O	5
11	67	68	O	HT
12	71	73	O	and
13	75	88	O	norepinephrine
14	91	92	O	NE
15	95	106	O	transporters
16	109	110	O	Ki
17	112	112	O	=
18	114	115	O	11
19	117	119	O	and
20	121	122	O	91
21	124	125	O	nM
22	126	126	O	,
23	128	139	O	respectively
24	142	144	O	and
25	146	153	O	potently
26	155	162	O	inhibits
27	164	164	O	5
28	166	167	O	HT
29	169	171	O	and
30	173	174	O	NE
31	176	183	O	reuptake
32	186	189	O	IC50
33	191	191	O	=
34	193	194	O	16
35	196	197	O	19
36	199	201	O	and
37	203	204	O	11
38	206	207	O	nM
39	208	208	O	,
40	210	221	O	respectively
NULL

Fetzima	5363	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	41
Levomilnacipran lacks significant affinity for any other receptors, ion channels or transporters tested in vitro, including serotonergic (5HT1-7), A- and B- adrenergic, muscarinic, or histaminergic receptors and Ca2+, Na+, K+ or Cl- channels.
1	0	14	O	XXXXXXXX
2	16	20	O	lacks
3	22	32	O	significant
4	34	41	O	affinity
5	43	45	O	for
6	47	49	O	any
7	51	55	O	other
8	57	65	O	receptors
9	66	66	O	,
10	68	70	O	ion
11	72	79	B-D	channels
12	81	82	O	or
13	84	95	O	transporters
14	97	102	O	tested
15	104	105	O	in
16	107	111	O	vitro
17	112	112	O	,
18	114	122	O	including
19	124	135	O	serotonergic
20	138	141	O	5HT1
21	143	143	O	7
22	145	145	O	,
23	147	147	O	A
24	150	152	O	and
25	154	154	B-D	B
26	157	166	I-D	adrenergic
27	167	167	O	,
28	169	178	O	muscarinic
29	179	179	O	,
30	181	182	O	or
31	184	196	B-D	histaminergic
32	198	206	I-D	receptors
33	208	210	O	and
34	212	215	B-D	Ca2+
35	216	216	O	,
36	218	220	O	Na+
37	221	221	O	,
38	223	224	O	K+
39	226	227	O	or
40	229	230	O	Cl
41	233	240	O	channels
NULL

Fetzima	5364	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	7
Levomilnacipran did not inhibit monoamine oxidase (MAO).
1	0	14	O	XXXXXXXX
2	16	18	O	did
3	20	22	O	not
4	24	30	O	inhibit
5	32	40	O	monoamine
6	42	48	O	oxidase
7	51	53	O	MAO
NULL

Fetzima	5365	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	22
Cardiovascular Electrophysiology At a dose 2.5 times the maximum recommended dose, levomilnacipran does not prolong QTc to any clinically relevant extent.
1	0	13	O	Cardiovascular
2	15	31	O	Electrophysiology
3	33	34	O	At
4	36	36	O	a
5	38	41	O	dose
6	43	45	O	2.5
7	47	51	O	times
8	53	55	O	the
9	57	63	O	maximum
10	65	75	O	recommended
11	77	80	O	dose
12	81	81	O	,
13	83	97	O	XXXXXXXX
14	99	102	O	does
15	104	106	O	not
16	108	114	O	prolong
17	116	118	O	QTc
18	120	121	O	to
19	123	125	O	any
20	127	136	O	clinically
21	138	145	O	relevant
22	147	152	O	extent
NULL

Fetzima	5366	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	20
The concentration of levomilnacipran at steady state is proportional to dose when administered from 25 to 300 mg once daily.
1	0	2	O	The
2	4	16	O	concentration
3	18	19	O	of
4	21	35	O	XXXXXXXX
5	37	38	O	at
6	40	45	O	steady
7	47	51	O	state
8	53	54	O	is
9	56	67	O	proportional
10	69	70	O	to
11	72	75	O	dose
12	77	80	O	when
13	82	93	O	administered
14	95	98	O	from
15	100	101	O	25
16	103	104	O	to
17	106	108	O	300
18	110	111	O	mg
19	113	116	O	once
20	118	122	O	daily
NULL

Fetzima	5367	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	18
Following an oral administration, the mean apparent total clearance of levomilnacipran is 21-29 L/h.
1	0	8	O	Following
2	10	11	O	an
3	13	16	O	oral
4	18	31	O	administration
5	32	32	O	,
6	34	36	O	the
7	38	41	O	mean
8	43	50	O	apparent
9	52	56	O	total
10	58	66	O	clearance
11	68	69	O	of
12	71	85	O	XXXXXXXX
13	87	88	O	is
14	90	91	O	21
15	93	94	O	29
16	96	96	O	L
17	97	97	O	/
18	98	98	O	h
NULL

Fetzima	5368	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	11
Steady-state concentrations of levomilnacipran are predictable from single-dose data.
1	0	5	O	Steady
2	7	11	O	state
3	13	26	O	concentrations
4	28	29	O	of
5	31	45	O	XXXXXXXX
6	47	49	O	are
7	51	61	O	predictable
8	63	66	O	from
9	68	73	O	single
10	75	78	O	dose
11	80	83	O	data
NULL

Fetzima	5369	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	12
The apparent terminal elimination half-life of levomilnacipran is approximately 12 hours.
1	0	2	O	The
2	4	11	O	apparent
3	13	20	O	terminal
4	22	32	O	elimination
5	34	37	O	half
6	39	42	O	life
7	44	45	O	of
8	47	61	O	XXXXXXXX
9	63	64	O	is
10	66	78	O	approximately
11	80	81	O	12
12	83	87	O	hours
NULL

Fetzima	5370	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	30
After daily dosing of FETZIMA 120 mg, the mean Cmax value is 341 ng/mL, and the mean steady-state AUC value is 5196 ng*h/mL.
1	0	4	O	After
2	6	10	O	daily
3	12	17	O	dosing
4	19	20	O	of
5	22	28	O	XXXXXXXX
6	30	32	O	120
7	34	35	O	mg
8	36	36	O	,
9	38	40	O	the
10	42	45	O	mean
11	47	50	O	Cmax
12	52	56	O	value
13	58	59	O	is
14	61	63	O	341
15	65	66	O	ng
16	67	67	O	/
17	68	69	O	mL
18	70	70	O	,
19	72	74	O	and
20	76	78	O	the
21	80	83	O	mean
22	85	90	O	steady
23	92	96	O	state
24	98	100	O	AUC
25	102	106	O	value
26	108	109	O	is
27	111	114	O	5196
28	116	119	O	ng*h
29	120	120	O	/
30	121	122	O	mL
NULL

Fetzima	5371	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	11
Interconversion between levomilnacipran and its stereoisomer does not occur in humans.
1	0	14	O	Interconversion
2	16	22	O	between
3	24	38	O	XXXXXXXX
4	40	42	O	and
5	44	46	O	its
6	48	59	O	stereoisomer
7	61	64	O	does
8	66	68	O	not
9	70	74	O	occur
10	76	77	O	in
11	79	84	O	humans
NULL

Fetzima	5372	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	18
Absorption The relative bioavailability of levomilnacipran after administration of FETZIMA ER was 92% when compared to oral solution.
1	0	9	O	Absorption
2	11	13	O	The
3	15	22	O	relative
4	24	38	O	bioavailability
5	40	41	O	of
6	43	57	O	XXXXXXXX
7	59	63	O	after
8	65	78	O	administration
9	80	81	O	of
10	83	89	O	XXXXXXXX
11	91	92	O	ER
12	94	96	O	was
13	98	100	O	92%
14	102	105	O	when
15	107	114	O	compared
16	116	117	O	to
17	119	122	O	oral
18	124	131	B-K	solution
K/18:C54356

Fetzima	5373	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	12
Levomilnacipran concentration was not significantly affected when FETZIMA was administered with food.
1	0	14	O	XXXXXXXX
2	16	28	O	concentration
3	30	32	O	was
4	34	36	O	not
5	38	50	O	significantly
6	52	59	O	affected
7	61	64	O	when
8	66	72	O	XXXXXXXX
9	74	76	O	was
10	78	89	O	administered
11	91	94	O	with
12	96	99	O	food
NULL

Fetzima	5374	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	16
The median time to peak concentration (Tmax) of levomilnacipran is 6-8 hours after oral administration.
1	0	2	O	The
2	4	9	O	median
3	11	14	O	time
4	16	17	O	to
5	19	22	O	peak
6	24	36	O	concentration
7	39	42	O	Tmax
8	45	46	O	of
9	48	62	O	XXXXXXXX
10	64	65	O	is
11	67	67	O	6
12	69	69	O	8
13	71	75	O	hours
14	77	81	O	after
15	83	86	O	oral
16	88	101	O	administration
NULL

Fetzima	5375	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	30
Distribution Levomilnacipran is widely distributed with an apparent volume of distribution of 387-473 L; plasma protein binding is 22% over concentration range of 10 to 1000 ng/mL.
1	0	11	O	Distribution
2	13	27	O	XXXXXXXX
3	29	30	O	is
4	32	37	O	widely
5	39	49	O	distributed
6	51	54	O	with
7	56	57	O	an
8	59	66	O	apparent
9	68	73	O	volume
10	75	76	O	of
11	78	89	O	distribution
12	91	92	O	of
13	94	96	O	387
14	98	100	O	473
15	102	102	O	L
16	105	110	O	plasma
17	112	118	O	protein
18	120	126	O	binding
19	128	129	O	is
20	131	133	O	22%
21	135	138	O	over
22	140	152	O	concentration
23	154	158	O	range
24	160	161	O	of
25	163	164	O	10
26	166	167	O	to
27	169	172	O	1000
28	174	175	O	ng
29	176	176	O	/
30	177	178	O	mL
NULL

Fetzima	5376	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	15
Metabolism Levomilnacipran undergoes desethylation to form desethyl levomilnacipran and hydroxylation to form p-hydroxy-levomilnacipran.
1	0	9	O	Metabolism
2	11	25	O	XXXXXXXX
3	27	35	O	undergoes
4	37	49	O	desethylation
5	51	52	O	to
6	54	57	O	form
7	59	66	O	desethyl
8	68	82	O	XXXXXXXX
9	84	86	O	and
10	88	100	O	hydroxylation
11	102	103	O	to
12	105	108	O	form
13	110	110	O	p
14	112	118	O	hydroxy
15	120	134	O	XXXXXXXX
NULL

Fetzima	5377	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	11
Both oxidative metabolites undergo further conjugation with glucuronide to form conjugates.
1	0	3	O	Both
2	5	13	O	oxidative
3	15	25	O	metabolites
4	27	33	O	undergo
5	35	41	O	further
6	43	53	O	conjugation
7	55	58	O	with
8	60	70	O	glucuronide
9	72	73	O	to
10	75	78	O	form
11	80	89	O	conjugates
NULL

Fetzima	5378	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	19
The desethylation is catalyzed primarily by CYP3A4 with minor contribution by CYP2C8, 2C19, 2D6, and 2J2.
1	0	2	O	The
2	4	16	O	desethylation
3	18	19	O	is
4	21	29	O	catalyzed
5	31	39	O	primarily
6	41	42	O	by
7	44	49	O	CYP3A4
8	51	54	O	with
9	56	60	O	minor
10	62	73	O	contribution
11	75	76	O	by
12	78	83	O	CYP2C8
13	84	84	O	,
14	86	89	O	2C19
15	90	90	O	,
16	92	94	O	2D6
17	95	95	O	,
18	97	99	O	and
19	101	103	O	2J2
NULL

Fetzima	5379	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	13
Elimination/Excretion Levomilnacipran and its metabolites are eliminated primarily by renal excretion.
1	0	10	O	Elimination
2	11	11	O	/
3	12	20	O	Excretion
4	22	36	O	XXXXXXXX
5	38	40	O	and
6	42	44	O	its
7	46	56	O	metabolites
8	58	60	O	are
9	62	71	O	eliminated
10	73	81	O	primarily
11	83	84	O	by
12	86	90	O	renal
13	92	100	O	excretion
NULL

Fetzima	5380	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	20
Following oral administration of 14C-levomilnacipran solution, approximately 58% of the dose is excreted in urine as unchanged levomilnacipran.
1	0	8	O	Following
2	10	13	O	oral
3	15	28	O	administration
4	30	31	O	of
5	33	35	O	14C
6	37	51	O	XXXXXXXX
7	53	60	O	solution
8	61	61	O	,
9	63	75	O	approximately
10	77	79	O	58%
11	81	82	O	of
12	84	86	O	the
13	88	91	O	dose
14	93	94	O	is
15	96	103	O	excreted
16	105	106	O	in
17	108	112	O	urine
18	114	115	O	as
19	117	125	O	unchanged
20	127	141	O	XXXXXXXX
NULL

Fetzima	5381	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	19
N-desethyl levomilnacipran is the major metabolite excreted in the urine and accounted for approximately 18% of the dose.
1	0	0	O	N
2	2	9	O	desethyl
3	11	25	O	XXXXXXXX
4	27	28	O	is
5	30	32	O	the
6	34	38	O	major
7	40	49	O	metabolite
8	51	58	O	excreted
9	60	61	O	in
10	63	65	O	the
11	67	71	O	urine
12	73	75	O	and
13	77	85	O	accounted
14	87	89	O	for
15	91	103	O	approximately
16	105	107	O	18%
17	109	110	O	of
18	112	114	O	the
19	116	119	O	dose
NULL

Fetzima	5382	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	28
Other identifiable metabolites excreted in the urine are levomilnacipran glucuronide (4%), desethyl levomilnacipran glucuronide (3%), p-hydroxy levomilnacipran glucuronide (1%), and p-hydroxy levomilnacipran (1%).
1	0	4	O	Other
2	6	17	O	identifiable
3	19	29	O	metabolites
4	31	38	O	excreted
5	40	41	O	in
6	43	45	O	the
7	47	51	O	urine
8	53	55	O	are
9	57	71	O	XXXXXXXX
10	73	83	O	glucuronide
11	86	87	O	4%
12	89	89	O	,
13	91	98	O	desethyl
14	100	114	O	XXXXXXXX
15	116	126	O	glucuronide
16	129	130	O	3%
17	132	132	O	,
18	134	134	O	p
19	136	142	O	hydroxy
20	144	158	O	XXXXXXXX
21	160	170	O	glucuronide
22	173	174	O	1%
23	176	176	O	,
24	178	180	O	and
25	182	182	O	p
26	184	190	O	hydroxy
27	192	206	O	XXXXXXXX
28	209	210	O	1%
NULL

Fetzima	5383	34090-1	f371258d-91b3-4b6a-ac99-434a1964c3af	4
The metabolites are inactive.
1	0	2	O	The
2	4	14	O	metabolites
3	16	18	O	are
4	20	27	O	inactive
NULL

