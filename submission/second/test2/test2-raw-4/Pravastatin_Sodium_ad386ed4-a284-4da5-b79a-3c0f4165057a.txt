Pravastatin Sodium	1976	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	22
For the concurrent therapy of either cyclosporine, fibrates, niacin (nicotinic acid), or erythromycin, the risk of myopathy increases.
1	0	2	O	For
2	4	6	O	the
3	8	17	O	concurrent
4	19	25	O	therapy
5	27	28	O	of
6	30	35	O	either
7	37	48	B-D	cyclosporine
8	49	49	O	,
9	51	58	B-D	fibrates
10	59	59	O	,
11	61	66	B-D	niacin
12	69	77	I-D	nicotinic
13	79	82	I-D	acid
14	84	84	O	,
15	86	87	O	or
16	89	100	B-D	erythromycin
17	101	101	O	,
18	103	105	O	the
19	107	110	O	risk
20	112	113	O	of
21	115	122	B-E	myopathy
22	124	132	I-E	increases
D/7:21:1 D/9:21:1 D/11:21:1 D/16:21:1

Pravastatin Sodium	1977	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	28
* Concomitant lipid-lowering therapies: use with fibrates or lipid-modifying doses (> 1 g/day) of niacin increases the risk of adverse skeletal muscle effects.
1	0	0	O	*
2	2	12	O	Concomitant
3	14	18	O	lipid
4	20	27	O	lowering
5	29	37	O	therapies
6	38	38	O	:
7	40	42	O	use
8	44	47	O	with
9	49	56	B-D	fibrates
10	58	59	O	or
11	61	65	O	lipid
12	67	75	O	modifying
13	77	81	O	doses
14	84	84	O	>
15	86	86	O	1
16	88	88	O	g
17	89	89	O	/
18	90	92	O	day
19	95	96	O	of
20	98	103	B-D	niacin
21	105	113	O	increases
22	115	117	O	the
23	119	122	O	risk
24	124	125	O	of
25	127	133	O	adverse
26	135	142	B-E	skeletal
27	144	149	I-E	muscle
28	151	157	I-E	effects
D/9:26:1 D/20:26:1

Pravastatin Sodium	1978	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	8
Caution should be used when prescribing with pravastatin sodium.
1	0	6	B-T	Caution
2	8	13	O	should
3	15	16	O	be
4	18	21	O	used
5	23	26	O	when
6	28	38	O	prescribing
7	40	43	O	with
8	45	62	O	XXXXXXXX
NULL

Pravastatin Sodium	1979	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	5
Cyclosporine: combination increases exposure.
1	0	11	B-K	Cyclosporine
2	12	12	O	:
3	14	24	O	combination
4	26	34	B-T	increases
5	36	43	I-T	exposure
K/1:C54355

Pravastatin Sodium	1980	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	5
Clarithromycin: combination increases exposure.
1	0	13	B-K	Clarithromycin
2	14	14	O	:
3	16	26	O	combination
4	28	36	B-T	increases
5	38	45	I-T	exposure
K/1:C54355

Pravastatin Sodium	1981	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	13
The risk of myopathy/rhabdomyolysis is increased with concomitant administration of cyclosporine.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	19	B-E	myopathy
5	20	20	O	/
6	21	34	B-E	rhabdomyolysis
7	36	37	I-E	is
8	39	47	O	increased
9	49	52	O	with
10	54	64	O	concomitant
11	66	79	O	administration
12	81	82	O	of
13	84	95	B-D	cyclosporine
D/13:4:1 D/13:6:1

Pravastatin Sodium	1982	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	12
Limit pravastatin to 20 mg once daily for concomitant use with cyclosporine.
1	0	4	B-T	Limit
2	6	16	I-T	XXXXXXXX
3	18	19	O	to
4	21	22	O	20
5	24	25	O	mg
6	27	30	O	once
7	32	36	O	daily
8	38	40	O	for
9	42	52	O	concomitant
10	54	56	O	use
11	58	61	O	with
12	63	74	B-U	cyclosporine
NULL

Pravastatin Sodium	1983	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	13
The risk of myopathy/rhabdomyolysis is increased with concomitant administration of clarithromycin.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	19	B-E	myopathy
5	20	20	O	/
6	21	34	B-E	rhabdomyolysis
7	36	37	I-E	is
8	39	47	O	increased
9	49	52	O	with
10	54	64	O	concomitant
11	66	79	O	administration
12	81	82	O	of
13	84	97	B-D	clarithromycin
D/13:4:1 D/13:6:1

Pravastatin Sodium	1984	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	12
Limit pravastatin to 40 mg once daily for concomitant use with clarithromycin.
1	0	4	B-T	Limit
2	6	16	O	XXXXXXXX
3	18	19	O	to
4	21	22	O	40
5	24	25	O	mg
6	27	30	O	once
7	32	36	O	daily
8	38	40	O	for
9	42	52	O	concomitant
10	54	56	O	use
11	58	61	O	with
12	63	76	B-U	clarithromycin
NULL

Pravastatin Sodium	1985	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	18
Other macrolides (e.g., erythromycin and azithromycin) have the potential to increase statin exposures while used in combination.
1	0	4	O	Other
2	6	15	O	macrolides
3	18	20	O	e.g
4	22	22	O	,
5	24	35	B-D	erythromycin
6	37	39	O	and
7	41	52	O	azithromycin
8	55	58	O	have
9	60	62	O	the
10	64	72	O	potential
11	74	75	O	to
12	77	84	O	increase
13	86	91	O	statin
14	93	101	O	exposures
15	103	107	O	while
16	109	112	O	used
17	114	115	O	in
18	117	127	O	combination
NULL

Pravastatin Sodium	1986	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	16
Pravastatin should be used cautiously with macrolide antibiotics due to a potential increased risk of myopathies.
1	0	10	O	XXXXXXXX
2	12	17	O	should
3	19	20	O	be
4	22	25	O	used
5	27	36	O	cautiously
6	38	41	O	with
7	43	51	B-D	macrolide
8	53	63	I-D	antibiotics
9	65	67	O	due
10	69	70	O	to
11	72	72	O	a
12	74	82	O	potential
13	84	92	O	increased
14	94	97	O	risk
15	99	100	O	of
16	102	111	B-E	myopathies
D/7:16:1

Pravastatin Sodium	1987	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	13
The risk of myopathy/rhabdomyolysis is increased with concomitant administration of colchicine.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	19	B-E	myopathy
5	20	20	O	/
6	21	34	B-E	rhabdomyolysis
7	36	37	I-E	is
8	39	47	O	increased
9	49	52	O	with
10	54	64	O	concomitant
11	66	79	O	administration
12	81	82	O	of
13	84	93	B-D	colchicine
D/13:4:1 D/13:6:1

Pravastatin Sodium	1988	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	28
Due to an increased risk of myopathy/rhabdomyolysis when HMG-CoA reductase inhibitors are coadministered with gemfibrozil, concomitant administration of pravastatin sodium with gemfibrozil should be avoided.
1	0	2	O	Due
2	4	5	O	to
3	7	8	O	an
4	10	18	O	increased
5	20	23	O	risk
6	25	26	O	of
7	28	35	B-E	myopathy
8	36	36	O	/
9	37	50	B-E	rhabdomyolysis
10	52	55	O	when
11	57	59	B-D	HMG
12	61	63	I-D	CoA
13	65	73	I-D	reductase
14	75	84	I-D	inhibitors
15	86	88	O	are
16	90	103	O	coadministered
17	105	108	O	with
18	110	120	B-D	gemfibrozil
19	121	121	O	,
20	123	133	O	concomitant
21	135	148	O	administration
22	150	151	O	of
23	153	170	O	XXXXXXXX
24	172	175	O	with
25	177	187	B-U	gemfibrozil
26	189	194	O	should
27	196	197	O	be
28	199	205	B-T	avoided
D/11:7:1 D/11:9:1 D/18:7:1 D/18:9:1

Pravastatin Sodium	1989	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	37
Because it is known that the risk of myopathy during treatment with HMG-CoA reductase inhibitors is increased with concurrent administration of other fibrates, pravastatin sodium should be administered with caution when used concomitantly with other fibrates.
1	0	6	O	Because
2	8	9	O	it
3	11	12	O	is
4	14	18	O	known
5	20	23	O	that
6	25	27	O	the
7	29	32	O	risk
8	34	35	O	of
9	37	44	O	myopathy
10	46	51	O	during
11	53	61	O	treatment
12	63	66	O	with
13	68	70	B-D	HMG
14	72	74	I-D	CoA
15	76	84	I-D	reductase
16	86	95	I-D	inhibitors
17	97	98	O	is
18	100	108	O	increased
19	110	113	O	with
20	115	124	O	concurrent
21	126	139	O	administration
22	141	142	O	of
23	144	148	O	other
24	150	157	B-U	fibrates
25	158	158	O	,
26	160	177	O	XXXXXXXX
27	179	184	O	should
28	186	187	O	be
29	189	200	B-T	administered
30	202	205	O	with
31	207	213	B-T	caution
32	215	218	O	when
33	220	223	O	used
34	225	237	O	concomitantly
35	239	242	O	with
36	244	248	O	other
37	250	257	B-U	fibrates
NULL

Pravastatin Sodium	1990	34073-7	ad386ed4-a284-4da5-b79a-3c0f4165057a	28
The risk of skeletal muscle effects may be enhanced when pravastatin is used in combination with niacin; a reduction in pravastatin sodium dosage should be considered in this setting.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	19	O	skeletal
5	21	26	O	muscle
6	28	34	B-E	effects
7	36	38	O	may
8	40	41	B-E	be
9	43	50	I-E	enhanced
10	52	55	O	when
11	57	67	O	XXXXXXXX
12	69	70	O	is
13	72	75	O	used
14	77	78	O	in
15	80	90	O	combination
16	92	95	O	with
17	97	102	B-D	niacin
18	105	105	B-U	a
19	107	115	O	reduction
20	117	118	O	in
21	120	137	O	XXXXXXXX
22	139	144	B-T	dosage
23	146	151	O	should
24	153	154	O	be
25	156	165	O	considered
26	167	168	O	in
27	170	173	O	this
28	175	181	O	setting
D/17:6:1 D/17:8:1

Pravastatin Sodium	1991	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	40
Pravastatin is a reversible inhibitor of 3-hydroxy-3-methylglutaryl-coenzyme A (HMG-CoA) reductase, the enzyme that catalyzes the conversion of HMG-CoA to mevalonate, an early and rate limiting step in the biosynthetic pathway for cholesterol.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	15	O	a
4	17	26	O	reversible
5	28	36	O	inhibitor
6	38	39	O	of
7	41	41	O	3
8	43	49	O	hydroxy
9	51	51	O	3
10	53	66	O	methylglutaryl
11	68	75	O	coenzyme
12	77	77	O	A
13	80	82	O	HMG
14	84	86	O	CoA
15	89	97	O	reductase
16	98	98	O	,
17	100	102	O	the
18	104	109	O	enzyme
19	111	114	O	that
20	116	124	O	catalyzes
21	126	128	O	the
22	130	139	O	conversion
23	141	142	O	of
24	144	146	O	HMG
25	148	150	O	CoA
26	152	153	O	to
27	155	164	O	mevalonate
28	165	165	O	,
29	167	168	O	an
30	170	174	O	early
31	176	178	O	and
32	180	183	O	rate
33	185	192	O	limiting
34	194	197	O	step
35	199	200	O	in
36	202	204	O	the
37	206	217	O	biosynthetic
38	219	225	O	pathway
39	227	229	O	for
40	231	241	O	cholesterol
NULL

Pravastatin Sodium	1992	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	23
In addition, pravastatin reduces VLDL and TG and increases HDL-C. General Absorption:Pravastatin sodiumis administered orally in the active form.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	23	O	XXXXXXXX
5	25	31	O	reduces
6	33	36	O	VLDL
7	38	40	O	and
8	42	43	O	TG
9	45	47	O	and
10	49	57	O	increases
11	59	61	O	HDL
12	63	63	O	C
13	66	72	O	General
14	74	83	O	Absorption
15	84	84	O	:
16	85	102	O	XXXXXXXX
17	103	104	O	is
18	106	117	O	administered
19	119	124	O	orally
20	126	127	O	in
21	129	131	O	the
22	133	138	O	active
23	140	143	O	form
NULL

Pravastatin Sodium	1993	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	17
In studies in man, peak plasma pravastatin concentrations occurred 1 to 1.5 hours upon oral administration.
1	0	1	O	In
2	3	9	O	studies
3	11	12	O	in
4	14	16	O	man
5	17	17	O	,
6	19	22	O	peak
7	24	29	O	plasma
8	31	41	O	XXXXXXXX
9	43	56	O	concentrations
10	58	65	O	occurred
11	67	67	O	1
12	69	70	O	to
13	72	74	O	1.5
14	76	80	O	hours
15	82	85	O	upon
16	87	90	O	oral
17	92	105	O	administration
NULL

Pravastatin Sodium	1994	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	22
Based on urinary recovery of total radiolabeled drug, the average oral absorption of pravastatin is 34% and absolute bioavailability is 17%.
1	0	4	O	Based
2	6	7	O	on
3	9	15	O	urinary
4	17	24	O	recovery
5	26	27	O	of
6	29	33	O	total
7	35	46	O	radiolabeled
8	48	51	O	drug
9	52	52	O	,
10	54	56	O	the
11	58	64	O	average
12	66	69	O	oral
13	71	80	O	absorption
14	82	83	O	of
15	85	95	O	XXXXXXXX
16	97	98	O	is
17	100	102	O	34%
18	104	106	O	and
19	108	115	O	absolute
20	117	131	O	bioavailability
21	133	134	O	is
22	136	138	O	17%
NULL

Pravastatin Sodium	1995	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	31
While the presence of food in the gastrointestinal tract reduces systemic bioavailability, the lipid-lowering effects of the drug are similar whether taken with or 1 hour prior to meals.
1	0	4	O	While
2	6	8	O	the
3	10	17	O	presence
4	19	20	O	of
5	22	25	O	food
6	27	28	O	in
7	30	32	O	the
8	34	49	O	gastrointestinal
9	51	55	O	tract
10	57	63	O	reduces
11	65	72	O	systemic
12	74	88	O	bioavailability
13	89	89	O	,
14	91	93	O	the
15	95	99	O	lipid
16	101	108	O	lowering
17	110	116	O	effects
18	118	119	O	of
19	121	123	O	the
20	125	128	O	drug
21	130	132	O	are
22	134	140	O	similar
23	142	148	O	whether
24	150	154	O	taken
25	156	159	O	with
26	161	162	O	or
27	164	164	O	1
28	166	169	O	hour
29	171	175	O	prior
30	177	178	O	to
31	180	184	O	meals
NULL

Pravastatin Sodium	1996	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	27
Pravastatin plasma concentrations, including area under the concentration-time curve (AUC), Cmax, and steady-state minimum (Cmin), are directly proportional to administered dose.
1	0	10	O	XXXXXXXX
2	12	17	O	plasma
3	19	32	O	concentrations
4	33	33	O	,
5	35	43	O	including
6	45	48	O	area
7	50	54	O	under
8	56	58	O	the
9	60	72	O	concentration
10	74	77	O	time
11	79	83	O	curve
12	86	88	O	AUC
13	90	90	O	,
14	92	95	O	Cmax
15	96	96	O	,
16	98	100	O	and
17	102	107	O	steady
18	109	113	O	state
19	115	121	O	minimum
20	124	127	O	Cmin
21	129	129	O	,
22	131	133	O	are
23	135	142	O	directly
24	144	155	O	proportional
25	157	158	O	to
26	160	171	O	administered
27	173	176	O	dose
NULL

Pravastatin Sodium	1997	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	19
Systemic bioavailability of pravastatin administered following a bedtime dose was decreased 60% compared to that following an AM dose.
1	0	7	O	Systemic
2	9	23	O	bioavailability
3	25	26	O	of
4	28	38	O	XXXXXXXX
5	40	51	O	administered
6	53	61	O	following
7	63	63	O	a
8	65	71	O	bedtime
9	73	76	O	dose
10	78	80	O	was
11	82	90	O	decreased
12	92	94	O	60%
13	96	103	O	compared
14	105	106	O	to
15	108	111	O	that
16	113	121	O	following
17	123	124	O	an
18	126	127	O	AM
19	129	132	O	dose
NULL

Pravastatin Sodium	1998	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	33
Despite this decrease in systemic bioavailability, the efficacy of pravastatin administered once daily in the evening, although not statistically significant, was marginally more effective than that after a morning dose.
1	0	6	O	Despite
2	8	11	O	this
3	13	20	O	decrease
4	22	23	O	in
5	25	32	O	systemic
6	34	48	O	bioavailability
7	49	49	O	,
8	51	53	O	the
9	55	62	O	efficacy
10	64	65	O	of
11	67	77	O	XXXXXXXX
12	79	90	O	administered
13	92	95	O	once
14	97	101	O	daily
15	103	104	O	in
16	106	108	O	the
17	110	116	O	evening
18	117	117	O	,
19	119	126	O	although
20	128	130	O	not
21	132	144	O	statistically
22	146	156	O	significant
23	157	157	O	,
24	159	161	O	was
25	163	172	O	marginally
26	174	177	O	more
27	179	187	O	effective
28	189	192	O	than
29	194	197	O	that
30	199	203	O	after
31	205	205	O	a
32	207	213	O	morning
33	215	218	O	dose
NULL

Pravastatin Sodium	1999	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	18
The coefficient of variation (CV), based on between-subject variability, was 50% to 60% for AUC.
1	0	2	O	The
2	4	14	O	coefficient
3	16	17	O	of
4	19	27	O	variation
5	30	31	O	CV
6	33	33	O	,
7	35	39	O	based
8	41	42	O	on
9	44	50	O	between
10	52	58	O	subject
11	60	70	O	variability
12	71	71	O	,
13	73	75	O	was
14	77	79	O	50%
15	81	82	O	to
16	84	86	O	60%
17	88	90	O	for
18	92	94	O	AUC
NULL

Pravastatin Sodium	2000	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	29
The geometric means of pravastatin Cma xand AUC following a 20 mg dose in the fasted state were 26.5 ng/mL and 59.8 ng*hr/mL, respectively.
1	0	2	O	The
2	4	12	O	geometric
3	14	18	O	means
4	20	21	O	of
5	23	33	O	XXXXXXXX
6	35	37	O	Cma
7	39	42	O	xand
8	44	46	O	AUC
9	48	56	O	following
10	58	58	O	a
11	60	61	O	20
12	63	64	O	mg
13	66	69	O	dose
14	71	72	O	in
15	74	76	O	the
16	78	83	O	fasted
17	85	89	O	state
18	91	94	O	were
19	96	99	O	26.5
20	101	102	O	ng
21	103	103	O	/
22	104	105	O	mL
23	107	109	O	and
24	111	114	O	59.8
25	116	120	O	ng*hr
26	121	121	O	/
27	122	123	O	mL
28	124	124	O	,
29	126	137	O	respectively
NULL

Pravastatin Sodium	2001	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	24
Steady-state AUCs, Cmax, and Cminplasma concentrations showed no evidence of pravastatin accumulation following once or twice daily administration of pravastatin sodium tablets.
1	0	5	O	Steady
2	7	11	O	state
3	13	16	O	AUCs
4	17	17	O	,
5	19	22	O	Cmax
6	23	23	O	,
7	25	27	O	and
8	29	38	O	Cminplasma
9	40	53	O	concentrations
10	55	60	O	showed
11	62	63	O	no
12	65	72	O	evidence
13	74	75	O	of
14	77	87	O	XXXXXXXX
15	89	100	O	accumulation
16	102	110	O	following
17	112	115	O	once
18	117	118	O	or
19	120	124	O	twice
20	126	130	O	daily
21	132	145	O	administration
22	147	148	O	of
23	150	167	O	XXXXXXXX
24	169	175	O	tablets
NULL

Pravastatin Sodium	2002	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	11
Distribution:Approximately 50% of the circulating drug is bound to plasma proteins.
1	0	25	O	Distribution:Approximately
2	27	29	O	50%
3	31	32	O	of
4	34	36	O	the
5	38	48	O	circulating
6	50	53	O	drug
7	55	56	O	is
8	58	62	O	bound
9	64	65	O	to
10	67	72	O	plasma
11	74	81	O	proteins
NULL

Pravastatin Sodium	2003	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	30
Metabolism:The major biotransformation pathways for pravastatin are: (a) isomerization to 6-epi pravastatin and the 3A-hydroxyisomer of pravastatin (SQ 31,906) and (b) enzymatic ring hydroxylation to SQ 31,945.
1	0	13	O	Metabolism:The
2	15	19	O	major
3	21	37	O	biotransformation
4	39	46	O	pathways
5	48	50	O	for
6	52	62	O	XXXXXXXX
7	64	66	O	are
8	67	67	O	:
9	70	70	O	a
10	73	85	O	isomerization
11	87	88	O	to
12	90	90	O	6
13	92	94	O	epi
14	96	106	O	XXXXXXXX
15	108	110	O	and
16	112	114	O	the
17	116	117	O	3A
18	119	131	O	hydroxyisomer
19	133	134	O	of
20	136	146	O	XXXXXXXX
21	149	150	O	SQ
22	152	157	O	31,906
23	160	162	O	and
24	165	165	O	b
25	168	176	B-K	enzymatic
26	178	181	I-K	ring
27	183	195	I-K	hydroxylation
28	197	198	O	to
29	200	201	O	SQ
30	203	208	O	31,945
K/25:C54606

Pravastatin Sodium	2004	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	24
The 3A-hydroxyisomeric metabolite (SQ 31,906) has 1/10 to 1/40 the HMG-CoA reductase inhibitory activity of the parent compound.
1	0	2	O	The
2	4	5	O	3A
3	7	21	O	hydroxyisomeric
4	23	32	O	metabolite
5	35	36	O	SQ
6	38	43	O	31,906
7	46	48	O	has
8	50	50	O	1
9	51	51	O	/
10	52	53	O	10
11	55	56	O	to
12	58	58	O	1
13	59	59	O	/
14	60	61	O	40
15	63	65	O	the
16	67	69	B-D	HMG
17	71	73	B-E	CoA
18	75	83	I-E	reductase
19	85	94	I-E	inhibitory
20	96	103	I-E	activity
21	105	106	O	of
22	108	110	O	the
23	112	117	O	parent
24	119	126	O	compound
D/16:17:1

Pravastatin Sodium	2005	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	12
Pravastatin undergoes extensive first-pass extraction in the liver (extraction ratio 0.66).
1	0	10	O	XXXXXXXX
2	12	20	O	undergoes
3	22	30	O	extensive
4	32	36	O	first
5	38	41	O	pass
6	43	52	O	extraction
7	54	55	O	in
8	57	59	O	the
9	61	65	O	liver
10	68	77	O	extraction
11	79	83	O	ratio
12	85	88	O	0.66
NULL

Pravastatin Sodium	2006	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	16
Excretion:Approximately 20% of a radiolabeled oral dose is excreted in urine and 70% in the feces.
1	0	22	O	Excretion:Approximately
2	24	26	O	20%
3	28	29	O	of
4	31	31	O	a
5	33	44	O	radiolabeled
6	46	49	O	oral
7	51	54	O	dose
8	56	57	O	is
9	59	66	O	excreted
10	68	69	O	in
11	71	75	O	urine
12	77	79	O	and
13	81	83	O	70%
14	85	86	O	in
15	88	90	O	the
16	92	96	O	feces
NULL

Pravastatin Sodium	2007	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	32
After intravenous administration of radiolabeled pravastatin to normal volunteers, approximately 47% of total body clearance was via renal excretion and 53% by non-renal routes (i.e., biliary excretion and biotransformation).
1	0	4	O	After
2	6	16	O	intravenous
3	18	31	O	administration
4	33	34	O	of
5	36	47	O	radiolabeled
6	49	59	O	XXXXXXXX
7	61	62	O	to
8	64	69	O	normal
9	71	80	O	volunteers
10	81	81	O	,
11	83	95	O	approximately
12	97	99	O	47%
13	101	102	O	of
14	104	108	O	total
15	110	113	B-T	body
16	115	123	I-T	clearance
17	125	127	O	was
18	129	131	O	via
19	133	137	O	renal
20	139	147	O	excretion
21	149	151	O	and
22	153	155	O	53%
23	157	158	O	by
24	160	162	O	non
25	164	168	O	renal
26	170	175	O	routes
27	178	180	O	i.e
28	182	182	O	,
29	184	190	O	biliary
30	192	200	O	excretion
31	202	204	O	and
32	206	222	O	biotransformation
NULL

Pravastatin Sodium	2008	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	18
Following single dose oral administration of14C-pravastatin, the radioactive elimination t%for pravastatin is 1.8 hours in humans.
1	0	8	O	Following
2	10	15	O	single
3	17	20	O	dose
4	22	25	O	oral
5	27	40	O	administration
6	42	46	O	of14C
7	48	58	O	XXXXXXXX
8	59	59	O	,
9	61	63	O	the
10	65	75	O	radioactive
11	77	87	O	elimination
12	89	93	O	t%for
13	95	105	O	XXXXXXXX
14	107	108	O	is
15	110	112	O	1.8
16	114	118	O	hours
17	120	121	O	in
18	123	128	O	humans
NULL

Pravastatin Sodium	2009	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	27
Specific Populations Renal Impairment:A single 20 mg oral dose of pravastatin was administered to 24 patients with varying degrees of renal impairment (as determined by creatinine clearance).
1	0	7	O	Specific
2	9	19	O	Populations
3	21	25	O	Renal
4	27	38	O	Impairment:A
5	40	45	O	single
6	47	48	O	20
7	50	51	O	mg
8	53	56	O	oral
9	58	61	O	dose
10	63	64	O	of
11	66	76	O	XXXXXXXX
12	78	80	O	was
13	82	93	O	administered
14	95	96	O	to
15	98	99	O	24
16	101	108	O	patients
17	110	113	O	with
18	115	121	O	varying
19	123	129	O	degrees
20	131	132	O	of
21	134	138	O	renal
22	140	149	O	impairment
23	152	153	O	as
24	155	164	O	determined
25	166	167	O	by
26	169	178	O	creatinine
27	180	188	O	clearance
NULL

Pravastatin Sodium	2010	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	17
No effect was observed on the pharmacokinetics of pravastatin or its 3A-hydroxy isomeric metabolite (SQ 31,906).
1	0	1	O	No
2	3	8	O	effect
3	10	12	O	was
4	14	21	O	observed
5	23	24	O	on
6	26	28	O	the
7	30	45	O	pharmacokinetics
8	47	48	O	of
9	50	60	O	XXXXXXXX
10	62	63	O	or
11	65	67	O	its
12	69	70	O	3A
13	72	78	O	hydroxy
14	80	87	O	isomeric
15	89	98	O	metabolite
16	101	102	O	SQ
17	104	109	O	31,906
NULL

Pravastatin Sodium	2011	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	40
Compared to healthy subjects with normal renal function, patients with severe renal impairment had 69% and 37% higher mean AUC and Cmaxvalues, respectively, and a 0.61 hour shorter t%for the inactive enzymatic ring hydroxylation metabolite (SQ 31,945).
1	0	7	O	Compared
2	9	10	O	to
3	12	18	O	healthy
4	20	27	O	subjects
5	29	32	O	with
6	34	39	O	normal
7	41	45	O	renal
8	47	54	O	function
9	55	55	O	,
10	57	64	O	patients
11	66	69	O	with
12	71	76	O	severe
13	78	82	O	renal
14	84	93	O	impairment
15	95	97	O	had
16	99	101	O	69%
17	103	105	O	and
18	107	109	O	37%
19	111	116	O	higher
20	118	121	O	mean
21	123	125	B-T	AUC
22	127	129	O	and
23	131	140	O	Cmaxvalues
24	141	141	O	,
25	143	154	O	respectively
26	155	155	O	,
27	157	159	O	and
28	161	161	O	a
29	163	166	O	0.61
30	168	171	O	hour
31	173	179	O	shorter
32	181	185	O	t%for
33	187	189	O	the
34	191	198	O	inactive
35	200	208	O	enzymatic
36	210	213	O	ring
37	215	227	O	hydroxylation
38	229	238	B-K	metabolite
39	241	242	I-K	SQ
40	244	249	I-K	31,945
K/38:C54357

Pravastatin Sodium	2012	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	40
Hepatic Impairment:In a study comparing the kinetics of pravastatin in patients with biopsy confirmed cirrhosis (N = 7) and normal subjects (N = 7), the mean AUC varied 18 fold in cirrhotic patients and 5 fold in healthy subjects.
1	0	6	O	Hepatic
2	8	20	O	Impairment:In
3	22	22	O	a
4	24	28	O	study
5	30	38	O	comparing
6	40	42	O	the
7	44	51	O	kinetics
8	53	54	O	of
9	56	66	O	XXXXXXXX
10	68	69	O	in
11	71	78	O	patients
12	80	83	O	with
13	85	90	O	biopsy
14	92	100	O	confirmed
15	102	110	O	cirrhosis
16	113	113	O	N
17	115	115	O	=
18	117	117	O	7
19	120	122	O	and
20	124	129	O	normal
21	131	138	O	subjects
22	141	141	O	N
23	143	143	O	=
24	145	145	O	7
25	147	147	O	,
26	149	151	O	the
27	153	156	O	mean
28	158	160	O	AUC
29	162	167	O	varied
30	169	170	O	18
31	172	175	O	fold
32	177	178	O	in
33	180	188	O	cirrhotic
34	190	197	O	patients
35	199	201	O	and
36	203	203	O	5
37	205	208	O	fold
38	210	211	O	in
39	213	219	O	healthy
40	221	228	O	subjects
NULL

Pravastatin Sodium	2013	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	19
Similarly, the peak pravastatin values varied 47 fold for cirrhotic patients compared to 6 fold for healthy subjects.
1	0	8	O	Similarly
2	9	9	O	,
3	11	13	O	the
4	15	18	O	peak
5	20	30	O	XXXXXXXX
6	32	37	O	values
7	39	44	O	varied
8	46	47	O	47
9	49	52	O	fold
10	54	56	O	for
11	58	66	O	cirrhotic
12	68	75	O	patients
13	77	84	O	compared
14	86	87	O	to
15	89	89	O	6
16	91	94	O	fold
17	96	98	O	for
18	100	106	O	healthy
19	108	115	O	subjects
NULL

Pravastatin Sodium	2014	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	47
Geriatric:In a single oral dose study using pravastatin 20 mg, the mean AUC for pravastatin was approximately 27% greater and the mean cumulative urinary excretion (CUE) approximately 19% lower in elderly men (65 to 75 years old) compared with younger men (19 to 31 years old).
1	0	11	O	Geriatric:In
2	13	13	O	a
3	15	20	O	single
4	22	25	O	oral
5	27	30	O	dose
6	32	36	O	study
7	38	42	O	using
8	44	54	O	XXXXXXXX
9	56	57	O	20
10	59	60	O	mg
11	61	61	O	,
12	63	65	O	the
13	67	70	O	mean
14	72	74	O	AUC
15	76	78	O	for
16	80	90	O	XXXXXXXX
17	92	94	O	was
18	96	108	O	approximately
19	110	112	O	27%
20	114	120	O	greater
21	122	124	O	and
22	126	128	O	the
23	130	133	O	mean
24	135	144	O	cumulative
25	146	152	O	urinary
26	154	162	O	excretion
27	165	167	O	CUE
28	170	182	O	approximately
29	184	186	O	19%
30	188	192	O	lower
31	194	195	O	in
32	197	203	O	elderly
33	205	207	O	men
34	210	211	O	65
35	213	214	O	to
36	216	217	O	75
37	219	223	O	years
38	225	227	O	old
39	230	237	O	compared
40	239	242	O	with
41	244	250	O	younger
42	252	254	O	men
43	257	258	O	19
44	260	261	O	to
45	263	264	O	31
46	266	270	O	years
47	272	274	O	old
NULL

Pravastatin Sodium	2015	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	41
In a similar study conducted in women, the mean AUC for pravastatin was approximately 46% higher and the mean CUE approximately 18% lower in elderly women (65 to 78 years old) compared with younger women (18 to 38 years old).
1	0	1	O	In
2	3	3	O	a
3	5	11	O	similar
4	13	17	O	study
5	19	27	O	conducted
6	29	30	O	in
7	32	36	O	women
8	37	37	O	,
9	39	41	O	the
10	43	46	O	mean
11	48	50	O	AUC
12	52	54	O	for
13	56	66	O	XXXXXXXX
14	68	70	O	was
15	72	84	O	approximately
16	86	88	O	46%
17	90	95	O	higher
18	97	99	O	and
19	101	103	O	the
20	105	108	O	mean
21	110	112	O	CUE
22	114	126	O	approximately
23	128	130	O	18%
24	132	136	O	lower
25	138	139	O	in
26	141	147	O	elderly
27	149	153	O	women
28	156	157	O	65
29	159	160	O	to
30	162	163	O	78
31	165	169	O	years
32	171	173	O	old
33	176	183	O	compared
34	185	188	O	with
35	190	196	O	younger
36	198	202	O	women
37	205	206	O	18
38	208	209	O	to
39	211	212	O	38
40	214	218	O	years
41	220	222	O	old
NULL

Pravastatin Sodium	2016	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	17
In both studies, Cmax, Tmax, and t%values were similar in older and younger subjects.
1	0	1	O	In
2	3	6	O	both
3	8	14	O	studies
4	15	15	O	,
5	17	20	O	Cmax
6	21	21	O	,
7	23	26	O	Tmax
8	27	27	O	,
9	29	31	O	and
10	33	40	O	t%values
11	42	45	O	were
12	47	53	O	similar
13	55	56	O	in
14	58	62	O	older
15	64	66	O	and
16	68	74	O	younger
17	76	83	O	subjects
NULL

Pravastatin Sodium	2017	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	50
Pediatric:After 2 weeks of once-daily 20 mg oral pravastatin administration, the geometric means of AUC were 80.7 (CV 44%) and 44.8 (CV 89%) ng*hr/mL for children (8 to 11 years, N = 14) and adolescents (12 to 16 years, N = 10), respectively.
1	0	14	O	Pediatric:After
2	16	16	O	2
3	18	22	O	weeks
4	24	25	O	of
5	27	30	O	once
6	32	36	O	daily
7	38	39	O	20
8	41	42	O	mg
9	44	47	O	oral
10	49	59	O	XXXXXXXX
11	61	74	O	administration
12	75	75	O	,
13	77	79	O	the
14	81	89	O	geometric
15	91	95	O	means
16	97	98	O	of
17	100	102	O	AUC
18	104	107	O	were
19	109	112	O	80.7
20	115	116	O	CV
21	118	120	O	44%
22	123	125	O	and
23	127	130	O	44.8
24	133	134	O	CV
25	136	138	O	89%
26	141	145	O	ng*hr
27	146	146	O	/
28	147	148	O	mL
29	150	152	O	for
30	154	161	O	children
31	164	164	O	8
32	166	167	O	to
33	169	170	O	11
34	172	176	O	years
35	177	177	O	,
36	179	179	O	N
37	181	181	O	=
38	183	184	O	14
39	187	189	O	and
40	191	201	O	adolescents
41	204	205	O	12
42	207	208	O	to
43	210	211	O	16
44	213	217	O	years
45	218	218	O	,
46	220	220	O	N
47	222	222	O	=
48	224	225	O	10
49	227	227	O	,
50	229	240	O	respectively
NULL

Pravastatin Sodium	2018	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	21
The corresponding values for Cmaxwere 42.4 (CV 54%) and 18.6 ng/mL (CV 100%) for children and adolescents, respectively.
1	0	2	O	The
2	4	16	O	corresponding
3	18	23	O	values
4	25	27	O	for
5	29	36	O	Cmaxwere
6	38	41	O	42.4
7	44	45	O	CV
8	47	49	O	54%
9	52	54	O	and
10	56	59	O	18.6
11	61	62	O	ng
12	63	63	O	/
13	64	65	O	mL
14	68	69	O	CV
15	71	74	O	100%
16	77	79	O	for
17	81	88	O	children
18	90	92	O	and
19	94	104	O	adolescents
20	105	105	O	,
21	107	118	O	respectively
NULL

Pravastatin Sodium	2019	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	19
No conclusion can be made based on these findings due to the small number of samples and large variability.
1	0	1	O	No
2	3	12	O	conclusion
3	14	16	O	can
4	18	19	O	be
5	21	24	O	made
6	26	30	O	based
7	32	33	O	on
8	35	39	O	these
9	41	48	O	findings
10	50	52	O	due
11	54	55	O	to
12	57	59	O	the
13	61	65	O	small
14	67	72	O	number
15	74	75	O	of
16	77	83	O	samples
17	85	87	O	and
18	89	93	O	large
19	95	105	O	variability
NULL

Pravastatin Sodium	2020	34090-1	ad386ed4-a284-4da5-b79a-3c0f4165057a	101
Drug-Drug Interactions Table 3: Effect of Coadministered Drugs on the Pharmacokinetics of Pravastatin Coadministered Drug and Dosing Regimen Pravastatin Dose (mg) Change in AUC Change in Cmax Cyclosporine 5 mg/kg single dose 40 mg single dose *282% *327% Clarithromycin 500 mg BID for 9 days 40 mg OD for 8 days *110% *128% Boceprevir 800 mg TID for 6 days 40 mg single dose *63% *49% Darunavir 600 mg BID/Ritonavir 100 mg BID for 7 days 40 mg single dose *81% *63% Colestipol 10 g single dose 20 mg single dose *47% *53% Cholestyramine 4 g single dose 20 mg single dose Administered simultaneously *40% *39% Administered 1 hour prior tocholestyramine *12% *30% Administered 4 hours aftercholestyramine *12% *6.8% Cholestyramine 24 g OD for 4 weeks 20 mg BID for 8 weeks *51% *4.9% 5 mg BID for 8 weeks *38% *23% 10 mg BID for 8 weeks *18% *33% Fluconazole 200 mg IV for 6 days 20 mg PO + 10 mg IV *34% *33% 200 mg PO for 6 days 20 mg PO + 10 mg IV *16% *16% Kaletra 400 mg/100 mg BID for 14 days 20 mg OD for 4 days *33% *26% Verapamil IR 120 mg for 1 day and Verapamil ER 480 mg for 3 days 40 mg single dose *31% *42% Cimetidine 300 mg QID for 3 days 20 mg single dose *30% *9.8% Antacids 15 mL QID for 3 days 20 mg single dose *28% *24% Digoxin 0.2 mg OD for 9 days 20 mg OD for 9 days *23% *26% Probucol 500 mg single dose 20 mg single dose *14% *24% Warfarin 5 mg OD for 6 days 20 mg BID for 6 days *13% *6.7% Itraconazole 200 mg OD for 30 days 40 mg OD for 30 days *11% (compared to Day 1) *17% (compared to Day 1) Gemfibrozil 600 mg single dose 20 mg single dose *7.0% *20% Aspirin 324 mg single dose 20 mg single dose *4.7% *8.9% Niacin 1 g single dose 20 mg single dose *3.6% *8.2% Diltiazem 20 mg single dose *2.7% *30% Grapefruit juice 40 mg single dose *1.8% *3.7% BID = twice daily; OD = once daily; QID = four times daily Table 4: Effect of Pravastatin on the Pharmacokinetics of Coadministered Drugs Pravastatin Dosing Regimen Name and Dose Change in AUC Change in Cmax 20 mg BID for 6 days Warfarin 5 mg OD for 6 days *17% *15% Change in mean prothrombin time *0.4 sec 20 mg OD for 9 days Digoxin 0.2 mg OD for 9 days *4.6% *5.3% 20 mg BID for 4 weeks Antipyrine 1.2 g single dose *3.0% Not Reported 10 mg BID for 4 weeks *1.6% 5 mg BID for 4 weeks *Less than1% 20 mg OD for 4 days Kaletra 400 mg/100 mg BID for 14 days No change No change BID = twice daily; OD = once daily
1	0	3	O	Drug
2	5	8	O	Drug
3	10	21	O	Interactions
4	23	27	O	Table
5	29	29	O	3
6	30	30	O	:
7	32	37	O	Effect
8	39	40	O	of
9	42	55	O	Coadministered
10	57	61	O	Drugs
11	63	64	O	on
12	66	68	O	the
13	70	85	O	Pharmacokinetics
14	87	88	O	of
15	90	100	O	XXXXXXXX
16	102	115	O	Coadministered
17	117	120	O	Drug
18	122	124	O	and
19	126	131	O	Dosing
20	133	139	O	Regimen
21	141	151	O	XXXXXXXX
22	153	156	O	Dose
23	159	160	O	mg
24	163	168	O	Change
25	170	171	O	in
26	173	175	O	AUC
27	177	182	O	Change
28	184	185	O	in
29	187	190	O	Cmax
30	192	203	O	Cyclosporine
31	205	205	O	5
32	207	208	O	mg
33	209	209	O	/
34	210	211	O	kg
35	213	218	O	single
36	220	223	O	dose
37	225	226	O	40
38	228	229	O	mg
39	231	236	O	single
40	238	241	O	dose
41	243	247	O	*282%
42	249	253	O	*327%
43	255	268	O	Clarithromycin
44	270	272	O	500
45	274	275	O	mg
46	277	279	O	BID
47	281	283	O	for
48	285	285	O	9
49	287	290	O	days
50	292	293	O	40
51	295	296	O	mg
52	298	299	O	OD
53	301	303	O	for
54	305	305	O	8
55	307	310	O	days
56	312	316	O	*110%
57	318	322	O	*128%
58	324	333	B-K	Boceprevir
59	335	337	O	800
60	339	340	O	mg
61	342	344	O	TID
62	346	348	O	for
63	350	350	O	6
64	352	355	O	days
65	357	358	O	40
66	360	361	O	mg
67	363	368	O	single
68	370	373	O	dose
69	375	378	O	*63%
70	380	383	O	*49%
71	385	393	B-K	Darunavir
72	395	397	O	600
73	399	400	O	mg
74	402	404	O	BID
75	405	405	O	/
76	406	414	O	Ritonavir
77	416	418	O	100
78	420	421	O	mg
79	423	425	O	BID
80	427	429	O	for
81	431	431	O	7
82	433	436	O	days
83	438	439	O	40
84	441	442	O	mg
85	444	449	O	single
86	451	454	O	dose
87	456	459	O	*81%
88	461	464	O	*63%
89	466	475	O	Colestipol
90	477	478	O	10
91	480	480	O	g
92	482	487	O	single
93	489	492	O	dose
94	494	495	O	20
95	497	498	O	mg
96	500	505	O	single
97	507	510	O	dose
98	512	515	O	*47%
99	517	520	O	*53%
100	522	535	O	Cholestyramine
101	537	537	O	4
K/58:C54355 K/71:C54355

