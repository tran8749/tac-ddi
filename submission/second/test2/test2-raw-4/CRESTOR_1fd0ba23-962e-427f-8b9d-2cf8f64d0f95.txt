CRESTOR	5995	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	7
* Cyclosporine: Combination increases rosuvastatin exposure.
1	0	0	O	*
2	2	13	B-K	Cyclosporine
3	14	14	O	:
4	16	26	O	Combination
5	28	36	B-T	increases
6	38	49	I-T	XXXXXXXX
7	51	58	I-T	exposure
K/2:C54355

CRESTOR	5996	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	7
* Gemfibrozil: Combination should be avoided.
1	0	0	O	*
2	2	12	B-U	Gemfibrozil
3	13	13	O	:
4	15	25	O	Combination
5	27	32	O	should
6	34	35	O	be
7	37	43	B-T	avoided
NULL

CRESTOR	5997	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	11
If used together, limit CRESTOR dose to 10mg once daily.
1	0	1	O	If
2	3	6	O	used
3	8	15	O	together
4	16	16	O	,
5	18	22	B-T	limit
6	24	30	O	XXXXXXXX
7	32	35	B-T	dose
8	37	38	O	to
9	40	43	B-U	10mg
10	45	48	O	once
11	50	54	O	daily
NULL

CRESTOR	5998	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	13
* Lopinavir/Ritonavir or atazanavir/ritonavir: Combination increases rosuvastatin exposure.
1	0	0	O	*
2	2	10	B-K	Lopinavir
3	11	11	I-K	/
4	12	20	I-K	Ritonavir
5	22	23	O	or
6	25	34	B-K	atazanavir
7	35	35	I-K	/
8	36	44	I-K	ritonavir
9	45	45	O	:
10	47	57	O	Combination
11	59	67	B-T	increases
12	69	80	I-T	XXXXXXXX
13	82	89	I-T	exposure
K/2:C54355 K/6:C54355

CRESTOR	5999	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	7
* Coumarin anticoagulants: Combination prolongs INR.
1	0	0	O	*
2	2	9	B-D	Coumarin
3	11	24	I-D	anticoagulants
4	25	25	O	:
5	27	37	O	Combination
6	39	46	B-E	prolongs
7	48	50	I-E	INR
D/2:6:1

CRESTOR	6000	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	7
Achieve stable INR prior to starting CRESTOR.
1	0	6	O	Achieve
2	8	13	O	stable
3	15	17	O	INR
4	19	23	O	prior
5	25	26	O	to
6	28	35	O	starting
7	37	43	O	XXXXXXXX
NULL

CRESTOR	6001	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	12
Monitor INR frequently until stable upon initiation or alteration of CRESTOR therapy.
1	0	6	O	Monitor
2	8	10	O	INR
3	12	21	O	frequently
4	23	27	O	until
5	29	34	O	stable
6	36	39	O	upon
7	41	50	O	initiation
8	52	53	O	or
9	55	64	O	alteration
10	66	67	O	of
11	69	75	O	XXXXXXXX
12	77	83	O	therapy
NULL

CRESTOR	6002	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	28
* Concomitant lipid-lowering therapies: use with fibrates or lipid-modifying doses (> 1 g/day) of niacin increases the risk of adverse skeletal muscle effects.
1	0	0	O	*
2	2	12	O	Concomitant
3	14	18	O	lipid
4	20	27	O	lowering
5	29	37	O	therapies
6	38	38	O	:
7	40	42	O	use
8	44	47	O	with
9	49	56	B-D	fibrates
10	58	59	O	or
11	61	65	O	lipid
12	67	75	O	modifying
13	77	81	O	doses
14	84	84	O	>
15	86	86	O	1
16	88	88	O	g
17	89	89	O	/
18	90	92	O	day
19	95	96	O	of
20	98	103	B-D	niacin
21	105	113	O	increases
22	115	117	O	the
23	119	122	O	risk
24	124	125	O	of
25	127	133	O	adverse
26	135	142	B-E	skeletal
27	144	149	I-E	muscle
28	151	157	I-E	effects
D/9:26:1 D/20:26:1

CRESTOR	6003	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	8
Caution should be used when prescribing with CRESTOR.
1	0	6	B-T	Caution
2	8	13	O	should
3	15	16	O	be
4	18	21	O	used
5	23	26	O	when
6	28	38	O	prescribing
7	40	43	O	with
8	45	51	O	XXXXXXXX
NULL

CRESTOR	6004	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	7
Cyclosporine increased rosuvastatin exposure (AUC) 7-fold.
1	0	11	B-K	Cyclosporine
2	13	21	O	increased
3	23	34	O	XXXXXXXX
4	36	43	B-T	exposure
5	46	48	I-T	AUC
6	51	51	O	7
7	53	56	O	fold
K/1:C54355

CRESTOR	6005	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	17
Therefore, in patients taking cyclosporine, the dose of CRESTOR should not exceed 5mg once daily.
1	0	8	O	Therefore
2	9	9	O	,
3	11	12	O	in
4	14	21	O	patients
5	23	28	O	taking
6	30	41	B-U	cyclosporine
7	42	42	O	,
8	44	46	O	the
9	48	51	O	dose
10	53	54	O	of
11	56	62	O	XXXXXXXX
12	64	69	O	should
13	71	73	B-T	not
14	75	80	I-T	exceed
15	82	84	B-U	5mg
16	86	89	O	once
17	91	95	O	daily
NULL

CRESTOR	6006	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	5
Gemfibrozil significantly increased rosuvastatin exposure.
1	0	10	B-K	Gemfibrozil
2	12	24	O	significantly
3	26	34	O	increased
4	36	47	O	XXXXXXXX
5	49	56	B-T	exposure
K/1:C54355

CRESTOR	6007	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	20
Due to an observed increased risk of myopathy/rhabdomyolysis, combination therapy with CRESTOR and gemfibrozil should be avoided.
1	0	2	O	Due
2	4	5	O	to
3	7	8	O	an
4	10	17	O	observed
5	19	27	O	increased
6	29	32	O	risk
7	34	35	O	of
8	37	44	B-E	myopathy
9	45	45	O	/
10	46	59	B-E	rhabdomyolysis
11	60	60	O	,
12	62	72	O	combination
13	74	80	O	therapy
14	82	85	O	with
15	87	93	O	XXXXXXXX
16	95	97	O	and
17	99	109	B-D	gemfibrozil
18	111	116	O	should
19	118	119	O	be
20	121	127	B-T	avoided
D/17:8:1 D/17:10:1

CRESTOR	6008	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	14
If used together, the dose of CRESTOR should not exceed 10mg once daily.
1	0	1	O	If
2	3	6	O	used
3	8	15	O	together
4	16	16	O	,
5	18	20	O	the
6	22	25	O	dose
7	27	28	O	of
8	30	36	O	XXXXXXXX
9	38	43	O	should
10	45	47	B-T	not
11	49	54	I-T	exceed
12	56	59	B-U	10mg
13	61	64	O	once
14	66	70	O	daily
NULL

CRESTOR	6009	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	18
Coadministration of rosuvastatin with certain protease inhibitors given in combination with ritonavir has differing effects on rosuvastatin exposure.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	31	O	XXXXXXXX
4	33	36	O	with
5	38	44	O	certain
6	46	53	B-D	protease
7	55	64	I-D	inhibitors
8	66	70	O	given
9	72	73	O	in
10	75	85	O	combination
11	87	90	O	with
12	92	100	B-D	ritonavir
13	102	104	O	has
14	106	114	O	differing
15	116	122	B-E	effects
16	124	125	I-E	on
17	127	138	I-E	XXXXXXXX
18	140	147	B-T	exposure
D/6:15:1 D/12:15:1

CRESTOR	6010	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	18
The protease inhibitor combinations lopinavir/ritonavir and atazanavir/ritonavir increase rosuvastatin exposure (AUC) up to threefold.
1	0	2	O	The
2	4	11	B-K	protease
3	13	21	I-K	inhibitor
4	23	34	I-K	combinations
5	36	44	B-K	lopinavir
6	45	45	I-K	/
7	46	54	I-K	ritonavir
8	56	58	O	and
9	60	69	B-K	atazanavir
10	70	70	I-K	/
11	71	79	I-K	ritonavir
12	81	88	O	increase
13	90	101	O	XXXXXXXX
14	103	110	B-T	exposure
15	113	115	I-T	AUC
16	118	119	O	up
17	121	122	O	to
18	124	132	O	threefold
K/2:C54355 K/5:C54355 K/9:C54355

CRESTOR	6011	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	13
For these combinations the dose of CRESTOR should not exceed 10mg once daily.
1	0	2	O	For
2	4	8	O	these
3	10	21	O	combinations
4	23	25	O	the
5	27	30	O	dose
6	32	33	O	of
7	35	41	O	XXXXXXXX
8	43	48	O	should
9	50	52	O	not
10	54	59	O	exceed
11	61	64	O	10mg
12	66	69	O	once
13	71	75	O	daily
NULL

CRESTOR	6012	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	18
The combinations of tipranavir/ritonavir or fosamprenavir/ritonavir produce little or no change in rosuvastatin exposure.
1	0	2	O	The
2	4	15	O	combinations
3	17	18	O	of
4	20	29	O	tipranavir
5	30	30	O	/
6	31	39	O	ritonavir
7	41	42	O	or
8	44	56	O	fosamprenavir
9	57	57	O	/
10	58	66	O	ritonavir
11	68	74	O	produce
12	76	81	O	little
13	83	84	O	or
14	86	87	O	no
15	89	94	O	change
16	96	97	O	in
17	99	110	O	XXXXXXXX
18	112	119	O	exposure
NULL

CRESTOR	6013	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	16
Caution should be exercised when rosuvastatin is coadministered with protease inhibitors given in combination with ritonavir.
1	0	6	B-T	Caution
2	8	13	O	should
3	15	16	O	be
4	18	26	O	exercised
5	28	31	O	when
6	33	44	O	XXXXXXXX
7	46	47	O	is
8	49	62	O	coadministered
9	64	67	O	with
10	69	76	B-U	protease
11	78	87	I-U	inhibitors
12	89	93	I-U	given
13	95	96	O	in
14	98	108	O	combination
15	110	113	O	with
16	115	123	B-U	ritonavir
NULL

CRESTOR	6014	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	9
CRESTOR significantly increased INR in patients receiving coumarin anticoagulants.
1	0	6	O	XXXXXXXX
2	8	20	O	significantly
3	22	30	B-T	increased
4	32	34	B-E	INR
5	36	37	O	in
6	39	46	O	patients
7	48	56	O	receiving
8	58	65	B-D	coumarin
9	67	80	I-D	anticoagulants
D/8:4:1

CRESTOR	6015	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	15
Therefore, caution should be exercised when coumarin anticoagulants are given in conjunction with CRESTOR.
1	0	8	O	Therefore
2	9	9	O	,
3	11	17	B-T	caution
4	19	24	O	should
5	26	27	O	be
6	29	37	O	exercised
7	39	42	O	when
8	44	51	B-U	coumarin
9	53	66	I-U	anticoagulants
10	68	70	O	are
11	72	76	O	given
12	78	79	O	in
13	81	91	O	conjunction
14	93	96	O	with
15	98	104	O	XXXXXXXX
NULL

CRESTOR	6016	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	31
In patients taking coumarin anticoagulants and CRESTOR concomitantly, INR should be determined before starting CRESTOR and frequently enough during early therapy to ensure that no significant alteration of INR occurs.
1	0	1	O	In
2	3	10	O	patients
3	12	17	O	taking
4	19	26	O	coumarin
5	28	41	O	anticoagulants
6	43	45	O	and
7	47	53	O	XXXXXXXX
8	55	67	O	concomitantly
9	68	68	O	,
10	70	72	O	INR
11	74	79	O	should
12	81	82	O	be
13	84	93	O	determined
14	95	100	O	before
15	102	109	O	starting
16	111	117	O	XXXXXXXX
17	119	121	O	and
18	123	132	O	frequently
19	134	139	O	enough
20	141	146	O	during
21	148	152	O	early
22	154	160	O	therapy
23	162	163	O	to
24	165	170	O	ensure
25	172	175	O	that
26	177	178	O	no
27	180	190	O	significant
28	192	201	O	alteration
29	203	204	O	of
30	206	208	O	INR
31	210	215	O	occurs
NULL

CRESTOR	6017	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	32
The risk of skeletal muscle effects may be enhanced when CRESTOR is used in combination with lipid-modifying doses (>1g/day) of niacin; caution should be used when prescribing with CRESTOR.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	19	O	skeletal
5	21	26	O	muscle
6	28	34	B-E	effects
7	36	38	O	may
8	40	41	O	be
9	43	50	B-E	enhanced
10	52	55	O	when
11	57	63	O	XXXXXXXX
12	65	66	O	is
13	68	71	O	used
14	73	74	O	in
15	76	86	O	combination
16	88	91	O	with
17	93	97	O	lipid
18	99	107	O	modifying
19	109	113	O	doses
20	116	118	O	>1g
21	119	119	O	/
22	120	122	O	day
23	125	126	O	of
24	128	133	B-U	niacin
25	136	142	O	caution
26	144	149	O	should
27	151	152	O	be
28	154	157	O	used
29	159	162	O	when
30	164	174	O	prescribing
31	176	179	O	with
32	181	187	O	XXXXXXXX
NULL

CRESTOR	6018	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	20
When CRESTOR was coadministered with fenofibrate, no clinically significant increase in the AUC of rosuvastatin or fenofibrate was observed.
1	0	3	O	When
2	5	11	O	XXXXXXXX
3	13	15	O	was
4	17	30	O	coadministered
5	32	35	O	with
6	37	47	B-K	fenofibrate
7	48	48	O	,
8	50	51	O	no
9	53	62	O	clinically
10	64	74	O	significant
11	76	83	B-T	increase
12	85	86	O	in
13	88	90	O	the
14	92	94	O	AUC
15	96	97	O	of
16	99	110	O	XXXXXXXX
17	112	113	O	or
18	115	125	B-K	fenofibrate
19	127	129	O	was
20	131	138	O	observed
K/6:C54355 K/18:C54355

CRESTOR	6019	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	33
Because it is known that the risk of myopathy during treatment with HMG-CoA reductase inhibitors is increased with concomitant use of fenofibrates, caution should be used when prescribing fenofibrates with CRESTOR.
1	0	6	O	Because
2	8	9	O	it
3	11	12	O	is
4	14	18	O	known
5	20	23	O	that
6	25	27	O	the
7	29	32	O	risk
8	34	35	O	of
9	37	44	O	myopathy
10	46	51	O	during
11	53	61	O	treatment
12	63	66	O	with
13	68	70	B-D	HMG
14	72	74	I-D	CoA
15	76	84	I-D	reductase
16	86	95	B-U	inhibitors
17	97	98	O	is
18	100	108	O	increased
19	110	113	O	with
20	115	125	O	concomitant
21	127	129	O	use
22	131	132	O	of
23	134	145	B-U	fenofibrates
24	146	146	O	,
25	148	154	B-T	caution
26	156	161	O	should
27	163	164	O	be
28	166	169	O	used
29	171	174	O	when
30	176	186	O	prescribing
31	188	199	O	fenofibrates
32	201	204	O	with
33	206	212	O	XXXXXXXX
NULL

CRESTOR	6020	34073-7	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	33
Cases of myopathy, including rhabdomyolysis, have been reported with HMG-CoA reductase inhibitors, including rosuvastatin, coadministered with colchicine, and caution should be exercised when prescribing CRESTOR with colchicine.
1	0	4	B-E	Cases
2	6	7	O	of
3	9	16	B-E	myopathy
4	17	17	O	,
5	19	27	O	including
6	29	42	B-E	rhabdomyolysis
7	43	43	O	,
8	45	48	O	have
9	50	53	O	been
10	55	62	O	reported
11	64	67	O	with
12	69	71	B-D	HMG
13	73	75	I-D	CoA
14	77	85	I-D	reductase
15	87	96	I-D	inhibitors
16	97	97	O	,
17	99	107	O	including
18	109	120	O	XXXXXXXX
19	121	121	O	,
20	123	136	O	coadministered
21	138	141	O	with
22	143	152	B-D	colchicine
23	153	153	O	,
24	155	157	O	and
25	159	165	B-T	caution
26	167	172	O	should
27	174	175	O	be
28	177	185	O	exercised
29	187	190	O	when
30	192	202	O	prescribing
31	204	210	O	XXXXXXXX
32	212	215	O	with
33	217	226	B-U	colchicine
D/12:1:1 D/12:3:1 D/12:6:1 D/22:1:1 D/22:3:1 D/22:6:1

CRESTOR	6021	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	31
CRESTOR is a selective and competitive inhibitor of HMG-CoA reductase, the rate-limiting enzyme that converts 3-hydroxy-3-methylglutaryl coenzyme A to mevalonate, a precursor of cholesterol.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	11	O	a
4	13	21	O	selective
5	23	25	O	and
6	27	37	O	competitive
7	39	47	O	inhibitor
8	49	50	O	of
9	52	54	O	HMG
10	56	58	O	CoA
11	60	68	O	reductase
12	69	69	O	,
13	71	73	O	the
14	75	78	O	rate
15	80	87	O	limiting
16	89	94	O	enzyme
17	96	99	O	that
18	101	108	O	converts
19	110	110	O	3
20	112	118	O	hydroxy
21	120	120	O	3
22	122	135	O	methylglutaryl
23	137	144	O	coenzyme
24	146	146	O	A
25	148	149	O	to
26	151	160	O	mevalonate
27	161	161	O	,
28	163	163	O	a
29	165	173	O	precursor
30	175	176	O	of
31	178	188	O	cholesterol
NULL

CRESTOR	6022	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	41
In vivo studies in animals, and in vitro studies in cultured animal and human cells have shown rosuvastatin to have a high uptake into, and selectivity for, action in the liver, the target organ for cholesterol lowering.
1	0	1	O	In
2	3	6	O	vivo
3	8	14	O	studies
4	16	17	O	in
5	19	25	O	animals
6	26	26	O	,
7	28	30	O	and
8	32	33	O	in
9	35	39	O	vitro
10	41	47	O	studies
11	49	50	O	in
12	52	59	O	cultured
13	61	66	O	animal
14	68	70	O	and
15	72	76	O	human
16	78	82	O	cells
17	84	87	O	have
18	89	93	O	shown
19	95	106	O	XXXXXXXX
20	108	109	O	to
21	111	114	O	have
22	116	116	O	a
23	118	121	O	high
24	123	128	O	uptake
25	130	133	O	into
26	134	134	O	,
27	136	138	O	and
28	140	150	O	selectivity
29	152	154	O	for
30	155	155	O	,
31	157	162	O	action
32	164	165	O	in
33	167	169	O	the
34	171	175	O	liver
35	176	176	O	,
36	178	180	O	the
37	182	187	O	target
38	189	193	O	organ
39	195	197	O	for
40	199	209	O	cholesterol
41	211	218	O	lowering
NULL

CRESTOR	6023	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	17
In in vivo and in vitro studies, rosuvastatin produces its lipid-modifying effects in two ways.
1	0	1	O	In
2	3	4	O	in
3	6	9	O	vivo
4	11	13	O	and
5	15	16	O	in
6	18	22	O	vitro
7	24	30	O	studies
8	31	31	O	,
9	33	44	O	XXXXXXXX
10	46	53	O	produces
11	55	57	O	its
12	59	63	O	lipid
13	65	73	O	modifying
14	75	81	O	effects
15	83	84	O	in
16	86	88	O	two
17	90	93	O	ways
NULL

CRESTOR	6024	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	21
First, it increases the number of hepatic LDL receptors on the cell-surface to enhance uptake and catabolism of LDL.
1	0	4	O	First
2	5	5	O	,
3	7	8	O	it
4	10	18	O	increases
5	20	22	O	the
6	24	29	O	number
7	31	32	O	of
8	34	40	O	hepatic
9	42	44	O	LDL
10	46	54	O	receptors
11	56	57	O	on
12	59	61	O	the
13	63	66	O	cell
14	68	74	O	surface
15	76	77	O	to
16	79	85	B-T	enhance
17	87	92	I-T	uptake
18	94	96	O	and
19	98	107	O	catabolism
20	109	110	O	of
21	112	114	O	LDL
NULL

CRESTOR	6025	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	19
Second, rosuvastatin inhibits hepatic synthesis of VLDL, which reduces the total number of VLDL and LDL particles.
1	0	5	O	Second
2	6	6	O	,
3	8	19	O	XXXXXXXX
4	21	28	O	inhibits
5	30	36	O	hepatic
6	38	46	O	synthesis
7	48	49	O	of
8	51	54	O	VLDL
9	55	55	O	,
10	57	61	O	which
11	63	69	B-T	reduces
12	71	73	I-T	the
13	75	79	I-T	total
14	81	86	O	number
15	88	89	O	of
16	91	94	O	VLDL
17	96	98	O	and
18	100	102	B-K	LDL
19	104	112	O	particles
K/18:C54357

CRESTOR	6026	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	23
* Absorption: In clinical pharmacology studies in man, peak plasma concentrations of rosuvastatin were reached 3 to 5hours following oral dosing.
1	0	0	O	*
2	2	11	O	Absorption
3	12	12	O	:
4	14	15	O	In
5	17	24	O	clinical
6	26	37	O	pharmacology
7	39	45	O	studies
8	47	48	O	in
9	50	52	O	man
10	53	53	O	,
11	55	58	O	peak
12	60	65	O	plasma
13	67	80	O	concentrations
14	82	83	O	of
15	85	96	O	XXXXXXXX
16	98	101	O	were
17	103	109	O	reached
18	111	111	O	3
19	113	114	O	to
20	116	121	O	5hours
21	123	131	O	following
22	133	136	O	oral
23	138	143	O	dosing
NULL

CRESTOR	6027	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	11
Both Cmax and AUC increased in approximate proportion to CRESTOR dose.
1	0	3	O	Both
2	5	8	O	Cmax
3	10	12	O	and
4	14	16	O	AUC
5	18	26	O	increased
6	28	29	O	in
7	31	41	O	approximate
8	43	52	O	proportion
9	54	55	O	to
10	57	63	O	XXXXXXXX
11	65	68	O	dose
NULL

CRESTOR	6028	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	8
The absolute bioavailability of rosuvastatin is approximately 20%.
1	0	2	O	The
2	4	11	O	absolute
3	13	27	O	bioavailability
4	29	30	O	of
5	32	43	O	XXXXXXXX
6	45	46	O	is
7	48	60	O	approximately
8	62	64	O	20%
NULL

CRESTOR	6029	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	12
Administration of CRESTOR with food did not affect the AUC of rosuvastatin.
1	0	13	O	Administration
2	15	16	O	of
3	18	24	O	XXXXXXXX
4	26	29	O	with
5	31	34	O	food
6	36	38	O	did
7	40	42	O	not
8	44	49	O	affect
9	51	53	O	the
10	55	57	O	AUC
11	59	60	O	of
12	62	73	O	XXXXXXXX
NULL

CRESTOR	6030	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	13
The AUC of rosuvastatin does not differ following evening or morning drug administration.
1	0	2	O	The
2	4	6	O	AUC
3	8	9	O	of
4	11	22	O	XXXXXXXX
5	24	27	O	does
6	29	31	O	not
7	33	38	O	differ
8	40	48	O	following
9	50	56	O	evening
10	58	59	O	or
11	61	67	O	morning
12	69	72	O	drug
13	74	87	O	administration
NULL

CRESTOR	6031	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	16
* Distribution: Mean volume of distribution at steady-state of rosuvastatin is approximately 134 liters.
1	0	0	O	*
2	2	13	O	Distribution
3	14	14	O	:
4	16	19	O	Mean
5	21	26	O	volume
6	28	29	O	of
7	31	42	O	distribution
8	44	45	O	at
9	47	52	O	steady
10	54	58	O	state
11	60	61	O	of
12	63	74	O	XXXXXXXX
13	76	77	O	is
14	79	91	O	approximately
15	93	95	O	134
16	97	102	O	liters
NULL

CRESTOR	6032	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	10
Rosuvastatin is 88% bound to plasma proteins, mostly albumin.
1	0	11	O	XXXXXXXX
2	13	14	O	is
3	16	18	O	88%
4	20	24	O	bound
5	26	27	O	to
6	29	34	O	plasma
7	36	43	O	proteins
8	44	44	O	,
9	46	51	O	mostly
10	53	59	O	albumin
NULL

CRESTOR	6033	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	9
This binding is reversible and independent of plasma concentrations.
1	0	3	O	This
2	5	11	O	binding
3	13	14	O	is
4	16	25	O	reversible
5	27	29	O	and
6	31	41	O	independent
7	43	44	O	of
8	46	51	O	plasma
9	53	66	O	concentrations
NULL

CRESTOR	6034	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	18
* Metabolism: Rosuvastatin is not extensively metabolized; approximately 10% of a radiolabeled dose is recovered as metabolite.
1	0	0	O	*
2	2	11	O	Metabolism
3	12	12	O	:
4	14	25	O	XXXXXXXX
5	27	28	O	is
6	30	32	O	not
7	34	44	O	extensively
8	46	56	O	metabolized
9	59	71	O	approximately
10	73	75	O	10%
11	77	78	O	of
12	80	80	O	a
13	82	93	O	radiolabeled
14	95	98	O	dose
15	100	101	O	is
16	103	111	O	recovered
17	113	114	O	as
18	116	125	O	metabolite
NULL

CRESTOR	6035	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	44
The major metabolite is N-desmethyl rosuvastatin, which is formed principally by cytochrome P450 \2C9, and in vitro studies have demonstrated that N-desmethyl rosuvastatin has approximately one-sixth to one-half the HMG-CoA reductase inhibitory activity of the parent compound.
1	0	2	O	The
2	4	8	O	major
3	10	19	O	metabolite
4	21	22	O	is
5	24	24	O	N
6	26	34	O	desmethyl
7	36	47	O	XXXXXXXX
8	48	48	O	,
9	50	54	O	which
10	56	57	O	is
11	59	64	O	formed
12	66	76	O	principally
13	78	79	O	by
14	81	90	O	cytochrome
15	92	95	O	P450
16	97	100	O	\2C9
17	101	101	O	,
18	103	105	O	and
19	107	108	O	in
20	110	114	O	vitro
21	116	122	O	studies
22	124	127	O	have
23	129	140	O	demonstrated
24	142	145	O	that
25	147	147	O	N
26	149	157	O	desmethyl
27	159	170	O	XXXXXXXX
28	172	174	O	has
29	176	188	O	approximately
30	190	192	O	one
31	194	198	O	sixth
32	200	201	O	to
33	203	205	O	one
34	207	210	O	half
35	212	214	O	the
36	216	218	B-E	HMG
37	220	222	I-E	CoA
38	224	232	I-E	reductase
39	234	243	I-E	inhibitory
40	245	252	I-E	activity
41	254	255	O	of
42	257	259	O	the
43	261	266	O	parent
44	268	275	O	compound
NULL

CRESTOR	6036	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	20
Overall, greater than 90% of active plasma HMG-CoA reductase inhibitory activity is accounted for by the parent compound.
1	0	6	O	Overall
2	7	7	O	,
3	9	15	O	greater
4	17	20	O	than
5	22	24	O	90%
6	26	27	O	of
7	29	34	O	active
8	36	41	O	plasma
9	43	45	O	HMG
10	47	49	O	CoA
11	51	59	O	reductase
12	61	70	O	inhibitory
13	72	79	O	activity
14	81	82	O	is
15	84	92	O	accounted
16	94	96	O	for
17	98	99	O	by
18	101	103	O	the
19	105	110	O	parent
20	112	119	O	compound
NULL

CRESTOR	6037	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	18
* Excretion: Following oral administration, rosuvastatin and its metabolites are primarily excreted in the feces (90%).
1	0	0	O	*
2	2	10	O	Excretion
3	11	11	O	:
4	13	21	O	Following
5	23	26	O	oral
6	28	41	O	administration
7	42	42	O	,
8	44	55	O	XXXXXXXX
9	57	59	O	and
10	61	63	O	its
11	65	75	O	metabolites
12	77	79	O	are
13	81	89	O	primarily
14	91	98	O	excreted
15	100	101	O	in
16	103	105	O	the
17	107	111	O	feces
18	114	116	O	90%
NULL

CRESTOR	6038	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	13
The elimination half-life (t1/2) of rosuvastatin is approximately 19 hours.
1	0	2	O	The
2	4	14	B-T	elimination
3	16	19	I-T	half
4	21	24	I-T	life
5	27	28	O	t1
6	29	29	O	/
7	30	30	O	2
8	33	34	O	of
9	36	47	O	XXXXXXXX
10	49	50	O	is
11	52	64	O	approximately
12	66	67	O	19
13	69	73	O	hours
NULL

CRESTOR	6039	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	23
After an intravenous dose, approximately 28% of total body clearance was via the renal route, and 72% by the hepatic route.
1	0	4	O	After
2	6	7	O	an
3	9	19	O	intravenous
4	21	24	O	dose
5	25	25	O	,
6	27	39	O	approximately
7	41	43	O	28%
8	45	46	O	of
9	48	52	O	total
10	54	57	B-T	body
11	59	67	I-T	clearance
12	69	71	O	was
13	73	75	O	via
14	77	79	O	the
15	81	85	O	renal
16	87	91	B-T	route
17	92	92	O	,
18	94	96	O	and
19	98	100	O	72%
20	102	103	O	by
21	105	107	O	the
22	109	115	O	hepatic
23	117	121	O	route
NULL

CRESTOR	6040	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	25
* Race: A population pharmacokinetic analysis revealed no clinically relevant differences in pharmacokinetics among Caucasian, Hispanic, and Black or Afro-Caribbean groups.
1	0	0	O	*
2	2	5	O	Race
3	6	6	O	:
4	8	8	O	A
5	10	19	O	population
6	21	35	O	pharmacokinetic
7	37	44	O	analysis
8	46	53	O	revealed
9	55	56	O	no
10	58	67	O	clinically
11	69	76	O	relevant
12	78	88	O	differences
13	90	91	O	in
14	93	108	O	pharmacokinetics
15	110	114	O	among
16	116	124	O	Caucasian
17	125	125	O	,
18	127	134	O	Hispanic
19	135	135	O	,
20	137	139	O	and
21	141	145	O	Black
22	147	148	O	or
23	150	153	O	Afro
24	155	163	O	Caribbean
25	165	170	O	groups
NULL

CRESTOR	6041	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	35
However, pharmacokinetic studies, including one conducted in the US, have demonstrated an approximate 2-fold elevation in median exposure (AUC and Cmax) in Asian subjects when compared with a Caucasian control group.
1	0	6	O	However
2	7	7	O	,
3	9	23	O	pharmacokinetic
4	25	31	O	studies
5	32	32	O	,
6	34	42	O	including
7	44	46	O	one
8	48	56	O	conducted
9	58	59	O	in
10	61	63	O	the
11	65	66	O	US
12	67	67	O	,
13	69	72	O	have
14	74	85	O	demonstrated
15	87	88	O	an
16	90	100	O	approximate
17	102	102	O	2
18	104	107	O	fold
19	109	117	O	elevation
20	119	120	O	in
21	122	127	O	median
22	129	136	O	exposure
23	139	141	O	AUC
24	143	145	O	and
25	147	150	O	Cmax
26	153	154	O	in
27	156	160	O	Asian
28	162	169	O	subjects
29	171	174	O	when
30	176	183	O	compared
31	185	188	O	with
32	190	190	O	a
33	192	200	O	Caucasian
34	202	208	O	control
35	210	214	O	group
NULL

CRESTOR	6042	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	16
* Gender: There were no differences in plasma concentrations of rosuvastatin between men and women.
1	0	0	O	*
2	2	7	O	Gender
3	8	8	O	:
4	10	14	O	There
5	16	19	O	were
6	21	22	O	no
7	24	34	O	differences
8	36	37	O	in
9	39	44	O	plasma
10	46	59	O	concentrations
11	61	62	O	of
12	64	75	O	XXXXXXXX
13	77	83	O	between
14	85	87	O	men
15	89	91	O	and
16	93	97	O	women
NULL

CRESTOR	6043	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	20
* Geriatric: There were no differences in plasma concentrations of rosuvastatin between the nonelderly and elderly populations (age >65years).
1	0	0	O	*
2	2	10	O	Geriatric
3	11	11	O	:
4	13	17	O	There
5	19	22	O	were
6	24	25	O	no
7	27	37	O	differences
8	39	40	O	in
9	42	47	O	plasma
10	49	62	O	concentrations
11	64	65	O	of
12	67	78	O	XXXXXXXX
13	80	86	O	between
14	88	90	O	the
15	92	101	O	nonelderly
16	103	105	O	and
17	107	113	O	elderly
18	115	125	O	populations
19	128	130	O	age
20	132	139	O	>65years
NULL

CRESTOR	6044	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	23
* Renal Impairment: Mild to moderate renal impairment (CLcr>30mL/min/1.73 m2) had no influence on plasma concentrations of rosuvastatin.
1	0	0	O	*
2	2	6	O	Renal
3	8	17	O	Impairment
4	18	18	O	:
5	20	23	O	Mild
6	25	26	O	to
7	28	35	O	moderate
8	37	41	O	renal
9	43	52	O	impairment
10	55	63	O	CLcr>30mL
11	64	64	O	/
12	65	67	O	min
13	68	68	O	/
14	69	72	O	1.73
15	74	75	O	m2
16	78	80	O	had
17	82	83	O	no
18	85	93	O	influence
19	95	96	O	on
20	98	103	O	plasma
21	105	118	O	concentrations
22	120	121	O	of
23	123	134	O	XXXXXXXX
NULL

CRESTOR	6045	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	41
However, plasma concentrations of rosuvastatin increased to a clinically significant extent (about 3-fold) in patients with severe renal impairment (CLcr<30 mL/min/1.73 m2) not receiving hemodialysis compared with healthy subjects (CLcr> 80mL/min/1.73m2).
1	0	6	O	However
2	7	7	O	,
3	9	14	O	plasma
4	16	29	O	concentrations
5	31	32	O	of
6	34	45	O	XXXXXXXX
7	47	55	O	increased
8	57	58	O	to
9	60	60	O	a
10	62	71	O	clinically
11	73	83	O	significant
12	85	90	O	extent
13	93	97	O	about
14	99	99	O	3
15	101	104	O	fold
16	107	108	O	in
17	110	117	O	patients
18	119	122	O	with
19	124	129	O	severe
20	131	135	O	renal
21	137	146	O	impairment
22	149	155	O	CLcr<30
23	157	158	O	mL
24	159	159	O	/
25	160	162	O	min
26	163	163	O	/
27	164	167	O	1.73
28	169	170	O	m2
29	173	175	O	not
30	177	185	O	receiving
31	187	198	O	hemodialysis
32	200	207	O	compared
33	209	212	O	with
34	214	220	O	healthy
35	222	229	O	subjects
36	232	236	O	CLcr>
37	238	241	O	80mL
38	242	242	O	/
39	243	245	O	min
40	246	246	O	/
41	247	252	O	1.73m2
NULL

CRESTOR	6046	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	27
* Hemodialysis: Steady-state plasma concentrations of rosuvastatin in patients on chronic hemodialysis were approximately 50% greater compared with healthy volunteer subjects with normal renal function.
1	0	0	O	*
2	2	13	O	Hemodialysis
3	14	14	O	:
4	16	21	O	Steady
5	23	27	O	state
6	29	34	O	plasma
7	36	49	O	concentrations
8	51	52	O	of
9	54	65	O	XXXXXXXX
10	67	68	O	in
11	70	77	O	patients
12	79	80	O	on
13	82	88	O	chronic
14	90	101	O	hemodialysis
15	103	106	O	were
16	108	120	O	approximately
17	122	124	O	50%
18	126	132	O	greater
19	134	141	O	compared
20	143	146	O	with
21	148	154	O	healthy
22	156	164	O	volunteer
23	166	173	O	subjects
24	175	178	O	with
25	180	185	O	normal
26	187	191	O	renal
27	193	200	O	function
NULL

CRESTOR	6047	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	19
* Hepatic Impairment: In patients with chronic alcohol liver disease, plasma concentrations of rosuvastatin were modestly increased.
1	0	0	O	*
2	2	8	O	Hepatic
3	10	19	O	Impairment
4	20	20	O	:
5	22	23	O	In
6	25	32	O	patients
7	34	37	O	with
8	39	45	O	chronic
9	47	53	B-K	alcohol
10	55	59	O	liver
11	61	67	O	disease
12	68	68	O	,
13	70	75	O	plasma
14	77	90	O	concentrations
15	92	93	O	of
16	95	106	O	XXXXXXXX
17	108	111	O	were
18	113	120	O	modestly
19	122	130	O	increased
K/9:C54355

CRESTOR	6048	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	28
In patients with Child-Pugh A disease, Cmax and AUC were increased by 60% and 5%, respectively, as compared with patients with normal liver function.
1	0	1	O	In
2	3	10	O	patients
3	12	15	O	with
4	17	21	O	Child
5	23	26	O	Pugh
6	28	28	O	A
7	30	36	O	disease
8	37	37	O	,
9	39	42	O	Cmax
10	44	46	O	and
11	48	50	B-T	AUC
12	52	55	I-T	were
13	57	65	I-T	increased
14	67	68	O	by
15	70	72	O	60%
16	74	76	O	and
17	78	79	O	5%
18	80	80	O	,
19	82	93	O	respectively
20	94	94	O	,
21	96	97	O	as
22	99	106	O	compared
23	108	111	O	with
24	113	120	O	patients
25	122	125	O	with
26	127	132	O	normal
27	134	138	O	liver
28	140	147	O	function
NULL

CRESTOR	6049	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	26
In patients with Child-Pugh B disease, Cmax and AUC were increased 100% and 21%, respectively, compared with patients with normal liver function.
1	0	1	O	In
2	3	10	O	patients
3	12	15	O	with
4	17	21	O	Child
5	23	26	O	Pugh
6	28	28	O	B
7	30	36	O	disease
8	37	37	O	,
9	39	42	O	Cmax
10	44	46	O	and
11	48	50	O	AUC
12	52	55	O	were
13	57	65	O	increased
14	67	70	O	100%
15	72	74	O	and
16	76	78	O	21%
17	79	79	O	,
18	81	92	O	respectively
19	93	93	O	,
20	95	102	O	compared
21	104	107	O	with
22	109	116	O	patients
23	118	121	O	with
24	123	128	O	normal
25	130	134	O	liver
26	136	143	O	function
NULL

CRESTOR	6050	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	20
Drug-Drug Interactions: Rosuvastatin clearance is not dependent on metabolism by cytochrome P450 3A4 to a clinically significant extent.
1	0	3	O	Drug
2	5	8	O	Drug
3	10	21	O	Interactions
4	22	22	O	:
5	24	35	O	XXXXXXXX
6	37	45	O	clearance
7	47	48	O	is
8	50	52	O	not
9	54	62	O	dependent
10	64	65	O	on
11	67	76	O	metabolism
12	78	79	O	by
13	81	90	O	cytochrome
14	92	95	O	P450
15	97	99	O	3A4
16	101	102	O	to
17	104	104	O	a
18	106	115	O	clinically
19	117	127	O	significant
20	129	134	O	extent
NULL

CRESTOR	6051	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	27
Rosuvastatin is a substrate for certain transporter proteins including the hepatic uptake transporter organic anion-transporting polyprotein 1B1 (OATP1B1) and efflux transporter breast cancer resistance protein (BCRP).
1	0	11	O	XXXXXXXX
2	13	14	O	is
3	16	16	O	a
4	18	26	O	substrate
5	28	30	O	for
6	32	38	O	certain
7	40	50	O	transporter
8	52	59	O	proteins
9	61	69	O	including
10	71	73	O	the
11	75	81	O	hepatic
12	83	88	O	uptake
13	90	100	O	transporter
14	102	108	O	organic
15	110	114	O	anion
16	116	127	O	transporting
17	129	139	O	polyprotein
18	141	143	O	1B1
19	146	152	O	OATP1B1
20	155	157	O	and
21	159	164	O	efflux
22	166	176	O	transporter
23	178	183	O	breast
24	185	190	O	cancer
25	192	201	O	resistance
26	203	209	O	protein
27	212	215	O	BCRP
NULL

CRESTOR	6052	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	33
Concomitant administration of CRESTOR with medications that are inhibitors of these transporter proteins (e.g. cyclosporine, certain HIV protease inhibitors) may result in increased rosuvastatin plasma concentrations and an increased risk of myopathy.
1	0	10	O	Concomitant
2	12	25	O	administration
3	27	28	O	of
4	30	36	O	XXXXXXXX
5	38	41	O	with
6	43	53	B-D	medications
7	55	58	I-D	that
8	60	62	I-D	are
9	64	73	I-D	inhibitors
10	75	76	I-D	of
11	78	82	I-D	these
12	84	94	I-D	transporter
13	96	103	I-D	proteins
14	106	108	O	e.g
15	111	122	B-D	cyclosporine
16	123	123	O	,
17	125	131	O	certain
18	133	135	O	HIV
19	137	144	B-D	protease
20	146	155	I-D	inhibitors
21	158	160	O	may
22	162	167	O	result
23	169	170	O	in
24	172	180	B-T	increased
25	182	193	I-T	XXXXXXXX
26	195	200	I-T	plasma
27	202	215	I-T	concentrations
28	217	219	O	and
29	221	222	O	an
30	224	232	O	increased
31	234	237	O	risk
32	239	240	O	of
33	242	249	B-E	myopathy
D/6:33:1 D/15:33:1 D/19:33:1

CRESTOR	6053	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	21
Effect of Coadministered Drugs on Rosuvastatin Systemic Exposure Coadministered drug and dosing regimen Rosuvastatin Dose (mg) Single dose unless otherwise noted.
1	0	5	O	Effect
2	7	8	O	of
3	10	23	O	Coadministered
4	25	29	O	Drugs
5	31	32	O	on
6	34	45	O	XXXXXXXX
7	47	54	O	Systemic
8	56	63	O	Exposure
9	65	78	O	Coadministered
10	80	83	O	drug
11	85	87	O	and
12	89	94	O	dosing
13	96	102	O	regimen
14	104	115	O	XXXXXXXX
15	117	120	O	Dose
16	123	124	O	mg
17	127	132	O	Single
18	134	137	O	dose
19	139	144	O	unless
20	146	154	O	otherwise
21	156	160	O	noted
NULL

CRESTOR	6054	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	41
Change in AUC Mean ratio (with/without coadministered drug and no change = 1fold) or % change (with/without coadministered drug and no change = 0%); symbols of * and * indicate the exposure increase and decrease, respectively.
1	0	5	O	Change
2	7	8	O	in
3	10	12	O	AUC
4	14	17	O	Mean
5	19	23	O	ratio
6	26	29	O	with
7	30	30	O	/
8	31	37	O	without
9	39	52	O	coadministered
10	54	57	O	drug
11	59	61	O	and
12	63	64	O	no
13	66	71	O	change
14	73	73	O	=
15	75	79	O	1fold
16	82	83	O	or
17	85	85	O	%
18	87	92	O	change
19	95	98	O	with
20	99	99	O	/
21	100	106	O	without
22	108	121	O	coadministered
23	123	126	O	drug
24	128	130	O	and
25	132	133	O	no
26	135	140	O	change
27	142	142	O	=
28	144	145	O	0%
29	149	155	O	symbols
30	157	158	O	of
31	160	160	O	*
32	162	164	O	and
33	166	166	O	*
34	168	175	O	indicate
35	177	179	O	the
36	181	188	O	exposure
37	190	197	O	increase
38	199	201	O	and
39	203	210	O	decrease
40	211	211	O	,
41	213	224	O	respectively
NULL

CRESTOR	6055	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	101
Change in Cmax Cyclosporine - stable dose required (75mg - 200mgBID) 10mg QD for 10days * 7.1-foldClinically significant * 11-fold Atazanavir/ritonavir combination 300mg/100mgQD for 8days 10mg * 3.1-fold * 7-fold Lopinavir/ritonavir combination 400mg/100mgBID for 17days 20mgQD for 7days * 2.1-fold * 5-fold Gemfibrozil 600mgBID for 7days 80mg * 1.9-fold * 2.2-fold Eltrombopag 75 mg QD, 5 days 10 mg * 1.6-fold * 2-fold Darunavir 600 mg/ritonavir 100 mg BID, 7 days 10 mg QD for 7 days * 1.5-fold * 2.4-fold Tipranavir/ritonavir combination 500mg/200mgBID for 11days 10mg * 26% * 2.2-fold Dronedarone 400 mg BID 10 mg * 1.4-fold Itraconazole 200 mg QD, 5 days 10 mg or 80mg * 39% * 28% * 36% * 15% Ezetimibe 10 mg QD, 14 days 10 mg QD for 14 days * 1.2-fold Fosamprenavir/ritonavir 700mg/100mgBID for 7days 10mg * 8% * 45% Fenofibrate 67mgTID for 7days 10mg * * 21% Rifampicin 450 mg QD, 7 days 20 mg * Aluminum & magnesium hydroxide combination antacid Administered simultaneouslyAdministered 2hours apart 40mg40mg * 54% * 22% * 50% * 16% Ketoconazole 200mgBID for 7days 80mg * 2% * 5% Fluconazole 200mg QD for 11days 80mg * 14% * 9% Erythromycin 500mgQID for 7days 80mg * 20% * 31% Table 5.
1	0	5	O	Change
2	7	8	O	in
3	10	13	O	Cmax
4	15	26	O	Cyclosporine
5	30	35	O	stable
6	37	40	O	dose
7	42	49	O	required
8	52	55	O	75mg
9	59	66	O	200mgBID
10	69	72	O	10mg
11	74	75	O	QD
12	77	79	O	for
13	81	86	O	10days
14	88	88	O	*
15	90	92	O	7.1
16	94	107	O	foldClinically
17	109	119	O	significant
18	121	121	O	*
19	123	124	O	11
20	126	129	O	fold
21	131	140	B-K	Atazanavir
22	141	141	I-K	/
23	142	150	I-K	ritonavir
24	152	162	O	combination
25	164	168	O	300mg
26	169	169	O	/
27	170	176	O	100mgQD
28	178	180	O	for
29	182	186	O	8days
30	188	191	B-K	10mg
31	193	193	I-K	*
32	195	197	O	3.1
33	199	202	O	fold
34	204	204	O	*
35	206	206	O	7
36	208	211	O	fold
37	213	221	B-K	Lopinavir
38	222	222	I-K	/
39	223	231	I-K	ritonavir
40	233	243	O	combination
41	245	249	B-K	400mg
42	250	250	O	/
43	251	258	O	100mgBID
44	260	262	O	for
45	264	269	O	17days
46	271	276	O	20mgQD
47	278	280	O	for
48	282	286	O	7days
49	288	288	O	*
50	290	292	O	2.1
51	294	297	O	fold
52	299	299	O	*
53	301	301	O	5
54	303	306	O	fold
55	308	318	B-K	Gemfibrozil
56	320	327	O	600mgBID
57	329	331	O	for
58	333	337	O	7days
59	339	342	O	80mg
60	344	344	O	*
61	346	348	O	1.9
62	350	353	O	fold
63	355	355	O	*
64	357	359	O	2.2
65	361	364	O	fold
66	366	376	O	Eltrombopag
67	378	379	O	75
68	381	382	O	mg
69	384	385	O	QD
70	386	386	O	,
71	388	388	O	5
72	390	393	O	days
73	395	396	O	10
74	398	399	O	mg
75	401	401	O	*
76	403	405	O	1.6
77	407	410	O	fold
78	412	412	O	*
79	414	414	O	2
80	416	419	O	fold
81	421	429	O	Darunavir
82	431	433	O	600
83	435	436	O	mg
84	437	437	O	/
85	438	446	O	ritonavir
86	448	450	O	100
87	452	453	O	mg
88	455	457	O	BID
89	458	458	O	,
90	460	460	O	7
91	462	465	O	days
92	467	468	O	10
93	470	471	O	mg
94	473	474	O	QD
95	476	478	O	for
96	480	480	O	7
97	482	485	O	days
98	487	487	O	*
99	489	491	O	1.5
100	493	496	O	fold
101	498	498	O	*
K/21:C54355 K/30:C54355 K/37:C54355 K/41:C54355 K/55:C54355

CRESTOR	6056	34090-1	1fd0ba23-962e-427f-8b9d-2cf8f64d0f95	57
Effect of Rosuvastatin Coadministration on Systemic Exposure To Other Drugs Rosuvastatin Dosage Regimen Coadministered Drug Name and Dose Change in AUC Change in Cmax 40mg QD for 10 days WarfarinClinically significant pharmacodynamic effects QD for 21 Days EE * 26% NG * 34% EE * 25% NG * 23% EE = ethinyl estradiol, NG = norgestrel
1	0	5	O	Effect
2	7	8	O	of
3	10	21	O	XXXXXXXX
4	23	38	O	Coadministration
5	40	41	O	on
6	43	50	O	Systemic
7	52	59	O	Exposure
8	61	62	O	To
9	64	68	O	Other
10	70	74	O	Drugs
11	76	87	O	XXXXXXXX
12	89	94	O	Dosage
13	96	102	O	Regimen
14	104	117	O	Coadministered
15	119	122	O	Drug
16	124	127	O	Name
17	129	131	O	and
18	133	136	O	Dose
19	138	143	O	Change
20	145	146	O	in
21	148	150	O	AUC
22	152	157	O	Change
23	159	160	O	in
24	162	165	O	Cmax
25	167	170	O	40mg
26	172	173	O	QD
27	175	177	O	for
28	179	180	O	10
29	182	185	O	days
30	187	204	O	WarfarinClinically
31	206	216	O	significant
32	218	232	O	pharmacodynamic
33	234	240	O	effects
34	242	243	O	QD
35	245	247	O	for
36	249	250	O	21
37	252	255	O	Days
38	257	258	O	EE
39	260	260	O	*
40	262	264	O	26%
41	266	267	O	NG
42	269	269	O	*
43	271	273	O	34%
44	275	276	O	EE
45	278	278	O	*
46	280	282	O	25%
47	284	285	O	NG
48	287	287	O	*
49	289	291	O	23%
50	293	294	O	EE
51	296	296	O	=
52	298	304	B-K	ethinyl
53	306	314	I-K	estradiol
54	315	315	O	,
55	317	318	O	NG
56	320	320	O	=
57	322	331	O	norgestrel
K/52:C54358

