<?xml version="1.0" ?>
<Label drug="Amitriptyline Hydrochloride" setid="ea35caf8-c8c8-481a-97a2-25f68cbc240b">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Amitriptyline hydrochloride is an antidepressant with sedative effects. Its mechanism of action in man is not known. It is not a monoamine oxidase inhibitor and it does not act primarily by stimulation of the central nervous system.  Amitriptyline inhibits the membrane pump mechanism responsible for uptake of norepinephrine and serotonin in adrenergic and serotonergic neurons. Pharmacologically this action may potentiate or prolong neuronal activity since reuptake of these biogenic amines is important physiologically in terminating transmitting activity. This interference with reuptake of norepinephrine and/or serotonin is believed by some to underlie the antidepressant activity of amitriptyline.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1181" section="34073-7">
      

      <SentenceText>Some patients may experience a large increase in amitriptyline concentration in the presence of topiramate and any adjustments in amitriptyline dose should be made according to the patient’s clinical response and not on the basis of plasma levels.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1182" section="34073-7">
      

      <SentenceText>The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the caucasian population (about 7% to 10% of Caucasians are so called “poor metabolizers”); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African, and other populations are not yet available.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="222 18" str="reliable estimates" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="58 44" str="cytochrome P450 2D6 (debrisoquin hydroxylase" type="Precipitant"/>
      <Interaction effect="C54358" id="I1" precipitant="M2" trigger="M1" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1183" section="34073-7">
      

      <SentenceText>Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="23 42" str="higher than expected plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="69 31" str="tricyclic antidepressants (TCAs" type="Precipitant"/>
      <Interaction effect="C54357" id="I2" precipitant="M4" trigger="M3" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1184" section="34073-7">
      

      <SentenceText>Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8-fold increase in plasma AUC of the TCA).</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="133 22" str="increase in plasma AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M6" span="49 8" str="P450 2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I3" precipitant="M6" trigger="M5" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1185" section="34073-7">
      

      <SentenceText>In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M7" span="21 42" str="drugs inhibit the activity of this isozyme" type="Precipitant"/>
      <Interaction id="I4" precipitant="M7" trigger="M7" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1186" section="34073-7">
      

      <SentenceText>An individual who is stable on a given dose of TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1187" section="34073-7">
      

      <SentenceText>The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="61 19" str="are not metabolized" type="Trigger"/>
      <Mention code="NO MAP" id="M9" span="10 50" str="that inhibit cytochrome P450 2D6 include some that" type="Precipitant"/>
      <Interaction effect="C54357" id="I5" precipitant="M9" trigger="M8" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M10" span="96 9" str="quinidine" type="Precipitant"/>
      <Interaction effect="C54358" id="I6" precipitant="M10" trigger="M8" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M11" span="107 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="C54358" id="I7" precipitant="M11" trigger="M8" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M12" span="152 8" str="P450 2D6" type="Precipitant"/>
      <Interaction effect="C54358" id="I8" precipitant="M12" trigger="M8" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1188" section="34073-7">
      

      <SentenceText>While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1189" section="34073-7">
      

      <SentenceText>The extent to which SSRI-TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1190" section="34073-7">
      

      <SentenceText>Nevertheless, caution is indicated in the coadministration of TCAs with any of the SSRIs and also in switching from one class to the other.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="14 10" str="caution is" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1191" section="34073-7">
      

      <SentenceText>Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1192" section="34073-7">
      

      <SentenceText>Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="105 24" str="lower doses than usually" type="Trigger"/>
      <Mention code="NO MAP" id="M15" span="19 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Mention code="NO MAP" id="M16" span="73 10" str="cytochrome" type="Precipitant"/>
      <Interaction id="I9" precipitant="M16" trigger="M14" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M17" span="84 8" str="P450 2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I10" precipitant="M17" trigger="M14" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M18" span="156 24" str="tricyclic antidepressant" type="Precipitant"/>
      <Interaction id="I11" precipitant="M18" trigger="M14" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1193" section="34073-7">
      

      <SentenceText>Furthermore, whenever one of these other drugs is withdrawn from cotherapy, an increased dose of tricyclic antidepressant may be required.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="97 9" str="tricyclic" type="Precipitant"/>
      <Interaction id="I12" precipitant="M19" trigger="M19" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1194" section="34073-7">
      

      <SentenceText>It is desirable to monitor TCA plasma levels whenever a TCA is going to be coadministered with another drug known to be an inhibitor of P450 2D6.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1195" section="34073-7">
      

      <SentenceText>Guanethidine or similarly acting compounds; thyroid medication; alcohol, barbiturates and other CNS depressants; and disulfiram.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1196" section="34073-7">
      

      <SentenceText>When amitriptyline hydrochloride is given with anticholinergic agents or sympathomimetic drugs, including epinephrine combined with local anesthetics, close supervision and careful adjustment of dosages are required.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="157 11" str="supervision" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="173 29" str="careful adjustment of dosages" type="Trigger"/>
      <Mention code="NO MAP" id="M22" span="47 22" str="anticholinergic agents" type="Precipitant"/>
      <Interaction id="I13" precipitant="M22" trigger="M21" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M23" span="73 21" str="sympathomimetic drugs" type="Precipitant"/>
      <Interaction id="I14" precipitant="M23" trigger="M20" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M24" span="106 11" str="epinephrine" type="Precipitant"/>
      <Interaction id="I15" precipitant="M24" trigger="M20" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M25" span="132 17" str="local anesthetics" type="Precipitant"/>
      <Interaction id="I16" precipitant="M25" trigger="M20" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1197" section="34073-7">
      

      <SentenceText>Hyperpyrexia has been reported when amitriptyline hydrochloride is administered with anticholinergic agents or with neuroleptic drugs, particularly during hot weather.</SentenceText>
      

      <Mention code="NO MAP" id="M26" span="0 12" str="Hyperpyrexia" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M27" span="85 22" str="anticholinergic agents" type="Precipitant"/>
      <Interaction effect="M26" id="I17" precipitant="M27" trigger="M27" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M28" span="116 17" str="neuroleptic drugs" type="Precipitant"/>
      <Interaction effect="M26" id="I18" precipitant="M28" trigger="M28" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1198" section="34073-7">
      

      <SentenceText>Paralytic ileus may occur in patients taking tricyclic antidepressants in combination with anticholinergic type drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="10 5" str="ileus" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M30" span="45 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="M29" id="I19" precipitant="M30" trigger="M30" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M31" span="91 26" str="anticholinergic type drugs" type="Precipitant"/>
      <Interaction effect="M29" id="I20" precipitant="M31" trigger="M31" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1199" section="34073-7">
      

      <SentenceText>Cimetidine is reported to reduce hepatic metabolism of certain tricyclic antidepressants, thereby delaying elimination and increasing steady-state concentrations of these drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="33 7" str="hepatic" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M33" span="98 20" str="delaying elimination" type="Trigger"/>
      <Mention code="NO MAP" id="M34" span="123 38" str="increasing steady-state concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M35" span="0 10" str="Cimetidine" type="Precipitant"/>
      <Interaction effect="C54357" id="I21" precipitant="M35" trigger="M34" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M36" span="63 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="M32" id="I22" precipitant="M36" trigger="M33" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1200" section="34073-7">
      

      <SentenceText>Clinically significant effects have been reported with the tricyclic antidepressants when used concomitantly with cimetidine.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="23 7" str="effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M38" span="59 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="M37" id="I23" precipitant="M38" trigger="M38" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M39" span="114 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="M37" id="I24" precipitant="M39" trigger="M39" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1201" section="34073-7">
      

      <SentenceText>Increases in plasma levels of tricyclic antidepressants, and in the frequency and severity of side effects, particularly anticholinergic, have been reported when cimetidine was added to the drug regimen.</SentenceText>
      

      <Mention code="NO MAP" id="M40" span="0 26" str="Increases in plasma levels" type="Trigger"/>
      <Mention code="NO MAP" id="M41" span="30 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Mention code="NO MAP" id="M42" span="121 15" str="anticholinergic" type="Precipitant"/>
      <Mention code="NO MAP" id="M43" span="162 10" str="cimetidine" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1202" section="34073-7">
      

      <SentenceText>Discontinuation of cimetidine in well controlled patients receiving tricyclic antidepressants and cimetidine may decrease the plasma levels and efficacy of the antidepressants.</SentenceText>
      

      <Mention code="NO MAP" id="M44" span="0 15" str="Discontinuation" type="Trigger"/>
      <Mention code="NO MAP" id="M45" span="113 26" str="decrease the plasma levels" type="Trigger"/>
      <Mention code="NO MAP" id="M46" span="98 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="C54357" id="I25" precipitant="M46" trigger="M45" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1203" section="34073-7">
      

      <SentenceText>Caution is advised if patients receive large doses of ethchlorvynol concurrently.</SentenceText>
      

      <Mention code="NO MAP" id="M47" span="0 7" str="Caution" type="Trigger"/>
      <Mention code="NO MAP" id="M48" span="54 13" str="ethchlorvynol" type="Precipitant"/>
      <Interaction id="I26" precipitant="M48" trigger="M47" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1204" section="34073-7">
      

      <SentenceText>Transient delirium has been reported in patients who were treated with one gram of ethchlorvynol and 75 mg to 150 mg of amitriptyline hydrochloride.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1205" section="34090-1">
      

      <SentenceText>Amitriptyline hydrochloride is an antidepressant with sedative effects.</SentenceText>
      

      <Mention code="NO MAP" id="M49" span="54 8" str="sedative" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1206" section="34090-1">
      

      <SentenceText>Its mechanism of action in man is not known.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1207" section="34090-1">
      

      <SentenceText>It is not a monoamine oxidase inhibitor and it does not act primarily by stimulation of the central nervous system.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1208" section="34090-1">
      

      <SentenceText>Amitriptyline inhibits the membrane pump mechanism responsible for uptake of norepinephrine and serotonin in adrenergic and serotonergic neurons.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1209" section="34090-1">
      

      <SentenceText>Pharmacologically this action may potentiate or prolong neuronal activity since reuptake of these biogenic amines is important physiologically in terminating transmitting activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1210" section="34090-1">
      

      <SentenceText>This interference with reuptake of norepinephrine and/or serotonin is believed by some to underlie the antidepressant activity of amitriptyline.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

