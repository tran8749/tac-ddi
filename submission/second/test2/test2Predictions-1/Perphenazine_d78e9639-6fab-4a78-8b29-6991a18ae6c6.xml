<?xml version="1.0" ?>
<Label drug="Perphenazine" setid="d78e9639-6fab-4a78-8b29-6991a18ae6c6">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Pharmacokinetics  Following oral administration of perphenazine tablets, mean peak plasma perphenazine concentrations were observed between 1 to 3 hours. The plasma elimination half-life of perphenazine was independent of dose and ranged between 9 and 12 hours. In a study in which normal volunteers (n=12) received perphenazine 4 mg q8h for 5 days, steady-state concentrations of perphenazine were reached within 72 hours. Mean (%CV) C max and C min values for perphenazine and 7-hydroxyperphenazine at steady-state are listed below:  Parameter    Perphenazine    7-Hydroxyperphenazine    C max (pg/mL)   984 (43)   509 (25)   C min (pg/mL)   442 (76)   350 (56)   Peak 7-hydroxyperphenazine concentrations were observed between 2 to 4 hours with a terminal phase half-life ranging between 9.9 to 18.8 hours. Perphenazine is extensively metabolized in the liver to a number of metabolites by sulfoxidation, hydroxylation, dealkylation, and glucuronidation. The pharmacokinetics of perphenazine covary with the hydroxylation of debrisoquine which is mediated by cytochrome P450 2D6 (CYP 2D6) and thus is subject to genetic polymorphism—i.e., 7% to 10% of Caucasians and a low percentage of Asians have little or no activity and are called “poor metabolizers.” Poor metabolizers of CYP 2D6 will metabolize perphenazine more slowly and will experience higher concentrations compared with normal or “extensive” metabolizers.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Perphenazine" id="4550" section="34073-7">
      

      <SentenceText>Metabolism of a number of medications, including antipsychotics, antidepressants, β-blockers, and antiarrhythmics, occurs through the cytochrome P450 2D6 isoenzyme (debrisoquine hydroxylase).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4551" section="34073-7">
      

      <SentenceText>Approximately 10% of the Caucasian population has reduced activity of this enzyme, so-called “poor” metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="50 16" str="reduced activity" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4552" section="34073-7">
      

      <SentenceText>Among other populations the prevalence is not known.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4553" section="34073-7">
      

      <SentenceText>Poor metabolizers demonstrate higher plasma concentrations of antipsychotic drugs at usual doses, which may correlate with emergence of side effects.</SentenceText>
      

      <Mention code="NO MAP" id="M2" span="30 28" str="higher plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M3" span="62 19" str="antipsychotic drugs" type="Precipitant"/>
      <Interaction effect="C54357" id="I1" precipitant="M3" trigger="M2" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4554" section="34073-7">
      

      <SentenceText>In one study of 45 elderly patients suffering from dementia treated with perphenazine, the 5 patients who were prospectively identified as poor P450 2D6 metabolizers had reported significantly greater side effects during the first 10 days of treatment than the 40 extensive metabolizers, following which the groups tended to converge.</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="139 13" str="poor P450 2D6" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4555" section="34073-7">
      

      <SentenceText>Prospective phenotyping of elderly patients prior to antipsychotic treatment may identify those at risk for adverse events.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="53 13" str="antipsychotic" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4556" section="34073-7">
      

      <SentenceText>The concomitant administration of other drugs that inhibit the activity of P450 2D6 may acutely increase plasma concentrations of antipsychotics.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="96 30" str="increase plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M7" span="40 43" str="drugs that inhibit the activity of P450 2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I2" precipitant="M7" trigger="M6" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M8" span="130 14" str="antipsychotics" type="Precipitant"/>
      <Interaction effect="C54357" id="I3" precipitant="M8" trigger="M6" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4557" section="34073-7">
      

      <SentenceText>Among these are tricyclic antidepressants and selective serotonin reuptake inhibitors, e.g., fluoxetine, sertraline and paroxetine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4558" section="34073-7">
      

      <SentenceText>When prescribing these drugs to patients already receiving antipsychotic therapy, close monitoring is essential and dose reduction may become necessary to avoid toxicity.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="88 10" str="monitoring" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="155 14" str="avoid toxicity" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="59 13" str="antipsychotic" type="Precipitant"/>
      <Interaction id="I4" precipitant="M11" trigger="M10" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4559" section="34073-7">
      

      <SentenceText>Lower doses than usually prescribed for either the antipsychotic or the other drug may be required.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="0 16" str="Lower doses than" type="Trigger"/>
      <Mention code="NO MAP" id="M13" span="51 13" str="antipsychotic" type="Precipitant"/>
      <Interaction id="I5" precipitant="M13" trigger="M12" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4560" section="34090-1">
      

      <SentenceText>Following oral administration of perphenazine tablets, mean peak plasma perphenazine concentrations were observed between 1 to 3 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4561" section="34090-1">
      

      <SentenceText>The plasma elimination half-life of perphenazine was independent of dose and ranged between 9 and 12 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4562" section="34090-1">
      

      <SentenceText>In a study in which normal volunteers (n=12) received perphenazine 4 mg q8h for 5 days, steady-state concentrations of perphenazine were reached within 72 hours.</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="95 20" str="state concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M15" span="72 3" str="q8h" type="Precipitant"/>
      <Interaction effect="C54355" id="I6" precipitant="M15" trigger="M14" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4563" section="34090-1">
      

      <SentenceText>Mean (%CV) Cmax and Cmin values for perphenazine and 7-hydroxyperphenazine at steady-state are listed below: Parameter Perphenazine 7-Hydroxyperphenazine Cmax (pg/mL) 984 (43) 509 (25) Cmin (pg/mL) 442 (76) 350 (56) Peak 7-hydroxyperphenazine concentrations were observed between 2 to 4 hours with a terminal phase half-life ranging between 9.9 to 18.8 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4564" section="34090-1">
      

      <SentenceText>Perphenazine is extensively metabolized in the liver to a number of metabolites by sulfoxidation, hydroxylation, dealkylation, and glucuronidation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Perphenazine" id="4565" section="34090-1">
      

      <SentenceText>The pharmacokinetics of perphenazine covary with the hydroxylation of debrisoquine which is mediated by cytochrome P450 2D6 (CYP 2D6) and thus is subject to genetic polymorphism—i.e., 7% to 10% of Caucasians and a low percentage of Asians have little or no activity and are called “poor metabolizers.” Poor metabolizers of CYP 2D6 will metabolize perphenazine more slowly and will experience higher concentrations compared with normal or “extensive” metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="392 21" str="higher concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M17" span="70 12" str="debrisoquine" type="Precipitant"/>
      <Interaction effect="C54355" id="I7" precipitant="M17" trigger="M16" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M18" span="104 28" str="cytochrome P450 2D6 (CYP 2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I8" precipitant="M18" trigger="M16" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M19" span="323 7" str="CYP 2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I9" precipitant="M19" trigger="M16" type="Pharmacokinetic interaction"/>
    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

