<?xml version="1.0" ?>
<Label drug="Fetzima" setid="f371258d-91b3-4b6a-ac99-434a1964c3af">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS Other than CYP3A4 drug interactions, FETZIMA is predicted, based on in vitro studies, to have a low potential to be involved in clinically significant pharmacokinetic drug interactions. Strong CYP3A4 inhibitors such as ketoconazole : Do not exceed 80 mg once daily ( 7 ). 7.1 Monoamine Oxidase Inhibitors (MAOIs) [see Dosage and Administration ( 2.5 , 2.6 ), Contraindications ( 4 ), and Warnings and Precautions ( 5.2 )]  7.2 Serotonergic Drugs [see Dosage and Administration ( 2.5 , 2.6 ), Contraindications ( 4 ), and Warnings and Precautions ( 5.2 )]  7.3 Drugs that Interfere with Hemostasis (e.g., NSAIDs, Aspirin, and Warfarin) Serotonin release by platelets plays an important role in hemostasis. Epidemiological studies of case-control and cohort design have demonstrated an association between use of psychotropic drugs that interfere with serotonin reuptake and the occurrence of upper gastrointestinal bleeding. These studies have also shown that concurrent use of an NSAID or aspirin may potentiate this risk of bleeding. Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs and SNRIs are co-administered with warfarin. Patients receiving warfarin therapy should be carefully monitored when FETZIMA is initiated or discontinued [see Warnings and Precautions ( 5.5 )] . 7.4 Potential for Other Drugs to Affect FETZIMA Dose adjustment is recommended when FETZIMA is co-administered with strong inhibitors of CYP3A4 (e.g. ketoconazole) [see Dosage and Administration ( 2.7 )]. An in vivo study showed a clinically meaningful increase in levomilnacipran exposure when FETZIMA was co-administered with the CYP3A4 inhibitor ketoconazole (see  Figure 1  ). No dose adjustment of FETZIMA is needed when co-administered with a CYP3A4 inducer or substrate. In vivo studies showed no clinically meaningful change in levomilnacipran exposure when co-administered with the CYP3A4 inducer carbamazepine or the CYP3A4 substrate alprazolam (see  Figure 1  ). No dose adjustment of FETZIMA is needed when co-administered with inhibitors of CYP2C8 , CYP2C19, CYP2D6, CYP2J2, P-glycoprotein, BCRP, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2. In vitro studies suggested that CYP2C8, CYP2C19, CYP2D6, and CYP2J2 had minimal contributions to metabolism of levomilnacipran. In addition, levomilnacipran is not a substrate of BCRP, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2 and is a weak substrate of P-gp.  Figure 1 PK Interactions between Levomilnacipran (LVM) and Other Drugs  Figure 1 7.5 Potential for FETZIMA to Affect Other Drugs No dose adjustment of the concomitant medication is recommended when FETZIMA is administered with a substrate of CYP3A4, CYP1A2, CYP2A6, CYP2C8, CYP2C9, CYP2C19, CYP2D6, CYP2E1, P-gp, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2. In vitro studies have shown that levomilnacipran is not an inhibitor of CYP1A2, CYP2A6, CYP2C8, CYP2C9, CYP2C19, CYP2D6, CYP2E1, P-gp, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2. Concomitant use of FETZIMA with alprazolam or carbamazepine, substrates of CYP3A4, had no significant effect on alprazolam or carbamazepine plasma concentrations (see  Figure 1  ). 7.6 Central Nervous System (CNS)-Active Agents The risk of using FETZIMA in combination with other CNS-active drugs has not been systematically evaluated. Consequently, caution is advised when FETZIMA is prescribed in combination with other CNS-active drugs, including those with a similar mechanism of action. Alcohol  In an in vitro study, alcohol interacted with the extended-release properties of FETZIMA. If FETZIMA is taken with alcohol, a pronounced accelerated drug release may occur. It is recommended that FETZIMA extended-release capsules not be taken with alcohol.</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY 12.1 Mechanism of Action The exact mechanism of the antidepressant action of levomilnacipran is unknown, but is thought to be related to the potentiation of serotonin and norepinephrine in the central nervous system, through inhibition of reuptake at serotonin and norepinephrine transporters. Non-clinical studies have shown that levomilnacipran is a potent and selective serotonin and norepinephrine reuptake inhibitor (SNRI). 12.2 Pharmacodynamics Levomilnacipran binds with high affinity to the human serotonin (5-HT) and norepinephrine (NE) transporters (Ki = 11 and 91 nM, respectively) and potently inhibits 5-HT and NE reuptake (IC50 = 16-19 and 11 nM, respectively). Levomilnacipran lacks significant affinity for any other receptors, ion channels or transporters tested in vitro , including serotonergic (5HT1-7), α- and β- adrenergic, muscarinic, or histaminergic receptors and Ca2+, Na+, K+ or Cl- channels. Levomilnacipran did not inhibit monoamine oxidase (MAO). Cardiovascular Electrophysiology  At a dose 2.5 times the maximum recommended dose, levomilnacipran does not prolong QTc to any clinically relevant extent. 12.3 Pharmacokinetics The concentration of levomilnacipran at steady state is proportional to dose when administered from 25 to 300 mg once daily. Following an oral administration, the mean apparent total clearance of levomilnacipran is 21-29 L/h. Steady-state concentrations of levomilnacipran are predictable from single-dose data. The apparent terminal elimination half-life of levomilnacipran is approximately 12 hours. After daily dosing of FETZIMA 120 mg, the mean C max value is 341 ng/mL, and the mean steady-state AUC value is 5196 ng·h/mL. Interconversion between levomilnacipran and its stereoisomer does not occur in humans. Absorption  The relative bioavailability of levomilnacipran after administration of FETZIMA ER was 92% when compared to oral solution. Levomilnacipran concentration was not significantly affected when FETZIMA was administered with food. The median time to peak concentration (Tmax) of levomilnacipran is 6-8 hours after oral administration. Distribution  Levomilnacipran is widely distributed with an apparent volume of distribution of 387-473 L; plasma protein binding is 22% over concentration range of 10 to 1000 ng/mL. Metabolism  Levomilnacipran undergoes desethylation to form desethyl levomilnacipran and hydroxylation to form p-hydroxy-levomilnacipran. Both oxidative metabolites undergo further conjugation with glucuronide to form conjugates. The desethylation is catalyzed primarily by CYP3A4 with minor contribution by CYP2C8, 2C19, 2D6, and 2J2 [ see Drug Interactions ( 7.4 , 7.5 )].  Elimination/Excretion  Levomilnacipran and its metabolites are eliminated primarily by renal excretion. Following oral administration of 14C-levomilnacipran solution, approximately 58% of the dose is excreted in urine as unchanged levomilnacipran. N-desethyl levomilnacipran is the major metabolite excreted in the urine and accounted for approximately 18% of the dose. Other identifiable metabolites excreted in the urine are levomilnacipran glucuronide (4%), desethyl levomilnacipran glucuronide (3%), p-hydroxy levomilnacipran glucuronide (1%), and p-hydroxy levomilnacipran (1%). The metabolites are inactive [see Dosage and Administration ( 2.3 )] .</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Fetzima" id="5338" section="34073-7">
      

      <SentenceText>Other than CYP3A4 drug interactions, FETZIMA is predicted, based on in vitro studies, to have a low potential to be involved in clinically significant pharmacokinetic drug interactions.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5339" section="34073-7">
      

      <SentenceText>Strong CYP3A4 inhibitors such as ketoconazole: Do not exceed 80 mg once daily (7).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5340" section="34073-7">
      

      <SentenceText>Serotonin release by platelets plays an important role in hemostasis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5341" section="34073-7">
      

      <SentenceText>Epidemiological studies of case-control and cohort design have demonstrated an association between use of psychotropic drugs that interfere with serotonin reuptake and the occurrence of upper gastrointestinal bleeding.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="63 12" str="demonstrated" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="209 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M3" span="106 33" str="psychotropic drugs that interfere" type="Precipitant"/>
      <Interaction effect="M1;M2" id="I1" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M4" span="145 18" str="serotonin reuptake" type="Precipitant"/>
      <Interaction effect="M1;M2" id="I2" precipitant="M4" trigger="M4" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5342" section="34073-7">
      

      <SentenceText>These studies have also shown that concurrent use of an NSAID or aspirin may potentiate this risk of bleeding.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="101 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M6" span="56 5" str="NSAID" type="Precipitant"/>
      <Interaction effect="M5" id="I3" precipitant="M6" trigger="M6" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M7" span="65 7" str="aspirin" type="Precipitant"/>
      <Interaction effect="M5" id="I4" precipitant="M7" trigger="M7" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5343" section="34073-7">
      

      <SentenceText>Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs and SNRIs are co-administered with warfarin.</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="41 18" str="increased bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M9" span="126 8" str="warfarin" type="Precipitant"/>
      <Interaction effect="M8" id="I5" precipitant="M9" trigger="M9" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5344" section="34073-7">
      

      <SentenceText>Patients receiving warfarin therapy should be carefully monitored when FETZIMA is initiated or discontinued.</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="56 9" str="monitored" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="19 8" str="warfarin" type="Precipitant"/>
      <Interaction id="I6" precipitant="M11" trigger="M10" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5345" section="34073-7">
      

      <SentenceText>Dose adjustment is recommended when FETZIMA is co-administered with strong inhibitors of CYP3A4 (e.g. ketoconazole).</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="0 15" str="Dose adjustment" type="Trigger"/>
      <Mention code="NO MAP" id="M13" span="75 20" str="inhibitors of CYP3A4" type="Precipitant"/>
      <Interaction id="I7" precipitant="M13" trigger="M12" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M14" span="102 12" str="ketoconazole" type="Precipitant"/>
      <Interaction id="I8" precipitant="M14" trigger="M12" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5346" section="34073-7">
      

      <SentenceText>An in vivo study showed a clinically meaningful increase in levomilnacipran exposure when FETZIMA was co-administered with the CYP3A4 inhibitor ketoconazole.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="144 12" str="ketoconazole" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5347" section="34073-7">
      

      <SentenceText>No dose adjustment of FETZIMA is needed when co-administered with a CYP3A4 inducer or substrate.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="68 14" str="CYP3A4 inducer" type="Precipitant"/>
      <Interaction id="I9" precipitant="M16" trigger="M16" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5348" section="34073-7">
      

      <SentenceText>In vivo studies showed no clinically meaningful change in levomilnacipran exposure when co-administered with the CYP3A4 inducer carbamazepine or the CYP3A4 substrate alprazolam.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="128 13" str="carbamazepine" type="Precipitant"/>
      <Interaction effect="C54613" id="I10" precipitant="M17" trigger="M17" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M18" span="166 10" str="alprazolam" type="Precipitant"/>
      <Interaction effect="C54613" id="I11" precipitant="M18" trigger="M18" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5349" section="34073-7">
      

      <SentenceText>No dose adjustment of FETZIMA is needed when co-administered with inhibitors of CYP2C8 , CYP2C19, CYP2D6, CYP2J2, P-glycoprotein, BCRP, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="66 20" str="inhibitors of CYP2C8" type="Precipitant"/>
      <Interaction id="I12" precipitant="M19" trigger="M19" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5350" section="34073-7">
      

      <SentenceText>In vitro studies suggested that CYP2C8, CYP2C19, CYP2D6, and CYP2J2 had minimal contributions to metabolism of levomilnacipran.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5351" section="34073-7">
      

      <SentenceText>In addition, levomilnacipran is not a substrate of BCRP, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2 and is a weak substrate of P-gp.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="122 4" str="P-gp" type="Precipitant"/>
      <Interaction effect="C54357" id="I13" precipitant="M20" trigger="M20" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5352" section="34073-7">
      

      <SentenceText>Figure 1 PK Interactions between Levomilnacipran (LVM) and Other Drugs Figure 1 No dose adjustment of the concomitant medication is recommended when FETZIMA is administered with a substrate of CYP3A4, CYP1A2, CYP2A6, CYP2C8, CYP2C9, CYP2C19, CYP2D6, CYP2E1, P-gp, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5353" section="34073-7">
      

      <SentenceText>In vitro studies have shown that levomilnacipran is not an inhibitor of CYP1A2, CYP2A6, CYP2C8, CYP2C9, CYP2C19, CYP2D6, CYP2E1, P-gp, OATP1B1, OATP1B3, OAT1, OAT3, or OCT2.</SentenceText>
      

      <Mention code="NO MAP" id="M21" span="52 16" str="not an inhibitor" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5354" section="34073-7">
      

      <SentenceText>Concomitant use of FETZIMA with alprazolam or carbamazepine, substrates of CYP3A4, had no significant effect on alprazolam or carbamazepine plasma concentrations.</SentenceText>
      

      <Mention code="NO MAP" id="M22" span="32 10" str="alprazolam" type="Precipitant"/>
      <Interaction effect="C54357" id="I14" precipitant="M22" trigger="M22" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M23" span="46 13" str="carbamazepine" type="Precipitant"/>
      <Interaction effect="C54357" id="I15" precipitant="M23" trigger="M23" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M24" span="72 9" str="of CYP3A4" type="Precipitant"/>
      <Interaction effect="C54357" id="I16" precipitant="M24" trigger="M24" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M25" span="112 10" str="alprazolam" type="Precipitant"/>
      <Interaction effect="C54357" id="I17" precipitant="M25" trigger="M25" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M26" span="126 13" str="carbamazepine" type="Precipitant"/>
      <Interaction effect="C54357" id="I18" precipitant="M26" trigger="M26" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5355" section="34073-7">
      

      <SentenceText>The risk of using FETZIMA in combination with other CNS-active drugs has not been systematically evaluated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5356" section="34073-7">
      

      <SentenceText>Consequently, caution is advised when FETZIMA is prescribed in combination with other CNS-active drugs, including those with a similar mechanism of action.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="14 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M28" span="86 16" str="CNS-active drugs" type="Precipitant"/>
      <Interaction id="I19" precipitant="M28" trigger="M27" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5357" section="34073-7">
      

      <SentenceText>Alcohol In an in vitro study, alcohol interacted with the extended-release properties of FETZIMA.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="67 18" str="release properties" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M30" span="0 7" str="Alcohol" type="Precipitant"/>
      <Interaction effect="M29" id="I20" precipitant="M30" trigger="M30" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M31" span="30 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="M29" id="I21" precipitant="M31" trigger="M31" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5358" section="34073-7">
      

      <SentenceText>If FETZIMA is taken with alcohol, a pronounced accelerated drug release may occur.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="25 7" str="alcohol" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5359" section="34073-7">
      

      <SentenceText>It is recommended that FETZIMA extended-release capsules not be taken with alcohol.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5360" section="34090-1">
      

      <SentenceText>The exact mechanism of the antidepressant action of levomilnacipran is unknown, but is thought to be related to the potentiation of serotonin and norepinephrine in the central nervous system, through inhibition of reuptake at serotonin and norepinephrine transporters.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5361" section="34090-1">
      

      <SentenceText>Non-clinical studies have shown that levomilnacipran is a potent and selective serotonin and norepinephrine reuptake inhibitor (SNRI).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5362" section="34090-1">
      

      <SentenceText>Levomilnacipran binds with high affinity to the human serotonin (5-HT) and norepinephrine (NE) transporters (Ki = 11 and 91 nM, respectively) and potently inhibits 5-HT and NE reuptake (IC50 = 16-19 and 11 nM, respectively).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5363" section="34090-1">
      

      <SentenceText>Levomilnacipran lacks significant affinity for any other receptors, ion channels or transporters tested in vitro, including serotonergic (5HT1-7), α- and β- adrenergic, muscarinic, or histaminergic receptors and Ca2+, Na+, K+ or Cl- channels.</SentenceText>
      

      <Mention code="NO MAP" id="M33" span="157 10" str="adrenergic" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5364" section="34090-1">
      

      <SentenceText>Levomilnacipran did not inhibit monoamine oxidase (MAO).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5365" section="34090-1">
      

      <SentenceText>Cardiovascular Electrophysiology At a dose 2.5 times the maximum recommended dose, levomilnacipran does not prolong QTc to any clinically relevant extent.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5366" section="34090-1">
      

      <SentenceText>The concentration of levomilnacipran at steady state is proportional to dose when administered from 25 to 300 mg once daily.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5367" section="34090-1">
      

      <SentenceText>Following an oral administration, the mean apparent total clearance of levomilnacipran is 21-29 L/h.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5368" section="34090-1">
      

      <SentenceText>Steady-state concentrations of levomilnacipran are predictable from single-dose data.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="0 27" str="Steady-state concentrations" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5369" section="34090-1">
      

      <SentenceText>The apparent terminal elimination half-life of levomilnacipran is approximately 12 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5370" section="34090-1">
      

      <SentenceText>After daily dosing of FETZIMA 120 mg, the mean Cmax value is 341 ng/mL, and the mean steady-state AUC value is 5196 ng·h/mL.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5371" section="34090-1">
      

      <SentenceText>Interconversion between levomilnacipran and its stereoisomer does not occur in humans.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5372" section="34090-1">
      

      <SentenceText>Absorption The relative bioavailability of levomilnacipran after administration of FETZIMA ER was 92% when compared to oral solution.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="119 13" str="oral solution" type="Precipitant"/>
      <Interaction effect="C54356" id="I22" precipitant="M35" trigger="M35" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5373" section="34090-1">
      

      <SentenceText>Levomilnacipran concentration was not significantly affected when FETZIMA was administered with food.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5374" section="34090-1">
      

      <SentenceText>The median time to peak concentration (Tmax) of levomilnacipran is 6-8 hours after oral administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5375" section="34090-1">
      

      <SentenceText>Distribution Levomilnacipran is widely distributed with an apparent volume of distribution of 387-473 L; plasma protein binding is 22% over concentration range of 10 to 1000 ng/mL.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5376" section="34090-1">
      

      <SentenceText>Metabolism Levomilnacipran undergoes desethylation to form desethyl levomilnacipran and hydroxylation to form p-hydroxy-levomilnacipran.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5377" section="34090-1">
      

      <SentenceText>Both oxidative metabolites undergo further conjugation with glucuronide to form conjugates.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="60 11" str="glucuronide" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5378" section="34090-1">
      

      <SentenceText>The desethylation is catalyzed primarily by CYP3A4 with minor contribution by CYP2C8, 2C19, 2D6, and 2J2.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="92 3" str="2D6" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5379" section="34090-1">
      

      <SentenceText>Elimination/Excretion Levomilnacipran and its metabolites are eliminated primarily by renal excretion.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5380" section="34090-1">
      

      <SentenceText>Following oral administration of 14C-levomilnacipran solution, approximately 58% of the dose is excreted in urine as unchanged levomilnacipran.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5381" section="34090-1">
      

      <SentenceText>N-desethyl levomilnacipran is the major metabolite excreted in the urine and accounted for approximately 18% of the dose.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="0 10" str="N-desethyl" type="Precipitant"/>
      <Interaction effect="C54355" id="I23" precipitant="M38" trigger="M38" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5382" section="34090-1">
      

      <SentenceText>Other identifiable metabolites excreted in the urine are levomilnacipran glucuronide (4%), desethyl levomilnacipran glucuronide (3%), p-hydroxy levomilnacipran glucuronide (1%), and p-hydroxy levomilnacipran (1%).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Fetzima" id="5383" section="34090-1">
      

      <SentenceText>The metabolites are inactive.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

