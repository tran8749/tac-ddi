SAPHRIS	5933	34073-7	5429f134-839f-4ffc-9944-55f51238def8	7
Antihypertensive Drugs: SAPHRIS may cause hypotension.
1	0	15	B-D	Antihypertensive
2	17	21	I-D	Drugs
3	22	22	O	:
4	24	30	O	XXXXXXXX
5	32	34	O	may
6	36	40	O	cause
7	42	52	B-E	hypotension
D/1:7:1

SAPHRIS	5934	34073-7	5429f134-839f-4ffc-9944-55f51238def8	16
Paroxetine (CYP2D6 substrate and inhibitor): Reduce paroxetine by half when used in combination with SAPHRIS.
1	0	9	O	Paroxetine
2	12	17	O	CYP2D6
3	19	27	O	substrate
4	29	31	O	and
5	33	41	O	inhibitor
6	43	43	O	:
7	45	50	O	Reduce
8	52	61	O	paroxetine
9	63	64	O	by
10	66	69	O	half
11	71	74	O	when
12	76	79	O	used
13	81	82	O	in
14	84	94	O	combination
15	96	99	O	with
16	101	107	O	XXXXXXXX
NULL

SAPHRIS	5935	34073-7	5429f134-839f-4ffc-9944-55f51238def8	42
Table 12: Clinically Important Drug Interactions with SAPHRIS Concomitant Drug Name or Drug Class Clinical Rationale Clinical Recommendation Antihypertensive Drugs Because of its A1-adrenergic antagonism with potential for inducing hypotension, SAPHRIS may enhance the effects of certain antihypertensive agents.
1	0	4	O	Table
2	6	7	O	12
3	8	8	O	:
4	10	19	O	Clinically
5	21	29	O	Important
6	31	34	O	Drug
7	36	47	O	Interactions
8	49	52	O	with
9	54	60	O	XXXXXXXX
10	62	72	O	Concomitant
11	74	77	O	Drug
12	79	82	O	Name
13	84	85	O	or
14	87	90	O	Drug
15	92	96	O	Class
16	98	105	O	Clinical
17	107	115	O	Rationale
18	117	124	O	Clinical
19	126	139	O	Recommendation
20	141	156	O	Antihypertensive
21	158	162	O	Drugs
22	164	170	O	Because
23	172	173	O	of
24	175	177	O	its
25	179	180	O	A1
26	182	191	B-D	adrenergic
27	193	202	O	antagonism
28	204	207	O	with
29	209	217	O	potential
30	219	221	O	for
31	223	230	O	inducing
32	232	242	B-E	hypotension
33	243	243	O	,
34	245	251	O	XXXXXXXX
35	253	255	O	may
36	257	263	B-E	enhance
37	265	267	I-E	the
38	269	275	I-E	effects
39	277	278	O	of
40	280	286	O	certain
41	288	303	B-D	antihypertensive
42	305	310	I-D	agents
D/26:32:1 D/26:36:1 D/41:32:1 D/41:36:1

SAPHRIS	5936	34073-7	5429f134-839f-4ffc-9944-55f51238def8	10
Monitor blood pressure and adjust dosage of antihypertensive drug accordingly.
1	0	6	B-T	Monitor
2	8	12	O	blood
3	14	21	O	pressure
4	23	25	O	and
5	27	32	B-T	adjust
6	34	39	O	dosage
7	41	42	O	of
8	44	59	B-U	antihypertensive
9	61	64	O	drug
10	66	76	O	accordingly
NULL

SAPHRIS	5937	34073-7	5429f134-839f-4ffc-9944-55f51238def8	11
Strong CYP1A2 Inhibitors (e.g., Fluvoxamine) SAPHRIS is metabolized by CYP1A2.
1	0	5	O	Strong
2	7	12	O	CYP1A2
3	14	23	O	Inhibitors
4	26	28	O	e.g
5	30	30	O	,
6	32	42	O	Fluvoxamine
7	45	51	O	XXXXXXXX
8	53	54	O	is
9	56	66	O	metabolized
10	68	69	O	by
11	71	76	O	CYP1A2
NULL

SAPHRIS	5938	34073-7	5429f134-839f-4ffc-9944-55f51238def8	19
Marginal increase of asenapine exposure was observed when SAPHRIS is used with fluvoxamine at 25 mg administered twice daily.
1	0	7	O	Marginal
2	9	16	O	increase
3	18	19	O	of
4	21	29	O	XXXXXXXX
5	31	38	O	exposure
6	40	42	O	was
7	44	51	O	observed
8	53	56	O	when
9	58	64	O	XXXXXXXX
10	66	67	O	is
11	69	72	O	used
12	74	77	O	with
13	79	89	O	fluvoxamine
14	91	92	O	at
15	94	95	O	25
16	97	98	O	mg
17	100	111	O	administered
18	113	117	O	twice
19	119	123	O	daily
NULL

SAPHRIS	5939	34073-7	5429f134-839f-4ffc-9944-55f51238def8	8
However, the tested fluvoxamine dose was suboptimal.
1	0	6	O	However
2	7	7	O	,
3	9	11	O	the
4	13	18	O	tested
5	20	30	O	fluvoxamine
6	32	35	O	dose
7	37	39	O	was
8	41	50	O	suboptimal
NULL

SAPHRIS	5940	34073-7	5429f134-839f-4ffc-9944-55f51238def8	15
Full therapeutic dose of fluvoxamine is expected to cause a greater increase in asenapine exposure.
1	0	3	O	Full
2	5	15	O	therapeutic
3	17	20	O	dose
4	22	23	O	of
5	25	35	B-K	fluvoxamine
6	37	38	O	is
7	40	47	O	expected
8	49	50	O	to
9	52	56	O	cause
10	58	58	O	a
11	60	66	O	greater
12	68	75	O	increase
13	77	78	O	in
14	80	88	O	XXXXXXXX
15	90	97	B-T	exposure
K/5:C54355

SAPHRIS	5941	34073-7	5429f134-839f-4ffc-9944-55f51238def8	11
Dosage reduction for SAPHRIS based on clinical response may be necessary.
1	0	5	O	Dosage
2	7	15	O	reduction
3	17	19	O	for
4	21	27	O	XXXXXXXX
5	29	33	O	based
6	35	36	O	on
7	38	45	O	clinical
8	47	54	O	response
9	56	58	O	may
10	60	61	O	be
11	63	71	O	necessary
NULL

SAPHRIS	5942	34073-7	5429f134-839f-4ffc-9944-55f51238def8	19
CYP2D6 substrates and inhibitors (e.g., paroxetine) SAPHRIS may enhance the inhibitory effects of paroxetine on its own metabolism.
1	0	5	B-D	CYP2D6
2	7	16	I-D	substrates
3	18	20	O	and
4	22	31	O	inhibitors
5	34	36	O	e.g
6	38	38	O	,
7	40	49	O	paroxetine
8	52	58	O	XXXXXXXX
9	60	62	O	may
10	64	70	B-E	enhance
11	72	74	I-E	the
12	76	85	I-E	inhibitory
13	87	93	I-E	effects
14	95	96	O	of
15	98	107	O	paroxetine
16	109	110	O	on
17	112	114	O	its
18	116	118	O	own
19	120	129	O	metabolism
D/1:10:1

SAPHRIS	5943	34073-7	5429f134-839f-4ffc-9944-55f51238def8	19
Concomitant use of paroxetine with SAPHRIS increased the paroxetine exposure by 2-fold as compared to use paroxetine alone.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	28	O	paroxetine
5	30	33	O	with
6	35	41	O	XXXXXXXX
7	43	51	O	increased
8	53	55	O	the
9	57	66	O	paroxetine
10	68	75	O	exposure
11	77	78	O	by
12	80	80	O	2
13	82	85	O	fold
14	87	88	O	as
15	90	97	O	compared
16	99	100	O	to
17	102	104	O	use
18	106	115	O	paroxetine
19	117	121	O	alone
NULL

SAPHRIS	5944	34073-7	5429f134-839f-4ffc-9944-55f51238def8	13
Reduce paroxetine dose by half when paroxetine is used in combination with SAPHRIS.
1	0	5	O	Reduce
2	7	16	O	paroxetine
3	18	21	O	dose
4	23	24	O	by
5	26	29	O	half
6	31	34	O	when
7	36	45	O	paroxetine
8	47	48	O	is
9	50	53	O	used
10	55	56	O	in
11	58	68	O	combination
12	70	73	O	with
13	75	81	O	XXXXXXXX
NULL

SAPHRIS	5945	34073-7	5429f134-839f-4ffc-9944-55f51238def8	12
No dosage adjustment of SAPHRIS is necessary when administered concomitantly with paroxetine.
1	0	1	O	No
2	3	8	O	dosage
3	10	19	O	adjustment
4	21	22	O	of
5	24	30	O	XXXXXXXX
6	32	33	O	is
7	35	43	O	necessary
8	45	48	O	when
9	50	61	O	administered
10	63	75	O	concomitantly
11	77	80	O	with
12	82	91	O	paroxetine
NULL

SAPHRIS	5946	34073-7	5429f134-839f-4ffc-9944-55f51238def8	40
In addition, valproic acid and lithium pre-dose serum concentrations collected from an adjunctive therapy study were comparable between asenapine-treated patients and placebo-treated patients indicating a lack of effect of asenapine on valproic and lithium plasma levels.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	20	B-K	valproic
5	22	25	I-K	acid
6	27	29	O	and
7	31	37	B-K	lithium
8	39	41	O	pre
9	43	46	B-T	dose
10	48	52	I-T	serum
11	54	67	I-T	concentrations
12	69	77	O	collected
13	79	82	O	from
14	84	85	O	an
15	87	96	O	adjunctive
16	98	104	O	therapy
17	106	110	O	study
18	112	115	O	were
19	117	126	O	comparable
20	128	134	O	between
21	136	144	O	XXXXXXXX
22	146	152	O	treated
23	154	161	O	patients
24	163	165	O	and
25	167	173	O	placebo
26	175	181	O	treated
27	183	190	O	patients
28	192	201	O	indicating
29	203	203	O	a
30	205	208	O	lack
31	210	211	O	of
32	213	218	O	effect
33	220	221	O	of
34	223	231	O	XXXXXXXX
35	233	234	O	on
36	236	243	B-K	valproic
37	245	247	O	and
38	249	255	B-K	lithium
39	257	262	O	plasma
40	264	269	O	levels
K/4:C54357 K/7:C54357 K/36:C54357 K/38:C54611

SAPHRIS	5947	34090-1	5429f134-839f-4ffc-9944-55f51238def8	16
The mechanism of action of asenapine, in schizophrenia and bipolar I disorder, is unknown.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	22	O	action
5	24	25	O	of
6	27	35	O	XXXXXXXX
7	36	36	O	,
8	38	39	O	in
9	41	53	O	schizophrenia
10	55	57	O	and
11	59	65	O	bipolar
12	67	67	O	I
13	69	76	O	disorder
14	77	77	O	,
15	79	80	O	is
16	82	88	O	unknown
NULL

SAPHRIS	5948	34090-1	5429f134-839f-4ffc-9944-55f51238def8	26
It has been suggested that the efficacy of asenapine in schizophrenia could be mediated through a combination of antagonist activity at D2 and 5-HT2A receptors.
1	0	1	O	It
2	3	5	O	has
3	7	10	O	been
4	12	20	O	suggested
5	22	25	O	that
6	27	29	O	the
7	31	38	O	efficacy
8	40	41	O	of
9	43	51	O	XXXXXXXX
10	53	54	O	in
11	56	68	O	schizophrenia
12	70	74	O	could
13	76	77	O	be
14	79	86	O	mediated
15	88	94	O	through
16	96	96	O	a
17	98	108	O	combination
18	110	111	O	of
19	113	122	O	antagonist
20	124	131	O	activity
21	133	134	O	at
22	136	137	O	D2
23	139	141	O	and
24	143	143	O	5
25	145	148	O	HT2A
26	150	158	O	receptors
NULL

SAPHRIS	5949	34090-1	5429f134-839f-4ffc-9944-55f51238def8	101
Asenapine exhibits high affinity for serotonin 5-HT1A, 5-HT1B, 5-HT2A, 5-HT2B, 5-HT2C, 5-HT5A, 5-HT6, and 5-HT7 receptors (Ki values of 2.5, 2.7, 0.07, 0.18, 0.03, 1.6, 0.25, and 0.11nM, respectively), dopamine D2A, D2B, D3, D4, and D1 receptors (Ki values of 1.3, 1.4, 0.42, 1.1, and 1.4 nM, respectively), A1A, A2A, A2B, and A2C -adrenergic receptors (Ki values of 1.2, 1.2, 0.33 and 1.2 nM, respectively), and histamine H1 receptors (Ki value 1.0 nM), and moderate affinity for H2 receptors (Ki value of 6.2 nM).
1	0	8	O	XXXXXXXX
2	10	17	O	exhibits
3	19	22	O	high
4	24	31	O	affinity
5	33	35	O	for
6	37	45	O	serotonin
7	47	47	O	5
8	49	52	O	HT1A
9	53	53	O	,
10	55	55	O	5
11	57	60	O	HT1B
12	61	61	O	,
13	63	63	O	5
14	65	68	O	HT2A
15	69	69	O	,
16	71	71	O	5
17	73	76	O	HT2B
18	77	77	O	,
19	79	79	O	5
20	81	84	O	HT2C
21	85	85	O	,
22	87	87	O	5
23	89	92	O	HT5A
24	93	93	O	,
25	95	95	O	5
26	97	99	O	HT6
27	100	100	O	,
28	102	104	O	and
29	106	106	O	5
30	108	110	O	HT7
31	112	120	B-K	receptors
32	123	124	I-K	Ki
33	126	131	O	values
34	133	134	O	of
35	136	138	O	2.5
36	139	139	O	,
37	141	143	O	2.7
38	144	144	O	,
39	146	149	O	0.07
40	150	150	O	,
41	152	155	O	0.18
42	156	156	O	,
43	158	161	O	0.03
44	162	162	O	,
45	164	166	O	1.6
46	167	167	O	,
47	169	172	O	0.25
48	173	173	O	,
49	175	177	O	and
50	179	184	O	0.11nM
51	185	185	O	,
52	187	198	O	respectively
53	200	200	O	,
54	202	209	O	dopamine
55	211	213	O	D2A
56	214	214	O	,
57	216	218	O	D2B
58	219	219	O	,
59	221	222	O	D3
60	223	223	O	,
61	225	226	O	D4
62	227	227	O	,
63	229	231	O	and
64	233	234	O	D1
65	236	244	O	receptors
66	247	248	O	Ki
67	250	255	O	values
68	257	258	O	of
69	260	262	O	1.3
70	263	263	O	,
71	265	267	O	1.4
72	268	268	O	,
73	270	273	O	0.42
74	274	274	O	,
75	276	278	O	1.1
76	279	279	O	,
77	281	283	O	and
78	285	287	O	1.4
79	289	290	O	nM
80	291	291	O	,
81	293	304	O	respectively
82	306	306	O	,
83	308	310	O	A1A
84	311	311	O	,
85	313	315	O	A2A
86	316	316	O	,
87	318	320	O	A2B
88	321	321	O	,
89	323	325	O	and
90	327	329	O	A2C
91	332	341	O	adrenergic
92	343	351	B-D	receptors
93	354	355	O	Ki
94	357	362	O	values
95	364	365	O	of
96	367	369	O	1.2
97	370	370	O	,
98	372	374	O	1.2
99	375	375	O	,
100	377	380	O	0.33
101	382	384	O	and
K/31:C54610

SAPHRIS	5950	34090-1	5429f134-839f-4ffc-9944-55f51238def8	12
In in vitro assays asenapine acts as an antagonist at these receptors.
1	0	1	O	In
2	3	4	O	in
3	6	10	O	vitro
4	12	17	O	assays
5	19	27	O	XXXXXXXX
6	29	32	O	acts
7	34	35	O	as
8	37	38	O	an
9	40	49	O	antagonist
10	51	52	O	at
11	54	58	O	these
12	60	68	O	receptors
NULL

SAPHRIS	5951	34090-1	5429f134-839f-4ffc-9944-55f51238def8	18
Asenapine has no appreciable affinity for muscarinic cholinergic receptors (e.g., Ki value of 8128 nM for M1).
1	0	8	O	XXXXXXXX
2	10	12	O	has
3	14	15	O	no
4	17	27	O	appreciable
5	29	36	O	affinity
6	38	40	O	for
7	42	51	O	muscarinic
8	53	63	O	cholinergic
9	65	73	O	receptors
10	76	78	O	e.g
11	80	80	O	,
12	82	83	O	Ki
13	85	89	O	value
14	91	92	O	of
15	94	97	O	8128
16	99	100	O	nM
17	102	104	O	for
18	106	107	O	M1
NULL

SAPHRIS	5952	34090-1	5429f134-839f-4ffc-9944-55f51238def8	28
Following a single 5 mg dose of SAPHRIS, the mean Cmax was approximately 4 ng/mL and was observed at a mean tmax of 1 hour.
1	0	8	O	Following
2	10	10	O	a
3	12	17	O	single
4	19	19	O	5
5	21	22	O	mg
6	24	27	O	dose
7	29	30	O	of
8	32	38	O	XXXXXXXX
9	39	39	O	,
10	41	43	O	the
11	45	48	O	mean
12	50	53	O	Cmax
13	55	57	O	was
14	59	71	O	approximately
15	73	73	O	4
16	75	76	O	ng
17	77	77	O	/
18	78	79	O	mL
19	81	83	O	and
20	85	87	O	was
21	89	96	O	observed
22	98	99	O	at
23	101	101	O	a
24	103	106	O	mean
25	108	111	O	tmax
26	113	114	O	of
27	116	116	O	1
28	118	121	O	hour
NULL

SAPHRIS	5953	34090-1	5429f134-839f-4ffc-9944-55f51238def8	19
Elimination of asenapine is primarily through direct glucuronidation by UGT1A4 and oxidative metabolism by cytochrome P450 isoenzymes (predominantly CYP1A2).
1	0	10	O	Elimination
2	12	13	O	of
3	15	23	O	XXXXXXXX
4	25	26	O	is
5	28	36	O	primarily
6	38	44	O	through
7	46	51	O	direct
8	53	67	O	glucuronidation
9	69	70	O	by
10	72	77	O	UGT1A4
11	79	81	O	and
12	83	91	O	oxidative
13	93	102	O	metabolism
14	104	105	O	by
15	107	116	O	cytochrome
16	118	121	O	P450
17	123	132	O	isoenzymes
18	135	147	O	predominantly
19	149	154	O	CYP1A2
NULL

SAPHRIS	5954	34090-1	5429f134-839f-4ffc-9944-55f51238def8	17
Following an initial more rapid distribution phase, the mean terminal half-life is approximately 24 hrs.
1	0	8	O	Following
2	10	11	O	an
3	13	19	O	initial
4	21	24	O	more
5	26	30	O	rapid
6	32	43	O	distribution
7	45	49	O	phase
8	50	50	O	,
9	52	54	O	the
10	56	59	O	mean
11	61	68	O	terminal
12	70	73	O	half
13	75	78	O	life
14	80	81	O	is
15	83	95	O	approximately
16	97	98	O	24
17	100	102	O	hrs
NULL

SAPHRIS	5955	34090-1	5429f134-839f-4ffc-9944-55f51238def8	14
With multiple-dose twice-daily dosing, steady-state is attained within 3 days.
1	0	3	O	With
2	5	12	O	multiple
3	14	17	O	dose
4	19	23	O	twice
5	25	29	O	daily
6	31	36	O	dosing
7	37	37	O	,
8	39	44	O	steady
9	46	50	O	state
10	52	53	O	is
11	55	62	O	attained
12	64	69	O	within
13	71	71	O	3
14	73	76	O	days
NULL

SAPHRIS	5956	34090-1	5429f134-839f-4ffc-9944-55f51238def8	12
Overall, steady-state asenapine pharmacokinetics are similar to single-dose pharmacokinetics.
1	0	6	O	Overall
2	7	7	O	,
3	9	14	O	steady
4	16	20	O	state
5	22	30	O	XXXXXXXX
6	32	47	O	pharmacokinetics
7	49	51	O	are
8	53	59	O	similar
9	61	62	O	to
10	64	69	O	single
11	71	74	O	dose
12	76	91	O	pharmacokinetics
NULL

SAPHRIS	5957	34090-1	5429f134-839f-4ffc-9944-55f51238def8	20
Absorption: Following sublingual administration, asenapine is rapidly absorbed with peak plasma concentrations occurring within 0.5 to 1.5 hours.
1	0	9	O	Absorption
2	10	10	O	:
3	12	20	O	Following
4	22	31	O	sublingual
5	33	46	O	administration
6	47	47	O	,
7	49	57	O	XXXXXXXX
8	59	60	O	is
9	62	68	O	rapidly
10	70	77	O	absorbed
11	79	82	O	with
12	84	87	O	peak
13	89	94	O	plasma
14	96	109	O	concentrations
15	111	119	O	occurring
16	121	126	O	within
17	128	130	O	0.5
18	132	133	O	to
19	135	137	O	1.5
20	139	143	O	hours
NULL

SAPHRIS	5958	34090-1	5429f134-839f-4ffc-9944-55f51238def8	11
The absolute bioavailability of sublingual asenapine at 5 mg is 35%.
1	0	2	O	The
2	4	11	O	absolute
3	13	27	O	bioavailability
4	29	30	O	of
5	32	41	O	sublingual
6	43	51	O	XXXXXXXX
7	53	54	O	at
8	56	56	O	5
9	58	59	O	mg
10	61	62	O	is
11	64	66	O	35%
NULL

SAPHRIS	5959	34090-1	5429f134-839f-4ffc-9944-55f51238def8	32
Increasing the dose from 5 mg to 10 mg twice daily (a two-fold increase) results in less than linear (1.7 times) increases in both the extent of exposure and maximum concentration.
1	0	9	B-T	Increasing
2	11	13	I-T	the
3	15	18	I-T	dose
4	20	23	O	from
5	25	25	O	5
6	27	28	O	mg
7	30	31	O	to
8	33	34	O	10
9	36	37	O	mg
10	39	43	O	twice
11	45	49	O	daily
12	52	52	O	a
13	54	56	O	two
14	58	61	O	fold
15	63	70	O	increase
16	73	79	O	results
17	81	82	O	in
18	84	87	O	less
19	89	92	O	than
20	94	99	O	linear
21	102	104	O	1.7
22	106	110	O	times
23	113	121	O	increases
24	123	124	O	in
25	126	129	O	both
26	131	133	O	the
27	135	140	O	extent
28	142	143	O	of
29	145	152	O	exposure
30	154	156	O	and
31	158	164	O	maximum
32	166	178	O	concentration
NULL

SAPHRIS	5960	34090-1	5429f134-839f-4ffc-9944-55f51238def8	15
The absolute bioavailability of asenapine when swallowed is low (<2% with an oral tablet formulation).
1	0	2	O	The
2	4	11	O	absolute
3	13	27	O	bioavailability
4	29	30	O	of
5	32	40	O	XXXXXXXX
6	42	45	O	when
7	47	55	O	swallowed
8	57	58	O	is
9	60	62	O	low
10	65	67	O	<2%
11	69	72	O	with
12	74	75	O	an
13	77	80	O	oral
14	82	87	O	tablet
15	89	99	O	formulation
NULL

SAPHRIS	5961	34090-1	5429f134-839f-4ffc-9944-55f51238def8	17
The intake of water several (2 or 5) minutes after asenapine administration resulted in decreased asenapine exposure.
1	0	2	O	The
2	4	9	O	intake
3	11	12	O	of
4	14	18	O	water
5	20	26	O	several
6	29	29	O	2
7	31	32	O	or
8	34	34	O	5
9	37	43	O	minutes
10	45	49	O	after
11	51	59	O	XXXXXXXX
12	61	74	O	administration
13	76	83	O	resulted
14	85	86	O	in
15	88	96	O	decreased
16	98	106	O	XXXXXXXX
17	108	115	O	exposure
NULL

SAPHRIS	5962	34090-1	5429f134-839f-4ffc-9944-55f51238def8	13
Therefore, eating and drinking should be avoided for 10 minutes after administration.
1	0	8	O	Therefore
2	9	9	O	,
3	11	16	O	eating
4	18	20	O	and
5	22	29	O	drinking
6	31	36	O	should
7	38	39	O	be
8	41	47	B-T	avoided
9	49	51	O	for
10	53	54	O	10
11	56	62	O	minutes
12	64	68	O	after
13	70	83	O	administration
NULL

SAPHRIS	5963	34090-1	5429f134-839f-4ffc-9944-55f51238def8	24
Distribution: Asenapine is rapidly distributed and has a large volume of distribution (approximately 20 - 25 L/kg), indicating extensive extravascular distribution.
1	0	11	O	Distribution
2	12	12	O	:
3	14	22	O	XXXXXXXX
4	24	25	O	is
5	27	33	O	rapidly
6	35	45	O	distributed
7	47	49	O	and
8	51	53	O	has
9	55	55	O	a
10	57	61	O	large
11	63	68	O	volume
12	70	71	O	of
13	73	84	O	distribution
14	87	99	O	approximately
15	101	102	O	20
16	106	107	O	25
17	109	109	O	L
18	110	110	O	/
19	111	112	O	kg
20	114	114	O	,
21	116	125	O	indicating
22	127	135	O	extensive
23	137	149	O	extravascular
24	151	162	O	distribution
NULL

SAPHRIS	5964	34090-1	5429f134-839f-4ffc-9944-55f51238def8	15
Asenapine is highly bound (95%) to plasma proteins, including albumin and A1-acid glycoprotein.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	18	O	highly
4	20	24	O	bound
5	27	29	O	95%
6	32	33	O	to
7	35	40	O	plasma
8	42	49	O	proteins
9	50	50	O	,
10	52	60	O	including
11	62	68	O	albumin
12	70	72	O	and
13	74	75	O	A1
14	77	80	O	acid
15	82	93	O	glycoprotein
NULL

SAPHRIS	5965	34090-1	5429f134-839f-4ffc-9944-55f51238def8	24
Metabolism and Elimination: Direct glucuronidation by UGT1A4 and oxidative metabolism by cytochrome P450 isoenzymes (predominantly CYP1A2) are the primary metabolic pathways for asenapine.
1	0	9	B-T	Metabolism
2	11	13	O	and
3	15	25	O	Elimination
4	26	26	O	:
5	28	33	O	Direct
6	35	49	O	glucuronidation
7	51	52	O	by
8	54	59	O	UGT1A4
9	61	63	O	and
10	65	73	O	oxidative
11	75	84	O	metabolism
12	86	87	O	by
13	89	98	O	cytochrome
14	100	103	O	P450
15	105	114	O	isoenzymes
16	117	129	O	predominantly
17	131	136	O	CYP1A2
18	139	141	O	are
19	143	145	O	the
20	147	153	O	primary
21	155	163	O	metabolic
22	165	172	O	pathways
23	174	176	O	for
24	178	186	O	XXXXXXXX
NULL

SAPHRIS	5966	34090-1	5429f134-839f-4ffc-9944-55f51238def8	17
Asenapine is a high clearance drug with a clearance after intravenous administration of 52 L/h.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	13	O	a
4	15	18	O	high
5	20	28	O	clearance
6	30	33	O	drug
7	35	38	O	with
8	40	40	O	a
9	42	50	O	clearance
10	52	56	O	after
11	58	68	O	intravenous
12	70	83	O	administration
13	85	86	O	of
14	88	89	O	52
15	91	91	O	L
16	92	92	O	/
17	93	93	O	h
NULL

SAPHRIS	5967	34090-1	5429f134-839f-4ffc-9944-55f51238def8	30
In this circumstance, hepatic clearance is influenced primarily by changes in liver blood flow rather than by changes in the intrinsic clearance, i.e., the metabolizing enzymatic activity.
1	0	1	O	In
2	3	6	O	this
3	8	19	O	circumstance
4	20	20	O	,
5	22	28	O	hepatic
6	30	38	O	clearance
7	40	41	O	is
8	43	52	O	influenced
9	54	62	O	primarily
10	64	65	O	by
11	67	73	O	changes
12	75	76	O	in
13	78	82	O	liver
14	84	88	O	blood
15	90	93	O	flow
16	95	100	O	rather
17	102	105	O	than
18	107	108	O	by
19	110	116	O	changes
20	118	119	O	in
21	121	123	O	the
22	125	133	O	intrinsic
23	135	143	O	clearance
24	144	144	O	,
25	146	148	O	i.e
26	150	150	O	,
27	152	154	O	the
28	156	167	O	metabolizing
29	169	177	O	enzymatic
30	179	186	O	activity
NULL

SAPHRIS	5968	34090-1	5429f134-839f-4ffc-9944-55f51238def8	18
Following an initial more rapid distribution phase, the terminal half-life of asenapine is approximately 24 hours.
1	0	8	O	Following
2	10	11	O	an
3	13	19	O	initial
4	21	24	O	more
5	26	30	O	rapid
6	32	43	O	distribution
7	45	49	O	phase
8	50	50	O	,
9	52	54	O	the
10	56	63	O	terminal
11	65	68	O	half
12	70	73	O	life
13	75	76	O	of
14	78	86	O	XXXXXXXX
15	88	89	O	is
16	91	103	O	approximately
17	105	106	O	24
18	108	112	O	hours
NULL

SAPHRIS	5969	34090-1	5429f134-839f-4ffc-9944-55f51238def8	14
Steady-state concentrations of asenapine are reached within 3 days of twice daily dosing.
1	0	5	O	Steady
2	7	11	O	state
3	13	26	O	concentrations
4	28	29	O	of
5	31	39	O	XXXXXXXX
6	41	43	O	are
7	45	51	O	reached
8	53	58	O	within
9	60	60	O	3
10	62	65	O	days
11	67	68	O	of
12	70	74	O	twice
13	76	80	O	daily
14	82	87	O	dosing
NULL

SAPHRIS	5970	34090-1	5429f134-839f-4ffc-9944-55f51238def8	30
After administration of a single dose of [14C]-labeled asenapine, about 90% of the dose was recovered; approximately 50% was recovered in urine, and 40% recovered in feces.
1	0	4	O	After
2	6	19	O	administration
3	21	22	O	of
4	24	24	O	a
5	26	31	O	single
6	33	36	O	dose
7	38	39	O	of
8	42	44	O	14C
9	47	53	O	labeled
10	55	63	O	XXXXXXXX
11	64	64	O	,
12	66	70	O	about
13	72	74	O	90%
14	76	77	O	of
15	79	81	O	the
16	83	86	O	dose
17	88	90	O	was
18	92	100	O	recovered
19	103	115	O	approximately
20	117	119	O	50%
21	121	123	O	was
22	125	133	O	recovered
23	135	136	O	in
24	138	142	O	urine
25	143	143	O	,
26	145	147	O	and
27	149	151	O	40%
28	153	161	O	recovered
29	163	164	O	in
30	166	170	O	feces
NULL

SAPHRIS	5971	34090-1	5429f134-839f-4ffc-9944-55f51238def8	11
About 50% of the circulating species in plasma have been identified.
1	0	4	O	About
2	6	8	O	50%
3	10	11	O	of
4	13	15	O	the
5	17	27	O	circulating
6	29	35	O	species
7	37	38	O	in
8	40	45	O	plasma
9	47	50	O	have
10	52	55	O	been
11	57	66	O	identified
NULL

SAPHRIS	5972	34090-1	5429f134-839f-4ffc-9944-55f51238def8	26
The predominant species was asenapine N+-glucuronide; others included N-desmethylasenapine, N-desmethylasenapine N-carbamoyl glucuronide, and unchanged asenapine in smaller amounts.
1	0	2	O	The
2	4	14	O	predominant
3	16	22	O	species
4	24	26	O	was
5	28	36	O	XXXXXXXX
6	38	39	O	N+
7	41	51	O	glucuronide
8	54	59	O	others
9	61	68	O	included
10	70	70	O	N
11	72	80	O	desmethyl
12	81	89	O	XXXXXXXX
13	90	90	O	,
14	92	92	O	N
15	94	102	O	desmethyl
16	103	111	O	XXXXXXXX
17	113	113	O	N
18	115	123	O	carbamoyl
19	125	135	O	glucuronide
20	136	136	O	,
21	138	140	O	and
22	142	150	O	unchanged
23	152	160	O	XXXXXXXX
24	162	163	O	in
25	165	171	O	smaller
26	173	179	O	amounts
NULL

SAPHRIS	5973	34090-1	5429f134-839f-4ffc-9944-55f51238def8	9
SAPHRIS activity is primarily due to the parent drug.
1	0	6	O	XXXXXXXX
2	8	15	O	activity
3	17	18	O	is
4	20	28	O	primarily
5	30	32	O	due
6	34	35	O	to
7	37	39	O	the
8	41	46	O	parent
9	48	51	O	drug
NULL

SAPHRIS	5974	34090-1	5429f134-839f-4ffc-9944-55f51238def8	21
In vitro studies indicate that asenapine is a substrate for UGT1A4, CYP1A2 and to a lesser extent CYP3A4 and CYP2D6.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	24	O	indicate
5	26	29	O	that
6	31	39	O	XXXXXXXX
7	41	42	O	is
8	44	44	O	a
9	46	54	O	substrate
10	56	58	O	for
11	60	65	O	UGT1A4
12	66	66	O	,
13	68	73	O	CYP1A2
14	75	77	O	and
15	79	80	O	to
16	82	82	O	a
17	84	89	O	lesser
18	91	96	O	extent
19	98	103	O	CYP3A4
20	105	107	O	and
21	109	114	O	CYP2D6
NULL

SAPHRIS	5975	34090-1	5429f134-839f-4ffc-9944-55f51238def8	14
Asenapine does not cause induction of CYP1A2 or CYP3A4 activities in cultured human hepatocytes.
1	0	8	O	XXXXXXXX
2	10	13	O	does
3	15	17	O	not
4	19	23	O	cause
5	25	33	O	induction
6	35	36	O	of
7	38	43	O	CYP1A2
8	45	46	O	or
9	48	53	O	CYP3A4
10	55	64	O	activities
11	66	67	O	in
12	69	76	O	cultured
13	78	82	O	human
14	84	94	O	hepatocytes
NULL

SAPHRIS	5976	34090-1	5429f134-839f-4ffc-9944-55f51238def8	25
Coadministration of asenapine with known inhibitors, inducers or substrates of these metabolic pathways has been studied in a number of drug-drug interaction studies.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	28	O	XXXXXXXX
4	30	33	O	with
5	35	39	O	known
6	41	50	O	inhibitors
7	51	51	O	,
8	53	60	O	inducers
9	62	63	O	or
10	65	74	O	substrates
11	76	77	O	of
12	79	83	O	these
13	85	93	O	metabolic
14	95	102	O	pathways
15	104	106	O	has
16	108	111	O	been
17	113	119	O	studied
18	121	122	O	in
19	124	124	O	a
20	126	131	O	number
21	133	134	O	of
22	136	139	O	drug
23	141	144	O	drug
24	146	156	O	interaction
25	158	164	O	studies
NULL

SAPHRIS	5977	34090-1	5429f134-839f-4ffc-9944-55f51238def8	30
Food: A crossover study in 26 healthy adult male subjects was performed to evaluate the effect of food on the pharmacokinetics of a single 5 mg dose of asenapine.
1	0	3	O	Food
2	4	4	O	:
3	6	6	O	A
4	8	16	O	crossover
5	18	22	O	study
6	24	25	O	in
7	27	28	O	26
8	30	36	O	healthy
9	38	42	O	adult
10	44	47	O	male
11	49	56	O	subjects
12	58	60	O	was
13	62	70	O	performed
14	72	73	O	to
15	75	82	O	evaluate
16	84	86	O	the
17	88	93	O	effect
18	95	96	O	of
19	98	101	O	food
20	103	104	O	on
21	106	108	O	the
22	110	125	O	pharmacokinetics
23	127	128	O	of
24	130	130	O	a
25	132	137	O	single
26	139	139	O	5
27	141	142	O	mg
28	144	147	O	dose
29	149	150	O	of
30	152	160	O	XXXXXXXX
NULL

SAPHRIS	5978	34090-1	5429f134-839f-4ffc-9944-55f51238def8	27
Consumption of food immediately prior to sublingual administration decreased asenapine exposure by 20%; consumption of food 4 hours after sublingual administration decreased asenapine exposure by about 10%.
1	0	10	O	Consumption
2	12	13	O	of
3	15	18	O	food
4	20	30	O	immediately
5	32	36	O	prior
6	38	39	O	to
7	41	50	O	sublingual
8	52	65	O	administration
9	67	75	O	decreased
10	77	85	O	XXXXXXXX
11	87	94	O	exposure
12	96	97	O	by
13	99	101	O	20%
14	104	114	O	consumption
15	116	117	O	of
16	119	122	O	food
17	124	124	O	4
18	126	130	O	hours
19	132	136	O	after
20	138	147	O	sublingual
21	149	162	O	administration
22	164	172	B-T	decreased
23	174	182	I-T	XXXXXXXX
24	184	191	I-T	exposure
25	193	194	O	by
26	196	200	O	about
27	202	204	O	10%
NULL

SAPHRIS	5979	34090-1	5429f134-839f-4ffc-9944-55f51238def8	10
These effects are probably due to increased hepatic blood flow.
1	0	4	B-E	These
2	6	12	O	effects
3	14	16	O	are
4	18	25	O	probably
5	27	29	O	due
6	31	32	O	to
7	34	42	B-E	increased
8	44	50	I-E	hepatic
9	52	56	I-E	blood
10	58	61	I-E	flow
NULL

SAPHRIS	5980	34090-1	5429f134-839f-4ffc-9944-55f51238def8	23
In clinical trials establishing the efficacy and safety of SAPHRIS, patients were instructed to avoid eating for 10 minutes following sublingual dosing.
1	0	1	O	In
2	3	10	O	clinical
3	12	17	O	trials
4	19	30	O	establishing
5	32	34	O	the
6	36	43	O	efficacy
7	45	47	O	and
8	49	54	O	safety
9	56	57	O	of
10	59	65	O	XXXXXXXX
11	66	66	O	,
12	68	75	O	patients
13	77	80	O	were
14	82	91	O	instructed
15	93	94	O	to
16	96	100	O	avoid
17	102	107	O	eating
18	109	111	O	for
19	113	114	O	10
20	116	122	O	minutes
21	124	132	O	following
22	134	143	O	sublingual
23	145	150	O	dosing
NULL

SAPHRIS	5981	34090-1	5429f134-839f-4ffc-9944-55f51238def8	15
There were no other restrictions with regard to the timing of meals in these trials.
1	0	4	O	There
2	6	9	O	were
3	11	12	O	no
4	14	18	O	other
5	20	31	O	restrictions
6	33	36	O	with
7	38	43	O	regard
8	45	46	O	to
9	48	50	O	the
10	52	57	O	timing
11	59	60	O	of
12	62	66	O	meals
13	68	69	O	in
14	71	75	O	these
15	77	82	O	trials
NULL

SAPHRIS	5982	34090-1	5429f134-839f-4ffc-9944-55f51238def8	25
Water: In clinical trials establishing the efficacy and safety of SAPHRIS, patients were instructed to avoid drinking for 10 minutes following sublingual dosing.
1	0	4	O	Water
2	5	5	O	:
3	7	8	O	In
4	10	17	O	clinical
5	19	24	O	trials
6	26	37	O	establishing
7	39	41	O	the
8	43	50	O	efficacy
9	52	54	O	and
10	56	61	O	safety
11	63	64	O	of
12	66	72	O	XXXXXXXX
13	73	73	O	,
14	75	82	O	patients
15	84	87	O	were
16	89	98	O	instructed
17	100	101	O	to
18	103	107	O	avoid
19	109	116	O	drinking
20	118	120	O	for
21	122	123	O	10
22	125	131	O	minutes
23	133	141	O	following
24	143	152	O	sublingual
25	154	159	O	dosing
NULL

SAPHRIS	5983	34090-1	5429f134-839f-4ffc-9944-55f51238def8	33
The effect of water administration following 10 mg sublingual SAPHRIS dosing was studied at different time points of 2, 5, 10, and 30 minutes in 15 healthy adult male subjects.
1	0	2	O	The
2	4	9	O	effect
3	11	12	O	of
4	14	18	O	water
5	20	33	O	administration
6	35	43	O	following
7	45	46	O	10
8	48	49	O	mg
9	51	60	O	sublingual
10	62	68	O	XXXXXXXX
11	70	75	O	dosing
12	77	79	O	was
13	81	87	O	studied
14	89	90	O	at
15	92	100	O	different
16	102	105	O	time
17	107	112	O	points
18	114	115	O	of
19	117	117	O	2
20	118	118	O	,
21	120	120	O	5
22	121	121	O	,
23	123	124	O	10
24	125	125	O	,
25	127	129	O	and
26	131	132	O	30
27	134	140	O	minutes
28	142	143	O	in
29	145	146	O	15
30	148	154	O	healthy
31	156	160	O	adult
32	162	165	O	male
33	167	174	O	subjects
NULL

SAPHRIS	5984	34090-1	5429f134-839f-4ffc-9944-55f51238def8	25
The exposure of asenapine following administration of water 10 minutes after sublingual dosing was equivalent to that when water was administered 30 minutes after dosing.
1	0	2	O	The
2	4	11	O	exposure
3	13	14	O	of
4	16	24	O	XXXXXXXX
5	26	34	O	following
6	36	49	O	administration
7	51	52	O	of
8	54	58	O	water
9	60	61	O	10
10	63	69	O	minutes
11	71	75	O	after
12	77	86	O	sublingual
13	88	93	O	dosing
14	95	97	O	was
15	99	108	O	equivalent
16	110	111	O	to
17	113	116	O	that
18	118	121	O	when
19	123	127	O	water
20	129	131	O	was
21	133	144	O	administered
22	146	147	O	30
23	149	155	O	minutes
24	157	161	O	after
25	163	168	O	dosing
NULL

SAPHRIS	5985	34090-1	5429f134-839f-4ffc-9944-55f51238def8	19
Reduced exposure to asenapine was observed following water administration at 2 minutes (19% decrease) and 5 minutes (10% decrease).
1	0	6	B-T	Reduced
2	8	15	I-T	exposure
3	17	18	O	to
4	20	28	O	XXXXXXXX
5	30	32	O	was
6	34	41	O	observed
7	43	51	O	following
8	53	57	O	water
9	59	72	O	administration
10	74	75	O	at
11	77	77	O	2
12	79	85	O	minutes
13	88	90	O	19%
14	92	99	O	decrease
15	102	104	O	and
16	106	106	O	5
17	108	114	O	minutes
18	117	119	O	10%
19	121	128	O	decrease
NULL

SAPHRIS	5986	34090-1	5429f134-839f-4ffc-9944-55f51238def8	18
Drug Interaction Studies: Effects of other drugs on the exposure of asenapine are summarized in Figure 1.
1	0	3	O	Drug
2	5	15	O	Interaction
3	17	23	O	Studies
4	24	24	O	:
5	26	32	O	Effects
6	34	35	O	of
7	37	41	O	other
8	43	47	O	drugs
9	49	50	O	on
10	52	54	O	the
11	56	63	O	exposure
12	65	66	O	of
13	68	76	O	XXXXXXXX
14	78	80	O	are
15	82	91	O	summarized
16	93	94	O	in
17	96	101	O	Figure
18	103	103	O	1
NULL

SAPHRIS	5987	34090-1	5429f134-839f-4ffc-9944-55f51238def8	22
In addition, a population pharmacokinetic analysis indicated that the concomitant administration of lithium had no effect on the pharmacokinetics of asenapine.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	13	O	a
5	15	24	O	population
6	26	40	O	pharmacokinetic
7	42	49	O	analysis
8	51	59	O	indicated
9	61	64	O	that
10	66	68	O	the
11	70	80	O	concomitant
12	82	95	O	administration
13	97	98	O	of
14	100	106	O	lithium
15	108	110	O	had
16	112	113	O	no
17	115	120	O	effect
18	122	123	O	on
19	125	127	O	the
20	129	144	O	pharmacokinetics
21	146	147	O	of
22	149	157	O	XXXXXXXX
NULL

SAPHRIS	5988	34090-1	5429f134-839f-4ffc-9944-55f51238def8	27
Figure 1: Effect of Other Drugs on Asenapine Pharmacokinetics The effects of asenapine on the pharmacokinetics of other co-administered drugs are summarized in Figure 2.
1	0	5	O	Figure
2	7	7	O	1
3	8	8	O	:
4	10	15	O	Effect
5	17	18	O	of
6	20	24	O	Other
7	26	30	O	Drugs
8	32	33	O	on
9	35	43	O	XXXXXXXX
10	45	60	O	Pharmacokinetics
11	62	64	O	The
12	66	72	O	effects
13	74	75	O	of
14	77	85	O	XXXXXXXX
15	87	88	O	on
16	90	92	O	the
17	94	109	O	pharmacokinetics
18	111	112	O	of
19	114	118	O	other
20	120	121	O	co
21	123	134	O	administered
22	136	140	O	drugs
23	142	144	O	are
24	146	155	O	summarized
25	157	158	O	in
26	160	165	O	Figure
27	167	167	O	2
NULL

SAPHRIS	5989	34090-1	5429f134-839f-4ffc-9944-55f51238def8	20
Coadministration of paroxetine with SAPHRIS caused a two-fold increase in the maximum plasma concentrations and systemic exposure of paroxetine.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	29	O	paroxetine
4	31	34	O	with
5	36	42	O	XXXXXXXX
6	44	49	O	caused
7	51	51	O	a
8	53	55	O	two
9	57	60	O	fold
10	62	69	O	increase
11	71	72	O	in
12	74	76	O	the
13	78	84	O	maximum
14	86	91	O	plasma
15	93	106	O	concentrations
16	108	110	O	and
17	112	119	O	systemic
18	121	128	O	exposure
19	130	131	O	of
20	133	142	O	paroxetine
NULL

SAPHRIS	5990	34090-1	5429f134-839f-4ffc-9944-55f51238def8	13
Asenapine enhances the inhibitory effects of paroxetine on its own metabolism by CYP2D6.
1	0	8	O	XXXXXXXX
2	10	17	O	enhances
3	19	21	O	the
4	23	32	O	inhibitory
5	34	40	O	effects
6	42	43	O	of
7	45	54	O	paroxetine
8	56	57	O	on
9	59	61	O	its
10	63	65	O	own
11	67	76	O	metabolism
12	78	79	O	by
13	81	86	B-D	CYP2D6
NULL

SAPHRIS	5991	34090-1	5429f134-839f-4ffc-9944-55f51238def8	30
Figure 2: Effect of Asenapine on Other Drug Pharmacokinetics Figure 1 Figure 2 Studies in Special Populations: Exposures of asenapine in special populations are summarized in Figure 3.
1	0	5	O	Figure
2	7	7	O	2
3	8	8	O	:
4	10	15	O	Effect
5	17	18	O	of
6	20	28	O	XXXXXXXX
7	30	31	O	on
8	33	37	O	Other
9	39	42	O	Drug
10	44	59	O	Pharmacokinetics
11	61	66	O	Figure
12	68	68	O	1
13	70	75	O	Figure
14	77	77	O	2
15	79	85	O	Studies
16	87	88	O	in
17	90	96	O	Special
18	98	108	O	Populations
19	109	109	O	:
20	111	119	O	Exposures
21	121	122	O	of
22	124	132	O	XXXXXXXX
23	134	135	O	in
24	137	143	O	special
25	145	155	O	populations
26	157	159	O	are
27	161	170	O	summarized
28	172	173	O	in
29	175	180	O	Figure
30	182	182	O	3
NULL

SAPHRIS	5992	34090-1	5429f134-839f-4ffc-9944-55f51238def8	25
Additionally, based on population pharmacokinetic analysis, no effects of sex, race, BMI, and smoking status on asenapine exposure were observed.
1	0	11	O	Additionally
2	12	12	O	,
3	14	18	O	based
4	20	21	O	on
5	23	32	O	population
6	34	48	O	pharmacokinetic
7	50	57	O	analysis
8	58	58	O	,
9	60	61	O	no
10	63	69	O	effects
11	71	72	O	of
12	74	76	O	sex
13	77	77	O	,
14	79	82	O	race
15	83	83	O	,
16	85	87	O	BMI
17	88	88	O	,
18	90	92	O	and
19	94	100	O	smoking
20	102	107	O	status
21	109	110	O	on
22	112	120	O	XXXXXXXX
23	122	129	O	exposure
24	131	134	O	were
25	136	143	O	observed
NULL

SAPHRIS	5993	34090-1	5429f134-839f-4ffc-9944-55f51238def8	12
Exposure in elderly patients is 30-40% higher as compared to adults.
1	0	7	O	Exposure
2	9	10	O	in
3	12	18	O	elderly
4	20	27	O	patients
5	29	30	O	is
6	32	33	O	30
7	35	37	O	40%
8	39	44	O	higher
9	46	47	O	as
10	49	56	O	compared
11	58	59	O	to
12	61	66	O	adults
NULL

SAPHRIS	5994	34090-1	5429f134-839f-4ffc-9944-55f51238def8	12
Figure 3: Effect of Intrinsic Factors on Asenapine Pharmacokinetics Figure 3
1	0	5	O	Figure
2	7	7	O	3
3	8	8	O	:
4	10	15	O	Effect
5	17	18	O	of
6	20	28	O	Intrinsic
7	30	36	O	Factors
8	38	39	O	on
9	41	49	O	XXXXXXXX
10	51	66	O	Pharmacokinetics
11	68	73	O	Figure
12	75	75	O	3
NULL

