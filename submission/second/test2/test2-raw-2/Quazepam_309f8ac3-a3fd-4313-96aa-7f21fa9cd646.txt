Quazepam	1	34073-7	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	26
Benzodiazepines, including QUAZEPAM, produce additive CNS depressant effects when co-administered with ethanol or other CNS depressants (e.g. psychotropic medications, anticonvulsants, antihistamines).
1	0	14	B-D	Benzodiazepines
2	15	15	O	,
3	17	25	O	including
4	27	34	O	XXXXXXXX
5	35	35	O	,
6	37	43	O	produce
7	45	52	B-E	additive
8	54	56	I-E	CNS
9	58	67	I-E	depressant
10	69	75	I-E	effects
11	77	80	O	when
12	82	83	O	co
13	85	96	O	administered
14	98	101	O	with
15	103	109	B-D	ethanol
16	111	112	O	or
17	114	118	O	other
18	120	122	B-D	CNS
19	124	134	I-D	depressants
20	137	139	O	e.g
21	142	153	B-D	psychotropic
22	155	165	I-D	medications
23	166	166	O	,
24	168	182	O	anticonvulsants
25	183	183	O	,
26	185	198	O	antihistamines
D/1:7:1 D/15:7:1 D/18:7:1 D/21:7:1

Quazepam	2	34073-7	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	18
Downward dose adjustment of QUAZEPAM and/or concomitant CNS depressants may be necessary because of additive effects.
1	0	7	B-T	Downward
2	9	12	I-T	dose
3	14	23	I-T	adjustment
4	25	26	O	of
5	28	35	O	XXXXXXXX
6	37	39	O	and
7	40	40	O	/
8	41	42	O	or
9	44	54	O	concomitant
10	56	58	B-U	CNS
11	60	70	I-U	depressants
12	72	74	O	may
13	76	77	O	be
14	79	87	O	necessary
15	89	95	O	because
16	97	98	O	of
17	100	107	B-E	additive
18	109	115	I-E	effects
NULL

Quazepam	3	34073-7	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	14
CNS Depressants: downward dose adjustment may be necessary due to additive effects (7)
1	0	2	B-D	CNS
2	4	14	I-D	Depressants
3	15	15	O	:
4	17	24	O	downward
5	26	29	B-T	dose
6	31	40	I-T	adjustment
7	42	44	O	may
8	46	47	O	be
9	49	57	O	necessary
10	59	61	O	due
11	63	64	O	to
12	66	73	B-E	additive
13	75	81	I-E	effects
14	84	84	O	7
D/1:12:1

Quazepam	4	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	33
Quazepam, like other central nervous system agents of the 1,4-benzodiazepine class, presumably exerts its effects by binding to stereo-specific receptors at several sites within the central nervous system (CNS).
1	0	7	O	XXXXXXXX
2	8	8	O	,
3	10	13	O	like
4	15	19	O	other
5	21	27	O	central
6	29	35	O	nervous
7	37	42	O	system
8	44	49	O	agents
9	51	52	O	of
10	54	56	O	the
11	58	60	O	1,4
12	62	75	O	benzodiazepine
13	77	81	O	class
14	82	82	O	,
15	84	93	O	presumably
16	95	100	O	exerts
17	102	104	O	its
18	106	112	O	effects
19	114	115	O	by
20	117	123	O	binding
21	125	126	O	to
22	128	133	O	stereo
23	135	142	O	specific
24	144	152	O	receptors
25	154	155	O	at
26	157	163	O	several
27	165	169	O	sites
28	171	176	O	within
29	178	180	O	the
30	182	188	O	central
31	190	196	O	nervous
32	198	203	O	system
33	206	208	O	CNS
NULL

Quazepam	5	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	7
The exact mechanism of action is unknown.
1	0	2	O	The
2	4	8	O	exact
3	10	18	O	mechanism
4	20	21	O	of
5	23	28	O	action
6	30	31	O	is
7	33	39	O	unknown
NULL

Quazepam	6	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	19
Absorption: Quazepam is rapidly (absorption half-life of about 30 minutes) and well absorbed from the gastrointestinal tract.
1	0	9	O	Absorption
2	10	10	O	:
3	12	19	O	XXXXXXXX
4	21	22	O	is
5	24	30	B-T	rapidly
6	33	42	I-T	absorption
7	44	47	I-T	half
8	49	52	I-T	life
9	54	55	O	of
10	57	61	O	about
11	63	64	O	30
12	66	72	O	minutes
13	75	77	O	and
14	79	82	O	well
15	84	91	O	absorbed
16	93	96	O	from
17	98	100	O	the
18	102	117	O	gastrointestinal
19	119	123	O	tract
NULL

Quazepam	7	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	23
The peak plasma concentration of quazepam is approximately 20 ng/mL after a 15 mg dose and occurs at about 2 hours.
1	0	2	O	The
2	4	7	O	peak
3	9	14	O	plasma
4	16	28	O	concentration
5	30	31	O	of
6	33	40	O	XXXXXXXX
7	42	43	O	is
8	45	57	O	approximately
9	59	60	O	20
10	62	63	O	ng
11	64	64	O	/
12	65	66	O	mL
13	68	72	O	after
14	74	74	O	a
15	76	77	O	15
16	79	80	O	mg
17	82	85	O	dose
18	87	89	O	and
19	91	96	O	occurs
20	98	99	O	at
21	101	105	O	about
22	107	107	O	2
23	109	113	O	hours
NULL

Quazepam	8	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	30
Metabolism: Quazepam, the active parent compound, is extensively metabolized in the liver; two of the plasma metabolites are 2-oxoquazepam and N-desalkyl-2-oxoquazepam.
1	0	9	O	Metabolism
2	10	10	O	:
3	12	19	O	XXXXXXXX
4	20	20	O	,
5	22	24	O	the
6	26	31	O	active
7	33	38	O	parent
8	40	47	O	compound
9	48	48	O	,
10	50	51	O	is
11	53	63	O	extensively
12	65	75	O	metabolized
13	77	78	O	in
14	80	82	O	the
15	84	88	O	liver
16	91	93	O	two
17	95	96	O	of
18	98	100	O	the
19	102	107	O	plasma
20	109	119	O	metabolites
21	121	123	O	are
22	125	125	O	2
23	127	129	O	oxo
24	130	137	O	XXXXXXXX
25	139	141	O	and
26	143	143	O	N
27	145	152	O	desalkyl
28	154	154	O	2
29	156	158	O	oxo
30	159	166	O	XXXXXXXX
NULL

Quazepam	9	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	7
All three compounds show CNS depressant activity.
1	0	2	O	All
2	4	8	O	three
3	10	18	O	compounds
4	20	23	O	show
5	25	27	O	CNS
6	29	38	O	depressant
7	40	47	B-E	activity
NULL

Quazepam	10	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	19
Distribution: The degree of plasma protein binding for quazepam and its two major metabolites is greater than 95%.
1	0	11	O	Distribution
2	12	12	O	:
3	14	16	O	The
4	18	23	O	degree
5	25	26	O	of
6	28	33	O	plasma
7	35	41	O	protein
8	43	49	O	binding
9	51	53	O	for
10	55	62	O	XXXXXXXX
11	64	66	O	and
12	68	70	O	its
13	72	74	O	two
14	76	80	O	major
15	82	92	O	metabolites
16	94	95	O	is
17	97	103	O	greater
18	105	108	O	than
19	110	112	O	95%
NULL

Quazepam	11	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	35
Elimination: Following administration of 14C-quazepam, 31% of the dose appeared in the urine and 23% in the feces over five days; only trace amounts of unchanged drug were present in the urine.
1	0	10	O	Elimination
2	11	11	O	:
3	13	21	O	Following
4	23	36	O	administration
5	38	39	O	of
6	41	43	O	14C
7	45	52	O	XXXXXXXX
8	53	53	O	,
9	55	57	O	31%
10	59	60	O	of
11	62	64	O	the
12	66	69	O	dose
13	71	78	O	appeared
14	80	81	O	in
15	83	85	O	the
16	87	91	O	urine
17	93	95	O	and
18	97	99	O	23%
19	101	102	O	in
20	104	106	O	the
21	108	112	O	feces
22	114	117	O	over
23	119	122	O	five
24	124	127	O	days
25	130	133	O	only
26	135	139	O	trace
27	141	147	O	amounts
28	149	150	O	of
29	152	160	O	unchanged
30	162	165	O	drug
31	167	170	O	were
32	172	178	O	present
33	180	181	O	in
34	183	185	O	the
35	187	191	O	urine
NULL

Quazepam	12	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	25
The mean elimination half-life of quazepam and 2-oxoquazepam is 39 hours and that of N-desalkyl-2-oxoquazepam is 73 hours.
1	0	2	B-T	The
2	4	7	I-T	mean
3	9	19	I-T	elimination
4	21	24	I-T	half
5	26	29	I-T	life
6	31	32	O	of
7	34	41	O	XXXXXXXX
8	43	45	O	and
9	47	47	O	2
10	49	51	O	oxo
11	52	59	O	XXXXXXXX
12	61	62	O	is
13	64	65	O	39
14	67	71	O	hours
15	73	75	O	and
16	77	80	O	that
17	82	83	O	of
18	85	85	O	N
19	87	94	O	desalkyl
20	96	96	O	2
21	98	100	O	oxo
22	101	108	O	XXXXXXXX
23	110	111	O	is
24	113	114	O	73
25	116	120	O	hours
NULL

Quazepam	13	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	29
Steady-state levels of quazepam and 2-oxoquazepam are attained by the seventh daily dose and that of N-desalkyl-2-oxoquazepam by the thirteenth daily dose.
1	0	5	O	Steady
2	7	11	B-T	state
3	13	18	O	levels
4	20	21	O	of
5	23	30	O	XXXXXXXX
6	32	34	O	and
7	36	36	O	2
8	38	40	O	oxo
9	41	48	O	XXXXXXXX
10	50	52	O	are
11	54	61	O	attained
12	63	64	O	by
13	66	68	O	the
14	70	76	O	seventh
15	78	82	B-T	daily
16	84	87	I-T	dose
17	89	91	O	and
18	93	96	O	that
19	98	99	O	of
20	101	101	O	N
21	103	110	B-U	desalkyl
22	112	112	O	2
23	114	116	O	oxo
24	117	124	O	XXXXXXXX
25	126	127	O	by
26	129	131	O	the
27	133	142	O	thirteenth
28	144	148	O	daily
29	150	153	B-T	dose
NULL

Quazepam	14	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	52
Special Populations: Geriatrics: The pharmacokinetics of quazepam and 2-oxoquazepam in geriatric subjects are comparable to those seen in young adults; as with desalkyl metabolites of other benzodiazepines, the elimination half-life of N-desalkyl-2-oxoquazepam in geriatric patients is about twice that of young adults.
1	0	6	O	Special
2	8	18	O	Populations
3	19	19	O	:
4	21	30	O	Geriatrics
5	31	31	O	:
6	33	35	O	The
7	37	52	O	pharmacokinetics
8	54	55	O	of
9	57	64	O	XXXXXXXX
10	66	68	O	and
11	70	70	O	2
12	72	74	O	oxo
13	75	82	O	XXXXXXXX
14	84	85	O	in
15	87	95	O	geriatric
16	97	104	O	subjects
17	106	108	O	are
18	110	119	O	comparable
19	121	122	O	to
20	124	128	O	those
21	130	133	O	seen
22	135	136	O	in
23	138	142	O	young
24	144	149	O	adults
25	152	153	O	as
26	155	158	O	with
27	160	167	B-K	desalkyl
28	169	179	I-K	metabolites
29	181	182	O	of
30	184	188	O	other
31	190	204	B-K	benzodiazepines
32	205	205	O	,
33	207	209	O	the
34	211	221	B-T	elimination
35	223	226	I-T	half
36	228	231	I-T	life
37	233	234	O	of
38	236	236	O	N
39	238	245	O	desalkyl
40	247	247	O	2
41	249	251	O	oxo
42	252	259	O	XXXXXXXX
43	261	262	O	in
44	264	272	O	geriatric
45	274	281	O	patients
46	283	284	O	is
47	286	290	O	about
48	292	296	O	twice
49	298	301	O	that
50	303	304	O	of
51	306	310	O	young
52	312	317	O	adults
K/27:C54611 K/31:C54611

Quazepam	15	34090-1	309f8ac3-a3fd-4313-96aa-7f21fa9cd646	37
Bupropion (a CYP2B6 substrate): Co-administration of a single dose of 150 mg Bupropion Hydrochloride XL with steady state quazepam did not significantly affect the AUC and Cmax of bupropion or its primary metabolite, hydroxybupropion.
1	0	8	O	Bupropion
2	11	11	O	a
3	13	18	O	CYP2B6
4	20	28	O	substrate
5	30	30	O	:
6	32	33	O	Co
7	35	48	O	administration
8	50	51	O	of
9	53	53	O	a
10	55	60	O	single
11	62	65	O	dose
12	67	68	O	of
13	70	72	O	150
14	74	75	O	mg
15	77	85	O	Bupropion
16	87	99	O	Hydrochloride
17	101	102	O	XL
18	104	107	O	with
19	109	114	O	steady
20	116	120	O	state
21	122	129	O	XXXXXXXX
22	131	133	O	did
23	135	137	O	not
24	139	151	O	significantly
25	153	158	O	affect
26	160	162	O	the
27	164	166	O	AUC
28	168	170	O	and
29	172	175	O	Cmax
30	177	178	O	of
31	180	188	O	bupropion
32	190	191	O	or
33	193	195	O	its
34	197	203	O	primary
35	205	214	O	metabolite
36	215	215	O	,
37	217	232	O	hydroxybupropion
NULL

