Venlafaxine Hydrochloride	5770	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	28
Serotonergic Drugs (e.g., MAOIs, triptans, SSRIs, other SNRIs, linezolid, lithium, tramadol, or St. John's wort): Potential for serotonin syndrome.
1	0	11	O	Serotonergic
2	13	17	O	Drugs
3	20	22	O	e.g
4	24	24	O	,
5	26	30	O	MAOIs
6	31	31	O	,
7	33	40	O	triptans
8	41	41	O	,
9	43	47	O	SSRIs
10	48	48	O	,
11	50	54	O	other
12	56	60	O	SNRIs
13	61	61	O	,
14	63	71	O	linezolid
15	72	72	O	,
16	74	80	O	lithium
17	81	81	O	,
18	83	90	O	tramadol
19	91	91	O	,
20	93	94	O	or
21	96	97	O	St
22	100	105	O	John's
23	107	110	O	wort
24	112	112	O	:
25	114	122	O	Potential
26	124	126	O	for
27	128	136	O	serotonin
28	138	145	O	syndrome
NULL

Venlafaxine Hydrochloride	5771	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	10
Careful patient observation is advised (4.2, 5.2, 7.3).
1	0	6	O	Careful
2	8	14	O	patient
3	16	26	B-T	observation
4	28	29	O	is
5	31	37	O	advised
6	40	42	O	4.2
7	43	43	O	,
8	45	47	O	5.2
9	48	48	O	,
10	50	52	O	7.3
NULL

Venlafaxine Hydrochloride	5772	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	17
The risk of using venlafaxine in combination with other CNS-active drugs has not been systematically evaluated.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	16	O	using
5	18	28	O	XXXXXXXX
6	30	31	O	in
7	33	43	O	combination
8	45	48	O	with
9	50	54	O	other
10	56	58	O	CNS
11	60	65	O	active
12	67	71	O	drugs
13	73	75	O	has
14	77	79	O	not
15	81	84	O	been
16	86	99	O	systematically
17	101	109	O	evaluated
NULL

Venlafaxine Hydrochloride	5773	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	18
Consequently, caution is advised when venlafaxine hydrochloride extended-release is taken in combination with other CNS-active drugs.
1	0	11	O	Consequently
2	12	12	O	,
3	14	20	B-T	caution
4	22	23	O	is
5	25	31	O	advised
6	33	36	O	when
7	38	62	O	XXXXXXXX
8	64	71	O	extended
9	73	79	O	release
10	81	82	O	is
11	84	88	O	taken
12	90	91	O	in
13	93	103	O	combination
14	105	108	O	with
15	110	114	O	other
16	116	118	O	CNS
17	120	125	O	active
18	127	131	O	drugs
NULL

Venlafaxine Hydrochloride	5774	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	54
Adverse reactions, some of which were serious, have been reported in patients who have recently been discontinued from an MAOI and started on antidepressants with pharmacological properties similar to venlafaxine hydrochloride extended-release (SNRIs or SSRIs), or who have recently had SNRI or SSRI therapy discontinued prior to initiation of an MAOI.
1	0	6	O	Adverse
2	8	16	O	reactions
3	17	17	O	,
4	19	22	O	some
5	24	25	O	of
6	27	31	O	which
7	33	36	O	were
8	38	44	O	serious
9	45	45	O	,
10	47	50	O	have
11	52	55	O	been
12	57	64	O	reported
13	66	67	O	in
14	69	76	O	patients
15	78	80	O	who
16	82	85	O	have
17	87	94	O	recently
18	96	99	O	been
19	101	112	O	discontinued
20	114	117	O	from
21	119	120	O	an
22	122	125	O	MAOI
23	127	129	O	and
24	131	137	O	started
25	139	140	O	on
26	142	156	O	GGGGGGGG
27	158	161	O	with
28	163	177	O	pharmacological
29	179	188	O	properties
30	190	196	O	similar
31	198	199	O	to
32	201	225	O	XXXXXXXX
33	227	234	O	extended
34	236	242	O	release
35	245	249	O	SNRIs
36	251	252	O	or
37	254	258	O	SSRIs
38	260	260	O	,
39	262	263	O	or
40	265	267	O	who
41	269	272	O	have
42	274	281	O	recently
43	283	285	O	had
44	287	290	O	SNRI
45	292	293	O	or
46	295	298	O	SSRI
47	300	306	O	therapy
48	308	319	O	discontinued
49	321	325	O	prior
50	327	328	O	to
51	330	339	O	initiation
52	341	342	O	of
53	344	345	O	an
54	347	350	O	MAOI
NULL

Venlafaxine Hydrochloride	5775	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	65
Based on the mechanism of action of venlafaxine hydrochloride extended-release and the potential for serotonin syndrome, caution is advised when venlafaxine hydrochloride extended-release is coadministered with other drugs that may affect the serotonergic neurotransmitter systems, such as triptans, SSRIs, other SNRIs, linezolid (an antibiotic which is a reversible non-selective MAOI), lithium, tramadol, or St. John's wort.
1	0	4	O	Based
2	6	7	O	on
3	9	11	O	the
4	13	21	O	mechanism
5	23	24	O	of
6	26	31	O	action
7	33	34	O	of
8	36	60	O	XXXXXXXX
9	62	69	O	extended
10	71	77	O	release
11	79	81	O	and
12	83	85	O	the
13	87	95	O	potential
14	97	99	O	for
15	101	109	O	serotonin
16	111	118	O	syndrome
17	119	119	O	,
18	121	127	O	caution
19	129	130	O	is
20	132	138	O	advised
21	140	143	O	when
22	145	169	O	XXXXXXXX
23	171	178	O	extended
24	180	186	O	release
25	188	189	O	is
26	191	204	O	coadministered
27	206	209	O	with
28	211	215	O	other
29	217	221	O	drugs
30	223	226	B-D	that
31	228	230	I-D	may
32	232	237	I-D	affect
33	239	241	I-D	the
34	243	254	I-D	serotonergic
35	256	271	I-D	neurotransmitter
36	273	279	I-D	systems
37	280	280	O	,
38	282	285	O	such
39	287	288	O	as
40	290	297	O	triptans
41	298	298	O	,
42	300	304	O	SSRIs
43	305	305	O	,
44	307	311	O	other
45	313	317	O	SNRIs
46	318	318	O	,
47	320	328	O	linezolid
48	331	332	O	an
49	334	343	O	antibiotic
50	345	349	O	which
51	351	352	O	is
52	354	354	O	a
53	356	365	O	reversible
54	367	369	O	non
55	371	379	O	selective
56	381	384	O	MAOI
57	386	386	O	,
58	388	394	O	lithium
59	395	395	O	,
60	397	404	O	tramadol
61	405	405	O	,
62	407	408	O	or
63	410	411	O	St
64	414	419	O	John's
65	421	424	O	wort
NULL

Venlafaxine Hydrochloride	5776	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	29
If concomitant treatment with venlafaxine hydrochloride extended-release and these drugs is clinically warranted, careful observation of the patient is advised, particularly during treatment initiation and dose increases.
1	0	1	O	If
2	3	13	O	concomitant
3	15	23	O	treatment
4	25	28	O	with
5	30	54	O	XXXXXXXX
6	56	63	O	extended
7	65	71	O	release
8	73	75	O	and
9	77	81	O	these
10	83	87	O	drugs
11	89	90	O	is
12	92	101	O	clinically
13	103	111	O	warranted
14	112	112	O	,
15	114	120	O	careful
16	122	132	O	observation
17	134	135	O	of
18	137	139	O	the
19	141	147	O	patient
20	149	150	O	is
21	152	158	O	advised
22	159	159	O	,
23	161	172	O	particularly
24	174	179	O	during
25	181	189	O	treatment
26	191	200	O	initiation
27	202	204	O	and
28	206	209	O	dose
29	211	219	O	increases
NULL

Venlafaxine Hydrochloride	5777	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	13
The concomitant use of venlafaxine hydrochloride extended-release with tryptophan supplements is not recommended.
1	0	2	O	The
2	4	14	O	concomitant
3	16	18	O	use
4	20	21	O	of
5	23	47	O	XXXXXXXX
6	49	56	O	extended
7	58	64	O	release
8	66	69	O	with
9	71	80	B-U	tryptophan
10	82	92	I-U	supplements
11	94	95	O	is
12	97	99	B-T	not
13	101	111	I-T	recommended
NULL

Venlafaxine Hydrochloride	5778	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	10
Serotonin release by platelets plays an important role in hemostasis.
1	0	8	O	Serotonin
2	10	16	O	release
3	18	19	O	by
4	21	29	O	platelets
5	31	35	O	plays
6	37	38	O	an
7	40	48	O	important
8	50	53	O	role
9	55	56	O	in
10	58	67	O	hemostasis
NULL

Venlafaxine Hydrochloride	5779	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	33
The use of psychotropic drugs that interfere with serotonin reuptake is associated with the occurrence of upper gastrointestinal bleeding and concurrent use of an NSAID or aspirin may potentiate this risk of bleeding.
1	0	2	O	The
2	4	6	O	use
3	8	9	O	of
4	11	22	B-D	psychotropic
5	24	28	I-D	drugs
6	30	33	I-D	that
7	35	43	I-D	interfere
8	45	48	I-D	with
9	50	58	I-D	serotonin
10	60	67	I-D	reuptake
11	69	70	O	is
12	72	81	O	associated
13	83	86	O	with
14	88	90	O	the
15	92	101	O	occurrence
16	103	104	O	of
17	106	110	O	upper
18	112	127	B-E	gastrointestinal
19	129	136	I-E	bleeding
20	138	140	O	and
21	142	151	O	concurrent
22	153	155	O	use
23	157	158	O	of
24	160	161	O	an
25	163	167	B-D	NSAID
26	169	170	O	or
27	172	178	B-D	aspirin
28	180	182	O	may
29	184	193	O	potentiate
30	195	198	O	this
31	200	203	B-E	risk
32	205	206	I-E	of
33	208	215	B-E	bleeding
D/4:18:1 D/4:31:1 D/4:33:1 D/25:18:1 D/25:31:1 D/25:33:1 D/27:18:1 D/27:31:1 D/27:33:1

Venlafaxine Hydrochloride	5780	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	19
Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs and SNRIs are coadministered with warfarin.
1	0	6	O	Altered
2	8	20	O	anticoagulant
3	22	28	O	effects
4	29	29	O	,
5	31	39	O	including
6	41	49	B-E	increased
7	51	58	I-E	bleeding
8	59	59	O	,
9	61	64	O	have
10	66	69	O	been
11	71	78	O	reported
12	80	83	O	when
13	85	89	O	SSRIs
14	91	93	O	and
15	95	99	O	SNRIs
16	101	103	O	are
17	105	118	O	coadministered
18	120	123	O	with
19	125	132	B-D	warfarin
D/19:6:1

Venlafaxine Hydrochloride	5781	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	16
Patients receiving warfarin therapy should be carefully monitored when venlafaxine hydrochloride extended-release is initiated or discontinued.
1	0	7	O	Patients
2	9	17	O	receiving
3	19	26	O	warfarin
4	28	34	O	therapy
5	36	41	O	should
6	43	44	O	be
7	46	54	O	carefully
8	56	64	O	monitored
9	66	69	O	when
10	71	95	O	XXXXXXXX
11	97	104	O	extended
12	106	112	O	release
13	114	115	O	is
14	117	125	O	initiated
15	127	128	O	or
16	130	141	O	discontinued
NULL

Venlafaxine Hydrochloride	5782	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	21
The safety and efficacy of venlafaxine therapy in combination with weight loss agents, including phentermine, have not been established.
1	0	2	O	The
2	4	9	O	safety
3	11	13	O	and
4	15	22	O	efficacy
5	24	25	O	of
6	27	37	O	XXXXXXXX
7	39	45	O	therapy
8	47	48	O	in
9	50	60	O	combination
10	62	65	O	with
11	67	72	O	weight
12	74	77	O	loss
13	79	84	O	agents
14	85	85	O	,
15	87	95	O	including
16	97	107	O	phentermine
17	108	108	O	,
18	110	113	O	have
19	115	117	O	not
20	119	122	O	been
21	124	134	O	established
NULL

Venlafaxine Hydrochloride	5783	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	12
Coadministration of venlafaxine hydrochloride extended-release and weight loss agents is not recommended.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	44	O	XXXXXXXX
4	46	53	O	extended
5	55	61	O	release
6	63	65	O	and
7	67	72	O	weight
8	74	77	O	loss
9	79	84	O	agents
10	86	87	O	is
11	89	91	B-T	not
12	93	103	O	recommended
NULL

Venlafaxine Hydrochloride	5784	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	16
Venlafaxine hydrochloride extended-release is not indicated for weight loss alone or in combination with other products.
1	0	24	O	XXXXXXXX
2	26	33	O	extended
3	35	41	O	release
4	43	44	O	is
5	46	48	O	not
6	50	58	O	indicated
7	60	62	O	for
8	64	69	O	weight
9	71	74	O	loss
10	76	80	O	alone
11	82	83	O	or
12	85	86	O	in
13	88	98	O	combination
14	100	103	O	with
15	105	109	O	other
16	111	118	O	products
NULL

Venlafaxine Hydrochloride	5785	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	19
Figure 1: Effect of interacting drugs on the pharmacokinetics of venlafaxine and active metabolite O-desmethylvenlafaxine (ODV).
1	0	5	O	Figure
2	7	7	O	1
3	8	8	O	:
4	10	15	O	Effect
5	17	18	O	of
6	20	30	O	interacting
7	32	36	O	drugs
8	38	39	O	on
9	41	43	O	the
10	45	60	O	pharmacokinetics
11	62	63	O	of
12	65	75	O	XXXXXXXX
13	77	79	O	and
14	81	86	O	active
15	88	97	O	metabolite
16	99	99	O	O
17	101	109	O	desmethyl
18	110	120	O	XXXXXXXX
19	123	125	O	ODV
NULL

Venlafaxine Hydrochloride	5786	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	59
Abbreviations: ODV, O-desmethylvenlafaxine; AUC, area under the curve; Cmax, peak plasma concentrations; EM's, extensive metabolizers; PM's, poor metabolizers * No dose adjustment on co-administration with CYP2D6 inhibitors (Fig 3 and Metabolism Section 12.3) Figure 1 Figure 2: Effect of venlafaxine on the pharmacokinetics interacting drugs and their active metabolites.
1	0	12	O	Abbreviations
2	13	13	O	:
3	15	17	O	ODV
4	18	18	O	,
5	20	20	O	O
6	22	30	O	desmethyl
7	31	41	O	XXXXXXXX
8	44	46	O	AUC
9	47	47	O	,
10	49	52	O	area
11	54	58	O	under
12	60	62	O	the
13	64	68	O	curve
14	71	74	O	Cmax
15	75	75	O	,
16	77	80	O	peak
17	82	87	O	plasma
18	89	102	O	concentrations
19	105	108	O	EM's
20	109	109	O	,
21	111	119	O	extensive
22	121	132	O	metabolizers
23	135	138	O	PM's
24	139	139	O	,
25	141	144	O	poor
26	146	157	O	metabolizers
27	159	159	O	*
28	161	162	O	No
29	164	167	O	dose
30	169	178	O	adjustment
31	180	181	O	on
32	183	184	O	co
33	186	199	O	administration
34	201	204	O	with
35	206	211	O	CYP2D6
36	213	222	O	inhibitors
37	225	227	O	Fig
38	229	229	O	3
39	231	233	O	and
40	235	244	O	Metabolism
41	246	252	O	Section
42	254	257	O	12.3
43	260	265	O	Figure
44	267	267	O	1
45	269	274	O	Figure
46	276	276	O	2
47	277	277	O	:
48	279	284	O	Effect
49	286	287	O	of
50	289	299	O	XXXXXXXX
51	301	302	O	on
52	304	306	O	the
53	308	323	O	pharmacokinetics
54	325	335	O	interacting
55	337	341	O	drugs
56	343	345	O	and
57	347	351	O	their
58	353	358	O	active
59	360	370	O	metabolites
NULL

Venlafaxine Hydrochloride	5787	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	54
Abbreviations: AUC, area under the curve; Cmax, peak plasma concentrations; OH, hydroxyl * Data for 2-OH desipramine were not plotted to enhance clarity; the fold change and 90% CI for Cmax and AUC of 2-OH desipramine were 6.6 (5.5, 7.9) and 4.4 (3.8, 5.0), respectively.
1	0	12	O	Abbreviations
2	13	13	O	:
3	15	17	O	AUC
4	18	18	O	,
5	20	23	O	area
6	25	29	O	under
7	31	33	O	the
8	35	39	O	curve
9	42	45	O	Cmax
10	46	46	O	,
11	48	51	O	peak
12	53	58	O	plasma
13	60	73	O	concentrations
14	76	77	O	OH
15	78	78	O	,
16	80	87	O	hydroxyl
17	89	89	O	*
18	91	94	O	Data
19	96	98	O	for
20	100	100	O	2
21	102	103	O	OH
22	105	115	B-K	desipramine
23	117	120	O	were
24	122	124	O	not
25	126	132	O	plotted
26	134	135	O	to
27	137	143	O	enhance
28	145	151	O	clarity
29	154	156	O	the
30	158	161	O	fold
31	163	168	O	change
32	170	172	O	and
33	174	176	O	90%
34	178	179	O	CI
35	181	183	O	for
36	185	188	O	Cmax
37	190	192	O	and
38	194	196	O	AUC
39	198	199	O	of
40	201	201	O	2
41	203	204	O	OH
42	206	216	O	desipramine
43	218	221	O	were
44	223	225	O	6.6
45	228	230	O	5.5
46	231	231	O	,
47	233	235	O	7.9
48	238	240	O	and
49	242	244	O	4.4
50	247	249	O	3.8
51	250	250	O	,
52	252	254	O	5.0
53	256	256	O	,
54	258	269	O	respectively
K/22:C54610

Venlafaxine Hydrochloride	5788	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	32
Note: *: Administration of venlafaxine in a stable regimen did not exaggerate the psychomotor and psychometric effects induced by ethanol in these same subjects when they were not receiving venlafaxine.
1	0	3	O	Note
2	4	4	O	:
3	6	6	O	*
4	7	7	O	:
5	9	22	O	Administration
6	24	25	O	of
7	27	37	O	XXXXXXXX
8	39	40	O	in
9	42	42	O	a
10	44	49	O	stable
11	51	57	O	regimen
12	59	61	O	did
13	63	65	O	not
14	67	76	O	exaggerate
15	78	80	O	the
16	82	92	O	psychomotor
17	94	96	O	and
18	98	109	O	psychometric
19	111	117	O	effects
20	119	125	O	induced
21	127	128	O	by
22	130	136	O	ethanol
23	138	139	O	in
24	141	145	O	these
25	147	150	O	same
26	152	159	O	subjects
27	161	164	O	when
28	166	169	O	they
29	171	174	O	were
30	176	178	O	not
31	180	188	O	receiving
32	190	200	O	XXXXXXXX
NULL

Venlafaxine Hydrochloride	5789	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	20
Figure 2 False-positive urine immunoassay screening tests for phencyclidine (PCP) and amphetamine have been reported in patients taking venlafaxine.
1	0	5	O	Figure
2	7	7	O	2
3	9	13	O	False
4	15	22	O	positive
5	24	28	O	urine
6	30	40	O	immunoassay
7	42	50	O	screening
8	52	56	O	tests
9	58	60	O	for
10	62	74	O	phencyclidine
11	77	79	O	PCP
12	82	84	O	and
13	86	96	O	amphetamine
14	98	101	O	have
15	103	106	O	been
16	108	115	O	reported
17	117	118	O	in
18	120	127	O	patients
19	129	134	O	taking
20	136	146	O	XXXXXXXX
NULL

Venlafaxine Hydrochloride	5790	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	11
This is due to lack of specificity of the screening tests.
1	0	3	O	This
2	5	6	O	is
3	8	10	O	due
4	12	13	O	to
5	15	18	O	lack
6	20	21	O	of
7	23	33	O	specificity
8	35	36	O	of
9	38	40	O	the
10	42	50	O	screening
11	52	56	O	tests
NULL

Venlafaxine Hydrochloride	5791	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	15
False positive test results may be expected for several days following discontinuation of venlafaxine therapy.
1	0	4	O	False
2	6	13	O	positive
3	15	18	O	test
4	20	26	O	results
5	28	30	O	may
6	32	33	O	be
7	35	42	O	expected
8	44	46	O	for
9	48	54	O	several
10	56	59	O	days
11	61	69	O	following
12	71	85	O	discontinuation
13	87	88	O	of
14	90	100	O	XXXXXXXX
15	102	108	O	therapy
NULL

Venlafaxine Hydrochloride	5792	34073-7	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	18
Confirmatory tests, such as gas chromatography/mass spectrometry, will distinguish venlafaxine from PCP and amphetamine.
1	0	11	O	Confirmatory
2	13	17	O	tests
3	18	18	O	,
4	20	23	O	such
5	25	26	O	as
6	28	30	O	gas
7	32	45	O	chromatography
8	46	46	O	/
9	47	50	O	mass
10	52	63	O	spectrometry
11	64	64	O	,
12	66	69	O	will
13	71	81	O	distinguish
14	83	93	O	XXXXXXXX
15	95	98	O	from
16	100	102	O	PCP
17	104	106	O	and
18	108	118	O	amphetamine
NULL

Venlafaxine Hydrochloride	5793	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	38
The exact mechanism of the antidepressant action of venlafaxine in humans is unknown, but is thought to be related to the potentiation of serotonin and norepinephrine in the central nervous system, through inhibition of their reuptake.
1	0	2	O	The
2	4	8	O	exact
3	10	18	O	mechanism
4	20	21	O	of
5	23	25	O	the
6	27	40	O	antidepressant
7	42	47	O	action
8	49	50	O	of
9	52	62	O	XXXXXXXX
10	64	65	O	in
11	67	72	O	humans
12	74	75	O	is
13	77	83	O	unknown
14	84	84	O	,
15	86	88	O	but
16	90	91	O	is
17	93	99	O	thought
18	101	102	O	to
19	104	105	O	be
20	107	113	O	related
21	115	116	O	to
22	118	120	O	the
23	122	133	O	potentiation
24	135	136	O	of
25	138	146	O	serotonin
26	148	150	O	and
27	152	165	O	norepinephrine
28	167	168	O	in
29	170	172	O	the
30	174	180	O	central
31	182	188	O	nervous
32	190	195	O	system
33	196	196	O	,
34	198	204	O	through
35	206	215	O	inhibition
36	217	218	O	of
37	220	224	O	their
38	226	233	O	reuptake
NULL

Venlafaxine Hydrochloride	5794	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	31
Non- clinical studies have demonstrated that venlafaxine and its active metabolite, ODV, are potent and selective inhibitors of neuronal serotonin and norepinephrine reuptake and weak inhibitors of dopamine reuptake.
1	0	2	O	Non
2	5	12	O	clinical
3	14	20	O	studies
4	22	25	O	have
5	27	38	O	demonstrated
6	40	43	O	that
7	45	55	O	XXXXXXXX
8	57	59	O	and
9	61	63	O	its
10	65	70	O	active
11	72	81	O	metabolite
12	82	82	O	,
13	84	86	O	ODV
14	87	87	O	,
15	89	91	O	are
16	93	98	O	potent
17	100	102	O	and
18	104	112	O	selective
19	114	123	O	inhibitors
20	125	126	O	of
21	128	135	O	neuronal
22	137	145	O	serotonin
23	147	149	O	and
24	151	164	O	norepinephrine
25	166	173	O	reuptake
26	175	177	O	and
27	179	182	O	weak
28	184	193	O	inhibitors
29	195	196	O	of
30	198	205	O	dopamine
31	207	214	O	reuptake
NULL

Venlafaxine Hydrochloride	5795	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	20
Venlafaxine and ODV have no significant affinity for muscarinic-cholinergic, H1-histaminergic, or A1-adrenergic receptors in vitro.
1	0	10	O	XXXXXXXX
2	12	14	O	and
3	16	18	O	ODV
4	20	23	O	have
5	25	26	O	no
6	28	38	O	significant
7	40	47	O	affinity
8	49	51	O	for
9	53	62	O	muscarinic
10	64	74	O	cholinergic
11	75	75	O	,
12	77	78	O	H1
13	80	92	O	histaminergic
14	93	93	O	,
15	95	96	O	or
16	98	99	O	A1
17	101	110	O	adrenergic
18	112	120	O	receptors
19	122	123	O	in
20	125	129	O	vitro
NULL

Venlafaxine Hydrochloride	5796	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	25
Pharmacologic activity at these receptors is hypothesized to be associated with the various anticholinergic, sedative, and cardiovascular effects seen with other psychotropic drugs.
1	0	12	O	Pharmacologic
2	14	21	O	activity
3	23	24	O	at
4	26	30	O	these
5	32	40	O	receptors
6	42	43	O	is
7	45	56	O	hypothesized
8	58	59	O	to
9	61	62	O	be
10	64	73	O	associated
11	75	78	O	with
12	80	82	O	the
13	84	90	O	various
14	92	106	O	anticholinergic
15	107	107	O	,
16	109	116	O	sedative
17	117	117	O	,
18	119	121	O	and
19	123	136	O	cardiovascular
20	138	144	O	effects
21	146	149	O	seen
22	151	154	O	with
23	156	160	O	other
24	162	173	B-D	psychotropic
25	175	179	I-D	drugs
NULL

Venlafaxine Hydrochloride	5797	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	11
Venlafaxine and ODV do not possess monoamine oxidase (MAO) inhibitory activity.
1	0	10	O	XXXXXXXX
2	12	14	O	and
3	16	18	O	ODV
4	20	21	B-T	do
5	23	25	I-T	not
6	27	33	I-T	possess
7	35	43	B-U	monoamine
8	45	51	I-U	oxidase
9	54	56	I-U	MAO
10	59	68	I-U	inhibitory
11	70	77	I-U	activity
NULL

Venlafaxine Hydrochloride	5798	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	34
Cardiac Electrophysiology The effect of venlafaxine on the QT interval was evaluated in a randomized, double-blind, placebo- and positive-controlled three-period crossover thorough QT study in 54 healthy adult subjects.
1	0	6	O	Cardiac
2	8	24	O	Electrophysiology
3	26	28	O	The
4	30	35	O	effect
5	37	38	O	of
6	40	50	O	XXXXXXXX
7	52	53	O	on
8	55	57	O	the
9	59	60	O	QT
10	62	69	O	interval
11	71	73	O	was
12	75	83	O	evaluated
13	85	86	O	in
14	88	88	O	a
15	90	99	O	randomized
16	100	100	O	,
17	102	107	O	double
18	109	113	O	blind
19	114	114	O	,
20	116	122	O	placebo
21	125	127	O	and
22	129	136	O	positive
23	138	147	O	controlled
24	149	153	O	three
25	155	160	O	period
26	162	170	O	crossover
27	172	179	O	thorough
28	181	182	O	QT
29	184	188	O	study
30	190	191	O	in
31	193	194	O	54
32	196	202	O	healthy
33	204	208	O	adult
34	210	217	O	subjects
NULL

Venlafaxine Hydrochloride	5799	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	11
No significant QT prolongation effect of venlafaxine 450 mg was detected.
1	0	1	O	No
2	3	13	O	significant
3	15	16	O	QT
4	18	29	O	prolongation
5	31	36	O	effect
6	38	39	O	of
7	41	51	O	XXXXXXXX
8	53	55	O	450
9	57	58	O	mg
10	60	62	O	was
11	64	71	O	detected
NULL

Venlafaxine Hydrochloride	5800	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	19
Steady-state concentrations of venlafaxine and ODV in plasma are attained within 3 days of oral multiple-dose therapy.
1	0	5	B-T	Steady
2	7	11	I-T	state
3	13	26	I-T	concentrations
4	28	29	O	of
5	31	41	O	XXXXXXXX
6	43	45	O	and
7	47	49	O	ODV
8	51	52	O	in
9	54	59	O	plasma
10	61	63	O	are
11	65	72	O	attained
12	74	79	O	within
13	81	81	O	3
14	83	86	O	days
15	88	89	O	of
16	91	94	O	oral
17	96	103	O	multiple
18	105	108	O	dose
19	110	116	O	therapy
NULL

Venlafaxine Hydrochloride	5801	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	17
Venlafaxine and ODV exhibited linear kinetics over the dose range of 75 to 450 mg per day.
1	0	10	O	XXXXXXXX
2	12	14	O	and
3	16	18	O	ODV
4	20	28	O	exhibited
5	30	35	O	linear
6	37	44	O	kinetics
7	46	49	O	over
8	51	53	O	the
9	55	58	O	dose
10	60	64	O	range
11	66	67	O	of
12	69	70	O	75
13	72	73	O	to
14	75	77	O	450
15	79	80	O	mg
16	82	84	O	per
17	86	88	O	day
NULL

Venlafaxine Hydrochloride	5802	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	47
Mean*SD steady-state plasma clearance of venlafaxine and ODV is 1.3*0.6 and 0.4*0.2 L/h/kg, respectively; apparent elimination half-life is 5*2 and 11*2 hours, respectively; and apparent (steady-state) volume of distribution is 7.5*3.7 and 5.7*1.8 L/kg, respectively.
1	0	6	O	Mean*SD
2	8	13	O	steady
3	15	19	O	state
4	21	26	O	plasma
5	28	36	O	clearance
6	38	39	O	of
7	41	51	O	XXXXXXXX
8	53	55	O	and
9	57	59	O	ODV
10	61	62	O	is
11	64	70	O	1.3*0.6
12	72	74	O	and
13	76	82	O	0.4*0.2
14	84	84	O	L
15	85	85	O	/
16	86	86	O	h
17	87	87	O	/
18	88	89	O	kg
19	90	90	O	,
20	92	103	O	respectively
21	106	113	O	apparent
22	115	125	O	elimination
23	127	130	O	half
24	132	135	O	life
25	137	138	O	is
26	140	142	O	5*2
27	144	146	O	and
28	148	151	O	11*2
29	153	157	O	hours
30	158	158	O	,
31	160	171	O	respectively
32	174	176	O	and
33	178	185	O	apparent
34	188	193	O	steady
35	195	199	O	state
36	202	207	O	volume
37	209	210	O	of
38	212	223	O	distribution
39	225	226	O	is
40	228	234	O	7.5*3.7
41	236	238	O	and
42	240	246	O	5.7*1.8
43	248	248	O	L
44	249	249	O	/
45	250	251	O	kg
46	252	252	O	,
47	254	265	O	respectively
NULL

Venlafaxine Hydrochloride	5803	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	17
Venlafaxine and ODV are minimally bound at therapeutic concentrations to plasma proteins (27% and 30%, respectively).
1	0	10	O	XXXXXXXX
2	12	14	O	and
3	16	18	O	ODV
4	20	22	O	are
5	24	32	O	minimally
6	34	38	O	bound
7	40	41	O	at
8	43	53	O	therapeutic
9	55	68	O	concentrations
10	70	71	O	to
11	73	78	O	plasma
12	80	87	O	proteins
13	90	92	O	27%
14	94	96	O	and
15	98	100	O	30%
16	101	101	O	,
17	103	114	O	respectively
NULL

Venlafaxine Hydrochloride	5804	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	13
Absorption and Distribution Venlafaxine is well absorbed and extensively metabolized in the liver.
1	0	9	O	Absorption
2	11	13	O	and
3	15	26	O	Distribution
4	28	38	O	XXXXXXXX
5	40	41	O	is
6	43	46	O	well
7	48	55	O	absorbed
8	57	59	O	and
9	61	71	O	extensively
10	73	83	O	metabolized
11	85	86	O	in
12	88	90	O	the
13	92	96	O	liver
NULL

Venlafaxine Hydrochloride	5805	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	20
On the basis of mass balance studies, at least 92% of a single oral dose of venlafaxine is absorbed.
1	0	1	O	On
2	3	5	O	the
3	7	11	O	basis
4	13	14	O	of
5	16	19	O	mass
6	21	27	O	balance
7	29	35	O	studies
8	36	36	O	,
9	38	39	O	at
10	41	45	O	least
11	47	49	O	92%
12	51	52	O	of
13	54	54	O	a
14	56	61	O	single
15	63	66	O	oral
16	68	71	O	dose
17	73	74	O	of
18	76	86	O	XXXXXXXX
19	88	89	O	is
20	91	98	O	absorbed
NULL

Venlafaxine Hydrochloride	5806	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	8
The absolute bioavailability of venlafaxine is approximately 45%.
1	0	2	O	The
2	4	11	O	absolute
3	13	27	O	bioavailability
4	29	30	O	of
5	32	42	O	XXXXXXXX
6	44	45	O	is
7	47	59	O	approximately
8	61	63	O	45%
NULL

Venlafaxine Hydrochloride	5807	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	28
Administration of venlafaxine hydrochloride extended-release (150 mg once daily) generally resulted in lower Cmax and later Tmax values than for venlafaxine hydrochloride immediate-release administered twice daily (Table 16).
1	0	13	O	Administration
2	15	16	O	of
3	18	42	O	XXXXXXXX
4	44	51	O	extended
5	53	59	O	release
6	62	64	O	150
7	66	67	O	mg
8	69	72	O	once
9	74	78	O	daily
10	81	89	O	generally
11	91	98	O	resulted
12	100	101	O	in
13	103	107	O	lower
14	109	112	O	Cmax
15	114	116	O	and
16	118	122	O	later
17	124	127	O	Tmax
18	129	134	O	values
19	136	139	O	than
20	141	143	O	for
21	145	169	O	XXXXXXXX
22	171	179	O	immediate
23	181	187	O	release
24	189	200	O	administered
25	202	206	O	twice
26	208	212	O	daily
27	215	219	O	Table
28	221	222	O	16
NULL

Venlafaxine Hydrochloride	5808	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	49
When equal daily doses of venlafaxine were administered as either an immediate-release tablet or the extended-release capsule, the exposure to both venlafaxine and ODV was similar for the two treatments, and the fluctuation in plasma concentrations was slightly lower with the venlafaxine hydrochloride extended-release capsule.
1	0	3	O	When
2	5	9	O	equal
3	11	15	O	daily
4	17	21	O	doses
5	23	24	O	of
6	26	36	O	XXXXXXXX
7	38	41	O	were
8	43	54	O	administered
9	56	57	O	as
10	59	64	O	either
11	66	67	O	an
12	69	77	O	immediate
13	79	85	O	release
14	87	92	O	tablet
15	94	95	O	or
16	97	99	O	the
17	101	108	O	extended
18	110	116	O	release
19	118	124	O	capsule
20	125	125	O	,
21	127	129	O	the
22	131	138	O	exposure
23	140	141	O	to
24	143	146	O	both
25	148	158	O	XXXXXXXX
26	160	162	O	and
27	164	166	O	ODV
28	168	170	O	was
29	172	178	O	similar
30	180	182	O	for
31	184	186	O	the
32	188	190	O	two
33	192	201	O	treatments
34	202	202	O	,
35	204	206	O	and
36	208	210	O	the
37	212	222	O	fluctuation
38	224	225	O	in
39	227	232	O	plasma
40	234	247	O	concentrations
41	249	251	O	was
42	253	260	O	slightly
43	262	266	O	lower
44	268	271	O	with
45	273	275	O	the
46	277	301	O	XXXXXXXX
47	303	310	O	extended
48	312	318	O	release
49	320	326	O	capsule
NULL

Venlafaxine Hydrochloride	5809	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	24
Therefore, venlafaxine hydrochloride extended-release provides a slower rate of absorption, but the same extent of absorption compared with the immediate-release tablet.
1	0	8	O	Therefore
2	9	9	O	,
3	11	35	O	XXXXXXXX
4	37	44	O	extended
5	46	52	O	release
6	54	61	O	provides
7	63	63	O	a
8	65	70	O	slower
9	72	75	O	rate
10	77	78	O	of
11	80	89	O	absorption
12	90	90	O	,
13	92	94	O	but
14	96	98	O	the
15	100	103	O	same
16	105	110	O	extent
17	112	113	O	of
18	115	124	O	absorption
19	126	133	O	compared
20	135	138	O	with
21	140	142	O	the
22	144	152	O	immediate
23	154	160	O	release
24	162	167	O	tablet
NULL

Venlafaxine Hydrochloride	5810	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	74
Table 16: Comparison of Cmax and Tmax Values for Venlafaxine and ODV Following Oral Administration of Venlafaxine hydrochloride Extended-Release and Venlafaxine hydrochloride Immediate-Release) Venlafaxine ODV Cmax (ng/mL) Tmax (h) Cmax (ng/mL) Tmax (h) Venlafaxine hydrochloride Extended-Release (150 mg once daily) 150 5.5 260 9 Venlafaxine hydrochloride Immediate-Release (75 mg twice daily) 225 2 290 3 Food did not affect the bioavailability of venlafaxine or its active metabolite, ODV.
1	0	4	O	Table
2	6	7	O	16
3	8	8	O	:
4	10	19	O	Comparison
5	21	22	O	of
6	24	27	O	Cmax
7	29	31	O	and
8	33	36	O	Tmax
9	38	43	O	Values
10	45	47	O	for
11	49	59	O	XXXXXXXX
12	61	63	O	and
13	65	67	O	ODV
14	69	77	O	Following
15	79	82	O	Oral
16	84	97	O	Administration
17	99	100	O	of
18	102	126	O	XXXXXXXX
19	128	135	O	Extended
20	137	143	O	Release
21	145	147	O	and
22	149	173	O	XXXXXXXX
23	175	183	O	Immediate
24	185	191	O	Release
25	194	204	O	XXXXXXXX
26	206	208	O	ODV
27	210	213	O	Cmax
28	216	217	O	ng
29	218	218	O	/
30	219	220	O	mL
31	223	226	O	Tmax
32	229	229	O	h
33	232	235	O	Cmax
34	238	239	O	ng
35	240	240	O	/
36	241	242	O	mL
37	245	248	O	Tmax
38	251	251	O	h
39	254	278	O	XXXXXXXX
40	280	287	O	Extended
41	289	295	O	Release
42	298	300	O	150
43	302	303	O	mg
44	305	308	O	once
45	310	314	O	daily
46	317	319	O	150
47	321	323	O	5.5
48	325	327	O	260
49	329	329	O	9
50	331	355	O	XXXXXXXX
51	357	365	O	Immediate
52	367	373	O	Release
53	376	377	O	75
54	379	380	O	mg
55	382	386	O	twice
56	388	392	O	daily
57	395	397	O	225
58	399	399	O	2
59	401	403	O	290
60	405	405	O	3
61	407	410	O	Food
62	412	414	O	did
63	416	418	O	not
64	420	425	O	affect
65	427	429	O	the
66	431	445	O	bioavailability
67	447	448	O	of
68	450	460	O	XXXXXXXX
69	462	463	O	or
70	465	467	O	its
71	469	474	O	active
72	476	485	O	metabolite
73	486	486	O	,
74	488	490	O	ODV
NULL

Venlafaxine Hydrochloride	5811	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	23
Time of administration (AM versus PM) did not affect the pharmacokinetics of venlafaxine and ODV from the 75 mg venlafaxine hydrochloride extended-release capsule.
1	0	3	O	Time
2	5	6	O	of
3	8	21	O	administration
4	24	25	O	AM
5	27	32	O	versus
6	34	35	O	PM
7	38	40	O	did
8	42	44	O	not
9	46	51	O	affect
10	53	55	O	the
11	57	72	O	pharmacokinetics
12	74	75	O	of
13	77	87	O	XXXXXXXX
14	89	91	O	and
15	93	95	O	ODV
16	97	100	O	from
17	102	104	O	the
18	106	107	O	75
19	109	110	O	mg
20	112	136	O	XXXXXXXX
21	138	145	O	extended
22	147	153	O	release
23	155	161	O	capsule
NULL

Venlafaxine Hydrochloride	5812	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	36
Venlafaxine is not highly bound to plasma proteins; therefore, administration of venlafaxine hydrochloride extended-release to a patient taking another drug that is highly protein-bound should not cause increased free concentrations of the other drug.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	17	O	not
4	19	24	O	highly
5	26	30	O	bound
6	32	33	O	to
7	35	40	O	plasma
8	42	49	O	proteins
9	52	60	O	therefore
10	61	61	O	,
11	63	76	O	administration
12	78	79	O	of
13	81	105	O	XXXXXXXX
14	107	114	O	extended
15	116	122	O	release
16	124	125	O	to
17	127	127	O	a
18	129	135	O	patient
19	137	142	O	taking
20	144	150	O	another
21	152	155	O	drug
22	157	160	O	that
23	162	163	O	is
24	165	170	O	highly
25	172	178	O	protein
26	180	184	O	bound
27	186	191	O	should
28	193	195	O	not
29	197	201	O	cause
30	203	211	O	increased
31	213	216	O	free
32	218	231	O	concentrations
33	233	234	O	of
34	236	238	O	the
35	240	244	O	other
36	246	249	O	drug
NULL

Venlafaxine Hydrochloride	5813	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	34
Metabolism and Elimination Following absorption, venlafaxine undergoes extensive presystemic metabolism in the liver, primarily to ODV, but also to N-desmethylvenlafaxine, N,O-didesmethylvenlafaxine, and other minor metabolites.
1	0	9	O	Metabolism
2	11	13	O	and
3	15	25	O	Elimination
4	27	35	O	Following
5	37	46	O	absorption
6	47	47	O	,
7	49	59	O	XXXXXXXX
8	61	69	O	undergoes
9	71	79	O	extensive
10	81	91	O	presystemic
11	93	102	O	metabolism
12	104	105	O	in
13	107	109	O	the
14	111	115	O	liver
15	116	116	O	,
16	118	126	O	primarily
17	128	129	O	to
18	131	133	O	ODV
19	134	134	O	,
20	136	138	O	but
21	140	143	O	also
22	145	146	O	to
23	148	148	O	N
24	150	158	O	desmethyl
25	159	169	O	XXXXXXXX
26	170	170	O	,
27	172	174	O	N,O
28	176	186	O	didesmethyl
29	187	197	O	XXXXXXXX
30	198	198	O	,
31	200	202	O	and
32	204	208	O	other
33	210	214	O	minor
34	216	226	O	metabolites
NULL

Venlafaxine Hydrochloride	5814	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	49
In vitro studies indicate that the formation of ODV is catalyzed by CYP2D6; this has been confirmed in a clinical study showing that patients with low CYP2D6 levels (poor metabolizers) had increased levels of venlafaxine and reduced levels of ODV compared to people with normal CYP2D6 levels (extensive metabolizers).
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	24	O	indicate
5	26	29	O	that
6	31	33	B-T	the
7	35	43	I-T	formation
8	45	46	O	of
9	48	50	O	ODV
10	52	53	O	is
11	55	63	O	catalyzed
12	65	66	O	by
13	68	73	B-K	CYP2D6
14	76	79	O	this
15	81	83	O	has
16	85	88	O	been
17	90	98	O	confirmed
18	100	101	O	in
19	103	103	O	a
20	105	112	O	clinical
21	114	118	O	study
22	120	126	O	showing
23	128	131	O	that
24	133	140	O	patients
25	142	145	O	with
26	147	149	B-K	low
27	151	156	I-K	CYP2D6
28	158	163	O	levels
29	166	169	O	poor
30	171	182	O	metabolizers
31	185	187	O	had
32	189	197	B-T	increased
33	199	204	I-T	levels
34	206	207	O	of
35	209	219	O	XXXXXXXX
36	221	223	O	and
37	225	231	B-T	reduced
38	233	238	I-T	levels
39	240	241	O	of
40	243	245	O	ODV
41	247	254	O	compared
42	256	257	O	to
43	259	264	O	people
44	266	269	O	with
45	271	276	O	normal
46	278	283	B-K	CYP2D6
47	285	290	O	levels
48	293	301	O	extensive
49	303	314	O	metabolizers
K/13:C54605 K/26:C54605 K/46:C54605

Venlafaxine Hydrochloride	5815	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	33
Approximately 87% of a venlafaxine dose is recovered in the urine within 48 hours as unchanged venlafaxine (5%), unconjugated ODV (29%), conjugated ODV (26%), or other minor inactive metabolites (27%).
1	0	12	O	Approximately
2	14	16	O	87%
3	18	19	O	of
4	21	21	O	a
5	23	33	O	XXXXXXXX
6	35	38	O	dose
7	40	41	O	is
8	43	51	O	recovered
9	53	54	O	in
10	56	58	O	the
11	60	64	O	urine
12	66	71	O	within
13	73	74	O	48
14	76	80	O	hours
15	82	83	O	as
16	85	93	O	unchanged
17	95	105	O	XXXXXXXX
18	108	109	O	5%
19	111	111	O	,
20	113	124	O	unconjugated
21	126	128	O	ODV
22	131	133	O	29%
23	135	135	O	,
24	137	146	O	conjugated
25	148	150	O	ODV
26	153	155	O	26%
27	157	157	O	,
28	159	160	O	or
29	162	166	O	other
30	168	172	O	minor
31	174	181	O	inactive
32	183	193	O	metabolites
33	196	198	O	27%
NULL

Venlafaxine Hydrochloride	5816	34090-1	4c401522-0108-49cb-8a41-fb5ad4dd0fb1	14
Renal elimination of venlafaxine and its metabolites is thus the primary route of excretion.
1	0	4	O	Renal
2	6	16	B-T	elimination
3	18	19	O	of
4	21	31	O	XXXXXXXX
5	33	35	O	and
6	37	39	O	its
7	41	51	O	metabolites
8	53	54	O	is
9	56	59	O	thus
10	61	63	O	the
11	65	71	O	primary
12	73	77	O	route
13	79	80	O	of
14	82	90	O	excretion
NULL

