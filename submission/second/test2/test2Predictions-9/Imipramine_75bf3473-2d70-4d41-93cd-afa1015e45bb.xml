<?xml version="1.0" ?>
<Label drug="Imipramine" setid="75bf3473-2d70-4d41-93cd-afa1015e45bb">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  The mechanism of action of imipramine is not definitely known. However, it does not act primarily by stimulation of the central nervous system. The clinical effect is hypothesized as being due to potentiation of adrenergic synapses by blocking uptake of norepinephrine at nerve endings.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Imipramine" id="972" section="34073-7">
      

      <SentenceText>Drugs Metabolized by P450 2D6 – The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the Caucasian population (about 7% to 10% of Caucasians are so called “poor metabolizers”); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African and other populations are not yet available.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="6 23" str="Metabolized by P450 2D6" type="Precipitant"/>
      <Interaction effect="C54358" id="I1" precipitant="M1" trigger="M1" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M2" span="69 65" str="metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase" type="Precipitant"/>
      <Interaction effect="C54356" id="I2" precipitant="M2" trigger="M2" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="973" section="34073-7">
      

      <SentenceText>Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="23 42" str="higher than expected plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="79 21" str="antidepressants (TCAs" type="Precipitant"/>
      <Interaction effect="C54357" id="I3" precipitant="M4" trigger="M3" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="974" section="34073-7">
      

      <SentenceText>Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8-fold increase in plasma AUC of the TCA).</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="133 22" str="increase in plasma AUC" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="975" section="34073-7">
      

      <SentenceText>In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="21 34" str="drugs inhibit the activity of this" type="Precipitant"/>
      <Interaction id="I4" precipitant="M6" trigger="M6" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="976" section="34073-7">
      

      <SentenceText>An individual who is stable on a given dose of TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="977" section="34073-7">
      

      <SentenceText>The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).</SentenceText>
      

      <Mention code="NO MAP" id="M7" span="4 38" str="drugs that inhibit cytochrome P450 2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I5" precipitant="M7" trigger="M7" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M8" span="254 10" str="flecainide" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="978" section="34073-7">
      

      <SentenceText>While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="81 10" str="sertraline" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="979" section="34073-7">
      

      <SentenceText>The extent to which SSRI-TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="980" section="34073-7">
      

      <SentenceText>Nevertheless, caution is indicated in the co-administration of TCAs with any of the SSRIs and also in switching from one class to the other.</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="14 20" str="caution is indicated" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="84 5" str="SSRIs" type="Precipitant"/>
      <Interaction id="I6" precipitant="M11" trigger="M10" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M12" span="121 18" str="class to the other" type="Precipitant"/>
      <Interaction id="I7" precipitant="M12" trigger="M10" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="981" section="34073-7">
      

      <SentenceText>Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="982" section="34073-7">
      

      <SentenceText>Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="105 24" str="lower doses than usually" type="Trigger"/>
      <Mention code="NO MAP" id="M14" span="73 19" str="cytochrome P450 2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I8" precipitant="M14" trigger="M13" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="983" section="34073-7">
      

      <SentenceText>Furthermore, whenever one of these other drugs is withdrawn from co-therapy, an increased dose of tricyclic antidepressant may be required.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="984" section="34073-7">
      

      <SentenceText>It is desirable to monitor TCA plasma levels whenever a TCA is going to be co-administered with another drug known to be an inhibitor of P450 2D6.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="137 8" str="P450 2D6" type="Precipitant"/>
      <Interaction id="I9" precipitant="M15" trigger="M15" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="985" section="34073-7">
      

      <SentenceText>The plasma concentration of imipramine may increase when the drug is given concomitantly with hepatic enzyme inhibitors (e.g., cimetidine, fluoxetine) and decrease by concomitant administration with hepatic enzyme inducers (e.g., barbiturates, phenytoin), and adjustment of the dosage of imipramine may therefore be necessary.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="278 6" str="dosage" type="Trigger"/>
      <Mention code="NO MAP" id="M17" span="102 17" str="enzyme inhibitors" type="Precipitant"/>
      <Interaction effect="C54358" id="I10" precipitant="M17" trigger="M16" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="986" section="34073-7">
      

      <SentenceText>In occasional susceptible patients or in those receiving anticholinergic drugs (including antiparkinsonism agents) in addition, the atropine-like effects may become more pronounced (e.g., paralytic ileus).</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="57 21" str="anticholinergic drugs" type="Precipitant"/>
      <Mention code="NO MAP" id="M19" span="90 23" str="antiparkinsonism agents" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="987" section="34073-7">
      

      <SentenceText>Close supervision and careful adjustment of dosage is required when imipramine pamoate is administered concomitantly with anticholinergic drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="6 11" str="supervision" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="79 7" str="pamoate" type="Precipitant"/>
      <Interaction id="I11" precipitant="M21" trigger="M20" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M22" span="122 21" str="anticholinergic drugs" type="Precipitant"/>
      <Interaction id="I12" precipitant="M22" trigger="M20" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="988" section="34073-7">
      

      <SentenceText>Avoid the use of preparations, such as decongestants and local anesthetics, that contain any sympathomimetic amine (e.g., epinephrine, norepinephrine), since it has been reported that tricyclic antidepressants can potentiate the effects of catecholamines.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="0 5" str="Avoid" type="Trigger"/>
      <Mention code="NO MAP" id="M24" span="225 11" str="the effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M25" span="240 14" str="catecholamines" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="17 12" str="preparations" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I13" precipitant="M26" trigger="M23" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M27" span="57 17" str="local anesthetics" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I14" precipitant="M27" trigger="M23" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M28" span="93 21" str="sympathomimetic amine" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I15" precipitant="M28" trigger="M23" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M29" span="122 11" str="epinephrine" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I16" precipitant="M29" trigger="M23" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M30" span="135 14" str="norepinephrine" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I17" precipitant="M30" trigger="M23" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M31" span="194 15" str="antidepressants" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I18" precipitant="M31" trigger="M23" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="989" section="34073-7">
      

      <SentenceText>Caution should be exercised when imipramine pamoate is used with agents that lower blood pressure.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="0 7" str="Caution" type="Trigger"/>
      <Mention code="NO MAP" id="M33" span="44 7" str="pamoate" type="Precipitant"/>
      <Interaction id="I19" precipitant="M33" trigger="M32" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M34" span="65 32" str="agents that lower blood pressure" type="Precipitant"/>
      <Interaction id="I20" precipitant="M34" trigger="M32" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="990" section="34073-7">
      

      <SentenceText>Imipramine pamoate may potentiate the effects of CNS depressant drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="23 46" str="potentiate the effects of CNS depressant drugs" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M36" span="11 7" str="pamoate" type="Precipitant"/>
      <Interaction effect="M35" id="I21" precipitant="M36" trigger="M36" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="991" section="34073-7">
      

      <SentenceText>Patients should be warned that imipramine pamoate may enhance the CNS depressant effects of alcohol.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="54 34" str="enhance the CNS depressant effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M38" span="42 7" str="pamoate" type="Precipitant"/>
      <Interaction effect="M37" id="I22" precipitant="M38" trigger="M38" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M39" span="92 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="M37" id="I23" precipitant="M39" trigger="M39" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="992" section="34073-7">
      

      <SentenceText>Monoamine Oxidase Inhibitors (MAOIs)</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="993" section="34073-7">
      

      <SentenceText>Serotonergic Drugs</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="994" section="34090-1">
      

      <SentenceText>The mechanism of action of imipramine is not definitely known.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="995" section="34090-1">
      

      <SentenceText>However, it does not act primarily by stimulation of the central nervous system.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Imipramine" id="996" section="34090-1">
      

      <SentenceText>The clinical effect is hypothesized as being due to potentiation of adrenergic synapses by blocking uptake of norepinephrine at nerve endings.</SentenceText>
      

      <Mention code="NO MAP" id="M40" span="68 19" str="adrenergic synapses" type="Precipitant"/>
      <Mention code="NO MAP" id="M41" span="91 33" str="blocking uptake of norepinephrine" type="Precipitant"/>
    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

