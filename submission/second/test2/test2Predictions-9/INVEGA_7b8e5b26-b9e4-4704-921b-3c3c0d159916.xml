<?xml version="1.0" ?>
<Label drug="INVEGA" setid="7b8e5b26-b9e4-4704-921b-3c3c0d159916">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS  Centrally-acting drugs: Due to CNS effects, use caution in combination. Avoid alcohol. ( 7.1 )  Drugs that may cause orthostatic hypotension: An additive effect may be observed when co-administered with INVEGA ® . ( 7.1 )  Strong CYP3A4/P-glycoprotein (P-gp) inducers: It may be necessary to increase the dose of INVEGA ® when a strong inducer of both CYP3A4 and P-gp (e.g., carbamazepine) is co-administered. Conversely, on discontinuation of the strong inducer, it may be necessary to decrease the dose of INVEGA ® . ( 7.2 )  Co-administration of divalproex sodium increased C max and AUC of paliperidone by approximately 50%. Adjust dose of INVEGA ® if necessary based on clinical assessment. ( 7.2 )  7.1 Potential for INVEGA ® to Affect Other Drugs  Given the primary CNS effects of paliperidone [see Adverse Reactions (6.1 , 6.2 )] , INVEGA ® should be used with caution in combination with other centrally acting drugs and alcohol. Paliperidone may antagonize the effect of levodopa and other dopamine agonists.  Because of its potential for inducing orthostatic hypotension, an additive effect may be observed when INVEGA ® is administered with other therapeutic agents that have this potential [see Warnings and Precautions (5.9) ] .  Paliperidone is not expected to cause clinically important pharmacokinetic interactions with drugs that are metabolized by cytochrome P450 isozymes. In vitro studies in human liver microsomes showed that paliperidone does not substantially inhibit the metabolism of drugs metabolized by cytochrome P450 isozymes, including CYP1A2, CYP2A6, CYP2C8/9/10, CYP2D6, CYP2E1, CYP3A4, and CYP3A5. Therefore, paliperidone is not expected to inhibit clearance of drugs that are metabolized by these metabolic pathways in a clinically relevant manner. Paliperidone is also not expected to have enzyme inducing properties.  Paliperidone is a weak inhibitor of P-glycoprotein (P-gp) at high concentrations. No in vivo data are available and the clinical relevance is unknown.  Pharmacokinetic interaction between lithium and INVEGA ® is unlikely.  In a drug interaction study, co-administration of INVEGA ® (12 mg once daily for 5 days) with divalproex sodium extended-release tablets (500 mg to 2000 mg once daily) did not affect the steady-state pharmacokinetics (AUC 24h and C max,ss ) of valproate in 13 patients stabilized on valproate. In a clinical study, subjects on stable doses of valproate had comparable valproate average plasma concentrations when INVEGA ® 3–15 mg/day was added to their existing valproate treatment.  7.2 Potential for Other Drugs to Affect INVEGA ®  Paliperidone is not a substrate of CYP1A2, CYP2A6, CYP2C9, and CYP2C19, so that an interaction with inhibitors or inducers of these isozymes is unlikely. While in vitro studies indicate that CYP2D6 and CYP3A4 may be minimally involved in paliperidone metabolism, in vivo studies do not show decreased elimination by these isozymes and they contribute to only a small fraction of total body clearance. In vitro studies have shown that paliperidone is a P-gp substrate.  Co-administration of INVEGA ® 6 mg once daily with carbamazepine, a strong inducer of both CYP3A4 and P-glycoprotein (P-gp), at 200 mg twice daily caused a decrease of approximately 37% in the mean steady-state C max and AUC of paliperidone. This decrease is caused, to a substantial degree, by a 35% increase in renal clearance of paliperidone. A minor decrease in the amount of drug excreted unchanged in the urine suggests that there was little effect on the CYP metabolism or bioavailability of paliperidone during carbamazepine co-administration. On initiation of carbamazepine, the dose of INVEGA ® should be re-evaluated and increased if necessary. Conversely, on discontinuation of carbamazepine, the dose of INVEGA ® should be re-evaluated and decreased if necessary.  Paliperidone is metabolized to a limited extent by CYP2D6 [see Clinical Pharmacology (12.3) ] . In an interaction study in healthy subjects in which a single 3 mg dose of INVEGA ® was administered concomitantly with 20 mg per day of paroxetine (a potent CYP2D6 inhibitor), paliperidone exposures were on average 16% (90% CI: 4, 30) higher in CYP2D6 extensive metabolizers. Higher doses of paroxetine have not been studied. The clinical relevance is unknown.  Co-administration of a single dose of INVEGA ® 12 mg with divalproex sodium extended-release tablets (two 500 mg tablets once daily) resulted in an increase of approximately 50% in the C max and AUC of paliperidone. Dosage reduction for INVEGA ® should be considered when INVEGA ® is co-administered with valproate after clinical assessment.  Pharmacokinetic interaction between lithium and INVEGA ® is unlikely.</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY  12.1 Mechanism of Action  Paliperidone is the major active metabolite of risperidone. The mechanism of action of paliperidone, as with other drugs having efficacy in schizophrenia, is unknown, but it has been proposed that the drug's therapeutic activity in schizophrenia is mediated through a combination of central dopamine Type 2 (D 2 ) and serotonin Type 2 (5HT 2A ) receptor antagonism.  12.2 Pharmacodynamics  Paliperidone is a centrally active dopamine Type 2 (D 2 ) antagonist and with predominant serotonin Type 2 (5HT 2A ) activity. Paliperidone is also active as an antagonist at α 1 and α 2 adrenergic receptors and H 1 histaminergic receptors, which may explain some of the other effects of the drug. Paliperidone has no affinity for cholinergic muscarinic or β 1 - and β 2 -adrenergic receptors. The pharmacological activity of the (+)- and (-)- paliperidone enantiomers is qualitatively and quantitatively similar in vitro.  12.3 Pharmacokinetics  Following a single dose, the plasma concentrations of paliperidone gradually rise to reach peak plasma concentration (C max ) approximately 24 hours after dosing. The pharmacokinetics of paliperidone following INVEGA ® administration are dose-proportional within the available dose range. The terminal elimination half-life of paliperidone is approximately 23 hours.  Steady-state concentrations of paliperidone are attained within 4–5 days of dosing with INVEGA ® in most subjects. The mean steady-state peak:trough ratio for an INVEGA ® dose of 9 mg was 1.7 with a range of 1.2–3.1.  Following administration of INVEGA ® , the (+) and (-) enantiomers of paliperidone interconvert, reaching an AUC (+) to (-) ratio of approximately 1.6 at steady state.  Absorption and Distribution  The absolute oral bioavailability of paliperidone following INVEGA ® administration is 28%.  Administration of a 12 mg paliperidone extended-release tablet to healthy ambulatory subjects with a standard high-fat/high-caloric meal gave mean C max and AUC values of paliperidone that were increased by 60% and 54%, respectively, compared with administration under fasting conditions. Clinical trials establishing the safety and efficacy of INVEGA ® were carried out in subjects without regard to the timing of meals. While INVEGA ® can be taken without regard to food, the presence of food at the time of INVEGA ® administration may increase exposure to paliperidone [see Dosage and Administration (2.3) ] .  Based on a population analysis, the apparent volume of distribution of paliperidone is 487 L. The plasma protein binding of racemic paliperidone is 74%.  Metabolism and Elimination  Although in vitro studies suggested a role for CYP2D6 and CYP3A4 in the metabolism of paliperidone, in vivo results indicate that these isozymes play a limited role in the overall elimination of paliperidone [see Drug Interactions (7) ] .  One week following administration of a single oral dose of 1 mg immediate-release 14 C-paliperidone to 5 healthy volunteers, 59% (range 51% – 67%) of the dose was excreted unchanged into urine, 32% (26% – 41%) of the dose was recovered as metabolites, and 6% – 12% of the dose was not recovered. Approximately 80% of the administered radioactivity was recovered in urine and 11% in the feces. Four primary metabolic pathways have been identified in vivo , none of which could be shown to account for more than 10% of the dose: dealkylation, hydroxylation, dehydrogenation, and benzisoxazole scission.  Population pharmacokinetic analyses found no difference in exposure or clearance of paliperidone between extensive metabolizers and poor metabolizers of CYP2D6 substrates.  Special Populations  Renal Impairment  The dose of INVEGA ® should be reduced in patients with moderate or severe renal impairment [see Dosage and Administration (2.5) ] . The disposition of a single dose paliperidone 3 mg extended-release tablet was studied in adult subjects with varying degrees of renal function. Elimination of paliperidone decreased with decreasing estimated creatinine clearance. Total clearance of paliperidone was reduced in subjects with impaired renal function by 32% on average in mild (CrCl = 50 mL/min to &lt; 80 mL/min), 64% in moderate (CrCl = 30 mL/min to &lt; 50 mL/min), and 71% in severe (CrCl = 10 mL/min to &lt; 30 mL/min) renal impairment, corresponding to an average increase in exposure (AUC inf ) of 1.5 fold, 2.6 fold, and 4.8 fold, respectively, compared to healthy subjects. The mean terminal elimination half-life of paliperidone was 24 hours, 40 hours, and 51 hours in subjects with mild, moderate, and severe renal impairment, respectively, compared with 23 hours in subjects with normal renal function (CrCl ≥ 80 mL/min).  Hepatic Impairment  In a study in adult subjects with moderate hepatic impairment (Child-Pugh class B), the plasma concentrations of free paliperidone were similar to those of healthy subjects, although total paliperidone exposure decreased because of a decrease in protein binding. Consequently, no dose adjustment is required in patients with mild or moderate hepatic impairment. INVEGA ® has not been studied in patients with severe hepatic impairment.  Adolescents (12–17 years of age)  Paliperidone systemic exposure in adolescents weighing ≥ 51 kg (≥ 112 lbs) was similar to that in adults. In adolescents weighing &lt; 51 kg (&lt; 112 lbs), a 23% higher exposure was observed; this is considered not to be clinically significant. Age did not influence the paliperidone exposure.  Elderly  No dosage adjustment is recommended based on age alone. However, dose adjustment may be required because of age-related decreases in creatinine clearance [see Renal Impairment above and Dosage and Administration (2.1 , 2.5 )] .  Race  No dosage adjustment is recommended based on race. No differences in pharmacokinetics were observed in a pharmacokinetic study conducted in Japanese and Caucasians.  Gender  No dosage adjustment is recommended based on gender. No differences in pharmacokinetics were observed in a pharmacokinetic study conducted in men and women.  Smoking  No dosage adjustment is recommended based on smoking status. Based on in vitro studies utilizing human liver enzymes, paliperidone is not a substrate for CYP1A2; smoking should, therefore, not have an effect on the pharmacokinetics of paliperidone.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="INVEGA" id="4247" section="34073-7">
      

      <SentenceText>Centrally-acting drugs: Due to CNS effects, use caution in combination.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="31 11" str="CNS effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="0 22" str="Centrally-acting drugs" type="Precipitant"/>
      <Interaction effect="M1" id="I1" precipitant="M2" trigger="M2" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4248" section="34073-7">
      

      <SentenceText>Drugs that may cause orthostatic hypotension: An additive effect may be observed when co-administered with INVEGA®.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="21 23" str="orthostatic hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M4" span="49 22" str="additive effect may be" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M5" span="0 10" str="Drugs that" type="Precipitant"/>
      <Interaction effect="M3;M4" id="I2" precipitant="M5" trigger="M5" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4249" section="34073-7">
      

      <SentenceText>Strong CYP3A4/P-glycoprotein (P-gp) inducers: It may be necessary to increase the dose of INVEGA® when a strong inducer of both CYP3A4 and P-gp (e.g., carbamazepine) is co-administered.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="69 17" str="increase the dose" type="Trigger"/>
      <Mention code="NO MAP" id="M7" span="0 28" str="Strong CYP3A4/P-glycoprotein" type="Precipitant"/>
      <Interaction id="I3" precipitant="M7" trigger="M6" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M8" span="112 31" str="inducer of both CYP3A4 and P-gp" type="Precipitant"/>
      <Interaction id="I4" precipitant="M8" trigger="M6" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4250" section="34073-7">
      

      <SentenceText>Conversely, on discontinuation of the strong inducer, it may be necessary to decrease the dose of INVEGA®.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="15 15" str="discontinuation" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="77 17" str="decrease the dose" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="45 7" str="inducer" type="Precipitant"/>
      <Interaction id="I5" precipitant="M11" trigger="M10" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4251" section="34073-7">
      

      <SentenceText>Co-administration of divalproex sodium increased Cmax and AUC of paliperidone by approximately 50%.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="39 14" str="increased Cmax" type="Trigger"/>
      <Mention code="NO MAP" id="M13" span="21 17" str="divalproex sodium" type="Precipitant"/>
      <Interaction effect="C54602" id="I6" precipitant="M13" trigger="M12" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I7" precipitant="M13" trigger="M12" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4252" section="34073-7">
      

      <SentenceText>Adjust dose of INVEGA® if necessary based on clinical assessment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4253" section="34073-7">
      

      <SentenceText>Given the primary CNS effects of paliperidone , INVEGA® should be used with caution in combination with other centrally acting drugs and alcohol.</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="76 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M15" span="110 22" str="centrally acting drugs" type="Precipitant"/>
      <Interaction id="I8" precipitant="M15" trigger="M14" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M16" span="137 7" str="alcohol" type="Precipitant"/>
      <Interaction id="I9" precipitant="M16" trigger="M14" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4254" section="34073-7">
      

      <SentenceText>Paliperidone may antagonize the effect of levodopa and other dopamine agonists.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="17 21" str="antagonize the effect" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M18" span="42 8" str="levodopa" type="Precipitant"/>
      <Interaction effect="M17" id="I10" precipitant="M18" trigger="M18" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M19" span="61 17" str="dopamine agonists" type="Precipitant"/>
      <Interaction effect="M17" id="I11" precipitant="M19" trigger="M19" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4255" section="34073-7">
      

      <SentenceText>Because of its potential for inducing orthostatic hypotension, an additive effect may be observed when INVEGA® is administered with other therapeutic agents that have this potential.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="38 23" str="orthostatic hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M21" span="66 15" str="additive effect" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M22" span="138 33" str="therapeutic agents that have this" type="Precipitant"/>
      <Interaction effect="M20;M21" id="I12" precipitant="M22" trigger="M22" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4256" section="34073-7">
      

      <SentenceText>Paliperidone is not expected to cause clinically important pharmacokinetic interactions with drugs that are metabolized by cytochrome P450 isozymes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4257" section="34073-7">
      

      <SentenceText>In vitro studies in human liver microsomes showed that paliperidone does not substantially inhibit the metabolism of drugs metabolized by cytochrome P450 isozymes, including CYP1A2, CYP2A6, CYP2C8/9/10, CYP2D6, CYP2E1, CYP3A4, and CYP3A5.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4258" section="34073-7">
      

      <SentenceText>Therefore, paliperidone is not expected to inhibit clearance of drugs that are metabolized by these metabolic pathways in a clinically relevant manner.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4259" section="34073-7">
      

      <SentenceText>Paliperidone is also not expected to have enzyme inducing properties.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4260" section="34073-7">
      

      <SentenceText>Paliperidone is a weak inhibitor of P-glycoprotein (P-gp) at high concentrations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4261" section="34073-7">
      

      <SentenceText>No in vivo data are available and the clinical relevance is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4262" section="34073-7">
      

      <SentenceText>Pharmacokinetic interaction between lithium and INVEGA® is unlikely.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4263" section="34073-7">
      

      <SentenceText>In a drug interaction study, co-administration of INVEGA® (12 mg once daily for 5 days) with divalproex sodium extended-release tablets (500 mg to 2000 mg once daily) did not affect the steady-state pharmacokinetics (AUC24h and Cmax,ss) of valproate in 13 patients stabilized on valproate.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4264" section="34073-7">
      

      <SentenceText>In a clinical study, subjects on stable doses of valproate had comparable valproate average plasma concentrations when INVEGA® 3–15 mg/day was added to their existing valproate treatment.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="49 9" str="valproate" type="Precipitant"/>
      <Interaction effect="C54356" id="I13" precipitant="M23" trigger="M23" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M24" span="74 9" str="valproate" type="Precipitant"/>
      <Interaction effect="C54356" id="I14" precipitant="M24" trigger="M24" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M25" span="167 9" str="valproate" type="Precipitant"/>
      <Interaction effect="C54356" id="I15" precipitant="M25" trigger="M25" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4265" section="34073-7">
      

      <SentenceText>Paliperidone is not a substrate of CYP1A2, CYP2A6, CYP2C9, and CYP2C19, so that an interaction with inhibitors or inducers of these isozymes is unlikely.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4266" section="34073-7">
      

      <SentenceText>While in vitro studies indicate that CYP2D6 and CYP3A4 may be minimally involved in paliperidone metabolism, in vivo studies do not show decreased elimination by these isozymes and they contribute to only a small fraction of total body clearance.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4267" section="34073-7">
      

      <SentenceText>In vitro studies have shown that paliperidone is a P-gp substrate.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4268" section="34073-7">
      

      <SentenceText>Co-administration of INVEGA® 6 mg once daily with carbamazepine, a strong inducer of both CYP3A4 and P-glycoprotein (P-gp), at 200 mg twice daily caused a decrease of approximately 37% in the mean steady-state Cmax and AUC of paliperidone.</SentenceText>
      

      <Mention code="NO MAP" id="M26" span="155 8" str="decrease" type="Trigger"/>
      <Mention code="NO MAP" id="M27" span="181 33" str="37% in the mean steady-state Cmax" type="Trigger"/>
      <Mention code="NO MAP" id="M28" span="50 13" str="carbamazepine" type="Precipitant"/>
      <Interaction effect="C54602" id="I16" precipitant="M28" trigger="M27" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I17" precipitant="M28" trigger="M27" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M29" span="74 10" str="inducer of" type="Precipitant"/>
      <Interaction effect="C54602" id="I18" precipitant="M29" trigger="M26" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I19" precipitant="M29" trigger="M26" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M30" span="90 6" str="CYP3A4" type="Precipitant"/>
      <Interaction effect="C54602" id="I20" precipitant="M30" trigger="M26" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I21" precipitant="M30" trigger="M26" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M31" span="101 17" str="P-glycoprotein (P" type="Precipitant"/>
      <Interaction effect="C54602" id="I22" precipitant="M31" trigger="M26" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I23" precipitant="M31" trigger="M26" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4269" section="34073-7">
      

      <SentenceText>This decrease is caused, to a substantial degree, by a 35% increase in renal clearance of paliperidone.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="59 27" str="increase in renal clearance" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4270" section="34073-7">
      

      <SentenceText>A minor decrease in the amount of drug excreted unchanged in the urine suggests that there was little effect on the CYP metabolism or bioavailability of paliperidone during carbamazepine co-administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4271" section="34073-7">
      

      <SentenceText>On initiation of carbamazepine, the dose of INVEGA® should be re-evaluated and increased if necessary.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4272" section="34073-7">
      

      <SentenceText>Conversely, on discontinuation of carbamazepine, the dose of INVEGA® should be re-evaluated and decreased if necessary.</SentenceText>
      

      <Mention code="NO MAP" id="M33" span="15 15" str="discontinuation" type="Trigger"/>
      <Mention code="NO MAP" id="M34" span="34 13" str="carbamazepine" type="Precipitant"/>
      <Interaction id="I24" precipitant="M34" trigger="M33" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4273" section="34073-7">
      

      <SentenceText>Paliperidone is metabolized to a limited extent by CYP2D6.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4274" section="34073-7">
      

      <SentenceText>In an interaction study in healthy subjects in which a single 3 mg dose of INVEGA® was administered concomitantly with 20 mg per day of paroxetine (a potent CYP2D6 inhibitor), paliperidone exposures were on average 16% (90% CI: 4, 30) higher in CYP2D6 extensive metabolizers.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4275" section="34073-7">
      

      <SentenceText>Higher doses of paroxetine have not been studied.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4276" section="34073-7">
      

      <SentenceText>Co-administration of a single dose of INVEGA® 12 mg with divalproex sodium extended-release tablets (two 500 mg tablets once daily) resulted in an increase of approximately 50% in the Cmax and AUC of paliperidone.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="57 10" str="divalproex" type="Precipitant"/>
      <Interaction effect="C54602" id="I25" precipitant="M35" trigger="M35" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I26" precipitant="M35" trigger="M35" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4277" section="34073-7">
      

      <SentenceText>Dosage reduction for INVEGA® should be considered when INVEGA® is co-administered with valproate after clinical assessment.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="0 16" str="Dosage reduction" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4279" section="34090-1">
      

      <SentenceText>Paliperidone is the major active metabolite of risperidone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4280" section="34090-1">
      

      <SentenceText>The mechanism of action of paliperidone, as with other drugs having efficacy in schizophrenia, is unknown, but it has been proposed that the drug's therapeutic activity in schizophrenia is mediated through a combination of central dopamine Type 2 (D2) and serotonin Type 2 (5HT2A) receptor antagonism.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4281" section="34090-1">
      

      <SentenceText>Paliperidone is a centrally active dopamine Type 2 (D2) antagonist and with predominant serotonin Type 2 (5HT2A) activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4282" section="34090-1">
      

      <SentenceText>Paliperidone is also active as an antagonist at α1 and α2 adrenergic receptors and H1 histaminergic receptors, which may explain some of the other effects of the drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4283" section="34090-1">
      

      <SentenceText>Paliperidone has no affinity for cholinergic muscarinic or β1- and β2-adrenergic receptors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4284" section="34090-1">
      

      <SentenceText>The pharmacological activity of the (+)- and (-)- paliperidone enantiomers is qualitatively and quantitatively similar in vitro.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4285" section="34090-1">
      

      <SentenceText>Following a single dose, the plasma concentrations of paliperidone gradually rise to reach peak plasma concentration (Cmax) approximately 24 hours after dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4286" section="34090-1">
      

      <SentenceText>The pharmacokinetics of paliperidone following INVEGA® administration are dose-proportional within the available dose range.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4287" section="34090-1">
      

      <SentenceText>The terminal elimination half-life of paliperidone is approximately 23 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4288" section="34090-1">
      

      <SentenceText>Steady-state concentrations of paliperidone are attained within 4–5 days of dosing with INVEGA® in most subjects.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="0 27" str="Steady-state concentrations" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4289" section="34090-1">
      

      <SentenceText>The mean steady-state peak:trough ratio for an INVEGA® dose of 9 mg was 1.7 with a range of 1.2–3.1.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4290" section="34090-1">
      

      <SentenceText>Following administration of INVEGA®, the (+) and (-) enantiomers of paliperidone interconvert, reaching an AUC (+) to (-) ratio of approximately 1.6 at steady state.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4291" section="34090-1">
      

      <SentenceText>Absorption and Distribution The absolute oral bioavailability of paliperidone following INVEGA® administration is 28%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4292" section="34090-1">
      

      <SentenceText>Administration of a 12 mg paliperidone extended-release tablet to healthy ambulatory subjects with a standard high-fat/high-caloric meal gave mean Cmax and AUC values of paliperidone that were increased by 60% and 54%, respectively, compared with administration under fasting conditions.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4293" section="34090-1">
      

      <SentenceText>Clinical trials establishing the safety and efficacy of INVEGA® were carried out in subjects without regard to the timing of meals.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4294" section="34090-1">
      

      <SentenceText>While INVEGA® can be taken without regard to food, the presence of food at the time of INVEGA® administration may increase exposure to paliperidone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4295" section="34090-1">
      

      <SentenceText>Based on a population analysis, the apparent volume of distribution of paliperidone is 487 L. The plasma protein binding of racemic paliperidone is 74%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4296" section="34090-1">
      

      <SentenceText>Metabolism and Elimination Although in vitro studies suggested a role for CYP2D6 and CYP3A4 in the metabolism of paliperidone, in vivo results indicate that these isozymes play a limited role in the overall elimination of paliperidone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4297" section="34090-1">
      

      <SentenceText>One week following administration of a single oral dose of 1 mg immediate-release 14C-paliperidone to 5 healthy volunteers, 59% (range 51% – 67%) of the dose was excreted unchanged into urine, 32% (26% – 41%) of the dose was recovered as metabolites, and 6% – 12% of the dose was not recovered.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4298" section="34090-1">
      

      <SentenceText>Approximately 80% of the administered radioactivity was recovered in urine and 11% in the feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4299" section="34090-1">
      

      <SentenceText>Four primary metabolic pathways have been identified in vivo, none of which could be shown to account for more than 10% of the dose: dealkylation, hydroxylation, dehydrogenation, and benzisoxazole scission.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4300" section="34090-1">
      

      <SentenceText>Population pharmacokinetic analyses found no difference in exposure or clearance of paliperidone between extensive metabolizers and poor metabolizers of CYP2D6 substrates.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="153 17" str="CYP2D6 substrates" type="Precipitant"/>
      <Interaction effect="C54605" id="I27" precipitant="M38" trigger="M38" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4301" section="34090-1">
      

      <SentenceText>Special Populations Renal Impairment The dose of INVEGA® should be reduced in patients with moderate or severe renal impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4302" section="34090-1">
      

      <SentenceText>The disposition of a single dose paliperidone 3 mg extended-release tablet was studied in adult subjects with varying degrees of renal function.</SentenceText>
      

      <Mention code="NO MAP" id="M39" span="110 33" str="varying degrees of renal function" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4303" section="34090-1">
      

      <SentenceText>Elimination of paliperidone decreased with decreasing estimated creatinine clearance.</SentenceText>
      

      <Mention code="NO MAP" id="M40" span="43 20" str="decreasing estimated" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4304" section="34090-1">
      

      <SentenceText>Total clearance of paliperidone was reduced in subjects with impaired renal function by 32% on average in mild (CrCl = 50 mL/min to &lt; 80 mL/min), 64% in moderate (CrCl = 30 mL/min to &lt; 50 mL/min), and 71% in severe (CrCl = 10 mL/min to &lt; 30 mL/min) renal impairment, corresponding to an average increase in exposure (AUCinf) of 1.5 fold, 2.6 fold, and 4.8 fold, respectively, compared to healthy subjects.</SentenceText>
      

      <Mention code="NO MAP" id="M41" span="6 9" str="clearance" type="Trigger"/>
      <Mention code="NO MAP" id="M42" span="304 2" str="in" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4305" section="34090-1">
      

      <SentenceText>The mean terminal elimination half-life of paliperidone was 24 hours, 40 hours, and 51 hours in subjects with mild, moderate, and severe renal impairment, respectively, compared with 23 hours in subjects with normal renal function (CrCl ≥ 80 mL/min).</SentenceText>
      

      <Mention code="NO MAP" id="M43" span="18 11" str="elimination" type="Trigger"/>
      <Mention code="NO MAP" id="M44" span="183 8" str="23 hours" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4306" section="34090-1">
      

      <SentenceText>Hepatic Impairment In a study in adult subjects with moderate hepatic impairment (Child-Pugh class B), the plasma concentrations of free paliperidone were similar to those of healthy subjects, although total paliperidone exposure decreased because of a decrease in protein binding.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4307" section="34090-1">
      

      <SentenceText>Consequently, no dose adjustment is required in patients with mild or moderate hepatic impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4308" section="34090-1">
      

      <SentenceText>INVEGA® has not been studied in patients with severe hepatic impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4309" section="34090-1">
      

      <SentenceText>Adolescents (12–17 years of age) Paliperidone systemic exposure in adolescents weighing ≥ 51 kg (≥ 112 lbs) was similar to that in adults.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4310" section="34090-1">
      

      <SentenceText>In adolescents weighing &lt; 51 kg (&lt; 112 lbs), a 23% higher exposure was observed; this is considered not to be clinically significant.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4311" section="34090-1">
      

      <SentenceText>Age did not influence the paliperidone exposure.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4312" section="34090-1">
      

      <SentenceText>Elderly No dosage adjustment is recommended based on age alone.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4313" section="34090-1">
      

      <SentenceText>However, dose adjustment may be required because of age-related decreases in creatinine clearance.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4314" section="34090-1">
      

      <SentenceText>Race No dosage adjustment is recommended based on race.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4315" section="34090-1">
      

      <SentenceText>No differences in pharmacokinetics were observed in a pharmacokinetic study conducted in Japanese and Caucasians.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4316" section="34090-1">
      

      <SentenceText>Gender No dosage adjustment is recommended based on gender.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4317" section="34090-1">
      

      <SentenceText>No differences in pharmacokinetics were observed in a pharmacokinetic study conducted in men and women.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4318" section="34090-1">
      

      <SentenceText>Smoking No dosage adjustment is recommended based on smoking status.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="INVEGA" id="4319" section="34090-1">
      

      <SentenceText>Based on in vitro studies utilizing human liver enzymes, paliperidone is not a substrate for CYP1A2; smoking should, therefore, not have an effect on the pharmacokinetics of paliperidone.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

