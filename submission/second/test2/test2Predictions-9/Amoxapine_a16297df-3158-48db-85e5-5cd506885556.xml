<?xml version="1.0" ?>
<Label drug="Amoxapine" setid="a16297df-3158-48db-85e5-5cd506885556">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Amoxapine is an antidepressant with a mild sedative component to its action. The mechanism of its clinical action in man is not well understood. In animals, amoxapine reduced the uptake of norepinephrine and serotonin and blocked the response of dopamine receptors to dopamine. Amoxapine is not a monoamine oxidase inhibitor.  Amoxapine is absorbed rapidly and reaches peak blood levels approximately 90 minutes after ingestion. It is almost completely metabolized. The main route of excretion is the kidney. In vitro tests show that amoxapine binding to human serum is approximately 90%.  In man, amoxapine serum concentration declines with a half-life of eight hours. However, the major metabolite, 8-hydroxyamoxapine, has a biologic half-life of 30 hours. Metabolites are excreted in the urine in conjugated form as glucuronides.  Clinical studies have demonstrated that amoxapine has a more rapid onset of action than either amitriptyline or imipramine. The initial clinical effect may occur within four to seven days and occurs within two weeks in over 80% of responders.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Amoxapine" id="3401" section="34073-7">
      

      <SentenceText>See CONTRAINDICATIONS about concurrent usage of tricyclic antidepressants and monoamine oxidase inhibitors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3402" section="34073-7">
      

      <SentenceText>Paralytic ileus may occur in patients taking tricyclic antidepressants in combination with anticholinergic drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="0 9" str="Paralytic" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="10 5" str="ileus" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M3" span="45 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="M1;M2" id="I1" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M4" span="91 21" str="anticholinergic drugs" type="Precipitant"/>
      <Interaction effect="M1;M2" id="I2" precipitant="M4" trigger="M4" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3403" section="34073-7">
      

      <SentenceText>Amoxapine may enhance the response to alcohol and the effects of barbiturates and other CNS depressants.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="14 20" str="enhance the response" type="Trigger"/>
      <Mention code="NO MAP" id="M6" span="38 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="C54357" id="I3" precipitant="M6" trigger="M5" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M7" span="65 12" str="barbiturates" type="Precipitant"/>
      <Interaction effect="C54357" id="I4" precipitant="M7" trigger="M5" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M8" span="88 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="C54357" id="I5" precipitant="M8" trigger="M5" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3404" section="34073-7">
      

      <SentenceText>Serum levels of several tricyclic antidepressants have been reported to be significantly increased when cimetidine is administered concurrently.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="89 9" str="increased" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="24 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="C54357" id="I6" precipitant="M10" trigger="M9" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M11" span="104 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="C54357" id="I7" precipitant="M11" trigger="M9" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3405" section="34073-7">
      

      <SentenceText>Although such an interaction has not been reported to date with amoxapine, specific interaction studies have not been done, and the possibility should be considered.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3406" section="34073-7">
      

      <SentenceText>Drugs Metabolized by P450 2D6 – The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the Caucasian population (about 7% to 10% of Caucasians are so called “poor metabolizers”); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African and other populations are not yet available.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="6 23" str="Metabolized by P450 2D6" type="Precipitant"/>
      <Interaction effect="C54358" id="I8" precipitant="M12" trigger="M12" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M13" span="69 65" str="metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase" type="Precipitant"/>
      <Interaction effect="C54356" id="I9" precipitant="M13" trigger="M13" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3407" section="34073-7">
      

      <SentenceText>Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="23 42" str="higher than expected plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M15" span="69 31" str="tricyclic antidepressants (TCAs" type="Precipitant"/>
      <Interaction effect="C54357" id="I10" precipitant="M15" trigger="M14" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3408" section="34073-7">
      

      <SentenceText>Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8 fold increase in plasma AUC of the TCA).</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="133 22" str="increase in plasma AUC" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3409" section="34073-7">
      

      <SentenceText>In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="21 34" str="drugs inhibit the activity of this" type="Precipitant"/>
      <Interaction id="I11" precipitant="M17" trigger="M17" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3410" section="34073-7">
      

      <SentenceText>An individual who is stable on a given dose of TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3411" section="34073-7">
      

      <SentenceText>The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="4 38" str="drugs that inhibit cytochrome P450 2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I12" precipitant="M18" trigger="M18" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M19" span="254 10" str="flecainide" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3412" section="34073-7">
      

      <SentenceText>While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="69 10" str="fluoxetine" type="Precipitant"/>
      <Mention code="NO MAP" id="M21" span="81 10" str="sertraline" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3413" section="34073-7">
      

      <SentenceText>The extent to which SSRI-TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3414" section="34073-7">
      

      <SentenceText>Nevertheless, caution is indicated in the co-administration of TCAs with any of the SSRIs and also in switching from one class to the other.</SentenceText>
      

      <Mention code="NO MAP" id="M22" span="14 20" str="caution is indicated" type="Trigger"/>
      <Mention code="NO MAP" id="M23" span="84 5" str="SSRIs" type="Precipitant"/>
      <Interaction id="I13" precipitant="M23" trigger="M22" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M24" span="121 18" str="class to the other" type="Precipitant"/>
      <Interaction id="I14" precipitant="M24" trigger="M22" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3415" section="34073-7">
      

      <SentenceText>Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3416" section="34073-7">
      

      <SentenceText>Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="105 24" str="lower doses than usually" type="Trigger"/>
      <Mention code="NO MAP" id="M26" span="19 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Mention code="NO MAP" id="M27" span="73 15" str="cytochrome P450" type="Precipitant"/>
      <Interaction effect="C54357" id="I15" precipitant="M27" trigger="M25" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M28" span="89 3" str="2D6" type="Precipitant"/>
      <Mention code="NO MAP" id="M29" span="166 14" str="antidepressant" type="Precipitant"/>
      <Interaction effect="C54357" id="I16" precipitant="M29" trigger="M25" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3417" section="34073-7">
      

      <SentenceText>Furthermore, whenever one of these other drugs is withdrawn from co-therapy, an increased dose of tricyclic antidepressant may be required.</SentenceText>
      

      <Mention code="NO MAP" id="M30" span="98 24" str="tricyclic antidepressant" type="Precipitant"/>
      <Interaction id="I17" precipitant="M30" trigger="M30" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3418" section="34073-7">
      

      <SentenceText>It is desirable to monitor TCA plasma levels whenever a TCA is going to be co-administered with another drug known to be an inhibitor of P450 2D6.</SentenceText>
      

      <Mention code="NO MAP" id="M31" span="137 8" str="P450 2D6" type="Precipitant"/>
      <Interaction id="I18" precipitant="M31" trigger="M31" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3419" section="34090-1">
      

      <SentenceText>Amoxapine is an antidepressant with a mild sedative component to its action.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3420" section="34090-1">
      

      <SentenceText>The mechanism of its clinical action in man is not well understood.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3421" section="34090-1">
      

      <SentenceText>In animals, amoxapine reduced the uptake of norepinephrine and serotonin and blocked the response of dopamine receptors to dopamine.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="22 18" str="reduced the uptake" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3422" section="34090-1">
      

      <SentenceText>Amoxapine is not a monoamine oxidase inhibitor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3423" section="34090-1">
      

      <SentenceText>Amoxapine is absorbed rapidly and reaches peak blood levels approximately 90 minutes after ingestion.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3424" section="34090-1">
      

      <SentenceText>The main route of excretion is the kidney.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3425" section="34090-1">
      

      <SentenceText>In vitro tests show that amoxapine binding to human serum is approximately 90%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3426" section="34090-1">
      

      <SentenceText>In man, amoxapine serum concentration declines with a half-life of eight hours.</SentenceText>
      

      <Mention code="NO MAP" id="M33" span="59 4" str="life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3427" section="34090-1">
      

      <SentenceText>However, the major metabolite, 8-hydroxyamoxapine, has a biologic half-life of 30 hours.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="71 4" str="life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3428" section="34090-1">
      

      <SentenceText>Metabolites are excreted in the urine in conjugated form as glucuronides.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3429" section="34090-1">
      

      <SentenceText>Clinical studies have demonstrated that amoxapine has a more rapid onset of action than either amitriptyline or imipramine.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="61 21" str="rapid onset of action" type="Trigger"/>
      <Mention code="NO MAP" id="M36" span="95 13" str="amitriptyline" type="Precipitant"/>
      <Interaction effect="C54357" id="I19" precipitant="M36" trigger="M35" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M37" span="112 10" str="imipramine" type="Precipitant"/>
      <Interaction effect="C54357" id="I20" precipitant="M37" trigger="M35" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amoxapine" id="3430" section="34090-1">
      

      <SentenceText>The initial clinical effect may occur within four to seven days and occurs within two weeks in over 80% of responders.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

