ABILIFY MAINTENA	2281	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	19
Dosage adjustments for patients taking CYP2D6 inhibitors, CYP3A4 inhibitors, or CYP3A4 inducers for greater than 14 days
1	0	5	B-T	Dosage
2	7	17	I-T	adjustments
3	19	21	O	for
4	23	30	O	patients
5	32	37	O	taking
6	39	44	B-U	CYP2D6
7	46	55	I-U	inhibitors
8	56	56	O	,
9	58	63	B-U	CYP3A4
10	65	74	I-U	inhibitors
11	75	75	O	,
12	77	78	O	or
13	80	85	B-U	CYP3A4
14	87	94	I-U	inducers
15	96	98	O	for
16	100	106	O	greater
17	108	111	O	than
18	113	114	O	14
19	116	119	O	days
NULL

ABILIFY MAINTENA	2282	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	44
Patients Taking 400 mg of ABILIFY MAINTENA Strong CYP2D6 or CYP3A4 inhibitors 300 mg CYP2D6 and CYP3A4 inhibitors 200 mg CYP3A4 inducers Avoid use Patients Taking 300 mg of ABILIFY MAINTENA Strong CYP2D6 or CYP3A4 inhibitors 200 mg CYP2D6 and CYP3A4 inhibitors 160 mg CYP3A4 inducers
1	0	7	O	Patients
2	9	14	O	Taking
3	16	18	O	400
4	20	21	O	mg
5	23	24	O	of
6	26	41	O	XXXXXXXX
7	43	48	O	Strong
8	50	55	B-U	CYP2D6
9	57	58	O	or
10	60	65	B-U	CYP3A4
11	67	76	I-U	inhibitors
12	78	80	O	300
13	82	83	O	mg
14	85	90	O	CYP2D6
15	92	94	O	and
16	96	101	O	CYP3A4
17	103	112	O	inhibitors
18	114	116	O	200
19	118	119	O	mg
20	121	126	O	CYP3A4
21	128	135	O	inducers
22	137	141	B-T	Avoid
23	143	145	O	use
24	147	154	O	Patients
25	156	161	O	Taking
26	163	165	O	300
27	167	168	O	mg
28	170	171	O	of
29	173	188	O	XXXXXXXX
30	190	195	B-U	Strong
31	197	202	I-U	CYP2D6
32	204	205	O	or
33	207	212	B-U	CYP3A4
34	214	223	I-U	inhibitors
35	225	227	O	200
36	229	230	O	mg
37	232	237	O	CYP2D6
38	239	241	O	and
39	243	248	O	CYP3A4
40	250	259	O	inhibitors
41	261	263	O	160
42	265	266	O	mg
43	268	273	O	CYP3A4
44	275	282	O	inducers
NULL

ABILIFY MAINTENA	2283	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	23
With concomitant use of ABILIFY MAINTENA with a strong CYP3A4 inhibitor or CYP2D6 inhibitor for more than 14 days, reduce the ABILIFY MAINTENA dosage.
1	0	3	O	With
2	5	15	O	concomitant
3	17	19	O	use
4	21	22	O	of
5	24	39	O	XXXXXXXX
6	41	44	O	with
7	46	46	O	a
8	48	53	B-K	strong
9	55	60	B-U	CYP3A4
10	62	70	B-K	inhibitor
11	72	73	O	or
12	75	80	B-K	CYP2D6
13	82	90	B-U	inhibitor
14	92	94	O	for
15	96	99	O	more
16	101	104	O	than
17	106	107	O	14
18	109	112	O	days
19	113	113	O	,
20	115	120	O	reduce
21	122	124	O	the
22	126	141	O	XXXXXXXX
23	143	148	O	dosage
K/8:C54355 K/10:C54355 K/12:C54355

ABILIFY MAINTENA	2284	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	19
Strong CYP3A4 Inducers (e.g., carbamazepine) The concomitant use of oral aripiprazole and carbamazepine decreased the exposure of aripiprazole.
1	0	5	B-K	Strong
2	7	12	I-K	CYP3A4
3	14	21	I-K	Inducers
4	24	26	O	e.g
5	28	28	O	,
6	30	42	B-K	carbamazepine
7	45	47	O	The
8	49	59	O	concomitant
9	61	63	O	use
10	65	66	O	of
11	68	71	O	oral
12	73	84	O	XXXXXXXX
13	86	88	O	and
14	90	102	B-K	carbamazepine
15	104	112	B-T	decreased
16	114	116	I-T	the
17	118	125	I-T	exposure
18	127	128	O	of
19	130	141	O	XXXXXXXX
K/1:C54356 K/6:C54356 K/14:C54356

ABILIFY MAINTENA	2285	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	17
Avoid use of ABILIFY MAINTENA in combination with carbamazepine and other inducers of CYP3A4 for greater than 14days.
1	0	4	B-T	Avoid
2	6	8	I-T	use
3	10	11	O	of
4	13	28	O	XXXXXXXX
5	30	31	O	in
6	33	43	O	combination
7	45	48	O	with
8	50	62	B-U	carbamazepine
9	64	66	O	and
10	68	72	O	other
11	74	81	B-U	inducers
12	83	84	I-U	of
13	86	91	I-U	CYP3A4
14	93	95	I-U	for
15	97	103	I-U	greater
16	105	108	I-U	than
17	110	115	I-U	14days
NULL

ABILIFY MAINTENA	2286	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	21
Antihypertensive Drugs Due to its alpha adrenergic antagonism, aripiprazole has the potential to enhance the effect of certain antihypertensive agents.
1	0	15	B-D	Antihypertensive
2	17	21	I-D	Drugs
3	23	25	O	Due
4	27	28	O	to
5	30	32	O	its
6	34	38	O	alpha
7	40	49	O	adrenergic
8	51	60	O	antagonism
9	61	61	O	,
10	63	74	O	XXXXXXXX
11	76	78	O	has
12	80	82	O	the
13	84	92	O	potential
14	94	95	O	to
15	97	103	B-E	enhance
16	105	107	I-E	the
17	109	114	I-E	effect
18	116	117	I-E	of
19	119	125	O	certain
20	127	142	B-D	antihypertensive
21	144	149	I-D	agents
D/1:15:1 D/20:15:1

ABILIFY MAINTENA	2287	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	7
Monitor blood pressure and adjust dose accordingly.
1	0	6	B-T	Monitor
2	8	12	O	blood
3	14	21	O	pressure
4	23	25	O	and
5	27	32	O	adjust
6	34	37	O	dose
7	39	49	O	accordingly
NULL

ABILIFY MAINTENA	2288	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	26
Benzodiazepines (e.g., lorazepam) The intensity of sedation was greater with the combination of oral aripiprazole and lorazepam as compared to that observed with aripiprazole alone.
1	0	14	O	Benzodiazepines
2	17	19	O	e.g
3	21	21	O	,
4	23	31	O	lorazepam
5	34	36	O	The
6	38	46	O	intensity
7	48	49	O	of
8	51	58	B-E	sedation
9	60	62	O	was
10	64	70	O	greater
11	72	75	O	with
12	77	79	O	the
13	81	91	O	combination
14	93	94	O	of
15	96	99	O	oral
16	101	112	O	XXXXXXXX
17	114	116	O	and
18	118	126	O	lorazepam
19	128	129	O	as
20	131	138	O	compared
21	140	141	O	to
22	143	146	O	that
23	148	155	O	observed
24	157	160	O	with
25	162	173	O	XXXXXXXX
26	175	179	O	alone
NULL

ABILIFY MAINTENA	2289	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	17
The orthostatic hypotension observed was greater with the combination as compared to that observed with lorazepam alone.
1	0	2	O	The
2	4	14	B-E	orthostatic
3	16	26	I-E	hypotension
4	28	35	I-E	observed
5	37	39	O	was
6	41	47	O	greater
7	49	52	O	with
8	54	56	O	the
9	58	68	O	combination
10	70	71	O	as
11	73	80	O	compared
12	82	83	O	to
13	85	88	O	that
14	90	97	O	observed
15	99	102	O	with
16	104	112	O	lorazepam
17	114	118	O	alone
NULL

ABILIFY MAINTENA	2290	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	26
Based on pharmacokinetic studies with oral aripiprazole, no dosage adjustment of ABILIFY MAINTENA is required when administered concomitantly with famotidine, valproate, lithium, lorazepam.
1	0	4	O	Based
2	6	7	O	on
3	9	23	O	pharmacokinetic
4	25	31	O	studies
5	33	36	O	with
6	38	41	O	oral
7	43	54	O	XXXXXXXX
8	55	55	O	,
9	57	58	O	no
10	60	65	O	dosage
11	67	76	O	adjustment
12	78	79	O	of
13	81	96	O	XXXXXXXX
14	98	99	O	is
15	101	108	O	required
16	110	113	O	when
17	115	126	O	administered
18	128	140	O	concomitantly
19	142	145	O	with
20	147	156	O	famotidine
21	157	157	O	,
22	159	167	O	valproate
23	168	168	O	,
24	170	176	O	lithium
25	177	177	O	,
26	179	187	O	lorazepam
NULL

ABILIFY MAINTENA	2291	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	45
In addition, no dosage adjustment is necessary for substrates of CYP2D6 (e.g., dextromethorphan, fluoxetine, paroxetine, or venlafaxine), CYP2C9 (e.g., warfarin), CYP2C19 (e.g., omeprazole, warfarin), or CYP3A4 (e.g., dextromethorphan) when co-administered with ABILIFY MAINTENA.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	14	O	no
5	16	21	O	dosage
6	23	32	O	adjustment
7	34	35	O	is
8	37	45	O	necessary
9	47	49	O	for
10	51	60	O	substrates
11	62	63	O	of
12	65	70	O	CYP2D6
13	73	75	O	e.g
14	77	77	O	,
15	79	94	O	dextromethorphan
16	95	95	O	,
17	97	106	O	fluoxetine
18	107	107	O	,
19	109	118	O	paroxetine
20	119	119	O	,
21	121	122	O	or
22	124	134	O	venlafaxine
23	136	136	O	,
24	138	143	O	CYP2C9
25	146	148	O	e.g
26	150	150	O	,
27	152	159	O	warfarin
28	161	161	O	,
29	163	169	O	CYP2C19
30	172	174	O	e.g
31	176	176	O	,
32	178	187	O	omeprazole
33	188	188	O	,
34	190	197	O	warfarin
35	199	199	O	,
36	201	202	O	or
37	204	209	O	CYP3A4
38	212	214	O	e.g
39	216	216	O	,
40	218	233	O	dextromethorphan
41	236	239	O	when
42	241	242	O	co
43	244	255	O	administered
44	257	260	O	with
45	262	277	O	XXXXXXXX
NULL

ABILIFY MAINTENA	2292	34073-7	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	23
Additionally, no dosage adjustment is necessary for valproate, lithium, lamotrigine, lorazepam, or sertraline when co-administered with ABILIFY MAINTENA.
1	0	11	O	Additionally
2	12	12	O	,
3	14	15	O	no
4	17	22	O	dosage
5	24	33	O	adjustment
6	35	36	O	is
7	38	46	O	necessary
8	48	50	O	for
9	52	60	O	valproate
10	61	61	O	,
11	63	69	O	lithium
12	70	70	O	,
13	72	82	O	lamotrigine
14	83	83	O	,
15	85	93	O	lorazepam
16	94	94	O	,
17	96	97	O	or
18	99	108	O	sertraline
19	110	113	O	when
20	115	116	O	co
21	118	129	O	administered
22	131	134	O	with
23	136	151	O	XXXXXXXX
NULL

ABILIFY MAINTENA	2293	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	17
The mechanism of action of aripiprazole in the treatment of schizophrenia and bipolar I disorder is unknown.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	22	O	action
5	24	25	O	of
6	27	38	O	XXXXXXXX
7	40	41	O	in
8	43	45	O	the
9	47	55	O	treatment
10	57	58	O	of
11	60	72	O	schizophrenia
12	74	76	O	and
13	78	84	O	bipolar
14	86	86	O	I
15	88	95	O	disorder
16	97	98	O	is
17	100	106	O	unknown
NULL

ABILIFY MAINTENA	2294	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	29
The efficacy of aripiprazole could be mediated through a combination of partial agonist activity at dopamine D2 and serotonin 5-HT1A receptors and antagonist activity at 5-HT2A receptors.
1	0	2	O	The
2	4	11	O	efficacy
3	13	14	O	of
4	16	27	O	XXXXXXXX
5	29	33	O	could
6	35	36	O	be
7	38	45	O	mediated
8	47	53	O	through
9	55	55	O	a
10	57	67	O	combination
11	69	70	O	of
12	72	78	O	partial
13	80	86	O	agonist
14	88	95	O	activity
15	97	98	O	at
16	100	107	B-D	dopamine
17	109	110	I-D	D2
18	112	114	O	and
19	116	124	B-D	serotonin
20	126	126	O	5
21	128	131	O	HT1A
22	133	141	O	receptors
23	143	145	O	and
24	147	156	B-E	antagonist
25	158	165	I-E	activity
26	167	168	O	at
27	170	170	O	5
28	172	175	O	HT2A
29	177	185	O	receptors
D/16:24:1 D/19:24:1

ABILIFY MAINTENA	2295	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	79
Aripiprazole exhibits high affinity for dopamine D2 and D3 (Kis 0.34 and 0.8 nM respectively), serotonin 5-HT1A and 5-HT2A receptors (Kis 1.7 and 3.4 nM respectively), moderate affinity for dopamine D4, serotonin 5-HT2C and 5-HT7, alpha1-adrenergic and histamine H1 receptors (Kis 44 nM, 15 nM, 39 nM, 57 nM, and 61 nM, respectively), and moderate affinity for the serotonin reuptake site (Ki 98 nM).
1	0	11	O	XXXXXXXX
2	13	20	O	exhibits
3	22	25	O	high
4	27	34	O	affinity
5	36	38	O	for
6	40	47	B-K	dopamine
7	49	50	O	D2
8	52	54	O	and
9	56	57	O	D3
10	60	62	O	Kis
11	64	67	O	0.34
12	69	71	O	and
13	73	75	O	0.8
14	77	78	O	nM
15	80	91	O	respectively
16	93	93	O	,
17	95	103	O	serotonin
18	105	105	O	5
19	107	110	O	HT1A
20	112	114	O	and
21	116	116	O	5
22	118	121	O	HT2A
23	123	131	O	receptors
24	134	136	O	Kis
25	138	140	O	1.7
26	142	144	O	and
27	146	148	O	3.4
28	150	151	O	nM
29	153	164	O	respectively
30	166	166	O	,
31	168	175	O	moderate
32	177	184	O	affinity
33	186	188	O	for
34	190	197	O	dopamine
35	199	200	O	D4
36	201	201	O	,
37	203	211	O	serotonin
38	213	213	O	5
39	215	218	O	HT2C
40	220	222	O	and
41	224	224	O	5
42	226	228	O	HT7
43	229	229	O	,
44	231	236	O	alpha1
45	238	247	O	adrenergic
46	249	251	O	and
47	253	261	O	histamine
48	263	264	O	H1
49	266	274	O	receptors
50	277	279	O	Kis
51	281	282	O	44
52	284	285	O	nM
53	286	286	O	,
54	288	289	O	15
55	291	292	O	nM
56	293	293	O	,
57	295	296	O	39
58	298	299	O	nM
59	300	300	O	,
60	302	303	O	57
61	305	306	O	nM
62	307	307	O	,
63	309	311	O	and
64	313	314	O	61
65	316	317	O	nM
66	318	318	O	,
67	320	331	O	respectively
68	333	333	O	,
69	335	337	O	and
70	339	346	O	moderate
71	348	355	O	affinity
72	357	359	O	for
73	361	363	O	the
74	365	373	O	serotonin
75	375	382	O	reuptake
76	384	387	O	site
77	390	391	O	Ki
78	393	394	O	98
79	396	397	O	nM
K/6:C54357

ABILIFY MAINTENA	2296	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	10
Aripiprazole has no appreciable affinity for cholinergic muscarinic receptors (IC50>1000nM).
1	0	11	O	XXXXXXXX
2	13	15	O	has
3	17	18	O	no
4	20	30	O	appreciable
5	32	39	O	affinity
6	41	43	O	for
7	45	55	O	cholinergic
8	57	66	O	muscarinic
9	68	76	O	receptors
10	79	89	O	IC50>1000nM
NULL

ABILIFY MAINTENA	2297	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	42
Actions at receptors other than D2, 5-HT1A, and 5-HT2A may explain some of the other adverse reactions of aripiprazole (e.g., the orthostatic hypotension observed with aripiprazole may be explained by its antagonist activity at adrenergic alpha1 receptors).
1	0	6	B-D	Actions
2	8	9	O	at
3	11	19	O	receptors
4	21	25	O	other
5	27	30	O	than
6	32	33	O	D2
7	34	34	O	,
8	36	36	O	5
9	38	41	O	HT1A
10	42	42	O	,
11	44	46	O	and
12	48	48	O	5
13	50	53	O	HT2A
14	55	57	O	may
15	59	65	O	explain
16	67	70	O	some
17	72	73	O	of
18	75	77	O	the
19	79	83	O	other
20	85	91	O	adverse
21	93	101	O	reactions
22	103	104	O	of
23	106	117	O	XXXXXXXX
24	120	122	O	e.g
25	124	124	O	,
26	126	128	O	the
27	130	140	O	orthostatic
28	142	152	O	hypotension
29	154	161	O	observed
30	163	166	O	with
31	168	179	O	XXXXXXXX
32	181	183	O	may
33	185	186	O	be
34	188	196	O	explained
35	198	199	O	by
36	201	203	O	its
37	205	214	O	antagonist
38	216	223	O	activity
39	225	226	O	at
40	228	237	O	adrenergic
41	239	244	O	alpha1
42	246	254	O	receptors
NULL

ABILIFY MAINTENA	2298	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	31
Alcohol There was no significant difference between oral aripiprazole co-administered with ethanol and placebo co-administered with ethanol on performance of gross motor skills or stimulus response in healthy subjects.
1	0	6	O	Alcohol
2	8	12	O	There
3	14	16	O	was
4	18	19	O	no
5	21	31	O	significant
6	33	42	O	difference
7	44	50	O	between
8	52	55	O	oral
9	57	68	O	XXXXXXXX
10	70	71	O	co
11	73	84	O	administered
12	86	89	O	with
13	91	97	O	ethanol
14	99	101	O	and
15	103	109	O	placebo
16	111	112	O	co
17	114	125	O	administered
18	127	130	O	with
19	132	138	B-D	ethanol
20	140	141	O	on
21	143	153	O	performance
22	155	156	O	of
23	158	162	B-E	gross
24	164	168	B-E	motor
25	170	175	I-E	skills
26	177	178	I-E	or
27	180	187	I-E	stimulus
28	189	196	I-E	response
29	198	199	O	in
30	201	207	O	healthy
31	209	216	O	subjects
D/19:23:1 D/19:24:1

ABILIFY MAINTENA	2299	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	16
As with most psychoactive medications, patients should be advised to avoid alcohol while taking ABILIFY MAINTENA.
1	0	1	O	As
2	3	6	O	with
3	8	11	O	most
4	13	24	O	psychoactive
5	26	36	O	medications
6	37	37	O	,
7	39	46	O	patients
8	48	53	O	should
9	55	56	O	be
10	58	64	O	advised
11	66	67	O	to
12	69	73	O	avoid
13	75	81	B-U	alcohol
14	83	87	O	while
15	89	94	O	taking
16	96	111	O	XXXXXXXX
NULL

ABILIFY MAINTENA	2300	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	53
ABILIFY MAINTENA activity is presumably primarily due to the parent drug, aripiprazole, and to a lesser extent, to its major metabolite, dehydro-aripiprazole, which has been shown to have affinities for D2 receptors similar to the parent drug and represents about 29% of the parent drug exposure in plasma.
1	0	15	O	XXXXXXXX
2	17	24	O	activity
3	26	27	O	is
4	29	38	O	presumably
5	40	48	O	primarily
6	50	52	O	due
7	54	55	O	to
8	57	59	O	the
9	61	66	O	parent
10	68	71	O	drug
11	72	72	O	,
12	74	85	O	XXXXXXXX
13	86	86	O	,
14	88	90	O	and
15	92	93	O	to
16	95	95	O	a
17	97	102	O	lesser
18	104	109	O	extent
19	110	110	O	,
20	112	113	O	to
21	115	117	O	its
22	119	123	O	major
23	125	134	O	metabolite
24	135	135	O	,
25	137	143	O	dehydro
26	145	156	O	XXXXXXXX
27	157	157	O	,
28	159	163	O	which
29	165	167	O	has
30	169	172	O	been
31	174	178	O	shown
32	180	181	O	to
33	183	186	O	have
34	188	197	O	affinities
35	199	201	O	for
36	203	204	O	D2
37	206	214	O	receptors
38	216	222	O	similar
39	224	225	O	to
40	227	229	O	the
41	231	236	O	parent
42	238	241	O	drug
43	243	245	O	and
44	247	256	O	represents
45	258	262	O	about
46	264	266	O	29%
47	268	269	O	of
48	271	273	O	the
49	275	280	O	parent
50	282	285	O	drug
51	287	294	O	exposure
52	296	297	O	in
53	299	304	O	plasma
NULL

ABILIFY MAINTENA	2301	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	20
Aripiprazole absorption into the systemic circulation is slow and prolonged following intramuscular injection due to low solubility of aripiprazole particles.
1	0	11	O	XXXXXXXX
2	13	22	O	absorption
3	24	27	O	into
4	29	31	O	the
5	33	40	O	systemic
6	42	52	O	circulation
7	54	55	O	is
8	57	60	O	slow
9	62	64	O	and
10	66	74	O	prolonged
11	76	84	O	following
12	86	98	O	intramuscular
13	100	108	O	injection
14	110	112	O	due
15	114	115	O	to
16	117	119	O	low
17	121	130	O	solubility
18	132	133	O	of
19	135	146	O	XXXXXXXX
20	148	156	O	particles
NULL

ABILIFY MAINTENA	2302	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	49
Following a single dose administration of ABILIFY MAINTENA in the deltoid and gluteal muscle, the extent of absorption (AUCt, AUC*) of aripiprazole was similar for both injection sites, but the rate of absorption (Cmax) was 31% higher following administration to the deltoid compared to the gluteal site.
1	0	8	O	Following
2	10	10	O	a
3	12	17	O	single
4	19	22	O	dose
5	24	37	O	administration
6	39	40	O	of
7	42	57	O	XXXXXXXX
8	59	60	O	in
9	62	64	O	the
10	66	72	O	deltoid
11	74	76	O	and
12	78	84	O	gluteal
13	86	91	O	muscle
14	92	92	O	,
15	94	96	O	the
16	98	103	O	extent
17	105	106	O	of
18	108	117	O	absorption
19	120	123	O	AUCt
20	124	124	O	,
21	126	129	O	AUC*
22	132	133	O	of
23	135	146	O	XXXXXXXX
24	148	150	O	was
25	152	158	O	similar
26	160	162	O	for
27	164	167	O	both
28	169	177	O	injection
29	179	183	O	sites
30	184	184	O	,
31	186	188	O	but
32	190	192	O	the
33	194	197	O	rate
34	199	200	B-T	of
35	202	211	I-T	absorption
36	214	217	I-T	Cmax
37	220	222	O	was
38	224	226	O	31%
39	228	233	O	higher
40	235	243	O	following
41	245	258	O	administration
42	260	261	O	to
43	263	265	O	the
44	267	273	O	deltoid
45	275	282	O	compared
46	284	285	O	to
47	287	289	O	the
48	291	297	O	gluteal
49	299	302	O	site
NULL

ABILIFY MAINTENA	2303	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	16
However, at steady state, AUC and Cmax were similar for both sites of injection.
1	0	6	O	However
2	7	7	O	,
3	9	10	O	at
4	12	17	O	steady
5	19	23	O	state
6	24	24	O	,
7	26	28	O	AUC
8	30	32	O	and
9	34	37	O	Cmax
10	39	42	O	were
11	44	50	O	similar
12	52	54	O	for
13	56	59	O	both
14	61	65	O	sites
15	67	68	O	of
16	70	78	O	injection
NULL

ABILIFY MAINTENA	2304	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	35
Following multiple intramuscular doses, the plasma concentrations of aripiprazole gradually rise to maximum plasma concentrations at a median Tmax of 5 - 7 days for the gluteal muscle and 4 days for the deltoid muscle.
1	0	8	O	Following
2	10	17	O	multiple
3	19	31	O	intramuscular
4	33	37	O	doses
5	38	38	O	,
6	40	42	O	the
7	44	49	O	plasma
8	51	64	O	concentrations
9	66	67	O	of
10	69	80	O	XXXXXXXX
11	82	90	O	gradually
12	92	95	O	rise
13	97	98	O	to
14	100	106	O	maximum
15	108	113	O	plasma
16	115	128	O	concentrations
17	130	131	O	at
18	133	133	O	a
19	135	140	O	median
20	142	145	O	Tmax
21	147	148	O	of
22	150	150	O	5
23	154	154	O	7
24	156	159	O	days
25	161	163	O	for
26	165	167	O	the
27	169	175	O	gluteal
28	177	182	O	muscle
29	184	186	O	and
30	188	188	O	4
31	190	193	O	days
32	195	197	O	for
33	199	201	O	the
34	203	209	O	deltoid
35	211	216	O	muscle
NULL

ABILIFY MAINTENA	2305	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	35
After gluteal administration, the mean apparent aripiprazole terminal elimination half-life was 29.9 days and 46.5 days after multiple injections for every 4-week injection of ABILIFY MAINTENA 300 mg and 400 mg, respectively.
1	0	4	O	After
2	6	12	O	gluteal
3	14	27	O	administration
4	28	28	O	,
5	30	32	O	the
6	34	37	O	mean
7	39	46	O	apparent
8	48	59	O	XXXXXXXX
9	61	68	O	terminal
10	70	80	O	elimination
11	82	85	O	half
12	87	90	O	life
13	92	94	O	was
14	96	99	O	29.9
15	101	104	O	days
16	106	108	O	and
17	110	113	O	46.5
18	115	118	O	days
19	120	124	O	after
20	126	133	O	multiple
21	135	144	O	injections
22	146	148	O	for
23	150	154	O	every
24	156	156	O	4
25	158	161	O	week
26	163	171	O	injection
27	173	174	O	of
28	176	191	O	XXXXXXXX
29	193	195	O	300
30	197	198	O	mg
31	200	202	O	and
32	204	206	O	400
33	208	209	O	mg
34	210	210	O	,
35	212	223	O	respectively
NULL

ABILIFY MAINTENA	2306	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	18
Steady state concentrations for the typical subject were attained by the fourth dose for both sites of administration.
1	0	5	O	Steady
2	7	11	O	state
3	13	26	O	concentrations
4	28	30	O	for
5	32	34	O	the
6	36	42	O	typical
7	44	50	O	subject
8	52	55	O	were
9	57	64	O	attained
10	66	67	O	by
11	69	71	O	the
12	73	78	O	fourth
13	80	83	O	dose
14	85	87	O	for
15	89	92	O	both
16	94	98	O	sites
17	100	101	O	of
18	103	116	O	administration
NULL

ABILIFY MAINTENA	2307	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	24
Approximate dose-proportional increases in aripiprazole and dehydro-aripiprazole exposure were observed after every four week ABILIFY MAINTENA injections of 300 mg and 400 mg.
1	0	10	O	Approximate
2	12	15	O	dose
3	17	28	O	proportional
4	30	38	O	increases
5	40	41	O	in
6	43	54	O	XXXXXXXX
7	56	58	O	and
8	60	66	O	dehydro
9	68	79	O	XXXXXXXX
10	81	88	O	exposure
11	90	93	O	were
12	95	102	O	observed
13	104	108	O	after
14	110	114	O	every
15	116	119	O	four
16	121	124	O	week
17	126	141	O	XXXXXXXX
18	143	152	O	injections
19	154	155	O	of
20	157	159	O	300
21	161	162	O	mg
22	164	166	O	and
23	168	170	O	400
24	172	173	O	mg
NULL

ABILIFY MAINTENA	2308	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	16
Elimination of aripiprazole is mainly through hepatic metabolism involving two P450 isozymes, CYP2D6 and CYP3A4.
1	0	10	O	Elimination
2	12	13	O	of
3	15	26	O	XXXXXXXX
4	28	29	O	is
5	31	36	O	mainly
6	38	44	O	through
7	46	52	O	hepatic
8	54	63	O	metabolism
9	65	73	O	involving
10	75	77	O	two
11	79	82	O	P450
12	84	91	O	isozymes
13	92	92	O	,
14	94	99	O	CYP2D6
15	101	103	O	and
16	105	110	O	CYP3A4
NULL

ABILIFY MAINTENA	2309	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	23
Aripiprazole is not a substrate of CYP1A1, CYP1A2, CYP2A6, CYP2B6, CYP2C8, CYP2C9, CYP2C19, or CYP2E1 enzymes.
1	0	11	O	XXXXXXXX
2	13	14	O	is
3	16	18	O	not
4	20	20	O	a
5	22	30	O	substrate
6	32	33	O	of
7	35	40	O	CYP1A1
8	41	41	O	,
9	43	48	O	CYP1A2
10	49	49	O	,
11	51	56	O	CYP2A6
12	57	57	O	,
13	59	64	O	CYP2B6
14	65	65	O	,
15	67	72	O	CYP2C8
16	73	73	O	,
17	75	80	O	CYP2C9
18	81	81	O	,
19	83	89	O	CYP2C19
20	90	90	O	,
21	92	93	O	or
22	95	100	O	CYP2E1
23	102	108	O	enzymes
NULL

ABILIFY MAINTENA	2310	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	7
Aripiprazole also does not undergo direct glucuronidation.
1	0	11	O	XXXXXXXX
2	13	16	O	also
3	18	21	O	does
4	23	25	O	not
5	27	33	O	undergo
6	35	40	O	direct
7	42	56	O	glucuronidation
NULL

ABILIFY MAINTENA	2311	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	13
Drug Interaction Studies No specific drug interaction studies have been performed with ABILIFY MAINTENA.
1	0	3	O	Drug
2	5	15	O	Interaction
3	17	23	O	Studies
4	25	26	O	No
5	28	35	O	specific
6	37	40	O	drug
7	42	52	O	interaction
8	54	60	O	studies
9	62	65	O	have
10	67	70	O	been
11	72	80	O	performed
12	82	85	O	with
13	87	102	O	XXXXXXXX
NULL

ABILIFY MAINTENA	2312	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	10
The information below is obtained from studies with oral aripiprazole.
1	0	2	O	The
2	4	14	O	information
3	16	20	O	below
4	22	23	O	is
5	25	32	O	obtained
6	34	37	O	from
7	39	45	O	studies
8	47	50	O	with
9	52	55	O	oral
10	57	68	O	XXXXXXXX
NULL

ABILIFY MAINTENA	2313	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	22
Effects of other drugs on the exposures of aripiprazole and dehydro-aripiprazole are summarized in Figure 19 and Figure 20, respectively.
1	0	6	O	Effects
2	8	9	O	of
3	11	15	O	other
4	17	21	O	drugs
5	23	24	O	on
6	26	28	O	the
7	30	38	O	exposures
8	40	41	O	of
9	43	54	O	XXXXXXXX
10	56	58	O	and
11	60	66	O	dehydro
12	68	79	O	XXXXXXXX
13	81	83	O	are
14	85	94	O	summarized
15	96	97	O	in
16	99	104	O	Figure
17	106	107	O	19
18	109	111	O	and
19	113	118	O	Figure
20	120	121	O	20
21	122	122	O	,
22	124	135	O	respectively
NULL

ABILIFY MAINTENA	2314	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	33
Based on simulation, a 4.5-fold increase in mean Cmax and AUC values at steady-state is expected when extensive metabolizers of CYP2D6 are administered with both strong CYP2D6 and CYP3A4 inhibitors.
1	0	4	O	Based
2	6	7	O	on
3	9	18	O	simulation
4	19	19	O	,
5	21	21	O	a
6	23	25	O	4.5
7	27	30	O	fold
8	32	39	O	increase
9	41	42	O	in
10	44	47	O	mean
11	49	52	O	Cmax
12	54	56	O	and
13	58	60	O	AUC
14	62	67	O	values
15	69	70	O	at
16	72	77	O	steady
17	79	83	O	state
18	85	86	O	is
19	88	95	O	expected
20	97	100	O	when
21	102	110	O	extensive
22	112	123	O	metabolizers
23	125	126	O	of
24	128	133	O	CYP2D6
25	135	137	O	are
26	139	150	O	administered
27	152	155	O	with
28	157	160	O	both
29	162	167	O	strong
30	169	174	B-K	CYP2D6
31	176	178	O	and
32	180	185	B-K	CYP3A4
33	187	196	I-K	inhibitors
K/30:C54355 K/32:C54355

ABILIFY MAINTENA	2315	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	29
After oral administration, a 3-fold increase in mean Cmax and AUC values at steady-state is expected in poor metabolizers of CYP2D6 administered with strong CYP3A4 inhibitors.
1	0	4	O	After
2	6	9	O	oral
3	11	24	O	administration
4	25	25	O	,
5	27	27	O	a
6	29	29	O	3
7	31	34	O	fold
8	36	43	O	increase
9	45	46	O	in
10	48	51	O	mean
11	53	56	O	Cmax
12	58	60	O	and
13	62	64	O	AUC
14	66	71	O	values
15	73	74	O	at
16	76	81	O	steady
17	83	87	O	state
18	89	90	O	is
19	92	99	O	expected
20	101	102	O	in
21	104	107	O	poor
22	109	120	O	metabolizers
23	122	123	O	of
24	125	130	O	CYP2D6
25	132	143	O	administered
26	145	148	O	with
27	150	155	B-K	strong
28	157	162	I-K	CYP3A4
29	164	173	I-K	inhibitors
K/27:C54355

ABILIFY MAINTENA	2316	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	38
Figure 19: The effects of other drugs on aripiprazole pharmacokinetics Figure 20: The effects of other drugs on dehydro-aripiprazole pharmacokinetics The effects of ABILIFY on the exposures of other drugs are summarized in Figure 21.
1	0	5	O	Figure
2	7	8	O	19
3	9	9	O	:
4	11	13	O	The
5	15	21	O	effects
6	23	24	O	of
7	26	30	O	other
8	32	36	O	drugs
9	38	39	O	on
10	41	52	O	XXXXXXXX
11	54	69	O	pharmacokinetics
12	71	76	O	Figure
13	78	79	O	20
14	80	80	O	:
15	82	84	O	The
16	86	92	O	effects
17	94	95	O	of
18	97	101	O	other
19	103	107	O	drugs
20	109	110	O	on
21	112	118	O	dehydro
22	120	131	O	XXXXXXXX
23	133	148	O	pharmacokinetics
24	150	152	O	The
25	154	160	O	effects
26	162	163	O	of
27	165	171	O	ABILIFY
28	173	174	O	on
29	176	178	O	the
30	180	188	O	exposures
31	190	191	O	of
32	193	197	O	other
33	199	203	O	drugs
34	205	207	O	are
35	209	218	O	summarized
36	220	221	O	in
37	223	228	O	Figure
38	230	231	O	21
NULL

ABILIFY MAINTENA	2317	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	56
A population PK analysis in patients with major depressive disorder showed no substantial change in plasma concentrations of fluoxetine (20 mg/day or 40 mg/day), paroxetine CR (37.5 mg/day or 50 mg/day), or sertraline (100 mg/day or 150 mg/day) dosed to steady-state.
1	0	0	O	A
2	2	11	O	population
3	13	14	O	PK
4	16	23	O	analysis
5	25	26	O	in
6	28	35	O	patients
7	37	40	O	with
8	42	46	O	major
9	48	57	O	depressive
10	59	66	O	disorder
11	68	73	O	showed
12	75	76	O	no
13	78	88	O	substantial
14	90	95	O	change
15	97	98	O	in
16	100	105	O	plasma
17	107	120	O	concentrations
18	122	123	O	of
19	125	134	O	fluoxetine
20	137	138	O	20
21	140	141	O	mg
22	142	142	O	/
23	143	145	O	day
24	147	148	O	or
25	150	151	O	40
26	153	154	O	mg
27	155	155	O	/
28	156	158	O	day
29	160	160	O	,
30	162	171	O	paroxetine
31	173	174	O	CR
32	177	180	O	37.5
33	182	183	O	mg
34	184	184	O	/
35	185	187	O	day
36	189	190	O	or
37	192	193	O	50
38	195	196	O	mg
39	197	197	O	/
40	198	200	O	day
41	202	202	O	,
42	204	205	O	or
43	207	216	O	sertraline
44	219	221	O	100
45	223	224	O	mg
46	225	225	O	/
47	226	228	O	day
48	230	231	O	or
49	233	235	O	150
50	237	238	O	mg
51	239	239	O	/
52	240	242	O	day
53	245	249	O	dosed
54	251	252	O	to
55	254	259	O	steady
56	261	265	O	state
NULL

ABILIFY MAINTENA	2318	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	26
The steady-state plasma concentrations of fluoxetine and norfluoxetine increased by about 18% and 36%, respectively, and concentrations of paroxetine decreased by about 27%.
1	0	2	O	The
2	4	9	O	steady
3	11	15	O	state
4	17	22	O	plasma
5	24	37	O	concentrations
6	39	40	O	of
7	42	51	B-K	fluoxetine
8	53	55	O	and
9	57	69	B-K	norfluoxetine
10	71	79	O	increased
11	81	82	O	by
12	84	88	O	about
13	90	92	O	18%
14	94	96	O	and
15	98	100	O	36%
16	101	101	O	,
17	103	114	O	respectively
18	115	115	O	,
19	117	119	O	and
20	121	134	O	concentrations
21	136	137	O	of
22	139	148	O	paroxetine
23	150	158	O	decreased
24	160	161	O	by
25	163	167	O	about
26	169	171	O	27%
K/7:C54357 K/9:C54357

ABILIFY MAINTENA	2319	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	21
The steady-state plasma concentrations of sertraline and desmethylsertraline were not substantially changed when these antidepressant therapies were coadministered with aripiprazole.
1	0	2	O	The
2	4	9	O	steady
3	11	15	O	state
4	17	22	O	plasma
5	24	37	O	concentrations
6	39	40	O	of
7	42	51	B-K	sertraline
8	53	55	O	and
9	57	75	O	desmethylsertraline
10	77	80	O	were
11	82	84	O	not
12	86	98	O	substantially
13	100	106	O	changed
14	108	111	O	when
15	113	117	O	these
16	119	132	O	antidepressant
17	134	142	O	therapies
18	144	147	O	were
19	149	162	O	coadministered
20	164	167	O	with
21	169	180	O	XXXXXXXX
K/7:C54358

ABILIFY MAINTENA	2320	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	34
Figure 21: The effects of oral aripiprazole on pharmacokinetics of other drugs Figure19 Figure 20 Figure 21 Studies in Specific Populations No specific pharmacokinetic studies have been performed with ABILIFY MAINTENA in specific populations.
1	0	5	O	Figure
2	7	8	O	21
3	9	9	O	:
4	11	13	O	The
5	15	21	O	effects
6	23	24	O	of
7	26	29	O	oral
8	31	42	O	XXXXXXXX
9	44	45	O	on
10	47	62	O	pharmacokinetics
11	64	65	O	of
12	67	71	O	other
13	73	77	O	drugs
14	79	86	O	Figure19
15	88	93	O	Figure
16	95	96	O	20
17	98	103	O	Figure
18	105	106	O	21
19	108	114	O	Studies
20	116	117	O	in
21	119	126	O	Specific
22	128	138	O	Populations
23	140	141	O	No
24	143	150	O	specific
25	152	166	O	pharmacokinetic
26	168	174	O	studies
27	176	179	O	have
28	181	184	O	been
29	186	194	O	performed
30	196	199	O	with
31	201	216	O	XXXXXXXX
32	218	219	O	in
33	221	228	O	specific
34	230	240	O	populations
NULL

ABILIFY MAINTENA	2321	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	10
All the information is obtained from studies with oral aripiprazole.
1	0	2	O	All
2	4	6	O	the
3	8	18	O	information
4	20	21	O	is
5	23	30	O	obtained
6	32	35	O	from
7	37	43	O	studies
8	45	48	O	with
9	50	53	O	oral
10	55	66	O	XXXXXXXX
NULL

ABILIFY MAINTENA	2322	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	19
Exposures of aripiprazole and dehydro-aripiprazole in specific populations are summarized in Figure 22 and Figure 23, respectively.
1	0	8	O	Exposures
2	10	11	O	of
3	13	24	O	XXXXXXXX
4	26	28	O	and
5	30	36	O	dehydro
6	38	49	O	XXXXXXXX
7	51	52	O	in
8	54	61	O	specific
9	63	73	O	populations
10	75	77	O	are
11	79	88	O	summarized
12	90	91	O	in
13	93	98	O	Figure
14	100	101	O	22
15	103	105	O	and
16	107	112	O	Figure
17	114	115	O	23
18	116	116	O	,
19	118	129	O	respectively
NULL

ABILIFY MAINTENA	2323	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	33
In addition, in pediatric patients (10 to 17 years of age) administered with oral aripiprazole (20 mg to 30 mg), the body weight corrected aripiprazole clearance was similar to the adults.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	14	O	in
5	16	24	O	pediatric
6	26	33	O	patients
7	36	37	O	10
8	39	40	O	to
9	42	43	O	17
10	45	49	O	years
11	51	52	O	of
12	54	56	O	age
13	59	70	O	administered
14	72	75	O	with
15	77	80	O	oral
16	82	93	O	XXXXXXXX
17	96	97	O	20
18	99	100	O	mg
19	102	103	O	to
20	105	106	O	30
21	108	109	O	mg
22	111	111	O	,
23	113	115	O	the
24	117	120	O	body
25	122	127	O	weight
26	129	137	O	corrected
27	139	150	O	XXXXXXXX
28	152	160	O	clearance
29	162	164	O	was
30	166	172	O	similar
31	174	175	O	to
32	177	179	O	the
33	181	186	O	adults
NULL

ABILIFY MAINTENA	2324	34090-1	ee49f3b1-1650-47ff-9fb1-ea53fe0b92b6	25
Figure 22 Effects of intrinsic factors on aripiprazole pharmacokinetics Figure 23: Effects of intrinsic factors on dehydro-aripiprazole pharmacokinetics: Figure 22 Figure 23
1	0	5	O	Figure
2	7	8	O	22
3	10	16	O	Effects
4	18	19	O	of
5	21	29	O	intrinsic
6	31	37	O	factors
7	39	40	O	on
8	42	53	O	XXXXXXXX
9	55	70	O	pharmacokinetics
10	72	77	O	Figure
11	79	80	O	23
12	81	81	O	:
13	83	89	O	Effects
14	91	92	O	of
15	94	102	O	intrinsic
16	104	110	O	factors
17	112	113	O	on
18	115	121	O	dehydro
19	123	134	O	XXXXXXXX
20	136	151	O	pharmacokinetics
21	152	152	O	:
22	154	159	O	Figure
23	161	162	O	22
24	164	169	O	Figure
25	171	172	O	23
NULL

