Protriptyline Hydrochloride	903	34090-1	b8881a81-75d7-43e8-825f-37c352c146dc	5
Protriptyline hydrochloride is an antidepressant agent.
1	0	26	O	XXXXXXXX
2	28	29	O	is
3	31	32	O	an
4	34	47	O	antidepressant
5	49	53	O	agent
NULL

Protriptyline Hydrochloride	904	34090-1	b8881a81-75d7-43e8-825f-37c352c146dc	11
The mechanism of its antidepressant action in man is not known.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	19	O	its
5	21	34	O	antidepressant
6	36	41	O	action
7	43	44	O	in
8	46	48	O	man
9	50	51	O	is
10	53	55	O	not
11	57	61	O	known
NULL

Protriptyline Hydrochloride	905	34090-1	b8881a81-75d7-43e8-825f-37c352c146dc	21
It is not a monoamine oxidase inhibitor, and it does not act primarily by stimulation of the central nervous system.
1	0	1	O	It
2	3	4	O	is
3	6	8	O	not
4	10	10	O	a
5	12	20	O	monoamine
6	22	28	O	oxidase
7	30	38	O	inhibitor
8	39	39	O	,
9	41	43	O	and
10	45	46	O	it
11	48	51	O	does
12	53	55	O	not
13	57	59	O	act
14	61	69	O	primarily
15	71	72	O	by
16	74	84	O	stimulation
17	86	87	O	of
18	89	91	O	the
19	93	99	O	central
20	101	107	O	nervous
21	109	114	O	system
NULL

Protriptyline Hydrochloride	906	34090-1	b8881a81-75d7-43e8-825f-37c352c146dc	19
Protriptyline has been found in some studies to have a more rapid onset of action than imipramine or amitriptyline.
1	0	12	O	XXXXXXXX
2	14	16	O	has
3	18	21	O	been
4	23	27	O	found
5	29	30	O	in
6	32	35	O	some
7	37	43	O	studies
8	45	46	O	to
9	48	51	O	have
10	53	53	O	a
11	55	58	O	more
12	60	64	O	rapid
13	66	70	B-T	onset
14	72	73	I-T	of
15	75	80	I-T	action
16	82	85	O	than
17	87	96	B-K	imipramine
18	98	99	O	or
19	101	113	B-K	amitriptyline
K/17:C54357 K/19:C54357

Protriptyline Hydrochloride	907	34090-1	b8881a81-75d7-43e8-825f-37c352c146dc	9
The initial clinical effect may occur within one week.
1	0	2	O	The
2	4	10	O	initial
3	12	19	O	clinical
4	21	26	O	effect
5	28	30	O	may
6	32	36	O	occur
7	38	43	O	within
8	45	47	O	one
9	49	52	O	week
NULL

Protriptyline Hydrochloride	908	34090-1	b8881a81-75d7-43e8-825f-37c352c146dc	6
Sedative and tranquilizing properties are lacking.
1	0	7	O	Sedative
2	9	11	O	and
3	13	25	O	tranquilizing
4	27	36	O	properties
5	38	40	O	are
6	42	48	O	lacking
NULL

