Maprotiline Hydrochloride	4320	34073-7	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	27
Close supervision and careful adjustment of dosage are required when administering maprotiline concomitantly with anticholinergic or sympathomimetic drugs because of the possibility of additive atropine like effects.
1	0	4	O	Close
2	6	16	B-T	supervision
3	18	20	O	and
4	22	28	O	careful
5	30	39	O	adjustment
6	41	42	O	of
7	44	49	O	dosage
8	51	53	O	are
9	55	62	O	required
10	64	67	O	when
11	69	81	O	administering
12	83	93	O	XXXXXXXX
13	95	107	O	concomitantly
14	109	112	O	with
15	114	128	B-D	anticholinergic
16	130	131	O	or
17	133	147	B-D	sympathomimetic
18	149	153	I-D	drugs
19	155	161	O	because
20	163	164	O	of
21	166	168	O	the
22	170	180	O	possibility
23	182	183	O	of
24	185	192	B-E	additive
25	194	201	I-E	atropine
26	203	206	I-E	like
27	208	214	I-E	effects
D/15:24:1 D/17:24:1

Maprotiline Hydrochloride	4321	34073-7	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	19
Concurrent administration of maprotiline with electroshock therapy should be avoided because of the lack of experience in this area.
1	0	9	O	Concurrent
2	11	24	O	administration
3	26	27	O	of
4	29	39	O	XXXXXXXX
5	41	44	O	with
6	46	57	B-D	electroshock
7	59	65	O	therapy
8	67	72	O	should
9	74	75	O	be
10	77	83	O	avoided
11	85	91	O	because
12	93	94	O	of
13	96	98	O	the
14	100	103	O	lack
15	105	106	O	of
16	108	117	O	experience
17	119	120	O	in
18	122	125	O	this
19	127	130	O	area
NULL

Maprotiline Hydrochloride	4322	34073-7	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	27
Caution should be exercised when administering maprotiline to hyperthyroid patients or those on thyroid medication because of the possibility of enhanced potential for cardiovascular toxicity of maprotiline.
1	0	6	B-T	Caution
2	8	13	O	should
3	15	16	O	be
4	18	26	O	exercised
5	28	31	O	when
6	33	45	O	administering
7	47	57	O	XXXXXXXX
8	59	60	O	to
9	62	73	B-U	hyperthyroid
10	75	82	O	patients
11	84	85	O	or
12	87	91	O	those
13	93	94	O	on
14	96	102	B-U	thyroid
15	104	113	I-U	medication
16	115	121	O	because
17	123	124	O	of
18	126	128	O	the
19	130	140	O	possibility
20	142	143	O	of
21	145	152	O	enhanced
22	154	162	O	potential
23	164	166	O	for
24	168	181	O	cardiovascular
25	183	190	B-T	toxicity
26	192	193	O	of
27	195	205	O	XXXXXXXX
NULL

Maprotiline Hydrochloride	4323	34073-7	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	23
Maprotiline should be used with caution in patients receiving guanethidine or similar agents since it may block the pharmacologic effects of these drugs.
1	0	10	O	XXXXXXXX
2	12	17	O	should
3	19	20	O	be
4	22	25	O	used
5	27	30	O	with
6	32	38	O	caution
7	40	41	O	in
8	43	50	O	patients
9	52	60	O	receiving
10	62	73	O	guanethidine
11	75	76	O	or
12	78	84	B-D	similar
13	86	91	I-D	agents
14	93	97	O	since
15	99	100	O	it
16	102	104	O	may
17	106	110	O	block
18	112	114	O	the
19	116	128	O	pharmacologic
20	130	136	O	effects
21	138	139	O	of
22	141	145	O	these
23	147	151	O	drugs
NULL

Maprotiline Hydrochloride	4324	34073-7	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	27
The risk of seizures may be increased when maprotiline is taken concomitantly with phenothiazines or when the dosage of benzodiazepines is rapidly tapered in patients receiving maprotiline.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	19	B-E	seizures
5	21	23	O	may
6	25	26	O	be
7	28	36	O	increased
8	38	41	O	when
9	43	53	O	XXXXXXXX
10	55	56	O	is
11	58	62	O	taken
12	64	76	O	concomitantly
13	78	81	O	with
14	83	96	B-D	phenothiazines
15	98	99	O	or
16	101	104	O	when
17	106	108	O	the
18	110	115	O	dosage
19	117	118	O	of
20	120	134	B-D	benzodiazepines
21	136	137	O	is
22	139	145	O	rapidly
23	147	153	O	tapered
24	155	156	O	in
25	158	165	O	patients
26	167	175	O	receiving
27	177	187	O	XXXXXXXX
D/14:4:1 D/20:4:1

Maprotiline Hydrochloride	4325	34073-7	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	56
Because of the pharmacologic similarity of maprotiline hydrochloride to the tricyclic antidepressants, the plasma concentration of maprotiline may be increased when the drug is given concomitantly with hepatic enzyme inhibitors (e.g., cimetidine, fluoxetine) and decreased by concomitant administration with hepatic enzyme inducers (e.g., barbiturates, phenytoin), as has occurred with tricyclic antidepressants.
1	0	6	O	Because
2	8	9	O	of
3	11	13	O	the
4	15	27	O	pharmacologic
5	29	38	O	similarity
6	40	41	O	of
7	43	67	O	XXXXXXXX
8	69	70	O	to
9	72	74	O	the
10	76	84	B-D	tricyclic
11	86	100	I-D	antidepressants
12	101	101	O	,
13	103	105	O	the
14	107	112	O	plasma
15	114	126	O	concentration
16	128	129	O	of
17	131	141	O	XXXXXXXX
18	143	145	O	may
19	147	148	O	be
20	150	158	O	increased
21	160	163	O	when
22	165	167	O	the
23	169	172	O	drug
24	174	175	O	is
25	177	181	O	given
26	183	195	O	concomitantly
27	197	200	O	with
28	202	208	B-D	hepatic
29	210	215	I-D	enzyme
30	217	226	I-D	inhibitors
31	229	231	O	e.g
32	233	233	O	,
33	235	244	O	cimetidine
34	245	245	O	,
35	247	256	O	fluoxetine
36	259	261	O	and
37	263	271	O	decreased
38	273	274	O	by
39	276	286	O	concomitant
40	288	301	O	administration
41	303	306	O	with
42	308	314	B-D	hepatic
43	316	321	I-D	enzyme
44	323	330	I-D	inducers
45	333	335	O	e.g
46	337	337	O	,
47	339	350	B-D	barbiturates
48	351	351	O	,
49	353	361	B-D	phenytoin
50	363	363	O	,
51	365	366	O	as
52	368	370	O	has
53	372	379	O	occurred
54	381	384	O	with
55	386	394	B-D	tricyclic
56	396	410	I-D	antidepressants
NULL

Maprotiline Hydrochloride	4326	34073-7	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	13
Adjustment of the dosage of maprotiline hydrochloride may therefore be necessary in such cases.
1	0	9	O	Adjustment
2	11	12	O	of
3	14	16	O	the
4	18	23	O	dosage
5	25	26	O	of
6	28	52	O	XXXXXXXX
7	54	56	O	may
8	58	66	O	therefore
9	68	69	O	be
10	71	79	O	necessary
11	81	82	O	in
12	84	87	O	such
13	89	93	O	cases
NULL

Maprotiline Hydrochloride	4328	34090-1	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	10
The mechanism of action of maprotiline is not precisely known.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	22	O	action
5	24	25	O	of
6	27	37	O	XXXXXXXX
7	39	40	O	is
8	42	44	O	not
9	46	54	O	precisely
10	56	60	O	known
NULL

Maprotiline Hydrochloride	4329	34090-1	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	19
It does not act primarily by stimulation of the central nervous system and is not a monoamine oxidase inhibitor.
1	0	1	O	It
2	3	6	O	does
3	8	10	O	not
4	12	14	O	act
5	16	24	O	primarily
6	26	27	O	by
7	29	39	O	stimulation
8	41	42	O	of
9	44	46	O	the
10	48	54	O	central
11	56	62	O	nervous
12	64	69	O	system
13	71	73	O	and
14	75	76	O	is
15	78	80	O	not
16	82	82	O	a
17	84	92	O	monoamine
18	94	100	O	oxidase
19	102	110	O	inhibitor
NULL

Maprotiline Hydrochloride	4330	34090-1	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	24
The postulated mechanism of maprotiline is that it acts primarily by potentiation of central adrenergic synapses by blocking reuptake of norepinephrine at nerve endings.
1	0	2	O	The
2	4	13	O	postulated
3	15	23	O	mechanism
4	25	26	O	of
5	28	38	O	XXXXXXXX
6	40	41	O	is
7	43	46	O	that
8	48	49	O	it
9	51	54	O	acts
10	56	64	O	primarily
11	66	67	O	by
12	69	80	O	potentiation
13	82	83	O	of
14	85	91	O	central
15	93	102	O	adrenergic
16	104	111	O	synapses
17	113	114	O	by
18	116	123	O	blocking
19	125	132	O	reuptake
20	134	135	O	of
21	137	150	O	norepinephrine
22	152	153	O	at
23	155	159	O	nerve
24	161	167	O	endings
NULL

Maprotiline Hydrochloride	4331	34090-1	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	15
This pharmacologic action is thought to be responsible for the drug's antidepressant and anxiolytic effects.
1	0	3	O	This
2	5	17	O	pharmacologic
3	19	24	O	action
4	26	27	O	is
5	29	35	O	thought
6	37	38	O	to
7	40	41	O	be
8	43	53	O	responsible
9	55	57	O	for
10	59	61	O	the
11	63	68	O	drug's
12	70	83	O	antidepressant
13	85	87	O	and
14	89	98	O	anxiolytic
15	100	106	O	effects
NULL

Maprotiline Hydrochloride	4332	34090-1	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	8
The half-life of elimination averages 51 hours.
1	0	2	O	The
2	4	7	O	half
3	9	12	O	life
4	14	15	O	of
5	17	27	O	elimination
6	29	36	O	averages
7	38	39	O	51
8	41	45	O	hours
NULL

Maprotiline Hydrochloride	4333	34090-1	c3ca69e6-1ea0-4c2c-abcb-7264b2e79a87	40
Steady-state levels measured prior to the morning dose on a one dosage regimen are summarized as follows: Average Minimum Concentration 95% Confidence Limits Regimen ng/mL ng/mL 50 mg * 3 daily 238 181-295
1	0	5	O	Steady
2	7	11	O	state
3	13	18	O	levels
4	20	27	O	measured
5	29	33	O	prior
6	35	36	O	to
7	38	40	O	the
8	42	48	O	morning
9	50	53	O	dose
10	55	56	O	on
11	58	58	O	a
12	60	62	O	one
13	64	69	O	dosage
14	71	77	O	regimen
15	79	81	O	are
16	83	92	O	summarized
17	94	95	O	as
18	97	103	O	follows
19	104	104	O	:
20	106	112	O	Average
21	114	120	O	Minimum
22	122	134	O	Concentration
23	136	138	O	95%
24	140	149	O	Confidence
25	151	156	O	Limits
26	158	164	O	Regimen
27	166	167	O	ng
28	168	168	O	/
29	169	170	O	mL
30	172	173	O	ng
31	174	174	O	/
32	175	176	O	mL
33	178	179	O	50
34	181	182	O	mg
35	184	184	O	*
36	186	186	O	3
37	188	192	O	daily
38	194	196	O	238
39	198	200	O	181
40	202	204	O	295
NULL

