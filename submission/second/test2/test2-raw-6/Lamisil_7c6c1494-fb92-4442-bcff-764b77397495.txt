Lamisil	5726	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	16
Terbinafine is an inhibitor of CYP450 2D6 isozyme and has an effect on metabolism of desipramine.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	16	O	an
4	18	26	O	inhibitor
5	28	29	O	of
6	31	36	O	CYP450
7	38	40	O	2D6
8	42	48	O	isozyme
9	50	52	O	and
10	54	56	O	has
11	58	59	O	an
12	61	66	B-T	effect
13	68	69	I-T	on
14	71	80	I-T	metabolism
15	82	83	O	of
16	85	95	B-K	desipramine
K/16:C54357

Lamisil	5727	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	17
Drug interactions have also been noted with cimetidine, fluconazole, cyclosporine, rifampin, and caffeine.
1	0	3	O	Drug
2	5	16	O	interactions
3	18	21	O	have
4	23	26	O	also
5	28	31	O	been
6	33	37	O	noted
7	39	42	O	with
8	44	53	B-K	cimetidine
9	54	54	O	,
10	56	66	O	fluconazole
11	67	67	O	,
12	69	80	O	cyclosporine
13	81	81	O	,
14	83	90	O	rifampin
15	91	91	O	,
16	93	95	O	and
17	97	104	O	caffeine
K/8:C54355

Lamisil	5728	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	15
In vivo studies have shown that terbinafine is an inhibitor of the CYP450 2D6 isozyme.
1	0	1	O	In
2	3	6	O	vivo
3	8	14	O	studies
4	16	19	O	have
5	21	25	O	shown
6	27	30	O	that
7	32	42	O	XXXXXXXX
8	44	45	O	is
9	47	48	O	an
10	50	58	O	inhibitor
11	60	61	O	of
12	63	65	O	the
13	67	72	O	CYP450
14	74	76	B-K	2D6
15	78	84	I-K	isozyme
K/14:C54355

Lamisil	5729	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	61
Drugs predominantly metabolized by the CYP450 2D6 isozyme include the following drug classes: tricyclic antidepressants, selective serotonin reuptake inhibitors, beta-blockers, antiarrhythmics class 1C (e.g., flecainide and propafenone) and monoamine oxidase inhibitors Type B. Coadministration of Lamisil Tablets should be done with careful monitoring and may require a reduction in dose of the 2D6-metabolized drug.
1	0	4	O	Drugs
2	6	18	B-K	predominantly
3	20	30	I-K	metabolized
4	32	33	I-K	by
5	35	37	I-K	the
6	39	44	B-D	CYP450
7	46	48	O	2D6
8	50	56	O	isozyme
9	58	64	O	include
10	66	68	O	the
11	70	78	O	following
12	80	83	O	drug
13	85	91	O	classes
14	92	92	O	:
15	94	102	O	tricyclic
16	104	118	O	antidepressants
17	119	119	O	,
18	121	129	O	selective
19	131	139	O	serotonin
20	141	148	O	reuptake
21	150	159	O	inhibitors
22	160	160	O	,
23	162	165	O	beta
24	167	174	O	blockers
25	175	175	O	,
26	177	191	O	antiarrhythmics
27	193	197	O	class
28	199	200	O	1C
29	203	205	O	e.g
30	207	207	O	,
31	209	218	O	flecainide
32	220	222	O	and
33	224	234	O	propafenone
34	237	239	O	and
35	241	249	O	monoamine
36	251	257	O	oxidase
37	259	268	O	inhibitors
38	270	273	O	Type
39	275	275	O	B
40	278	293	O	Coadministration
41	295	296	O	of
42	298	304	O	XXXXXXXX
43	306	312	O	Tablets
44	314	319	O	should
45	321	322	O	be
46	324	327	O	done
47	329	332	O	with
48	334	340	O	careful
49	342	351	B-T	monitoring
50	353	355	O	and
51	357	359	O	may
52	361	367	O	require
53	369	369	O	a
54	371	379	B-T	reduction
55	381	382	I-T	in
56	384	387	I-T	dose
57	389	390	O	of
58	392	394	O	the
59	396	398	B-K	2D6
60	400	410	I-K	metabolized
61	412	415	I-K	drug
K/2:C54357 K/59:C54357

Lamisil	5730	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	42
In a study to assess the effects of terbinafine on desipramine in healthy volunteers characterized as normal metabolizers, the administration of terbinafine resulted in a 2-fold increase in Cmax and a 5-fold increase in area under the curve (AUC).
1	0	1	O	In
2	3	3	O	a
3	5	9	O	study
4	11	12	O	to
5	14	19	O	assess
6	21	23	O	the
7	25	31	O	effects
8	33	34	O	of
9	36	46	O	XXXXXXXX
10	48	49	O	on
11	51	61	B-K	desipramine
12	63	64	O	in
13	66	72	O	healthy
14	74	83	O	volunteers
15	85	97	O	characterized
16	99	100	O	as
17	102	107	O	normal
18	109	120	O	metabolizers
19	121	121	O	,
20	123	125	O	the
21	127	140	O	administration
22	142	143	O	of
23	145	155	O	XXXXXXXX
24	157	164	O	resulted
25	166	167	O	in
26	169	169	O	a
27	171	171	O	2
28	173	176	O	fold
29	178	185	O	increase
30	187	188	O	in
31	190	193	O	Cmax
32	195	197	O	and
33	199	199	O	a
34	201	201	O	5
35	203	206	O	fold
36	208	215	B-T	increase
37	217	218	I-T	in
38	220	223	I-T	area
39	225	229	I-T	under
40	231	233	I-T	the
41	235	239	I-T	curve
42	242	244	I-T	AUC
K/11:C54355

Lamisil	5731	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	22
In this study, these effects were shown to persist at the last observation at 4 weeks after discontinuation of LAMISIL Tablets.
1	0	1	O	In
2	3	6	O	this
3	8	12	O	study
4	13	13	O	,
5	15	19	O	these
6	21	27	O	effects
7	29	32	O	were
8	34	38	O	shown
9	40	41	O	to
10	43	49	O	persist
11	51	52	O	at
12	54	56	O	the
13	58	61	O	last
14	63	73	O	observation
15	75	76	O	at
16	78	78	O	4
17	80	84	O	weeks
18	86	90	O	after
19	92	106	O	discontinuation
20	108	109	O	of
21	111	117	O	XXXXXXXX
22	119	125	O	Tablets
NULL

Lamisil	5732	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	35
In studies in healthy subjects characterized as extensive metabolizers of dextromethorphan (antitussive drug and CYP2D6 probe substrate), terbinafine increases the dextromethorphan/dextrorphan metabolite ratio in urine by 16- to 97-fold on average.
1	0	1	O	In
2	3	9	O	studies
3	11	12	O	in
4	14	20	O	healthy
5	22	29	O	subjects
6	31	43	O	characterized
7	45	46	O	as
8	48	56	O	extensive
9	58	69	O	metabolizers
10	71	72	O	of
11	74	89	B-K	dextromethorphan
12	92	102	O	antitussive
13	104	107	O	drug
14	109	111	O	and
15	113	118	O	CYP2D6
16	120	124	O	probe
17	126	134	O	substrate
18	136	136	O	,
19	138	148	O	XXXXXXXX
20	150	158	O	increases
21	160	162	O	the
22	164	179	B-K	dextromethorphan
23	180	180	O	/
24	181	191	B-K	dextrorphan
25	193	202	O	metabolite
26	204	208	O	ratio
27	210	211	O	in
28	213	217	O	urine
29	219	220	O	by
30	222	223	O	16
31	226	227	O	to
32	229	230	O	97
33	232	235	O	fold
34	237	238	O	on
35	240	246	O	average
K/11:C54357 K/22:C54357 K/24:C54357

Lamisil	5733	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	12
Thus, terbinafine may convert extensive CYP2D6 metabolizers to poor metabolizer status.
1	0	3	O	Thus
2	4	4	O	,
3	6	16	O	XXXXXXXX
4	18	20	O	may
5	22	28	O	convert
6	30	38	O	extensive
7	40	45	O	CYP2D6
8	47	58	O	metabolizers
9	60	61	O	to
10	63	66	O	poor
11	68	78	O	metabolizer
12	80	85	O	status
NULL

Lamisil	5734	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	27
In vitro studies with human liver microsomes showed that terbinafine does not inhibit the metabolism of tolbutamide, ethinylestradiol, ethoxycoumarin, cyclosporine, cisapride and fluvastatin.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	studies
4	17	20	O	with
5	22	26	O	human
6	28	32	O	liver
7	34	43	O	microsomes
8	45	50	O	showed
9	52	55	O	that
10	57	67	O	XXXXXXXX
11	69	72	O	does
12	74	76	O	not
13	78	84	O	inhibit
14	86	88	O	the
15	90	99	O	metabolism
16	101	102	O	of
17	104	114	O	tolbutamide
18	115	115	O	,
19	117	132	O	ethinylestradiol
20	133	133	O	,
21	135	148	O	ethoxycoumarin
22	149	149	O	,
23	151	162	O	cyclosporine
24	163	163	O	,
25	165	173	O	cisapride
26	175	177	O	and
27	179	189	O	fluvastatin
NULL

Lamisil	5735	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	23
In vivo drug-drug interaction studies conducted in healthy volunteer subjects showed that terbinafine does not affect the clearance of antipyrine or digoxin.
1	0	1	O	In
2	3	6	O	vivo
3	8	11	O	drug
4	13	16	O	drug
5	18	28	O	interaction
6	30	36	O	studies
7	38	46	O	conducted
8	48	49	O	in
9	51	57	O	healthy
10	59	67	O	volunteer
11	69	76	O	subjects
12	78	83	O	showed
13	85	88	O	that
14	90	100	O	XXXXXXXX
15	102	105	O	does
16	107	109	O	not
17	111	116	O	affect
18	118	120	O	the
19	122	130	O	clearance
20	132	133	O	of
21	135	144	O	antipyrine
22	146	147	O	or
23	149	155	O	digoxin
NULL

Lamisil	5736	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	8
Terbinafine decreases the clearance of caffeine by 19%.
1	0	10	O	XXXXXXXX
2	12	20	B-T	decreases
3	22	24	I-T	the
4	26	34	I-T	clearance
5	36	37	O	of
6	39	46	B-K	caffeine
7	48	49	O	by
8	51	53	O	19%
K/6:C54357

Lamisil	5737	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	8
Terbinafine increases the clearance of cyclosporine by 15%.
1	0	10	O	XXXXXXXX
2	12	20	B-T	increases
3	22	24	I-T	the
4	26	34	I-T	clearance
5	36	37	O	of
6	39	50	B-K	cyclosporine
7	52	53	O	by
8	55	57	O	15%
K/6:C54357

Lamisil	5738	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	25
The influence of terbinafine on the pharmacokinetics of fluconazole, cotrimoxazole (trimethoprim and sulfamethoxazole), zidovudine or theophylline was not considered to be clinically significant.
1	0	2	O	The
2	4	12	O	influence
3	14	15	O	of
4	17	27	O	XXXXXXXX
5	29	30	O	on
6	32	34	O	the
7	36	51	O	pharmacokinetics
8	53	54	O	of
9	56	66	O	fluconazole
10	67	67	O	,
11	69	81	O	cotrimoxazole
12	84	95	O	trimethoprim
13	97	99	O	and
14	101	116	O	sulfamethoxazole
15	118	118	O	,
16	120	129	O	zidovudine
17	131	132	O	or
18	134	145	O	theophylline
19	147	149	O	was
20	151	153	O	not
21	155	164	B-T	considered
22	166	167	O	to
23	169	170	O	be
24	172	181	O	clinically
25	183	193	O	significant
NULL

Lamisil	5739	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	29
Coadministration of a single dose of fluconazole (100 mg) with a single dose of terbinafine resulted in a 52% and 69% increase in terbinafine Cmax and AUC, respectively.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	20	O	a
4	22	27	O	single
5	29	32	O	dose
6	34	35	O	of
7	37	47	B-K	fluconazole
8	50	52	O	100
9	54	55	O	mg
10	58	61	O	with
11	63	63	O	a
12	65	70	O	single
13	72	75	O	dose
14	77	78	O	of
15	80	90	O	XXXXXXXX
16	92	99	O	resulted
17	101	102	O	in
18	104	104	O	a
19	106	108	O	52%
20	110	112	O	and
21	114	116	O	69%
22	118	125	O	increase
23	127	128	O	in
24	130	140	O	XXXXXXXX
25	142	145	O	Cmax
26	147	149	O	and
27	151	153	O	AUC
28	154	154	O	,
29	156	167	O	respectively
K/7:C54602

Lamisil	5740	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	9
Fluconazole is an inhibitor of CYP2C9 and CYP3A enzymes.
1	0	10	O	Fluconazole
2	12	13	O	is
3	15	16	O	an
4	18	26	O	inhibitor
5	28	29	O	of
6	31	36	O	CYP2C9
7	38	40	O	and
8	42	46	O	CYP3A
9	48	54	O	enzymes
NULL

Lamisil	5741	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	40
Based on this finding, it is likely that other inhibitors of both CYP2C9 and CYP3A4 (e.g., ketoconazole, amiodarone) may also lead to a substantial increase in the systemic exposure (Cmax and AUC) of terbinafine when concomitantly administered.
1	0	4	O	Based
2	6	7	O	on
3	9	12	O	this
4	14	20	O	finding
5	21	21	O	,
6	23	24	O	it
7	26	27	O	is
8	29	34	O	likely
9	36	39	O	that
10	41	45	O	other
11	47	56	O	inhibitors
12	58	59	O	of
13	61	64	O	both
14	66	71	O	CYP2C9
15	73	75	O	and
16	77	82	O	CYP3A4
17	85	87	O	e.g
18	89	89	O	,
19	91	102	B-K	ketoconazole
20	103	103	O	,
21	105	114	B-K	amiodarone
22	117	119	O	may
23	121	124	O	also
24	126	129	O	lead
25	131	132	O	to
26	134	134	O	a
27	136	146	O	substantial
28	148	155	O	increase
29	157	158	O	in
30	160	162	B-T	the
31	164	171	I-T	systemic
32	173	180	I-T	exposure
33	183	186	I-T	Cmax
34	188	190	O	and
35	192	194	O	AUC
36	197	198	O	of
37	200	210	O	XXXXXXXX
38	212	215	O	when
39	217	229	O	concomitantly
40	231	242	O	administered
K/19:C54355 K/21:C54355

Lamisil	5742	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	36
There have been spontaneous reports of increase or decrease in prothrombin times in patients concomitantly taking oral terbinafine and warfarin, however, a causal relationship between LAMISIL Tablets and these changes has not been established.
1	0	4	O	There
2	6	9	O	have
3	11	14	O	been
4	16	26	O	spontaneous
5	28	34	O	reports
6	36	37	O	of
7	39	46	O	increase
8	48	49	O	or
9	51	58	O	decrease
10	60	61	O	in
11	63	73	O	prothrombin
12	75	79	O	times
13	81	82	O	in
14	84	91	O	patients
15	93	105	O	concomitantly
16	107	112	O	taking
17	114	117	O	oral
18	119	129	O	XXXXXXXX
19	131	133	O	and
20	135	142	O	warfarin
21	143	143	O	,
22	145	151	O	however
23	152	152	O	,
24	154	154	O	a
25	156	161	O	causal
26	163	174	O	relationship
27	176	182	O	between
28	184	190	O	XXXXXXXX
29	192	198	O	Tablets
30	200	202	O	and
31	204	208	O	these
32	210	216	O	changes
33	218	220	O	has
34	222	224	O	not
35	226	229	O	been
36	231	241	O	established
NULL

Lamisil	5743	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	23
Terbinafine clearance is increased 100% by rifampin, a CYP450 enzyme inducer, and decreased 33% by cimetidine, a CYP450 enzyme inhibitor.
1	0	10	O	XXXXXXXX
2	12	20	O	clearance
3	22	23	O	is
4	25	33	O	increased
5	35	38	O	100%
6	40	41	O	by
7	43	50	B-K	rifampin
8	51	51	O	,
9	53	53	O	a
10	55	60	B-K	CYP450
11	62	67	I-K	enzyme
12	69	75	I-K	inducer
13	76	76	O	,
14	78	80	O	and
15	82	90	B-T	decreased
16	92	94	I-T	33%
17	96	97	O	by
18	99	108	B-K	cimetidine
19	109	109	O	,
20	111	111	O	a
21	113	118	B-D	CYP450
22	120	125	B-K	enzyme
23	127	135	I-K	inhibitor
K/7:C54355 K/10:C54355 K/18:C54355 K/22:C54355

Lamisil	5744	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	6
Terbinafine clearance is unaffected by cyclosporine.
1	0	10	O	XXXXXXXX
2	12	20	O	clearance
3	22	23	O	is
4	25	34	O	unaffected
5	36	37	O	by
6	39	50	O	cyclosporine
NULL

Lamisil	5745	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	36
There is no information available from adequate drug-drug interaction studies with the following classes of drugs: oral contraceptives, hormone replacement therapies, hypoglycemics, phenytoins, thiazide diuretics, and calcium channel blockers.
1	0	4	O	There
2	6	7	O	is
3	9	10	O	no
4	12	22	O	information
5	24	32	O	available
6	34	37	O	from
7	39	46	O	adequate
8	48	51	O	drug
9	53	56	O	drug
10	58	68	O	interaction
11	70	76	O	studies
12	78	81	O	with
13	83	85	O	the
14	87	95	O	following
15	97	103	O	classes
16	105	106	O	of
17	108	112	O	drugs
18	113	113	O	:
19	115	118	O	oral
20	120	133	O	contraceptives
21	134	134	O	,
22	136	142	O	hormone
23	144	154	O	replacement
24	156	164	O	therapies
25	165	165	O	,
26	167	179	O	hypoglycemics
27	180	180	O	,
28	182	191	O	phenytoins
29	192	192	O	,
30	194	201	O	thiazide
31	203	211	O	diuretics
32	212	212	O	,
33	214	216	O	and
34	218	224	O	calcium
35	226	232	O	channel
36	234	241	O	blockers
NULL

Lamisil	5746	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	12
An evaluation of the effect of food on LamisilTablets was conducted.
1	0	1	O	An
2	3	12	O	evaluation
3	14	15	O	of
4	17	19	O	the
5	21	26	O	effect
6	28	29	O	of
7	31	34	O	food
8	36	37	O	on
9	39	45	O	XXXXXXXX
10	46	52	O	Tablets
11	54	56	O	was
12	58	66	O	conducted
NULL

Lamisil	5747	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	20
An increase of less than 20% of the AUC of terbinafine was observed when LAMISIL Tablets were administered with food.
1	0	1	O	An
2	3	10	O	increase
3	12	13	O	of
4	15	18	O	less
5	20	23	O	than
6	25	27	O	20%
7	29	30	O	of
8	32	34	O	the
9	36	38	O	AUC
10	40	41	O	of
11	43	53	O	XXXXXXXX
12	55	57	O	was
13	59	66	O	observed
14	68	71	O	when
15	73	79	O	XXXXXXXX
16	81	87	O	Tablets
17	89	92	O	were
18	94	105	O	administered
19	107	110	O	with
20	112	115	O	food
NULL

Lamisil	5748	34073-7	7c6c1494-fb92-4442-bcff-764b77397495	9
Lamisil Tablets can be taken with or without food.
1	0	6	O	XXXXXXXX
2	8	14	O	Tablets
3	16	18	O	can
4	20	21	O	be
5	23	27	O	taken
6	29	32	O	with
7	34	35	O	or
8	37	43	O	without
9	45	48	O	food
NULL

Lamisil	5749	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	5
Terbinafine is an allylamine antifungal.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	16	O	an
4	18	27	O	allylamine
5	29	38	O	antifungal
NULL

Lamisil	5750	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	7
The pharmacodynamics of Lamisil Tablets is unknown.
1	0	2	O	The
2	4	19	O	pharmacodynamics
3	21	22	O	of
4	24	30	O	XXXXXXXX
5	32	38	O	Tablets
6	40	41	O	is
7	43	49	O	unknown
NULL

Lamisil	5751	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	25
Following oral administration, terbinafine is well absorbed (>70%) and the bioavailability of Lamisil Tablets as a result of first-pass metabolism is approximately 40%.
1	0	8	O	Following
2	10	13	O	oral
3	15	28	O	administration
4	29	29	O	,
5	31	41	O	XXXXXXXX
6	43	44	O	is
7	46	49	O	well
8	51	58	O	absorbed
9	61	64	O	>70%
10	67	69	O	and
11	71	73	O	the
12	75	89	O	bioavailability
13	91	92	O	of
14	94	100	O	XXXXXXXX
15	102	108	O	Tablets
16	110	111	O	as
17	113	113	O	a
18	115	120	O	result
19	122	123	O	of
20	125	129	O	first
21	131	134	B-T	pass
22	136	145	I-T	metabolism
23	147	148	O	is
24	150	162	O	approximately
25	164	166	O	40%
NULL

Lamisil	5752	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	26
Peak plasma concentrations of 1 mcg/mL appear within 2 hours after a single 250 mg dose; the AUC is approximately 4.56 mcg.h/mL.
1	0	3	O	Peak
2	5	10	O	plasma
3	12	25	O	concentrations
4	27	28	O	of
5	30	30	O	1
6	32	34	O	mcg
7	35	35	O	/
8	36	37	O	mL
9	39	44	O	appear
10	46	51	O	within
11	53	53	O	2
12	55	59	O	hours
13	61	65	O	after
14	67	67	O	a
15	69	74	O	single
16	76	78	O	250
17	80	81	O	mg
18	83	86	O	dose
19	89	91	O	the
20	93	95	O	AUC
21	97	98	O	is
22	100	112	O	approximately
23	114	117	O	4.56
24	119	123	O	mcg.h
25	124	124	O	/
26	125	126	O	mL
NULL

Lamisil	5753	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	19
An increase in the AUC of terbinafine of less than 20% is observed when LamisilTablets areadministered with food.
1	0	1	O	An
2	3	10	O	increase
3	12	13	O	in
4	15	17	O	the
5	19	21	O	AUC
6	23	24	O	of
7	26	36	O	XXXXXXXX
8	38	39	O	of
9	41	44	O	less
10	46	49	O	than
11	51	53	O	20%
12	55	56	O	is
13	58	65	O	observed
14	67	70	O	when
15	72	78	O	XXXXXXXX
16	79	85	O	Tablets
17	87	101	O	areadministered
18	103	106	O	with
19	108	111	O	food
NULL

Lamisil	5754	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	19
In plasma, terbinafine is greater than 99% bound to plasma proteins and there are no specific binding sites.
1	0	1	O	In
2	3	8	O	plasma
3	9	9	O	,
4	11	21	O	XXXXXXXX
5	23	24	O	is
6	26	32	O	greater
7	34	37	O	than
8	39	41	O	99%
9	43	47	O	bound
10	49	50	O	to
11	52	57	O	plasma
12	59	66	O	proteins
13	68	70	O	and
14	72	76	O	there
15	78	80	O	are
16	82	83	O	no
17	85	92	O	specific
18	94	100	O	binding
19	102	106	O	sites
NULL

Lamisil	5755	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	43
At steady-state, in comparison to a single dose, the peak concentration of terbinafine is 25% higher and plasma AUC increases by a factor of 2.5; the increase in plasma AUC is consistent with an effective half-life of ~36 hours.
1	0	1	O	At
2	3	8	O	steady
3	10	14	O	state
4	15	15	O	,
5	17	18	O	in
6	20	29	O	comparison
7	31	32	O	to
8	34	34	O	a
9	36	41	O	single
10	43	46	O	dose
11	47	47	O	,
12	49	51	O	the
13	53	56	O	peak
14	58	70	O	concentration
15	72	73	O	of
16	75	85	O	XXXXXXXX
17	87	88	O	is
18	90	92	O	25%
19	94	99	O	higher
20	101	103	O	and
21	105	110	O	plasma
22	112	114	B-T	AUC
23	116	124	I-T	increases
24	126	127	O	by
25	129	129	O	a
26	131	136	O	factor
27	138	139	O	of
28	141	143	O	2.5
29	146	148	O	the
30	150	157	O	increase
31	159	160	O	in
32	162	167	B-T	plasma
33	169	171	I-T	AUC
34	173	174	O	is
35	176	185	O	consistent
36	187	190	O	with
37	192	193	O	an
38	195	203	O	effective
39	205	208	O	half
40	210	213	B-T	life
41	215	216	O	of
42	218	220	O	~36
43	222	226	O	hours
NULL

Lamisil	5756	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	8
Terbinafine is distributed to the sebum and skin.
1	0	10	O	XXXXXXXX
2	12	13	O	is
3	15	25	O	distributed
4	27	28	O	to
5	30	32	O	the
6	34	38	O	sebum
7	40	42	O	and
8	44	47	O	skin
NULL

Lamisil	5757	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	22
A terminal half-life of 200-400 hours may represent the slow elimination of terbinafine from tissues such as skin and adipose.
1	0	0	O	A
2	2	9	O	terminal
3	11	14	O	half
4	16	19	O	life
5	21	22	O	of
6	24	26	O	200
7	28	30	O	400
8	32	36	O	hours
9	38	40	O	may
10	42	50	O	represent
11	52	54	O	the
12	56	59	O	slow
13	61	71	O	elimination
14	73	74	O	of
15	76	86	O	XXXXXXXX
16	88	91	O	from
17	93	99	O	tissues
18	101	104	O	such
19	106	107	O	as
20	109	112	O	skin
21	114	116	O	and
22	118	124	O	adipose
NULL

Lamisil	5758	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	27
Prior to excretion, terbinafine is extensively metabolizedby at least 7 CYP isoenzymes with major contributions from CYP2C9, CYP1A2, CYP3A4, CYP2C8, and CYP2C19.
1	0	4	O	Prior
2	6	7	O	to
3	9	17	O	excretion
4	18	18	O	,
5	20	30	O	XXXXXXXX
6	32	33	O	is
7	35	45	O	extensively
8	47	59	O	metabolizedby
9	61	62	O	at
10	64	68	O	least
11	70	70	O	7
12	72	74	O	CYP
13	76	85	O	isoenzymes
14	87	90	O	with
15	92	96	O	major
16	98	110	O	contributions
17	112	115	O	from
18	117	122	O	CYP2C9
19	123	123	O	,
20	125	130	O	CYP1A2
21	131	131	O	,
22	133	138	O	CYP3A4
23	139	139	O	,
24	141	146	O	CYP2C8
25	147	147	O	,
26	149	151	O	and
27	153	159	O	CYP2C19
NULL

Lamisil	5759	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	12
No metabolites have been identified that have antifungal activity similar to terbinafine.
1	0	1	O	No
2	3	13	O	metabolites
3	15	18	O	have
4	20	23	O	been
5	25	34	O	identified
6	36	39	O	that
7	41	44	O	have
8	46	55	O	antifungal
9	57	64	O	activity
10	66	72	O	similar
11	74	75	O	to
12	77	87	O	XXXXXXXX
NULL

Lamisil	5760	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	11
Approximately 70% of the administered dose is eliminated in the urine.
1	0	12	O	Approximately
2	14	16	O	70%
3	18	19	O	of
4	21	23	O	the
5	25	36	O	administered
6	38	41	O	dose
7	43	44	O	is
8	46	55	O	eliminated
9	57	58	O	in
10	60	62	O	the
11	64	68	O	urine
NULL

Lamisil	5761	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	33
In patients with renal impairment (creatinine clearance less than or equal to 50 mL/min) or hepatic cirrhosis, the clearance of terbinafine is decreased by approximately 50% compared to normal volunteers.
1	0	1	O	In
2	3	10	O	patients
3	12	15	O	with
4	17	21	O	renal
5	23	32	O	impairment
6	35	44	O	creatinine
7	46	54	O	clearance
8	56	59	O	less
9	61	64	O	than
10	66	67	O	or
11	69	73	O	equal
12	75	76	O	to
13	78	79	O	50
14	81	82	O	mL
15	83	83	O	/
16	84	86	O	min
17	89	90	O	or
18	92	98	O	hepatic
19	100	108	O	cirrhosis
20	109	109	O	,
21	111	113	O	the
22	115	123	O	clearance
23	125	126	O	of
24	128	138	O	XXXXXXXX
25	140	141	O	is
26	143	151	O	decreased
27	153	154	O	by
28	156	168	O	approximately
29	170	172	O	50%
30	174	181	O	compared
31	183	184	O	to
32	186	191	O	normal
33	193	202	O	volunteers
NULL

Lamisil	5762	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	15
No effect of gender on the blood levels of terbinafine was detected in clinical trials.
1	0	1	O	No
2	3	8	O	effect
3	10	11	O	of
4	13	18	O	gender
5	20	21	O	on
6	23	25	O	the
7	27	31	O	blood
8	33	38	O	levels
9	40	41	O	of
10	43	53	O	XXXXXXXX
11	55	57	O	was
12	59	66	O	detected
13	68	69	O	in
14	71	78	O	clinical
15	80	85	O	trials
NULL

Lamisil	5763	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	16
No clinically relevant age-dependent changes in steady-state plasma concentrations of terbinafine have been reported.
1	0	1	O	No
2	3	12	O	clinically
3	14	21	O	relevant
4	23	25	O	age
5	27	35	O	dependent
6	37	43	O	changes
7	45	46	O	in
8	48	53	O	steady
9	55	59	O	state
10	61	66	O	plasma
11	68	81	O	concentrations
12	83	84	O	of
13	86	96	O	XXXXXXXX
14	98	101	O	have
15	103	106	O	been
16	108	115	O	reported
NULL

Lamisil	5764	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	22
Terbinafine,an allylamine antifungal, inhibits biosynthesis of ergosterol, an essential component of fungal cell membrane, via inhibition of squaleneepoxidaseenzyme.
1	0	10	O	XXXXXXXX
2	11	13	O	,an
3	15	24	O	allylamine
4	26	35	O	antifungal
5	36	36	O	,
6	38	45	O	inhibits
7	47	58	O	biosynthesis
8	60	61	O	of
9	63	72	O	ergosterol
10	73	73	O	,
11	75	76	O	an
12	78	86	O	essential
13	88	96	O	component
14	98	99	O	of
15	101	106	O	fungal
16	108	111	O	cell
17	113	120	O	membrane
18	121	121	O	,
19	123	125	O	via
20	127	136	B-E	inhibition
21	138	139	O	of
22	141	163	B-E	squaleneepoxidaseenzyme
NULL

Lamisil	5765	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	28
This results in fungal cell death primarily due to the increased membrane permeability mediated by the accumulation of high concentrations of squalene but not due to ergosterol deficiency.
1	0	3	O	This
2	5	11	O	results
3	13	14	O	in
4	16	21	O	fungal
5	23	26	O	cell
6	28	32	O	death
7	34	42	O	primarily
8	44	46	O	due
9	48	49	O	to
10	51	53	O	the
11	55	63	B-E	increased
12	65	72	I-E	membrane
13	74	85	I-E	permeability
14	87	94	I-E	mediated
15	96	97	O	by
16	99	101	O	the
17	103	114	O	accumulation
18	116	117	O	of
19	119	122	O	high
20	124	137	B-T	concentrations
21	139	140	O	of
22	142	149	B-K	squalene
23	151	153	O	but
24	155	157	O	not
25	159	161	O	due
26	163	164	O	to
27	166	175	O	ergosterol
28	177	186	O	deficiency
K/22:C54357

Lamisil	5766	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	20
Depending on the concentration of the drug and the fungal species test in vitro, terbinafine hydrochloride may be fungicidal.
1	0	8	O	Depending
2	10	11	O	on
3	13	15	O	the
4	17	29	O	concentration
5	31	32	O	of
6	34	36	O	the
7	38	41	O	drug
8	43	45	O	and
9	47	49	O	the
10	51	56	O	fungal
11	58	64	O	species
12	66	69	O	test
13	71	72	O	in
14	74	78	O	vitro
15	79	79	O	,
16	81	91	O	XXXXXXXX
17	93	105	O	hydrochloride
18	107	109	O	may
19	111	112	O	be
20	114	123	O	fungicidal
NULL

Lamisil	5767	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	11
However, the clinical significance of in vitro data is unknown.
1	0	6	O	However
2	7	7	O	,
3	9	11	O	the
4	13	20	O	clinical
5	22	33	O	significance
6	35	36	O	of
7	38	39	O	in
8	41	45	O	vitro
9	47	50	O	data
10	52	53	O	is
11	55	61	O	unknown
NULL

Lamisil	5768	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	40
Terbinafine has been shown to be active against most strains of the following microorganisms both in vitro and in clinical infections: Trichophyton mentagrophytes Trichophyton rubrum The following in vitro data are available, but their clinical significance is unknown.
1	0	10	O	XXXXXXXX
2	12	14	O	has
3	16	19	O	been
4	21	25	O	shown
5	27	28	O	to
6	30	31	O	be
7	33	38	O	active
8	40	46	O	against
9	48	51	O	most
10	53	59	O	strains
11	61	62	O	of
12	64	66	O	the
13	68	76	O	following
14	78	91	O	microorganisms
15	93	96	O	both
16	98	99	O	in
17	101	105	O	vitro
18	107	109	O	and
19	111	112	O	in
20	114	121	O	clinical
21	123	132	O	infections
22	133	133	O	:
23	135	146	O	Trichophyton
24	148	161	O	mentagrophytes
25	163	174	O	Trichophyton
26	176	181	O	rubrum
27	183	185	O	The
28	187	195	O	following
29	197	198	O	in
30	200	204	O	vitro
31	206	209	O	data
32	211	213	O	are
33	215	223	O	available
34	224	224	O	,
35	226	228	O	but
36	230	234	O	their
37	236	243	O	clinical
38	245	256	O	significance
39	258	259	O	is
40	261	267	O	unknown
NULL

Lamisil	5769	34090-1	7c6c1494-fb92-4442-bcff-764b77397495	48
In vitro, terbinafine exhibits satisfactory MIC's against most strains of the following microorganisms; however, the safety and efficacy of terbinafine in treating clinical infections due to these microorganisms have not been established in adequate and well-controlled clinical trials: Candida albicans Epidermophyton floccosum Scopulariopsis brevicaulis
1	0	1	O	In
2	3	7	O	vitro
3	8	8	O	,
4	10	20	O	XXXXXXXX
5	22	29	O	exhibits
6	31	42	O	satisfactory
7	44	48	O	MIC's
8	50	56	O	against
9	58	61	O	most
10	63	69	O	strains
11	71	72	O	of
12	74	76	O	the
13	78	86	O	following
14	88	101	O	microorganisms
15	104	110	O	however
16	111	111	O	,
17	113	115	O	the
18	117	122	O	safety
19	124	126	O	and
20	128	135	O	efficacy
21	137	138	O	of
22	140	150	O	XXXXXXXX
23	152	153	O	in
24	155	162	O	treating
25	164	171	O	clinical
26	173	182	O	infections
27	184	186	O	due
28	188	189	O	to
29	191	195	O	these
30	197	210	O	microorganisms
31	212	215	O	have
32	217	219	O	not
33	221	224	O	been
34	226	236	O	established
35	238	239	O	in
36	241	248	O	adequate
37	250	252	O	and
38	254	257	O	well
39	259	268	O	controlled
40	270	277	O	clinical
41	279	284	O	trials
42	285	285	O	:
43	287	293	O	Candida
44	295	302	O	albicans
45	304	317	O	Epidermophyton
46	319	327	O	floccosum
47	329	342	O	Scopulariopsis
48	344	354	O	brevicaulis
NULL

