Amoxapine	3401	34073-7	a16297df-3158-48db-85e5-5cd506885556	12
See CONTRAINDICATIONS about concurrent usage of tricyclic antidepressants and monoamine oxidase inhibitors.
1	0	2	O	See
2	4	20	O	CONTRAINDICATIONS
3	22	26	O	about
4	28	37	O	concurrent
5	39	43	O	usage
6	45	46	O	of
7	48	56	O	tricyclic
8	58	72	O	antidepressants
9	74	76	O	and
10	78	86	O	monoamine
11	88	94	O	oxidase
12	96	105	O	inhibitors
NULL

Amoxapine	3402	34073-7	a16297df-3158-48db-85e5-5cd506885556	14
Paralytic ileus may occur in patients taking tricyclic antidepressants in combination with anticholinergic drugs.
1	0	8	O	Paralytic
2	10	14	O	ileus
3	16	18	O	may
4	20	24	O	occur
5	26	27	O	in
6	29	36	O	patients
7	38	43	O	taking
8	45	53	B-D	tricyclic
9	55	69	I-D	antidepressants
10	71	72	O	in
11	74	84	O	combination
12	86	89	O	with
13	91	105	B-D	anticholinergic
14	107	111	I-D	drugs
NULL

Amoxapine	3403	34073-7	a16297df-3158-48db-85e5-5cd506885556	16
Amoxapine may enhance the response to alcohol and the effects of barbiturates and other CNS depressants.
1	0	8	O	XXXXXXXX
2	10	12	O	may
3	14	20	B-T	enhance
4	22	24	I-T	the
5	26	33	I-T	response
6	35	36	O	to
7	38	44	B-K	alcohol
8	46	48	O	and
9	50	52	O	the
10	54	60	O	effects
11	62	63	O	of
12	65	76	B-K	barbiturates
13	78	80	O	and
14	82	86	O	other
15	88	90	B-K	CNS
16	92	102	I-K	depressants
K/7:C54357 K/12:C54357 K/15:C54357

Amoxapine	3404	34073-7	a16297df-3158-48db-85e5-5cd506885556	18
Serum levels of several tricyclic antidepressants have been reported to be significantly increased when cimetidine is administered concurrently.
1	0	4	O	Serum
2	6	11	O	levels
3	13	14	O	of
4	16	22	O	several
5	24	32	B-K	tricyclic
6	34	48	I-K	antidepressants
7	50	53	O	have
8	55	58	O	been
9	60	67	O	reported
10	69	70	O	to
11	72	73	O	be
12	75	87	O	significantly
13	89	97	O	increased
14	99	102	O	when
15	104	113	B-K	cimetidine
16	115	116	O	is
17	118	129	O	administered
18	131	142	O	concurrently
K/5:C54357 K/15:C54357

Amoxapine	3405	34073-7	a16297df-3158-48db-85e5-5cd506885556	27
Although such an interaction has not been reported to date with amoxapine, specific interaction studies have not been done, and the possibility should be considered.
1	0	7	O	Although
2	9	12	O	such
3	14	15	O	an
4	17	27	O	interaction
5	29	31	O	has
6	33	35	O	not
7	37	40	O	been
8	42	49	O	reported
9	51	52	O	to
10	54	57	O	date
11	59	62	O	with
12	64	72	O	XXXXXXXX
13	73	73	O	,
14	75	82	O	specific
15	84	94	O	interaction
16	96	102	O	studies
17	104	107	O	have
18	109	111	O	not
19	113	116	O	been
20	118	121	O	done
21	122	122	O	,
22	124	126	O	and
23	128	130	O	the
24	132	142	O	possibility
25	144	149	O	should
26	151	152	O	be
27	154	163	O	considered
NULL

Amoxapine	3406	34073-7	a16297df-3158-48db-85e5-5cd506885556	60
Drugs Metabolized by P450 2D6 - The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the Caucasian population (about 7% to 10% of Caucasians are so called "poor metabolizers"); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African and other populations are not yet available.
1	0	4	O	Drugs
2	6	16	O	Metabolized
3	18	19	O	by
4	21	24	O	P450
5	26	28	O	2D6
6	32	34	O	The
7	36	46	O	biochemical
8	48	55	O	activity
9	57	58	O	of
10	60	62	O	the
11	64	67	O	drug
12	69	80	O	metabolizing
13	82	88	O	isozyme
14	90	99	O	cytochrome
15	101	104	O	P450
16	106	108	B-K	2D6
17	111	121	O	debrisoquin
18	123	133	O	hydroxylase
19	136	137	O	is
20	139	145	O	reduced
21	147	148	O	in
22	150	150	O	a
23	152	157	O	subset
24	159	160	O	of
25	162	164	O	the
26	166	174	O	Caucasian
27	176	185	O	population
28	188	192	O	about
29	194	195	O	7%
30	197	198	O	to
31	200	202	O	10%
32	204	205	O	of
33	207	216	O	Caucasians
34	218	220	O	are
35	222	223	O	so
36	225	230	O	called
37	232	236	O	"poor
38	238	250	O	metabolizers"
39	254	261	O	reliable
40	263	271	O	estimates
41	273	274	O	of
42	276	278	O	the
43	280	289	O	prevalence
44	291	292	O	of
45	294	300	O	reduced
46	302	305	O	P450
47	307	309	O	2D6
48	311	317	O	isozyme
49	319	326	O	activity
50	328	332	O	among
51	334	338	O	Asian
52	339	339	O	,
53	341	347	O	African
54	349	351	O	and
55	353	357	O	other
56	359	369	O	populations
57	371	373	O	are
58	375	377	O	not
59	379	381	O	yet
60	383	391	O	available
K/16:C54358

Amoxapine	3407	34073-7	a16297df-3158-48db-85e5-5cd506885556	16
Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.
1	0	3	O	Poor
2	5	16	O	metabolizers
3	18	21	O	have
4	23	28	B-E	higher
5	30	33	B-T	than
6	35	42	B-E	expected
7	44	49	B-T	plasma
8	51	64	I-T	concentrations
9	66	67	O	of
10	69	77	B-K	tricyclic
11	79	93	I-K	antidepressants
12	96	99	I-K	TCAs
13	102	105	O	when
14	107	111	O	given
15	113	117	O	usual
16	119	123	O	doses
K/10:C54358

Amoxapine	3408	34073-7	a16297df-3158-48db-85e5-5cd506885556	32
Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8 fold increase in plasma AUC of the TCA).
1	0	8	O	Depending
2	10	11	O	on
3	13	15	O	the
4	17	24	O	fraction
5	26	27	O	of
6	29	32	O	drug
7	34	44	O	metabolized
8	46	47	O	by
9	49	52	O	P450
10	54	56	B-K	2D6
11	57	57	O	,
12	59	61	O	the
13	63	70	O	increase
14	72	73	O	in
15	75	80	O	plasma
16	82	94	O	concentration
17	96	98	O	may
18	100	101	O	be
19	103	107	O	small
20	108	108	O	,
21	110	111	O	or
22	113	117	O	quite
23	119	123	O	large
24	126	126	O	8
25	128	131	O	fold
26	133	140	B-T	increase
27	142	143	I-T	in
28	145	150	I-T	plasma
29	152	154	I-T	AUC
30	156	157	O	of
31	159	161	O	the
32	163	165	O	TCA
K/10:C54355

Amoxapine	3409	34073-7	a16297df-3158-48db-85e5-5cd506885556	18
In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	19	O	certain
5	21	25	B-D	drugs
6	27	33	I-D	inhibit
7	35	37	I-D	the
8	39	46	I-D	activity
9	48	49	I-D	of
10	51	54	I-D	this
11	56	62	O	isozyme
12	64	66	O	and
13	68	71	O	make
14	73	78	O	normal
15	80	91	O	metabolizers
16	93	100	O	resemble
17	102	105	O	poor
18	107	118	O	metabolizers
NULL

Amoxapine	3410	34073-7	a16297df-3158-48db-85e5-5cd506885556	25
An individual who is stable on a given dose of TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.
1	0	1	O	An
2	3	12	O	individual
3	14	16	O	who
4	18	19	O	is
5	21	26	O	stable
6	28	29	O	on
7	31	31	O	a
8	33	37	O	given
9	39	42	O	dose
10	44	45	O	of
11	47	49	O	TCA
12	51	53	O	may
13	55	60	O	become
14	62	69	O	abruptly
15	71	75	O	toxic
16	77	80	O	when
17	82	86	O	given
18	88	90	O	one
19	92	93	O	of
20	95	99	O	these
21	101	110	O	inhibiting
22	112	116	O	drugs
23	118	119	O	as
24	121	131	O	concomitant
25	133	139	O	therapy
NULL

Amoxapine	3411	34073-7	a16297df-3158-48db-85e5-5cd506885556	40
The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).
1	0	2	O	The
2	4	8	B-K	drugs
3	10	13	I-K	that
4	15	21	I-K	inhibit
5	23	32	I-K	cytochrome
6	34	37	I-K	P450
7	39	41	I-K	2D6
8	43	49	O	include
9	51	54	B-T	some
10	56	59	I-T	that
11	61	63	I-T	are
12	65	67	I-T	not
13	69	79	I-T	metabolized
14	81	82	O	by
15	84	86	O	the
16	88	93	O	enzyme
17	96	104	B-K	quinidine
18	107	116	O	cimetidine
19	119	121	O	and
20	123	126	O	many
21	128	131	O	that
22	133	135	O	are
23	137	146	O	substrates
24	148	150	O	for
25	152	155	O	P450
26	157	159	O	2D6
27	162	165	O	many
28	167	171	O	other
29	173	187	O	antidepressants
30	188	188	O	,
31	190	203	O	phenothiazines
32	204	204	O	,
33	206	208	O	and
34	210	212	O	the
35	214	217	O	Type
36	219	220	O	1C
37	222	236	O	antiarrhythmics
38	238	248	O	propafenone
39	250	252	O	and
40	254	263	O	flecainide
K/2:C54357 K/17:C54357

Amoxapine	3412	34073-7	a16297df-3158-48db-85e5-5cd506885556	28
While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.
1	0	4	O	While
2	6	8	O	all
3	10	12	O	the
4	14	22	O	selective
5	24	52	O	GGGGGGGG
6	55	59	O	SSRIs
7	61	61	O	,
8	63	65	O	e.g
9	67	67	O	,
10	69	78	O	fluoxetine
11	79	79	O	,
12	81	90	O	sertraline
13	91	91	O	,
14	93	95	O	and
15	97	106	O	paroxetine
16	107	107	O	,
17	109	115	O	inhibit
18	117	120	O	P450
19	122	124	O	2D6
20	125	125	O	,
21	127	130	O	they
22	132	134	O	may
23	136	139	O	vary
24	141	142	O	in
25	144	146	O	the
26	148	153	O	extent
27	155	156	O	of
28	158	167	O	inhibition
NULL

Amoxapine	3413	34073-7	a16297df-3158-48db-85e5-5cd506885556	25
The extent to which SSRI-TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.
1	0	2	O	The
2	4	9	O	extent
3	11	12	O	to
4	14	18	O	which
5	20	23	O	SSRI
6	25	27	O	TCA
7	29	40	O	interactions
8	42	44	O	may
9	46	49	O	pose
10	51	58	O	clinical
11	60	67	B-E	problems
12	69	72	O	will
13	74	79	O	depend
14	81	82	B-E	on
15	84	86	O	the
16	88	93	O	degree
17	95	96	O	of
18	98	107	O	inhibition
19	109	111	O	and
20	113	115	O	the
21	117	132	O	pharmacokinetics
22	134	135	O	of
23	137	139	O	the
24	141	144	O	SSRI
25	146	153	O	involved
NULL

Amoxapine	3414	34073-7	a16297df-3158-48db-85e5-5cd506885556	26
Nevertheless, caution is indicated in the co-administration of TCAs with any of the SSRIs and also in switching from one class to the other.
1	0	11	O	Nevertheless
2	12	12	O	,
3	14	20	B-T	caution
4	22	23	O	is
5	25	33	O	indicated
6	35	36	O	in
7	38	40	O	the
8	42	43	O	co
9	45	58	O	administration
10	60	61	O	of
11	63	66	O	TCAs
12	68	71	O	with
13	73	75	O	any
14	77	78	O	of
15	80	82	O	the
16	84	88	O	SSRIs
17	90	92	O	and
18	94	97	O	also
19	99	100	O	in
20	102	110	O	switching
21	112	115	O	from
22	117	119	O	one
23	121	125	O	class
24	127	128	O	to
25	130	132	O	the
26	134	138	O	other
NULL

Amoxapine	3415	34073-7	a16297df-3158-48db-85e5-5cd506885556	38
Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).
1	0	1	O	Of
2	3	12	O	particular
3	14	23	O	importance
4	24	24	O	,
5	26	35	O	sufficient
6	37	40	O	time
7	42	45	O	must
8	47	52	O	elapse
9	54	59	O	before
10	61	70	O	initiating
11	72	74	O	TCA
12	76	84	O	treatment
13	86	87	O	in
14	89	89	O	a
15	91	97	O	patient
16	99	103	O	being
17	105	113	O	withdrawn
18	115	118	O	from
19	120	129	O	fluoxetine
20	130	130	O	,
21	132	136	O	given
22	138	140	O	the
23	142	145	O	long
24	147	150	O	half
25	152	155	O	life
26	157	158	O	of
27	160	162	O	the
28	164	169	O	parent
29	171	173	O	and
30	175	180	O	active
31	182	191	O	metabolite
32	194	195	O	at
33	197	201	O	least
34	203	203	O	5
35	205	209	O	weeks
36	211	213	O	may
37	215	216	O	be
38	218	226	O	necessary
NULL

Amoxapine	3416	34073-7	a16297df-3158-48db-85e5-5cd506885556	29
Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	27	B-D	tricyclic
5	29	43	I-D	antidepressants
6	45	48	I-D	with
7	50	54	I-D	drugs
8	56	59	I-D	that
9	61	63	I-D	can
10	65	71	I-D	inhibit
11	73	82	I-D	cytochrome
12	84	87	I-D	P450
13	89	91	I-D	2D6
14	93	95	O	may
15	97	103	O	require
16	105	109	B-E	lower
17	111	115	I-E	doses
18	117	120	I-E	than
19	122	128	O	usually
20	130	139	O	prescribed
21	141	143	O	for
22	145	150	O	either
23	152	154	O	the
24	156	164	O	tricyclic
25	166	179	B-D	antidepressant
26	181	182	O	or
27	184	186	O	the
28	188	192	O	other
29	194	197	O	drug
D/4:16:1 D/25:16:1

Amoxapine	3417	34073-7	a16297df-3158-48db-85e5-5cd506885556	23
Furthermore, whenever one of these other drugs is withdrawn from co-therapy, an increased dose of tricyclic antidepressant may be required.
1	0	10	O	Furthermore
2	11	11	O	,
3	13	20	O	whenever
4	22	24	O	one
5	26	27	O	of
6	29	33	O	these
7	35	39	O	other
8	41	45	O	drugs
9	47	48	O	is
10	50	58	O	withdrawn
11	60	63	O	from
12	65	66	O	co
13	68	74	O	therapy
14	75	75	O	,
15	77	78	O	an
16	80	88	O	increased
17	90	93	O	dose
18	95	96	O	of
19	98	106	O	tricyclic
20	108	121	O	antidepressant
21	123	125	O	may
22	127	128	O	be
23	130	137	O	required
NULL

Amoxapine	3418	34073-7	a16297df-3158-48db-85e5-5cd506885556	28
It is desirable to monitor TCA plasma levels whenever a TCA is going to be co-administered with another drug known to be an inhibitor of P450 2D6.
1	0	1	O	It
2	3	4	O	is
3	6	14	O	desirable
4	16	17	O	to
5	19	25	O	monitor
6	27	29	O	TCA
7	31	36	O	plasma
8	38	43	O	levels
9	45	52	O	whenever
10	54	54	O	a
11	56	58	O	TCA
12	60	61	O	is
13	63	67	O	going
14	69	70	O	to
15	72	73	O	be
16	75	76	O	co
17	78	89	O	administered
18	91	94	O	with
19	96	102	O	another
20	104	107	O	drug
21	109	113	O	known
22	115	116	O	to
23	118	119	O	be
24	121	122	O	an
25	124	132	O	inhibitor
26	134	135	O	of
27	137	140	O	P450
28	142	144	O	2D6
NULL

Amoxapine	3419	34090-1	a16297df-3158-48db-85e5-5cd506885556	12
Amoxapine is an antidepressant with a mild sedative component to its action.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	14	O	an
4	16	29	O	antidepressant
5	31	34	O	with
6	36	36	O	a
7	38	41	O	mild
8	43	50	O	sedative
9	52	60	O	component
10	62	63	O	to
11	65	67	O	its
12	69	74	O	action
NULL

Amoxapine	3420	34090-1	a16297df-3158-48db-85e5-5cd506885556	12
The mechanism of its clinical action in man is not well understood.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	19	O	its
5	21	28	O	clinical
6	30	35	O	action
7	37	38	O	in
8	40	42	O	man
9	44	45	O	is
10	47	49	O	not
11	51	54	O	well
12	56	65	O	understood
NULL

Amoxapine	3421	34090-1	a16297df-3158-48db-85e5-5cd506885556	20
In animals, amoxapine reduced the uptake of norepinephrine and serotonin and blocked the response of dopamine receptors to dopamine.
1	0	1	O	In
2	3	9	O	animals
3	10	10	O	,
4	12	20	O	XXXXXXXX
5	22	28	O	reduced
6	30	32	O	the
7	34	39	O	uptake
8	41	42	O	of
9	44	57	O	norepinephrine
10	59	61	O	and
11	63	71	O	serotonin
12	73	75	O	and
13	77	83	O	blocked
14	85	87	O	the
15	89	96	O	response
16	98	99	O	of
17	101	108	O	dopamine
18	110	118	O	receptors
19	120	121	O	to
20	123	130	O	dopamine
NULL

Amoxapine	3422	34090-1	a16297df-3158-48db-85e5-5cd506885556	7
Amoxapine is not a monoamine oxidase inhibitor.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	15	O	not
4	17	17	O	a
5	19	27	O	monoamine
6	29	35	O	oxidase
7	37	45	O	inhibitor
NULL

Amoxapine	3423	34090-1	a16297df-3158-48db-85e5-5cd506885556	14
Amoxapine is absorbed rapidly and reaches peak blood levels approximately 90 minutes after ingestion.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	20	O	absorbed
4	22	28	O	rapidly
5	30	32	O	and
6	34	40	O	reaches
7	42	45	O	peak
8	47	51	O	blood
9	53	58	O	levels
10	60	72	O	approximately
11	74	75	O	90
12	77	83	O	minutes
13	85	89	O	after
14	91	99	O	ingestion
NULL

Amoxapine	3424	34090-1	a16297df-3158-48db-85e5-5cd506885556	8
The main route of excretion is the kidney.
1	0	2	O	The
2	4	7	O	main
3	9	13	O	route
4	15	16	O	of
5	18	26	O	excretion
6	28	29	O	is
7	31	33	O	the
8	35	40	O	kidney
NULL

Amoxapine	3425	34090-1	a16297df-3158-48db-85e5-5cd506885556	13
In vitro tests show that amoxapine binding to human serum is approximately 90%.
1	0	1	O	In
2	3	7	O	vitro
3	9	13	O	tests
4	15	18	O	show
5	20	23	O	that
6	25	33	O	XXXXXXXX
7	35	41	O	binding
8	43	44	O	to
9	46	50	O	human
10	52	56	O	serum
11	58	59	O	is
12	61	73	O	approximately
13	75	77	O	90%
NULL

Amoxapine	3426	34090-1	a16297df-3158-48db-85e5-5cd506885556	14
In man, amoxapine serum concentration declines with a half-life of eight hours.
1	0	1	O	In
2	3	5	O	man
3	6	6	O	,
4	8	16	O	XXXXXXXX
5	18	22	O	serum
6	24	36	O	concentration
7	38	45	O	declines
8	47	50	O	with
9	52	52	O	a
10	54	57	O	half
11	59	62	O	life
12	64	65	O	of
13	67	71	O	eight
14	73	77	O	hours
NULL

Amoxapine	3427	34090-1	a16297df-3158-48db-85e5-5cd506885556	18
However, the major metabolite, 8-hydroxyamoxapine, has a biologic half-life of 30 hours.
1	0	6	O	However
2	7	7	O	,
3	9	11	O	the
4	13	17	O	major
5	19	28	O	metabolite
6	29	29	O	,
7	31	31	O	8
8	33	39	O	hydroxy
9	40	48	O	XXXXXXXX
10	49	49	O	,
11	51	53	O	has
12	55	55	O	a
13	57	64	O	biologic
14	66	69	O	half
15	71	74	O	life
16	76	77	O	of
17	79	80	O	30
18	82	86	O	hours
NULL

Amoxapine	3428	34090-1	a16297df-3158-48db-85e5-5cd506885556	11
Metabolites are excreted in the urine in conjugated form as glucuronides.
1	0	10	O	Metabolites
2	12	14	O	are
3	16	23	O	excreted
4	25	26	O	in
5	28	30	O	the
6	32	36	O	urine
7	38	39	O	in
8	41	50	O	conjugated
9	52	55	O	form
10	57	58	O	as
11	60	71	O	glucuronides
NULL

Amoxapine	3429	34090-1	a16297df-3158-48db-85e5-5cd506885556	18
Clinical studies have demonstrated that amoxapine has a more rapid onset of action than either amitriptyline or imipramine.
1	0	7	O	Clinical
2	9	15	O	studies
3	17	20	O	have
4	22	33	O	demonstrated
5	35	38	O	that
6	40	48	O	XXXXXXXX
7	50	52	O	has
8	54	54	O	a
9	56	59	O	more
10	61	65	O	rapid
11	67	71	B-T	onset
12	73	74	I-T	of
13	76	81	I-T	action
14	83	86	O	than
15	88	93	O	either
16	95	107	B-K	amitriptyline
17	109	110	O	or
18	112	121	B-K	imipramine
K/16:C54357 K/18:C54357

Amoxapine	3430	34090-1	a16297df-3158-48db-85e5-5cd506885556	21
The initial clinical effect may occur within four to seven days and occurs within two weeks in over 80% of responders.
1	0	2	O	The
2	4	10	O	initial
3	12	19	O	clinical
4	21	26	O	effect
5	28	30	O	may
6	32	36	O	occur
7	38	43	O	within
8	45	48	O	four
9	50	51	O	to
10	53	57	O	seven
11	59	62	O	days
12	64	66	O	and
13	68	73	O	occurs
14	75	80	O	within
15	82	84	O	two
16	86	90	O	weeks
17	92	93	O	in
18	95	98	O	over
19	100	102	O	80%
20	104	105	O	of
21	107	116	O	responders
NULL

