Halcion	5013	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	26
The concomitant use of benzodiazepines and opioids increases the risk of respiratory depression because of actions at different receptor sites in the CNS that control respiration.
1	0	2	O	The
2	4	14	O	concomitant
3	16	18	O	use
4	20	21	O	of
5	23	37	B-D	benzodiazepines
6	39	41	O	and
7	43	49	B-D	opioids
8	51	59	O	increases
9	61	63	O	the
10	65	68	O	risk
11	70	71	O	of
12	73	83	B-E	respiratory
13	85	94	I-E	depression
14	96	102	O	because
15	104	105	O	of
16	107	113	O	actions
17	115	116	O	at
18	118	126	O	different
19	128	135	B-E	receptor
20	137	141	I-E	sites
21	143	144	I-E	in
22	146	148	I-E	the
23	150	152	I-E	CNS
24	154	157	I-E	that
25	159	165	O	control
26	167	177	B-D	respiration
D/5:12:1 D/5:19:1 D/7:12:1 D/7:19:1 D/26:12:1 D/26:19:1

Halcion	5014	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	12
Benzodiazepines interact at GABAA sites and opioids interact primarily at mu receptors.
1	0	14	O	Benzodiazepines
2	16	23	O	interact
3	25	26	O	at
4	28	32	O	GABAA
5	34	38	O	sites
6	40	42	O	and
7	44	50	O	opioids
8	52	59	B-U	interact
9	61	69	B-D	primarily
10	71	72	B-U	at
11	74	75	B-D	mu
12	77	85	I-D	receptors
NULL

Halcion	5015	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	19
When benzodiazepines and opioids are combined, the potential for benzodiazepines to significantly worsen opioid-related respiratory depression exists.
1	0	3	O	When
2	5	19	B-D	benzodiazepines
3	21	23	O	and
4	25	31	B-D	opioids
5	33	35	O	are
6	37	44	O	combined
7	45	45	O	,
8	47	49	O	the
9	51	59	O	potential
10	61	63	O	for
11	65	79	B-D	benzodiazepines
12	81	82	O	to
13	84	96	O	significantly
14	98	103	O	worsen
15	105	110	O	opioid
16	112	118	O	related
17	120	130	O	respiratory
18	132	141	O	depression
19	143	148	O	exists
NULL

Halcion	5016	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	21
Limit dosage and duration of concomitant use of benzodiazepines and opioids, and monitor patients closely for respiratory depression and sedation.
1	0	4	B-T	Limit
2	6	11	I-T	dosage
3	13	15	O	and
4	17	24	O	duration
5	26	27	O	of
6	29	39	O	concomitant
7	41	43	O	use
8	45	46	O	of
9	48	62	B-U	benzodiazepines
10	64	66	O	and
11	68	74	B-U	opioids
12	75	75	O	,
13	77	79	O	and
14	81	87	B-T	monitor
15	89	96	O	patients
16	98	104	O	closely
17	106	108	O	for
18	110	120	O	respiratory
19	122	131	O	depression
20	133	135	O	and
21	137	144	O	sedation
NULL

Halcion	5017	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	10
Both pharmacodynamic and pharmacokinetic interactions have been reported with benzodiazepines.
1	0	3	O	Both
2	5	19	O	pharmacodynamic
3	21	23	O	and
4	25	39	O	pharmacokinetic
5	41	52	O	interactions
6	54	57	O	have
7	59	62	O	been
8	64	71	O	reported
9	73	76	O	with
10	78	92	B-D	benzodiazepines
NULL

Halcion	5018	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	30
In particular, triazolam produces additive CNS depressant effects when coadministered with other psychotropic medications, anticonvulsants, antihistamines, ethanol, and other drugs which themselves produce CNS depression.
1	0	1	O	In
2	3	12	O	particular
3	13	13	O	,
4	15	23	O	XXXXXXXX
5	25	32	O	produces
6	34	41	B-E	additive
7	43	45	I-E	CNS
8	47	56	I-E	depressant
9	58	64	I-E	effects
10	66	69	O	when
11	71	84	B-D	coadministered
12	86	89	O	with
13	91	95	O	other
14	97	108	B-D	psychotropic
15	110	120	I-D	medications
16	121	121	O	,
17	123	137	O	anticonvulsants
18	138	138	O	,
19	140	153	O	antihistamines
20	154	154	O	,
21	156	162	B-D	ethanol
22	163	163	O	,
23	165	167	O	and
24	169	173	O	other
25	175	179	B-D	drugs
26	181	185	I-D	which
27	187	196	I-D	themselves
28	198	204	I-D	produce
29	206	208	I-D	CNS
30	210	219	I-D	depression
D/11:6:1 D/14:6:1 D/21:6:1 D/25:6:1

Halcion	5019	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	15
The initial step in triazolam metabolism is hydroxylation catalyzed by cytochrome P450 3A (CYP 3A).
1	0	2	O	The
2	4	10	O	initial
3	12	15	O	step
4	17	18	O	in
5	20	28	O	XXXXXXXX
6	30	39	O	metabolism
7	41	42	O	is
8	44	56	O	hydroxylation
9	58	66	O	catalyzed
10	68	69	O	by
11	71	80	O	cytochrome
12	82	85	O	P450
13	87	88	O	3A
14	91	93	O	CYP
15	95	96	B-K	3A
K/15:C54358

Halcion	5020	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	16
Drugs which inhibit this metabolic pathway may have a profound effect on the clearance of triazolam.
1	0	4	B-D	Drugs
2	6	10	I-D	which
3	12	18	I-D	inhibit
4	20	23	I-D	this
5	25	33	I-D	metabolic
6	35	41	O	pathway
7	43	45	O	may
8	47	50	O	have
9	52	52	O	a
10	54	61	B-E	profound
11	63	68	I-E	effect
12	70	71	I-E	on
13	73	75	I-E	the
14	77	85	I-E	clearance
15	87	88	I-E	of
16	90	98	O	XXXXXXXX
D/1:10:1

Halcion	5021	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	15
HALCION is contraindicated with ketoconzaole, itraconazole, nefazodone, and several HIV protease inhibitors.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	25	B-T	contraindicated
4	27	30	O	with
5	32	43	O	ketoconzaole
6	44	44	O	,
7	46	57	B-U	itraconazole
8	58	58	O	,
9	60	69	O	nefazodone
10	70	70	O	,
11	72	74	O	and
12	76	82	O	several
13	84	86	O	HIV
14	88	95	B-U	protease
15	97	106	I-U	inhibitors
NULL

Halcion	5022	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	24
Coadministration of isoniazid increased the maximum plasma concentration of triazolam by 20%, decreased clearance by 42%, and increased half-life by 31%.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	28	B-K	isoniazid
4	30	38	O	increased
5	40	42	O	the
6	44	50	O	maximum
7	52	57	O	plasma
8	59	71	O	concentration
9	73	74	O	of
10	76	84	O	XXXXXXXX
11	86	87	O	by
12	89	91	O	20%
13	92	92	O	,
14	94	102	B-T	decreased
15	104	112	I-T	clearance
16	114	115	O	by
17	117	119	O	42%
18	120	120	O	,
19	122	124	O	and
20	126	134	B-T	increased
21	136	139	I-T	half
22	141	144	I-T	life
23	146	147	O	by
24	149	151	O	31%
K/3:C54611

Halcion	5023	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	22
Coadministration of oral contraceptives increased maximum plasma concentration by 6%, decreased clearance by 32%, and increased half-life by 16%.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	23	B-K	oral
4	25	38	I-K	contraceptives
5	40	48	O	increased
6	50	56	O	maximum
7	58	63	O	plasma
8	65	77	O	concentration
9	79	80	O	by
10	82	83	O	6%
11	84	84	O	,
12	86	94	B-T	decreased
13	96	104	I-T	clearance
14	106	107	O	by
15	109	111	O	32%
16	112	112	O	,
17	114	116	O	and
18	118	126	B-T	increased
19	128	131	I-T	half
20	133	136	I-T	life
21	138	139	O	by
22	141	143	O	16%
K/3:C54611

Halcion	5024	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	30
Coadministration of grapefruit juice increased the maximum plasma concentration of triazolam by 25%, increased the area under the concentration curve by 48%, and increased half-life by 18%.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	29	B-K	grapefruit
4	31	35	I-K	juice
5	37	45	O	increased
6	47	49	O	the
7	51	57	O	maximum
8	59	64	O	plasma
9	66	78	O	concentration
10	80	81	O	of
11	83	91	O	XXXXXXXX
12	93	94	O	by
13	96	98	O	25%
14	99	99	O	,
15	101	109	B-T	increased
16	111	113	I-T	the
17	115	118	I-T	area
18	120	124	I-T	under
19	126	128	I-T	the
20	130	142	I-T	concentration
21	144	148	I-T	curve
22	150	151	O	by
23	153	155	O	48%
24	156	156	O	,
25	158	160	O	and
26	162	170	B-T	increased
27	172	175	I-T	half
28	177	180	I-T	life
29	182	183	O	by
30	185	187	B-K	18%
K/3:C54602 K/30:C54602

Halcion	5025	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	27
Available data from clinical studies of benzodiazepines other than triazolam suggest a possible drug interaction with triazolam for the following: fluvoxamine, diltiazem, and verapamil.
1	0	8	O	Available
2	10	13	O	data
3	15	18	O	from
4	20	27	O	clinical
5	29	35	O	studies
6	37	38	O	of
7	40	54	O	benzodiazepines
8	56	60	O	other
9	62	65	O	than
10	67	75	O	XXXXXXXX
11	77	83	O	suggest
12	85	85	O	a
13	87	94	O	possible
14	96	99	O	drug
15	101	111	O	interaction
16	113	116	O	with
17	118	126	O	XXXXXXXX
18	128	130	O	for
19	132	134	O	the
20	136	144	O	following
21	145	145	O	:
22	147	157	O	fluvoxamine
23	158	158	O	,
24	160	168	O	diltiazem
25	169	169	O	,
26	171	173	O	and
27	175	183	O	verapamil
NULL

Halcion	5026	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	21
Data from in vitro studies of triazolam suggest a possible drug interaction with triazolam for the following: sertraline and paroxetine.
1	0	3	O	Data
2	5	8	O	from
3	10	11	O	in
4	13	17	O	vitro
5	19	25	O	studies
6	27	28	O	of
7	30	38	O	XXXXXXXX
8	40	46	O	suggest
9	48	48	O	a
10	50	57	O	possible
11	59	62	O	drug
12	64	74	O	interaction
13	76	79	O	with
14	81	89	O	XXXXXXXX
15	91	93	O	for
16	95	97	O	the
17	99	107	O	following
18	108	108	O	:
19	110	119	O	sertraline
20	121	123	O	and
21	125	134	O	paroxetine
NULL

Halcion	5027	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	31
Data from in vitro studies of benzodiazepines other than triazolam suggest a possible drug interaction with triazolam for the following: ergotamine, cyclosporine, amiodarone, nicardipine, and nifedipine.
1	0	3	O	Data
2	5	8	O	from
3	10	11	O	in
4	13	17	O	vitro
5	19	25	O	studies
6	27	28	O	of
7	30	44	O	benzodiazepines
8	46	50	O	other
9	52	55	O	than
10	57	65	O	XXXXXXXX
11	67	73	O	suggest
12	75	75	O	a
13	77	84	O	possible
14	86	89	O	drug
15	91	101	O	interaction
16	103	106	O	with
17	108	116	O	XXXXXXXX
18	118	120	O	for
19	122	124	O	the
20	126	134	O	following
21	135	135	O	:
22	137	146	O	ergotamine
23	147	147	O	,
24	149	160	O	cyclosporine
25	161	161	O	,
26	163	172	O	amiodarone
27	173	173	O	,
28	175	185	O	nicardipine
29	186	186	O	,
30	188	190	O	and
31	192	201	O	nifedipine
NULL

Halcion	5028	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	12
Caution is recommended during coadministration of any of these drugs with triazolam.
1	0	6	B-T	Caution
2	8	9	O	is
3	11	21	O	recommended
4	23	28	O	during
5	30	45	O	coadministration
6	47	48	O	of
7	50	52	O	any
8	54	55	O	of
9	57	61	O	these
10	63	67	O	drugs
11	69	72	O	with
12	74	82	O	XXXXXXXX
NULL

Halcion	5029	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	29
Coadministration of ranitidine increased the maximum plasma concentration of triazolam by 30%, increased the area under the concentration curve by 27%, and increased half-life by 3.3%.
1	0	15	O	Coadministration
2	17	18	O	of
3	20	29	B-K	ranitidine
4	31	39	O	increased
5	41	43	O	the
6	45	51	O	maximum
7	53	58	O	plasma
8	60	72	O	concentration
9	74	75	O	of
10	77	85	O	XXXXXXXX
11	87	88	O	by
12	90	92	O	30%
13	93	93	O	,
14	95	103	B-T	increased
15	105	107	I-T	the
16	109	112	I-T	area
17	114	118	I-T	under
18	120	122	I-T	the
19	124	136	I-T	concentration
20	138	142	I-T	curve
21	144	145	O	by
22	147	149	O	27%
23	150	150	O	,
24	152	154	O	and
25	156	164	B-T	increased
26	166	169	I-T	half
27	171	174	I-T	life
28	176	177	O	by
29	179	182	O	3.3%
K/3:C54611

Halcion	5030	34073-7	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	7
Caution is recommended during coadministration with triazolam.
1	0	6	B-T	Caution
2	8	9	O	is
3	11	21	O	recommended
4	23	28	O	during
5	30	45	O	coadministration
6	47	50	O	with
7	52	60	O	XXXXXXXX
NULL

Halcion	5031	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	22
Triazolam is a hypnotic with a short mean plasma half-life reported to be in the range of 1.5 to 5.5 hours.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	13	O	a
4	15	22	O	hypnotic
5	24	27	O	with
6	29	29	O	a
7	31	35	O	short
8	37	40	O	mean
9	42	47	O	plasma
10	49	52	O	half
11	54	57	O	life
12	59	66	O	reported
13	68	69	O	to
14	71	72	O	be
15	74	75	O	in
16	77	79	O	the
17	81	85	O	range
18	87	88	O	of
19	90	92	O	1.5
20	94	95	O	to
21	97	99	O	5.5
22	101	105	O	hours
NULL

Halcion	5032	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	29
In normal subjects treated for 7 days with four times the recommended dosage, there was no evidence of altered systemic bioavailability, rate of elimination, or accumulation.
1	0	1	O	In
2	3	8	O	normal
3	10	17	O	subjects
4	19	25	O	treated
5	27	29	O	for
6	31	31	O	7
7	33	36	O	days
8	38	41	O	with
9	43	46	O	four
10	48	52	O	times
11	54	56	O	the
12	58	68	O	recommended
13	70	75	O	dosage
14	76	76	O	,
15	78	82	O	there
16	84	86	O	was
17	88	89	O	no
18	91	98	O	evidence
19	100	101	O	of
20	103	109	O	altered
21	111	118	O	systemic
22	120	134	O	bioavailability
23	135	135	O	,
24	137	140	O	rate
25	142	143	O	of
26	145	155	O	elimination
27	156	156	O	,
28	158	159	O	or
29	161	172	O	accumulation
NULL

Halcion	5033	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	11
Peak plasma levels are reached within 2 hours following oral administration.
1	0	3	O	Peak
2	5	10	O	plasma
3	12	17	O	levels
4	19	21	O	are
5	23	29	O	reached
6	31	36	O	within
7	38	38	O	2
8	40	44	O	hours
9	46	54	O	following
10	56	59	O	oral
11	61	74	O	administration
NULL

Halcion	5034	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	22
Following recommended doses of HALCION, triazolam peak plasma levels in the range of 1 to 6 ng/mL are seen.
1	0	8	O	Following
2	10	20	O	recommended
3	22	26	O	doses
4	28	29	O	of
5	31	37	O	XXXXXXXX
6	38	38	O	,
7	40	48	O	XXXXXXXX
8	50	53	O	peak
9	55	60	O	plasma
10	62	67	O	levels
11	69	70	O	in
12	72	74	O	the
13	76	80	O	range
14	82	83	O	of
15	85	85	O	1
16	87	88	O	to
17	90	90	O	6
18	92	93	O	ng
19	94	94	O	/
20	95	96	O	mL
21	98	100	O	are
22	102	105	O	seen
NULL

Halcion	5035	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	10
The plasma levels achieved are proportional to the dose given.
1	0	2	O	The
2	4	9	O	plasma
3	11	16	O	levels
4	18	25	O	achieved
5	27	29	O	are
6	31	42	O	proportional
7	44	45	O	to
8	47	49	O	the
9	51	54	O	dose
10	56	60	O	given
NULL

Halcion	5036	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	21
Triazolam and its metabolites, principally as conjugated glucuronides, which are presumably inactive, are excreted primarily in the urine.
1	0	8	O	XXXXXXXX
2	10	12	O	and
3	14	16	O	its
4	18	28	O	metabolites
5	29	29	O	,
6	31	41	O	principally
7	43	44	O	as
8	46	55	O	conjugated
9	57	68	O	glucuronides
10	69	69	O	,
11	71	75	O	which
12	77	79	O	are
13	81	90	O	presumably
14	92	99	O	inactive
15	100	100	O	,
16	102	104	O	are
17	106	113	O	excreted
18	115	123	O	primarily
19	125	126	O	in
20	128	130	O	the
21	132	136	O	urine
NULL

Halcion	5037	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	10
Only small amounts of unmetabolized triazolam appear in the urine.
1	0	3	O	Only
2	5	9	O	small
3	11	17	O	amounts
4	19	20	O	of
5	22	34	O	unmetabolized
6	36	44	O	XXXXXXXX
7	46	51	O	appear
8	53	54	O	in
9	56	58	O	the
10	60	64	O	urine
NULL

Halcion	5038	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	10
The two primary metabolites accounted for 79.9% of urinary excretion.
1	0	2	O	The
2	4	6	O	two
3	8	14	O	primary
4	16	26	O	metabolites
5	28	36	O	accounted
6	38	40	O	for
7	42	46	O	79.9%
8	48	49	O	of
9	51	57	O	urinary
10	59	67	O	excretion
NULL

Halcion	5039	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	10
Urinary excretion appeared to be biphasic in its time course.
1	0	6	O	Urinary
2	8	16	O	excretion
3	18	25	O	appeared
4	27	28	O	to
5	30	31	O	be
6	33	40	O	biphasic
7	42	43	O	in
8	45	47	O	its
9	49	52	O	time
10	54	59	O	course
NULL

Halcion	5040	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	27
HALCION Tablets 0.5 mg, in two separate studies, did not affect the prothrombin times or plasma warfarin levels in male volunteers administered sodium warfarin orally.
1	0	6	O	XXXXXXXX
2	8	14	O	Tablets
3	16	18	O	0.5
4	20	21	O	mg
5	22	22	O	,
6	24	25	O	in
7	27	29	O	two
8	31	38	O	separate
9	40	46	O	studies
10	47	47	O	,
11	49	51	O	did
12	53	55	O	not
13	57	62	O	affect
14	64	66	O	the
15	68	78	O	prothrombin
16	80	84	O	times
17	86	87	O	or
18	89	94	O	plasma
19	96	103	O	warfarin
20	105	110	O	levels
21	112	113	O	in
22	115	118	O	male
23	120	129	O	volunteers
24	131	142	O	administered
25	144	149	O	sodium
26	151	158	O	warfarin
27	160	165	O	orally
NULL

Halcion	5041	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	16
Extremely high concentrations of triazolam do not displace bilirubin bound to human serum albumin in vitro.
1	0	8	O	Extremely
2	10	13	O	high
3	15	28	O	concentrations
4	30	31	O	of
5	33	41	O	XXXXXXXX
6	43	44	B-T	do
7	46	48	I-T	not
8	50	57	O	displace
9	59	67	O	bilirubin
10	69	73	O	bound
11	75	76	O	to
12	78	82	O	human
13	84	88	O	serum
14	90	96	O	albumin
15	98	99	O	in
16	101	105	O	vitro
NULL

Halcion	5042	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	8
Triazolam 14C was administered orally to pregnant mice.
1	0	8	O	XXXXXXXX
2	10	12	O	14C
3	14	16	O	was
4	18	29	O	administered
5	31	36	O	orally
6	38	39	O	to
7	41	48	O	pregnant
8	50	53	O	mice
NULL

Halcion	5043	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	22
Drug-related material appeared uniformly distributed in the fetus with 14C concentrations approximately the same as in the brain of the mother.
1	0	3	O	Drug
2	5	11	O	related
3	13	20	O	material
4	22	29	O	appeared
5	31	39	O	uniformly
6	41	51	O	distributed
7	53	54	O	in
8	56	58	O	the
9	60	64	O	fetus
10	66	69	O	with
11	71	73	O	14C
12	75	88	O	concentrations
13	90	102	O	approximately
14	104	106	O	the
15	108	111	O	same
16	113	114	O	as
17	116	117	O	in
18	119	121	O	the
19	123	127	O	brain
20	129	130	O	of
21	132	134	O	the
22	136	141	O	mother
NULL

Halcion	5044	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	25
In sleep laboratory studies, HALCION Tablets significantly decreased sleep latency, increased the duration of sleep, and decreased the number of nocturnal awakenings.
1	0	1	O	In
2	3	7	O	sleep
3	9	18	O	laboratory
4	20	26	O	studies
5	27	27	O	,
6	29	35	O	XXXXXXXX
7	37	43	O	Tablets
8	45	57	O	significantly
9	59	67	O	decreased
10	69	73	O	sleep
11	75	81	O	latency
12	82	82	O	,
13	84	92	O	increased
14	94	96	O	the
15	98	105	O	duration
16	107	108	O	of
17	110	114	O	sleep
18	115	115	O	,
19	117	119	O	and
20	121	129	O	decreased
21	131	133	O	the
22	135	140	O	number
23	142	143	O	of
24	145	153	O	nocturnal
25	155	164	O	awakenings
NULL

Halcion	5045	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	32
After 2 weeks of consecutive nightly administration, the drug's effect on total wake time is decreased, and the values recorded in the last third of the night approach baseline levels.
1	0	4	O	After
2	6	6	O	2
3	8	12	O	weeks
4	14	15	O	of
5	17	27	O	consecutive
6	29	35	O	nightly
7	37	50	O	administration
8	51	51	O	,
9	53	55	O	the
10	57	62	O	drug's
11	64	69	O	effect
12	71	72	O	on
13	74	78	O	total
14	80	83	O	wake
15	85	88	O	time
16	90	91	O	is
17	93	101	O	decreased
18	102	102	O	,
19	104	106	O	and
20	108	110	O	the
21	112	117	O	values
22	119	126	O	recorded
23	128	129	O	in
24	131	133	O	the
25	135	138	O	last
26	140	144	O	third
27	146	147	O	of
28	149	151	O	the
29	153	157	O	night
30	159	166	O	approach
31	168	175	O	baseline
32	177	182	O	levels
NULL

Halcion	5046	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	42
On the first and/or second night after drug discontinuance (first or second post-drug night), total time asleep, percentage of time spent sleeping, and rapidity of falling asleep frequently were significantly less than on baseline (predrug) nights.
1	0	1	O	On
2	3	5	O	the
3	7	11	O	first
4	13	15	O	and
5	16	16	O	/
6	17	18	O	or
7	20	25	O	second
8	27	31	O	night
9	33	37	O	after
10	39	42	O	drug
11	44	57	O	discontinuance
12	60	64	O	first
13	66	67	O	or
14	69	74	O	second
15	76	79	O	post
16	81	84	O	drug
17	86	90	O	night
18	92	92	O	,
19	94	98	O	total
20	100	103	O	time
21	105	110	O	asleep
22	111	111	O	,
23	113	122	O	percentage
24	124	125	O	of
25	127	130	O	time
26	132	136	O	spent
27	138	145	O	sleeping
28	146	146	O	,
29	148	150	O	and
30	152	159	O	rapidity
31	161	162	O	of
32	164	170	O	falling
33	172	177	O	asleep
34	179	188	O	frequently
35	190	193	O	were
36	195	207	O	significantly
37	209	212	O	less
38	214	217	O	than
39	219	220	O	on
40	222	229	O	baseline
41	232	238	O	predrug
42	241	246	O	nights
NULL

Halcion	5047	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	7
This effect is often called "rebound" insomnia.
1	0	3	O	This
2	5	10	O	effect
3	12	13	O	is
4	15	19	O	often
5	21	26	O	called
6	28	36	O	"rebound"
7	38	45	O	insomnia
NULL

Halcion	5048	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	34
The type and duration of hypnotic effects and the profile of unwanted effects during administration of benzodiazepine drugs may be influenced by the biologic half-life of administered drug and any active metabolites formed.
1	0	2	O	The
2	4	7	O	type
3	9	11	O	and
4	13	20	O	duration
5	22	23	O	of
6	25	32	B-D	hypnotic
7	34	40	O	effects
8	42	44	O	and
9	46	48	O	the
10	50	56	O	profile
11	58	59	O	of
12	61	68	O	unwanted
13	70	76	B-E	effects
14	78	83	O	during
15	85	98	O	administration
16	100	101	O	of
17	103	116	B-D	benzodiazepine
18	118	122	I-D	drugs
19	124	126	O	may
20	128	129	O	be
21	131	140	O	influenced
22	142	143	O	by
23	145	147	O	the
24	149	156	O	biologic
25	158	161	O	half
26	163	166	B-T	life
27	168	169	O	of
28	171	182	O	administered
29	184	187	O	drug
30	189	191	O	and
31	193	195	O	any
32	197	202	O	active
33	204	214	O	metabolites
34	216	221	O	formed
D/6:13:1 D/17:13:1

Halcion	5049	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	43
When half-lives are long, the drug or metabolites may accumulate during periods of nightly administration and be associated with impairments of cognitive and motor performance during waking hours; the possibility of interaction with other psychoactive drugs or alcohol will be enhanced.
1	0	3	O	When
2	5	8	O	half
3	10	14	O	lives
4	16	18	O	are
5	20	23	O	long
6	24	24	O	,
7	26	28	O	the
8	30	33	O	drug
9	35	36	O	or
10	38	48	O	metabolites
11	50	52	O	may
12	54	63	O	accumulate
13	65	70	O	during
14	72	78	O	periods
15	80	81	O	of
16	83	89	O	nightly
17	91	104	O	administration
18	106	108	O	and
19	110	111	O	be
20	113	122	O	associated
21	124	127	O	with
22	129	139	O	impairments
23	141	142	O	of
24	144	152	O	cognitive
25	154	156	O	and
26	158	162	O	motor
27	164	174	O	performance
28	176	181	O	during
29	183	188	O	waking
30	190	194	O	hours
31	197	199	O	the
32	201	211	O	possibility
33	213	214	O	of
34	216	226	O	interaction
35	228	231	O	with
36	233	237	O	other
37	239	250	B-K	psychoactive
38	252	256	I-K	drugs
39	258	259	O	or
40	261	267	B-K	alcohol
41	269	272	O	will
42	274	275	O	be
43	277	284	B-T	enhanced
K/37:C54357 K/40:C54357

Halcion	5050	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	39
In contrast, if half-lives are short, the drug and metabolites will be cleared before the next dose is ingested, and carry-over effects related to excessive sedation or CNS depression should be minimal or absent.
1	0	1	O	In
2	3	10	O	contrast
3	11	11	O	,
4	13	14	O	if
5	16	19	O	half
6	21	25	O	lives
7	27	29	O	are
8	31	35	O	short
9	36	36	O	,
10	38	40	O	the
11	42	45	O	drug
12	47	49	O	and
13	51	61	O	metabolites
14	63	66	O	will
15	68	69	O	be
16	71	77	O	cleared
17	79	84	O	before
18	86	88	O	the
19	90	93	O	next
20	95	98	O	dose
21	100	101	O	is
22	103	110	O	ingested
23	111	111	O	,
24	113	115	O	and
25	117	121	O	carry
26	123	126	O	over
27	128	134	O	effects
28	136	142	O	related
29	144	145	O	to
30	147	155	B-E	excessive
31	157	164	I-E	sedation
32	166	167	O	or
33	169	171	O	CNS
34	173	182	O	depression
35	184	189	O	should
36	191	192	O	be
37	194	200	O	minimal
38	202	203	O	or
39	205	210	O	absent
NULL

Halcion	5051	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	21
However, during nightly use for an extended period pharmacodynamic tolerance or adaptation to some effects of benzodiazepine hypnotics may develop.
1	0	6	O	However
2	7	7	O	,
3	9	14	O	during
4	16	22	O	nightly
5	24	26	O	use
6	28	30	O	for
7	32	33	O	an
8	35	42	O	extended
9	44	49	O	period
10	51	65	O	pharmacodynamic
11	67	75	O	tolerance
12	77	78	O	or
13	80	89	O	adaptation
14	91	92	O	to
15	94	97	O	some
16	99	105	O	effects
17	107	108	O	of
18	110	123	B-D	benzodiazepine
19	125	133	O	hypnotics
20	135	137	O	may
21	139	145	O	develop
NULL

Halcion	5052	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	45
If the drug has a short half-life of elimination, it is possible that a relative deficiency of the drug or its active metabolites (ie, in relationship to the receptor site) may occur at some point in the interval between each night's use.
1	0	1	O	If
2	3	5	O	the
3	7	10	O	drug
4	12	14	O	has
5	16	16	O	a
6	18	22	O	short
7	24	27	O	half
8	29	32	O	life
9	34	35	O	of
10	37	47	O	elimination
11	48	48	O	,
12	50	51	O	it
13	53	54	O	is
14	56	63	O	possible
15	65	68	O	that
16	70	70	O	a
17	72	79	O	relative
18	81	90	O	deficiency
19	92	93	O	of
20	95	97	O	the
21	99	102	O	drug
22	104	105	O	or
23	107	109	O	its
24	111	116	O	active
25	118	128	O	metabolites
26	131	132	O	ie
27	133	133	O	,
28	135	136	O	in
29	138	149	O	relationship
30	151	152	O	to
31	154	156	O	the
32	158	165	O	receptor
33	167	170	O	site
34	173	175	O	may
35	177	181	O	occur
36	183	184	O	at
37	186	189	O	some
38	191	195	O	point
39	197	198	O	in
40	200	202	O	the
41	204	211	O	interval
42	213	219	O	between
43	221	224	O	each
44	226	232	O	night's
45	234	236	O	use
NULL

Halcion	5053	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	49
This sequence of events may account for two clinical findings reported to occur after several weeks of nightly use of rapidly eliminated benzodiazepine hypnotics: 1) increased wakefulness during the last third of the night and 2) the appearance of increased daytime anxiety after 10 days of continuous treatment.
1	0	3	O	This
2	5	12	O	sequence
3	14	15	O	of
4	17	22	O	events
5	24	26	O	may
6	28	34	O	account
7	36	38	O	for
8	40	42	O	two
9	44	51	O	clinical
10	53	60	O	findings
11	62	69	O	reported
12	71	72	O	to
13	74	78	O	occur
14	80	84	O	after
15	86	92	O	several
16	94	98	O	weeks
17	100	101	O	of
18	103	109	O	nightly
19	111	113	O	use
20	115	116	O	of
21	118	124	O	rapidly
22	126	135	O	eliminated
23	137	150	O	benzodiazepine
24	152	160	O	hypnotics
25	161	161	O	:
26	163	163	O	1
27	166	174	O	increased
28	176	186	B-E	wakefulness
29	188	193	O	during
30	195	197	O	the
31	199	202	O	last
32	204	208	O	third
33	210	211	O	of
34	213	215	O	the
35	217	221	O	night
36	223	225	O	and
37	227	227	O	2
38	230	232	O	the
39	234	243	O	appearance
40	245	246	O	of
41	248	256	O	increased
42	258	264	B-E	daytime
43	266	272	O	anxiety
44	274	278	O	after
45	280	281	O	10
46	283	286	O	days
47	288	289	O	of
48	291	300	O	continuous
49	302	310	O	treatment
NULL

Halcion	5054	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	41
In a study of elderly (62-83 years old) versus younger subjects (21-41 years old) who received HALCION at the same dose levels (0.125 mg and 0.25 mg), the elderly experienced both greater sedation and impairment of psychomotor performance.
1	0	1	O	In
2	3	3	O	a
3	5	9	O	study
4	11	12	O	of
5	14	20	O	elderly
6	23	24	O	62
7	26	27	O	83
8	29	33	O	years
9	35	37	O	old
10	40	45	O	versus
11	47	53	O	younger
12	55	62	O	subjects
13	65	66	O	21
14	68	69	O	41
15	71	75	O	years
16	77	79	O	old
17	82	84	O	who
18	86	93	O	received
19	95	101	O	XXXXXXXX
20	103	104	O	at
21	106	108	O	the
22	110	113	O	same
23	115	118	O	dose
24	120	125	O	levels
25	128	132	O	0.125
26	134	135	O	mg
27	137	139	O	and
28	141	144	O	0.25
29	146	147	O	mg
30	149	149	O	,
31	151	153	O	the
32	155	161	O	elderly
33	163	173	O	experienced
34	175	178	O	both
35	180	186	O	greater
36	188	195	O	sedation
37	197	199	O	and
38	201	210	O	impairment
39	212	213	O	of
40	215	225	O	psychomotor
41	227	237	O	performance
NULL

Halcion	5055	34090-1	a0da0dba-a56d-486b-a45b-e8a7cdfbeac6	13
These effects resulted largely from higher plasma concentrations of triazolam in the elderly.
1	0	4	O	These
2	6	12	O	effects
3	14	21	O	resulted
4	23	29	O	largely
5	31	34	O	from
6	36	41	B-T	higher
7	43	48	I-T	plasma
8	50	63	O	concentrations
9	65	66	O	of
10	68	76	O	XXXXXXXX
11	78	79	O	in
12	81	83	O	the
13	85	91	O	elderly
NULL

