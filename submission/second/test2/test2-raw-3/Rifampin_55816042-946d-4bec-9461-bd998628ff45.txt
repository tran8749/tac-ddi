Rifampin	997	34090-1	55816042-946d-4bec-9461-bd998628ff45	8
Rifampin is readily absorbed from the gastrointestinal tract.
1	0	7	O	XXXXXXXX
2	9	10	O	is
3	12	18	O	readily
4	20	27	O	absorbed
5	29	32	O	from
6	34	36	O	the
7	38	53	O	gastrointestinal
8	55	59	O	tract
NULL

Rifampin	998	34090-1	55816042-946d-4bec-9461-bd998628ff45	15
Peak serum concentrations in healthy adults and pediatric populations vary widely from individual to individual.
1	0	3	O	Peak
2	5	9	O	serum
3	11	24	O	concentrations
4	26	27	O	in
5	29	35	O	healthy
6	37	42	O	adults
7	44	46	O	and
8	48	56	O	pediatric
9	58	68	O	populations
10	70	73	O	vary
11	75	80	O	widely
12	82	85	O	from
13	87	96	O	individual
14	98	99	O	to
15	101	110	O	individual
NULL

Rifampin	999	34090-1	55816042-946d-4bec-9461-bd998628ff45	32
Following a single 600 mg oral dose of rifampin in healthy adults, the peak serum concentration averages 7 mcg/mL but may vary from 4 to 32 mcg/mL.
1	0	8	O	Following
2	10	10	O	a
3	12	17	O	single
4	19	21	O	600
5	23	24	O	mg
6	26	29	O	oral
7	31	34	O	dose
8	36	37	O	of
9	39	46	O	XXXXXXXX
10	48	49	O	in
11	51	57	O	healthy
12	59	64	O	adults
13	65	65	O	,
14	67	69	O	the
15	71	74	O	peak
16	76	80	O	serum
17	82	94	O	concentration
18	96	103	O	averages
19	105	105	O	7
20	107	109	O	mcg
21	110	110	O	/
22	111	112	O	mL
23	114	116	O	but
24	118	120	O	may
25	122	125	O	vary
26	127	130	O	from
27	132	132	O	4
28	134	135	O	to
29	137	138	O	32
30	140	142	O	mcg
31	143	143	O	/
32	144	145	O	mL
NULL

Rifampin	1000	34090-1	55816042-946d-4bec-9461-bd998628ff45	15
Absorption of rifampin is reduced by about 30% when the drug is ingested with food.
1	0	9	O	Absorption
2	11	12	O	of
3	14	21	O	XXXXXXXX
4	23	24	O	is
5	26	32	O	reduced
6	34	35	O	by
7	37	41	O	about
8	43	45	O	30%
9	47	50	O	when
10	52	54	O	the
11	56	59	O	drug
12	61	62	O	is
13	64	71	O	ingested
14	73	76	O	with
15	78	81	O	food
NULL

Rifampin	1001	34090-1	55816042-946d-4bec-9461-bd998628ff45	7
Rifampin is widely distributed throughout the body.
1	0	7	O	XXXXXXXX
2	9	10	O	is
3	12	17	O	widely
4	19	29	O	distributed
5	31	40	O	throughout
6	42	44	O	the
7	46	49	O	body
NULL

Rifampin	1002	34090-1	55816042-946d-4bec-9461-bd998628ff45	16
It is present in effective concentrations in many organs and body fluids, including cerebrospinal fluid.
1	0	1	O	It
2	3	4	O	is
3	6	12	O	present
4	14	15	O	in
5	17	25	O	effective
6	27	40	O	concentrations
7	42	43	O	in
8	45	48	O	many
9	50	55	O	organs
10	57	59	O	and
11	61	64	O	body
12	66	71	O	fluids
13	72	72	O	,
14	74	82	O	including
15	84	96	O	cerebrospinal
16	98	102	O	fluid
NULL

Rifampin	1003	34090-1	55816042-946d-4bec-9461-bd998628ff45	16
Most of the unbound fraction is not ionized and, therefore, diffuses freely into tissues.
1	0	3	O	Most
2	5	6	O	of
3	8	10	O	the
4	12	18	O	unbound
5	20	27	O	fraction
6	29	30	O	is
7	32	34	O	not
8	36	42	O	ionized
9	44	46	O	and
10	47	47	O	,
11	49	57	O	therefore
12	58	58	O	,
13	60	67	O	diffuses
14	69	74	O	freely
15	76	79	O	into
16	81	87	O	tissues
NULL

Rifampin	1004	34090-1	55816042-946d-4bec-9461-bd998628ff45	39
In healthy adults, the mean biological half-life of rifampin in serum averages 3.35 * 0.66 hours after a 600 mg oral dose, with increases up to 5.08 * 2.45 hours reported after a 900 mg dose.
1	0	1	O	In
2	3	9	O	healthy
3	11	16	O	adults
4	17	17	O	,
5	19	21	O	the
6	23	26	O	mean
7	28	37	O	biological
8	39	42	O	half
9	44	47	O	life
10	49	50	O	of
11	52	59	O	XXXXXXXX
12	61	62	O	in
13	64	68	O	serum
14	70	77	O	averages
15	79	82	O	3.35
16	84	84	O	*
17	86	89	O	0.66
18	91	95	O	hours
19	97	101	O	after
20	103	103	O	a
21	105	107	O	600
22	109	110	O	mg
23	112	115	O	oral
24	117	120	O	dose
25	121	121	O	,
26	123	126	O	with
27	128	136	O	increases
28	138	139	O	up
29	141	142	O	to
30	144	147	O	5.08
31	149	149	O	*
32	151	154	O	2.45
33	156	160	O	hours
34	162	169	O	reported
35	171	175	O	after
36	177	177	O	a
37	179	181	O	900
38	183	184	O	mg
39	186	189	O	dose
NULL

Rifampin	1005	34090-1	55816042-946d-4bec-9461-bd998628ff45	18
With repeated administration, the half-life decreases and reaches average values of approximately 2 to 3 hours.
1	0	3	O	With
2	5	12	O	repeated
3	14	27	O	administration
4	28	28	O	,
5	30	32	O	the
6	34	37	O	half
7	39	42	O	life
8	44	52	O	decreases
9	54	56	O	and
10	58	64	O	reaches
11	66	72	O	average
12	74	79	O	values
13	81	82	O	of
14	84	96	O	approximately
15	98	98	O	2
16	100	101	O	to
17	103	103	O	3
18	105	109	O	hours
NULL

Rifampin	1006	34090-1	55816042-946d-4bec-9461-bd998628ff45	27
The half-life does not differ in patients with renal failure at doses not exceeding 600 mg daily, and consequently, no dosage adjustment is required.
1	0	2	O	The
2	4	7	O	half
3	9	12	O	life
4	14	17	O	does
5	19	21	O	not
6	23	28	O	differ
7	30	31	O	in
8	33	40	O	patients
9	42	45	O	with
10	47	51	O	renal
11	53	59	O	failure
12	61	62	O	at
13	64	68	O	doses
14	70	72	O	not
15	74	82	O	exceeding
16	84	86	O	600
17	88	89	O	mg
18	91	95	O	daily
19	96	96	O	,
20	98	100	O	and
21	102	113	O	consequently
22	114	114	O	,
23	116	117	O	no
24	119	124	O	dosage
25	126	135	O	adjustment
26	137	138	O	is
27	140	147	O	required
NULL

Rifampin	1007	34090-1	55816042-946d-4bec-9461-bd998628ff45	21
The half-life of rifampin at a dose of 720 mg daily has not been established in patients with renal failure.
1	0	2	O	The
2	4	7	O	half
3	9	12	O	life
4	14	15	O	of
5	17	24	O	XXXXXXXX
6	26	27	O	at
7	29	29	O	a
8	31	34	O	dose
9	36	37	O	of
10	39	41	O	720
11	43	44	O	mg
12	46	50	O	daily
13	52	54	O	has
14	56	58	O	not
15	60	63	O	been
16	65	75	O	established
17	77	78	O	in
18	80	87	O	patients
19	89	92	O	with
20	94	98	O	renal
21	100	106	O	failure
NULL

Rifampin	1008	34090-1	55816042-946d-4bec-9461-bd998628ff45	64
Following a single 900 mg oral dose of rifampin in patients with varying degrees of renal insufficiency, the mean half-life increased from 3.6 hours in healthy adults to 5, 7.3, and 11 hours in patients with glomerular filtration rates of 30 to 50 mL/min, less than 30 mL/min, and in anuric patients, respectively.
1	0	8	O	Following
2	10	10	O	a
3	12	17	O	single
4	19	21	O	900
5	23	24	O	mg
6	26	29	O	oral
7	31	34	O	dose
8	36	37	O	of
9	39	46	O	XXXXXXXX
10	48	49	O	in
11	51	58	O	patients
12	60	63	O	with
13	65	71	O	varying
14	73	79	O	degrees
15	81	82	O	of
16	84	88	O	renal
17	90	102	O	insufficiency
18	103	103	O	,
19	105	107	O	the
20	109	112	O	mean
21	114	117	B-T	half
22	119	122	I-T	life
23	124	132	O	increased
24	134	137	O	from
25	139	141	O	3.6
26	143	147	O	hours
27	149	150	O	in
28	152	158	O	healthy
29	160	165	O	adults
30	167	168	O	to
31	170	170	O	5
32	171	171	O	,
33	173	175	O	7.3
34	176	176	O	,
35	178	180	O	and
36	182	183	O	11
37	185	189	O	hours
38	191	192	O	in
39	194	201	O	patients
40	203	206	O	with
41	208	217	O	glomerular
42	219	228	O	filtration
43	230	234	O	rates
44	236	237	O	of
45	239	240	O	30
46	242	243	O	to
47	245	246	O	50
48	248	249	O	mL
49	250	250	O	/
50	251	253	O	min
51	254	254	O	,
52	256	259	O	less
53	261	264	O	than
54	266	267	O	30
55	269	270	O	mL
56	271	271	O	/
57	272	274	O	min
58	275	275	O	,
59	277	279	O	and
60	281	282	O	in
61	284	289	O	anuric
62	291	298	O	patients
63	299	299	O	,
64	301	312	O	respectively
NULL

Rifampin	1009	34090-1	55816042-946d-4bec-9461-bd998628ff45	12
Refer to the WARNINGS section for information regarding patients with hepatic insufficiency.
1	0	4	O	Refer
2	6	7	O	to
3	9	11	O	the
4	13	20	O	WARNINGS
5	22	28	O	section
6	30	32	O	for
7	34	44	O	information
8	46	54	O	regarding
9	56	63	O	patients
10	65	68	O	with
11	70	76	O	hepatic
12	78	90	O	insufficiency
NULL

Rifampin	1010	34090-1	55816042-946d-4bec-9461-bd998628ff45	16
After absorption, rifampin is rapidly eliminated in the bile, and an enterohepatic circulation ensues.
1	0	4	O	After
2	6	15	O	absorption
3	16	16	O	,
4	18	25	O	XXXXXXXX
5	27	28	O	is
6	30	36	O	rapidly
7	38	47	O	eliminated
8	49	50	O	in
9	52	54	O	the
10	56	59	O	bile
11	60	60	O	,
12	62	64	O	and
13	66	67	O	an
14	69	81	O	enterohepatic
15	83	93	O	circulation
16	95	100	O	ensues
NULL

Rifampin	1011	34090-1	55816042-946d-4bec-9461-bd998628ff45	25
During this process, rifampin undergoes progressive deacetylation so that nearly all the drug in the bile is in this form in about 6 hours.
1	0	5	O	During
2	7	10	O	this
3	12	18	O	process
4	19	19	O	,
5	21	28	O	XXXXXXXX
6	30	38	O	undergoes
7	40	50	O	progressive
8	52	64	O	deacetylation
9	66	67	O	so
10	69	72	O	that
11	74	79	O	nearly
12	81	83	O	all
13	85	87	O	the
14	89	92	O	drug
15	94	95	O	in
16	97	99	O	the
17	101	104	O	bile
18	106	107	O	is
19	109	110	O	in
20	112	115	O	this
21	117	120	O	form
22	122	123	O	in
23	125	129	O	about
24	131	131	O	6
25	133	137	O	hours
NULL

Rifampin	1012	34090-1	55816042-946d-4bec-9461-bd998628ff45	5
This metabolite has antibacterial activity.
1	0	3	O	This
2	5	14	O	metabolite
3	16	18	O	has
4	20	32	O	antibacterial
5	34	41	B-E	activity
NULL

Rifampin	1013	34090-1	55816042-946d-4bec-9461-bd998628ff45	11
Intestinal reabsorption is reduced by deacetylation, and elimination is facilitated.
1	0	9	O	Intestinal
2	11	22	O	reabsorption
3	24	25	O	is
4	27	33	O	reduced
5	35	36	O	by
6	38	50	O	deacetylation
7	51	51	O	,
8	53	55	O	and
9	57	67	O	elimination
10	69	70	O	is
11	72	82	O	facilitated
NULL

Rifampin	1014	34090-1	55816042-946d-4bec-9461-bd998628ff45	20
Up to 30% of a dose is excreted in the urine, with about half of this being unchanged drug.
1	0	1	O	Up
2	3	4	O	to
3	6	8	O	30%
4	10	11	O	of
5	13	13	O	a
6	15	18	O	dose
7	20	21	O	is
8	23	30	O	excreted
9	32	33	O	in
10	35	37	O	the
11	39	43	O	urine
12	44	44	O	,
13	46	49	O	with
14	51	55	O	about
15	57	60	O	half
16	62	63	O	of
17	65	68	O	this
18	70	74	O	being
19	76	84	O	unchanged
20	86	89	O	drug
NULL

Rifampin	1015	34090-1	55816042-946d-4bec-9461-bd998628ff45	35
In one study, pediatric patients 6 to 58 months old were given rifampin suspended in simple syrup or as dry powder mixed with applesauce at a dose of 10 mg/kg body weight.
1	0	1	O	In
2	3	5	O	one
3	7	11	O	study
4	12	12	O	,
5	14	22	O	pediatric
6	24	31	O	patients
7	33	33	O	6
8	35	36	O	to
9	38	39	O	58
10	41	46	O	months
11	48	50	O	old
12	52	55	O	were
13	57	61	O	given
14	63	70	O	XXXXXXXX
15	72	80	O	suspended
16	82	83	O	in
17	85	90	O	simple
18	92	96	O	syrup
19	98	99	O	or
20	101	102	O	as
21	104	106	O	dry
22	108	113	O	powder
23	115	119	O	mixed
24	121	124	O	with
25	126	135	O	applesauce
26	137	138	O	at
27	140	140	O	a
28	142	145	O	dose
29	147	148	O	of
30	150	151	O	10
31	153	154	O	mg
32	155	155	O	/
33	156	157	O	kg
34	159	162	O	body
35	164	169	O	weight
NULL

Rifampin	1016	34090-1	55816042-946d-4bec-9461-bd998628ff45	31
Peak serum concentrations of 10.7 * 3.7 and 11.5 * 5.1 mcg/mL were obtained 1 hour after preprandial ingestion of the drug suspension and the applesauce mixture, respectively.
1	0	3	O	Peak
2	5	9	O	serum
3	11	24	O	concentrations
4	26	27	O	of
5	29	32	O	10.7
6	34	34	O	*
7	36	38	O	3.7
8	40	42	O	and
9	44	47	O	11.5
10	49	49	O	*
11	51	53	O	5.1
12	55	57	O	mcg
13	58	58	O	/
14	59	60	O	mL
15	62	65	O	were
16	67	74	O	obtained
17	76	76	O	1
18	78	81	O	hour
19	83	87	O	after
20	89	99	O	preprandial
21	101	109	O	ingestion
22	111	112	O	of
23	114	116	O	the
24	118	121	O	drug
25	123	132	O	suspension
26	134	136	O	and
27	138	140	O	the
28	142	151	O	applesauce
29	153	159	O	mixture
30	160	160	O	,
31	162	173	O	respectively
NULL

Rifampin	1017	34090-1	55816042-946d-4bec-9461-bd998628ff45	16
After the administration of either preparation, the t1/2 of rifampin averaged 2.9 hours.
1	0	4	O	After
2	6	8	O	the
3	10	23	O	administration
4	25	26	O	of
5	28	33	O	either
6	35	45	O	preparation
7	46	46	O	,
8	48	50	O	the
9	52	53	O	t1
10	54	54	O	/
11	55	55	O	2
12	57	58	O	of
13	60	67	O	XXXXXXXX
14	69	76	O	averaged
15	78	80	O	2.9
16	82	86	O	hours
NULL

Rifampin	1018	34090-1	55816042-946d-4bec-9461-bd998628ff45	39
It should be noted that in other studies in pediatric populations, at doses of 10 mg/kg body weight, mean peak serum concentrations of 3.5 mcg/mL to 15 mcg/mL have been reported.
1	0	1	O	It
2	3	8	O	should
3	10	11	O	be
4	13	17	O	noted
5	19	22	O	that
6	24	25	O	in
7	27	31	O	other
8	33	39	O	studies
9	41	42	O	in
10	44	52	O	pediatric
11	54	64	O	populations
12	65	65	O	,
13	67	68	O	at
14	70	74	O	doses
15	76	77	O	of
16	79	80	O	10
17	82	83	O	mg
18	84	84	O	/
19	85	86	O	kg
20	88	91	O	body
21	93	98	O	weight
22	99	99	O	,
23	101	104	O	mean
24	106	109	O	peak
25	111	115	O	serum
26	117	130	O	concentrations
27	132	133	O	of
28	135	137	O	3.5
29	139	141	O	mcg
30	142	142	O	/
31	143	144	O	mL
32	146	147	O	to
33	149	150	O	15
34	152	154	O	mcg
35	155	155	O	/
36	156	157	O	mL
37	159	162	O	have
38	164	167	O	been
39	169	176	O	reported
NULL

Rifampin	1019	34090-1	55816042-946d-4bec-9461-bd998628ff45	15
Mechanism of Action Rifampin inhibits DNA-dependent RNA polymerase activity in susceptible Mycobacterium tuberculosis organisms.
1	0	8	O	Mechanism
2	10	11	O	of
3	13	18	O	Action
4	20	27	O	XXXXXXXX
5	29	36	O	inhibits
6	38	40	O	DNA
7	42	50	O	dependent
8	52	54	O	RNA
9	56	65	O	polymerase
10	67	74	O	activity
11	76	77	O	in
12	79	89	O	susceptible
13	91	103	O	Mycobacterium
14	105	116	O	tuberculosis
15	118	126	O	organisms
NULL

Rifampin	1020	34090-1	55816042-946d-4bec-9461-bd998628ff45	15
Specifically, it interacts with bacterial RNA polymerase but does not inhibit the mammalian enzyme.
1	0	11	O	Specifically
2	12	12	O	,
3	14	15	O	it
4	17	25	O	interacts
5	27	30	O	with
6	32	40	O	bacterial
7	42	44	O	RNA
8	46	55	O	polymerase
9	57	59	O	but
10	61	64	O	does
11	66	68	O	not
12	70	76	O	inhibit
13	78	80	O	the
14	82	90	O	mammalian
15	92	97	O	enzyme
NULL

Rifampin	1021	34090-1	55816042-946d-4bec-9461-bd998628ff45	14
Drug Resistance Organisms resistant to rifampin are likely to be resistant to other rifamycins.
1	0	3	O	Drug
2	5	14	O	Resistance
3	16	24	O	Organisms
4	26	34	O	resistant
5	36	37	O	to
6	39	46	O	XXXXXXXX
7	48	50	O	are
8	52	57	O	likely
9	59	60	O	to
10	62	63	O	be
11	65	73	O	resistant
12	75	76	O	to
13	78	82	O	other
14	84	93	O	rifamycins
NULL

Rifampin	1022	34090-1	55816042-946d-4bec-9461-bd998628ff45	29
In the treatment of both tuberculosis and the meningococcal carrier state , the small number of resistant cells present within large populations of susceptible cells can rapidly become predominant.
1	0	1	O	In
2	3	5	O	the
3	7	15	O	treatment
4	17	18	O	of
5	20	23	O	both
6	25	36	O	tuberculosis
7	38	40	O	and
8	42	44	O	the
9	46	58	O	meningococcal
10	60	66	O	carrier
11	68	72	O	state
12	74	74	O	,
13	76	78	O	the
14	80	84	O	small
15	86	91	O	number
16	93	94	O	of
17	96	104	O	resistant
18	106	110	O	cells
19	112	118	O	present
20	120	125	O	within
21	127	131	O	large
22	133	143	O	populations
23	145	146	O	of
24	148	158	O	susceptible
25	160	164	O	cells
26	166	168	O	can
27	170	176	O	rapidly
28	178	183	O	become
29	185	195	O	predominant
NULL

Rifampin	1023	34090-1	55816042-946d-4bec-9461-bd998628ff45	21
In addition, resistance to rifampin has been determined to occur as single-step mutations of the DNA-dependent RNA polymerase.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	22	O	resistance
5	24	25	O	to
6	27	34	O	XXXXXXXX
7	36	38	O	has
8	40	43	O	been
9	45	54	O	determined
10	56	57	O	to
11	59	63	O	occur
12	65	66	O	as
13	68	73	O	single
14	75	78	O	step
15	80	88	O	mutations
16	90	91	O	of
17	93	95	O	the
18	97	99	O	DNA
19	101	109	O	dependent
20	111	113	O	RNA
21	115	124	O	polymerase
NULL

Rifampin	1024	34090-1	55816042-946d-4bec-9461-bd998628ff45	19
Since resistance can emerge rapidly, appropriate susceptibility tests should be performed in the event of persistent positive cultures.
1	0	4	O	Since
2	6	15	O	resistance
3	17	19	O	can
4	21	26	O	emerge
5	28	34	O	rapidly
6	35	35	O	,
7	37	47	O	appropriate
8	49	62	O	susceptibility
9	64	68	O	tests
10	70	75	O	should
11	77	78	O	be
12	80	88	O	performed
13	90	91	O	in
14	93	95	O	the
15	97	101	O	event
16	103	104	O	of
17	106	115	O	persistent
18	117	124	O	positive
19	126	133	O	cultures
NULL

Rifampin	1025	34090-1	55816042-946d-4bec-9461-bd998628ff45	20
Activity in vitro and in vivo Rifampin has bactericidal activity in vitro against slow and intermittently growing M tuberculosis organisms.
1	0	7	O	Activity
2	9	10	O	in
3	12	16	O	vitro
4	18	20	O	and
5	22	23	O	in
6	25	28	O	vivo
7	30	37	O	XXXXXXXX
8	39	41	O	has
9	43	54	O	bactericidal
10	56	63	O	activity
11	65	66	O	in
12	68	72	O	vitro
13	74	80	O	against
14	82	85	O	slow
15	87	89	O	and
16	91	104	O	intermittently
17	106	112	O	growing
18	114	114	O	M
19	116	127	O	tuberculosis
20	129	137	O	organisms
NULL

Rifampin	1026	34090-1	55816042-946d-4bec-9461-bd998628ff45	30
Rifampin has been shown to be active against most strains of the following microorganisms, both in vitro and in clinical infections as described in the INDICATIONS AND USAGE section.
1	0	7	O	XXXXXXXX
2	9	11	O	has
3	13	16	O	been
4	18	22	O	shown
5	24	25	O	to
6	27	28	O	be
7	30	35	O	active
8	37	43	O	against
9	45	48	O	most
10	50	56	O	strains
11	58	59	O	of
12	61	63	O	the
13	65	73	O	following
14	75	88	O	microorganisms
15	89	89	O	,
16	91	94	O	both
17	96	97	O	in
18	99	103	O	vitro
19	105	107	O	and
20	109	110	O	in
21	112	119	O	clinical
22	121	130	O	infections
23	132	133	O	as
24	135	143	O	described
25	145	146	O	in
26	148	150	O	the
27	152	162	O	INDICATIONS
28	164	166	O	AND
29	168	172	O	USAGE
30	174	180	O	section
NULL

Rifampin	1027	34090-1	55816042-946d-4bec-9461-bd998628ff45	26
Aerobic Gram-Negative Microorganisms: Neisseria meningitidis "Other" Microorganisms: Mycobacterium tuberculosis The following in vitro data are available, but their clinical significance is unknown.
1	0	6	O	Aerobic
2	8	11	O	Gram
3	13	20	O	Negative
4	22	35	O	Microorganisms
5	36	36	O	:
6	38	46	O	Neisseria
7	48	59	O	meningitidis
8	61	67	O	"Other"
9	69	82	O	Microorganisms
10	83	83	O	:
11	85	97	O	Mycobacterium
12	99	110	O	tuberculosis
13	112	114	O	The
14	116	124	O	following
15	126	127	O	in
16	129	133	O	vitro
17	135	138	O	data
18	140	142	O	are
19	144	152	O	available
20	153	153	O	,
21	155	157	O	but
22	159	163	O	their
23	165	172	O	clinical
24	174	185	O	significance
25	187	188	O	is
26	190	196	O	unknown
NULL

Rifampin	1028	34090-1	55816042-946d-4bec-9461-bd998628ff45	38
Rifampin exhibits in vitro activity against most strains of the following microorganisms; however, the safety and effectiveness of rifampin in treating clinical infections due to these microorganisms have not been established in adequate and well controlled trials.
1	0	7	O	XXXXXXXX
2	9	16	O	exhibits
3	18	19	O	in
4	21	25	O	vitro
5	27	34	O	activity
6	36	42	O	against
7	44	47	O	most
8	49	55	O	strains
9	57	58	O	of
10	60	62	O	the
11	64	72	O	following
12	74	87	O	microorganisms
13	90	96	O	however
14	97	97	O	,
15	99	101	O	the
16	103	108	O	safety
17	110	112	O	and
18	114	126	O	effectiveness
19	128	129	O	of
20	131	138	O	XXXXXXXX
21	140	141	O	in
22	143	150	O	treating
23	152	159	O	clinical
24	161	170	O	infections
25	172	174	O	due
26	176	177	O	to
27	179	183	O	these
28	185	198	O	microorganisms
29	200	203	O	have
30	205	207	O	not
31	209	212	O	been
32	214	224	O	established
33	226	227	O	in
34	229	236	O	adequate
35	238	240	O	and
36	242	245	O	well
37	247	256	O	controlled
38	258	263	O	trials
NULL

Rifampin	1029	34090-1	55816042-946d-4bec-9461-bd998628ff45	38
Aerobic Gram-Positive Microorganisms: Staphylococcus aureus (including Methicillin-Resistant S. aureus/MRSA) Staphylococcus epidermis Aerobic Gram-Negative Microorganisms: Haemophilus influenzae "Other" Microorganisms: Mycobacterium leprae *-lactamase production should have no effect on rifampin activity.
1	0	6	O	Aerobic
2	8	11	O	Gram
3	13	20	O	Positive
4	22	35	O	Microorganisms
5	36	36	O	:
6	38	51	O	Staphylococcus
7	53	58	O	aureus
8	61	69	O	including
9	71	81	O	Methicillin
10	83	91	O	Resistant
11	93	93	O	S
12	96	101	O	aureus
13	102	102	O	/
14	103	106	O	MRSA
15	109	122	O	Staphylococcus
16	124	132	O	epidermis
17	134	140	O	Aerobic
18	142	145	O	Gram
19	147	154	O	Negative
20	156	169	O	Microorganisms
21	170	170	O	:
22	172	182	O	Haemophilus
23	184	193	O	influenzae
24	195	201	O	"Other"
25	203	216	O	Microorganisms
26	217	217	O	:
27	219	231	O	Mycobacterium
28	233	238	O	leprae
29	240	240	O	*
30	242	250	O	lactamase
31	252	261	O	production
32	263	268	O	should
33	270	273	O	have
34	275	276	O	no
35	278	283	O	effect
36	285	286	O	on
37	288	295	O	XXXXXXXX
38	297	304	O	activity
NULL

Rifampin	1030	34090-1	55816042-946d-4bec-9461-bd998628ff45	22
Prior to initiation of therapy, appropriate specimens should be collected for identification of the infecting organism and in vitro susceptibility tests.
1	0	4	O	Prior
2	6	7	O	to
3	9	18	O	initiation
4	20	21	O	of
5	23	29	O	therapy
6	30	30	O	,
7	32	42	O	appropriate
8	44	52	O	specimens
9	54	59	O	should
10	61	62	O	be
11	64	72	O	collected
12	74	76	O	for
13	78	91	O	identification
14	93	94	O	of
15	96	98	O	the
16	100	108	O	infecting
17	110	117	O	organism
18	119	121	O	and
19	123	124	O	in
20	126	130	O	vitro
21	132	145	O	susceptibility
22	147	151	O	tests
NULL

Rifampin	1031	34090-1	55816042-946d-4bec-9461-bd998628ff45	23
In vitro testing for Mycobacterium tuberculosis isolates: Two standardized in vitro susceptibility methods are available for testing rifampin against M tuberculosis organisms.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	testing
4	17	19	O	for
5	21	33	O	Mycobacterium
6	35	46	O	tuberculosis
7	48	55	O	isolates
8	56	56	O	:
9	58	60	O	Two
10	62	73	O	standardized
11	75	76	O	in
12	78	82	O	vitro
13	84	97	O	susceptibility
14	99	105	O	methods
15	107	109	O	are
16	111	119	O	available
17	121	123	O	for
18	125	131	O	testing
19	133	140	O	XXXXXXXX
20	142	148	O	against
21	150	150	O	M
22	152	163	O	tuberculosis
23	165	173	O	organisms
NULL

Rifampin	1032	34090-1	55816042-946d-4bec-9461-bd998628ff45	29
The agar proportion method (CDC or CLSI(1) M24-A) utilizes Middlebrook 7H10 medium impregnated with rifampin at a final concentration of 1.0 mcg/mL to determine drug resistance.
1	0	2	O	The
2	4	7	O	agar
3	9	18	O	proportion
4	20	25	O	method
5	28	30	O	CDC
6	32	33	O	or
7	35	40	O	CLSI(1
8	43	45	O	M24
9	47	47	O	A
10	50	57	O	utilizes
11	59	69	O	Middlebrook
12	71	74	B-T	7H10
13	76	81	I-T	medium
14	83	93	O	impregnated
15	95	98	O	with
16	100	107	O	XXXXXXXX
17	109	110	O	at
18	112	112	O	a
19	114	118	O	final
20	120	132	O	concentration
21	134	135	O	of
22	137	139	O	1.0
23	141	143	O	mcg
24	144	144	O	/
25	145	146	O	mL
26	148	149	O	to
27	151	159	O	determine
28	161	164	O	drug
29	166	175	O	resistance
NULL

Rifampin	1033	34090-1	55816042-946d-4bec-9461-bd998628ff45	25
After three weeks of incubation MIC99 values are calculated by comparing the quantity of organisms growing in the medium containing drug to the control cultures.
1	0	4	O	After
2	6	10	O	three
3	12	16	O	weeks
4	18	19	O	of
5	21	30	O	incubation
6	32	36	O	MIC99
7	38	43	O	values
8	45	47	O	are
9	49	58	O	calculated
10	60	61	O	by
11	63	71	O	comparing
12	73	75	O	the
13	77	84	O	quantity
14	86	87	O	of
15	89	97	O	organisms
16	99	105	O	growing
17	107	108	O	in
18	110	112	O	the
19	114	119	O	medium
20	121	130	O	containing
21	132	135	O	drug
22	137	138	O	to
23	140	142	O	the
24	144	150	O	control
25	152	159	O	cultures
NULL

Rifampin	1034	34090-1	55816042-946d-4bec-9461-bd998628ff45	22
Mycobacterial growth in the presence of drug, of at least 1% of the growth in the control culture, indicates resistance.
1	0	12	O	Mycobacterial
2	14	19	O	growth
3	21	22	O	in
4	24	26	O	the
5	28	35	O	presence
6	37	38	O	of
7	40	43	O	drug
8	44	44	O	,
9	46	47	O	of
10	49	50	O	at
11	52	56	O	least
12	58	59	O	1%
13	61	62	O	of
14	64	66	O	the
15	68	73	O	growth
16	75	76	O	in
17	78	80	O	the
18	82	88	O	control
19	90	96	O	culture
20	97	97	O	,
21	99	107	O	indicates
22	109	118	O	resistance
NULL

Rifampin	1035	34090-1	55816042-946d-4bec-9461-bd998628ff45	31
The radiometric broth method employs the BACTEC 460 machine to compare the growth index from untreated control cultures to cultures grown in the presence of 2.0 mcg/mL of rifampin.
1	0	2	O	The
2	4	14	O	radiometric
3	16	20	O	broth
4	22	27	O	method
5	29	35	O	employs
6	37	39	O	the
7	41	46	O	BACTEC
8	48	50	O	460
9	52	58	O	machine
10	60	61	O	to
11	63	69	O	compare
12	71	73	O	the
13	75	80	O	growth
14	82	86	O	index
15	88	91	O	from
16	93	101	O	untreated
17	103	109	O	control
18	111	118	O	cultures
19	120	121	O	to
20	123	130	O	cultures
21	132	136	O	grown
22	138	139	O	in
23	141	143	O	the
24	145	152	O	presence
25	154	155	O	of
26	157	159	O	2.0
27	161	163	O	mcg
28	164	164	O	/
29	165	166	O	mL
30	168	169	O	of
31	171	178	O	XXXXXXXX
NULL

Rifampin	1036	34090-1	55816042-946d-4bec-9461-bd998628ff45	17
Strict adherence to the manufacturer's instructions for sample processing and data interpretation is required for this assay.
1	0	5	O	Strict
2	7	15	O	adherence
3	17	18	O	to
4	20	22	O	the
5	24	37	O	manufacturer's
6	39	50	O	instructions
7	52	54	O	for
8	56	61	O	sample
9	63	72	O	processing
10	74	76	O	and
11	78	81	O	data
12	83	96	O	interpretation
13	98	99	O	is
14	101	108	O	required
15	110	112	O	for
16	114	117	O	this
17	119	123	O	assay
NULL

Rifampin	1037	34090-1	55816042-946d-4bec-9461-bd998628ff45	27
Susceptibility test results obtained by the two different methods can only be compared if the appropriate rifampin concentration is used for each test method as indicated above.
1	0	13	O	Susceptibility
2	15	18	O	test
3	20	26	O	results
4	28	35	O	obtained
5	37	38	O	by
6	40	42	O	the
7	44	46	O	two
8	48	56	O	different
9	58	64	O	methods
10	66	68	O	can
11	70	73	O	only
12	75	76	O	be
13	78	85	O	compared
14	87	88	O	if
15	90	92	O	the
16	94	104	O	appropriate
17	106	113	O	XXXXXXXX
18	115	127	O	concentration
19	129	130	O	is
20	132	135	O	used
21	137	139	O	for
22	141	144	O	each
23	146	149	O	test
24	151	156	O	method
25	158	159	O	as
26	161	169	O	indicated
27	171	175	O	above
NULL

Rifampin	1038	34090-1	55816042-946d-4bec-9461-bd998628ff45	15
Both procedures require the use of M tuberculosis H37Rv ATCC 27294 as a control organism.
1	0	3	O	Both
2	5	14	O	procedures
3	16	22	O	require
4	24	26	O	the
5	28	30	O	use
6	32	33	O	of
7	35	35	O	M
8	37	48	O	tuberculosis
9	50	54	O	H37Rv
10	56	59	O	ATCC
11	61	65	O	27294
12	67	68	O	as
13	70	70	O	a
14	72	78	O	control
15	80	87	O	organism
NULL

Rifampin	1039	34090-1	55816042-946d-4bec-9461-bd998628ff45	28
The clinical relevance of in vitro susceptibility test results for mycobacterial species other than M tuberculosis using either the radiometric or the proportion method has not been determined.
1	0	2	O	The
2	4	11	O	clinical
3	13	21	O	relevance
4	23	24	O	of
5	26	27	O	in
6	29	33	O	vitro
7	35	48	O	susceptibility
8	50	53	O	test
9	55	61	O	results
10	63	65	O	for
11	67	79	O	mycobacterial
12	81	87	O	species
13	89	93	O	other
14	95	98	O	than
15	100	100	O	M
16	102	113	O	tuberculosis
17	115	119	O	using
18	121	126	O	either
19	128	130	O	the
20	132	142	O	radiometric
21	144	145	O	or
22	147	149	O	the
23	151	160	O	proportion
24	162	167	O	method
25	169	171	O	has
26	173	175	O	not
27	177	180	O	been
28	182	191	O	determined
NULL

Rifampin	1040	34090-1	55816042-946d-4bec-9461-bd998628ff45	32
In vitro testing for Neisseria meningitidis isolates: Dilution Techniques: Quantitative methods that are used to determine minimum inhibitory concentrations provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds.
1	0	1	O	In
2	3	7	O	vitro
3	9	15	O	testing
4	17	19	O	for
5	21	29	O	Neisseria
6	31	42	O	meningitidis
7	44	51	O	isolates
8	52	52	O	:
9	54	61	O	Dilution
10	63	72	O	Techniques
11	73	73	O	:
12	75	86	O	Quantitative
13	88	94	O	methods
14	96	99	O	that
15	101	103	O	are
16	105	108	O	used
17	110	111	O	to
18	113	121	O	determine
19	123	129	O	minimum
20	131	140	O	inhibitory
21	142	155	O	concentrations
22	157	163	O	provide
23	165	176	O	reproducible
24	178	186	O	estimates
25	188	189	O	of
26	191	193	O	the
27	195	208	O	susceptibility
28	210	211	O	of
29	213	220	O	bacteria
30	222	223	O	to
31	225	237	O	antimicrobial
32	239	247	O	compounds
NULL

Rifampin	1041	34090-1	55816042-946d-4bec-9461-bd998628ff45	20
One such standardized procedure uses a standardized dilution method2,4 (broth, agar, or microdilution) or equivalent with rifampin powder.
1	0	2	O	One
2	4	7	O	such
3	9	20	O	standardized
4	22	30	O	procedure
5	32	35	O	uses
6	37	37	O	a
7	39	50	O	standardized
8	52	59	O	dilution
9	61	69	O	method2,4
10	72	76	O	broth
11	77	77	O	,
12	79	82	O	agar
13	83	83	O	,
14	85	86	O	or
15	88	100	O	microdilution
16	103	104	O	or
17	106	115	O	equivalent
18	117	120	O	with
19	122	129	O	XXXXXXXX
20	131	136	O	powder
NULL

Rifampin	1042	34090-1	55816042-946d-4bec-9461-bd998628ff45	15
The MIC values obtained should be interpreted according to the following criteria for Neisseria meningitidis
1	0	2	O	The
2	4	6	O	MIC
3	8	13	O	values
4	15	22	O	obtained
5	24	29	O	should
6	31	32	O	be
7	34	44	O	interpreted
8	46	54	O	according
9	56	57	O	to
10	59	61	O	the
11	63	71	O	following
12	73	80	O	criteria
13	82	84	O	for
14	86	94	O	Neisseria
15	96	107	O	meningitidis
NULL

Rifampin	1043	34090-1	55816042-946d-4bec-9461-bd998628ff45	33
A report of "intermediate" indicates that the result should be considered equivocal, and if the microorganism is not fully susceptible to alternative, clinically feasible drugs, the test should be repeated.
1	0	0	O	A
2	2	7	O	report
3	9	10	O	of
4	12	25	O	"intermediate"
5	27	35	O	indicates
6	37	40	O	that
7	42	44	O	the
8	46	51	O	result
9	53	58	O	should
10	60	61	O	be
11	63	72	O	considered
12	74	82	O	equivocal
13	83	83	O	,
14	85	87	O	and
15	89	90	O	if
16	92	94	O	the
17	96	108	O	microorganism
18	110	111	O	is
19	113	115	O	not
20	117	121	O	fully
21	123	133	O	susceptible
22	135	136	O	to
23	138	148	O	alternative
24	149	149	O	,
25	151	160	O	clinically
26	162	169	O	feasible
27	171	175	O	drugs
28	176	176	O	,
29	178	180	O	the
30	182	185	O	test
31	187	192	O	should
32	194	195	O	be
33	197	204	O	repeated
NULL

Rifampin	1044	34090-1	55816042-946d-4bec-9461-bd998628ff45	28
This category implies possible clinical applicability in body sites where the drug is physiologically concentrated or in situations where the maximum acceptable dose of drug can be used.
1	0	3	O	This
2	5	12	O	category
3	14	20	O	implies
4	22	29	O	possible
5	31	38	O	clinical
6	40	52	O	applicability
7	54	55	O	in
8	57	60	O	body
9	62	66	O	sites
10	68	72	O	where
11	74	76	O	the
12	78	81	O	drug
13	83	84	O	is
14	86	100	O	physiologically
15	102	113	O	concentrated
16	115	116	O	or
17	118	119	O	in
18	121	130	O	situations
19	132	136	O	where
20	138	140	O	the
21	142	148	O	maximum
22	150	159	O	acceptable
23	161	164	O	dose
24	166	167	O	of
25	169	172	O	drug
26	174	176	O	can
27	178	179	O	be
28	181	184	O	used
NULL

Rifampin	1045	34090-1	55816042-946d-4bec-9461-bd998628ff45	19
This category also provides a buffer zone that prevents small-uncontrolled technical factors from causing major discrepancies in interpretation.
1	0	3	O	This
2	5	12	O	category
3	14	17	O	also
4	19	26	O	provides
5	28	28	O	a
6	30	35	O	buffer
7	37	40	O	zone
8	42	45	O	that
9	47	54	O	prevents
10	56	60	O	small
11	62	73	O	uncontrolled
12	75	83	O	technical
13	85	91	O	factors
14	93	96	O	from
15	98	104	O	causing
16	106	110	O	major
17	112	124	O	discrepancies
18	126	127	O	in
19	129	142	O	interpretation
NULL

Rifampin	1046	34090-1	55816042-946d-4bec-9461-bd998628ff45	28
A report of "resistant" indicates that usually achievable concentrations of the antimicrobial compound in the blood are unlikely to be inhibitory and that other therapy should be selected.
1	0	0	O	A
2	2	7	O	report
3	9	10	O	of
4	12	22	B-T	"resistant"
5	24	32	I-T	indicates
6	34	37	I-T	that
7	39	45	I-T	usually
8	47	56	I-T	achievable
9	58	71	I-T	concentrations
10	73	74	O	of
11	76	78	O	the
12	80	92	O	antimicrobial
13	94	101	O	compound
14	103	104	O	in
15	106	108	O	the
16	110	114	O	blood
17	116	118	O	are
18	120	127	O	unlikely
19	129	130	O	to
20	132	133	O	be
21	135	144	O	inhibitory
22	146	148	O	and
23	150	153	O	that
24	155	159	O	other
25	161	167	O	therapy
26	169	174	O	should
27	176	177	O	be
28	179	186	O	selected
NULL

Rifampin	1047	34090-1	55816042-946d-4bec-9461-bd998628ff45	22
Measurement of MIC or minimum bactericidal concentrations (MBC) and achieved antimicrobial compound concentrations may be appropriate to guide therapy in some infections.
1	0	10	O	Measurement
2	12	13	O	of
3	15	17	O	MIC
4	19	20	O	or
5	22	28	O	minimum
6	30	41	O	bactericidal
7	43	56	O	concentrations
8	59	61	O	MBC
9	64	66	O	and
10	68	75	O	achieved
11	77	89	O	antimicrobial
12	91	98	O	compound
13	100	113	O	concentrations
14	115	117	O	may
15	119	120	O	be
16	122	132	O	appropriate
17	134	135	O	to
18	137	141	O	guide
19	143	149	O	therapy
20	151	152	O	in
21	154	157	O	some
22	159	168	O	infections
NULL

Rifampin	1049	34090-1	55816042-946d-4bec-9461-bd998628ff45	11
Standardized susceptibility test procedures require the use of laboratory control microorganisms.
1	0	11	O	Standardized
2	13	26	O	susceptibility
3	28	31	O	test
4	33	42	O	procedures
5	44	50	O	require
6	52	54	O	the
7	56	58	O	use
8	60	61	O	of
9	63	72	O	laboratory
10	74	80	O	control
11	82	95	O	microorganisms
NULL

Rifampin	1050	34090-1	55816042-946d-4bec-9461-bd998628ff45	22
The use of these microorganisms does not imply clinical efficacy ; they are used to control the technical aspects of the laboratory procedures.
1	0	2	O	The
2	4	6	O	use
3	8	9	O	of
4	11	15	O	these
5	17	30	O	microorganisms
6	32	35	O	does
7	37	39	O	not
8	41	45	O	imply
9	47	54	O	clinical
10	56	63	O	efficacy
11	67	70	O	they
12	72	74	O	are
13	76	79	O	used
14	81	82	O	to
15	84	90	O	control
16	92	94	O	the
17	96	104	O	technical
18	106	112	O	aspects
19	114	115	O	of
20	117	119	O	the
21	121	130	O	laboratory
22	132	141	O	procedures
NULL

Rifampin	1051	34090-1	55816042-946d-4bec-9461-bd998628ff45	67
Standard rifampin powder should give the following MIC values: Microorganism MIC (mcg/mL) Staphylococcus aureus ATCC 29213 0.008 - 0.06 Enterococcus faecalis ATCC 29212 1 - 4 Escherichia coli ATCC 25922 8 - 32 Pseudomonas aeruginosa ATCC 27853 32 - 64 Haemophilus influenzae ATCC 49247 0.25 - 1 Diffusion Techniques: Quantitative methods that require measurement of zone diameters provide reproducible estimates of the susceptibility of bacteria to antimicrobial compounds.
1	0	7	O	Standard
2	9	16	O	XXXXXXXX
3	18	23	O	powder
4	25	30	O	should
5	32	35	O	give
6	37	39	O	the
7	41	49	O	following
8	51	53	O	MIC
9	55	60	O	values
10	61	61	O	:
11	63	75	O	Microorganism
12	77	79	O	MIC
13	82	84	O	mcg
14	85	85	O	/
15	86	87	O	mL
16	90	103	O	Staphylococcus
17	105	110	O	aureus
18	112	115	O	ATCC
19	117	121	O	29213
20	123	127	O	0.008
21	131	134	O	0.06
22	136	147	O	Enterococcus
23	149	156	O	faecalis
24	158	161	O	ATCC
25	163	167	O	29212
26	169	169	O	1
27	173	173	O	4
28	175	185	O	Escherichia
29	187	190	O	coli
30	192	195	O	ATCC
31	197	201	O	25922
32	203	203	O	8
33	207	208	O	32
34	210	220	O	Pseudomonas
35	222	231	O	aeruginosa
36	233	236	O	ATCC
37	238	242	O	27853
38	244	245	O	32
39	249	250	O	64
40	252	262	O	Haemophilus
41	264	273	O	influenzae
42	275	278	O	ATCC
43	280	284	O	49247
44	286	289	O	0.25
45	293	293	O	1
46	295	303	O	Diffusion
47	305	314	O	Techniques
48	315	315	O	:
49	317	328	O	Quantitative
50	330	336	O	methods
51	338	341	O	that
52	343	349	O	require
53	351	361	O	measurement
54	363	364	O	of
55	366	369	O	zone
56	371	379	O	diameters
57	381	387	O	provide
58	389	400	O	reproducible
59	402	410	O	estimates
60	412	413	O	of
61	415	417	O	the
62	419	432	O	susceptibility
63	434	435	O	of
64	437	444	O	bacteria
65	446	447	O	to
66	449	461	O	antimicrobial
67	463	471	O	compounds
NULL

Rifampin	1052	34090-1	55816042-946d-4bec-9461-bd998628ff45	26
One such standardized procedure3,4 that has been recommended for use with disks to test the susceptibility of microorganisms to rifampin uses the 5 mcg rifampin disk.
1	0	2	O	One
2	4	7	O	such
3	9	20	O	standardized
4	22	33	O	procedure3,4
5	35	38	O	that
6	40	42	O	has
7	44	47	O	been
8	49	59	O	recommended
9	61	63	O	for
10	65	67	O	use
11	69	72	O	with
12	74	78	O	disks
13	80	81	O	to
14	83	86	O	test
15	88	90	O	the
16	92	105	O	susceptibility
17	107	108	O	of
18	110	123	O	microorganisms
19	125	126	O	to
20	128	135	O	XXXXXXXX
21	137	140	O	uses
22	142	144	O	the
23	146	146	O	5
24	148	150	O	mcg
25	152	159	O	XXXXXXXX
26	161	164	O	disk
NULL

Rifampin	1053	34090-1	55816042-946d-4bec-9461-bd998628ff45	16
Interpretation involves correlation of the diameter obtained in the disk test with the MIC for rifampin.
1	0	13	O	Interpretation
2	15	22	O	involves
3	24	34	B-T	correlation
4	36	37	O	of
5	39	41	O	the
6	43	50	O	diameter
7	52	59	O	obtained
8	61	62	O	in
9	64	66	O	the
10	68	71	O	disk
11	73	76	O	test
12	78	81	O	with
13	83	85	O	the
14	87	89	O	MIC
15	91	93	O	for
16	95	102	O	XXXXXXXX
NULL

Rifampin	1054	34090-1	55816042-946d-4bec-9461-bd998628ff45	58
Reports from the laboratory providing results of the standard single-disk susceptibility test with a 5 mcg rifampin disk should be interpreted according to the following criteria for Neisseria meningitides: Zone Diameter (mm) Interpretation > 20 (S) Susceptible 17 - 19 (I) Intermediate < 16 (R) Resistant Interpretation should be as stated above for results using dilution techniques.
1	0	6	O	Reports
2	8	11	O	from
3	13	15	O	the
4	17	26	O	laboratory
5	28	36	O	providing
6	38	44	O	results
7	46	47	O	of
8	49	51	O	the
9	53	60	O	standard
10	62	67	O	single
11	69	72	O	disk
12	74	87	O	susceptibility
13	89	92	O	test
14	94	97	O	with
15	99	99	O	a
16	101	101	O	5
17	103	105	O	mcg
18	107	114	O	XXXXXXXX
19	116	119	O	disk
20	121	126	O	should
21	128	129	O	be
22	131	141	O	interpreted
23	143	151	O	according
24	153	154	O	to
25	156	158	O	the
26	160	168	O	following
27	170	177	O	criteria
28	179	181	O	for
29	183	191	O	Neisseria
30	193	204	O	meningitides
31	205	205	O	:
32	207	210	O	Zone
33	212	219	O	Diameter
34	222	223	O	mm
35	226	239	O	Interpretation
36	241	241	O	>
37	243	244	O	20
38	247	247	O	S
39	250	260	O	Susceptible
40	262	263	O	17
41	267	268	O	19
42	271	271	O	I
43	274	285	O	Intermediate
44	287	287	O	<
45	289	290	O	16
46	293	293	O	R
47	296	304	O	Resistant
48	306	319	O	Interpretation
49	321	326	O	should
50	328	329	O	be
51	331	332	O	as
52	334	339	O	stated
53	341	345	O	above
54	347	349	O	for
55	351	357	O	results
56	359	363	O	using
57	365	372	O	dilution
58	374	383	O	techniques
NULL

Rifampin	1055	34090-1	55816042-946d-4bec-9461-bd998628ff45	15
As with standard dilution techniques, diffusion methods require the use of laboratory control microorganisms.
1	0	1	O	As
2	3	6	O	with
3	8	15	O	standard
4	17	24	O	dilution
5	26	35	O	techniques
6	36	36	O	,
7	38	46	O	diffusion
8	48	54	O	methods
9	56	62	O	require
10	64	66	O	the
11	68	70	O	use
12	72	73	O	of
13	75	84	O	laboratory
14	86	92	O	control
15	94	107	O	microorganisms
NULL

Rifampin	1057	34090-1	55816042-946d-4bec-9461-bd998628ff45	39
The 5 mcg rifampin disk should provide the following zone diameters in these quality control strains: Microorganism Zone Diameter (mm) S. aureus ATCC 25923 26 - 34 E. coli ATCC 25922 8 - 10 H. influenzae ATCC 49247 22 - 30
1	0	2	O	The
2	4	4	O	5
3	6	8	O	mcg
4	10	17	O	XXXXXXXX
5	19	22	O	disk
6	24	29	O	should
7	31	37	O	provide
8	39	41	O	the
9	43	51	O	following
10	53	56	O	zone
11	58	66	O	diameters
12	68	69	O	in
13	71	75	O	these
14	77	83	O	quality
15	85	91	O	control
16	93	99	O	strains
17	100	100	O	:
18	102	114	O	Microorganism
19	116	119	O	Zone
20	121	128	O	Diameter
21	131	132	O	mm
22	135	135	O	S
23	138	143	O	aureus
24	145	148	O	ATCC
25	150	154	O	25923
26	156	157	O	26
27	161	162	O	34
28	164	164	O	E
29	167	170	O	coli
30	172	175	O	ATCC
31	177	181	O	25922
32	183	183	O	8
33	187	188	O	10
34	190	190	O	H
35	193	202	O	influenzae
36	204	207	O	ATCC
37	209	213	O	49247
38	215	216	O	22
39	220	221	O	30
NULL

