Clozapine	2980	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	26
Concomitant use of Strong CYP1A2 Inhibitors: Reduce CLOZARIL dose to one-third when coadministered with strong CYP1A2 inhibitors (e.g., fluvoxamine, ciprofloxacin, enoxacin).
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	24	B-U	Strong
5	26	31	I-U	CYP1A2
6	33	42	I-U	Inhibitors
7	43	43	O	:
8	45	50	O	Reduce
9	52	59	O	CLOZARIL
10	61	64	O	dose
11	66	67	O	to
12	69	71	O	one
13	73	77	O	third
14	79	82	O	when
15	84	97	O	coadministered
16	99	102	O	with
17	104	109	B-U	strong
18	111	116	I-U	CYP1A2
19	118	127	I-U	inhibitors
20	130	132	O	e.g
21	134	134	O	,
22	136	146	O	fluvoxamine
23	147	147	O	,
24	149	161	O	ciprofloxacin
25	162	162	O	,
26	164	171	O	enoxacin
NULL

Clozapine	2981	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	9
Concomitant use of Strong CYP3A4 Inducers is not recommended.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	24	B-U	Strong
5	26	31	I-U	CYP3A4
6	33	40	I-U	Inducers
7	42	43	I-U	is
8	45	47	B-T	not
9	49	59	I-T	recommended
NULL

Clozapine	2982	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	26
Discontinuation of CYP1A2 or CYP3A4 Inducers: Consider reducing CLOZARIL dose when CYP1A2 inducers (e.g., tobacco smoke) or CYP3A4 inducers (e.g., carbamazepine) are discontinued.
1	0	14	B-T	Discontinuation
2	16	17	O	of
3	19	24	O	CYP1A2
4	26	27	O	or
5	29	34	B-U	CYP3A4
6	36	43	I-U	Inducers
7	44	44	O	:
8	46	53	O	Consider
9	55	62	B-T	reducing
10	64	71	B-U	CLOZARIL
11	73	76	O	dose
12	78	81	O	when
13	83	88	B-U	CYP1A2
14	90	97	I-U	inducers
15	100	102	O	e.g
16	104	104	O	,
17	106	112	O	tobacco
18	114	118	O	smoke
19	121	122	O	or
20	124	129	B-U	CYP3A4
21	131	138	I-U	inducers
22	141	143	O	e.g
23	145	145	O	,
24	147	159	B-U	carbamazepine
25	162	164	O	are
26	166	177	O	discontinued
NULL

Clozapine	2983	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	18
Clozapine is a substrate for many cytochrome P450 isozymes, in particular CYP1A2, CYP3A4, and CYP2D6.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	13	O	a
4	15	23	O	substrate
5	25	27	O	for
6	29	32	O	many
7	34	43	O	cytochrome
8	45	48	O	P450
9	50	57	O	isozymes
10	58	58	O	,
11	60	61	O	in
12	63	72	O	particular
13	74	79	O	CYP1A2
14	80	80	O	,
15	82	87	O	CYP3A4
16	88	88	O	,
17	90	92	O	and
18	94	99	O	CYP2D6
NULL

Clozapine	2984	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	16
Use caution when administering CLOZARIL concomitantly with drugs that are inducers or inhibitors of these enzymes.
1	0	2	B-T	Use
2	4	10	I-T	caution
3	12	15	O	when
4	17	29	O	administering
5	31	38	B-U	CLOZARIL
6	40	52	O	concomitantly
7	54	57	O	with
8	59	63	B-U	drugs
9	65	68	I-U	that
10	70	72	I-U	are
11	74	81	I-U	inducers
12	83	84	I-U	or
13	86	95	I-U	inhibitors
14	97	98	I-U	of
15	100	104	I-U	these
16	106	112	I-U	enzymes
NULL

Clozapine	2985	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	21
CYP1A2 Inhibitors Concomitant use of CLOZARIL and CYP1A2 inhibitors can increase plasma levels of clozapine, potentially resulting in adverse reactions.
1	0	5	O	CYP1A2
2	7	16	O	Inhibitors
3	18	28	O	Concomitant
4	30	32	O	use
5	34	35	O	of
6	37	44	B-D	CLOZARIL
7	46	48	O	and
8	50	55	B-D	CYP1A2
9	57	66	I-D	inhibitors
10	68	70	O	can
11	72	79	B-T	increase
12	81	86	I-T	plasma
13	88	93	I-T	levels
14	95	96	O	of
15	98	106	O	XXXXXXXX
16	107	107	O	,
17	109	119	O	potentially
18	121	129	O	resulting
19	131	132	O	in
20	134	140	B-E	adverse
21	142	150	I-E	reactions
D/6:20:1 D/8:20:1

Clozapine	2986	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	27
Reduce the CLOZARIL dose to one-third of the original dose when CLOZARIL is coadministered with strong CYP1A2 inhibitors (e.g., fluvoxamine, ciprofloxacin, or enoxacin).
1	0	5	B-T	Reduce
2	7	9	I-T	the
3	11	18	O	CLOZARIL
4	20	23	O	dose
5	25	26	O	to
6	28	30	O	one
7	32	36	O	third
8	38	39	O	of
9	41	43	O	the
10	45	52	O	original
11	54	57	O	dose
12	59	62	O	when
13	64	71	O	CLOZARIL
14	73	74	O	is
15	76	89	O	coadministered
16	91	94	O	with
17	96	101	B-U	strong
18	103	108	I-U	CYP1A2
19	110	119	I-U	inhibitors
20	122	124	O	e.g
21	126	126	O	,
22	128	138	O	fluvoxamine
23	139	139	O	,
24	141	153	O	ciprofloxacin
25	154	154	O	,
26	156	157	O	or
27	159	166	O	enoxacin
NULL

Clozapine	2987	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	18
The CLOZARIL dose should be increased to the original dose when coadministration of strong CYP1A2 inhibitors is discontinued.
1	0	2	O	The
2	4	11	B-U	CLOZARIL
3	13	16	O	dose
4	18	23	O	should
5	25	26	B-T	be
6	28	36	B-T	increased
7	38	39	I-T	to
8	41	43	I-T	the
9	45	52	I-T	original
10	54	57	I-T	dose
11	59	62	O	when
12	64	79	O	coadministration
13	81	82	O	of
14	84	89	B-U	strong
15	91	96	I-U	CYP1A2
16	98	107	I-U	inhibitors
17	109	110	O	is
18	112	123	O	discontinued
NULL

Clozapine	2988	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	10
Moderate or weak CYP1A2 inhibitors include oral contraceptives and caffeine.
1	0	7	O	Moderate
2	9	10	O	or
3	12	15	B-U	weak
4	17	22	I-U	CYP1A2
5	24	33	I-U	inhibitors
6	35	41	O	include
7	43	46	O	oral
8	48	61	B-U	contraceptives
9	63	65	O	and
10	67	74	O	caffeine
NULL

Clozapine	2989	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	10
Monitor patients closely when CLOZARIL is coadministered with these inhibitors.
1	0	6	B-T	Monitor
2	8	15	O	patients
3	17	23	O	closely
4	25	28	O	when
5	30	37	B-U	CLOZARIL
6	39	40	O	is
7	42	55	O	coadministered
8	57	60	O	with
9	62	66	O	these
10	68	77	B-U	inhibitors
NULL

Clozapine	2990	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	7
Consider reducing the CLOZARIL dosage if necessary.
1	0	7	O	Consider
2	9	16	B-T	reducing
3	18	20	I-T	the
4	22	29	O	CLOZARIL
5	31	36	O	dosage
6	38	39	O	if
7	41	49	O	necessary
NULL

Clozapine	2991	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	44
CYP2D6 and CYP3A4 Inhibitors Concomitant treatment with CLOZARIL and CYP2D6 or CYP3A4 inhibitors (e.g., cimetidine, escitalopram, erythromycin, paroxetine, bupropion, fluoxetine, quinidine, duloxetine, terbinafine, or sertraline) can increase clozapine levels and lead to adverse reactions.
1	0	5	O	CYP2D6
2	7	9	O	and
3	11	16	O	CYP3A4
4	18	27	O	Inhibitors
5	29	39	O	Concomitant
6	41	49	O	treatment
7	51	54	O	with
8	56	63	O	CLOZARIL
9	65	67	O	and
10	69	74	O	CYP2D6
11	76	77	O	or
12	79	84	B-D	CYP3A4
13	86	95	I-D	inhibitors
14	98	100	O	e.g
15	102	102	O	,
16	104	113	O	cimetidine
17	114	114	O	,
18	116	127	O	escitalopram
19	128	128	O	,
20	130	141	O	erythromycin
21	142	142	O	,
22	144	153	O	paroxetine
23	154	154	O	,
24	156	164	O	bupropion
25	165	165	O	,
26	167	176	O	fluoxetine
27	177	177	O	,
28	179	187	O	quinidine
29	188	188	O	,
30	190	199	O	duloxetine
31	200	200	O	,
32	202	212	O	terbinafine
33	213	213	O	,
34	215	216	O	or
35	218	227	B-D	sertraline
36	230	232	O	can
37	234	241	O	increase
38	243	251	O	XXXXXXXX
39	253	258	O	levels
40	260	262	O	and
41	264	267	O	lead
42	269	270	O	to
43	272	278	O	adverse
44	280	288	B-E	reactions
D/12:44:1 D/35:44:1

Clozapine	2992	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	10
Use caution and monitor patients closely when using such inhibitors.
1	0	2	B-T	Use
2	4	10	B-T	caution
3	12	14	O	and
4	16	22	B-T	monitor
5	24	31	O	patients
6	33	39	O	closely
7	41	44	O	when
8	46	50	O	using
9	52	55	O	such
10	57	66	B-U	inhibitors
NULL

Clozapine	2993	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	5
Consider reducing the CLOZARIL dose.
1	0	7	O	Consider
2	9	16	B-T	reducing
3	18	20	I-T	the
4	22	29	B-U	CLOZARIL
5	31	34	O	dose
NULL

Clozapine	2994	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	27
CYP1A2 and CYP3A4 Inducers Concomitant treatment with drugs that induce CYP1A2 or CYP3A4 can decrease the plasma concentration of clozapine, resulting in decreased effectiveness of CLOZARIL.
1	0	5	O	CYP1A2
2	7	9	O	and
3	11	16	O	CYP3A4
4	18	25	O	Inducers
5	27	37	O	Concomitant
6	39	47	O	treatment
7	49	52	O	with
8	54	58	O	drugs
9	60	63	O	that
10	65	70	O	induce
11	72	77	O	CYP1A2
12	79	80	O	or
13	82	87	O	CYP3A4
14	89	91	O	can
15	93	100	B-T	decrease
16	102	104	I-T	the
17	106	111	I-T	plasma
18	113	125	I-T	concentration
19	127	128	O	of
20	130	138	O	XXXXXXXX
21	139	139	O	,
22	141	149	O	resulting
23	151	152	O	in
24	154	162	B-T	decreased
25	164	176	I-T	effectiveness
26	178	179	O	of
27	181	188	B-K	CLOZARIL
K/27:C54356

Clozapine	2995	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	8
Tobacco smoke is a moderate inducer of CYP1A2.
1	0	6	O	Tobacco
2	8	12	O	smoke
3	14	15	O	is
4	17	17	O	a
5	19	26	O	moderate
6	28	34	O	inducer
7	36	37	O	of
8	39	44	O	CYP1A2
NULL

Clozapine	2996	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	14
Strong CYP3A4 inducers include carbamazepine, phenytoin, St. John's wort, and rifampin.
1	0	5	B-U	Strong
2	7	12	I-U	CYP3A4
3	14	21	I-U	inducers
4	23	29	O	include
5	31	43	O	carbamazepine
6	44	44	O	,
7	46	54	O	phenytoin
8	55	55	O	,
9	57	58	O	St
10	61	66	O	John's
11	68	71	O	wort
12	72	72	O	,
13	74	76	O	and
14	78	85	B-K	rifampin
K/14:C54356

Clozapine	2997	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	17
It may be necessary to increase the CLOZARIL dose if used concomitantly with inducers of these enzymes.
1	0	1	O	It
2	3	5	O	may
3	7	8	O	be
4	10	18	O	necessary
5	20	21	O	to
6	23	30	O	increase
7	32	34	O	the
8	36	43	O	CLOZARIL
9	45	48	O	dose
10	50	51	O	if
11	53	56	O	used
12	58	70	O	concomitantly
13	72	75	O	with
14	77	84	B-U	inducers
15	86	87	I-U	of
16	89	93	I-U	these
17	95	101	I-U	enzymes
NULL

Clozapine	2998	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	13
However, concomitant use of CLOZARIL and strong CYP3A4 inducers is not recommended.
1	0	6	O	However
2	7	7	O	,
3	9	19	O	concomitant
4	21	23	O	use
5	25	26	O	of
6	28	35	B-U	CLOZARIL
7	37	39	O	and
8	41	46	B-U	strong
9	48	53	I-U	CYP3A4
10	55	62	I-U	inducers
11	64	65	I-U	is
12	67	69	B-T	not
13	71	81	I-T	recommended
NULL

Clozapine	2999	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	28
Consider reducing the CLOZARIL dosage when discontinuing coadministered enzyme inducers; because discontinuation of inducers can result in increased clozapine plasma levels and an increased risk of adverse reactions.
1	0	7	O	Consider
2	9	16	B-T	reducing
3	18	20	O	the
4	22	29	O	CLOZARIL
5	31	36	O	dosage
6	38	41	O	when
7	43	55	O	discontinuing
8	57	70	O	coadministered
9	72	77	B-D	enzyme
10	79	86	I-D	inducers
11	89	95	O	because
12	97	111	B-T	discontinuation
13	113	114	O	of
14	116	123	B-D	inducers
15	125	127	O	can
16	129	134	O	result
17	136	137	O	in
18	139	147	B-T	increased
19	149	157	I-T	XXXXXXXX
20	159	164	I-T	plasma
21	166	171	O	levels
22	173	175	O	and
23	177	178	O	an
24	180	188	O	increased
25	190	193	O	risk
26	195	196	O	of
27	198	204	B-E	adverse
28	206	214	I-E	reactions
D/9:27:1 D/14:27:1

Clozapine	3000	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	23
Drugs that Cause QT Interval Prolongation Use caution when administering concomitant medications that prolong the QT interval or inhibit the metabolism of clozapine.
1	0	4	B-U	Drugs
2	6	9	I-U	that
3	11	15	I-U	Cause
4	17	18	I-U	QT
5	20	27	I-U	Interval
6	29	40	B-T	Prolongation
7	42	44	B-T	Use
8	46	52	I-T	caution
9	54	57	O	when
10	59	71	O	administering
11	73	83	O	concomitant
12	85	95	B-U	medications
13	97	100	I-U	that
14	102	108	I-U	prolong
15	110	112	I-U	the
16	114	115	I-U	QT
17	117	124	I-U	interval
18	126	127	I-U	or
19	129	135	I-U	inhibit
20	137	139	I-U	the
21	141	150	I-U	metabolism
22	152	153	I-U	of
23	155	163	I-U	XXXXXXXX
NULL

Clozapine	3001	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	77
Drugs that cause QT prolongation include: specific antipsychotics (e.g., ziprasidone, iloperidone, chlorpromazine, thioridazine, mesoridazine, droperidol, and pimozide), specific antibiotics (e.g., erythromycin, gatifloxacin, moxifloxacin, sparfloxacin), Class 1A antiarrhythmics (e.g., quinidine, procainamide) or Class III antiarrhythmics (e.g., amiodarone, sotalol), and others (e.g., pentamidine, levomethadyl acetate, methadone, halofantrine, mefloquine, dolasetron mesylate, probucol or tacrolimus).
1	0	4	O	Drugs
2	6	9	B-U	that
3	11	15	I-U	cause
4	17	18	I-U	QT
5	20	31	O	prolongation
6	33	39	O	include
7	40	40	O	:
8	42	49	O	specific
9	51	64	O	antipsychotics
10	67	69	O	e.g
11	71	71	O	,
12	73	83	O	ziprasidone
13	84	84	O	,
14	86	96	O	iloperidone
15	97	97	O	,
16	99	112	O	chlorpromazine
17	113	113	O	,
18	115	126	O	thioridazine
19	127	127	O	,
20	129	140	O	mesoridazine
21	141	141	O	,
22	143	152	O	droperidol
23	153	153	O	,
24	155	157	O	and
25	159	166	O	pimozide
26	168	168	O	,
27	170	177	O	specific
28	179	189	O	antibiotics
29	192	194	O	e.g
30	196	196	O	,
31	198	209	O	erythromycin
32	210	210	O	,
33	212	223	O	gatifloxacin
34	224	224	O	,
35	226	237	O	moxifloxacin
36	238	238	O	,
37	240	251	O	sparfloxacin
38	253	253	O	,
39	255	259	O	Class
40	261	262	O	1A
41	264	278	O	antiarrhythmics
42	281	283	O	e.g
43	285	285	O	,
44	287	295	O	quinidine
45	296	296	O	,
46	298	309	O	procainamide
47	312	313	O	or
48	315	319	O	Class
49	321	323	O	III
50	325	339	O	antiarrhythmics
51	342	344	O	e.g
52	346	346	O	,
53	348	357	O	amiodarone
54	358	358	O	,
55	360	366	O	sotalol
56	368	368	O	,
57	370	372	O	and
58	374	379	O	others
59	382	384	O	e.g
60	386	386	O	,
61	388	398	O	pentamidine
62	399	399	O	,
63	401	412	O	levomethadyl
64	414	420	O	acetate
65	421	421	O	,
66	423	431	O	methadone
67	432	432	O	,
68	434	445	O	halofantrine
69	446	446	O	,
70	448	457	O	mefloquine
71	458	458	O	,
72	460	469	O	dolasetron
73	471	478	O	mesylate
74	479	479	O	,
75	481	488	O	probucol
76	490	491	O	or
77	493	502	O	tacrolimus
NULL

Clozapine	3002	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	17
Concomitant use of CLOZARIL with other drugs metabolized by CYP2D6 can increase levels of these CYP2D6 substrates.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	26	B-K	CLOZARIL
5	28	31	O	with
6	33	37	O	other
7	39	43	B-K	drugs
8	45	55	I-K	metabolized
9	57	58	I-K	by
10	60	65	I-K	CYP2D6
11	67	69	O	can
12	71	78	B-T	increase
13	80	85	I-T	levels
14	87	88	O	of
15	90	94	O	these
16	96	101	B-K	CYP2D6
17	103	112	I-K	substrates
K/4:C54357 K/7:C54357 K/16:C54357

Clozapine	3003	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	13
Use caution when coadministering CLOZARIL with other drugs that are metabolized by CYP2D6.
1	0	2	B-T	Use
2	4	10	I-T	caution
3	12	15	O	when
4	17	31	O	coadministering
5	33	40	O	CLOZARIL
6	42	45	O	with
7	47	51	O	other
8	53	57	B-U	drugs
9	59	62	I-U	that
10	64	66	I-U	are
11	68	78	I-U	metabolized
12	80	81	I-U	by
13	83	88	I-U	CYP2D6
NULL

Clozapine	3004	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	14
It may be necessary to use lower doses of such drugs than usually prescribed.
1	0	1	O	It
2	3	5	O	may
3	7	8	O	be
4	10	18	O	necessary
5	20	21	O	to
6	23	25	B-T	use
7	27	31	I-T	lower
8	33	37	I-T	doses
9	39	40	O	of
10	42	45	O	such
11	47	51	O	drugs
12	53	56	O	than
13	58	64	O	usually
14	66	75	O	prescribed
NULL

Clozapine	3005	34073-7	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	22
Such drugs include specific antidepressants, phenothiazines, carbamazepine, and Type 1C antiarrhythmics (e.g., propafenone, flecainide, and encainide).
1	0	3	O	Such
2	5	9	O	drugs
3	11	17	O	include
4	19	26	O	specific
5	28	42	O	antidepressants
6	43	43	O	,
7	45	58	O	phenothiazines
8	59	59	O	,
9	61	73	O	carbamazepine
10	74	74	O	,
11	76	78	O	and
12	80	83	O	Type
13	85	86	O	1C
14	88	102	O	antiarrhythmics
15	105	107	O	e.g
16	109	109	O	,
17	111	121	O	propafenone
18	122	122	O	,
19	124	133	O	flecainide
20	134	134	O	,
21	136	138	O	and
22	140	148	O	encainide
NULL

Clozapine	3006	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	8
The mechanism of action of clozapine is unknown.
1	0	2	O	The
2	4	12	O	mechanism
3	14	15	O	of
4	17	22	O	action
5	24	25	O	of
6	27	35	O	XXXXXXXX
7	37	38	O	is
8	40	46	O	unknown
NULL

Clozapine	3007	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	32
However, it has been proposed that the therapeutic efficacy of clozapine in schizophrenia is mediated through antagonism of the dopamine type 2 (D2) and the serotonin type 2A (5-HT2A) receptors.
1	0	6	O	However
2	7	7	O	,
3	9	10	O	it
4	12	14	O	has
5	16	19	O	been
6	21	28	O	proposed
7	30	33	O	that
8	35	37	O	the
9	39	49	O	therapeutic
10	51	58	O	efficacy
11	60	61	O	of
12	63	71	O	XXXXXXXX
13	73	74	O	in
14	76	88	O	schizophrenia
15	90	91	O	is
16	93	100	O	mediated
17	102	108	O	through
18	110	119	O	antagonism
19	121	122	O	of
20	124	126	O	the
21	128	135	O	dopamine
22	137	140	O	type
23	142	142	O	2
24	145	146	O	D2
25	149	151	O	and
26	153	155	O	the
27	157	165	O	serotonin
28	167	170	O	type
29	172	173	O	2A
30	176	176	O	5
31	178	181	O	HT2A
32	184	192	O	receptors
NULL

Clozapine	3008	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	18
CLOZARIL also acts as an antagonist at adrenergic, cholinergic, histaminergic and other dopaminergic and serotonergic receptors.
1	0	7	O	CLOZARIL
2	9	12	O	also
3	14	17	O	acts
4	19	20	O	as
5	22	23	O	an
6	25	34	O	antagonist
7	36	37	O	at
8	39	48	O	adrenergic
9	49	49	O	,
10	51	61	O	cholinergic
11	62	62	O	,
12	64	76	O	histaminergic
13	78	80	O	and
14	82	86	O	other
15	88	99	O	dopaminergic
16	101	103	O	and
17	105	116	O	serotonergic
18	118	126	O	receptors
NULL

Clozapine	3009	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	101
Clozapine demonstrated binding affinity to the following receptors: histamine H1 (Ki 1.1 nM), adrenergic A1A (Ki 1.6 nM), serotonin 5-HT6 (Ki 4 nM), serotonin 5-HT2A (Ki 5.4 nM), muscarinic M1 (Ki 6.2 nM), serotonin 5-HT7 (Ki 6.3 nM), serotonin 5-HT2C (Ki 9.4 nM), dopamine D4 (Ki 24 nM), adrenergic A2A (Ki 90 nM), serotonin 5-HT3 (Ki 95 nM), serotonin 5-HT1A (Ki 120 nM), dopamine D2 (Ki 160 nM), dopamine D1 (Ki 270 nM), dopamine D5 (Ki 454 nM), and dopamine D3 (Ki 555 nM).
1	0	8	O	XXXXXXXX
2	10	21	O	demonstrated
3	23	29	O	binding
4	31	38	O	affinity
5	40	41	O	to
6	43	45	O	the
7	47	55	O	following
8	57	65	O	receptors
9	66	66	O	:
10	68	76	O	histamine
11	78	79	O	H1
12	82	83	O	Ki
13	85	87	O	1.1
14	89	90	O	nM
15	92	92	O	,
16	94	103	O	adrenergic
17	105	107	O	A1A
18	110	111	O	Ki
19	113	115	O	1.6
20	117	118	O	nM
21	120	120	O	,
22	122	130	O	serotonin
23	132	132	O	5
24	134	136	O	HT6
25	139	140	O	Ki
26	142	142	O	4
27	144	145	O	nM
28	147	147	O	,
29	149	157	O	serotonin
30	159	159	O	5
31	161	164	O	HT2A
32	167	168	O	Ki
33	170	172	O	5.4
34	174	175	O	nM
35	177	177	O	,
36	179	188	O	muscarinic
37	190	191	O	M1
38	194	195	O	Ki
39	197	199	O	6.2
40	201	202	O	nM
41	204	204	O	,
42	206	214	O	serotonin
43	216	216	O	5
44	218	220	O	HT7
45	223	224	O	Ki
46	226	228	O	6.3
47	230	231	O	nM
48	233	233	O	,
49	235	243	O	serotonin
50	245	245	O	5
51	247	250	O	HT2C
52	253	254	O	Ki
53	256	258	O	9.4
54	260	261	O	nM
55	263	263	O	,
56	265	272	O	dopamine
57	274	275	O	D4
58	278	279	O	Ki
59	281	282	O	24
60	284	285	O	nM
61	287	287	O	,
62	289	298	O	adrenergic
63	300	302	O	A2A
64	305	306	O	Ki
65	308	309	O	90
66	311	312	O	nM
67	314	314	O	,
68	316	324	O	serotonin
69	326	326	O	5
70	328	330	O	HT3
71	333	334	O	Ki
72	336	337	O	95
73	339	340	O	nM
74	342	342	O	,
75	344	352	O	serotonin
76	354	354	O	5
77	356	359	O	HT1A
78	362	363	O	Ki
79	365	367	O	120
80	369	370	O	nM
81	372	372	O	,
82	374	381	O	dopamine
83	383	384	O	D2
84	387	388	O	Ki
85	390	392	O	160
86	394	395	O	nM
87	397	397	O	,
88	399	406	O	dopamine
89	408	409	O	D1
90	412	413	O	Ki
91	415	417	O	270
92	419	420	O	nM
93	422	422	O	,
94	424	431	O	dopamine
95	433	434	O	D5
96	437	438	O	Ki
97	440	442	O	454
98	444	445	O	nM
99	447	447	O	,
100	449	451	O	and
101	453	460	O	dopamine
NULL

Clozapine	3010	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	7
Clozapine causes little or no prolactin elevation.
1	0	8	O	XXXXXXXX
2	10	15	O	causes
3	17	22	O	little
4	24	25	O	or
5	27	28	O	no
6	30	38	O	prolactin
7	40	48	O	elevation
NULL

Clozapine	3011	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	17
Clinical electroencephalogram (EEG) studies demonstrated that clozapine increases delta and theta activity and slows dominant alpha frequencies.
1	0	7	O	Clinical
2	9	28	O	electroencephalogram
3	31	33	O	EEG
4	36	42	O	studies
5	44	55	O	demonstrated
6	57	60	O	that
7	62	70	O	XXXXXXXX
8	72	80	O	increases
9	82	86	O	delta
10	88	90	O	and
11	92	96	O	theta
12	98	105	O	activity
13	107	109	O	and
14	111	115	O	slows
15	117	124	O	dominant
16	126	130	O	alpha
17	132	142	O	frequencies
NULL

Clozapine	3012	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	11
Sharp wave activity and spike and wave complexes may also develop.
1	0	4	B-E	Sharp
2	6	9	I-E	wave
3	11	18	I-E	activity
4	20	22	O	and
5	24	28	O	spike
6	30	32	O	and
7	34	37	O	wave
8	39	47	O	complexes
9	49	51	O	may
10	53	56	O	also
11	58	64	O	develop
NULL

Clozapine	3013	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	11
Patients have reported an intensification of dream activity during clozapine therapy.
1	0	7	O	Patients
2	9	12	O	have
3	14	21	O	reported
4	23	24	O	an
5	26	40	O	intensification
6	42	43	O	of
7	45	49	O	dream
8	51	58	O	activity
9	60	65	O	during
10	67	75	O	XXXXXXXX
11	77	83	O	therapy
NULL

Clozapine	3014	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	14
REM sleep was found to be increased to 85% of the total sleep time.
1	0	2	O	REM
2	4	8	O	sleep
3	10	12	O	was
4	14	18	O	found
5	20	21	O	to
6	23	24	O	be
7	26	34	O	increased
8	36	37	O	to
9	39	41	O	85%
10	43	44	O	of
11	46	48	O	the
12	50	54	O	total
13	56	60	O	sleep
14	62	65	O	time
NULL

Clozapine	3015	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	15
In these patients, the onset of REM sleep occurred almost immediately after falling asleep.
1	0	1	O	In
2	3	7	O	these
3	9	16	O	patients
4	17	17	O	,
5	19	21	O	the
6	23	27	O	onset
7	29	30	O	of
8	32	34	O	REM
9	36	40	O	sleep
10	42	49	O	occurred
11	51	56	O	almost
12	58	68	O	immediately
13	70	74	O	after
14	76	82	O	falling
15	84	89	O	asleep
NULL

Clozapine	3016	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	19
Absorption In humans, CLOZARIL tablets (25 mg and 100 mg) are equally bioavailable relative to a CLOZARIL solution.
1	0	9	O	Absorption
2	11	12	O	In
3	14	19	O	humans
4	20	20	O	,
5	22	29	O	CLOZARIL
6	31	37	O	tablets
7	40	41	O	25
8	43	44	O	mg
9	46	48	O	and
10	50	52	O	100
11	54	55	O	mg
12	58	60	O	are
13	62	68	O	equally
14	70	81	O	bioavailable
15	83	90	O	relative
16	92	93	O	to
17	95	95	O	a
18	97	104	O	CLOZARIL
19	106	113	O	solution
NULL

Clozapine	3017	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	46
Following oral administration of CLOZARIL 100 mg twice daily, the average steady-state peak plasma concentration was 319 ng/mL (range: 102 to 771 ng/mL), occurring at the average of 2.5 hours (range: 1 to 6 hours) after dosing.
1	0	8	O	Following
2	10	13	O	oral
3	15	28	O	administration
4	30	31	O	of
5	33	40	O	CLOZARIL
6	42	44	O	100
7	46	47	O	mg
8	49	53	O	twice
9	55	59	O	daily
10	60	60	O	,
11	62	64	O	the
12	66	72	O	average
13	74	79	B-T	steady
14	81	85	I-T	state
15	87	90	I-T	peak
16	92	97	O	plasma
17	99	111	O	concentration
18	113	115	O	was
19	117	119	O	319
20	121	122	O	ng
21	123	123	O	/
22	124	125	O	mL
23	128	132	O	range
24	133	133	O	:
25	135	137	O	102
26	139	140	O	to
27	142	144	O	771
28	146	147	O	ng
29	148	148	O	/
30	149	150	O	mL
31	152	152	O	,
32	154	162	O	occurring
33	164	165	O	at
34	167	169	O	the
35	171	177	O	average
36	179	180	O	of
37	182	184	O	2.5
38	186	190	O	hours
39	193	197	O	range
40	198	198	O	:
41	200	200	O	1
42	202	203	O	to
43	205	205	O	6
44	207	211	O	hours
45	214	218	O	after
46	220	225	O	dosing
NULL

Clozapine	3018	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	27
The average minimum concentration at steady state was 122 ng/mL (range: 41 to 343 ng/mL), after 100 mg twice daily dosing.
1	0	2	O	The
2	4	10	O	average
3	12	18	O	minimum
4	20	32	O	concentration
5	34	35	O	at
6	37	42	O	steady
7	44	48	O	state
8	50	52	O	was
9	54	56	O	122
10	58	59	O	ng
11	60	60	O	/
12	61	62	O	mL
13	65	69	O	range
14	70	70	O	:
15	72	73	O	41
16	75	76	O	to
17	78	80	O	343
18	82	83	O	ng
19	84	84	O	/
20	85	86	O	mL
21	88	88	O	,
22	90	94	O	after
23	96	98	O	100
24	100	101	O	mg
25	103	107	O	twice
26	109	113	O	daily
27	115	120	O	dosing
NULL

Clozapine	3019	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	11
Food does not appear to affect the systemic bioavailability of CLOZARIL.
1	0	3	O	Food
2	5	8	O	does
3	10	12	O	not
4	14	19	O	appear
5	21	22	O	to
6	24	29	O	affect
7	31	33	O	the
8	35	42	O	systemic
9	44	58	O	bioavailability
10	60	61	O	of
11	63	70	O	CLOZARIL
NULL

Clozapine	3020	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	10
Thus, CLOZARIL may be administered with or without food.
1	0	3	O	Thus
2	4	4	O	,
3	6	13	O	CLOZARIL
4	15	17	O	may
5	19	20	O	be
6	22	33	O	administered
7	35	38	O	with
8	40	41	O	or
9	43	49	O	without
10	51	54	O	food
NULL

Clozapine	3021	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	9
Distribution Clozapine is approximately 97% bound to serum proteins.
1	0	11	O	Distribution
2	13	21	O	XXXXXXXX
3	23	24	O	is
4	26	38	O	approximately
5	40	42	O	97%
6	44	48	O	bound
7	50	51	O	to
8	53	57	O	serum
9	59	66	O	proteins
NULL

Clozapine	3022	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	19
The interaction between clozapine and other highly protein-bound drugs has not been fully evaluated but may be important.
1	0	2	O	The
2	4	14	O	interaction
3	16	22	O	between
4	24	32	O	XXXXXXXX
5	34	36	O	and
6	38	42	O	other
7	44	49	O	highly
8	51	57	O	protein
9	59	63	O	bound
10	65	69	O	drugs
11	71	73	O	has
12	75	77	O	not
13	79	82	O	been
14	84	88	O	fully
15	90	98	O	evaluated
16	100	102	O	but
17	104	106	O	may
18	108	109	O	be
19	111	119	O	important
NULL

Clozapine	3023	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	26
Metabolism and Excretion Clozapine is almost completely metabolized prior to excretion, and only trace amounts of unchanged drug are detected in the urine and feces.
1	0	9	O	Metabolism
2	11	13	O	and
3	15	23	O	Excretion
4	25	33	O	XXXXXXXX
5	35	36	O	is
6	38	43	O	almost
7	45	54	O	completely
8	56	66	O	metabolized
9	68	72	O	prior
10	74	75	O	to
11	77	85	O	excretion
12	86	86	O	,
13	88	90	O	and
14	92	95	O	only
15	97	101	O	trace
16	103	109	O	amounts
17	111	112	O	of
18	114	122	O	unchanged
19	124	127	O	drug
20	129	131	O	are
21	133	140	O	detected
22	142	143	O	in
23	145	147	O	the
24	149	153	O	urine
25	155	157	O	and
26	159	163	O	feces
NULL

Clozapine	3024	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	18
Clozapine is a substrate for many cytochrome P450 isozymes, in particular CYP1A2, CYP2D6, and CYP3A4.
1	0	8	O	XXXXXXXX
2	10	11	O	is
3	13	13	O	a
4	15	23	O	substrate
5	25	27	O	for
6	29	32	O	many
7	34	43	O	cytochrome
8	45	48	O	P450
9	50	57	O	isozymes
10	58	58	O	,
11	60	61	O	in
12	63	72	O	particular
13	74	79	O	CYP1A2
14	80	80	O	,
15	82	87	O	CYP2D6
16	88	88	O	,
17	90	92	O	and
18	94	99	O	CYP3A4
NULL

Clozapine	3025	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	16
Approximately 50% of the administered dose is excreted in the urine and 30% in the feces.
1	0	12	O	Approximately
2	14	16	O	50%
3	18	19	O	of
4	21	23	O	the
5	25	36	O	administered
6	38	41	O	dose
7	43	44	O	is
8	46	53	O	excreted
9	55	56	O	in
10	58	60	O	the
11	62	66	O	urine
12	68	70	O	and
13	72	74	O	30%
14	76	77	O	in
15	79	81	O	the
16	83	87	O	feces
NULL

Clozapine	3026	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	16
The demethylated, hydroxylated, and N-oxide derivatives are components in both urine and feces.
1	0	2	O	The
2	4	15	O	demethylated
3	16	16	O	,
4	18	29	O	hydroxylated
5	30	30	O	,
6	32	34	O	and
7	36	36	O	N
8	38	42	O	oxide
9	44	54	O	derivatives
10	56	58	O	are
11	60	69	O	components
12	71	72	O	in
13	74	77	O	both
14	79	83	O	urine
15	85	87	O	and
16	89	93	O	feces
NULL

Clozapine	3027	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	24
Pharmacological testing has shown the desmethyl metabolite (norclozapine) to have only limited activity, while the hydroxylated and N-oxide derivatives were inactive.
1	0	14	O	Pharmacological
2	16	22	O	testing
3	24	26	O	has
4	28	32	O	shown
5	34	36	O	the
6	38	46	O	desmethyl
7	48	57	O	metabolite
8	60	62	O	nor
9	63	71	O	XXXXXXXX
10	74	75	O	to
11	77	80	O	have
12	82	85	O	only
13	87	93	O	limited
14	95	102	O	activity
15	103	103	O	,
16	105	109	O	while
17	111	113	O	the
18	115	126	O	hydroxylated
19	128	130	O	and
20	132	132	O	N
21	134	138	O	oxide
22	140	150	O	derivatives
23	152	155	O	were
24	157	164	O	inactive
NULL

Clozapine	3028	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	49
The mean elimination half-life of clozapine after a single 75 mg dose was 8 hours (range: 4 to12 hours), compared to a mean elimination half-life of 12 hours (range: 4 to 66 hours), after achieving steady state with 100 mg twice daily dosing.
1	0	2	O	The
2	4	7	O	mean
3	9	19	B-T	elimination
4	21	24	O	half
5	26	29	O	life
6	31	32	O	of
7	34	42	O	XXXXXXXX
8	44	48	O	after
9	50	50	O	a
10	52	57	O	single
11	59	60	O	75
12	62	63	O	mg
13	65	68	O	dose
14	70	72	O	was
15	74	74	O	8
16	76	80	O	hours
17	83	87	O	range
18	88	88	O	:
19	90	90	O	4
20	92	95	O	to12
21	97	101	O	hours
22	103	103	O	,
23	105	112	O	compared
24	114	115	O	to
25	117	117	O	a
26	119	122	O	mean
27	124	134	B-T	elimination
28	136	139	I-T	half
29	141	144	O	life
30	146	147	O	of
31	149	150	O	12
32	152	156	O	hours
33	159	163	O	range
34	164	164	O	:
35	166	166	O	4
36	168	169	O	to
37	171	172	O	66
38	174	178	O	hours
39	180	180	O	,
40	182	186	O	after
41	188	196	O	achieving
42	198	203	O	steady
43	205	209	O	state
44	211	214	O	with
45	216	218	O	100
46	220	221	O	mg
47	223	227	O	twice
48	229	233	O	daily
49	235	240	O	dosing
NULL

Clozapine	3029	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	37
A comparison of single-dose and multiple-dose administration of clozapine demonstrated that the elimination half-life increased significantly after multiple dosing relative to that after single-dose administration, suggesting the possibility of concentration-dependent pharmacokinetics.
1	0	0	O	A
2	2	11	O	comparison
3	13	14	O	of
4	16	21	O	single
5	23	26	O	dose
6	28	30	O	and
7	32	39	O	multiple
8	41	44	O	dose
9	46	59	O	administration
10	61	62	O	of
11	64	72	O	XXXXXXXX
12	74	85	O	demonstrated
13	87	90	O	that
14	92	94	O	the
15	96	106	O	elimination
16	108	111	O	half
17	113	116	O	life
18	118	126	O	increased
19	128	140	O	significantly
20	142	146	O	after
21	148	155	O	multiple
22	157	162	O	dosing
23	164	171	O	relative
24	173	174	O	to
25	176	179	O	that
26	181	185	O	after
27	187	192	O	single
28	194	197	O	dose
29	199	212	O	administration
30	213	213	O	,
31	215	224	O	suggesting
32	226	228	O	the
33	230	240	O	possibility
34	242	243	O	of
35	245	257	O	concentration
36	259	267	O	dependent
37	269	284	O	pharmacokinetics
NULL

Clozapine	3030	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	40
However, at steady state, approximately dose-proportional changes with respect to AUC (area under the curve), peak, and minimum clozapine plasma concentrations were observed after administration of 37.5, 75, and 150 mg twice daily.
1	0	6	O	However
2	7	7	O	,
3	9	10	O	at
4	12	17	O	steady
5	19	23	O	state
6	24	24	O	,
7	26	38	O	approximately
8	40	43	O	dose
9	45	56	O	proportional
10	58	64	O	changes
11	66	69	O	with
12	71	77	O	respect
13	79	80	O	to
14	82	84	O	AUC
15	87	90	O	area
16	92	96	O	under
17	98	100	O	the
18	102	106	O	curve
19	108	108	O	,
20	110	113	O	peak
21	114	114	O	,
22	116	118	O	and
23	120	126	O	minimum
24	128	136	O	XXXXXXXX
25	138	143	O	plasma
26	145	158	O	concentrations
27	160	163	O	were
28	165	172	O	observed
29	174	178	O	after
30	180	193	O	administration
31	195	196	O	of
32	198	201	O	37.5
33	202	202	O	,
34	204	205	O	75
35	206	206	O	,
36	208	210	O	and
37	212	214	O	150
38	216	217	O	mg
39	219	223	O	twice
40	225	229	O	daily
NULL

Clozapine	3031	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	21
Drug-Drug Interaction Studies Fluvoxamine A pharmacokinetic study was conducted in 16 schizophrenic patients who received clozapine under steady-state conditions.
1	0	3	O	Drug
2	5	8	O	Drug
3	10	20	O	Interaction
4	22	28	O	Studies
5	30	40	O	Fluvoxamine
6	42	42	O	A
7	44	58	O	pharmacokinetic
8	60	64	O	study
9	66	68	O	was
10	70	78	O	conducted
11	80	81	O	in
12	83	84	O	16
13	86	98	O	schizophrenic
14	100	107	O	patients
15	109	111	O	who
16	113	120	O	received
17	122	130	O	XXXXXXXX
18	132	136	O	under
19	138	143	O	steady
20	145	149	O	state
21	151	160	O	conditions
NULL

Clozapine	3032	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	36
After coadministration of fluvoxamine for 14 days, mean trough concentrations of clozapine and its metabolites, N-desmethylclozapine and clozapine N-oxide, were elevated about 3-fold compared to baseline steady-state concentrations.
1	0	4	O	After
2	6	21	O	coadministration
3	23	24	O	of
4	26	36	B-K	fluvoxamine
5	38	40	O	for
6	42	43	O	14
7	45	48	O	days
8	49	49	O	,
9	51	54	O	mean
10	56	61	O	trough
11	63	76	O	concentrations
12	78	79	O	of
13	81	89	O	XXXXXXXX
14	91	93	O	and
15	95	97	O	its
16	99	109	O	metabolites
17	110	110	O	,
18	112	112	O	N
19	114	122	O	desmethyl
20	123	131	O	XXXXXXXX
21	133	135	O	and
22	137	145	O	XXXXXXXX
23	147	147	O	N
24	149	153	O	oxide
25	154	154	O	,
26	156	159	O	were
27	161	168	O	elevated
28	170	174	O	about
29	176	176	O	3
30	178	181	O	fold
31	183	190	O	compared
32	192	193	O	to
33	195	202	O	baseline
34	204	209	B-T	steady
35	211	215	I-T	state
36	217	230	I-T	concentrations
K/4:C54355

Clozapine	3033	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	36
Paroxetine, Fluoxetine, and Sertraline In a study of schizophrenic patients (n=14) who received clozapine under steady-state conditions, coadministration of paroxetine produced only minor changes in the levels of clozapine and its metabolites.
1	0	9	O	Paroxetine
2	10	10	O	,
3	12	21	O	Fluoxetine
4	22	22	O	,
5	24	26	O	and
6	28	37	O	Sertraline
7	39	40	O	In
8	42	42	O	a
9	44	48	O	study
10	50	51	O	of
11	53	65	O	schizophrenic
12	67	74	O	patients
13	77	80	O	n=14
14	83	85	O	who
15	87	94	O	received
16	96	104	O	XXXXXXXX
17	106	110	O	under
18	112	117	O	steady
19	119	123	O	state
20	125	134	O	conditions
21	135	135	O	,
22	137	152	O	coadministration
23	154	155	O	of
24	157	166	O	paroxetine
25	168	175	O	produced
26	177	180	O	only
27	182	186	O	minor
28	188	194	O	changes
29	196	197	O	in
30	199	201	O	the
31	203	208	O	levels
32	210	211	O	of
33	213	221	O	XXXXXXXX
34	223	225	O	and
35	227	229	O	its
36	231	241	O	metabolites
NULL

Clozapine	3034	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	28
However, other published reports describe modest elevations (less than 2-fold) of clozapine and metabolite concentrations when clozapine was taken with paroxetine, fluoxetine, and sertraline.
1	0	6	O	However
2	7	7	O	,
3	9	13	O	other
4	15	23	O	published
5	25	31	O	reports
6	33	40	O	describe
7	42	47	O	modest
8	49	58	O	elevations
9	61	64	O	less
10	66	69	O	than
11	71	71	O	2
12	73	76	O	fold
13	79	80	O	of
14	82	90	O	XXXXXXXX
15	92	94	O	and
16	96	105	O	metabolite
17	107	120	O	concentrations
18	122	125	O	when
19	127	135	O	XXXXXXXX
20	137	139	O	was
21	141	145	O	taken
22	147	150	O	with
23	152	161	O	paroxetine
24	162	162	O	,
25	164	173	O	fluoxetine
26	174	174	O	,
27	176	178	O	and
28	180	189	O	sertraline
NULL

Clozapine	3035	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	27
Specific Population Studies Renal or Hepatic Impairment No specific pharmacokinetic studies were conducted to investigate the effects of renal or hepatic impairment on the pharmacokinetics of clozapine.
1	0	7	O	Specific
2	9	18	O	Population
3	20	26	O	Studies
4	28	32	O	Renal
5	34	35	O	or
6	37	43	O	Hepatic
7	45	54	O	Impairment
8	56	57	O	No
9	59	66	O	specific
10	68	82	O	pharmacokinetic
11	84	90	O	studies
12	92	95	O	were
13	97	105	O	conducted
14	107	108	O	to
15	110	120	O	investigate
16	122	124	O	the
17	126	132	O	effects
18	134	135	O	of
19	137	141	O	renal
20	143	144	O	or
21	146	152	O	hepatic
22	154	163	O	impairment
23	165	166	O	on
24	168	170	O	the
25	172	187	O	pharmacokinetics
26	189	190	O	of
27	192	200	O	XXXXXXXX
NULL

Clozapine	3036	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	18
Higher clozapine plasma concentrations are likely in patients with significant renal or hepatic impairment when given usual doses.
1	0	5	O	Higher
2	7	15	O	XXXXXXXX
3	17	22	B-T	plasma
4	24	37	I-T	concentrations
5	39	41	O	are
6	43	48	O	likely
7	50	51	O	in
8	53	60	O	patients
9	62	65	O	with
10	67	77	O	significant
11	79	83	O	renal
12	85	86	O	or
13	88	94	O	hepatic
14	96	105	O	impairment
15	107	110	O	when
16	112	116	O	given
17	118	122	O	usual
18	124	128	O	doses
NULL

Clozapine	3037	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	18
CYP2D6 Poor Metabolizers A subset (3%-10%) of the population has reduced activity of CYP2D6 (CYP2D6 poor metabolizers).
1	0	5	O	CYP2D6
2	7	10	O	Poor
3	12	23	O	Metabolizers
4	25	25	O	A
5	27	32	O	subset
6	35	36	O	3%
7	38	40	O	10%
8	43	44	O	of
9	46	48	O	the
10	50	59	B-T	population
11	61	63	I-T	has
12	65	71	I-T	reduced
13	73	80	I-T	activity
14	82	83	O	of
15	85	90	B-K	CYP2D6
16	93	98	I-K	CYP2D6
17	100	103	O	poor
18	105	116	O	metabolizers
K/15:C54358

Clozapine	3038	34090-1	5f0c6f5f-b906-4c8f-8580-3939a476a1c1	15
These individuals may develop higher than expected plasma concentrations of clozapine when given usual doses.
1	0	4	O	These
2	6	16	O	individuals
3	18	20	O	may
4	22	28	O	develop
5	30	35	O	higher
6	37	40	O	than
7	42	49	O	expected
8	51	56	O	plasma
9	58	71	O	concentrations
10	73	74	O	of
11	76	84	O	XXXXXXXX
12	86	89	O	when
13	91	95	O	given
14	97	101	O	usual
15	103	107	O	doses
NULL

