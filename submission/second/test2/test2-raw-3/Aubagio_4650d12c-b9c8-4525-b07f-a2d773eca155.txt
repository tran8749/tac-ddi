Aubagio	3148	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	27
Drugs metabolized by CYP2C8 and OAT3 transporters: Monitor patients because teriflunomide may increase exposure of these drugs (7) Teriflunomide may increase exposure of ethinylestradiol and levonorgestrel.
1	0	4	B-K	Drugs
2	6	16	I-K	metabolized
3	18	19	O	by
4	21	26	O	CYP2C8
5	28	30	O	and
6	32	35	O	OAT3
7	37	48	O	transporters
8	49	49	O	:
9	51	57	O	Monitor
10	59	66	O	patients
11	68	74	O	because
12	76	88	O	XXXXXXXX
13	90	92	O	may
14	94	101	B-T	increase
15	103	110	I-T	exposure
16	112	113	O	of
17	115	119	O	these
18	121	125	O	drugs
19	128	128	O	7
20	131	143	O	XXXXXXXX
21	145	147	O	may
22	149	156	B-T	increase
23	158	165	I-T	exposure
24	167	168	O	of
25	170	185	B-K	ethinylestradiol
26	187	189	O	and
27	191	204	B-K	levonorgestrel
K/1:C54357 K/25:C54357 K/27:C54357

Aubagio	3149	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	85
Choose an appropriate oral contraceptive (7) Drugs metabolized by CYP1A2: Monitor patients because teriflunomide may decrease exposure of these drugs (7) Warfarin: Monitor INR as teriflunomide may decrease INR (7) Drugs metabolized by BCRP and OATP1B1/B3 transporters: Monitor patients because teriflunomide may increase exposure of these drugs (7) Rosuvastatin: The dose of rosuvastatin should not exceed 10 mg once daily in patients taking AUBAGIO (7) Effect of AUBAGIO on CYP2C8 substrates Teriflunomide is an inhibitor of CYP2C8 in vivo.
1	0	5	O	Choose
2	7	8	O	an
3	10	20	O	appropriate
4	22	25	B-U	oral
5	27	39	O	contraceptive
6	42	42	O	7
7	45	49	O	Drugs
8	51	61	B-K	metabolized
9	63	64	O	by
10	66	71	O	CYP1A2
11	72	72	O	:
12	74	80	O	Monitor
13	82	89	O	patients
14	91	97	O	because
15	99	111	O	XXXXXXXX
16	113	115	O	may
17	117	124	B-T	decrease
18	126	133	I-T	exposure
19	135	136	O	of
20	138	142	O	these
21	144	148	O	drugs
22	151	151	O	7
23	154	161	O	Warfarin
24	162	162	O	:
25	164	170	O	Monitor
26	172	174	O	INR
27	176	177	O	as
28	179	191	O	XXXXXXXX
29	193	195	O	may
30	197	204	O	decrease
31	206	208	B-E	INR
32	211	211	O	7
33	214	218	B-K	Drugs
34	220	230	I-K	metabolized
35	232	233	O	by
36	235	238	B-K	BCRP
37	240	242	O	and
38	244	250	O	OATP1B1
39	251	251	B-K	/
40	252	253	I-K	B3
41	255	266	I-K	transporters
42	267	267	O	:
43	269	275	O	Monitor
44	277	284	O	patients
45	286	292	O	because
46	294	306	O	XXXXXXXX
47	308	310	O	may
48	312	319	B-T	increase
49	321	328	I-T	exposure
50	330	331	O	of
51	333	337	O	these
52	339	343	O	drugs
53	346	346	O	7
54	349	360	B-K	Rosuvastatin
55	361	361	O	:
56	363	365	O	The
57	367	370	O	dose
58	372	373	O	of
59	375	386	B-K	rosuvastatin
60	388	393	O	should
61	395	397	B-T	not
62	399	404	I-T	exceed
63	406	407	O	10
64	409	410	O	mg
65	412	415	O	once
66	417	421	O	daily
67	423	424	O	in
68	426	433	O	patients
69	435	440	O	taking
70	442	448	O	XXXXXXXX
71	451	451	O	7
72	454	459	O	Effect
73	461	462	O	of
74	464	470	O	XXXXXXXX
75	472	473	O	on
76	475	480	O	CYP2C8
77	482	491	O	substrates
78	493	505	O	XXXXXXXX
79	507	508	O	is
80	510	511	O	an
81	513	521	O	inhibitor
82	523	524	O	of
83	526	531	O	CYP2C8
84	533	534	O	in
85	536	539	O	vivo
K/8:C54355 K/33:C54355 K/36:C54355 K/39:C54357 K/54:C54355 K/59:C54355

Aubagio	3150	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	23
In patients taking AUBAGIO, exposure of drugs metabolized by CYP2C8 (e.g., paclitaxel, pioglitazone, repaglinide, rosiglitazone) may be increased.
1	0	1	O	In
2	3	10	O	patients
3	12	17	O	taking
4	19	25	O	XXXXXXXX
5	26	26	O	,
6	28	35	O	exposure
7	37	38	O	of
8	40	44	B-K	drugs
9	46	56	I-K	metabolized
10	58	59	I-K	by
11	61	66	I-K	CYP2C8
12	69	71	O	e.g
13	73	73	O	,
14	75	84	B-K	paclitaxel
15	85	85	O	,
16	87	98	O	pioglitazone
17	99	99	O	,
18	101	111	B-K	repaglinide
19	112	112	O	,
20	114	126	B-K	rosiglitazone
21	129	131	O	may
22	133	134	O	be
23	136	144	B-T	increased
K/8:C54355 K/14:C54355 K/18:C54355 K/20:C54358

Aubagio	3151	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	16
Monitor these patients and adjust the dose of the concomitant drug(s) metabolized by CYP2C8 as required.
1	0	6	B-T	Monitor
2	8	12	O	these
3	14	21	O	patients
4	23	25	O	and
5	27	32	B-T	adjust
6	34	36	O	the
7	38	41	B-T	dose
8	43	44	O	of
9	46	48	O	the
10	50	60	O	concomitant
11	62	67	O	drug(s
12	70	80	B-U	metabolized
13	82	83	I-U	by
14	85	90	I-U	CYP2C8
15	92	93	O	as
16	95	102	O	required
NULL

Aubagio	3152	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	28
Effect of AUBAGIO on warfarin Coadministration of AUBAGIO with warfarin requires close monitoring of the international normalized ratio (INR) because AUBAGIO may decrease peak INR by approximately 25%.
1	0	5	O	Effect
2	7	8	O	of
3	10	16	O	XXXXXXXX
4	18	19	O	on
5	21	28	O	warfarin
6	30	45	O	Coadministration
7	47	48	O	of
8	50	56	O	XXXXXXXX
9	58	61	O	with
10	63	70	O	warfarin
11	72	79	O	requires
12	81	85	O	close
13	87	96	O	monitoring
14	98	99	O	of
15	101	103	O	the
16	105	117	O	international
17	119	128	O	normalized
18	130	134	O	ratio
19	137	139	O	INR
20	142	148	O	because
21	150	156	O	XXXXXXXX
22	158	160	O	may
23	162	169	O	decrease
24	171	174	O	peak
25	176	178	O	INR
26	180	181	O	by
27	183	195	O	approximately
28	197	199	O	25%
NULL

Aubagio	3153	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	16
Effect of AUBAGIO on oral contraceptives AUBAGIO may increase the systemic exposures of ethinylestradiol and levonorgestrel.
1	0	5	O	Effect
2	7	8	O	of
3	10	16	O	XXXXXXXX
4	18	19	O	on
5	21	24	B-K	oral
6	26	39	I-K	contraceptives
7	41	47	O	XXXXXXXX
8	49	51	O	may
9	53	60	B-T	increase
10	62	64	I-T	the
11	66	73	I-T	systemic
12	75	83	I-T	exposures
13	85	86	O	of
14	88	103	B-K	ethinylestradiol
15	105	107	O	and
16	109	122	O	levonorgestrel
K/5:C54358 K/14:C54358

Aubagio	3154	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	16
Consideration should be given to the type or dose of contraceptives used in combination with AUBAGIO.
1	0	12	O	Consideration
2	14	19	O	should
3	21	22	O	be
4	24	28	O	given
5	30	31	O	to
6	33	35	O	the
7	37	40	O	type
8	42	43	O	or
9	45	48	O	dose
10	50	51	O	of
11	53	66	B-U	contraceptives
12	68	71	O	used
13	73	74	O	in
14	76	86	O	combination
15	88	91	O	with
16	93	99	O	XXXXXXXX
NULL

Aubagio	3155	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	16
Effect of AUBAGIO on CYP1A2 substrates Teriflunomide may be a weak inducer of CYP1A2 in vivo.
1	0	5	O	Effect
2	7	8	O	of
3	10	16	O	XXXXXXXX
4	18	19	O	on
5	21	26	O	CYP1A2
6	28	37	O	substrates
7	39	51	O	XXXXXXXX
8	53	55	O	may
9	57	58	O	be
10	60	60	O	a
11	62	65	O	weak
12	67	73	O	inducer
13	75	76	O	of
14	78	83	O	CYP1A2
15	85	86	O	in
16	88	91	O	vivo
NULL

Aubagio	3156	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	23
In patients taking AUBAGIO, exposure of drugs metabolized by CYP1A2 (e.g., alosetron, duloxetine, theophylline, tizanidine) may be reduced.
1	0	1	O	In
2	3	10	O	patients
3	12	17	O	taking
4	19	25	O	XXXXXXXX
5	26	26	O	,
6	28	35	O	exposure
7	37	38	O	of
8	40	44	B-K	drugs
9	46	56	I-K	metabolized
10	58	59	I-K	by
11	61	66	I-K	CYP1A2
12	69	71	O	e.g
13	73	73	O	,
14	75	83	O	alosetron
15	84	84	O	,
16	86	95	O	duloxetine
17	96	96	O	,
18	98	109	B-K	theophylline
19	110	110	O	,
20	112	121	B-K	tizanidine
21	124	126	O	may
22	128	129	B-T	be
23	131	137	I-T	reduced
K/8:C54357 K/18:C54358 K/20:C54358

Aubagio	3157	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	16
Monitor these patients and adjust the dose of the concomitant drug(s) metabolized by CYP1A2 as required.
1	0	6	B-T	Monitor
2	8	12	O	these
3	14	21	O	patients
4	23	25	O	and
5	27	32	B-T	adjust
6	34	36	O	the
7	38	41	B-T	dose
8	43	44	O	of
9	46	48	O	the
10	50	60	O	concomitant
11	62	67	O	drug(s
12	70	80	B-U	metabolized
13	82	83	I-U	by
14	85	90	I-U	CYP1A2
15	92	93	O	as
16	95	102	O	required
NULL

Aubagio	3158	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	18
Effect of AUBAGIO on organic anion transporter 3 (OAT3) substrates Teriflunomide inhibits the activity of OAT3 in vivo.
1	0	5	O	Effect
2	7	8	O	of
3	10	16	O	XXXXXXXX
4	18	19	O	on
5	21	27	B-D	organic
6	29	33	I-D	anion
7	35	45	O	transporter
8	47	47	O	3
9	50	53	O	OAT3
10	56	65	O	substrates
11	67	79	O	XXXXXXXX
12	81	88	O	inhibits
13	90	92	O	the
14	94	101	O	activity
15	103	104	O	of
16	106	109	O	OAT3
17	111	112	O	in
18	114	117	O	vivo
NULL

Aubagio	3159	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	33
In patients taking AUBAGIO, exposure of drugs which are OAT3 substrates (e.g., cefaclor, cimetidine, ciprofloxacin, penicillin G, ketoprofen, furosemide, methotrexate, zidovudine) may be increased.
1	0	1	O	In
2	3	10	O	patients
3	12	17	O	taking
4	19	25	O	XXXXXXXX
5	26	26	O	,
6	28	35	O	exposure
7	37	38	O	of
8	40	44	B-K	drugs
9	46	50	I-K	which
10	52	54	I-K	are
11	56	59	I-K	OAT3
12	61	70	I-K	substrates
13	73	75	O	e.g
14	77	77	O	,
15	79	86	O	cefaclor
16	87	87	O	,
17	89	98	O	cimetidine
18	99	99	O	,
19	101	113	O	ciprofloxacin
20	114	114	O	,
21	116	125	O	penicillin
22	127	127	O	GGGGGGGG
23	128	128	O	,
24	130	139	O	ketoprofen
25	140	140	O	,
26	142	151	O	furosemide
27	152	152	O	,
28	154	165	B-K	methotrexate
29	166	166	O	,
30	168	177	B-K	zidovudine
31	180	182	O	may
32	184	185	O	be
33	187	195	B-T	increased
K/8:C54355 K/28:C54355 K/30:C54355

Aubagio	3160	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	17
Monitor these patients and adjust the dose of the concomitant drug(s) which are OAT3 substrates as required.
1	0	6	B-T	Monitor
2	8	12	O	these
3	14	21	O	patients
4	23	25	O	and
5	27	32	B-T	adjust
6	34	36	I-T	the
7	38	41	I-T	dose
8	43	44	O	of
9	46	48	O	the
10	50	60	O	concomitant
11	62	67	O	drug(s
12	70	74	O	which
13	76	78	O	are
14	80	83	B-U	OAT3
15	85	94	I-U	substrates
16	96	97	O	as
17	99	106	O	required
NULL

Aubagio	3161	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	29
Effect of AUBAGIO on BCRP and organic anion transporting polypeptide B1 and B3 (OATP1B1/1B3) substrates Teriflunomide inhibits the activity of BCRP and OATP1B1/1B3 in vivo.
1	0	5	B-T	Effect
2	7	8	O	of
3	10	16	O	XXXXXXXX
4	18	19	O	on
5	21	24	O	BCRP
6	26	28	O	and
7	30	36	B-U	organic
8	38	42	I-U	anion
9	44	55	O	transporting
10	57	67	O	polypeptide
11	69	70	O	B1
12	72	74	O	and
13	76	77	O	B3
14	80	86	O	OATP1B1
15	87	87	O	/
16	88	90	O	1B3
17	93	102	O	substrates
18	104	116	O	XXXXXXXX
19	118	125	B-T	inhibits
20	127	129	I-T	the
21	131	138	I-T	activity
22	140	141	O	of
23	143	146	O	BCRP
24	148	150	O	and
25	152	158	O	OATP1B1
26	159	159	O	/
27	160	162	O	1B3
28	164	165	O	in
29	167	170	O	vivo
NULL

Aubagio	3162	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	17
For a patient taking AUBAGIO, the dose of rosuvastatin should not exceed 10 mg once daily.
1	0	2	O	For
2	4	4	O	a
3	6	12	O	patient
4	14	19	O	taking
5	21	27	O	XXXXXXXX
6	28	28	O	,
7	30	32	O	the
8	34	37	O	dose
9	39	40	O	of
10	42	53	B-U	rosuvastatin
11	55	60	O	should
12	62	64	B-T	not
13	66	71	I-T	exceed
14	73	74	O	10
15	76	77	O	mg
16	79	82	O	once
17	84	88	O	daily
NULL

Aubagio	3163	34073-7	4650d12c-b9c8-4525-b07f-a2d773eca155	64
For other substrates of BCRP (e.g., mitoxantrone) and drugs in the OATP family (e.g., methotrexate, rifampin), especially HMG-Co reductase inhibitors (e.g., atorvastatin, nateglinide, pravastatin, repaglinide, and simvastatin), consider reducing the dose of these drugs and monitor patients closely for signs and symptoms of increased exposures to the drugs while patients are taking AUBAGIO.
1	0	2	O	For
2	4	8	O	other
3	10	19	B-U	substrates
4	21	22	I-U	of
5	24	27	I-U	BCRP
6	30	32	O	e.g
7	34	34	O	,
8	36	47	B-U	mitoxantrone
9	50	52	O	and
10	54	58	B-U	drugs
11	60	61	O	in
12	63	65	B-U	the
13	67	70	I-U	OATP
14	72	77	O	family
15	80	82	O	e.g
16	84	84	O	,
17	86	97	B-U	methotrexate
18	98	98	O	,
19	100	107	B-U	rifampin
20	109	109	O	,
21	111	120	O	especially
22	122	124	B-U	HMG
23	126	127	I-U	Co
24	129	137	I-U	reductase
25	139	148	I-U	inhibitors
26	151	153	O	e.g
27	155	155	O	,
28	157	168	B-U	atorvastatin
29	169	169	O	,
30	171	181	B-U	nateglinide
31	182	182	O	,
32	184	194	O	pravastatin
33	195	195	O	,
34	197	207	B-U	repaglinide
35	208	208	O	,
36	210	212	O	and
37	214	224	B-U	simvastatin
38	226	226	O	,
39	228	235	O	consider
40	237	244	B-T	reducing
41	246	248	I-T	the
42	250	253	I-T	dose
43	255	256	O	of
44	258	262	O	these
45	264	268	O	drugs
46	270	272	O	and
47	274	280	O	monitor
48	282	289	O	patients
49	291	297	O	closely
50	299	301	O	for
51	303	307	O	signs
52	309	311	O	and
53	313	320	O	symptoms
54	322	323	O	of
55	325	333	O	increased
56	335	343	O	exposures
57	345	346	O	to
58	348	350	O	the
59	352	356	O	drugs
60	358	362	O	while
61	364	371	O	patients
62	373	375	O	are
63	377	382	O	taking
64	384	390	O	XXXXXXXX
NULL

Aubagio	3164	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	23
Teriflunomide, an immunomodulatory agent with anti-inflammatory properties, inhibits dihydroorotate dehydrogenase, a mitochondrial enzyme involved in de novo pyrimidine synthesis.
1	0	12	O	XXXXXXXX
2	13	13	O	,
3	15	16	O	an
4	18	33	O	immunomodulatory
5	35	39	O	agent
6	41	44	O	with
7	46	49	O	anti
8	51	62	O	inflammatory
9	64	73	O	properties
10	74	74	O	,
11	76	83	O	inhibits
12	85	98	O	dihydroorotate
13	100	112	O	dehydrogenase
14	113	113	O	,
15	115	115	O	a
16	117	129	O	mitochondrial
17	131	136	O	enzyme
18	138	145	O	involved
19	147	148	O	in
20	150	151	O	de
21	153	156	O	novo
22	158	167	O	pyrimidine
23	169	177	O	synthesis
NULL

Aubagio	3165	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	28
The exact mechanism by which teriflunomide exerts its therapeutic effect in multiple sclerosis is unknown but may involve a reduction in the number of activated lymphocytes in CNS.
1	0	2	O	The
2	4	8	O	exact
3	10	18	O	mechanism
4	20	21	O	by
5	23	27	O	which
6	29	41	O	XXXXXXXX
7	43	48	O	exerts
8	50	52	O	its
9	54	64	O	therapeutic
10	66	71	O	effect
11	73	74	O	in
12	76	83	O	multiple
13	85	93	O	sclerosis
14	95	96	O	is
15	98	104	O	unknown
16	106	108	O	but
17	110	112	O	may
18	114	120	O	involve
19	122	122	O	a
20	124	132	O	reduction
21	134	135	O	in
22	137	139	O	the
23	141	146	O	number
24	148	149	O	of
25	151	159	O	activated
26	161	171	O	lymphocytes
27	173	174	O	in
28	176	178	O	CNS
NULL

Aubagio	3166	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	54
Potential to prolong the QT interval In a placebo controlled thorough QT study performed in healthy subjects, there was no evidence that teriflunomide caused QT interval prolongation of clinical significance (i.e., the upper bound of the 90% confidence interval for the largest placebo-adjusted, baseline-corrected QTc was below 10 ms).
1	0	8	O	Potential
2	10	11	O	to
3	13	19	O	prolong
4	21	23	O	the
5	25	26	O	QT
6	28	35	O	interval
7	37	38	O	In
8	40	40	O	a
9	42	48	O	placebo
10	50	59	O	controlled
11	61	68	O	thorough
12	70	71	O	QT
13	73	77	O	study
14	79	87	O	performed
15	89	90	O	in
16	92	98	O	healthy
17	100	107	O	subjects
18	108	108	O	,
19	110	114	O	there
20	116	118	O	was
21	120	121	O	no
22	123	130	O	evidence
23	132	135	O	that
24	137	149	O	XXXXXXXX
25	151	156	O	caused
26	158	159	O	QT
27	161	168	O	interval
28	170	181	O	prolongation
29	183	184	O	of
30	186	193	O	clinical
31	195	206	O	significance
32	209	211	O	i.e
33	213	213	O	,
34	215	217	O	the
35	219	223	O	upper
36	225	229	O	bound
37	231	232	O	of
38	234	236	O	the
39	238	240	O	90%
40	242	251	O	confidence
41	253	260	O	interval
42	262	264	O	for
43	266	268	O	the
44	270	276	O	largest
45	278	284	O	placebo
46	286	293	O	adjusted
47	294	294	O	,
48	296	303	O	baseline
49	305	313	O	corrected
50	315	317	O	QTc
51	319	321	O	was
52	323	327	O	below
53	329	330	O	10
54	332	333	O	ms
NULL

Aubagio	3167	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	16
Teriflunomide is the principal active metabolite of leflunomide and is responsible for leflunomide's activity in vivo.
1	0	12	O	XXXXXXXX
2	14	15	O	is
3	17	19	O	the
4	21	29	O	principal
5	31	36	O	active
6	38	47	O	metabolite
7	49	50	O	of
8	52	62	O	leflunomide
9	64	66	O	and
10	68	69	O	is
11	71	81	O	responsible
12	83	85	O	for
13	87	99	O	leflunomide's
14	101	108	O	activity
15	110	111	O	in
16	113	116	O	vivo
NULL

Aubagio	3168	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	17
At recommended doses, teriflunomide and leflunomide result in a similar range of plasma concentrations of teriflunomide.
1	0	1	O	At
2	3	13	O	recommended
3	15	19	O	doses
4	20	20	O	,
5	22	34	O	XXXXXXXX
6	36	38	O	and
7	40	50	B-K	leflunomide
8	52	57	O	result
9	59	60	O	in
10	62	62	O	a
11	64	70	O	similar
12	72	76	O	range
13	78	79	O	of
14	81	86	O	plasma
15	88	101	O	concentrations
16	103	104	O	of
17	106	118	O	XXXXXXXX
K/7:C54356

Aubagio	3169	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	34
Based on a population analysis of teriflunomide in healthy volunteers and MS patients, median t1/2 was approximately 18 and 19 days after repeated doses of 7 mg and 14 mg respectively.
1	0	4	O	Based
2	6	7	O	on
3	9	9	O	a
4	11	20	O	population
5	22	29	O	analysis
6	31	32	O	of
7	34	46	O	XXXXXXXX
8	48	49	O	in
9	51	57	O	healthy
10	59	68	O	volunteers
11	70	72	O	and
12	74	75	O	MS
13	77	84	O	patients
14	85	85	O	,
15	87	92	O	median
16	94	95	O	t1
17	96	96	O	/
18	97	97	O	2
19	99	101	O	was
20	103	115	O	approximately
21	117	118	O	18
22	120	122	O	and
23	124	125	O	19
24	127	130	O	days
25	132	136	O	after
26	138	145	O	repeated
27	147	151	O	doses
28	153	154	O	of
29	156	156	O	7
30	158	159	O	mg
31	161	163	O	and
32	165	166	O	14
33	168	169	O	mg
34	171	182	O	respectively
NULL

Aubagio	3170	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	11
It takes approximately 3 months respectively to reach steady-state concentrations.
1	0	1	O	It
2	3	7	O	takes
3	9	21	O	approximately
4	23	23	O	3
5	25	30	O	months
6	32	43	O	respectively
7	45	46	O	to
8	48	52	O	reach
9	54	59	B-T	steady
10	61	65	I-T	state
11	67	80	I-T	concentrations
NULL

Aubagio	3171	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	37
The estimated AUC accumulation ratio is approximately 30 after repeated doses of 7 or 14 mg. Absorption Median time to reach maximum plasma concentrations is between 1 to 4 hours post dose following oral administration of teriflunomide.
1	0	2	O	The
2	4	12	O	estimated
3	14	16	B-T	AUC
4	18	29	I-T	accumulation
5	31	35	O	ratio
6	37	38	O	is
7	40	52	O	approximately
8	54	55	O	30
9	57	61	O	after
10	63	70	O	repeated
11	72	76	O	doses
12	78	79	O	of
13	81	81	O	7
14	83	84	O	or
15	86	87	O	14
16	89	90	O	mg
17	93	102	O	Absorption
18	104	109	O	Median
19	111	114	O	time
20	116	117	O	to
21	119	123	O	reach
22	125	131	O	maximum
23	133	138	O	plasma
24	140	153	O	concentrations
25	155	156	O	is
26	158	164	O	between
27	166	166	O	1
28	168	169	O	to
29	171	171	O	4
30	173	177	O	hours
31	179	182	O	post
32	184	187	O	dose
33	189	197	O	following
34	199	202	O	oral
35	204	217	O	administration
36	219	220	O	of
37	222	234	O	XXXXXXXX
NULL

Aubagio	3172	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	11
Food does not have a clinically relevant effect on teriflunomide pharmacokinetics.
1	0	3	O	Food
2	5	8	O	does
3	10	12	O	not
4	14	17	O	have
5	19	19	O	a
6	21	30	O	clinically
7	32	39	O	relevant
8	41	46	O	effect
9	48	49	O	on
10	51	63	O	XXXXXXXX
11	65	80	O	pharmacokinetics
NULL

Aubagio	3173	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	15
Distribution Teriflunomide is extensively bound to plasma protein (>99%) and is mainly distributed in plasma.
1	0	11	O	Distribution
2	13	25	O	XXXXXXXX
3	27	28	O	is
4	30	40	O	extensively
5	42	46	O	bound
6	48	49	O	to
7	51	56	O	plasma
8	58	64	O	protein
9	67	70	O	>99%
10	73	75	O	and
11	77	78	O	is
12	80	85	O	mainly
13	87	97	O	distributed
14	99	100	O	in
15	102	107	O	plasma
NULL

Aubagio	3174	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	13
The volume of distribution is 11 L after a single intravenous (IV) administration.
1	0	2	O	The
2	4	9	O	volume
3	11	12	O	of
4	14	25	O	distribution
5	27	28	O	is
6	30	31	O	11
7	33	33	O	L
8	35	39	O	after
9	41	41	O	a
10	43	48	O	single
11	50	60	O	intravenous
12	63	64	O	IV
13	67	80	O	administration
NULL

Aubagio	3175	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	10
Metabolism Teriflunomide is the major circulating moiety detected in plasma.
1	0	9	O	Metabolism
2	11	23	O	XXXXXXXX
3	25	26	O	is
4	28	30	O	the
5	32	36	O	major
6	38	48	O	circulating
7	50	55	O	moiety
8	57	64	O	detected
9	66	67	O	in
10	69	74	O	plasma
NULL

Aubagio	3176	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	18
The primary biotransformation pathway to minor metabolites of teriflunomide is hydrolysis, with oxidation being a minor pathway.
1	0	2	O	The
2	4	10	O	primary
3	12	28	O	biotransformation
4	30	36	O	pathway
5	38	39	O	to
6	41	45	O	minor
7	47	57	O	metabolites
8	59	60	O	of
9	62	74	O	XXXXXXXX
10	76	77	O	is
11	79	88	O	hydrolysis
12	89	89	O	,
13	91	94	O	with
14	96	104	O	oxidation
15	106	110	O	being
16	112	112	O	a
17	114	118	O	minor
18	120	126	O	pathway
NULL

Aubagio	3177	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	10
Secondary pathways involve oxidation, N-acetylation and sulfate conjugation.
1	0	8	O	Secondary
2	10	17	O	pathways
3	19	25	O	involve
4	27	35	O	oxidation
5	36	36	O	,
6	38	38	O	N
7	40	50	O	acetylation
8	52	54	O	and
9	56	62	O	sulfate
10	64	74	B-K	conjugation
K/10:C54358

Aubagio	3178	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	19
Elimination Teriflunomide is eliminated mainly through direct biliary excretion of unchanged drug as well as renal excretion of metabolites.
1	0	10	O	Elimination
2	12	24	O	XXXXXXXX
3	26	27	O	is
4	29	38	O	eliminated
5	40	45	O	mainly
6	47	53	O	through
7	55	60	O	direct
8	62	68	O	biliary
9	70	78	O	excretion
10	80	81	O	of
11	83	91	O	unchanged
12	93	96	O	drug
13	98	99	O	as
14	101	104	O	well
15	106	107	O	as
16	109	113	O	renal
17	115	123	O	excretion
18	125	126	O	of
19	128	138	O	metabolites
NULL

Aubagio	3179	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	17
Over 21 days, 60.1% of the administered dose is excreted via feces (37.5%) and urine (22.6%).
1	0	3	O	Over
2	5	6	O	21
3	8	11	O	days
4	12	12	O	,
5	14	18	O	60.1%
6	20	21	O	of
7	23	25	O	the
8	27	38	O	administered
9	40	43	O	dose
10	45	46	O	is
11	48	55	O	excreted
12	57	59	O	via
13	61	65	O	feces
14	68	72	O	37.5%
15	75	77	O	and
16	79	83	O	urine
17	86	90	O	22.6%
NULL

Aubagio	3180	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	16
After an accelerated elimination procedure with cholestyramine, an additional 23.1% was recovered (mostly in feces).
1	0	4	O	After
2	6	7	O	an
3	9	19	O	accelerated
4	21	31	O	elimination
5	33	41	O	procedure
6	43	46	O	with
7	48	61	O	cholestyramine
8	62	62	O	,
9	64	65	O	an
10	67	76	O	additional
11	78	82	O	23.1%
12	84	86	O	was
13	88	96	O	recovered
14	99	104	O	mostly
15	106	107	O	in
16	109	113	O	feces
NULL

Aubagio	3181	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	17
After a single IV administration, the total body clearance of teriflunomide is 30.5 mL/h.
1	0	4	O	After
2	6	6	O	a
3	8	13	O	single
4	15	16	O	IV
5	18	31	O	administration
6	32	32	O	,
7	34	36	O	the
8	38	42	O	total
9	44	47	O	body
10	49	57	O	clearance
11	59	60	O	of
12	62	74	O	XXXXXXXX
13	76	77	O	is
14	79	82	O	30.5
15	84	85	O	mL
16	86	86	O	/
17	87	87	O	h
NULL

Aubagio	3182	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	15
Drug Interaction Studies Teriflunomide is not metabolized by Cytochrome P450 or flavin monoamine oxidase enzymes.
1	0	3	O	Drug
2	5	15	O	Interaction
3	17	23	O	Studies
4	25	37	O	XXXXXXXX
5	39	40	O	is
6	42	44	O	not
7	46	56	O	metabolized
8	58	59	O	by
9	61	70	O	Cytochrome
10	72	75	O	P450
11	77	78	O	or
12	80	85	O	flavin
13	87	95	O	monoamine
14	97	103	O	oxidase
15	105	111	O	enzymes
NULL

Aubagio	3183	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	51
The Potential Effect of AUBAGIO on Other Drugs CYP2C8 Substrates There was an increase in mean repaglinide Cmax and AUC (1.7- and 2.4-fold, respectively), following repeated doses of teriflunomide and a single dose of 0.25 mg repaglinide, suggesting that teriflunomide is an inhibitor of CYP2C8 in vivo.
1	0	2	O	The
2	4	12	O	Potential
3	14	19	O	Effect
4	21	22	O	of
5	24	30	O	XXXXXXXX
6	32	33	O	on
7	35	39	O	Other
8	41	45	B-K	Drugs
9	47	52	B-D	CYP2C8
10	54	63	I-D	Substrates
11	65	69	O	There
12	71	73	O	was
13	75	76	O	an
14	78	85	B-T	increase
15	87	88	O	in
16	90	93	O	mean
17	95	105	B-K	repaglinide
18	107	110	O	Cmax
19	112	114	O	and
20	116	118	O	AUC
21	121	123	O	1.7
22	126	128	O	and
23	130	132	O	2.4
24	134	137	O	fold
25	138	138	O	,
26	140	151	O	respectively
27	153	153	O	,
28	155	163	O	following
29	165	172	O	repeated
30	174	178	O	doses
31	180	181	O	of
32	183	195	O	XXXXXXXX
33	197	199	O	and
34	201	201	O	a
35	203	208	O	single
36	210	213	O	dose
37	215	216	O	of
38	218	221	O	0.25
39	223	224	O	mg
40	226	236	O	repaglinide
41	237	237	O	,
42	239	248	O	suggesting
43	250	253	O	that
44	255	267	O	XXXXXXXX
45	269	270	O	is
46	272	273	O	an
47	275	283	O	inhibitor
48	285	286	O	of
49	288	293	O	CYP2C8
50	295	296	O	in
51	298	301	O	vivo
K/8:C54602 K/17:C54602

Aubagio	3184	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	12
The magnitude of interaction could be higher at the recommended repaglinide dose.
1	0	2	O	The
2	4	12	O	magnitude
3	14	15	O	of
4	17	27	B-T	interaction
5	29	33	I-T	could
6	35	36	O	be
7	38	43	B-T	higher
8	45	46	I-T	at
9	48	50	O	the
10	52	62	O	recommended
11	64	74	B-U	repaglinide
12	76	79	O	dose
NULL

Aubagio	3185	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	32
CYP1A2 Substrates Repeated doses of teriflunomide decreased mean Cmax and AUC of caffeine by 18% and 55%, respectively, suggesting that teriflunomide may be a weak inducer of CYP1A2 in vivo.
1	0	5	O	CYP1A2
2	7	16	O	Substrates
3	18	25	O	Repeated
4	27	31	O	doses
5	33	34	O	of
6	36	48	O	XXXXXXXX
7	50	58	O	decreased
8	60	63	B-T	mean
9	65	68	I-T	Cmax
10	70	72	O	and
11	74	76	O	AUC
12	78	79	O	of
13	81	88	B-K	caffeine
14	90	91	O	by
15	93	95	O	18%
16	97	99	O	and
17	101	103	O	55%
18	104	104	O	,
19	106	117	O	respectively
20	118	118	O	,
21	120	129	O	suggesting
22	131	134	O	that
23	136	148	O	XXXXXXXX
24	150	152	O	may
25	154	155	O	be
26	157	157	O	a
27	159	162	B-E	weak
28	164	170	I-E	inducer
29	172	173	I-E	of
30	175	180	I-E	CYP1A2
31	182	183	O	in
32	185	188	O	vivo
K/13:C54602

Aubagio	3186	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	39
OAT3 Substrates There was an increase in mean cefaclor Cmax and AUC (1.43- and 1.54-fold, respectively), following repeated doses of teriflunomide, suggesting that teriflunomide is an inhibitor of organic anion transporter 3 (OAT3) in vivo.
1	0	3	O	OAT3
2	5	14	O	Substrates
3	16	20	O	There
4	22	24	O	was
5	26	27	O	an
6	29	36	O	increase
7	38	39	O	in
8	41	44	O	mean
9	46	53	B-K	cefaclor
10	55	58	O	Cmax
11	60	62	O	and
12	64	66	O	AUC
13	69	72	O	1.43
14	75	77	O	and
15	79	82	O	1.54
16	84	87	O	fold
17	88	88	O	,
18	90	101	O	respectively
19	103	103	O	,
20	105	113	O	following
21	115	122	O	repeated
22	124	128	O	doses
23	130	131	O	of
24	133	145	O	XXXXXXXX
25	146	146	O	,
26	148	157	O	suggesting
27	159	162	O	that
28	164	176	O	XXXXXXXX
29	178	179	O	is
30	181	182	O	an
31	184	192	O	inhibitor
32	194	195	O	of
33	197	203	B-K	organic
34	205	209	I-K	anion
35	211	221	O	transporter
36	223	223	O	3
37	226	229	O	OAT3
38	232	233	O	in
39	235	238	O	vivo
K/9:C54602 K/33:C54602

Aubagio	3187	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	49
BCRP and OATP1B1/1B3 Substrates There was an increase in mean rosuvastatin Cmax and AUC (2.65- and 2.51-fold, respectively), following repeated doses of teriflunomide , suggesting that teriflunomide is an inhibitor of BCRP transporter and organic anion transporting polypeptide 1B1 and 1B3 (OATP1B1/1B3).
1	0	3	O	BCRP
2	5	7	O	and
3	9	15	O	OATP1B1
4	16	16	O	/
5	17	19	O	1B3
6	21	30	B-K	Substrates
7	32	36	O	There
8	38	40	O	was
9	42	43	O	an
10	45	52	B-T	increase
11	54	55	O	in
12	57	60	O	mean
13	62	73	B-K	rosuvastatin
14	75	78	O	Cmax
15	80	82	O	and
16	84	86	O	AUC
17	89	92	O	2.65
18	95	97	O	and
19	99	102	O	2.51
20	104	107	O	fold
21	108	108	O	,
22	110	121	O	respectively
23	123	123	O	,
24	125	133	O	following
25	135	142	O	repeated
26	144	148	O	doses
27	150	151	O	of
28	153	165	O	XXXXXXXX
29	167	167	O	,
30	169	178	O	suggesting
31	180	183	O	that
32	185	197	O	XXXXXXXX
33	199	200	O	is
34	202	203	O	an
35	205	213	O	inhibitor
36	215	216	O	of
37	218	221	O	BCRP
38	223	233	O	transporter
39	235	237	O	and
40	239	245	B-U	organic
41	247	251	B-K	anion
42	253	264	O	transporting
43	266	276	B-K	polypeptide
44	278	280	O	1B1
45	282	284	O	and
46	286	288	O	1B3
47	291	297	O	OATP1B1
48	298	298	O	/
49	299	301	O	1B3
K/6:C54602 K/13:C54602 K/41:C54602 K/43:C54602

Aubagio	3188	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	36
Oral Contraceptives There was an increase in mean ethinylestradiol Cmax and AUC0-24 (1.58- and 1.54-fold, respectively) and levonorgestrel Cmax and AUC0-24 (1.33- and 1.41-fold, respectively) following repeated doses of teriflunomide.
1	0	3	B-K	Oral
2	5	18	I-K	Contraceptives
3	20	24	O	There
4	26	28	O	was
5	30	31	O	an
6	33	40	B-T	increase
7	42	43	I-T	in
8	45	48	O	mean
9	50	65	B-K	ethinylestradiol
10	67	70	O	Cmax
11	72	74	O	and
12	76	79	O	AUC0
13	81	82	O	24
14	85	88	O	1.58
15	91	93	O	and
16	95	98	O	1.54
17	100	103	O	fold
18	104	104	O	,
19	106	117	O	respectively
20	120	122	O	and
21	124	137	B-K	levonorgestrel
22	139	142	O	Cmax
23	144	146	O	and
24	148	151	O	AUC0
25	153	154	O	24
26	157	160	O	1.33
27	163	165	O	and
28	167	170	O	1.41
29	172	175	O	fold
30	176	176	O	,
31	178	189	O	respectively
32	192	200	O	following
33	202	209	O	repeated
34	211	215	O	doses
35	217	218	O	of
36	220	232	O	XXXXXXXX
K/1:C54602 K/9:C54602 K/21:C54602

Aubagio	3189	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	33
Teriflunomide did not affect the pharmacokinetics of bupropion (a CYP2B6 substrate), midazolam (a CYP3A4 substrate), S-warfarin (a CYP2C9 substrate), omeprazole (a CYP2C19 substrate), and metoprolol (a CYP2D6 substrate).
1	0	12	O	XXXXXXXX
2	14	16	O	did
3	18	20	O	not
4	22	27	O	affect
5	29	31	O	the
6	33	48	O	pharmacokinetics
7	50	51	O	of
8	53	61	O	bupropion
9	64	64	O	a
10	66	71	O	CYP2B6
11	73	81	O	substrate
12	83	83	O	,
13	85	93	O	midazolam
14	96	96	O	a
15	98	103	O	CYP3A4
16	105	113	O	substrate
17	115	115	O	,
18	117	117	O	S
19	119	126	O	warfarin
20	129	129	O	a
21	131	136	O	CYP2C9
22	138	146	O	substrate
23	148	148	O	,
24	150	159	O	omeprazole
25	162	162	B-U	a
26	164	170	O	CYP2C19
27	172	180	B-U	substrate
28	182	182	O	,
29	184	186	O	and
30	188	197	O	metoprolol
31	200	200	O	a
32	202	207	O	CYP2D6
33	209	217	B-U	substrate
NULL

Aubagio	3190	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	22
The Potential Effect of Other Drugs on AUBAGIO Potent CYP and transporter inducers: Rifampin did not affect the pharmacokinetics of teriflunomide.
1	0	2	O	The
2	4	12	O	Potential
3	14	19	O	Effect
4	21	22	O	of
5	24	28	O	Other
6	30	34	O	Drugs
7	36	37	O	on
8	39	45	O	XXXXXXXX
9	47	52	O	Potent
10	54	56	O	CYP
11	58	60	O	and
12	62	72	O	transporter
13	74	81	O	inducers
14	82	82	O	:
15	84	91	O	Rifampin
16	93	95	O	did
17	97	99	O	not
18	101	106	B-T	affect
19	108	110	I-T	the
20	112	127	I-T	pharmacokinetics
21	129	130	O	of
22	132	144	O	XXXXXXXX
NULL

Aubagio	3191	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	17
Specific populations Hepatic Impairment Mild and moderate hepatic impairment had no impact on the pharmacokinetics of teriflunomide.
1	0	7	O	Specific
2	9	19	O	populations
3	21	27	O	Hepatic
4	29	38	O	Impairment
5	40	43	O	Mild
6	45	47	O	and
7	49	56	O	moderate
8	58	64	O	hepatic
9	66	75	O	impairment
10	77	79	O	had
11	81	82	O	no
12	84	89	O	impact
13	91	92	O	on
14	94	96	O	the
15	98	113	O	pharmacokinetics
16	115	116	O	of
17	118	130	O	XXXXXXXX
NULL

Aubagio	3192	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	12
The pharmacokinetics of teriflunomide in severe hepatic impairment have not been evaluated.
1	0	2	O	The
2	4	19	O	pharmacokinetics
3	21	22	O	of
4	24	36	O	XXXXXXXX
5	38	39	O	in
6	41	46	O	severe
7	48	54	O	hepatic
8	56	65	O	impairment
9	67	70	O	have
10	72	74	O	not
11	76	79	O	been
12	81	89	O	evaluated
NULL

Aubagio	3193	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	13
Renal Impairment Severe renal impairment had no impact on the pharmacokinetics of teriflunomide.
1	0	4	O	Renal
2	6	15	O	Impairment
3	17	22	O	Severe
4	24	28	O	renal
5	30	39	O	impairment
6	41	43	O	had
7	45	46	O	no
8	48	53	O	impact
9	55	56	O	on
10	58	60	O	the
11	62	77	O	pharmacokinetics
12	79	80	O	of
13	82	94	O	XXXXXXXX
NULL

Aubagio	3194	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	19
Gender In a population analysis, the clearance rate for teriflunomide is 23% less in females than in males.
1	0	5	O	Gender
2	7	8	O	In
3	10	10	O	a
4	12	21	O	population
5	23	30	O	analysis
6	31	31	O	,
7	33	35	O	the
8	37	45	O	clearance
9	47	50	O	rate
10	52	54	O	for
11	56	68	O	XXXXXXXX
12	70	71	O	is
13	73	75	O	23%
14	77	80	O	less
15	82	83	O	in
16	85	91	O	females
17	93	96	O	than
18	98	99	O	in
19	101	105	O	males
NULL

Aubagio	3195	34090-1	4650d12c-b9c8-4525-b07f-a2d773eca155	26
Race Effect of race on the pharmacokinetics of teriflunomide cannot be adequately assessed due to a low number of non-white patients in the clinical trials.
1	0	3	O	Race
2	5	10	O	Effect
3	12	13	O	of
4	15	18	O	race
5	20	21	O	on
6	23	25	O	the
7	27	42	O	pharmacokinetics
8	44	45	O	of
9	47	59	O	XXXXXXXX
10	61	66	O	cannot
11	68	69	O	be
12	71	80	O	adequately
13	82	89	O	assessed
14	91	93	O	due
15	95	96	O	to
16	98	98	O	a
17	100	102	O	low
18	104	109	O	number
19	111	112	O	of
20	114	116	O	non
21	118	122	O	white
22	124	131	O	patients
23	133	134	O	in
24	136	138	O	the
25	140	147	O	clinical
26	149	154	O	trials
NULL

