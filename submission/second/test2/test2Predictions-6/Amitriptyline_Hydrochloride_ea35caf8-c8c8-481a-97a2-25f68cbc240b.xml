<?xml version="1.0" ?>
<Label drug="Amitriptyline Hydrochloride" setid="ea35caf8-c8c8-481a-97a2-25f68cbc240b">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Amitriptyline hydrochloride is an antidepressant with sedative effects. Its mechanism of action in man is not known. It is not a monoamine oxidase inhibitor and it does not act primarily by stimulation of the central nervous system.  Amitriptyline inhibits the membrane pump mechanism responsible for uptake of norepinephrine and serotonin in adrenergic and serotonergic neurons. Pharmacologically this action may potentiate or prolong neuronal activity since reuptake of these biogenic amines is important physiologically in terminating transmitting activity. This interference with reuptake of norepinephrine and/or serotonin is believed by some to underlie the antidepressant activity of amitriptyline.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1181" section="34073-7">
      

      <SentenceText>Some patients may experience a large increase in amitriptyline concentration in the presence of topiramate and any adjustments in amitriptyline dose should be made according to the patient’s clinical response and not on the basis of plasma levels.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1182" section="34073-7">
      

      <SentenceText>The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the caucasian population (about 7% to 10% of Caucasians are so called “poor metabolizers”); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African, and other populations are not yet available.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1183" section="34073-7">
      

      <SentenceText>Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="23 6" str="higher" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="30 4" str="than" type="Trigger"/>
      <Mention code="NO MAP" id="M3" span="35 8" str="expected" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M4" span="44 21" str="plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M5" span="69 31" str="tricyclic antidepressants (TCAs" type="Precipitant"/>
      <Interaction effect="C54358" id="I1" precipitant="M5" trigger="M4" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1184" section="34073-7">
      

      <SentenceText>Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8-fold increase in plasma AUC of the TCA).</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="133 22" str="increase in plasma AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M7" span="54 3" str="2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I2" precipitant="M7" trigger="M6" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1185" section="34073-7">
      

      <SentenceText>In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="21 34" str="drugs inhibit the activity of this" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1186" section="34073-7">
      

      <SentenceText>An individual who is stable on a given dose of TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1187" section="34073-7">
      

      <SentenceText>The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="51 29" str="some that are not metabolized" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="4 38" str="drugs that inhibit cytochrome P450 2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I3" precipitant="M10" trigger="M9" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M11" span="96 9" str="quinidine" type="Precipitant"/>
      <Interaction effect="C54357" id="I4" precipitant="M11" trigger="M9" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1188" section="34073-7">
      

      <SentenceText>While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1189" section="34073-7">
      

      <SentenceText>The extent to which SSRI-TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="60 8" str="problems" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1190" section="34073-7">
      

      <SentenceText>Nevertheless, caution is indicated in the coadministration of TCAs with any of the SSRIs and also in switching from one class to the other.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="14 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M14" span="25 9" str="indicated" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1191" section="34073-7">
      

      <SentenceText>Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1192" section="34073-7">
      

      <SentenceText>Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="105 16" str="lower doses than" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M16" span="19 73" str="tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6" type="Precipitant"/>
      <Interaction effect="M15" id="I5" precipitant="M16" trigger="M16" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M17" span="166 14" str="antidepressant" type="Precipitant"/>
      <Interaction effect="M15" id="I6" precipitant="M17" trigger="M17" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1193" section="34073-7">
      

      <SentenceText>Furthermore, whenever one of these other drugs is withdrawn from cotherapy, an increased dose of tricyclic antidepressant may be required.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1194" section="34073-7">
      

      <SentenceText>It is desirable to monitor TCA plasma levels whenever a TCA is going to be coadministered with another drug known to be an inhibitor of P450 2D6.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1195" section="34073-7">
      

      <SentenceText>Guanethidine or similarly acting compounds; thyroid medication; alcohol, barbiturates and other CNS depressants; and disulfiram.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1196" section="34073-7">
      

      <SentenceText>When amitriptyline hydrochloride is given with anticholinergic agents or sympathomimetic drugs, including epinephrine combined with local anesthetics, close supervision and careful adjustment of dosages are required.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="157 11" str="supervision" type="Trigger"/>
      <Mention code="NO MAP" id="M19" span="181 21" str="adjustment of dosages" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="47 22" str="anticholinergic agents" type="Precipitant"/>
      <Interaction id="I7" precipitant="M20" trigger="M19" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M21" span="73 21" str="sympathomimetic drugs" type="Precipitant"/>
      <Interaction id="I8" precipitant="M21" trigger="M18" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M22" span="106 11" str="epinephrine" type="Precipitant"/>
      <Interaction id="I9" precipitant="M22" trigger="M18" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M23" span="132 17" str="local anesthetics" type="Precipitant"/>
      <Interaction id="I10" precipitant="M23" trigger="M18" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1197" section="34073-7">
      

      <SentenceText>Hyperpyrexia has been reported when amitriptyline hydrochloride is administered with anticholinergic agents or with neuroleptic drugs, particularly during hot weather.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="0 12" str="Hyperpyrexia" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M25" span="85 22" str="anticholinergic agents" type="Precipitant"/>
      <Interaction effect="M24" id="I11" precipitant="M25" trigger="M25" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M26" span="116 17" str="neuroleptic drugs" type="Precipitant"/>
      <Interaction effect="M24" id="I12" precipitant="M26" trigger="M26" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1198" section="34073-7">
      

      <SentenceText>Paralytic ileus may occur in patients taking tricyclic antidepressants in combination with anticholinergic type drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="45 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Mention code="NO MAP" id="M28" span="91 26" str="anticholinergic type drugs" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1199" section="34073-7">
      

      <SentenceText>Cimetidine is reported to reduce hepatic metabolism of certain tricyclic antidepressants, thereby delaying elimination and increasing steady-state concentrations of these drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="134 27" str="steady-state concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M30" span="0 10" str="Cimetidine" type="Precipitant"/>
      <Interaction effect="C54357" id="I13" precipitant="M30" trigger="M29" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M31" span="63 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="C54357" id="I14" precipitant="M31" trigger="M29" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1200" section="34073-7">
      

      <SentenceText>Clinically significant effects have been reported with the tricyclic antidepressants when used concomitantly with cimetidine.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="23 7" str="effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M33" span="59 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="M32" id="I15" precipitant="M33" trigger="M33" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M34" span="114 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="M32" id="I16" precipitant="M34" trigger="M34" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1201" section="34073-7">
      

      <SentenceText>Increases in plasma levels of tricyclic antidepressants, and in the frequency and severity of side effects, particularly anticholinergic, have been reported when cimetidine was added to the drug regimen.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="0 19" str="Increases in plasma" type="Trigger"/>
      <Mention code="NO MAP" id="M36" span="30 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Mention code="NO MAP" id="M37" span="162 10" str="cimetidine" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1202" section="34073-7">
      

      <SentenceText>Discontinuation of cimetidine in well controlled patients receiving tricyclic antidepressants and cimetidine may decrease the plasma levels and efficacy of the antidepressants.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="113 26" str="decrease the plasma levels" type="Trigger"/>
      <Mention code="NO MAP" id="M39" span="68 9" str="tricyclic" type="Precipitant"/>
      <Interaction effect="C54357" id="I17" precipitant="M39" trigger="M38" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M40" span="98 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="C54357" id="I18" precipitant="M40" trigger="M38" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1203" section="34073-7">
      

      <SentenceText>Caution is advised if patients receive large doses of ethchlorvynol concurrently.</SentenceText>
      

      <Mention code="NO MAP" id="M41" span="0 7" str="Caution" type="Trigger"/>
      <Mention code="NO MAP" id="M42" span="54 13" str="ethchlorvynol" type="Precipitant"/>
      <Interaction id="I19" precipitant="M42" trigger="M41" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1204" section="34073-7">
      

      <SentenceText>Transient delirium has been reported in patients who were treated with one gram of ethchlorvynol and 75 mg to 150 mg of amitriptyline hydrochloride.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1205" section="34090-1">
      

      <SentenceText>Amitriptyline hydrochloride is an antidepressant with sedative effects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1206" section="34090-1">
      

      <SentenceText>Its mechanism of action in man is not known.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1207" section="34090-1">
      

      <SentenceText>It is not a monoamine oxidase inhibitor and it does not act primarily by stimulation of the central nervous system.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1208" section="34090-1">
      

      <SentenceText>Amitriptyline inhibits the membrane pump mechanism responsible for uptake of norepinephrine and serotonin in adrenergic and serotonergic neurons.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1209" section="34090-1">
      

      <SentenceText>Pharmacologically this action may potentiate or prolong neuronal activity since reuptake of these biogenic amines is important physiologically in terminating transmitting activity.</SentenceText>
      

      <Mention code="NO MAP" id="M43" span="171 8" str="activity" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Amitriptyline Hydrochloride" id="1210" section="34090-1">
      

      <SentenceText>This interference with reuptake of norepinephrine and/or serotonin is believed by some to underlie the antidepressant activity of amitriptyline.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

