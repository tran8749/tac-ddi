<?xml version="1.0" ?>
<Label drug="Pimozide" setid="70b079e2-a1f7-4a93-8685-d60a4d7c1280">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Pharmacodynamic Actions  Pimozide tablets, USP is an orally active antipsychotic drug product which shares with other antipsychotics the ability to blockade dopaminergic receptors on neurons in the central nervous system. Although its exact mode of action has not been established, the ability of pimozide to suppress motor and phonic tics in Tourette’s Disorder is thought to be a function of its dopaminergic blocking activity. However, receptor blockade is often accompanied by a series of secondary alterations in central dopamine metabolism and function which may contribute to both pimozide’s therapeutic and untoward effects. In addition, pimozide tablets, USP in common with other antipsychotic drugs, has various effects on other central nervous system receptor systems which are not fully characterized.  Metabolism and Pharmacokinetics  More than 50% of a dose of pimozide is absorbed after oral administration. Based on the pharmacokinetic and metabolic profile, pimozide appears to undergo significant first pass metabolism. Peak serum levels occur generally six to eight hours (range 4 to 12 hours) after dosing.  Pimozide is extensively metabolized, primarily by N-dealkylation in the liver. This metabolism is catalyzed mainly by the cytochrome P450 3A4 (CYP 3A4) enzymatic system and to a lesser extent, by cytochrome P450 1A2 (CYP 1A2) and cytochrome P450 2D6 (CYP 2D6). Two major metabolites have been identified, 1-(4-piperidyl)-2-benzimidazolinone and 4,4-bis(4-fluorophenyl) butyric acid. The antipsychotic activity of these metabolites is undetermined. The major route of elimination of pimozide and its metabolites is through the kidney.  The mean serum elimination half-life of pimozide in schizophrenic patients was approximately 55 hours. There was a 13-fold interindividual difference in the area under the serum pimozide level-time curve and an equivalent degree of variation in peak serum levels among patients studied. The significance of this is unclear since there are few correlations between plasma levels and clinical findings.  Effects of food and disease upon the absorption, distribution, metabolism and elimination of pimozide are not known. Effects of concomitant medication and genetic variations on pimozide metabolism are described in the CONTRAINDICATIONS and PRECAUTIONS sections.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Pimozide" id="2723" section="34073-7">
      

      <SentenceText>Because pimozide prolongs the QT interval of the electrocardiogram, an additive effect on QT interval would be anticipated if administered with other drugs, such as phenothiazines, tricyclic antidepressants or antiarrhythmic agents, which prolong the QT interval.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="17 24" str="prolongs the QT interval" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="90 2" str="QT" type="Precipitant"/>
      <Interaction effect="M1" id="I1" precipitant="M2" trigger="M2" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M3" span="165 14" str="phenothiazines" type="Precipitant"/>
      <Interaction effect="M1" id="I2" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M4" span="181 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="M1" id="I3" precipitant="M4" trigger="M4" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M5" span="210 21" str="antiarrhythmic agents" type="Precipitant"/>
      <Interaction effect="M1" id="I4" precipitant="M5" trigger="M5" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M6" span="239 23" str="prolong the QT interval" type="Precipitant"/>
      <Interaction effect="M1" id="I5" precipitant="M6" trigger="M6" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2724" section="34073-7">
      

      <SentenceText>Accordingly, pimozide should not be given with dofetilide, sotalol, quinidine, other Class Ia and III anti-arrhythmics, mesoridazine, thioridazine, chlorpromazine, droperidol, sparfloxacin, gatifloxacin, moxifloxacin, halofantrine, mefloquine, pentamidine, arsenic trioxide, levomethadyl acetate, dolasetron mesylate, probucol, tacrolimus, ziprasidone, or other drugs that have demonstrated QT prolongation as one of their pharmacodynamic effects.</SentenceText>
      

      <Mention code="NO MAP" id="M7" span="391 15" str="QT prolongation" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M8" span="423 23" str="pharmacodynamic effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M9" span="164 10" str="droperidol" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I6" precipitant="M9" trigger="M9" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M10" span="176 12" str="sparfloxacin" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I7" precipitant="M10" trigger="M10" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M11" span="190 12" str="gatifloxacin" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I8" precipitant="M11" trigger="M11" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M12" span="204 12" str="moxifloxacin" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I9" precipitant="M12" trigger="M12" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M13" span="218 12" str="halofantrine" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I10" precipitant="M13" trigger="M13" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M14" span="232 10" str="mefloquine" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I11" precipitant="M14" trigger="M14" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M15" span="244 11" str="pentamidine" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I12" precipitant="M15" trigger="M15" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M16" span="265 8" str="trioxide" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I13" precipitant="M16" trigger="M16" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M17" span="275 20" str="levomethadyl acetate" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I14" precipitant="M17" trigger="M17" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M18" span="297 19" str="dolasetron mesylate" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I15" precipitant="M18" trigger="M18" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M19" span="318 8" str="probucol" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I16" precipitant="M19" trigger="M19" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M20" span="328 10" str="tacrolimus" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I17" precipitant="M20" trigger="M20" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M21" span="340 11" str="ziprasidone" type="Precipitant"/>
      <Interaction effect="M7;M8" id="I18" precipitant="M21" trigger="M21" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2725" section="34073-7">
      

      <SentenceText>Also, the use of macrolide antibiotics in patients with prolonged QT intervals has been rarely associated with ventricular arrhythmias.</SentenceText>
      

      <Mention code="NO MAP" id="M22" span="66 2" str="QT" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M23" span="17 21" str="macrolide antibiotics" type="Precipitant"/>
      <Interaction effect="M22" id="I19" precipitant="M23" trigger="M23" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2726" section="34073-7">
      

      <SentenceText>Such concomitant administration should not be undertaken.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2727" section="34073-7">
      

      <SentenceText>Since pimozide is partly metabolized via CYP 3A4, it should not be administered concomitantly with inhibitors of this metabolic system, such as azole antifungal agents and protease inhibitor drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="53 26" str="should not be administered" type="Trigger"/>
      <Mention code="NO MAP" id="M25" span="99 35" str="inhibitors of this metabolic system" type="Precipitant"/>
      <Mention code="NO MAP" id="M26" span="172 24" str="protease inhibitor drugs" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2728" section="34073-7">
      

      <SentenceText>Pimozide and Celexa: In a controlled study, a single dose of pimozide 2 mg coadministered with racemic citalopram 40 mg given once daily for 11 days was associated with a mean increase in QTc values of approximately 10 msec compared to pimozide given alone.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="95 18" str="racemic citalopram" type="Precipitant"/>
      <Interaction effect="C54605" id="I20" precipitant="M27" trigger="M27" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2729" section="34073-7">
      

      <SentenceText>Racemic citalopram did not alter the mean AUC or Cmax of pimozide.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="0 7" str="Racemic" type="Precipitant"/>
      <Interaction effect="C54610" id="I21" precipitant="M28" trigger="M28" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54613" id="I22" precipitant="M28" trigger="M28" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M29" span="8 10" str="citalopram" type="Precipitant"/>
      <Interaction effect="C54610" id="I23" precipitant="M29" trigger="M29" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54613" id="I24" precipitant="M29" trigger="M29" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2730" section="34073-7">
      

      <SentenceText>The mechanism of this pharmacodynamic interaction is not known.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2731" section="34073-7">
      

      <SentenceText>Concomitant use of Pimozide and Celexa or Lexapro is contraindicated.</SentenceText>
      

      <Mention code="NO MAP" id="M30" span="53 15" str="contraindicated" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2732" section="34073-7">
      

      <SentenceText>CYP 2D6 inhibitors: In healthy subjects, co-administration of pimozide 2 mg (single dose) and paroxetine 60 mg resulted in a 151% increase in pimozide AUC and a 62% increase in pimozide Cmax compared to pimozide administered alone.</SentenceText>
      

      <Mention code="NO MAP" id="M31" span="0 18" str="CYP 2D6 inhibitors" type="Precipitant"/>
      <Interaction effect="C54355" id="I25" precipitant="M31" trigger="M31" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2733" section="34073-7">
      

      <SentenceText>The increase in pimozide AUC and Cmax is related to the CYP 2D6 inhibitory properties of paroxetine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2734" section="34073-7">
      

      <SentenceText>Concomitant use of pimozide and paroxetine or other strong CYP 2D6 inhibitors are contraindicated.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="82 15" str="contraindicated" type="Trigger"/>
      <Mention code="NO MAP" id="M33" span="59 18" str="CYP 2D6 inhibitors" type="Precipitant"/>
      <Interaction id="I26" precipitant="M33" trigger="M32" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2735" section="34073-7">
      

      <SentenceText>As CYP 1A2 may also contribute to the metabolism of pimozide, prescribers should be aware of the theoretical potential for drug interactions with inhibitors of this enzymatic system.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2736" section="34073-7">
      

      <SentenceText>Pimozide may be capable of potentiating CNS depressants, including analgesics, sedatives, anxiolytics, and alcohol.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="40 3" str="CNS" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M35" span="44 11" str="depressants" type="Precipitant"/>
      <Interaction effect="M34" id="I27" precipitant="M35" trigger="M35" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M36" span="107 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="M34" id="I28" precipitant="M36" trigger="M36" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2737" section="34073-7">
      

      <SentenceText>Rare case reports have suggested possible additive effects of pimozide and fluoxetine leading to bradycardia.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="42 16" str="additive effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M38" span="97 11" str="bradycardia" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M39" span="75 10" str="fluoxetine" type="Precipitant"/>
      <Interaction effect="M37;M38" id="I29" precipitant="M39" trigger="M39" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2738" section="34073-7">
      

      <SentenceText>Concomitant administration of pimozide and sertraline should be contraindicated.</SentenceText>
      

      <Mention code="NO MAP" id="M40" span="64 15" str="contraindicated" type="Trigger"/>
      <Mention code="NO MAP" id="M41" span="43 10" str="sertraline" type="Precipitant"/>
      <Interaction id="I30" precipitant="M41" trigger="M40" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2739" section="34073-7">
      

      <SentenceText>Pharmacogenomics Individuals with genetic variations resulting in poor CYP 2D6 metabolism (approximately 5 to 10% of the population) exhibit higher pimozide concentrations than extensive CYP 2D6 metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M42" span="79 10" str="metabolism" type="Trigger"/>
      <Mention code="NO MAP" id="M43" span="141 6;157 14" str="higher | concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M44" span="71 3" str="CYP" type="Precipitant"/>
      <Interaction effect="C54355" id="I31" precipitant="M44" trigger="M43" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M45" span="187 3" str="CYP" type="Precipitant"/>
      <Interaction effect="C54355" id="I32" precipitant="M45" trigger="M42" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2740" section="34073-7">
      

      <SentenceText>The concentrations observed in poor CYP 2D6 metabolizers are similar to those seen with strong CYP 2D6 inhibitors such as paroxetine.</SentenceText>
      

      <Mention code="NO MAP" id="M46" span="95 18" str="CYP 2D6 inhibitors" type="Precipitant"/>
      <Interaction id="I33" precipitant="M46" trigger="M46" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2741" section="34073-7">
      

      <SentenceText>The time to achieve steady state Pimozide concentrations is expected to be longer (approximately 2 weeks) in poor CYP 2D6 metabolizers because of the prolonged half-life.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2742" section="34073-7">
      

      <SentenceText>Alternative dosing strategies are recommended in patients who are genetically poor CYP 2D6 metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M47" span="19 10" str="strategies" type="Trigger"/>
      <Mention code="NO MAP" id="M48" span="34 11" str="recommended" type="Trigger"/>
      <Mention code="NO MAP" id="M49" span="83 7" str="CYP 2D6" type="Precipitant"/>
      <Interaction id="I34" precipitant="M49" trigger="M48" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2743" section="34073-7">
      

      <SentenceText>Interaction with Food Patients should avoid grapefruit juice because it may inhibit the metabolism of pimozide by CYP 3A4.</SentenceText>
      

      <Mention code="NO MAP" id="M50" span="76 22" str="inhibit the metabolism" type="Trigger"/>
      <Mention code="NO MAP" id="M51" span="44 16" str="grapefruit juice" type="Precipitant"/>
      <Interaction effect="C54355" id="I35" precipitant="M51" trigger="M50" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M52" span="114 7" str="CYP 3A4" type="Precipitant"/>
      <Interaction effect="C54355" id="I36" precipitant="M52" trigger="M50" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2744" section="34090-1">
      

      <SentenceText>Pharmacodynamic Actions Pimozide tablets, USP is an orally active antipsychotic drug product which shares with other antipsychotics the ability to blockade dopaminergic receptors on neurons in the central nervous system.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2745" section="34090-1">
      

      <SentenceText>Although its exact mode of action has not been established, the ability of pimozide to suppress motor and phonic tics in Tourette’s Disorder is thought to be a function of its dopaminergic blocking activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2746" section="34090-1">
      

      <SentenceText>However, receptor blockade is often accompanied by a series of secondary alterations in central dopamine metabolism and function which may contribute to both pimozide’s therapeutic and untoward effects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2747" section="34090-1">
      

      <SentenceText>In addition, pimozide tablets, USP in common with other antipsychotic drugs, has various effects on other central nervous system receptor systems which are not fully characterized.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2748" section="34090-1">
      

      <SentenceText>Metabolism and Pharmacokinetics More than 50% of a dose of pimozide is absorbed after oral administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2749" section="34090-1">
      

      <SentenceText>Based on the pharmacokinetic and metabolic profile, pimozide appears to undergo significant first pass metabolism.</SentenceText>
      

      <Mention code="NO MAP" id="M53" span="98 15" str="pass metabolism" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2750" section="34090-1">
      

      <SentenceText>Peak serum levels occur generally six to eight hours (range 4 to 12 hours) after dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2751" section="34090-1">
      

      <SentenceText>Pimozide is extensively metabolized, primarily by N-dealkylation in the liver.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2752" section="34090-1">
      

      <SentenceText>This metabolism is catalyzed mainly by the cytochrome P450 3A4 (CYP 3A4) enzymatic system and to a lesser extent, by cytochrome P450 1A2 (CYP 1A2) and cytochrome P450 2D6 (CYP 2D6).</SentenceText>
      

      <Mention code="NO MAP" id="M54" span="162 17" str="P450 2D6 (CYP 2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I37" precipitant="M54" trigger="M54" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2753" section="34090-1">
      

      <SentenceText>Two major metabolites have been identified, 1-(4-piperidyl)-2-benzimidazolinone and 4,4-bis(4-fluorophenyl) butyric acid.</SentenceText>
      

      <Mention code="NO MAP" id="M55" span="94 26" str="fluorophenyl) butyric acid" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2754" section="34090-1">
      

      <SentenceText>The antipsychotic activity of these metabolites is undetermined.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2755" section="34090-1">
      

      <SentenceText>The major route of elimination of pimozide and its metabolites is through the kidney.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2756" section="34090-1">
      

      <SentenceText>The mean serum elimination half-life of pimozide in schizophrenic patients was approximately 55 hours.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2757" section="34090-1">
      

      <SentenceText>There was a 13-fold interindividual difference in the area under the serum pimozide level-time curve and an equivalent degree of variation in peak serum levels among patients studied.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2758" section="34090-1">
      

      <SentenceText>The significance of this is unclear since there are few correlations between plasma levels and clinical findings.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2759" section="34090-1">
      

      <SentenceText>Effects of food and disease upon the absorption, distribution, metabolism and elimination of pimozide are not known.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Pimozide" id="2760" section="34090-1">
      

      <SentenceText>Effects of concomitant medication and genetic variations on pimozide metabolism are described in the CONTRAINDICATIONS and PRECAUTIONS sections.</SentenceText>
      

      <Mention code="NO MAP" id="M56" span="69 14" str="metabolism are" type="Trigger"/>
    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

