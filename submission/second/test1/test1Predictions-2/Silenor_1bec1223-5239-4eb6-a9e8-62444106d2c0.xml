<?xml version="1.0" ?>
<Label drug="Silenor" setid="1bec1223-5239-4eb6-a9e8-62444106d2c0">
  <Text>
    <Section id="34068-7" name="DOSAGE &amp; ADMINISTRATION SECTION">
2. DOSAGE AND ADMINISTRATION  The dose of Silenor should be individualized.  Initial dose: 6 mg, once daily for adults ( 2.1 ) and 3 mg, once daily for the elderly. ( 2.1 , 2.2 )  Take within 30 minutes of bedtime. Total daily dose should not exceed 6 mg. ( 2.3 )  Should not be taken within 3 hours of a meal. ( 2.3 , 12.3 )  2.1. Dosing in Adults  The recommended dose of Silenor for adults is 6 mg once daily. A 3 mg once daily dose may be appropriate for some patients, if clinically indicated.  2.2. Dosing in the Elderly  The recommended starting dose of Silenor in elderly patients (≥ 65 years old) is 3 mg once daily. The daily dose can be increased to 6 mg, if clinically indicated.  2.3. Administration  Silenor should be taken within 30 minutes of bedtime.  To minimize the potential for next day effects, Silenor should not be taken within 3 hours of a meal [ see Clinical Pharmacology (12.3)  ] .  The total Silenor dose should not exceed 6 mg per day.</Section>
    

    <Section id="34070-3" name="CONTRAINDICATIONS SECTION">
4. CONTRAINDICATIONS  Hypersensitivity to doxepin hydrochloride, inactive ingredients, or other dibenzoxepines. ( 4.1 )  Co-administration with Monoamine Oxidase Inhibitors (MAOIs): Do not administer if patient is taking MAOIs or has used MAOIs within the past two weeks. ( 4.2 )  Untreated narrow angle glaucoma or severe urinary retention. ( 4.3 )  4.1. Hypersensitivity  Silenor is contraindicated in individuals who have shown hypersensitivity to doxepin HCl, any of its inactive ingredients, or other dibenzoxepines.  4.2. Co-administration with Monoamine Oxidase Inhibitors (MAOIs)  Serious side effects and even death have been reported following the concomitant use of certain drugs with MAO inhibitors. Do not administer Silenor if patient is currently on MAOIs or has used MAOIs within the past two weeks. The exact length of time may vary depending on the particular MAOI dosage and duration of treatment.  4.3. Glaucoma and Urinary Retention  Silenor is contraindicated in individuals with untreated narrow angle glaucoma or severe urinary retention.</Section>
    

    <Section id="43685-7" name="WARNINGS AND PRECAUTIONS SECTION">
5. WARNINGS AND PRECAUTIONS  Need to Evaluate for Co-morbid Diagnoses: Reevaluate if insomnia persists after 7 to 10 days of use. ( 5.1 )  Abnormal thinking, behavioral changes, complex behaviors: May include &quot;Sleep-driving&quot; and hallucinations. Immediately evaluate any new onset behavioral changes. ( 5.2 )  Depression: Worsening of depression or suicidal thinking may occur. Prescribe the least amount feasible to avoid intentional overdose. ( 5.3 )  CNS-depressant effects: Use can impair alertness and motor coordination. Avoid engaging in hazardous activities such as operating a motor vehicle or heavy machinery after taking drug. ( 5.4 ) Do not use with alcohol. ( 5.4 , 7.3 )  Potential additive effects when used in combination with CNS depressants or sedating antihistamines. Dose reduction may be needed. ( 5.4 , 7.4 )  Patients with severe sleep apnea: Silenor is ordinarily not recommended for use in this population. ( 8.7 )  5.1. Need to Evaluate for Comorbid Diagnoses  Because sleep disturbances may be the presenting manifestation of a physical and/or psychiatric disorder, symptomatic treatment of insomnia should be initiated only after careful evaluation of the patient. The failure of insomnia to remit after 7 to 10 days of treatment may indicate the presence of a primary psychiatric and/or medical illness that should be evaluated. Exacerbation of insomnia or the emergence of new cognitive or behavioral abnormalities may be the consequence of an unrecognized psychiatric or physical disorder. Such findings have emerged during the course of treatment with hypnotic drugs.  5.2. Abnormal Thinking and Behavioral Changes  Complex behaviors such as &quot;sleep-driving&quot; (i.e., driving while not fully awake after ingestion of a hypnotic, with amnesia for the event) have been reported with hypnotics. These events can occur in hypnotic-naive as well as in hypnotic-experienced persons. Although behaviors such as &quot;sleep-driving&quot; may occur with hypnotics alone at therapeutic doses, the use of alcohol and other CNS depressants with hypnotics appears to increase the risk of such behaviors, as does the use of hypnotics at doses exceeding the maximum recommended dose. Due to the risk to the patient and the community, discontinuation of Silenor should be strongly considered for patients who report a &quot;sleep-driving&quot; episode. Other complex behaviors (e.g., preparing and eating food, making phone calls, or having sex) have been reported in patients who are not fully awake after taking a hypnotic. As with &quot;sleep-driving&quot;, patients usually do not remember these events. Amnesia, anxiety and other neuro-psychiatric symptoms may occur unpredictably.  5.3. Suicide Risk and Worsening of Depression  In primarily depressed patients, worsening of depression, including suicidal thoughts and actions (including completed suicides), has been reported in association with the use of hypnotics.  Doxepin, the active ingredient in Silenor, is an antidepressant at doses 10- to 100-fold higher than in Silenor. Antidepressants increased the risk compared to placebo of suicidal thinking and behavior (suicidality) in children, adolescents, and young adults in short-term studies of major depressive disorder (MDD) and other psychiatric disorders. Risk from the lower dose of doxepin in Silenor can not be excluded.  It can rarely be determined with certainty whether a particular instance of the abnormal behaviors listed above is drug induced, spontaneous in origin, or a result of an underlying psychiatric or physical disorder. Nonetheless, the emergence of any new behavioral sign or symptom of concern requires careful and immediate evaluation.  5.4. CNS Depressant Effects  After taking Silenor, patients should confine their activities to those necessary to prepare for bed. Patients should avoid engaging in hazardous activities, such as operating a motor vehicle or heavy machinery, at night after taking Silenor, and should be cautioned about potential impairment in the performance of such activities that may occur the day following ingestion.  When taken with Silenor, the sedative effects of alcoholic beverages, sedating antihistamines, and other CNS depressants may be potentiated [s ee Warnings and Precautions (5.2) and Drug Interactions (7.3 , 7.4)  ]. Patients should not consume alcohol with Silenor [s ee Warnings and Precautions (5.2) and Drug Interactions (7.3)  ]. Patients should be cautioned about potential additive effects of Silenor used in combination with CNS depressants or sedating antihistamines [s ee Warnings and Precautions (5.2) and Drug Interactions (7.4)  ].</Section>
    

    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7. DRUG INTERACTIONS  MAO inhibitors: Silenor should not be administered in patients on MAOIs within the past two weeks. ( 4.2 )  Cimetidine: Increases exposure to doxepin. ( 7.2 )  Alcohol: Sedative effects may be increased with doxepin. ( 7.3 , 5.4 )  CNS Depressants and Sedating Antihistamines: Sedative effects may be increased with doxepin. ( 7.4 , 5.4 )  Tolazamide: A case of severe hypoglycemia has been reported. ( 7.5 )  7.1. Cytochrome P450 Isozymes  Silenor is primarily metabolized by hepatic cytochrome P450 isozymes CYP2C19 and CYP2D6, and to a lesser extent, by CYP1A2 and CYP2C9. Inhibitors of these isozymes may increase the exposure of doxepin. Silenor is not an inhibitor of any CYP isozymes at therapeutically relevant concentrations. The ability of Silenor to induce CYP isozymes is not known.  7.2. Cimetidine  Silenor exposure is doubled with concomitant administration of cimetidine, a nonspecific inhibitor of CYP isozymes. A maximum dose of 3 mg is recommended in adults and elderly when cimetidine is co-administered with Silenor [see Clinical Pharmacology (12.4) ]  7.3. Alcohol  When taken with Silenor, the sedative effects of alcohol may be potentiated [ see Warnings and Precautions (5.2 , 5.4)  ] .  7.4. CNS Depressants and Sedating Antihistamines  When taken with Silenor, the sedative effects of sedating antihistamines and CNS depressants may be potentiated [ see Warnings and Precautions (5.2 , 5.4)  ] .  7.5. Tolazamide  A case of severe hypoglycemia has been reported in a type II diabetic patient maintained on tolazamide (1 g/day) 11 days after the addition of oral doxepin (75 mg/day).</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Silenor" id="1687" section="34068-7">
      

      <SentenceText>A 3 mg once daily dose may be appropriate for some patients, if clinically indicated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1688" section="34068-7">
      

      <SentenceText>Initial dose: 6 mg, once daily for adults (2.1) and 3 mg, once daily for the elderly.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1689" section="34068-7">
      

      <SentenceText>Silenor should be taken within 30 minutes of bedtime.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1690" section="34068-7">
      

      <SentenceText>Take within 30 minutes of bedtime.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="0 4" str="Take" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="26 7" str="bedtime" type="Precipitant"/>
      <Interaction id="I1" precipitant="M2" trigger="M1" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1691" section="34068-7">
      

      <SentenceText>The daily dose can be increased to 6 mg, if clinically indicated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1692" section="34068-7">
      

      <SentenceText>The dose of Silenor should be individualized.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1693" section="34068-7">
      

      <SentenceText>The recommended dose of Silenor for adults is 6 mg once daily.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1694" section="34068-7">
      

      <SentenceText>The recommended starting dose of Silenor in elderly patients (≥ 65 years old) is 3 mg once daily.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1695" section="34068-7">
      

      <SentenceText>The total Silenor dose should not exceed 6 mg per day.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1696" section="34068-7">
      

      <SentenceText>To minimize the potential for next day effects, Silenor should not be taken within 3 hours of a meal.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1697" section="34068-7">
      

      <SentenceText>Total daily dose should not exceed 6 mg. (2.3) Should not be taken within 3 hours of a meal.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1698" section="34070-3">
      

      <SentenceText>Co-administration with Monoamine Oxidase Inhibitors (MAOIs): Do not administer if patient is taking MAOIs or has used MAOIs within the past two weeks.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="61 17" str="Do not administer" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="23 28" str="Monoamine Oxidase Inhibitors" type="Precipitant"/>
      <Interaction id="I2" precipitant="M4" trigger="M3" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1699" section="34070-3">
      

      <SentenceText>Do not administer Silenor if patient is currently on MAOIs or has used MAOIs within the past two weeks.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="0 17" str="Do not administer" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1700" section="34070-3">
      

      <SentenceText>Hypersensitivity to doxepin hydrochloride, inactive ingredients, or other dibenzoxepines.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1701" section="34070-3">
      

      <SentenceText>Serious side effects and even death have been reported following the concomitant use of certain drugs with MAO inhibitors.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="30 5" str="death" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M7" span="107 14" str="MAO inhibitors" type="Precipitant"/>
      <Interaction effect="M6" id="I3" precipitant="M7" trigger="M7" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1702" section="34070-3">
      

      <SentenceText>Silenor is contraindicated in individuals who have shown hypersensitivity to doxepin HCl, any of its inactive ingredients, or other dibenzoxepines.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1703" section="34070-3">
      

      <SentenceText>Silenor is contraindicated in individuals with untreated narrow angle glaucoma or severe urinary retention.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1704" section="34070-3">
      

      <SentenceText>The exact length of time may vary depending on the particular MAOI dosage and duration of treatment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1705" section="34070-3">
      

      <SentenceText>Untreated narrow angle glaucoma or severe urinary retention.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1706" section="34073-7">
      

      <SentenceText>A case of severe hypoglycemia has been reported in a type II diabetic patient maintained on tolazamide (1 g/day) 11 days after the addition of oral doxepin (75 mg/day).</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="10 6" str="severe" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1707" section="34073-7">
      

      <SentenceText>A maximum dose of 3 mg is recommended in adults and elderly when cimetidine is co-administered with Silenor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1708" section="34073-7">
      

      <SentenceText>Alcohol: Sedative effects may be increased with doxepin.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="9 20" str="Sedative effects may" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M10" span="0 7" str="Alcohol" type="Precipitant"/>
      <Interaction effect="M9" id="I4" precipitant="M10" trigger="M10" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1709" section="34073-7">
      

      <SentenceText>Cimetidine: Increases exposure to doxepin.</SentenceText>
      

      <Mention code="NO MAP" id="M11" span="12 18" str="Increases exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M12" span="0 10" str="Cimetidine" type="Precipitant"/>
      <Interaction effect="C54355" id="I5" precipitant="M12" trigger="M11" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1710" section="34073-7">
      

      <SentenceText>CNS Depressants and Sedating Antihistamines: Sedative effects may be increased with doxepin.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="45 16" str="Sedative effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M14" span="0 15" str="CNS Depressants" type="Precipitant"/>
      <Interaction effect="M13" id="I6" precipitant="M14" trigger="M14" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M15" span="20 8" str="Sedating" type="Precipitant"/>
      <Interaction effect="M13" id="I7" precipitant="M15" trigger="M15" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1711" section="34073-7">
      

      <SentenceText>Inhibitors of these isozymes may increase the exposure of doxepin.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="33 21" str="increase the exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M17" span="0 28" str="Inhibitors of these isozymes" type="Precipitant"/>
      <Interaction effect="C54355" id="I8" precipitant="M17" trigger="M16" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1712" section="34073-7">
      

      <SentenceText>MAO inhibitors: Silenor should not be administered in patients on MAOIs within the past two weeks.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="24 26" str="should not be administered" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1713" section="34073-7">
      

      <SentenceText>Silenor exposure is doubled with concomitant administration of cimetidine, a nonspecific inhibitor of CYP isozymes.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="8 8" str="exposure" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="63 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="C54605" id="I9" precipitant="M20" trigger="M19" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M21" span="89 9" str="inhibitor" type="Precipitant"/>
      <Interaction effect="C54355" id="I10" precipitant="M21" trigger="M19" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M22" span="102 12" str="CYP isozymes" type="Precipitant"/>
      <Interaction effect="C54355" id="I11" precipitant="M22" trigger="M19" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1714" section="34073-7">
      

      <SentenceText>Silenor is not an inhibitor of any CYP isozymes at therapeutically relevant concentrations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1715" section="34073-7">
      

      <SentenceText>Silenor is primarily metabolized by hepatic cytochrome P450 isozymes CYP2C19 and CYP2D6, and to a lesser extent, by CYP1A2 and CYP2C9.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1716" section="34073-7">
      

      <SentenceText>The ability of Silenor to induce CYP isozymes is not known.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1717" section="34073-7">
      

      <SentenceText>Tolazamide: A case of severe hypoglycemia has been reported.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="22 6" str="severe" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M24" span="29 12" str="hypoglycemia" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1718" section="34073-7">
      

      <SentenceText>When taken with Silenor, the sedative effects of sedating antihistamines and CNS depressants may be potentiated.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="29 16" str="sedative effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="49 8" str="sedating" type="Precipitant"/>
      <Interaction effect="M25" id="I12" precipitant="M26" trigger="M26" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M27" span="77 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="M25" id="I13" precipitant="M27" trigger="M27" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1719" section="43685-7">
      

      <SentenceText>Abnormal thinking, behavioral changes, complex behaviors: May include &quot;Sleep-driving&quot; and hallucinations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1720" section="43685-7">
      

      <SentenceText>After taking Silenor, patients should confine their activities to those necessary to prepare for bed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1721" section="43685-7">
      

      <SentenceText>Although behaviors such as &quot;sleep-driving&quot; may occur with hypnotics alone at therapeutic doses, the use of alcohol and other CNS depressants with hypnotics appears to increase the risk of such behaviors, as does the use of hypnotics at doses exceeding the maximum recommended dose.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="167 8" str="increase" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M29" span="58 9" str="hypnotics" type="Precipitant"/>
      <Interaction effect="M28" id="I14" precipitant="M29" trigger="M29" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M30" span="107 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="M28" id="I15" precipitant="M30" trigger="M30" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M31" span="125 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="M28" id="I16" precipitant="M31" trigger="M31" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M32" span="146 9" str="hypnotics" type="Precipitant"/>
      <Interaction effect="M28" id="I17" precipitant="M32" trigger="M32" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M33" span="223 9" str="hypnotics" type="Precipitant"/>
      <Interaction effect="M28" id="I18" precipitant="M33" trigger="M33" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1722" section="43685-7">
      

      <SentenceText>Amnesia, anxiety and other neuro-psychiatric symptoms may occur unpredictably.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="0 7" str="Amnesia" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1723" section="43685-7">
      

      <SentenceText>Antidepressants increased the risk compared to placebo of suicidal thinking and behavior (suicidality) in children, adolescents, and young adults in short-term studies of major depressive disorder (MDD) and other psychiatric disorders.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1724" section="43685-7">
      

      <SentenceText>As with &quot;sleep-driving&quot;, patients usually do not remember these events.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1725" section="43685-7">
      

      <SentenceText>Avoid engaging in hazardous activities such as operating a motor vehicle or heavy machinery after taking drug.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1726" section="43685-7">
      

      <SentenceText>Because sleep disturbances may be the presenting manifestation of a physical and/or psychiatric disorder, symptomatic treatment of insomnia should be initiated only after careful evaluation of the patient.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1727" section="43685-7">
      

      <SentenceText>CNS-depressant effects: Use can impair alertness and motor coordination.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="4 10" str="depressant" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1728" section="43685-7">
      

      <SentenceText>Complex behaviors such as &quot;sleep-driving&quot; (i.e., driving while not fully awake after ingestion of a hypnotic, with amnesia for the event) have been reported with hypnotics.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="162 9" str="hypnotics" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1729" section="43685-7">
      

      <SentenceText>Depression: Worsening of depression or suicidal thinking may occur.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1730" section="43685-7">
      

      <SentenceText>Doxepin, the active ingredient in Silenor, is an antidepressant at doses 10- to 100-fold higher than in Silenor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1731" section="43685-7">
      

      <SentenceText>Due to the risk to the patient and the community, discontinuation of Silenor should be strongly considered for patients who report a &quot;sleep-driving&quot; episode.</SentenceText>
      

      <Mention code="NO MAP" id="M37" span="50 15" str="discontinuation" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1732" section="43685-7">
      

      <SentenceText>Exacerbation of insomnia or the emergence of new cognitive or behavioral abnormalities may be the consequence of an unrecognized psychiatric or physical disorder.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1733" section="43685-7">
      

      <SentenceText>Immediately evaluate any new onset behavioral changes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1734" section="43685-7">
      

      <SentenceText>In primarily depressed patients, worsening of depression, including suicidal thoughts and actions (including completed suicides), has been reported in association with the use of hypnotics.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="179 9" str="hypnotics" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1735" section="43685-7">
      

      <SentenceText>It can rarely be determined with certainty whether a particular instance of the abnormal behaviors listed above is drug induced, spontaneous in origin, or a result of an underlying psychiatric or physical disorder.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1736" section="43685-7">
      

      <SentenceText>Need to Evaluate for Co-morbid Diagnoses: Reevaluate if insomnia persists after 7 to 10 days of use.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1737" section="43685-7">
      

      <SentenceText>Nonetheless, the emergence of any new behavioral sign or symptom of concern requires careful and immediate evaluation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1738" section="43685-7">
      

      <SentenceText>Other complex behaviors (e.g., preparing and eating food, making phone calls, or having sex) have been reported in patients who are not fully awake after taking a hypnotic.</SentenceText>
      

      <Mention code="NO MAP" id="M39" span="163 8" str="hypnotic" type="Precipitant"/>
      <Interaction id="I19" precipitant="M39" trigger="M39" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1739" section="43685-7">
      

      <SentenceText>Patients should avoid engaging in hazardous activities, such as operating a motor vehicle or heavy machinery, at night after taking Silenor, and should be cautioned about potential impairment in the performance of such activities that may occur the day following ingestion.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1740" section="43685-7">
      

      <SentenceText>Patients should be cautioned about potential additive effects of Silenor used in combination with CNS depressants or sedating antihistamines.</SentenceText>
      

      <Mention code="NO MAP" id="M40" span="19 9" str="cautioned" type="Trigger"/>
      <Mention code="NO MAP" id="M41" span="45 16" str="additive effects" type="Trigger"/>
      <Mention code="NO MAP" id="M42" span="98 15" str="CNS depressants" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1741" section="43685-7">
      

      <SentenceText>Patients should not consume alcohol with Silenor.</SentenceText>
      

      <Mention code="NO MAP" id="M43" span="9 6" str="should" type="Trigger"/>
      <Mention code="NO MAP" id="M44" span="16 11" str="not consume" type="Trigger"/>
      <Mention code="NO MAP" id="M45" span="28 7" str="alcohol" type="Precipitant"/>
      <Interaction id="I20" precipitant="M45" trigger="M44" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1742" section="43685-7">
      

      <SentenceText>Patients with severe sleep apnea: Silenor is ordinarily not recommended for use in this population.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1743" section="43685-7">
      

      <SentenceText>Potential additive effects when used in combination with CNS depressants or sedating antihistamines.</SentenceText>
      

      <Mention code="NO MAP" id="M46" span="0 9" str="Potential" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M47" span="10 16" str="additive effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M48" span="57 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="M46;M47" id="I21" precipitant="M48" trigger="M48" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M49" span="76 8" str="sedating" type="Precipitant"/>
      <Interaction effect="M46;M47" id="I22" precipitant="M49" trigger="M49" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1744" section="43685-7">
      

      <SentenceText>Prescribe the least amount feasible to avoid intentional overdose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1745" section="43685-7">
      

      <SentenceText>Risk from the lower dose of doxepin in Silenor can not be excluded.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1746" section="43685-7">
      

      <SentenceText>Such findings have emerged during the course of treatment with hypnotic drugs.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1747" section="43685-7">
      

      <SentenceText>The failure of insomnia to remit after 7 to 10 days of treatment may indicate the presence of a primary psychiatric and/or medical illness that should be evaluated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1748" section="43685-7">
      

      <SentenceText>These events can occur in hypnotic-naive as well as in hypnotic-experienced persons.</SentenceText>
      

      <Mention code="NO MAP" id="M50" span="0 12" str="These events" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M51" span="26 8" str="hypnotic" type="Precipitant"/>
      <Interaction effect="M50" id="I23" precipitant="M51" trigger="M51" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M52" span="55 8" str="hypnotic" type="Precipitant"/>
      <Interaction effect="M50" id="I24" precipitant="M52" trigger="M52" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Silenor" id="1749" section="43685-7">
      

      <SentenceText>When taken with Silenor, the sedative effects of alcoholic beverages, sedating antihistamines, and other CNS depressants may be potentiated.</SentenceText>
      

      <Mention code="NO MAP" id="M53" span="29 16" str="sedative effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M54" span="49 19" str="alcoholic beverages" type="Precipitant"/>
      <Interaction effect="M53" id="I25" precipitant="M54" trigger="M54" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M55" span="70 8" str="sedating" type="Precipitant"/>
      <Interaction effect="M53" id="I26" precipitant="M55" trigger="M55" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M56" span="105 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="M53" id="I27" precipitant="M56" trigger="M56" type="Pharmacodynamic interaction"/>
    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

