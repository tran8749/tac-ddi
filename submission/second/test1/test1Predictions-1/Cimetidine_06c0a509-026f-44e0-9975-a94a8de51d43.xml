<?xml version="1.0" ?>
<Label drug="Cimetidine" setid="06c0a509-026f-44e0-9975-a94a8de51d43">
  <Text>
    <Section id="34070-3" name="CONTRAINDICATIONS SECTION">
CONTRAINDICATIONS  Cimetidine tablets are contraindicated for patients known to have hypersensitivity to the product.</Section>
    

    <Section id="42232-9" name="PRECAUTIONS SECTION">
PRECAUTIONS  General  Rare instances of cardiac arrhythmias and hypotension have been reported following the rapid administration of cimetidine hydrochloride injection by intravenous bolus.  Symptomatic response to treatment with cimetidine does not preclude the presence of a gastric malignancy. There have been rare reports of transient healing of gastric ulcers despite subsequently documented malignancy.  Reversible confusional states (see ADVERSE REACTIONS ) have been observed on occasion, predominantly, but not exclusively, in severely ill patients. Advancing age (50 or more years) and preexisting liver and/or renal disease appear to be contributing factors. In some patients these confusional states have been mild and have not required discontinuation of cimetidine. In cases where discontinuation was judged necessary, the condition usually cleared within 3 to 4 days of drug withdrawal.  Drug Interactions  Cimetidine, apparently through an effect on certain microsomal enzyme systems, has been reported to reduce the hepatic metabolism of warfarin-type anticoagulants, phenytoin, propranolol, nifedipine, chlordiazepoxide, diazepam, certain tricyclic antidepressants, lidocaine, theophylline and metronidazole, thereby delaying elimination and increasing blood levels of these drugs.  Clinically significant effects have been reported with the warfarin anticoagulants; therefore, close monitoring of prothrombin time is recommended, and adjustment of the anticoagulant dose may be necessary when cimetidine is administered concomitantly. Interaction with phenytoin, lidocaine and theophylline has also been reported to produce adverse clinical effects.  However, a crossover study in healthy subjects receiving either 300 mg 4 times daily or 800 mg at bedtime of cimetidine concomitantly with a 300 mg twice daily dose of theophylline extended-release tablets demonstrated less alteration in steady-state theophylline peak serum levels with the 800 mg at bedtime regimen, particularly in subjects aged 54 years and older. Data beyond 10 days are not available. (Note: All patients receiving theophylline should be monitored appropriately, regardless of concomitant drug therapy.)  Dosage of the drugs mentioned above and other similarly metabolized drugs, particularly those of low therapeutic ratio or in patients with renal and/or hepatic impairment, may require adjustment when starting or stopping the concomitant administration of cimetidine to maintain optimum therapeutic blood levels.  Alteration of pH may affect absorption of certain drugs (e.g., ketoconazole). If these products are needed, they should be given at least 2 hours before cimetidine administration.  Additional clinical experience may reveal other drugs affected by the concomitant administration of cimetidine.  Carcinogenesis, Mutagenesis, Impairment of Fertility  In a 24-month toxicity study conducted in rats, at dose levels of 150, 378 and 950 mg/kg/day (approximately 8 to 48 times the recommended human dose), there was a small increase in the incidence of benign Leydig cell tumors in each dose group; when the combined drug-treated groups and control groups were compared, this increase reached statistical significance. In a subsequent 24-month study, there were no differences between the rats receiving 150 mg/kg/day and the untreated controls. However, a statistically significant increase in benign Leydig cell tumor incidence was seen in the rats that received 378 and 950 mg/kg/day. These tumors were common in control groups as well as treated groups and the difference became apparent only in aged rats.  Cimetidine has demonstrated a weak antiandrogenic effect. In animal studies this was manifested as reduced prostate and seminal vesicle weights. However, there was no impairment of mating performance or fertility, nor any harm to the fetus in these animals at doses 8 to 48 times the full therapeutic dose of cimetidine, as compared with controls. The cases of gynecomastia seen in patients treated for one month or longer may be related to this effect.  In human studies, cimetidine has been shown to have no effect on spermatogenesis, sperm count, motility, morphology or in vitro fertilizing capacity.  Pregnancy  Teratogenic Effects. Pregnancy Category B  Reproduction studies have been performed in rats, rabbits and mice at doses up to 40 times the normal human dose and have revealed no evidence of impaired fertility or harm to the fetus due to cimetidine. There are, however, no adequate and well-controlled studies in pregnant women. Because animal reproductive studies are not always predictive of human response, this drug should be used during pregnancy only if clearly needed.  Nursing Mothers  Cimetidine is secreted in human milk and, as a general rule, nursing should not be undertaken while a patient is on a drug.  Pediatric Use  Clinical experience in children is limited. Therefore, therapy with cimetidine cannot be recommended for children under 16, unless, in the judgement of the physician, anticipated benefits outweigh the potential risks. In very limited experience, doses of 20 to 40 mg/kg/day have been used.  Immunocompromised Patients  In immunocompromised patients, decreased gastric acidity, including that produced by acid-suppressing agents such as cimetidine, may increase the possibility of a hyperinfection of strongyloidiasis.</Section>
    

    <Section id="34068-7" name="DOSAGE &amp; ADMINISTRATION SECTION">
DOSAGE AND ADMINISTRATION  Duodenal Ulcer  Active Duodenal Ulcer  Clinical studies have indicated that suppression of nocturnal acid is the most important factor in duodenal ulcer healing (see CLINICAL PHARMACOLOGY: Antisecretory Activity: Acid Secretion ). This is supported by recent clinical trials (see CLINICAL TRIALS: Duodenal Ulcer: Active Duodenal Ulcer ). Therefore, there is no apparent rationale, except for familiarity with use, for treating with anything other than a once daily at bedtime dosage regimen.  In a U.S. dose-ranging study of 400 mg at bedtime, 800 mg at bedtime and 1600 mg at bedtime, a continuous dose-response relationship for ulcer healing was demonstrated.  However, 800 mg at bedtime is the dose of choice for most patients, as it provides a high healing rate (the difference between 800 mg at bedtime and 1600 mg at bedtime being small), maximal pain relief, a decreased potential for drug interactions (see PRECAUTIONS: Drug Interactions ) and maximal patient convenience. Patients unhealed at 4 weeks, or those with persistent symptoms, have been shown to benefit from 2 to 4 weeks of continued therapy.  It has been shown that patients who both have an endoscopically demonstrated ulcer larger than 1 cm and are also heavy smokers (i.e., smoke one pack of cigarettes or more per day) are more difficult to heal. There is some evidence which suggests that more rapid healing can be achieved in this subpopulation with 1600 mg of cimetidine tablets at bedtime. While early pain relief with either 800 mg at bedtime or 1600 mg at bedtime is equivalent in all patients, 1600 mg at bedtime provides an appropriate alternative when it is important to ensure healing within 4 weeks for this subpopulation. Alternatively, approximately 94% of all patients will also heal in 8 weeks with 800 mg of cimetidine tablets at bedtime  Other regimens of cimetidine tablets in the United States which have been shown to be effective are: 300 mg 4 times daily, with meals and at bedtime, the original regimen with which U.S. physicians have the most experience, and 400 mg twice daily, in the morning and at bedtime (see CLINICAL TRIALS: Duodenal Ulcer: Active Duodenal Ulcer ).  Concomitant antacids should be given as needed for relief of pain. However, simultaneous administration of cimetidine tablets and antacids is not recommended, since antacids have been reported to interfere with the absorption of cimetidine.  While healing with cimetidine tablets often occurs during the first week or two, treatment should be continued for 4 to 6 weeks unless healing has been demonstrated by endoscopic examination.  Maintenance Therapy for Duodenal Ulcer  In those patients requiring maintenance therapy, the recommended adult oral dose is 400 mg at bedtime.  Active Benign Gastric Ulcer  The recommended adult oral dosage for short-term treatment of active benign gastric ulcer is 800 mg at bedtime, or 300 mg 4 times a day with meals and at bedtime. Controlled clinical studies were limited to 6 weeks of treatment (see CLINICAL TRIALS ). A dose of 800 mg at bedtime is the preferred regimen for most patients based upon convenience and reduced potential for drug interactions. Symptomatic response to cimetidine tablets does not preclude the presence of a gastric malignancy. It is important to follow gastric ulcer patients to assure rapid progress to complete healing.  Erosive Gastroesophageal Reflux Disease (GERD)  The recommended adult oral dosage for the treatment of erosive esophagitis that has been diagnosed by endoscopy is 1600 mg daily in divided doses (800 mg twice daily or 400 mg 4 times daily) for 12 weeks. The use of cimetidine tablets beyond 12 weeks has not been established.  Pathological Hypersecretory Conditions (such as Zollinger-Ellison Syndrome)  Recommended adult oral dosage: 300 mg 4 times a day with meals and at bedtime. In some patients it may be necessary to administer higher doses more frequently. Doses should be adjusted to individual patient needs, but should not usually exceed 2400 mg per day and should continue as long as clinically indicated.  Dosage Adjustment for Patients with Impaired Renal Function  Patients with severely impaired renal function have been treated with cimetidine tablets. However, such usage has been very limited. On the basis of this experience the recommended dosage is 300 mg every 12 hours orally. Should the patient’s condition require, the frequency of dosing may be increased to every 8 hours or even further with caution. In severe renal failure, accumulation may occur and the lowest frequency of dosing compatible with an adequate patient response should be used. When liver impairment is also present, further reductions in dosage may be necessary. Hemodialysis reduces the level of circulating cimetidine. Ideally, the dosage schedule should be adjusted so that the timing of a scheduled dose coincides with the end of hemodialysis.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Cimetidine" id="307" section="34068-7">
      

      <SentenceText>A dose of 800 mg at bedtime is the preferred regimen for most patients based upon convenience and reduced potential for drug interactions.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="308" section="34068-7">
      

      <SentenceText>Alternatively, approximately 94% of all patients will also heal in 8 weeks with 800 mg of cimetidine tablets at bedtime Other regimens of cimetidine tablets in the United States which have been shown to be effective are: 300 mg 4 times daily, with meals and at bedtime, the original regimen with which U.S. physicians have the most experience, and 400 mg twice daily, in the morning and at bedtime.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="309" section="34068-7">
      

      <SentenceText>Clinical studies have indicated that suppression of nocturnal acid is the most important factor in duodenal ulcer healing.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="99 22" str="duodenal ulcer healing" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="62 4" str="acid" type="Precipitant"/>
      <Interaction effect="M1" id="I1" precipitant="M2" trigger="M2" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="310" section="34068-7">
      

      <SentenceText>Concomitant antacids should be given as needed for relief of pain.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="12 8" str="antacids" type="Precipitant"/>
      <Interaction id="I2" precipitant="M3" trigger="M3" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="311" section="34068-7">
      

      <SentenceText>Controlled clinical studies were limited to 6 weeks of treatment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="312" section="34068-7">
      

      <SentenceText>Doses should be adjusted to individual patient needs, but should not usually exceed 2400 mg per day and should continue as long as clinically indicated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="313" section="34068-7">
      

      <SentenceText>Hemodialysis reduces the level of circulating cimetidine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="314" section="34068-7">
      

      <SentenceText>However, 800 mg at bedtime is the dose of choice for most patients, as it provides a high healing rate (the difference between 800 mg at bedtime and 1600 mg at bedtime being small), maximal pain relief, a decreased potential for drug interactions and maximal patient convenience.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="315" section="34068-7">
      

      <SentenceText>However, simultaneous administration of cimetidine tablets and antacids is not recommended, since antacids have been reported to interfere with the absorption of cimetidine.</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="75 15" str="not recommended" type="Trigger"/>
      <Mention code="NO MAP" id="M5" span="129 9" str="interfere" type="Trigger"/>
      <Mention code="NO MAP" id="M6" span="144 14" str="the absorption" type="Trigger"/>
      <Mention code="NO MAP" id="M7" span="63 8" str="antacids" type="Precipitant"/>
      <Interaction id="I3" precipitant="M7" trigger="M6" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M8" span="98 8" str="antacids" type="Precipitant"/>
      <Interaction id="I4" precipitant="M8" trigger="M5" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="316" section="34068-7">
      

      <SentenceText>However, such usage has been very limited.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="317" section="34068-7">
      

      <SentenceText>Ideally, the dosage schedule should be adjusted so that the timing of a scheduled dose coincides with the end of hemodialysis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="318" section="34068-7">
      

      <SentenceText>In a U.S. dose-ranging study of 400 mg at bedtime, 800 mg at bedtime and 1600 mg at bedtime, a continuous dose-response relationship for ulcer healing was demonstrated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="319" section="34068-7">
      

      <SentenceText>In severe renal failure, accumulation may occur and the lowest frequency of dosing compatible with an adequate patient response should be used.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="3 6" str="severe" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M10" span="16 7" str="failure" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="320" section="34068-7">
      

      <SentenceText>In some patients it may be necessary to administer higher doses more frequently.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="321" section="34068-7">
      

      <SentenceText>In those patients requiring maintenance therapy, the recommended adult oral dose is 400 mg at bedtime.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="322" section="34068-7">
      

      <SentenceText>It has been shown that patients who both have an endoscopically demonstrated ulcer larger than 1 cm and are also heavy smokers (i.e., smoke one pack of cigarettes or more per day) are more difficult to heal.</SentenceText>
      

      <Mention code="NO MAP" id="M11" span="77 5" str="ulcer" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M12" span="184 14" str="more difficult" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M13" span="202 4" str="heal" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M14" span="134 5" str="smoke" type="Precipitant"/>
      <Interaction effect="M11;M12;M13" id="I5" precipitant="M14" trigger="M14" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M15" span="144 18" str="pack of cigarettes" type="Precipitant"/>
      <Interaction effect="M11;M12;M13" id="I6" precipitant="M15" trigger="M15" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="323" section="34068-7">
      

      <SentenceText>It is important to follow gastric ulcer patients to assure rapid progress to complete healing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="324" section="34068-7">
      

      <SentenceText>On the basis of this experience the recommended dosage is 300 mg every 12 hours orally.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="325" section="34068-7">
      

      <SentenceText>Patients unhealed at 4 weeks, or those with persistent symptoms, have been shown to benefit from 2 to 4 weeks of continued therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="326" section="34068-7">
      

      <SentenceText>Patients with severely impaired renal function have been treated with cimetidine tablets.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="327" section="34068-7">
      

      <SentenceText>Recommended adult oral dosage: 300 mg 4 times a day with meals and at bedtime.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="328" section="34068-7">
      

      <SentenceText>Should the patient’s condition require, the frequency of dosing may be increased to every 8 hours or even further with caution.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="329" section="34068-7">
      

      <SentenceText>Symptomatic response to cimetidine tablets does not preclude the presence of a gastric malignancy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="330" section="34068-7">
      

      <SentenceText>The recommended adult oral dosage for short-term treatment of active benign gastric ulcer is 800 mg at bedtime, or 300 mg 4 times a day with meals and at bedtime.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="331" section="34068-7">
      

      <SentenceText>The recommended adult oral dosage for the treatment of erosive esophagitis that has been diagnosed by endoscopy is 1600 mg daily in divided doses (800 mg twice daily or 400 mg 4 times daily) for 12 weeks.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="332" section="34068-7">
      

      <SentenceText>The use of cimetidine tablets beyond 12 weeks has not been established.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="333" section="34068-7">
      

      <SentenceText>There is some evidence which suggests that more rapid healing can be achieved in this subpopulation with 1600 mg of cimetidine tablets at bedtime.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="334" section="34068-7">
      

      <SentenceText>Therefore, there is no apparent rationale, except for familiarity with use, for treating with anything other than a once daily at bedtime dosage regimen.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="335" section="34068-7">
      

      <SentenceText>This is supported by recent clinical trials.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="336" section="34068-7">
      

      <SentenceText>When liver impairment is also present, further reductions in dosage may be necessary.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="337" section="34068-7">
      

      <SentenceText>While early pain relief with either 800 mg at bedtime or 1600 mg at bedtime is equivalent in all patients, 1600 mg at bedtime provides an appropriate alternative when it is important to ensure healing within 4 weeks for this subpopulation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="338" section="34068-7">
      

      <SentenceText>While healing with cimetidine tablets often occurs during the first week or two, treatment should be continued for 4 to 6 weeks unless healing has been demonstrated by endoscopic examination.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="339" section="34070-3">
      

      <SentenceText>Cimetidine tablets are contraindicated for patients known to have hypersensitivity to the product.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="340" section="42232-9">
      

      <SentenceText>(Note: All patients receiving theophylline should be monitored appropriately, regardless of concomitant drug therapy.)</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="341" section="42232-9">
      

      <SentenceText>Additional clinical experience may reveal other drugs affected by the concomitant administration of cimetidine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="342" section="42232-9">
      

      <SentenceText>Advancing age (50 or more years) and preexisting liver and/or renal disease appear to be contributing factors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="343" section="42232-9">
      

      <SentenceText>Alteration of pH may affect absorption of certain drugs (e.g., ketoconazole).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="344" section="42232-9">
      

      <SentenceText>Because animal reproductive studies are not always predictive of human response, this drug should be used during pregnancy only if clearly needed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="345" section="42232-9">
      

      <SentenceText>Cimetidine has demonstrated a weak antiandrogenic effect.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="30 26" str="weak antiandrogenic effect" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="346" section="42232-9">
      

      <SentenceText>Cimetidine is secreted in human milk and, as a general rule, nursing should not be undertaken while a patient is on a drug.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="76 17" str="not be undertaken" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="347" section="42232-9">
      

      <SentenceText>Cimetidine, apparently through an effect on certain microsomal enzyme systems, has been reported to reduce the hepatic metabolism of warfarin-type anticoagulants, phenytoin, propranolol, nifedipine, chlordiazepoxide, diazepam, certain tricyclic antidepressants, lidocaine, theophylline and metronidazole, thereby delaying elimination and increasing blood levels of these drugs.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="100 18" str="reduce the hepatic" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M19" span="119 10" str="metabolism" type="Trigger"/>
      <Mention code="NO MAP" id="M20" span="313 20" str="delaying elimination" type="Trigger"/>
      <Mention code="NO MAP" id="M21" span="349 12" str="blood levels" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M22" span="52 17" str="microsomal enzyme" type="Precipitant"/>
      <Interaction effect="M18;M21" id="I7" precipitant="M22" trigger="M20" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M23" span="133 28" str="warfarin-type anticoagulants" type="Precipitant"/>
      <Interaction effect="M18;M21" id="I8" precipitant="M23" trigger="M19" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M24" span="163 9" str="phenytoin" type="Precipitant"/>
      <Interaction effect="M18;M21" id="I9" precipitant="M24" trigger="M19" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M25" span="235 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="M18;M21" id="I10" precipitant="M25" trigger="M19" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M26" span="262 9" str="lidocaine" type="Precipitant"/>
      <Interaction effect="M18;M21" id="I11" precipitant="M26" trigger="M19" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M27" span="273 12" str="theophylline" type="Precipitant"/>
      <Interaction effect="M18;M21" id="I12" precipitant="M27" trigger="M19" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M28" span="290 13" str="metronidazole" type="Precipitant"/>
      <Interaction effect="M18;M21" id="I13" precipitant="M28" trigger="M19" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="348" section="42232-9">
      

      <SentenceText>Clinical experience in children is limited.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="349" section="42232-9">
      

      <SentenceText>Clinically significant effects have been reported with the warfarin anticoagulants; therefore, close monitoring of prothrombin time is recommended, and adjustment of the anticoagulant dose may be necessary when cimetidine is administered concomitantly.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="101 10" str="monitoring" type="Trigger"/>
      <Mention code="NO MAP" id="M30" span="59 23" str="warfarin anticoagulants" type="Precipitant"/>
      <Interaction id="I14" precipitant="M30" trigger="M29" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="350" section="42232-9">
      

      <SentenceText>Dosage of the drugs mentioned above and other similarly metabolized drugs, particularly those of low therapeutic ratio or in patients with renal and/or hepatic impairment, may require adjustment when starting or stopping the concomitant administration of cimetidine to maintain optimum therapeutic blood levels.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="351" section="42232-9">
      

      <SentenceText>However, a crossover study in healthy subjects receiving either 300 mg 4 times daily or 800 mg at bedtime of cimetidine concomitantly with a 300 mg twice daily dose of theophylline extended-release tablets demonstrated less alteration in steady-state theophylline peak serum levels with the 800 mg at bedtime regimen, particularly in subjects aged 54 years and older.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="352" section="42232-9">
      

      <SentenceText>However, a statistically significant increase in benign Leydig cell tumor incidence was seen in the rats that received 378 and 950 mg/kg/day.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="353" section="42232-9">
      

      <SentenceText>However, there was no impairment of mating performance or fertility, nor any harm to the fetus in these animals at doses 8 to 48 times the full therapeutic dose of cimetidine, as compared with controls.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="354" section="42232-9">
      

      <SentenceText>If these products are needed, they should be given at least 2 hours before cimetidine administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="355" section="42232-9">
      

      <SentenceText>In a 24-month toxicity study conducted in rats, at dose levels of 150, 378 and 950 mg/kg/day (approximately 8 to 48 times the recommended human dose), there was a small increase in the incidence of benign Leydig cell tumors in each dose group; when the combined drug-treated groups and control groups were compared, this increase reached statistical significance.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="356" section="42232-9">
      

      <SentenceText>In a subsequent 24-month study, there were no differences between the rats receiving 150 mg/kg/day and the untreated controls.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="357" section="42232-9">
      

      <SentenceText>In animal studies this was manifested as reduced prostate and seminal vesicle weights.</SentenceText>
      

      <Mention code="NO MAP" id="M31" span="41 16" str="reduced prostate" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="358" section="42232-9">
      

      <SentenceText>In cases where discontinuation was judged necessary, the condition usually cleared within 3 to 4 days of drug withdrawal.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="359" section="42232-9">
      

      <SentenceText>In human studies, cimetidine has been shown to have no effect on spermatogenesis, sperm count, motility, morphology or in vitro fertilizing capacity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="360" section="42232-9">
      

      <SentenceText>In immunocompromised patients, decreased gastric acidity, including that produced by acid-suppressing agents such as cimetidine, may increase the possibility of a hyperinfection of strongyloidiasis.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="133 24" str="increase the possibility" type="Trigger"/>
      <Mention code="NO MAP" id="M33" span="85 23" str="acid-suppressing agents" type="Precipitant"/>
      <Interaction effect="C54355" id="I15" precipitant="M33" trigger="M32" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="361" section="42232-9">
      

      <SentenceText>In some patients these confusional states have been mild and have not required discontinuation of cimetidine.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="79 15" str="discontinuation" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="362" section="42232-9">
      

      <SentenceText>In very limited experience, doses of 20 to 40 mg/kg/day have been used.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="363" section="42232-9">
      

      <SentenceText>Interaction with phenytoin, lidocaine and theophylline has also been reported to produce adverse clinical effects.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="106 7" str="effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M36" span="17 9" str="phenytoin" type="Precipitant"/>
      <Interaction effect="C54357" id="I16" precipitant="M36" trigger="M36" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M37" span="28 9" str="lidocaine" type="Precipitant"/>
      <Interaction effect="C54357" id="I17" precipitant="M37" trigger="M37" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M38" span="42 12" str="theophylline" type="Precipitant"/>
      <Interaction effect="C54357" id="I18" precipitant="M38" trigger="M38" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="364" section="42232-9">
      

      <SentenceText>Rare instances of cardiac arrhythmias and hypotension have been reported following the rapid administration of cimetidine hydrochloride injection by intravenous bolus.</SentenceText>
      

      <Mention code="NO MAP" id="M39" span="42 11" str="hypotension" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="365" section="42232-9">
      

      <SentenceText>Reproduction studies have been performed in rats, rabbits and mice at doses up to 40 times the normal human dose and have revealed no evidence of impaired fertility or harm to the fetus due to cimetidine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="366" section="42232-9">
      

      <SentenceText>Reversible confusional states have been observed on occasion, predominantly, but not exclusively, in severely ill patients.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="367" section="42232-9">
      

      <SentenceText>Symptomatic response to treatment with cimetidine does not preclude the presence of a gastric malignancy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="368" section="42232-9">
      

      <SentenceText>The cases of gynecomastia seen in patients treated for one month or longer may be related to this effect.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="369" section="42232-9">
      

      <SentenceText>There are, however, no adequate and well-controlled studies in pregnant women.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="370" section="42232-9">
      

      <SentenceText>There have been rare reports of transient healing of gastric ulcers despite subsequently documented malignancy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="371" section="42232-9">
      

      <SentenceText>Therefore, therapy with cimetidine cannot be recommended for children under 16, unless, in the judgement of the physician, anticipated benefits outweigh the potential risks.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Cimetidine" id="372" section="42232-9">
      

      <SentenceText>These tumors were common in control groups as well as treated groups and the difference became apparent only in aged rats.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

