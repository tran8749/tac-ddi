<?xml version="1.0" ?>
<Label drug="ULORIC" setid="54de10ef-fe5f-4930-b91d-6bbb04c664bd">
  <Text>
    <Section id="34068-7" name="DOSAGE &amp; ADMINISTRATION SECTION">
2 DOSAGE AND ADMINISTRATION  ULORIC is recommended at 40 mg or 80 mg once daily. The recommended starting dose of ULORIC is 40 mg once daily. For patients who do not achieve a serum uric acid (sUA) less than 6 mg/dL after 2 weeks with 40 mg, ULORIC 80 mg is recommended. ( 2.1 )  ULORIC can be administered without regard to food or antacid use. ( 2.1 )  Limit the dose of ULORIC to 40 mg once daily in patients with severe renal impairment. ( 2.2 , 8.6 )  2.1 Recommended Dose  For treatment of hyperuricemia in patients with gout, ULORIC is recommended at 40 mg or 80 mg once daily.  The recommended starting dose of ULORIC is 40 mg once daily. For patients who do not achieve a serum uric acid (sUA) less than 6 mg/dL after two weeks with 40 mg, ULORIC 80 mg is recommended.  ULORIC can be taken without regard to food or antacid use [see Clinical Pharmacology (12.3) ].  2.2 Special Populations  No dose adjustment is necessary when administering ULORIC in patients with mild or moderate renal impairment. The recommended starting dose of ULORIC is 40 mg once daily. For patients who do not achieve a sUA less than 6 mg/dL after two weeks with 40 mg, ULORIC 80 mg is recommended.  The dose of ULORIC is limited to 40 mg once daily in patients with severe renal impairment [see Use in Specific Populations (8.6) and Clinical Pharmacology (12.3) ] .  No dose adjustment is necessary in patients with mild to moderate hepatic impairment [see Use in Specific Populations (8.7) and Clinical Pharmacology (12.3) ] .  2.3 Uric Acid Level  Testing for the target serum uric acid level of less than 6 mg/dL may be performed as early as two weeks after initiating ULORIC therapy.  2.4 Gout Flares  Gout flares may occur after initiation of ULORIC due to changing serum uric acid levels resulting in mobilization of urate from tissue deposits. Flare prophylaxis with a non-steroidal anti-inflammatory drug (NSAID) or colchicine is recommended upon initiation of ULORIC. Prophylactic therapy may be beneficial for up to six months [see Clinical Studies (14.1) ] .  If a gout flare occurs during ULORIC treatment, ULORIC need not be discontinued. The gout flare should be managed concurrently, as appropriate for the individual patient [see Warnings and Precautions (5.1) ] .</Section>
    

    <Section id="34070-3" name="CONTRAINDICATIONS SECTION">
4 CONTRAINDICATIONS  ULORIC is contraindicated in patients being treated with azathioprine or mercaptopurine [see Drug Interactions (7) ] .  ULORIC is contraindicated in patients being treated with azathioprine or mercaptopurine. ( 4 )</Section>
    

    <Section id="43685-7" name="WARNINGS AND PRECAUTIONS SECTION">
5 WARNINGS AND PRECAUTIONS  Gout Flare : An increase in gout flares is frequently observed during initiation of anti-hyperuricemic agents, including ULORIC. If a gout flare occurs during treatment, ULORIC need not be discontinued. Prophylactic therapy (i.e., non-steroidal anti-inflammatory drug [NSAID] or colchicine upon initiation of treatment) may be beneficial for up to six months. ( 2.4 , 5.1 )  Cardiovascular Events : A higher rate of cardiovascular thromboembolic events was observed in patients treated with ULORIC than allopurinol in clinical trials. Monitor for signs and symptoms of MI and stroke. ( 5.2 )  Hepatic Effects: Postmarketing reports of hepatic failure, sometimes fatal. Causality cannot be excluded. If liver injury is detected, promptly interrupt ULORIC and assess patient for probable cause, then treat cause if possible, to resolution or stabilization. Do not restart ULORIC if liver injury is confirmed and no alternate etiology can be found. ( 5.3 )  Serious Skin Reactions: Postmarketing reports of serious skin and hypersensitivity reactions, including Stevens-Johnson Syndrome, drug reaction with eosinophilia and systemic symptoms (DRESS) and toxic epidermal necrolysis (TEN) have been reported in patients taking ULORIC. Discontinue ULORIC if serious skin reactions are suspected. ( 5.4 )  5.1 Gout Flare  After initiation of ULORIC, an increase in gout flares is frequently observed. This increase is due to reduction in serum uric acid levels, resulting in mobilization of urate from tissue deposits.  In order to prevent gout flares when ULORIC is initiated, concurrent prophylactic treatment with an NSAID or colchicine is recommended [see Dosage and Administration (2.4) ].  5.2 Cardiovascular Events  In the randomized controlled studies, there was a higher rate of cardiovascular thromboembolic events (cardiovascular deaths, non-fatal myocardial infarctions, and non-fatal strokes) in patients treated with ULORIC (0.74 per 100 P-Y [95% Confidence Interval (CI) 0.36-1.37]) than allopurinol (0.60 per 100 P-Y [95% CI 0.16-1.53]) [see Adverse Reactions (6.1) ] . A causal relationship with ULORIC has not been established. Monitor for signs and symptoms of myocardial infarction (MI) and stroke.  5.3 Hepatic Effects  There have been postmarketing reports of fatal and non-fatal hepatic failure in patients taking ULORIC, although the reports contain insufficient information necessary to establish the probable cause. During randomized controlled studies, transaminase elevations greater than three times the upper limit of normal (ULN) were observed (AST: 2%, 2%, and ALT: 3%, 2% in ULORIC and allopurinol-treated patients, respectively). No dose-effect relationship for these transaminase elevations was noted [see Clinical Pharmacology (12.3) ] .  Obtain a liver test panel (serum alanine aminotransferase [ALT], aspartate aminotransferase [AST], alkaline phosphatase, and total bilirubin) as a baseline before initiating ULORIC.  Measure liver tests promptly in patients who report symptoms that may indicate liver injury, including fatigue, anorexia, right upper abdominal discomfort, dark urine or jaundice. In this clinical context, if the patient is found to have abnormal liver tests (ALT greater than three times the upper limit of the reference range), ULORIC treatment should be interrupted and investigation done to establish the probable cause. ULORIC should not be restarted in these patients without another explanation for the liver test abnormalities.  Patients who have serum ALT greater than three times the reference range with serum total bilirubin greater than two times the reference range without alternative etiologies are at risk for severe drug-induced liver injury and should not be restarted on ULORIC. For patients with lesser elevations of serum ALT or bilirubin and with an alternate probable cause, treatment with ULORIC can be used with caution.  5.4 Serious Skin Reactions  Postmarketing reports of serious skin and hypersensitivity reactions, including Stevens-Johnson Syndrome, drug reaction with eosinophilia and systemic symptoms (DRESS) and toxic epidermal necrolysis (TEN) have been reported in patients taking ULORIC. Discontinue ULORIC if serious skin reactions are suspected [see Patient Counseling Information (17) ]. Many of these patients had reported previous similar skin reactions to allopurinol. ULORIC should be used with caution in these patients.</Section>
    

    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS  Concomitant administration of ULORIC with XO substrate drugs, azathioprine or mercaptopurine could increase plasma concentrations of these drugs resulting in severe toxicity. ( 7 )  7.1 Xanthine Oxidase Substrate Drugs  ULORIC is an XO inhibitor. Based on a drug interaction study in healthy patients, febuxostat altered the metabolism of theophylline (a substrate of XO) in humans [see Clinical Pharmacology (12.3) ]. Therefore, use with caution when coadministering ULORIC with theophylline.  Drug interaction studies of ULORIC with other drugs that are metabolized by XO (e.g., mercaptopurine and azathioprine) have not been conducted. Inhibition of XO by ULORIC may cause increased plasma concentrations of these drugs leading to toxicity [see Clinical Pharmacology (12.3) ] . ULORIC is contraindicated in patients being treated with azathioprine or mercaptopurine [see Contraindications (4) ].  7.2 Cytotoxic Chemotherapy Drugs  Drug interaction studies of ULORIC with cytotoxic chemotherapy have not been conducted. No data are available regarding the safety of ULORIC during cytotoxic chemotherapy.  7.3 In Vivo Drug Interaction Studies  Based on drug interaction studies in healthy patients, ULORIC does not have clinically significant interactions with colchicine, naproxen, indomethacin, hydrochlorothiazide, warfarin or desipramine [see Clinical Pharmacology (12.3) ]. Therefore, ULORIC may be used concomitantly with these medications.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="ULORIC" id="3846" section="34068-7">
      

      <SentenceText>Flare prophylaxis with a non-steroidal anti-inflammatory drug (NSAID) or colchicine is recommended upon initiation of ULORIC.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="25 43" str="non-steroidal anti-inflammatory drug (NSAID" type="Precipitant"/>
      <Interaction id="I1" precipitant="M1" trigger="M1" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3847" section="34068-7">
      

      <SentenceText>For patients who do not achieve a serum uric acid (sUA) less than 6 mg/dL after 2 weeks with 40 mg, ULORIC 80 mg is recommended.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3848" section="34068-7">
      

      <SentenceText>For patients who do not achieve a serum uric acid (sUA) less than 6 mg/dL after two weeks with 40 mg, ULORIC 80 mg is recommended.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3849" section="34068-7">
      

      <SentenceText>For patients who do not achieve a sUA less than 6 mg/dL after two weeks with 40 mg, ULORIC 80 mg is recommended.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3850" section="34068-7">
      

      <SentenceText>For treatment of hyperuricemia in patients with gout, ULORIC is recommended at 40 mg or 80 mg once daily.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3851" section="34068-7">
      

      <SentenceText>Gout flares may occur after initiation of ULORIC due to changing serum uric acid levels resulting in mobilization of urate from tissue deposits.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3852" section="34068-7">
      

      <SentenceText>If a gout flare occurs during ULORIC treatment, ULORIC need not be discontinued.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3853" section="34068-7">
      

      <SentenceText>Limit the dose of ULORIC to 40 mg once daily in patients with severe renal impairment.</SentenceText>
      

      <Mention code="NO MAP" id="M2" span="0 14" str="Limit the dose" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3854" section="34068-7">
      

      <SentenceText>No dose adjustment is necessary in patients with mild to moderate hepatic impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3855" section="34068-7">
      

      <SentenceText>No dose adjustment is necessary when administering ULORIC in patients with mild or moderate renal impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3856" section="34068-7">
      

      <SentenceText>Prophylactic therapy may be beneficial for up to six months.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3857" section="34068-7">
      

      <SentenceText>Testing for the target serum uric acid level of less than 6 mg/dL may be performed as early as two weeks after initiating ULORIC therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3858" section="34068-7">
      

      <SentenceText>The dose of ULORIC is limited to 40 mg once daily in patients with severe renal impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3859" section="34068-7">
      

      <SentenceText>The gout flare should be managed concurrently, as appropriate for the individual patient.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3860" section="34068-7">
      

      <SentenceText>The recommended starting dose of ULORIC is 40 mg once daily.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3861" section="34068-7">
      

      <SentenceText>ULORIC can be administered without regard to food or antacid use.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3862" section="34068-7">
      

      <SentenceText>ULORIC can be taken without regard to food or antacid use.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3863" section="34068-7">
      

      <SentenceText>ULORIC is recommended at 40 mg or 80 mg once daily.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3864" section="34073-7">
      

      <SentenceText>Based on a drug interaction study in healthy patients, febuxostat altered the metabolism of theophylline (a substrate of XO) in humans.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3865" section="34073-7">
      

      <SentenceText>Based on drug interaction studies in healthy patients, ULORIC does not have clinically significant interactions with colchicine, naproxen, indomethacin, hydrochlorothiazide, warfarin or desipramine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3866" section="34073-7">
      

      <SentenceText>Concomitant administration of ULORIC with XO substrate drugs, azathioprine or mercaptopurine could increase plasma concentrations of these drugs resulting in severe toxicity.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="99 30" str="increase plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="165 8" str="toxicity" type="Trigger"/>
      <Mention code="NO MAP" id="M5" span="42 18" str="XO substrate drugs" type="Precipitant"/>
      <Interaction effect="C54357" id="I2" precipitant="M5" trigger="M4" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M6" span="62 12" str="azathioprine" type="Precipitant"/>
      <Interaction effect="C54357" id="I3" precipitant="M6" trigger="M3" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M7" span="78 14" str="mercaptopurine" type="Precipitant"/>
      <Interaction effect="C54357" id="I4" precipitant="M7" trigger="M3" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3867" section="34073-7">
      

      <SentenceText>Drug interaction studies of ULORIC with cytotoxic chemotherapy have not been conducted.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3868" section="34073-7">
      

      <SentenceText>Drug interaction studies of ULORIC with other drugs that are metabolized by XO (e.g., mercaptopurine and azathioprine) have not been conducted.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3869" section="34073-7">
      

      <SentenceText>Inhibition of XO by ULORIC may cause increased plasma concentrations of these drugs leading to toxicity.</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="37 31" str="increased plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M9" span="95 8" str="toxicity" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="0 10" str="Inhibition" type="Precipitant"/>
      <Interaction effect="C54357" id="I5" precipitant="M10" trigger="M9" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M11" span="14 2" str="XO" type="Precipitant"/>
      <Interaction effect="C54357" id="I6" precipitant="M11" trigger="M8" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3870" section="34073-7">
      

      <SentenceText>No data are available regarding the safety of ULORIC during cytotoxic chemotherapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3871" section="34073-7">
      

      <SentenceText>Therefore, ULORIC may be used concomitantly with these medications.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3872" section="34073-7">
      

      <SentenceText>Therefore, use with caution when coadministering ULORIC with theophylline.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="11 8" str="use with" type="Trigger"/>
      <Mention code="NO MAP" id="M13" span="20 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M14" span="61 12" str="theophylline" type="Precipitant"/>
      <Interaction id="I7" precipitant="M14" trigger="M13" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3873" section="34073-7">
      

      <SentenceText>ULORIC is an XO inhibitor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3874" section="34073-7">
      

      <SentenceText>ULORIC is contraindicated in patients being treated with azathioprine or mercaptopurine.</SentenceText>
      

      <Mention code="NO MAP" id="M15" span="10 15" str="contraindicated" type="Trigger"/>
      <Mention code="NO MAP" id="M16" span="57 12" str="azathioprine" type="Precipitant"/>
      <Interaction id="I8" precipitant="M16" trigger="M15" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M17" span="73 14" str="mercaptopurine" type="Precipitant"/>
      <Interaction id="I9" precipitant="M17" trigger="M15" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3875" section="43685-7">
      

      <SentenceText>A causal relationship with ULORIC has not been established.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3876" section="43685-7">
      

      <SentenceText>After initiation of ULORIC, an increase in gout flares is frequently observed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3877" section="43685-7">
      

      <SentenceText>Cardiovascular Events: A higher rate of cardiovascular thromboembolic events was observed in patients treated with ULORIC than allopurinol in clinical trials.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="40 36" str="cardiovascular thromboembolic events" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3878" section="43685-7">
      

      <SentenceText>Discontinue ULORIC if serious skin reactions are suspected.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3879" section="43685-7">
      

      <SentenceText>Do not restart ULORIC if liver injury is confirmed and no alternate etiology can be found.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3880" section="43685-7">
      

      <SentenceText>During randomized controlled studies, transaminase elevations greater than three times the upper limit of normal (ULN) were observed (AST: 2%, 2%, and ALT: 3%, 2% in ULORIC and allopurinol-treated patients, respectively).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3881" section="43685-7">
      

      <SentenceText>For patients with lesser elevations of serum ALT or bilirubin and with an alternate probable cause, treatment with ULORIC can be used with caution.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3882" section="43685-7">
      

      <SentenceText>Gout Flare: An increase in gout flares is frequently observed during initiation of anti-hyperuricemic agents, including ULORIC.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3883" section="43685-7">
      

      <SentenceText>Hepatic Effects: Postmarketing reports of hepatic failure, sometimes fatal.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3884" section="43685-7">
      

      <SentenceText>If a gout flare occurs during treatment, ULORIC need not be discontinued.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3885" section="43685-7">
      

      <SentenceText>If liver injury is detected, promptly interrupt ULORIC and assess patient for probable cause, then treat cause if possible, to resolution or stabilization.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3886" section="43685-7">
      

      <SentenceText>In order to prevent gout flares when ULORIC is initiated, concurrent prophylactic treatment with an NSAID or colchicine is recommended.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="100 5" str="NSAID" type="Precipitant"/>
      <Interaction id="I10" precipitant="M19" trigger="M19" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M20" span="109 10" str="colchicine" type="Precipitant"/>
      <Interaction id="I11" precipitant="M20" trigger="M20" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3887" section="43685-7">
      

      <SentenceText>In the randomized controlled studies, there was a higher rate of cardiovascular thromboembolic events (cardiovascular deaths, non-fatal myocardial infarctions, and non-fatal strokes) in patients treated with ULORIC (0.74 per 100 P-Y [95% Confidence Interval (CI) 0.36-1.37]) than allopurinol (0.60 per 100 P-Y [95% CI 0.16-1.53]).</SentenceText>
      

      <Mention code="NO MAP" id="M21" span="65 14" str="cardiovascular" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M22" span="80 37" str="thromboembolic events (cardiovascular" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3888" section="43685-7">
      

      <SentenceText>In this clinical context, if the patient is found to have abnormal liver tests (ALT greater than three times the upper limit of the reference range), ULORIC treatment should be interrupted and investigation done to establish the probable cause.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3889" section="43685-7">
      

      <SentenceText>Many of these patients had reported previous similar skin reactions to allopurinol.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3890" section="43685-7">
      

      <SentenceText>Measure liver tests promptly in patients who report symptoms that may indicate liver injury, including fatigue, anorexia, right upper abdominal discomfort, dark urine or jaundice.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3891" section="43685-7">
      

      <SentenceText>Monitor for signs and symptoms of MI and stroke.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="0 7" str="Monitor" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3892" section="43685-7">
      

      <SentenceText>Monitor for signs and symptoms of myocardial infarction (MI) and stroke.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="0 7" str="Monitor" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3893" section="43685-7">
      

      <SentenceText>No dose-effect relationship for these transaminase elevations was noted.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3894" section="43685-7">
      

      <SentenceText>Obtain a liver test panel (serum alanine aminotransferase [ALT], aspartate aminotransferase [AST], alkaline phosphatase, and total bilirubin) as a baseline before initiating ULORIC.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3895" section="43685-7">
      

      <SentenceText>Patients who have serum ALT greater than three times the reference range with serum total bilirubin greater than two times the reference range without alternative etiologies are at risk for severe drug-induced liver injury and should not be restarted on ULORIC.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3896" section="43685-7">
      

      <SentenceText>Postmarketing reports of serious skin and hypersensitivity reactions, including Stevens-Johnson Syndrome, drug reaction with eosinophilia and systemic symptoms (DRESS) and toxic epidermal necrolysis (TEN) have been reported in patients taking ULORIC.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3897" section="43685-7">
      

      <SentenceText>Prophylactic therapy (i.e., non-steroidal anti-inflammatory drug [NSAID] or colchicine upon initiation of treatment) may be beneficial for up to six months.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3898" section="43685-7">
      

      <SentenceText>Serious Skin Reactions: Postmarketing reports of serious skin and hypersensitivity reactions, including Stevens-Johnson Syndrome, drug reaction with eosinophilia and systemic symptoms (DRESS) and toxic epidermal necrolysis (TEN) have been reported in patients taking ULORIC.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3899" section="43685-7">
      

      <SentenceText>There have been postmarketing reports of fatal and non-fatal hepatic failure in patients taking ULORIC, although the reports contain insufficient information necessary to establish the probable cause.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3900" section="43685-7">
      

      <SentenceText>This increase is due to reduction in serum uric acid levels, resulting in mobilization of urate from tissue deposits.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="24 12" str="reduction in" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3901" section="43685-7">
      

      <SentenceText>ULORIC should be used with caution in these patients.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="ULORIC" id="3902" section="43685-7">
      

      <SentenceText>ULORIC should not be restarted in these patients without another explanation for the liver test abnormalities.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

