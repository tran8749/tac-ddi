Phendimetrazine Tartrate	7809	34068-7	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	17
Each extended-release capsule contains 105 mg phendimetrazine tartrate in a Brown/Clear capsule imprinted E 5254.
1	0	3	O	Each
2	5	12	O	extended
3	14	20	O	release
4	22	28	O	capsule
5	30	37	O	contains
6	39	41	O	105
7	43	44	O	mg
8	46	69	O	XXXXXXXX
9	71	72	O	in
10	74	74	O	a
11	76	80	O	Brown
12	81	81	O	/
13	82	86	O	Clear
14	88	94	O	capsule
15	96	104	O	imprinted
16	106	106	O	E
17	108	111	O	5254
NULL

Phendimetrazine Tartrate	7810	34068-7	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	32
Extended-release capsule Since the product is an extended-release dosage form, limit to one extended-release capsule (105 mg phendimetrazine tartrate) in the morning (30 to 60 minutes before morning meal).
1	0	7	O	Extended
2	9	15	O	release
3	17	23	O	capsule
4	25	29	O	Since
5	31	33	O	the
6	35	41	O	product
7	43	44	O	is
8	46	47	O	an
9	49	56	O	extended
10	58	64	O	release
11	66	71	O	dosage
12	73	76	O	form
13	77	77	O	,
14	79	83	O	limit
15	85	86	O	to
16	88	90	O	one
17	92	99	O	extended
18	101	107	O	release
19	109	115	O	capsule
20	118	120	O	105
21	122	123	O	mg
22	125	148	O	XXXXXXXX
23	151	152	O	in
24	154	156	O	the
25	158	164	O	morning
26	167	168	O	30
27	170	171	O	to
28	173	174	O	60
29	176	182	O	minutes
30	184	189	O	before
31	191	197	O	morning
32	199	202	O	meal
NULL

Phendimetrazine Tartrate	7811	34070-3	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	61
*History of cardiovascular disease (e.g., coronary artery disease, stroke, arrhythmias, congestive heart failure, uncontrolled hypertension, pulmonary hypertension) *During or within 14 days following the administration of monoamine oxidase inhibitors *Hyperthyroidism *Glaucoma *Agitated states *History of drug abuse *Pregnancy *Nursing *Use in combination with other anorectic agents or CNS stimulants *Known hypersensitivity or idiosyncratic reactions to sympathomimetics.
1	0	7	O	*History
2	9	10	O	of
3	12	25	O	cardiovascular
4	27	33	O	disease
5	36	38	O	e.g
6	40	40	O	,
7	42	49	O	coronary
8	51	56	O	artery
9	58	64	O	disease
10	65	65	O	,
11	67	72	O	stroke
12	73	73	O	,
13	75	85	O	arrhythmias
14	86	86	O	,
15	88	97	O	congestive
16	99	103	O	heart
17	105	111	O	failure
18	112	112	O	,
19	114	125	O	uncontrolled
20	127	138	O	hypertension
21	139	139	O	,
22	141	149	O	pulmonary
23	151	162	O	hypertension
24	165	171	O	*During
25	173	174	O	or
26	176	181	O	within
27	183	184	O	14
28	186	189	O	days
29	191	199	O	following
30	201	203	O	the
31	205	218	O	administration
32	220	221	O	of
33	223	231	O	monoamine
34	233	239	O	oxidase
35	241	250	O	inhibitors
36	252	267	O	*Hyperthyroidism
37	269	277	O	*Glaucoma
38	279	287	O	*Agitated
39	289	294	O	states
40	296	303	O	*History
41	305	306	O	of
42	308	311	O	drug
43	313	317	O	abuse
44	319	328	O	*Pregnancy
45	330	337	O	*Nursing
46	339	342	O	*Use
47	344	345	O	in
48	347	357	O	combination
49	359	362	O	with
50	364	368	O	other
51	370	385	O	GGGGGGGG
52	387	388	O	or
53	390	392	O	CNS
54	394	403	O	stimulants
55	405	410	O	*Known
56	412	427	O	hypersensitivity
57	429	430	O	or
58	432	444	O	idiosyncratic
59	446	454	O	reactions
60	456	457	O	to
61	459	474	O	sympathomimetics
NULL

Phendimetrazine Tartrate	7812	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	14
Abrupt cessation of administration following prolonged high dosage results in extreme fatigue and depression.
1	0	5	O	Abrupt
2	7	15	O	cessation
3	17	18	O	of
4	20	33	O	administration
5	35	43	O	following
6	45	53	O	prolonged
7	55	58	O	high
8	60	65	O	dosage
9	67	73	O	results
10	75	76	O	in
11	78	84	O	extreme
12	86	92	O	fatigue
13	94	96	O	and
14	98	107	O	depression
NULL

Phendimetrazine Tartrate	7813	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	21
Baseline cardiac evaluation should be considered to detect preexisting valvular heart diseases or pulmonary hypertension prior to initiation of phendimetrazine treatment.
1	0	7	O	Baseline
2	9	15	O	cardiac
3	17	26	O	evaluation
4	28	33	O	should
5	35	36	O	be
6	38	47	O	considered
7	49	50	O	to
8	52	57	O	detect
9	59	69	O	preexisting
10	71	78	O	valvular
11	80	84	O	heart
12	86	93	O	diseases
13	95	96	O	or
14	98	106	O	pulmonary
15	108	119	O	hypertension
16	121	125	O	prior
17	127	128	O	to
18	130	139	O	initiation
19	141	142	O	of
20	144	158	O	XXXXXXXX
21	160	168	O	treatment
NULL

Phendimetrazine Tartrate	7814	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	40
Because of the effect on the central nervous system, phendimetrazine may impair the ability of the patient to engage in potentially hazardous activities such as operating machinery or driving a motor vehicle; the patient should therefore be cautioned accordingly.
1	0	6	O	Because
2	8	9	O	of
3	11	13	O	the
4	15	20	O	effect
5	22	23	O	on
6	25	27	O	the
7	29	35	O	central
8	37	43	O	nervous
9	45	50	O	system
10	51	51	O	,
11	53	67	O	XXXXXXXX
12	69	71	O	may
13	73	78	O	impair
14	80	82	O	the
15	84	90	O	ability
16	92	93	O	of
17	95	97	O	the
18	99	105	O	patient
19	107	108	O	to
20	110	115	O	engage
21	117	118	O	in
22	120	130	O	potentially
23	132	140	O	hazardous
24	142	151	O	activities
25	153	156	O	such
26	158	159	O	as
27	161	169	O	operating
28	171	179	O	machinery
29	181	182	O	or
30	184	190	O	driving
31	192	192	O	a
32	194	198	O	motor
33	200	206	O	vehicle
34	209	211	O	the
35	213	219	O	patient
36	221	226	O	should
37	228	236	O	therefore
38	238	239	O	be
39	241	249	O	cautioned
40	251	261	O	accordingly
NULL

Phendimetrazine Tartrate	7815	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	16
Echocardiogram during and after treatment could be useful for detecting any valvular disorders which may occur.
1	0	13	O	Echocardiogram
2	15	20	O	during
3	22	24	O	and
4	26	30	O	after
5	32	40	O	treatment
6	42	46	O	could
7	48	49	O	be
8	51	56	O	useful
9	58	60	O	for
10	62	70	O	detecting
11	72	74	O	any
12	76	83	O	valvular
13	85	93	O	disorders
14	95	99	O	which
15	101	103	O	may
16	105	109	O	occur
NULL

Phendimetrazine Tartrate	7816	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	33
In a case-control epidemiological study, the use of anorectic agents, including phendimetrazine tartrate, was associated with an increased risk of developing pulmonary hypertension, a rare, but often fatal disorder.
1	0	1	O	In
2	3	3	O	a
3	5	8	O	case
4	10	16	O	control
5	18	32	O	epidemiological
6	34	38	O	study
7	39	39	O	,
8	41	43	O	the
9	45	47	O	use
10	49	50	O	of
11	52	67	O	GGGGGGGG
12	68	68	O	,
13	70	78	O	including
14	80	103	O	XXXXXXXX
15	104	104	O	,
16	106	108	O	was
17	110	119	O	associated
18	121	124	O	with
19	126	127	O	an
20	129	137	O	increased
21	139	142	O	risk
22	144	145	O	of
23	147	156	O	developing
24	158	166	O	pulmonary
25	168	179	O	hypertension
26	180	180	O	,
27	182	182	O	a
28	184	187	O	rare
29	188	188	O	,
30	190	192	O	but
31	194	198	O	often
32	200	204	O	fatal
33	206	213	O	disorder
NULL

Phendimetrazine Tartrate	7817	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	13
Increased risk of pulmonary hypertension with repeated courses of therapy cannot be excluded.
1	0	8	O	Increased
2	10	13	O	risk
3	15	16	O	of
4	18	26	O	pulmonary
5	28	39	O	hypertension
6	41	44	O	with
7	46	53	O	repeated
8	55	61	O	courses
9	63	64	O	of
10	66	72	O	therapy
11	74	79	O	cannot
12	81	82	O	be
13	84	91	O	excluded
NULL

Phendimetrazine Tartrate	7818	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	14
Phendimetrazine tartrate is not recommended for patients who used any anorectic agents within the prior year.
1	0	23	O	XXXXXXXX
2	25	26	O	is
3	28	30	B-T	not
4	32	42	I-T	recommended
5	44	46	O	for
6	48	55	O	patients
7	57	59	O	who
8	61	64	O	used
9	66	68	O	any
10	70	85	B-U	GGGGGGGG
11	87	92	O	within
12	94	96	O	the
13	98	102	O	prior
14	104	107	O	year
NULL

Phendimetrazine Tartrate	7819	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	14
Phendimetrazine tartrate is not recommended in patients with known heart murmur or valvular heart disease.
1	0	23	O	XXXXXXXX
2	25	26	O	is
3	28	30	O	not
4	32	42	O	recommended
5	44	45	O	in
6	47	54	O	patients
7	56	59	O	with
8	61	65	O	known
9	67	71	O	heart
10	73	78	O	murmur
11	80	81	O	or
12	83	90	O	valvular
13	92	96	O	heart
14	98	104	O	disease
NULL

Phendimetrazine Tartrate	7820	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	22
Phendimetrazine tartrate should not be used in combination with other anorectic agents, including prescribed drugs, over-the-counter preparations and herbal products.
1	0	23	O	XXXXXXXX
2	25	30	O	should
3	32	34	B-T	not
4	36	37	I-T	be
5	39	42	I-T	used
6	44	45	O	in
7	47	57	O	combination
8	59	62	O	with
9	64	68	O	other
10	70	85	B-D	GGGGGGGG
11	86	86	O	,
12	88	96	O	including
13	98	107	O	prescribed
14	109	113	O	drugs
15	114	114	O	,
16	116	119	B-D	over
17	121	123	I-D	the
18	125	131	I-D	counter
19	133	144	I-D	preparations
20	146	148	O	and
21	150	155	B-D	herbal
22	157	164	I-D	products
NULL

Phendimetrazine Tartrate	7821	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	26
Possible contributing factors include use for extended periods of time, higher than recommended dose, and/or use in combination with other anorectic drugs.
1	0	7	O	Possible
2	9	20	O	contributing
3	22	28	O	factors
4	30	36	O	include
5	38	40	O	use
6	42	44	O	for
7	46	53	O	extended
8	55	61	O	periods
9	63	64	O	of
10	66	69	O	time
11	70	70	O	,
12	72	77	O	higher
13	79	82	O	than
14	84	94	O	recommended
15	96	99	O	dose
16	100	100	O	,
17	102	104	O	and
18	105	105	O	/
19	106	107	O	or
20	109	111	O	use
21	113	114	O	in
22	116	126	O	combination
23	128	131	O	with
24	133	137	O	other
25	139	147	B-U	anorectic
26	149	153	I-U	drugs
NULL

Phendimetrazine Tartrate	7822	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	29
The onset or aggravation of exertional dyspnea, or unexplained symptoms of angina pectoris, syncope, or lower extremity edema suggest the possibility of occurrence of pulmonary hypertension.
1	0	2	O	The
2	4	8	O	onset
3	10	11	O	or
4	13	23	O	aggravation
5	25	26	O	of
6	28	37	O	exertional
7	39	45	O	dyspnea
8	46	46	O	,
9	48	49	O	or
10	51	61	O	unexplained
11	63	70	O	symptoms
12	72	73	O	of
13	75	80	O	angina
14	82	89	O	pectoris
15	90	90	O	,
16	92	98	O	syncope
17	99	99	O	,
18	101	102	O	or
19	104	108	O	lower
20	110	118	O	extremity
21	120	124	O	edema
22	126	132	O	suggest
23	134	136	O	the
24	138	148	O	possibility
25	150	151	O	of
26	153	162	O	occurrence
27	164	165	O	of
28	167	175	O	pulmonary
29	177	188	O	hypertension
NULL

Phendimetrazine Tartrate	7823	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	27
The potential risk of possible serious adverse effects such as valvular heart disease and pulmonary hypertension should be assessed carefully against the potential benefit of weight loss.
1	0	2	O	The
2	4	12	O	potential
3	14	17	O	risk
4	19	20	O	of
5	22	29	O	possible
6	31	37	O	serious
7	39	45	O	adverse
8	47	53	O	effects
9	55	58	O	such
10	60	61	O	as
11	63	70	O	valvular
12	72	76	O	heart
13	78	84	O	disease
14	86	88	O	and
15	90	98	O	pulmonary
16	100	111	O	hypertension
17	113	118	O	should
18	120	121	O	be
19	123	130	O	assessed
20	132	140	O	carefully
21	142	148	O	against
22	150	152	O	the
23	154	162	O	potential
24	164	170	O	benefit
25	172	173	O	of
26	175	180	O	weight
27	182	185	O	loss
NULL

Phendimetrazine Tartrate	7824	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	23
The use of anorectic agents for longer than three months was associated with a 23-fold increase in the risk of developing pulmonary hypertension.
1	0	2	O	The
2	4	6	O	use
3	8	9	O	of
4	11	26	O	GGGGGGGG
5	28	30	O	for
6	32	37	O	longer
7	39	42	O	than
8	44	48	O	three
9	50	55	O	months
10	57	59	O	was
11	61	70	O	associated
12	72	75	O	with
13	77	77	O	a
14	79	80	O	23
15	82	85	O	fold
16	87	94	O	increase
17	96	97	O	in
18	99	101	O	the
19	103	106	O	risk
20	108	109	O	of
21	111	120	O	developing
22	122	130	O	pulmonary
23	132	143	O	hypertension
NULL

Phendimetrazine Tartrate	7825	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	12
Tolerance to the anorectic effect of phendimetrazine develops within a few weeks.
1	0	8	O	Tolerance
2	10	11	O	to
3	13	15	O	the
4	17	25	O	anorectic
5	27	32	O	effect
6	34	35	O	of
7	37	51	O	XXXXXXXX
8	53	60	O	develops
9	62	67	O	within
10	69	69	O	a
11	71	73	O	few
12	75	79	O	weeks
NULL

Phendimetrazine Tartrate	7826	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	23
Under these circumstances, phendimetrazine tartrate should be immediately discontinued, and the patient should be evaluated for the possible presence of pulmonary hypertension.
1	0	4	O	Under
2	6	10	O	these
3	12	24	O	circumstances
4	25	25	O	,
5	27	50	O	XXXXXXXX
6	52	57	O	should
7	59	60	O	be
8	62	72	O	immediately
9	74	85	O	discontinued
10	86	86	O	,
11	88	90	O	and
12	92	94	O	the
13	96	102	O	patient
14	104	109	O	should
15	111	112	O	be
16	114	122	O	evaluated
17	124	126	O	for
18	128	130	O	the
19	132	139	O	possible
20	141	148	O	presence
21	150	151	O	of
22	153	161	O	pulmonary
23	163	174	O	hypertension
NULL

Phendimetrazine Tartrate	7827	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	19
Use of phendimetrazine tartrate within 14 days following the administration of monoamine oxidase inhibitors may result in a hypertensive crisis.
1	0	2	O	Use
2	4	5	O	of
3	7	30	O	XXXXXXXX
4	32	37	O	within
5	39	40	O	14
6	42	45	O	days
7	47	55	O	following
8	57	59	O	the
9	61	74	O	administration
10	76	77	O	of
11	79	87	O	monoamine
12	89	95	O	oxidase
13	97	106	O	inhibitors
14	108	110	O	may
15	112	117	O	result
16	119	120	O	in
17	122	122	O	a
18	124	135	O	hypertensive
19	137	142	O	crisis
NULL

Phendimetrazine Tartrate	7828	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	18
Valvular heart disease associated with the use of some anorectic agents such as fenfluramine and dexfenfluramine has been reported.
1	0	7	O	Valvular
2	9	13	O	heart
3	15	21	O	disease
4	23	32	O	associated
5	34	37	O	with
6	39	41	O	the
7	43	45	O	use
8	47	48	O	of
9	50	53	B-D	some
10	55	70	I-D	GGGGGGGG
11	72	75	O	such
12	77	78	O	as
13	80	91	B-D	fenfluramine
14	93	95	O	and
15	97	111	B-D	dexfenfluramine
16	113	115	O	has
17	117	120	O	been
18	122	129	O	reported
NULL

Phendimetrazine Tartrate	7829	34071-1	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	17
When this occurs, its use should be discontinued; the maximum recommended dose should not be exceeded.
1	0	3	O	When
2	5	8	O	this
3	10	15	O	occurs
4	16	16	O	,
5	18	20	O	its
6	22	24	O	use
7	26	31	O	should
8	33	34	O	be
9	36	47	O	discontinued
10	50	52	O	the
11	54	60	O	maximum
12	62	72	O	recommended
13	74	77	O	dose
14	79	84	O	should
15	86	88	O	not
16	90	91	O	be
17	93	100	O	exceeded
NULL

Phendimetrazine Tartrate	7830	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	39
A minimum weight gain, and no weight loss, is currently recommended for all pregnant women, including those who are already overweight or obese, due to obligatory weight gain that occurs in maternal tissues during pregnancy.
1	0	0	O	A
2	2	8	O	minimum
3	10	15	O	weight
4	17	20	O	gain
5	21	21	O	,
6	23	25	O	and
7	27	28	O	no
8	30	35	O	weight
9	37	40	O	loss
10	41	41	O	,
11	43	44	O	is
12	46	54	O	currently
13	56	66	O	recommended
14	68	70	O	for
15	72	74	O	all
16	76	83	O	pregnant
17	85	89	O	women
18	90	90	O	,
19	92	100	O	including
20	102	106	O	those
21	108	110	O	who
22	112	114	O	are
23	116	122	O	already
24	124	133	O	overweight
25	135	136	O	or
26	138	142	O	obese
27	143	143	O	,
28	145	147	O	due
29	149	150	O	to
30	152	161	O	obligatory
31	163	168	O	weight
32	170	173	O	gain
33	175	178	O	that
34	180	185	O	occurs
35	187	188	O	in
36	190	197	O	maternal
37	199	205	O	tissues
38	207	212	O	during
39	214	222	O	pregnancy
NULL

Phendimetrazine Tartrate	7831	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	14
Alcohol Concomitant use of alcohol with phendimetrazine tartrate may result in an adverse drug reaction.
1	0	6	B-K	Alcohol
2	8	18	O	Concomitant
3	20	22	O	use
4	24	25	O	of
5	27	33	B-K	alcohol
6	35	38	O	with
7	40	63	O	XXXXXXXX
8	65	67	O	may
9	69	74	O	result
10	76	77	O	in
11	79	80	O	an
12	82	88	O	adverse
13	90	93	O	drug
14	95	102	O	reaction
K/1:C54357 K/5:C54357

Phendimetrazine Tartrate	7832	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	9
Animal reproduction studies have not been conducted in phendimetrazine tartrate.
1	0	5	O	Animal
2	7	18	O	reproduction
3	20	26	O	studies
4	28	31	O	have
5	33	35	O	not
6	37	40	O	been
7	42	50	O	conducted
8	52	53	O	in
9	55	78	O	XXXXXXXX
NULL

Phendimetrazine Tartrate	7833	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	17
As phendimetrazine tartrate is excreted in urine, exposure increases can be expected in patients with renal impairment.
1	0	1	O	As
2	3	26	O	XXXXXXXX
3	28	29	O	is
4	31	38	O	excreted
5	40	41	O	in
6	43	47	O	urine
7	48	48	O	,
8	50	57	O	exposure
9	59	67	O	increases
10	69	71	O	can
11	73	74	O	be
12	76	83	O	expected
13	85	86	O	in
14	88	95	O	patients
15	97	100	O	with
16	102	106	O	renal
17	108	117	O	impairment
NULL

Phendimetrazine Tartrate	7834	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	29
Because elderly patients are more likely to have decreased renal function, care should be taken in dose selection, and it may be useful to monitor renal function.
1	0	6	O	Because
2	8	14	O	elderly
3	16	23	O	patients
4	25	27	O	are
5	29	32	O	more
6	34	39	O	likely
7	41	42	O	to
8	44	47	O	have
9	49	57	O	decreased
10	59	63	O	renal
11	65	72	O	function
12	73	73	O	,
13	75	78	O	care
14	80	85	O	should
15	87	88	O	be
16	90	94	O	taken
17	96	97	O	in
18	99	102	O	dose
19	104	112	O	selection
20	113	113	O	,
21	115	117	O	and
22	119	120	O	it
23	122	124	O	may
24	126	127	O	be
25	129	134	O	useful
26	136	137	O	to
27	139	145	B-T	monitor
28	147	151	O	renal
29	153	160	O	function
NULL

Phendimetrazine Tartrate	7835	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	38
Because of the potential for serious adverse reactions in nursing infants, a decision should be made whether to discontinue nursing or to discontinue the drug, taking into account the importance of the drug to the mother.
1	0	6	O	Because
2	8	9	O	of
3	11	13	O	the
4	15	23	O	potential
5	25	27	O	for
6	29	35	O	serious
7	37	43	O	adverse
8	45	53	O	reactions
9	55	56	O	in
10	58	64	O	nursing
11	66	72	O	infants
12	73	73	O	,
13	75	75	O	a
14	77	84	O	decision
15	86	91	O	should
16	93	94	O	be
17	96	99	O	made
18	101	107	O	whether
19	109	110	O	to
20	112	122	O	discontinue
21	124	130	O	nursing
22	132	133	O	or
23	135	136	O	to
24	138	148	O	discontinue
25	150	152	O	the
26	154	157	O	drug
27	158	158	O	,
28	160	165	O	taking
29	167	170	O	into
30	172	178	O	account
31	180	182	O	the
32	184	193	O	importance
33	195	196	O	of
34	198	200	O	the
35	202	205	O	drug
36	207	208	O	to
37	210	212	O	the
38	214	219	O	mother
NULL

Phendimetrazine Tartrate	7836	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	34
Because pediatric obesity is a chronic condition requiring long-term treatment, the use of phendimetrazine tartrate ER approved for short-term therapy, is not recommended in patients less that 17 years of age.
1	0	6	O	Because
2	8	16	O	pediatric
3	18	24	O	obesity
4	26	27	O	is
5	29	29	O	a
6	31	37	O	chronic
7	39	47	O	condition
8	49	57	O	requiring
9	59	62	O	long
10	64	67	O	term
11	69	77	O	treatment
12	78	78	O	,
13	80	82	O	the
14	84	86	O	use
15	88	89	O	of
16	91	114	O	XXXXXXXX
17	116	117	O	ER
18	119	126	O	approved
19	128	130	O	for
20	132	136	O	short
21	138	141	O	term
22	143	149	O	therapy
23	150	150	O	,
24	152	153	O	is
25	155	157	O	not
26	159	169	O	recommended
27	171	172	O	in
28	174	181	O	patients
29	183	186	O	less
30	188	191	O	that
31	193	194	O	17
32	196	200	O	years
33	202	203	O	of
34	205	207	O	age
NULL

Phendimetrazine Tartrate	7837	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	14
Caution is to be exercised in prescribing phendimetrazine tartrate for patients with even mild hypertension.
1	0	6	O	Caution
2	8	9	O	is
3	11	12	O	to
4	14	15	O	be
5	17	25	O	exercised
6	27	28	O	in
7	30	40	O	prescribing
8	42	65	O	XXXXXXXX
9	67	69	O	for
10	71	78	O	patients
11	80	83	O	with
12	85	88	O	even
13	90	93	O	mild
14	95	106	O	hypertension
NULL

Phendimetrazine Tartrate	7838	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	31
If this drug is used during pregnancy, or if the patient becomes pregnant while taking this drug, the patient should be apprised of the potential hazard to a fetus.
1	0	1	O	If
2	3	6	O	this
3	8	11	O	drug
4	13	14	O	is
5	16	19	O	used
6	21	26	O	during
7	28	36	O	pregnancy
8	37	37	O	,
9	39	40	O	or
10	42	43	O	if
11	45	47	O	the
12	49	55	O	patient
13	57	63	O	becomes
14	65	72	O	pregnant
15	74	78	O	while
16	80	85	O	taking
17	87	90	O	this
18	92	95	O	drug
19	96	96	O	,
20	98	100	O	the
21	102	108	O	patient
22	110	115	O	should
23	117	118	O	be
24	120	127	O	apprised
25	129	130	O	of
26	132	134	O	the
27	136	144	O	potential
28	146	151	O	hazard
29	153	154	O	to
30	156	156	O	a
31	158	162	O	fetus
NULL

Phendimetrazine Tartrate	7839	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	46
In general, dose selection for an elderly patient should be cautious, usually starting at the low end of the dosing range, reflecting the greater frequency of decreased hepatic, renal, or cardiac function, and of concomitant disease or other drug therapy.
1	0	1	O	In
2	3	9	O	general
3	10	10	O	,
4	12	15	O	dose
5	17	25	O	selection
6	27	29	O	for
7	31	32	O	an
8	34	40	O	elderly
9	42	48	O	patient
10	50	55	O	should
11	57	58	O	be
12	60	67	B-T	cautious
13	68	68	O	,
14	70	76	O	usually
15	78	85	O	starting
16	87	88	O	at
17	90	92	O	the
18	94	96	O	low
19	98	100	O	end
20	102	103	O	of
21	105	107	O	the
22	109	114	O	dosing
23	116	120	O	range
24	121	121	O	,
25	123	132	O	reflecting
26	134	136	O	the
27	138	144	O	greater
28	146	154	O	frequency
29	156	157	O	of
30	159	167	O	decreased
31	169	175	O	hepatic
32	176	176	O	,
33	178	182	O	renal
34	183	183	O	,
35	185	186	O	or
36	188	194	B-U	cardiac
37	196	203	O	function
38	204	204	O	,
39	206	208	O	and
40	210	211	O	of
41	213	223	O	concomitant
42	225	231	O	disease
43	233	234	O	or
44	236	240	O	other
45	242	245	O	drug
46	247	253	O	therapy
NULL

Phendimetrazine Tartrate	7840	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	24
Insulin and Oral Hypoglycemic Medications Requirements may be altered Adrenergic Neuron Blocking Drugs phendimetrazine tartrate may decrease the hypotensive effect of adrenergic neuron blocking drugs.
1	0	6	B-D	Insulin
2	8	10	O	and
3	12	15	O	Oral
4	17	28	O	Hypoglycemic
5	30	40	O	Medications
6	42	53	O	Requirements
7	55	57	O	may
8	59	60	O	be
9	62	68	O	altered
10	70	79	O	Adrenergic
11	81	86	O	Neuron
12	88	95	O	Blocking
13	97	101	O	Drugs
14	103	126	O	XXXXXXXX
15	128	130	O	may
16	132	139	B-E	decrease
17	141	143	I-E	the
18	145	155	I-E	hypotensive
19	157	162	I-E	effect
20	164	165	O	of
21	167	176	B-D	adrenergic
22	178	183	I-D	neuron
23	185	192	I-D	blocking
24	194	198	I-D	drugs
D/1:16:1 D/21:16:1

Phendimetrazine Tartrate	7841	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	24
Insulin or oral hypoglycemic medication requirements in diabetes mellitus may be altered in association with the use of phendimetrazine and the concomitant dietary regimen.
1	0	6	B-U	Insulin
2	8	9	O	or
3	11	14	B-U	oral
4	16	27	I-U	hypoglycemic
5	29	38	I-U	medication
6	40	51	O	requirements
7	53	54	O	in
8	56	63	O	diabetes
9	65	72	O	mellitus
10	74	76	O	may
11	78	79	O	be
12	81	87	O	altered
13	89	90	O	in
14	92	102	O	association
15	104	107	O	with
16	109	111	O	the
17	113	115	O	use
18	117	118	O	of
19	120	134	O	XXXXXXXX
20	136	138	O	and
21	140	142	O	the
22	144	154	O	concomitant
23	156	162	O	dietary
24	164	170	O	regimen
NULL

Phendimetrazine Tartrate	7842	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	11
It is not known if phendimetrazine tartrate is excreted in human milk.
1	0	1	O	It
2	3	4	O	is
3	6	8	O	not
4	10	14	O	known
5	16	17	O	if
6	19	42	O	XXXXXXXX
7	44	45	O	is
8	47	54	O	excreted
9	56	57	O	in
10	59	63	O	human
11	65	68	O	milk
NULL

Phendimetrazine Tartrate	7843	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	27
Monoamine Oxidase Inhibitors Use of phendimetrazine tartrate is contraindicated during or within 14 days following the administration of monoamine oxidase inhibitors because of the risk of hypertensive crisis.
1	0	8	O	Monoamine
2	10	16	O	Oxidase
3	18	27	O	Inhibitors
4	29	31	O	Use
5	33	34	O	of
6	36	59	O	XXXXXXXX
7	61	62	O	is
8	64	78	B-T	contraindicated
9	80	85	O	during
10	87	88	O	or
11	90	95	O	within
12	97	98	O	14
13	100	103	O	days
14	105	113	O	following
15	115	117	O	the
16	119	132	O	administration
17	134	135	O	of
18	137	145	O	monoamine
19	147	153	O	oxidase
20	155	164	O	inhibitors
21	166	172	O	because
22	174	175	O	of
23	177	179	O	the
24	181	184	O	risk
25	186	187	O	of
26	189	200	O	hypertensive
27	202	207	O	crisis
NULL

Phendimetrazine Tartrate	7844	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	8
Phendimetrazine may decrease the hypotensive effect of guanethidine.
1	0	14	O	XXXXXXXX
2	16	18	O	may
3	20	27	B-E	decrease
4	29	31	I-E	the
5	33	43	I-E	hypotensive
6	45	50	I-E	effect
7	52	53	O	of
8	55	66	O	guanethidine
NULL

Phendimetrazine Tartrate	7845	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	22
Phendimetrazine tartrate is contraindicated during pregnancy because weight loss offers no potential benefit to a pregnant woman and may result in fetal harm.
1	0	23	O	XXXXXXXX
2	25	26	O	is
3	28	42	O	contraindicated
4	44	49	O	during
5	51	59	O	pregnancy
6	61	67	O	because
7	69	74	O	weight
8	76	79	O	loss
9	81	86	O	offers
10	88	89	O	no
11	91	99	O	potential
12	101	107	O	benefit
13	109	110	O	to
14	112	112	O	a
15	114	121	O	pregnant
16	123	127	O	woman
17	129	131	O	and
18	133	135	O	may
19	137	142	O	result
20	144	145	O	in
21	147	151	O	fetal
22	153	156	O	harm
NULL

Phendimetrazine Tartrate	7846	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	12
Phendimetrazine tartrate, a phenylalkylamine sympathomimetic amine has pharmacological activity similar to amphetamines.
1	0	23	O	XXXXXXXX
2	24	24	O	,
3	26	26	O	a
4	28	43	O	phenylalkylamine
5	45	59	O	sympathomimetic
6	61	65	O	amine
7	67	69	O	has
8	71	85	O	pharmacological
9	87	94	O	activity
10	96	102	O	similar
11	104	105	O	to
12	107	118	O	amphetamines
NULL

Phendimetrazine Tartrate	7847	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	23
Phendimetrazine tartrate, a phenylalkylamine sympathomimetic amine, has pharmacological activity similar to the amphetamines , and other amphetamines are present in human milk.
1	0	23	O	XXXXXXXX
2	24	24	O	,
3	26	26	O	a
4	28	43	O	phenylalkylamine
5	45	59	O	sympathomimetic
6	61	65	O	amine
7	66	66	O	,
8	68	70	O	has
9	72	86	O	pharmacological
10	88	95	O	activity
11	97	103	O	similar
12	105	106	O	to
13	108	110	O	the
14	112	123	O	amphetamines
15	125	125	O	,
16	127	129	O	and
17	131	135	O	other
18	137	148	O	amphetamines
19	150	152	O	are
20	154	160	O	present
21	162	163	O	in
22	165	169	O	human
23	171	174	O	milk
NULL

Phendimetrazine Tartrate	7848	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	14
Renal Impairment Phendimetrazine tartrate extended-release capsules were not studied in patients with renal impairment.
1	0	4	O	Renal
2	6	15	O	Impairment
3	17	40	O	XXXXXXXX
4	42	49	O	extended
5	51	57	O	release
6	59	66	O	capsules
7	68	71	O	were
8	73	75	O	not
9	77	83	O	studied
10	85	86	O	in
11	88	95	O	patients
12	97	100	O	with
13	102	106	O	renal
14	108	117	O	impairment
NULL

Phendimetrazine Tartrate	7849	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	10
Safety and effectiveness in pediatric patients have not been established.
1	0	5	O	Safety
2	7	9	O	and
3	11	23	O	effectiveness
4	25	26	O	in
5	28	36	O	pediatric
6	38	45	O	patients
7	47	50	O	have
8	52	54	O	not
9	56	59	O	been
10	61	71	O	established
NULL

Phendimetrazine Tartrate	7850	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	20
Studies with phendimetrazine tartrate sustained release have not been performed to evaluate carcinogenic potential, mutagenic potential or effects on fertility.
1	0	6	O	Studies
2	8	11	O	with
3	13	36	O	XXXXXXXX
4	38	46	O	sustained
5	48	54	O	release
6	56	59	O	have
7	61	63	O	not
8	65	68	O	been
9	70	78	O	performed
10	80	81	O	to
11	83	90	O	evaluate
12	92	103	O	carcinogenic
13	105	113	O	potential
14	114	114	O	,
15	116	124	O	mutagenic
16	126	134	O	potential
17	136	137	O	or
18	139	145	O	effects
19	147	148	O	on
20	150	158	O	fertility
NULL

Phendimetrazine Tartrate	7851	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	20
The least amount feasible should be prescribed or dispensed at one time in order to minimize the possibility of overdosage.
1	0	2	O	The
2	4	8	O	least
3	10	15	O	amount
4	17	24	O	feasible
5	26	31	O	should
6	33	34	O	be
7	36	45	O	prescribed
8	47	48	O	or
9	50	58	O	dispensed
10	60	61	O	at
11	63	65	O	one
12	67	70	O	time
13	72	73	O	in
14	75	79	O	order
15	81	82	O	to
16	84	91	O	minimize
17	93	95	O	the
18	97	107	O	possibility
19	109	110	O	of
20	112	121	O	overdosage
NULL

Phendimetrazine Tartrate	7852	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	18
The major route of elimination is via the kidney where most of the drug and metabolites are excreted.
1	0	2	O	The
2	4	8	O	major
3	10	14	O	route
4	16	17	O	of
5	19	29	O	elimination
6	31	32	O	is
7	34	36	O	via
8	38	40	O	the
9	42	47	O	kidney
10	49	53	O	where
11	55	58	O	most
12	60	61	O	of
13	63	65	O	the
14	67	70	O	drug
15	72	74	O	and
16	76	86	O	metabolites
17	88	90	O	are
18	92	99	O	excreted
NULL

Phendimetrazine Tartrate	7853	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	17
The risk of toxic reactions to this drug may be greater in patients with impaired renal function.
1	0	2	O	The
2	4	7	O	risk
3	9	10	O	of
4	12	16	O	toxic
5	18	26	B-E	reactions
6	28	29	O	to
7	31	34	O	this
8	36	39	O	drug
9	41	43	O	may
10	45	46	O	be
11	48	54	O	greater
12	56	57	O	in
13	59	66	O	patients
14	68	71	O	with
15	73	80	O	impaired
16	82	86	O	renal
17	88	95	O	function
NULL

Phendimetrazine Tartrate	7854	42232-9	fc4a052c-29ff-428b-bdf1-bb079f10c0bc	10
Use caution when administering phendimetrazine tartrate to patients with renal impairment.
1	0	2	O	Use
2	4	10	B-T	caution
3	12	15	O	when
4	17	29	O	administering
5	31	54	O	XXXXXXXX
6	56	57	O	to
7	59	66	O	patients
8	68	71	O	with
9	73	77	O	renal
10	79	88	O	impairment
NULL

