Zanaflex	3222	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	0
.
NULL

Zanaflex	3223	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	33
Dosage can be gradually increased by 2 mg to 4 mg at each dose, with 1 to 4 days between dosage increases, until a satisfactory reduction of muscle tone is achieved.
1	0	5	O	Dosage
2	7	9	O	can
3	11	12	O	be
4	14	22	O	gradually
5	24	32	O	increased
6	34	35	O	by
7	37	37	O	2
8	39	40	O	mg
9	42	43	O	to
10	45	45	O	4
11	47	48	O	mg
12	50	51	O	at
13	53	56	O	each
14	58	61	O	dose
15	62	62	O	,
16	64	67	O	with
17	69	69	O	1
18	71	72	O	to
19	74	74	O	4
20	76	79	O	days
21	81	87	O	between
22	89	94	O	dosage
23	96	104	O	increases
24	105	105	O	,
25	107	111	O	until
26	113	113	O	a
27	115	126	O	satisfactory
28	128	136	O	reduction
29	138	139	O	of
30	141	146	O	muscle
31	148	151	O	tone
32	153	154	O	is
33	156	163	O	achieved
NULL

Zanaflex	3224	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	14
Food has complex effects on tizanidine pharmacokinetics, which differ with the different formulations.
1	0	3	O	Food
2	5	7	O	has
3	9	15	O	complex
4	17	23	O	effects
5	25	26	O	on
6	28	37	O	XXXXXXXX
7	39	54	O	pharmacokinetics
8	55	55	O	,
9	57	61	O	which
10	63	68	O	differ
11	70	73	O	with
12	75	77	O	the
13	79	87	O	different
14	89	100	O	formulations
NULL

Zanaflex	3225	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	20
For this reason, the prescriber should be thoroughly familiar with the changes in kinetics associated with these different conditions.
1	0	2	O	For
2	4	7	O	this
3	9	14	O	reason
4	15	15	O	,
5	17	19	O	the
6	21	30	O	prescriber
7	32	37	O	should
8	39	40	O	be
9	42	51	O	thoroughly
10	53	60	O	familiar
11	62	65	O	with
12	67	69	O	the
13	71	77	O	changes
14	79	80	O	in
15	82	89	O	kinetics
16	91	100	O	associated
17	102	105	O	with
18	107	111	O	these
19	113	121	O	different
20	123	132	O	conditions
NULL

Zanaflex	3226	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	25
Once the formulation has been selected and the decision to take with or without food has been made, this regimen should not be altered.
1	0	3	O	Once
2	5	7	O	the
3	9	19	O	formulation
4	21	23	O	has
5	25	28	O	been
6	30	37	O	selected
7	39	41	O	and
8	43	45	O	the
9	47	54	O	decision
10	56	57	O	to
11	59	62	O	take
12	64	67	O	with
13	69	70	O	or
14	72	78	O	without
15	80	83	O	food
16	85	87	O	has
17	89	92	O	been
18	94	97	O	made
19	98	98	O	,
20	100	103	O	this
21	105	111	O	regimen
22	113	118	O	should
23	120	122	O	not
24	124	125	O	be
25	127	133	O	altered
NULL

Zanaflex	3227	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	72
Recommended starting dose: 2 mg; dose can be repeated at 6 to 8 hour intervals, up to a maximum of 3 doses in 24 hours ( 2.1) Dosage can be increased by 2 mg to 4 mg per dose, with 1 to 4 days between increases; total daily dose should not exceed 36 mg ( 2.1) Tizanidine pharmacokinetics differs between tablets and capsules, and when taken with or without food.
1	0	10	O	Recommended
2	12	19	O	starting
3	21	24	O	dose
4	25	25	O	:
5	27	27	O	2
6	29	30	O	mg
7	33	36	O	dose
8	38	40	O	can
9	42	43	O	be
10	45	52	O	repeated
11	54	55	O	at
12	57	57	O	6
13	59	60	O	to
14	62	62	O	8
15	64	67	O	hour
16	69	77	O	intervals
17	78	78	O	,
18	80	81	O	up
19	83	84	O	to
20	86	86	O	a
21	88	94	O	maximum
22	96	97	O	of
23	99	99	O	3
24	101	105	O	doses
25	107	108	O	in
26	110	111	O	24
27	113	117	O	hours
28	121	123	O	2.1
29	126	131	O	Dosage
30	133	135	O	can
31	137	138	O	be
32	140	148	O	increased
33	150	151	O	by
34	153	153	O	2
35	155	156	O	mg
36	158	159	O	to
37	161	161	O	4
38	163	164	O	mg
39	166	168	O	per
40	170	173	O	dose
41	174	174	O	,
42	176	179	O	with
43	181	181	O	1
44	183	184	O	to
45	186	186	O	4
46	188	191	O	days
47	193	199	O	between
48	201	209	O	increases
49	212	216	O	total
50	218	222	O	daily
51	224	227	O	dose
52	229	234	O	should
53	236	238	O	not
54	240	245	O	exceed
55	247	248	O	36
56	250	251	O	mg
57	255	257	O	2.1
58	260	269	O	XXXXXXXX
59	271	286	O	pharmacokinetics
60	288	294	O	differs
61	296	302	O	between
62	304	310	O	tablets
63	312	314	O	and
64	316	323	O	capsules
65	324	324	O	,
66	326	328	O	and
67	330	333	O	when
68	335	339	O	taken
69	341	344	O	with
70	346	347	O	or
71	349	355	O	without
72	357	360	O	food
NULL

Zanaflex	3228	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	10
Single doses greater than 16 mg have not been studied.
1	0	5	O	Single
2	7	11	O	doses
3	13	19	O	greater
4	21	24	O	than
5	26	27	O	16
6	29	30	O	mg
7	32	35	O	have
8	37	39	O	not
9	41	44	O	been
10	46	52	O	studied
NULL

Zanaflex	3229	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	54
The recommended starting dose is 2 mg. Because the effect of Zanaflex peaks at approximately 1 to 2 hours post-dose and dissipates between 3 to 6 hours post-dose, treatment can be repeated at 6 to 8 hour intervals, as needed, to a maximum of three doses in 24 hours.
1	0	2	O	The
2	4	14	O	recommended
3	16	23	O	starting
4	25	28	O	dose
5	30	31	O	is
6	33	33	O	2
7	35	36	O	mg
8	39	45	O	Because
9	47	49	O	the
10	51	56	O	effect
11	58	59	O	of
12	61	68	O	XXXXXXXX
13	70	74	O	peaks
14	76	77	O	at
15	79	91	O	approximately
16	93	93	O	1
17	95	96	O	to
18	98	98	O	2
19	100	104	O	hours
20	106	109	O	post
21	111	114	O	dose
22	116	118	O	and
23	120	129	O	dissipates
24	131	137	O	between
25	139	139	O	3
26	141	142	O	to
27	144	144	O	6
28	146	150	O	hours
29	152	155	O	post
30	157	160	O	dose
31	161	161	O	,
32	163	171	O	treatment
33	173	175	O	can
34	177	178	O	be
35	180	187	O	repeated
36	189	190	O	at
37	192	192	O	6
38	194	195	O	to
39	197	197	O	8
40	199	202	O	hour
41	204	212	O	intervals
42	213	213	O	,
43	215	216	O	as
44	218	223	O	needed
45	224	224	O	,
46	226	227	O	to
47	229	229	O	a
48	231	237	O	maximum
49	239	240	O	of
50	242	246	O	three
51	248	252	O	doses
52	254	255	O	in
53	257	258	O	24
54	260	264	O	hours
NULL

Zanaflex	3230	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	9
The total daily dose should not exceed 36 mg.
1	0	2	O	The
2	4	8	O	total
3	10	14	O	daily
4	16	19	O	dose
5	21	26	O	should
6	28	30	B-T	not
7	32	37	I-T	exceed
8	39	40	I-T	36
9	42	43	O	mg
NULL

Zanaflex	3231	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	25
These changes may result in increased adverse events, or delayed or more rapid onset of activity, depending upon the nature of the switch.
1	0	4	O	These
2	6	12	O	changes
3	14	16	O	may
4	18	23	O	result
5	25	26	O	in
6	28	36	O	increased
7	38	44	O	adverse
8	46	51	O	events
9	52	52	O	,
10	54	55	O	or
11	57	63	O	delayed
12	65	66	O	or
13	68	71	O	more
14	73	77	O	rapid
15	79	83	O	onset
16	85	86	O	of
17	88	95	O	activity
18	96	96	O	,
19	98	106	O	depending
20	108	111	O	upon
21	113	115	O	the
22	117	122	O	nature
23	124	125	O	of
24	127	129	O	the
25	131	136	O	switch
NULL

Zanaflex	3232	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	52
These differences could result in a change in tolerability and control of symptoms ( 2.1, 12.3) To discontinue Zanaflex, decrease dose slowly to minimize the risk of withdrawal and rebound hypertension, tachycardia, and hypertonia ( 2.2) Zanaflex Capsules * or Zanaflex * tablets may be prescribed with or without food.
1	0	4	O	These
2	6	16	O	differences
3	18	22	O	could
4	24	29	O	result
5	31	32	O	in
6	34	34	O	a
7	36	41	O	change
8	43	44	O	in
9	46	57	O	tolerability
10	59	61	O	and
11	63	69	O	control
12	71	72	O	of
13	74	81	O	symptoms
14	85	87	O	2.1
15	88	88	O	,
16	90	93	O	12.3
17	96	97	O	To
18	99	109	O	discontinue
19	111	118	O	XXXXXXXX
20	119	119	O	,
21	121	128	O	decrease
22	130	133	O	dose
23	135	140	O	slowly
24	142	143	O	to
25	145	152	O	minimize
26	154	156	O	the
27	158	161	O	risk
28	163	164	O	of
29	166	175	O	withdrawal
30	177	179	O	and
31	181	187	O	rebound
32	189	200	O	hypertension
33	201	201	O	,
34	203	213	O	tachycardia
35	214	214	O	,
36	216	218	O	and
37	220	229	O	hypertonia
38	233	235	O	2.2
39	238	245	O	XXXXXXXX
40	247	254	O	Capsules
41	256	256	O	*
42	258	259	O	or
43	261	268	O	XXXXXXXX
44	270	270	O	*
45	272	278	O	tablets
46	280	282	O	may
47	284	285	O	be
48	287	296	O	prescribed
49	298	301	O	with
50	303	304	O	or
51	306	312	O	without
52	314	317	O	food
NULL

Zanaflex	3233	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	26
These pharmacokinetic differences may result in clinically significant differences when switching administration of tablet and capsules and when switching administration between the fed or fasted state.
1	0	4	O	These
2	6	20	O	pharmacokinetic
3	22	32	O	differences
4	34	36	O	may
5	38	43	O	result
6	45	46	O	in
7	48	57	O	clinically
8	59	69	O	significant
9	71	81	O	differences
10	83	86	O	when
11	88	96	O	switching
12	98	111	O	administration
13	113	114	O	of
14	116	121	O	tablet
15	123	125	O	and
16	127	134	O	capsules
17	136	138	O	and
18	140	143	O	when
19	145	153	O	switching
20	155	168	O	administration
21	170	176	O	between
22	178	180	O	the
23	182	184	O	fed
24	186	187	O	or
25	189	194	O	fasted
26	196	200	O	state
NULL

Zanaflex	3234	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	32
Zanaflex Capsules and Zanaflex tablets are bioequivalent to each other under fasting conditions (more than 3 hours after a meal), but not under fed conditions (within 30 minutes of a meal).
1	0	7	O	XXXXXXXX
2	9	16	O	Capsules
3	18	20	O	and
4	22	29	O	XXXXXXXX
5	31	37	O	tablets
6	39	41	O	are
7	43	55	O	bioequivalent
8	57	58	O	to
9	60	63	O	each
10	65	69	O	other
11	71	75	O	under
12	77	83	O	fasting
13	85	94	O	conditions
14	97	100	O	more
15	102	105	O	than
16	107	107	O	3
17	109	113	O	hours
18	115	119	O	after
19	121	121	O	a
20	123	126	O	meal
21	128	128	O	,
22	130	132	O	but
23	134	136	O	not
24	138	142	O	under
25	144	146	O	fed
26	148	157	O	conditions
27	160	165	O	within
28	167	168	O	30
29	170	176	O	minutes
30	178	179	O	of
31	181	181	O	a
32	183	186	O	meal
NULL

Zanaflex	3235	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	12
Zanaflex should be used with caution in patients with any hepatic impairment.
1	0	7	O	XXXXXXXX
2	9	14	O	should
3	16	17	O	be
4	19	22	B-T	used
5	24	27	O	with
6	29	35	B-T	caution
7	37	38	O	in
8	40	47	O	patients
9	49	52	O	with
10	54	56	O	any
11	58	64	O	hepatic
12	66	75	O	impairment
NULL

Zanaflex	3236	34068-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	27
Zanaflex should be used with caution in patients with renal insufficiency (creatinine clearance < 25 mL/min), as clearance is reduced by more than 50%.
1	0	7	O	XXXXXXXX
2	9	14	O	should
3	16	17	O	be
4	19	22	B-T	used
5	24	27	I-T	with
6	29	35	B-T	caution
7	37	38	O	in
8	40	47	O	patients
9	49	52	O	with
10	54	58	O	renal
11	60	72	O	insufficiency
12	75	84	O	creatinine
13	86	94	O	clearance
14	96	96	B-U	<
15	98	99	I-U	25
16	101	102	O	mL
17	103	103	O	/
18	104	106	O	min
19	108	108	O	,
20	110	111	O	as
21	113	121	B-T	clearance
22	123	124	I-T	is
23	126	132	I-T	reduced
24	134	135	O	by
25	137	140	O	more
26	142	145	O	than
27	147	149	O	50%
NULL

Zanaflex	3237	34070-3	413ee468-c7e8-4283-8e2b-b573dcc1f607	20
Concomitant use with potent inhibitors of CYP1A2, such as fluvoxamine or ciprofloxacin( 4, 5.5, 7.1, 7.2)
1	0	10	O	Concomitant
2	12	14	O	use
3	16	19	O	with
4	21	26	B-U	potent
5	28	37	I-U	inhibitors
6	39	40	I-U	of
7	42	47	I-U	CYP1A2
8	48	48	O	,
9	50	53	O	such
10	55	56	O	as
11	58	68	O	fluvoxamine
12	70	71	O	or
13	73	85	O	ciprofloxacin
14	88	88	O	4
15	89	89	O	,
16	91	93	O	5.5
17	94	94	O	,
18	96	98	O	7.1
19	99	99	O	,
20	101	103	O	7.2
NULL

Zanaflex	3238	34070-3	413ee468-c7e8-4283-8e2b-b573dcc1f607	15
Zanaflex is contraindicated in patientstaking potent inhibitors of CYP1A2, such as fluvoxamine or ciprofloxacin.
1	0	7	O	XXXXXXXX
2	9	10	O	is
3	12	26	B-T	contraindicated
4	28	29	O	in
5	31	44	O	patientstaking
6	46	51	B-U	potent
7	53	62	I-U	inhibitors
8	64	65	O	of
9	67	72	O	CYP1A2
10	73	73	O	,
11	75	78	O	such
12	80	81	O	as
13	83	93	O	fluvoxamine
14	95	96	O	or
15	98	110	O	ciprofloxacin
NULL

Zanaflex	3239	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	15
Alcohol increases the overall amount of drug in the bloodstream after a dose of Zanaflex.
1	0	6	B-K	Alcohol
2	8	16	B-T	increases
3	18	20	I-T	the
4	22	28	O	overall
5	30	35	O	amount
6	37	38	O	of
7	40	43	O	drug
8	45	46	O	in
9	48	50	O	the
10	52	62	O	bloodstream
11	64	68	O	after
12	70	70	O	a
13	72	75	O	dose
14	77	78	O	of
15	80	87	O	XXXXXXXX
K/1:C54355

Zanaflex	3240	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	21
Because hypotensive effects may be cumulative, it is not recommended that Zanaflex be used with other A 2-adrenergic agonists.
1	0	6	O	Because
2	8	18	B-E	hypotensive
3	20	26	O	effects
4	28	30	O	may
5	32	33	O	be
6	35	44	O	cumulative
7	45	45	O	,
8	47	48	O	it
9	50	51	O	is
10	53	55	B-T	not
11	57	67	I-T	recommended
12	69	72	O	that
13	74	81	O	XXXXXXXX
14	83	84	O	be
15	86	89	O	used
16	91	94	O	with
17	96	100	O	other
18	102	102	O	A
19	104	104	B-U	2
20	106	115	I-U	adrenergic
21	117	124	I-U	agonists
NULL

Zanaflex	3241	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	83
Changes in pharmacokinetics of tizanidine when administered with ciprofloxacin resulted in significantly decreased blood pressure, increased drowsiness, and increased psychomotor impairment sSee Contraindications (4) and Clinical Pharmacology (12.3) ] Because of potential drug interactions, concomitant use of Zanaflex with other CYP1A2 inhibitors, such as zileuton, fluoroquinolones other than strong CYP1A2 inhibitors (which are contraindicated), antiarrythmics (amiodarone, mexiletine, propafenone, and verapamil), cimetidine, famotidine, oral contraceptives, acyclovir, and ticlopidine) should be avoided.
1	0	6	O	Changes
2	8	9	O	in
3	11	26	O	pharmacokinetics
4	28	29	O	of
5	31	40	O	XXXXXXXX
6	42	45	O	when
7	47	58	O	administered
8	60	63	O	with
9	65	77	B-K	ciprofloxacin
10	79	86	O	resulted
11	88	89	O	in
12	91	103	O	significantly
13	105	113	B-E	decreased
14	115	119	I-E	blood
15	121	128	I-E	pressure
16	129	129	O	,
17	131	139	O	increased
18	141	150	O	drowsiness
19	151	151	O	,
20	153	155	O	and
21	157	165	B-T	increased
22	167	177	I-T	psychomotor
23	179	188	I-T	impairment
24	190	193	O	sSee
25	195	211	O	Contraindications
26	214	214	O	4
27	217	219	O	and
28	221	228	O	Clinical
29	230	241	O	Pharmacology
30	244	247	O	12.3
31	252	258	O	Because
32	260	261	O	of
33	263	271	O	potential
34	273	276	O	drug
35	278	289	O	interactions
36	290	290	O	,
37	292	302	O	concomitant
38	304	306	O	use
39	308	309	O	of
40	311	318	O	XXXXXXXX
41	320	323	O	with
42	325	329	O	other
43	331	336	B-U	CYP1A2
44	338	347	I-U	inhibitors
45	348	348	O	,
46	350	353	O	such
47	355	356	O	as
48	358	365	B-U	zileuton
49	366	366	O	,
50	368	383	B-U	fluoroquinolones
51	385	389	O	other
52	391	394	O	than
53	396	401	B-U	strong
54	403	408	I-U	CYP1A2
55	410	419	I-U	inhibitors
56	422	426	O	which
57	428	430	O	are
58	432	446	B-T	contraindicated
59	448	448	O	,
60	450	463	O	antiarrythmics
61	466	475	O	amiodarone
62	476	476	O	,
63	478	487	B-U	mexiletine
64	488	488	O	,
65	490	500	B-U	propafenone
66	501	501	O	,
67	503	505	O	and
68	507	515	B-U	verapamil
69	517	517	O	,
70	519	528	O	cimetidine
71	529	529	O	,
72	531	540	B-U	famotidine
73	541	541	O	,
74	543	546	O	oral
75	548	561	O	contraceptives
76	562	562	O	,
77	564	572	B-U	acyclovir
78	573	573	O	,
79	575	577	O	and
80	579	589	B-U	ticlopidine
81	592	597	O	should
82	599	600	O	be
83	602	608	B-T	avoided
K/9:C54358

Zanaflex	3242	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	23
Changes in pharmacokinetics of tizanidine when administered with fluvoxamine resulted in significantly decreased blood pressure, increased drowsiness, and increased psychomotor impairment.
1	0	6	O	Changes
2	8	9	O	in
3	11	26	O	pharmacokinetics
4	28	29	O	of
5	31	40	O	XXXXXXXX
6	42	45	O	when
7	47	58	O	administered
8	60	63	O	with
9	65	75	B-D	fluvoxamine
10	77	84	O	resulted
11	86	87	O	in
12	89	101	O	significantly
13	103	111	B-E	decreased
14	113	117	I-E	blood
15	119	126	I-E	pressure
16	127	127	O	,
17	129	137	O	increased
18	139	148	B-E	drowsiness
19	149	149	O	,
20	151	153	O	and
21	155	163	B-E	increased
22	165	175	I-E	psychomotor
23	177	186	I-E	impairment
D/9:13:1 D/9:18:1 D/9:21:1

Zanaflex	3243	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	8
Concomitant use of ciprofoxacin and Zanaflex is contraindicated.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	30	O	ciprofoxacin
5	32	34	O	and
6	36	43	O	XXXXXXXX
7	45	46	O	is
8	48	62	B-T	contraindicated
NULL

Zanaflex	3244	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	8
Concomitant use of fluvoxamine and Zanaflex is contraindicated.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	29	B-U	fluvoxamine
5	31	33	O	and
6	35	42	O	XXXXXXXX
7	44	45	O	is
8	47	61	B-T	contraindicated
NULL

Zanaflex	3245	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	10
Concomitant use of Zanaflex with oral contraceptives is not recommended.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	17	O	of
4	19	26	O	XXXXXXXX
5	28	31	O	with
6	33	36	B-U	oral
7	38	51	I-U	contraceptives
8	53	54	O	is
9	56	58	B-T	not
10	60	70	I-T	recommended
NULL

Zanaflex	3246	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	31
However, if concomitant use is clinically necessary, initiate Zanaflex with a single 2 mg dose and increase in 2-4 mg steps daily based on patient response to therapy.
1	0	6	O	However
2	7	7	O	,
3	9	10	O	if
4	12	22	O	concomitant
5	24	26	O	use
6	28	29	O	is
7	31	40	O	clinically
8	42	50	O	necessary
9	51	51	O	,
10	53	60	O	initiate
11	62	69	O	XXXXXXXX
12	71	74	O	with
13	76	76	O	a
14	78	83	O	single
15	85	85	O	2
16	87	88	O	mg
17	90	93	O	dose
18	95	97	O	and
19	99	106	O	increase
20	108	109	O	in
21	111	111	O	2
22	113	113	O	4
23	115	116	O	mg
24	118	122	O	steps
25	124	128	O	daily
26	130	134	O	based
27	136	137	O	on
28	139	145	O	patient
29	147	154	O	response
30	156	157	O	to
31	159	165	O	therapy
NULL

Zanaflex	3247	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	19
If adverse reactions such as hypotension, bradycardia, or excessive drowsiness occur, reduce or discontinue Zanaflex therapy.
1	0	1	O	If
2	3	9	O	adverse
3	11	19	O	reactions
4	21	24	O	such
5	26	27	O	as
6	29	39	O	hypotension
7	40	40	O	,
8	42	52	B-E	bradycardia
9	53	53	O	,
10	55	56	O	or
11	58	66	O	excessive
12	68	77	O	drowsiness
13	79	83	O	occur
14	84	84	O	,
15	86	91	O	reduce
16	93	94	O	or
17	96	106	O	discontinue
18	108	115	O	XXXXXXXX
19	117	123	O	therapy
NULL

Zanaflex	3248	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	29
If their use is clinically necessary, therapy should be initiated with 2 mg dose and increased in 2-4 mg steps daily based on patient response to therapy.
1	0	1	O	If
2	3	7	O	their
3	9	11	O	use
4	13	14	O	is
5	16	25	O	clinically
6	27	35	O	necessary
7	36	36	O	,
8	38	44	O	therapy
9	46	51	O	should
10	53	54	O	be
11	56	64	O	initiated
12	66	69	O	with
13	71	71	O	2
14	73	74	O	mg
15	76	79	O	dose
16	81	83	O	and
17	85	93	O	increased
18	95	96	O	in
19	98	98	O	2
20	100	100	O	4
21	102	103	O	mg
22	105	109	O	steps
23	111	115	O	daily
24	117	121	O	based
25	123	124	O	on
26	126	132	O	patient
27	134	141	O	response
28	143	144	O	to
29	146	152	O	therapy
NULL

Zanaflex	3249	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	3
may be additive.
1	0	2	O	may
2	4	5	O	be
3	7	14	B-E	additive
NULL

Zanaflex	3250	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	10
The CNS depressant effects of Zanaflex and alcohol are additive.
1	0	2	B-E	The
2	4	6	I-E	CNS
3	8	17	I-E	depressant
4	19	25	I-E	effects
5	27	28	O	of
6	30	37	O	XXXXXXXX
7	39	41	O	and
8	43	49	B-D	alcohol
9	51	53	O	are
10	55	62	B-E	additive
D/8:1:1 D/8:10:1

Zanaflex	3251	34073-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	11
This was associated with an increase in adverse reactions of Zanaflex.
1	0	3	O	This
2	5	7	O	was
3	9	18	O	associated
4	20	23	O	with
5	25	26	O	an
6	28	35	B-E	increase
7	37	38	I-E	in
8	40	46	I-E	adverse
9	48	56	I-E	reactions
10	58	59	O	of
11	61	68	O	XXXXXXXX
NULL

Zanaflex	3252	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	9
, as clearance is reduced by more than 50%.
1	0	0	O	,
2	2	3	O	as
3	5	13	B-T	clearance
4	15	16	I-T	is
5	18	24	I-T	reduced
6	26	27	O	by
7	29	32	O	more
8	34	37	O	than
9	39	41	O	50%
NULL

Zanaflex	3253	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	52
Adverse reactions such as hypotension, bradycardia, or excessive drowsiness can occur when Zanaflex is taken with other CYP1A2 inhibitors, such as zileuton, fluoroquinolones other than ciprofloxacin (which is contraindicated), antiarrythmics (amiodarone, mexiletine, propafenone), cimetidine, famotidine, oral contraceptives, acyclovir, and ticlopidine ).
1	0	6	O	Adverse
2	8	16	O	reactions
3	18	21	O	such
4	23	24	O	as
5	26	36	B-E	hypotension
6	37	37	O	,
7	39	49	B-E	bradycardia
8	50	50	O	,
9	52	53	O	or
10	55	63	B-E	excessive
11	65	74	B-E	drowsiness
12	76	78	O	can
13	80	84	O	occur
14	86	89	O	when
15	91	98	O	XXXXXXXX
16	100	101	O	is
17	103	107	O	taken
18	109	112	O	with
19	114	118	O	other
20	120	125	B-D	CYP1A2
21	127	136	I-D	inhibitors
22	137	137	O	,
23	139	142	O	such
24	144	145	O	as
25	147	154	O	zileuton
26	155	155	O	,
27	157	172	O	fluoroquinolones
28	174	178	O	other
29	180	183	O	than
30	185	197	B-U	ciprofloxacin
31	200	204	O	which
32	206	207	O	is
33	209	223	B-T	contraindicated
34	225	225	O	,
35	227	240	O	antiarrythmics
36	243	252	O	amiodarone
37	253	253	O	,
38	255	264	O	mexiletine
39	265	265	O	,
40	267	277	O	propafenone
41	279	279	O	,
42	281	290	O	cimetidine
43	291	291	O	,
44	293	302	O	famotidine
45	303	303	O	,
46	305	308	O	oral
47	310	323	O	contraceptives
48	324	324	O	,
49	326	334	O	acyclovir
50	335	335	O	,
51	337	339	O	and
52	341	351	O	ticlopidine
D/20:5:1 D/20:7:1 D/20:10:1 D/20:11:1

Zanaflex	3254	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	21
Because of potential drug interactions, Zanaflex is contraindicated in patients taking potent CYP1A2 inhibitors, such as fluvoxamine or ciprofloxacin.
1	0	6	O	Because
2	8	9	O	of
3	11	19	O	potential
4	21	24	O	drug
5	26	37	O	interactions
6	38	38	O	,
7	40	47	O	XXXXXXXX
8	49	50	O	is
9	52	66	B-T	contraindicated
10	68	69	O	in
11	71	78	O	patients
12	80	85	O	taking
13	87	92	B-U	potent
14	94	99	I-U	CYP1A2
15	101	110	I-U	inhibitors
16	111	111	O	,
17	113	116	O	such
18	118	119	O	as
19	121	131	O	fluvoxamine
20	133	134	O	or
21	136	148	O	ciprofloxacin
NULL

Zanaflex	3255	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	29
Clinically significant hypotension (decreases in both systolic and diastolic pressure) has been reported with concomitant administration of either fluvoxamine or ciprofloxacin and single doses of 4 mg of Zanaflex.
1	0	9	O	Clinically
2	11	21	O	significant
3	23	33	B-E	hypotension
4	36	44	I-E	decreases
5	46	47	O	in
6	49	52	O	both
7	54	61	O	systolic
8	63	65	O	and
9	67	75	O	diastolic
10	77	84	O	pressure
11	87	89	O	has
12	91	94	O	been
13	96	103	O	reported
14	105	108	O	with
15	110	120	O	concomitant
16	122	135	O	administration
17	137	138	O	of
18	140	145	O	either
19	147	157	B-D	fluvoxamine
20	159	160	O	or
21	162	174	B-D	ciprofloxacin
22	176	178	O	and
23	180	185	O	single
24	187	191	O	doses
25	193	194	O	of
26	196	196	O	4
27	198	199	O	mg
28	201	202	O	of
29	204	211	O	XXXXXXXX
D/19:3:1 D/21:3:1

Zanaflex	3256	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	14
Concomitant use should be avoided unless the necessity for Zanaflex therapy is clinically evident.
1	0	10	O	Concomitant
2	12	14	O	use
3	16	21	O	should
4	23	24	O	be
5	26	32	O	avoided
6	34	39	O	unless
7	41	43	O	the
8	45	53	O	necessity
9	55	57	O	for
10	59	66	O	XXXXXXXX
11	68	74	O	therapy
12	76	77	O	is
13	79	88	O	clinically
14	90	96	O	evident
NULL

Zanaflex	3257	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	8
Consider discontinuing Zanaflex in patients who develop hallucinations.
1	0	7	O	Consider
2	9	21	B-T	discontinuing
3	23	30	O	XXXXXXXX
4	32	33	O	in
5	35	42	O	patients
6	44	46	O	who
7	48	54	O	develop
8	56	69	O	hallucinations
NULL

Zanaflex	3258	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	22
Formed, visual hallucinations or delusions have been reported in 5 of 170 patients (3%) in two North American controlled clinical studies.
1	0	5	O	Formed
2	6	6	O	,
3	8	13	O	visual
4	15	28	O	hallucinations
5	30	31	O	or
6	33	41	O	delusions
7	43	46	O	have
8	48	51	O	been
9	53	60	O	reported
10	62	63	O	in
11	65	65	O	5
12	67	68	O	of
13	70	72	O	170
14	74	81	O	patients
15	84	85	O	3%
16	88	89	O	in
17	91	93	O	two
18	95	99	O	North
19	101	108	O	American
20	110	119	O	controlled
21	121	128	O	clinical
22	130	136	O	studies
NULL

Zanaflex	3259	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	101
Hypotension: monitor for signs and symptoms of hypotension, in particular in patients receiving concurrent antihypertensives; Zanaflex should not be used with other A 2-adrenergic agonists ( 5.1, 7.7) Risk of liver injury: monitor ALTs; discontinue Zanaflex if liver injury occurs ( 5.2) Sedation: Zanaflex may interfere with everyday activities; sedative effects of Zanaflex, alcohol, and other CNS depressants are additive ( 5.3, 7.5, 7.6) Hallucinations: consider discontinuation of Zanaflex ( 5.4) Less potent inhibitors of CYP1A2: may cause hypotension, bradycardia, or excessive drowsiness, use caution if Zanaflex is used with less potent inhibitors of CYP1A2, e.g., zileuton, other fluoroquinolones, antiarrythmics , cimetidine, famotidine, oral contraceptives, acyclovir, and ticlopidine ( 5.5, 7.3, 12.3) Renal impairment (creatinine clearance < 25 mL/min): use Zanaflex with caution, and monitor closely for dry mouth, somnolence, asthenia and dizziness as indicators of potential overdose ( 5.7) Tizanidine is an A 2-adrenergic agonist that can produce hypotension.
1	0	10	B-E	Hypotension
2	11	11	O	:
3	13	19	B-T	monitor
4	21	23	O	for
5	25	29	O	signs
6	31	33	O	and
7	35	42	B-E	symptoms
8	44	45	O	of
9	47	57	B-E	hypotension
10	58	58	O	,
11	60	61	O	in
12	63	72	O	particular
13	74	75	O	in
14	77	84	O	patients
15	86	94	O	receiving
16	96	105	O	concurrent
17	107	123	B-D	antihypertensives
18	126	133	O	XXXXXXXX
19	135	140	O	should
20	142	144	O	not
21	146	147	O	be
22	149	152	O	used
23	154	157	O	with
24	159	163	O	other
25	165	165	O	A
26	167	167	B-D	2
27	169	178	I-D	adrenergic
28	180	187	I-D	agonists
29	191	193	O	5.1
30	194	194	O	,
31	196	198	O	7.7
32	201	204	O	Risk
33	206	207	O	of
34	209	213	O	liver
35	215	220	O	injury
36	221	221	O	:
37	223	229	O	monitor
38	231	234	O	ALTs
39	237	247	O	discontinue
40	249	256	O	XXXXXXXX
41	258	259	O	if
42	261	265	O	liver
43	267	272	O	injury
44	274	279	O	occurs
45	283	285	O	5.2
46	288	295	O	Sedation
47	296	296	O	:
48	298	305	O	XXXXXXXX
49	307	309	O	may
50	311	319	O	interfere
51	321	324	O	with
52	326	333	O	everyday
53	335	344	B-E	activities
54	347	354	I-E	sedative
55	356	362	I-E	effects
56	364	365	O	of
57	367	374	O	XXXXXXXX
58	375	375	O	,
59	377	383	B-D	alcohol
60	384	384	O	,
61	386	388	O	and
62	390	394	O	other
63	396	398	B-D	CNS
64	400	410	I-D	depressants
65	412	414	O	are
66	416	423	B-E	additive
67	427	429	O	5.3
68	430	430	O	,
69	432	434	O	7.5
70	435	435	O	,
71	437	439	O	7.6
72	442	455	O	Hallucinations
73	456	456	O	:
74	458	465	O	consider
75	467	481	B-T	discontinuation
76	483	484	O	of
77	486	493	O	XXXXXXXX
78	497	499	O	5.4
79	502	505	O	Less
80	507	512	O	potent
81	514	523	O	inhibitors
82	525	526	O	of
83	528	533	O	CYP1A2
84	534	534	O	:
85	536	538	O	may
86	540	544	O	cause
87	546	556	B-E	hypotension
88	557	557	O	,
89	559	569	B-E	bradycardia
90	570	570	O	,
91	572	573	O	or
92	575	583	B-E	excessive
93	585	594	I-E	drowsiness
94	595	595	O	,
95	597	599	B-T	use
96	601	607	B-T	caution
97	609	610	O	if
98	612	619	O	XXXXXXXX
99	621	622	O	is
100	624	627	O	used
101	629	632	O	with
D/17:1:1 D/17:7:1 D/17:9:1 D/17:53:1 D/17:66:1 D/17:87:1 D/17:89:1 D/17:92:1 D/26:1:1 D/26:7:1 D/26:9:1 D/26:53:1 D/26:66:1 D/26:87:1 D/26:89:1 D/26:92:1 D/59:1:1 D/59:7:1 D/59:9:1 D/59:53:1 D/59:66:1 D/59:87:1 D/59:89:1 D/59:92:1 D/63:1:1 D/63:7:1 D/63:9:1 D/63:53:1 D/63:66:1 D/63:87:1 D/63:89:1 D/63:92:1

Zanaflex	3260	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	15
If higher doses are required, individual doses rather than dosing frequency should be increased.
1	0	1	O	If
2	3	8	O	higher
3	10	14	O	doses
4	16	18	O	are
5	20	27	O	required
6	28	28	O	,
7	30	39	O	individual
8	41	45	O	doses
9	47	52	O	rather
10	54	57	O	than
11	59	64	O	dosing
12	66	74	O	frequency
13	76	81	O	should
14	83	84	O	be
15	86	94	O	increased
NULL

Zanaflex	3261	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	22
In addition, patients moving from a supine to fixed upright position may be at increased risk for hypotension and orthostatic effects.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	20	O	patients
5	22	27	O	moving
6	29	32	O	from
7	34	34	O	a
8	36	41	O	supine
9	43	44	O	to
10	46	50	O	fixed
11	52	58	O	upright
12	60	67	O	position
13	69	71	O	may
14	73	74	O	be
15	76	77	O	at
16	79	87	O	increased
17	89	92	O	risk
18	94	96	O	for
19	98	108	O	hypotension
20	110	112	O	and
21	114	124	O	orthostatic
22	126	132	O	effects
NULL

Zanaflex	3262	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	33
In the multiple dose studies, the prevalence of patients with sedation peaked following the first week of titration and then remained stable for the duration of the maintenance phase of the study.
1	0	1	O	In
2	3	5	O	the
3	7	14	O	multiple
4	16	19	O	dose
5	21	27	O	studies
6	28	28	O	,
7	30	32	O	the
8	34	43	O	prevalence
9	45	46	O	of
10	48	55	O	patients
11	57	60	O	with
12	62	69	O	sedation
13	71	76	O	peaked
14	78	86	O	following
15	88	90	O	the
16	92	96	O	first
17	98	101	O	week
18	103	104	O	of
19	106	114	O	titration
20	116	118	O	and
21	120	123	O	then
22	125	132	O	remained
23	134	139	O	stable
24	141	143	O	for
25	145	147	O	the
26	149	156	O	duration
27	158	159	O	of
28	161	163	O	the
29	165	175	O	maintenance
30	177	181	O	phase
31	183	184	O	of
32	186	188	O	the
33	190	194	O	study
NULL

Zanaflex	3263	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	13
In these patients, during titration, the individual doses should be reduced.
1	0	1	O	In
2	3	7	O	these
3	9	16	O	patients
4	17	17	O	,
5	19	24	O	during
6	26	34	O	titration
7	35	35	O	,
8	37	39	O	the
9	41	50	O	individual
10	52	56	O	doses
11	58	63	O	should
12	65	66	O	be
13	68	74	O	reduced
NULL

Zanaflex	3264	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	14
It is not recommended that Zanaflex be used with other A 2-adrenergic agonists.
1	0	1	O	It
2	3	4	O	is
3	6	8	B-T	not
4	10	20	I-T	recommended
5	22	25	O	that
6	27	34	O	XXXXXXXX
7	36	37	O	be
8	39	42	O	used
9	44	47	O	with
10	49	53	O	other
11	55	55	O	A
12	57	57	B-U	2
13	59	68	I-U	adrenergic
14	70	77	I-U	agonists
NULL

Zanaflex	3265	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	13
Monitor for hypotension when Zanaflex is used in patients receiving concurrent antihypertensive therapy.
1	0	6	B-T	Monitor
2	8	10	O	for
3	12	22	O	hypotension
4	24	27	O	when
5	29	36	O	XXXXXXXX
6	38	39	O	is
7	41	44	O	used
8	46	47	O	in
9	49	56	O	patients
10	58	66	O	receiving
11	68	77	O	concurrent
12	79	94	B-U	antihypertensive
13	96	102	O	therapy
NULL

Zanaflex	3266	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	14
Monitor patients who take Zanaflex with another CNS depressant for symptoms of excess sedation.
1	0	6	O	Monitor
2	8	15	O	patients
3	17	19	O	who
4	21	24	O	take
5	26	33	O	XXXXXXXX
6	35	38	O	with
7	40	46	O	another
8	48	50	B-D	CNS
9	52	61	I-D	depressant
10	63	65	O	for
11	67	74	O	symptoms
12	76	77	O	of
13	79	84	B-E	excess
14	86	93	I-E	sedation
D/8:13:1

Zanaflex	3267	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	23
Monitoring of aminotransferase levels is recommended for baseline and 1 month after maximum dose is achieved, or if hepatic injury is suspected.
1	0	9	B-T	Monitoring
2	11	12	O	of
3	14	29	O	aminotransferase
4	31	36	O	levels
5	38	39	O	is
6	41	51	O	recommended
7	53	55	O	for
8	57	64	O	baseline
9	66	68	O	and
10	70	70	O	1
11	72	76	O	month
12	78	82	O	after
13	84	90	O	maximum
14	92	95	O	dose
15	97	98	O	is
16	100	107	O	achieved
17	108	108	O	,
18	110	111	O	or
19	113	114	O	if
20	116	122	O	hepatic
21	124	129	O	injury
22	131	132	O	is
23	134	142	O	suspected
NULL

Zanaflex	3268	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	11
Most of the patients were aware that the events were unreal.
1	0	3	O	Most
2	5	6	O	of
3	8	10	O	the
4	12	19	O	patients
5	21	24	O	were
6	26	30	O	aware
7	32	35	O	that
8	37	39	O	the
9	41	46	O	events
10	48	51	O	were
11	53	58	O	unreal
NULL

Zanaflex	3269	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	18
One patient among these 5 continued to have problems for at least 2 weeks following discontinuation of tizanidine.
1	0	2	O	One
2	4	10	O	patient
3	12	16	O	among
4	18	22	O	these
5	24	24	O	5
6	26	34	O	continued
7	36	37	O	to
8	39	42	O	have
9	44	51	O	problems
10	53	55	O	for
11	57	58	O	at
12	60	64	O	least
13	66	66	O	2
14	68	72	O	weeks
15	74	82	O	following
16	84	98	O	discontinuation
17	100	101	O	of
18	103	112	O	XXXXXXXX
NULL

Zanaflex	3270	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	9
One patient developed psychosis in association with the hallucinations.
1	0	2	O	One
2	4	10	O	patient
3	12	20	O	developed
4	22	30	B-E	psychosis
5	32	33	B-T	in
6	35	45	I-T	association
7	47	50	O	with
8	52	54	O	the
9	56	69	B-E	hallucinations
NULL

Zanaflex	3271	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	29
Patients should be informed of the signs and symptoms of severe allergic reactions and instructed to discontinue Zanaflex and seek immediate medical care should these signs and symptoms occur.
1	0	7	O	Patients
2	9	14	O	should
3	16	17	O	be
4	19	26	O	informed
5	28	29	O	of
6	31	33	O	the
7	35	39	O	signs
8	41	43	O	and
9	45	52	O	symptoms
10	54	55	O	of
11	57	62	O	severe
12	64	71	O	allergic
13	73	81	O	reactions
14	83	85	O	and
15	87	96	O	instructed
16	98	99	O	to
17	101	111	O	discontinue
18	113	120	O	XXXXXXXX
19	122	124	O	and
20	126	129	O	seek
21	131	139	O	immediate
22	141	147	O	medical
23	149	152	O	care
24	154	159	O	should
25	161	165	O	these
26	167	171	O	signs
27	173	175	O	and
28	177	184	O	symptoms
29	186	190	O	occur
NULL

Zanaflex	3272	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	19
Signs and symptoms including respiratory compromise, urticaria, and angioedema of the throat and tongue have been reported.
1	0	4	O	Signs
2	6	8	O	and
3	10	17	O	symptoms
4	19	27	O	including
5	29	39	O	respiratory
6	41	50	O	compromise
7	51	51	O	,
8	53	61	O	urticaria
9	62	62	O	,
10	64	66	O	and
11	68	77	O	angioedema
12	79	80	O	of
13	82	84	O	the
14	86	91	O	throat
15	93	95	O	and
16	97	102	O	tongue
17	104	107	O	have
18	109	112	O	been
19	114	121	O	reported
NULL

Zanaflex	3273	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	9
Syncope has been reported in the post marketing setting.
1	0	6	B-E	Syncope
2	8	10	O	has
3	12	15	O	been
4	17	24	O	reported
5	26	27	O	in
6	29	31	O	the
7	33	36	O	post
8	38	46	O	marketing
9	48	54	O	setting
NULL

Zanaflex	3274	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	28
The chance of significant hypotension may possibly be minimized by titration of the dose and by focusing attention on signs and symptoms of hypotension prior to dose advancement.
1	0	2	O	The
2	4	9	O	chance
3	11	12	O	of
4	14	24	O	significant
5	26	36	B-E	hypotension
6	38	40	O	may
7	42	49	O	possibly
8	51	52	O	be
9	54	62	O	minimized
10	64	65	O	by
11	67	75	B-T	titration
12	77	78	O	of
13	80	82	O	the
14	84	87	O	dose
15	89	91	O	and
16	93	94	O	by
17	96	103	O	focusing
18	105	113	B-T	attention
19	115	116	O	on
20	118	122	O	signs
21	124	126	O	and
22	128	135	O	symptoms
23	137	138	O	of
24	140	150	B-E	hypotension
25	152	156	O	prior
26	158	159	O	to
27	161	164	O	dose
28	166	176	O	advancement
NULL

Zanaflex	3275	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	23
The CNS depressant effects of Zanaflex with alcohol and other CNS depressants (e.g., benzodiazepines, opioids, tricyclic antidepressants) may be additive.
1	0	2	B-E	The
2	4	6	I-E	CNS
3	8	17	I-E	depressant
4	19	25	I-E	effects
5	27	28	O	of
6	30	37	O	XXXXXXXX
7	39	42	O	with
8	44	50	B-D	alcohol
9	52	54	O	and
10	56	60	O	other
11	62	64	B-D	CNS
12	66	76	I-D	depressants
13	79	81	O	e.g
14	83	83	O	,
15	85	99	O	benzodiazepines
16	100	100	O	,
17	102	108	B-D	opioids
18	109	109	O	,
19	111	119	B-D	tricyclic
20	121	135	I-D	antidepressants
21	138	140	O	may
22	142	143	O	be
23	145	152	B-E	additive
D/8:1:1 D/8:23:1 D/11:1:1 D/11:23:1 D/17:1:1 D/17:23:1 D/19:1:1 D/19:23:1

Zanaflex	3276	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	19
Therefore, concomitant use of Zanaflex with fluvoxamine or with ciprofloxacin, potent inhibitors of CYP1A2, is contraindicated.
1	0	8	O	Therefore
2	9	9	O	,
3	11	21	O	concomitant
4	23	25	O	use
5	27	28	O	of
6	30	37	O	XXXXXXXX
7	39	42	O	with
8	44	54	B-U	fluvoxamine
9	56	57	O	or
10	59	62	O	with
11	64	76	B-U	ciprofloxacin
12	77	77	O	,
13	79	84	B-U	potent
14	86	95	I-U	inhibitors
15	97	98	I-U	of
16	100	105	O	CYP1A2
17	106	106	O	,
18	108	109	O	is
19	111	125	B-T	contraindicated
NULL

Zanaflex	3277	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	31
These patients should be monitored closely for the onset or increase in severity of the common adverse events (dry mouth, somnolence, asthenia and dizziness) as indicators of potential overdose.
1	0	4	O	These
2	6	13	O	patients
3	15	20	O	should
4	22	23	O	be
5	25	33	B-T	monitored
6	35	41	O	closely
7	43	45	O	for
8	47	49	O	the
9	51	55	O	onset
10	57	58	O	or
11	60	67	O	increase
12	69	70	O	in
13	72	79	O	severity
14	81	82	O	of
15	84	86	O	the
16	88	93	O	common
17	95	101	O	adverse
18	103	108	O	events
19	111	113	O	dry
20	115	119	O	mouth
21	120	120	O	,
22	122	131	O	somnolence
23	132	132	O	,
24	134	141	O	asthenia
25	143	145	O	and
26	147	155	B-E	dizziness
27	158	159	O	as
28	161	170	O	indicators
29	172	173	O	of
30	175	183	O	potential
31	185	192	O	overdose
NULL

Zanaflex	3278	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	53
To minimize the risk of these reactions, particularly in patients who have been receiving high doses (20 to 28 mg daily) for long periods of time (9 weeks or more) or who may be on concomitant treatment with narcotics, the dose should be decreased slowly (2 to 4 mg per day).
1	0	1	O	To
2	3	10	O	minimize
3	12	14	O	the
4	16	19	O	risk
5	21	22	O	of
6	24	28	O	these
7	30	38	O	reactions
8	39	39	O	,
9	41	52	O	particularly
10	54	55	O	in
11	57	64	O	patients
12	66	68	O	who
13	70	73	O	have
14	75	78	O	been
15	80	88	O	receiving
16	90	93	O	high
17	95	99	O	doses
18	102	103	O	20
19	105	106	O	to
20	108	109	O	28
21	111	112	O	mg
22	114	118	O	daily
23	121	123	O	for
24	125	128	O	long
25	130	136	O	periods
26	138	139	O	of
27	141	144	O	time
28	147	147	O	9
29	149	153	O	weeks
30	155	156	O	or
31	158	161	O	more
32	164	165	O	or
33	167	169	O	who
34	171	173	O	may
35	175	176	O	be
36	178	179	O	on
37	181	191	O	concomitant
38	193	201	O	treatment
39	203	206	O	with
40	208	216	B-K	narcotics
41	217	217	O	,
42	219	221	O	the
43	223	226	O	dose
44	228	233	O	should
45	235	236	O	be
46	238	246	O	decreased
47	248	253	B-T	slowly
48	256	256	O	2
49	258	259	O	to
50	261	261	O	4
51	263	264	O	mg
52	266	268	O	per
53	270	272	O	day
K/40:C54355

Zanaflex	3279	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	11
Withdrawal adverse reactions include rebound hypertension, tachycardia, and hypertonia.
1	0	9	O	Withdrawal
2	11	17	O	adverse
3	19	27	O	reactions
4	29	35	O	include
5	37	43	O	rebound
6	45	56	O	hypertension
7	57	57	O	,
8	59	69	O	tachycardia
9	70	70	O	,
10	72	74	O	and
11	76	85	B-E	hypertonia
NULL

Zanaflex	3280	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	4
Zanaflex can cause anaphylaxis.
1	0	7	O	XXXXXXXX
2	9	11	O	can
3	13	17	O	cause
4	19	29	O	anaphylaxis
NULL

Zanaflex	3281	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	11
Zanaflex can cause sedation, which may interfere with everyday activity.
1	0	7	O	XXXXXXXX
2	9	11	O	can
3	13	17	O	cause
4	19	26	O	sedation
5	27	27	O	,
6	29	33	O	which
7	35	37	O	may
8	39	47	B-E	interfere
9	49	52	I-E	with
10	54	61	O	everyday
11	63	70	B-E	activity
NULL

Zanaflex	3282	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	6
Zanaflex may cause hepatocellular liver injury.
1	0	7	O	XXXXXXXX
2	9	11	O	may
3	13	17	O	cause
4	19	32	O	hepatocellular
5	34	38	O	liver
6	40	45	O	injury
NULL

Zanaflex	3283	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	12
Zanaflex should be used with caution in patients with any hepatic impairment..
1	0	7	O	XXXXXXXX
2	9	14	O	should
3	16	17	O	be
4	19	22	B-T	used
5	24	27	O	with
6	29	35	B-T	caution
7	37	38	O	in
8	40	47	O	patients
9	49	52	O	with
10	54	56	O	any
11	58	64	O	hepatic
12	66	75	O	impairment
NULL

Zanaflex	3284	43685-7	413ee468-c7e8-4283-8e2b-b573dcc1f607	7
Zanaflex use has been associated with hallucinations.
1	0	7	O	XXXXXXXX
2	9	11	O	use
3	13	15	O	has
4	17	20	O	been
5	22	31	O	associated
6	33	36	O	with
7	38	51	O	hallucinations
NULL

