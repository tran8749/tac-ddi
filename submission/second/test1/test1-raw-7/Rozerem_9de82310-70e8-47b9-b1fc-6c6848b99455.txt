Rozerem	5432	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	13
Adult dose: 8 mg taken within 30 minutes of going to bed.
1	0	4	B-T	Adult
2	6	9	O	dose
3	10	10	O	:
4	12	12	O	8
5	14	15	O	mg
6	17	21	O	taken
7	23	28	O	within
8	30	31	O	30
9	33	39	O	minutes
10	41	42	O	of
11	44	48	O	going
12	50	51	O	to
13	53	55	O	bed
NULL

Rozerem	5433	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	16
It is recommended that ROZEREM not be taken with or immediately after a high-fat meal.
1	0	1	O	It
2	3	4	O	is
3	6	16	O	recommended
4	18	21	O	that
5	23	29	O	XXXXXXXX
6	31	33	O	not
7	35	36	O	be
8	38	42	O	taken
9	44	47	O	with
10	49	50	O	or
11	52	62	O	immediately
12	64	68	O	after
13	70	70	O	a
14	72	75	O	high
15	77	79	O	fat
16	81	84	O	meal
NULL

Rozerem	5434	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	10
ROZEREM is not recommended in patients with severe hepatic impairment.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	13	O	not
4	15	25	O	recommended
5	27	28	O	in
6	30	37	O	patients
7	39	42	O	with
8	44	49	O	severe
9	51	57	O	hepatic
10	59	68	O	impairment
NULL

Rozerem	5435	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	13
ROZEREM should be used with caution in patients taking other CYP1A2 inhibiting drugs.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	16	O	be
4	18	21	O	used
5	23	26	O	with
6	28	34	O	caution
7	36	37	O	in
8	39	46	O	patients
9	48	53	O	taking
10	55	59	O	other
11	61	66	O	CYP1A2
12	68	77	O	inhibiting
13	79	83	O	drugs
NULL

Rozerem	5436	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	12
ROZEREM should be used with caution in patients with moderate hepatic impairment.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	16	O	be
4	18	21	O	used
5	23	26	O	with
6	28	34	O	caution
7	36	37	O	in
8	39	46	O	patients
9	48	51	O	with
10	53	60	O	moderate
11	62	68	O	hepatic
12	70	79	O	impairment
NULL

Rozerem	5437	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	12
Should not be taken with or immediately after a high-fat meal.
1	0	5	O	Should
2	7	9	O	not
3	11	12	B-T	be
4	14	18	O	taken
5	20	23	O	with
6	25	26	O	or
7	28	38	O	immediately
8	40	44	O	after
9	46	46	O	a
10	48	51	O	high
11	53	55	O	fat
12	57	60	O	meal
NULL

Rozerem	5438	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	11
The total ROZEREM dose should not exceed 8 mg per day.
1	0	2	O	The
2	4	8	O	total
3	10	16	O	XXXXXXXX
4	18	21	O	dose
5	23	28	O	should
6	30	32	O	not
7	34	39	O	exceed
8	41	41	O	8
9	43	44	O	mg
10	46	48	O	per
11	50	52	O	day
NULL

Rozerem	5439	34068-7	9de82310-70e8-47b9-b1fc-6c6848b99455	25
Total daily dose should not exceed 8 mg. (2.1) The recommended dose of ROZEREM is 8 mg taken within 30 minutes of going to bed.
1	0	4	O	Total
2	6	10	O	daily
3	12	15	O	dose
4	17	22	O	should
5	24	26	O	not
6	28	33	O	exceed
7	35	35	O	8
8	37	38	O	mg
9	42	44	O	2.1
10	47	49	O	The
11	51	61	O	recommended
12	63	66	O	dose
13	68	69	O	of
14	71	77	O	XXXXXXXX
15	79	80	O	is
16	82	82	O	8
17	84	85	O	mg
18	87	91	O	taken
19	93	98	O	within
20	100	101	O	30
21	103	109	O	minutes
22	111	112	O	of
23	114	118	O	going
24	120	121	O	to
25	123	125	O	bed
NULL

Rozerem	5440	34070-3	9de82310-70e8-47b9-b1fc-6c6848b99455	16
Fluvoxamine (strong CYP1A2 inhibitor): Increases AUC for ramelteon and should not be used in combination.
1	0	10	O	Fluvoxamine
2	13	18	O	strong
3	20	25	O	CYP1A2
4	27	35	O	inhibitor
5	37	37	O	:
6	39	47	B-T	Increases
7	49	51	I-T	AUC
8	53	55	O	for
9	57	65	O	XXXXXXXX
10	67	69	O	and
11	71	76	O	should
12	78	80	O	not
13	82	83	B-T	be
14	85	88	O	used
15	90	91	O	in
16	93	103	O	combination
NULL

Rozerem	5441	34070-3	9de82310-70e8-47b9-b1fc-6c6848b99455	6
History of angioedema while taking ROZEREM.
1	0	6	O	History
2	8	9	O	of
3	11	20	O	angioedema
4	22	26	O	while
5	28	33	O	taking
6	35	41	O	XXXXXXXX
NULL

Rozerem	5442	34070-3	9de82310-70e8-47b9-b1fc-6c6848b99455	10
Patients should not take ROZEREM in conjunction with fluvoxamine (Luvox).
1	0	7	O	Patients
2	9	14	O	should
3	16	18	B-T	not
4	20	23	I-T	take
5	25	31	O	XXXXXXXX
6	33	34	O	in
7	36	46	O	conjunction
8	48	51	O	with
9	53	63	B-U	fluvoxamine
10	66	70	I-U	Luvox
NULL

Rozerem	5443	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	9
Alcohol by itself impairs performance and can cause sleepiness.
1	0	6	B-K	Alcohol
2	8	9	O	by
3	11	16	O	itself
4	18	24	O	impairs
5	26	36	O	performance
6	38	40	O	and
7	42	44	O	can
8	46	50	O	cause
9	52	61	O	sleepiness
K/1:C54357

Rozerem	5444	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	12
Alcohol: Causes additive psychomotor impairment; should not be used in combination.
1	0	6	B-K	Alcohol
2	7	7	O	:
3	9	14	O	Causes
4	16	23	O	additive
5	25	35	O	psychomotor
6	37	46	O	impairment
7	49	54	O	should
8	56	58	O	not
9	60	61	O	be
10	63	66	O	used
11	68	69	O	in
12	71	81	O	combination
K/1:C54357

Rozerem	5445	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	17
Donepezil increases systemic exposure of ramelteon; patients should be closely monitored when ramelteon is coadministered with donepezil.
1	0	8	B-K	Donepezil
2	10	18	B-T	increases
3	20	27	I-T	systemic
4	29	36	I-T	exposure
5	38	39	O	of
6	41	49	O	XXXXXXXX
7	52	59	O	patients
8	61	66	O	should
9	68	69	O	be
10	71	77	O	closely
11	79	87	B-T	monitored
12	89	92	O	when
13	94	102	O	XXXXXXXX
14	104	105	O	is
15	107	120	O	coadministered
16	122	125	O	with
17	127	135	B-K	donepezil
K/1:C54357 K/17:C54357

Rozerem	5446	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	23
Donepezil: The AUC0-inf and Cmax of ramelteon increased by approximately 100% and 87%, respectively upon coadministration of donepezil with ROZEREM.
1	0	8	B-K	Donepezil
2	9	9	O	:
3	11	13	O	The
4	15	18	B-T	AUC0
5	20	22	I-T	inf
6	24	26	O	and
7	28	31	O	Cmax
8	33	34	O	of
9	36	44	O	XXXXXXXX
10	46	54	O	increased
11	56	57	O	by
12	59	71	O	approximately
13	73	76	O	100%
14	78	80	O	and
15	82	84	O	87%
16	85	85	O	,
17	87	98	O	respectively
18	100	103	O	upon
19	105	120	O	coadministration
20	122	123	O	of
21	125	133	B-K	donepezil
22	135	138	O	with
23	140	146	O	XXXXXXXX
K/1:C54602 K/21:C54602

Rozerem	5447	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	17
Doxepin increases systemic exposure of ramelteon; patients should be closely monitored when ramelteon is coadministered with doxepin.
1	0	6	B-K	Doxepin
2	8	16	B-T	increases
3	18	25	I-T	systemic
4	27	34	I-T	exposure
5	36	37	O	of
6	39	47	O	XXXXXXXX
7	50	57	O	patients
8	59	64	O	should
9	66	67	O	be
10	69	75	O	closely
11	77	85	B-T	monitored
12	87	90	O	when
13	92	100	O	XXXXXXXX
14	102	103	O	is
15	105	118	O	coadministered
16	120	123	O	with
17	125	131	B-K	doxepin
K/1:C54357 K/17:C54357

Rozerem	5448	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	24
Doxepin: The AUC0-inf and Cmax of ramelteon increased by approximately 66% and 69%, respectively, upon coadministration of doxepin with ROZEREM.
1	0	6	B-K	Doxepin
2	7	7	O	:
3	9	11	O	The
4	13	16	O	AUC0
5	18	20	O	inf
6	22	24	O	and
7	26	29	O	Cmax
8	31	32	O	of
9	34	42	O	XXXXXXXX
10	44	52	O	increased
11	54	55	O	by
12	57	69	O	approximately
13	71	73	O	66%
14	75	77	O	and
15	79	81	O	69%
16	82	82	O	,
17	84	95	O	respectively
18	96	96	O	,
19	98	101	O	upon
20	103	118	O	coadministration
21	120	121	O	of
22	123	129	B-K	doxepin
23	131	134	O	with
24	136	142	O	XXXXXXXX
K/1:C54602 K/22:C54602

Rozerem	5449	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	18
Efficacy may be reduced when ROZEREM is used in combination with strong CYP enzyme inducers such as rifampin.
1	0	7	B-T	Efficacy
2	9	11	O	may
3	13	14	O	be
4	16	22	O	reduced
5	24	27	O	when
6	29	35	O	XXXXXXXX
7	37	38	O	is
8	40	43	O	used
9	45	46	O	in
10	48	58	O	combination
11	60	63	O	with
12	65	70	B-K	strong
13	72	74	I-K	CYP
14	76	81	I-K	enzyme
15	83	90	I-K	inducers
16	92	95	O	such
17	97	98	O	as
18	100	107	O	rifampin
K/12:C54356

Rozerem	5450	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	13
Fluconazole (strong CYP2C9 inhibitor): Increases systemic exposure of ramelteon; administer with caution.
1	0	10	B-K	Fluconazole
2	13	18	B-K	strong
3	20	25	O	CYP2C9
4	27	35	B-K	inhibitor
5	37	37	O	:
6	39	47	B-T	Increases
7	49	56	I-T	systemic
8	58	65	I-T	exposure
9	67	68	O	of
10	70	78	O	XXXXXXXX
11	81	90	B-T	administer
12	92	95	O	with
13	97	103	B-T	caution
K/1:C54355 K/2:C54355 K/4:C54355

Rozerem	5451	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	23
Fluconazole (strong CYP2C9 inhibitor): The AUC0-inf and Cmax of ramelteon was increased by approximately 150% when ROZEREM was coadministered with fluconazole.
1	0	10	B-K	Fluconazole
2	13	18	O	strong
3	20	25	O	CYP2C9
4	27	35	O	inhibitor
5	37	37	O	:
6	39	41	O	The
7	43	46	B-T	AUC0
8	48	50	I-T	inf
9	52	54	O	and
10	56	59	O	Cmax
11	61	62	O	of
12	64	72	O	XXXXXXXX
13	74	76	O	was
14	78	86	B-T	increased
15	88	89	O	by
16	91	103	O	approximately
17	105	108	O	150%
18	110	113	O	when
19	115	121	O	XXXXXXXX
20	123	125	O	was
21	127	140	O	coadministered
22	142	145	O	with
23	147	157	B-K	fluconazole
K/1:C54602 K/23:C54602

Rozerem	5452	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	33
Fluvoxamine (strong CYP1A2 inhibitor): AUC0-inf for ramelteon increased approximately 190-fold, and the Cmax increased approximately 70-fold upon coadministration of fluvoxamine and ROZEREM, compared to ROZEREM administered alone.
1	0	10	O	Fluvoxamine
2	13	18	O	strong
3	20	25	O	CYP1A2
4	27	35	O	inhibitor
5	37	37	O	:
6	39	42	O	AUC0
7	44	46	O	inf
8	48	50	O	for
9	52	60	O	XXXXXXXX
10	62	70	B-T	increased
11	72	84	O	approximately
12	86	88	O	190
13	90	93	O	fold
14	94	94	O	,
15	96	98	O	and
16	100	102	O	the
17	104	107	O	Cmax
18	109	117	B-T	increased
19	119	131	O	approximately
20	133	134	O	70
21	136	139	O	fold
22	141	144	O	upon
23	146	161	O	coadministration
24	163	164	O	of
25	166	176	B-K	fluvoxamine
26	178	180	O	and
27	182	188	O	XXXXXXXX
28	189	189	O	,
29	191	198	O	compared
30	200	201	O	to
31	203	209	O	XXXXXXXX
32	211	222	O	administered
33	224	228	O	alone
K/25:C54602

Rozerem	5453	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	12
Ketoconazole (strong CYP3A4 inhibitor): Increases AUC for ramelteon; administer with caution.
1	0	11	B-K	Ketoconazole
2	14	19	I-K	strong
3	21	26	I-K	CYP3A4
4	28	36	I-K	inhibitor
5	38	38	O	:
6	40	48	B-T	Increases
7	50	52	I-T	AUC
8	54	56	O	for
9	58	66	O	XXXXXXXX
10	69	78	B-T	administer
11	80	83	O	with
12	85	91	B-T	caution
K/1:C54355

Rozerem	5454	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	24
Ketoconazole (strong CYP3A4 inhibitor): The AUC0-inf and Cmax of ramelteon increased by approximately 84% and 36% upon coadministration of ketoconazole with ROZEREM.
1	0	11	B-K	Ketoconazole
2	14	19	I-K	strong
3	21	26	I-K	CYP3A4
4	28	36	I-K	inhibitor
5	38	38	O	:
6	40	42	B-T	The
7	44	47	I-T	AUC0
8	49	51	I-T	inf
9	53	55	O	and
10	57	60	O	Cmax
11	62	63	O	of
12	65	73	O	XXXXXXXX
13	75	83	O	increased
14	85	86	O	by
15	88	100	O	approximately
16	102	104	O	84%
17	106	108	O	and
18	110	112	O	36%
19	114	117	O	upon
20	119	134	O	coadministration
21	136	137	O	of
22	139	150	B-K	ketoconazole
23	152	155	O	with
24	157	163	O	XXXXXXXX
K/1:C54602 K/22:C54602

Rozerem	5455	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	10
Other less strong CYP1A2 inhibitors have not been adequately studied.
1	0	4	O	Other
2	6	9	O	less
3	11	16	O	strong
4	18	23	O	CYP1A2
5	25	34	O	inhibitors
6	36	39	O	have
7	41	43	O	not
8	45	48	O	been
9	50	59	O	adequately
10	61	67	O	studied
NULL

Rozerem	5456	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	11
Patients should be closely monitored when ROZEREM is coadministered with donepezil.
1	0	7	O	Patients
2	9	14	O	should
3	16	17	O	be
4	19	25	O	closely
5	27	35	B-T	monitored
6	37	40	O	when
7	42	48	O	XXXXXXXX
8	50	51	O	is
9	53	66	O	coadministered
10	68	71	O	with
11	73	81	B-U	donepezil
NULL

Rozerem	5457	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	11
Patients should be closely monitored when ROZEREM is coadministered with doxepin.
1	0	7	O	Patients
2	9	14	O	should
3	16	17	O	be
4	19	25	O	closely
5	27	35	B-T	monitored
6	37	40	O	when
7	42	48	O	XXXXXXXX
8	50	51	O	is
9	53	66	O	coadministered
10	68	71	O	with
11	73	79	B-U	doxepin
NULL

Rozerem	5458	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	29
Rifampin (strong CYP enzyme inducer): Administration of multiple doses of rifampin resulted in a mean decrease of approximately 80% in total exposure to ramelteon and metabolite M-II.
1	0	7	B-K	Rifampin
2	10	15	I-K	strong
3	17	19	I-K	CYP
4	21	26	I-K	enzyme
5	28	34	I-K	inducer
6	36	36	O	:
7	38	51	O	Administration
8	53	54	O	of
9	56	63	O	multiple
10	65	69	O	doses
11	71	72	O	of
12	74	81	B-K	rifampin
13	83	90	O	resulted
14	92	93	O	in
15	95	95	O	a
16	97	100	O	mean
17	102	109	O	decrease
18	111	112	O	of
19	114	126	O	approximately
20	128	130	O	80%
21	132	133	O	in
22	135	139	O	total
23	141	148	B-T	exposure
24	150	151	O	to
25	153	161	O	XXXXXXXX
26	163	165	O	and
27	167	176	B-K	metabolite
28	178	178	I-K	M
29	180	181	I-K	II
K/1:C54356 K/12:C54356 K/27:C54356

Rozerem	5459	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	13
Rifampin (strong CYP enzyme inducer): Decreases exposure to and effects of ramelteon.
1	0	7	B-K	Rifampin
2	10	15	I-K	strong
3	17	19	I-K	CYP
4	21	26	I-K	enzyme
5	28	34	I-K	inducer
6	36	36	O	:
7	38	46	B-T	Decreases
8	48	55	I-T	exposure
9	57	58	O	to
10	60	62	O	and
11	64	70	O	effects
12	72	73	O	of
13	75	83	O	XXXXXXXX
K/1:C54356

Rozerem	5460	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	12
ROZEREM is not known to interfere with commonly used clinical laboratory tests.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	13	O	not
4	15	19	O	known
5	21	22	O	to
6	24	32	O	interfere
7	34	37	O	with
8	39	46	O	commonly
9	48	51	O	used
10	53	60	O	clinical
11	62	71	O	laboratory
12	73	77	O	tests
NULL

Rozerem	5461	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	15
ROZEREM should be administered with caution in subjects taking strong CYP2C9 inhibitors such as fluconazole.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	16	O	be
4	18	29	B-T	administered
5	31	34	I-T	with
6	36	42	B-T	caution
7	44	45	O	in
8	47	54	O	subjects
9	56	61	O	taking
10	63	68	B-U	strong
11	70	75	I-U	CYP2C9
12	77	86	I-U	inhibitors
13	88	91	O	such
14	93	94	O	as
15	96	106	B-U	fluconazole
NULL

Rozerem	5462	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	15
ROZEREM should be administered with caution in subjects taking strong CYP3A4 inhibitors such as ketoconazole.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	16	O	be
4	18	29	B-T	administered
5	31	34	I-T	with
6	36	42	B-T	caution
7	44	45	O	in
8	47	54	O	subjects
9	56	61	O	taking
10	63	68	B-U	strong
11	70	75	I-U	CYP3A4
12	77	86	I-U	inhibitors
13	88	91	O	such
14	93	94	O	as
15	96	107	B-U	ketoconazole
NULL

Rozerem	5463	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	13
ROZEREM should be administered with caution to patients taking less strong CYP1A2 inhibitors.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	16	O	be
4	18	29	B-T	administered
5	31	34	O	with
6	36	42	B-T	caution
7	44	45	O	to
8	47	54	O	patients
9	56	61	O	taking
10	63	66	O	less
11	68	73	B-U	strong
12	75	80	I-U	CYP1A2
13	82	91	I-U	inhibitors
NULL

Rozerem	5464	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	9
ROZEREM should not be used in combination with fluvoxamine.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	17	B-T	not
4	19	20	I-T	be
5	22	25	O	used
6	27	28	O	in
7	30	40	O	combination
8	42	45	O	with
9	47	57	B-U	fluvoxamine
NULL

Rozerem	5465	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	22
Since the intended effect of ROZEREM is to promote sleep, patients should be cautioned not to consume alcohol when using ROZEREM.
1	0	4	O	Since
2	6	8	O	the
3	10	17	O	intended
4	19	24	O	effect
5	26	27	O	of
6	29	35	O	XXXXXXXX
7	37	38	O	is
8	40	41	O	to
9	43	49	O	promote
10	51	55	O	sleep
11	56	56	O	,
12	58	65	O	patients
13	67	72	O	should
14	74	75	O	be
15	77	85	B-T	cautioned
16	87	89	B-T	not
17	91	92	O	to
18	94	100	O	consume
19	102	108	B-U	alcohol
20	110	113	O	when
21	115	119	O	using
22	121	127	O	XXXXXXXX
NULL

Rozerem	5466	34073-7	9de82310-70e8-47b9-b1fc-6c6848b99455	11
Use of the products in combination may have an additive effect.
1	0	2	O	Use
2	4	5	O	of
3	7	9	O	the
4	11	18	B-D	products
5	20	21	O	in
6	23	33	O	combination
7	35	37	O	may
8	39	42	O	have
9	44	45	O	an
10	47	54	B-E	additive
11	56	61	I-E	effect
D/4:10:1

Rozerem	5467	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	19
A variety of cognitive and behavior changes have been reported to occur in association with the use of hypnotics.
1	0	0	O	A
2	2	8	O	variety
3	10	11	O	of
4	13	21	O	cognitive
5	23	25	O	and
6	27	34	O	behavior
7	36	42	B-E	changes
8	44	47	O	have
9	49	52	O	been
10	54	61	O	reported
11	63	64	O	to
12	66	70	O	occur
13	72	73	O	in
14	75	85	O	association
15	87	90	O	with
16	92	94	O	the
17	96	98	O	use
18	100	101	O	of
19	103	111	B-D	hypnotics
D/19:7:1

Rozerem	5468	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	15
Abnormal thinking, behavioral changes, complex behaviors: May include "sleep-driving" and hallucinations.
1	0	7	O	Abnormal
2	9	16	O	thinking
3	17	17	O	,
4	19	28	O	behavioral
5	30	36	O	changes
6	37	37	O	,
7	39	45	O	complex
8	47	55	O	behaviors
9	56	56	O	:
10	58	60	O	May
11	62	68	O	include
12	70	75	O	"sleep
13	77	84	O	driving"
14	86	88	O	and
15	90	103	B-E	hallucinations
NULL

Rozerem	5469	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	16
After taking ROZEREM, patients should confine their activities to those necessary to prepare for bed.
1	0	4	O	After
2	6	11	O	taking
3	13	19	O	XXXXXXXX
4	20	20	O	,
5	22	29	O	patients
6	31	36	O	should
7	38	44	O	confine
8	46	50	O	their
9	52	61	O	activities
10	63	64	O	to
11	66	70	O	those
12	72	80	O	necessary
13	82	83	O	to
14	85	91	O	prepare
15	93	95	O	for
16	97	99	O	bed
NULL

Rozerem	5470	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	12
Amnesia, anxiety and other neuro-psychiatric symptoms may also occur unpredictably.
1	0	6	B-E	Amnesia
2	7	7	O	,
3	9	15	B-E	anxiety
4	17	19	O	and
5	21	25	O	other
6	27	31	O	neuro
7	33	43	B-E	psychiatric
8	45	52	O	symptoms
9	54	56	O	may
10	58	61	O	also
11	63	67	O	occur
12	69	81	O	unpredictably
NULL

Rozerem	5471	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	25
CNS effects: Potential impairment of activities requiring complete mental alertness such as operating machinery or driving a motor vehicle, after ingesting the drug.
1	0	2	O	CNS
2	4	10	O	effects
3	11	11	O	:
4	13	21	O	Potential
5	23	32	O	impairment
6	34	35	O	of
7	37	46	O	activities
8	48	56	O	requiring
9	58	65	O	complete
10	67	72	O	mental
11	74	82	O	alertness
12	84	87	O	such
13	89	90	O	as
14	92	100	O	operating
15	102	110	O	machinery
16	112	113	O	or
17	115	121	O	driving
18	123	123	O	a
19	125	129	O	motor
20	131	137	O	vehicle
21	138	138	O	,
22	140	144	O	after
23	146	154	O	ingesting
24	156	158	O	the
25	160	163	O	drug
NULL

Rozerem	5472	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	10
Complex behaviors have been reported with the use of ROZEREM.
1	0	6	O	Complex
2	8	16	O	behaviors
3	18	21	O	have
4	23	26	O	been
5	28	35	O	reported
6	37	40	O	with
7	42	44	O	the
8	46	48	O	use
9	50	51	O	of
10	53	59	O	XXXXXXXX
NULL

Rozerem	5473	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	51
Complex behaviors such as "sleep-driving" (i.e., driving while not fully awake after ingestion of a hypnotic) and other complex behaviors (e.g., preparing and eating food, making phone calls, or having sex), with amnesia for the event, have been reported in association with hypnotic use.
1	0	6	O	Complex
2	8	16	O	behaviors
3	18	21	O	such
4	23	24	O	as
5	26	31	O	"sleep
6	33	40	O	driving"
7	43	45	O	i.e
8	47	47	O	,
9	49	55	O	driving
10	57	61	O	while
11	63	65	O	not
12	67	71	O	fully
13	73	77	O	awake
14	79	83	O	after
15	85	93	O	ingestion
16	95	96	O	of
17	98	98	O	a
18	100	107	O	hypnotic
19	110	112	O	and
20	114	118	O	other
21	120	126	O	complex
22	128	136	O	behaviors
23	139	141	O	e.g
24	143	143	O	,
25	145	153	O	preparing
26	155	157	O	and
27	159	164	O	eating
28	166	169	O	food
29	170	170	O	,
30	172	177	O	making
31	179	183	O	phone
32	185	189	O	calls
33	190	190	O	,
34	192	193	O	or
35	195	200	O	having
36	202	204	O	sex
37	206	206	O	,
38	208	211	O	with
39	213	219	O	amnesia
40	221	223	O	for
41	225	227	O	the
42	229	233	O	event
43	234	234	O	,
44	236	239	O	have
45	241	244	O	been
46	246	253	O	reported
47	255	256	O	in
48	258	268	O	association
49	270	273	O	with
50	275	282	O	hypnotic
51	284	286	O	use
NULL

Rozerem	5474	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	10
Depression: Worsening of depression or suicidal thinking may occur.
1	0	9	O	Depression
2	10	10	O	:
3	12	20	O	Worsening
4	22	23	O	of
5	25	34	O	depression
6	36	37	O	or
7	39	46	O	suicidal
8	48	55	O	thinking
9	57	59	O	may
10	61	65	O	occur
NULL

Rozerem	5475	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	15
Discontinuation of ROZEREM should be strongly considered for patients who report any complex sleep behavior.
1	0	14	O	Discontinuation
2	16	17	O	of
3	19	25	O	XXXXXXXX
4	27	32	O	should
5	34	35	O	be
6	37	44	O	strongly
7	46	55	O	considered
8	57	59	O	for
9	61	68	O	patients
10	70	72	O	who
11	74	79	O	report
12	81	83	O	any
13	85	91	O	complex
14	93	97	O	sleep
15	99	106	O	behavior
NULL

Rozerem	5476	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	7
Do not rechallenge if such reactions occur.
1	0	1	O	Do
2	3	5	O	not
3	7	17	O	rechallenge
4	19	20	O	if
5	22	25	O	such
6	27	35	O	reactions
7	37	41	O	occur
NULL

Rozerem	5477	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	9
Effect on reproductive axis in developing humans is unknown.
1	0	5	O	Effect
2	7	8	O	on
3	10	21	O	reproductive
4	23	26	O	axis
5	28	29	O	in
6	31	40	O	developing
7	42	47	O	humans
8	49	50	O	is
9	52	58	O	unknown
NULL

Rozerem	5478	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	19
Exacerbation of insomnia and emergence of cognitive and behavioral abnormalities were seen with ROZEREM during the clinical development program.
1	0	11	O	Exacerbation
2	13	14	O	of
3	16	23	B-E	insomnia
4	25	27	O	and
5	29	37	O	emergence
6	39	40	O	of
7	42	50	O	cognitive
8	52	54	O	and
9	56	65	O	behavioral
10	67	79	O	abnormalities
11	81	84	O	were
12	86	89	O	seen
13	91	94	O	with
14	96	102	O	XXXXXXXX
15	104	109	O	during
16	111	113	O	the
17	115	122	O	clinical
18	124	134	O	development
19	136	142	O	program
NULL

Rozerem	5479	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	29
For patients presenting with unexplained amenorrhea, galactorrhea, decreased libido, or problems with fertility, assessment of prolactin levels and testosterone levels should be considered as appropriate.
1	0	2	O	For
2	4	11	O	patients
3	13	22	O	presenting
4	24	27	O	with
5	29	39	O	unexplained
6	41	50	O	amenorrhea
7	51	51	O	,
8	53	64	O	galactorrhea
9	65	65	O	,
10	67	75	O	decreased
11	77	82	O	libido
12	83	83	O	,
13	85	86	O	or
14	88	95	O	problems
15	97	100	O	with
16	102	110	O	fertility
17	111	111	O	,
18	113	122	O	assessment
19	124	125	O	of
20	127	135	O	prolactin
21	137	142	O	levels
22	144	146	O	and
23	148	159	O	testosterone
24	161	166	O	levels
25	168	173	O	should
26	175	176	O	be
27	178	187	O	considered
28	189	190	O	as
29	192	202	O	appropriate
NULL

Rozerem	5480	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	21
Hallucinations, as well as behavioral changes such as bizarre behavior, agitation and mania have been reported with ROZEREM use.
1	0	13	B-E	Hallucinations
2	14	14	O	,
3	16	17	O	as
4	19	22	O	well
5	24	25	O	as
6	27	36	O	behavioral
7	38	44	B-E	changes
8	46	49	O	such
9	51	52	O	as
10	54	60	B-E	bizarre
11	62	69	I-E	behavior
12	70	70	O	,
13	72	80	B-E	agitation
14	82	84	O	and
15	86	90	B-E	mania
16	92	95	O	have
17	97	100	O	been
18	102	109	O	reported
19	111	114	O	with
20	116	122	O	XXXXXXXX
21	124	126	O	use
NULL

Rozerem	5481	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	17
If angioedema involves the tongue, glottis or larynx, airway obstruction may occur and be fatal.
1	0	1	O	If
2	3	12	O	angioedema
3	14	21	O	involves
4	23	25	O	the
5	27	32	O	tongue
6	33	33	O	,
7	35	41	O	glottis
8	43	44	O	or
9	46	51	O	larynx
10	52	52	O	,
11	54	59	O	airway
12	61	71	O	obstruction
13	73	75	O	may
14	77	81	O	occur
15	83	85	O	and
16	87	88	O	be
17	90	94	O	fatal
NULL

Rozerem	5482	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	7
Immediately evaluate any new onset behavioral changes.
1	0	10	O	Immediately
2	12	19	O	evaluate
3	21	23	O	any
4	25	27	O	new
5	29	33	O	onset
6	35	44	O	behavioral
7	46	52	O	changes
NULL

Rozerem	5483	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	37
In addition, in vitro data indicate that ramelteon does not cause false-positive results for benzodiazepines, opiates, barbiturates, cocaine, cannabinoids, or amphetamines in two standard urine drug screening methods in vitro.
1	0	1	O	In
2	3	10	O	addition
3	11	11	O	,
4	13	14	O	in
5	16	20	O	vitro
6	22	25	O	data
7	27	34	O	indicate
8	36	39	O	that
9	41	49	O	XXXXXXXX
10	51	54	O	does
11	56	58	O	not
12	60	64	O	cause
13	66	70	O	false
14	72	79	O	positive
15	81	87	O	results
16	89	91	O	for
17	93	107	O	benzodiazepines
18	108	108	O	,
19	110	116	O	opiates
20	117	117	O	,
21	119	130	O	barbiturates
22	131	131	O	,
23	133	139	O	cocaine
24	140	140	O	,
25	142	153	O	cannabinoids
26	154	154	O	,
27	156	157	O	or
28	159	170	O	amphetamines
29	172	173	O	in
30	175	177	O	two
31	179	186	O	standard
32	188	192	O	urine
33	194	197	O	drug
34	199	207	O	screening
35	209	215	O	methods
36	217	218	O	in
37	220	224	O	vitro
NULL

Rozerem	5484	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	24
In primarily depressed patients, worsening of depression (including suicidal ideation and completed suicides) has been reported in association with the use of hypnotics.
1	0	1	O	In
2	3	11	O	primarily
3	13	21	O	depressed
4	23	30	O	patients
5	31	31	O	,
6	33	41	O	worsening
7	43	44	O	of
8	46	55	O	depression
9	58	66	O	including
10	68	75	O	suicidal
11	77	84	O	ideation
12	86	88	O	and
13	90	98	O	completed
14	100	107	O	suicides
15	110	112	O	has
16	114	117	O	been
17	119	126	O	reported
18	128	129	O	in
19	131	141	O	association
20	143	146	O	with
21	148	150	O	the
22	152	154	O	use
23	156	157	O	of
24	159	167	O	hypnotics
NULL

Rozerem	5485	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	16
Interference with Laboratory Tests ROZEREM is not known to interfere with commonly used clinical laboratory tests.
1	0	11	O	Interference
2	13	16	O	with
3	18	27	O	Laboratory
4	29	33	O	Tests
5	35	41	O	XXXXXXXX
6	43	44	O	is
7	46	48	O	not
8	50	54	O	known
9	56	57	O	to
10	59	67	O	interfere
11	69	72	O	with
12	74	81	O	commonly
13	83	86	O	used
14	88	95	O	clinical
15	97	106	O	laboratory
16	108	112	O	tests
NULL

Rozerem	5486	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	23
It is not known what effect chronic or even chronic intermittent use of ROZEREM may have on the reproductive axis in developing humans.
1	0	1	O	It
2	3	4	O	is
3	6	8	O	not
4	10	14	O	known
5	16	19	O	what
6	21	26	O	effect
7	28	34	O	chronic
8	36	37	O	or
9	39	42	O	even
10	44	50	O	chronic
11	52	63	O	intermittent
12	65	67	O	use
13	69	70	O	of
14	72	78	O	XXXXXXXX
15	80	82	O	may
16	84	87	O	have
17	89	90	O	on
18	92	94	O	the
19	96	107	O	reproductive
20	109	112	O	axis
21	114	115	O	in
22	117	126	O	developing
23	128	133	O	humans
NULL

Rozerem	5487	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	6
Monitoring No standard monitoring is required.
1	0	9	O	Monitoring
2	11	12	O	No
3	14	21	O	standard
4	23	32	O	monitoring
5	34	35	O	is
6	37	44	O	required
NULL

Rozerem	5488	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	19
Need to evaluate for co-morbid diagnoses: Reevaluate if insomnia persists after 7 to 10 days of treatment.
1	0	3	O	Need
2	5	6	O	to
3	8	15	O	evaluate
4	17	19	O	for
5	21	22	O	co
6	24	29	O	morbid
7	31	39	O	diagnoses
8	40	40	O	:
9	42	51	O	Reevaluate
10	53	54	O	if
11	56	63	O	insomnia
12	65	72	O	persists
13	74	78	O	after
14	80	80	O	7
15	82	83	O	to
16	85	86	O	10
17	88	91	O	days
18	93	94	O	of
19	96	104	O	treatment
NULL

Rozerem	5489	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	22
Patients should avoid engaging in hazardous activities that require concentration (such as operating a motor vehicle or heavy machinery) after taking ROZEREM.
1	0	7	O	Patients
2	9	14	O	should
3	16	20	O	avoid
4	22	29	O	engaging
5	31	32	O	in
6	34	42	O	hazardous
7	44	53	O	activities
8	55	58	O	that
9	60	66	O	require
10	68	80	O	concentration
11	83	86	O	such
12	88	89	O	as
13	91	99	O	operating
14	101	101	O	a
15	103	107	O	motor
16	109	115	O	vehicle
17	117	118	O	or
18	120	124	O	heavy
19	126	134	O	machinery
20	137	141	O	after
21	143	148	O	taking
22	150	156	O	XXXXXXXX
NULL

Rozerem	5490	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	24
Patients should be advised not to consume alcohol in combination with ROZEREM as alcohol and ROZEREM may have additive effects when used in conjunction.
1	0	7	O	Patients
2	9	14	O	should
3	16	17	O	be
4	19	25	O	advised
5	27	29	O	not
6	31	32	O	to
7	34	40	O	consume
8	42	48	B-D	alcohol
9	50	51	O	in
10	53	63	O	combination
11	65	68	O	with
12	70	76	O	XXXXXXXX
13	78	79	O	as
14	81	87	B-D	alcohol
15	89	91	O	and
16	93	99	O	XXXXXXXX
17	101	103	O	may
18	105	108	O	have
19	110	117	B-E	additive
20	119	125	I-E	effects
21	127	130	O	when
22	132	135	O	used
23	137	138	O	in
24	140	150	O	conjunction
D/8:19:1 D/14:19:1

Rozerem	5491	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	15
Patients who develop angioedema after treatment with ROZEREM should not be rechallenged with the drug.
1	0	7	O	Patients
2	9	11	O	who
3	13	19	O	develop
4	21	30	O	angioedema
5	32	36	O	after
6	38	46	O	treatment
7	48	51	O	with
8	53	59	O	XXXXXXXX
9	61	66	O	should
10	68	70	O	not
11	72	73	O	be
12	75	86	O	rechallenged
13	88	91	O	with
14	93	95	O	the
15	97	100	O	drug
NULL

Rozerem	5492	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	15
Patients with severe sleep apnea: Rozerem is not recommended for use in this population.
1	0	7	O	Patients
2	9	12	O	with
3	14	19	O	severe
4	21	25	O	sleep
5	27	31	O	apnea
6	32	32	O	:
7	34	40	O	XXXXXXXX
8	42	43	O	is
9	45	47	O	not
10	49	59	O	recommended
11	61	63	O	for
12	65	67	O	use
13	69	70	O	in
14	72	75	O	this
15	77	86	O	population
NULL

Rozerem	5493	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	25
Rare cases of angioedema involving the tongue, glottis or larynx have been reported in patients after taking the first or subsequent doses of ROZEREM.
1	0	3	O	Rare
2	5	9	O	cases
3	11	12	O	of
4	14	23	O	angioedema
5	25	33	O	involving
6	35	37	O	the
7	39	44	O	tongue
8	45	45	O	,
9	47	53	O	glottis
10	55	56	O	or
11	58	63	O	larynx
12	65	68	O	have
13	70	73	O	been
14	75	82	O	reported
15	84	85	O	in
16	87	94	O	patients
17	96	100	O	after
18	102	107	O	taking
19	109	111	O	the
20	113	117	O	first
21	119	120	O	or
22	122	131	O	subsequent
23	133	137	O	doses
24	139	140	O	of
25	142	148	O	XXXXXXXX
NULL

Rozerem	5494	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	10
Reproductive effects: Include decreased testosterone and increased prolactin levels.
1	0	11	O	Reproductive
2	13	19	O	effects
3	20	20	O	:
4	22	28	O	Include
5	30	38	O	decreased
6	40	51	O	testosterone
7	53	55	O	and
8	57	65	O	increased
9	67	75	O	prolactin
10	77	82	O	levels
NULL

Rozerem	5495	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	20
ROZEREM has not been studied in subjects with severe sleep apnea and is not recommended for use in this population.
1	0	6	O	XXXXXXXX
2	8	10	O	has
3	12	14	O	not
4	16	19	O	been
5	21	27	O	studied
6	29	30	O	in
7	32	39	O	subjects
8	41	44	O	with
9	46	51	O	severe
10	53	57	O	sleep
11	59	63	O	apnea
12	65	67	O	and
13	69	70	O	is
14	72	74	O	not
15	76	86	O	recommended
16	88	90	O	for
17	92	94	O	use
18	96	97	O	in
19	99	102	O	this
20	104	113	O	population
NULL

Rozerem	5496	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	11
ROZEREM should not be used by patients with severe hepatic impairment.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	17	O	not
4	19	20	O	be
5	22	25	O	used
6	27	28	O	by
7	30	37	O	patients
8	39	42	O	with
9	44	49	O	severe
10	51	57	O	hepatic
11	59	68	O	impairment
NULL

Rozerem	5497	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	12
Severe anaphylactic/anaphylactoid reactions: Angioedema and anaphylaxis have been reported.
1	0	5	O	Severe
2	7	18	O	anaphylactic
3	19	19	O	/
4	20	32	O	anaphylactoid
5	34	42	O	reactions
6	43	43	O	:
7	45	54	O	Angioedema
8	56	58	O	and
9	60	70	O	anaphylaxis
10	72	75	O	have
11	77	80	O	been
12	82	89	O	reported
NULL

Rozerem	5498	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	32
Since sleep disturbances may be the presenting manifestation of a physical and/or psychiatric disorder, symptomatic treatment of insomnia should be initiated only after a careful evaluation of the patient.
1	0	4	O	Since
2	6	10	O	sleep
3	12	23	O	disturbances
4	25	27	O	may
5	29	30	O	be
6	32	34	O	the
7	36	45	O	presenting
8	47	59	O	manifestation
9	61	62	O	of
10	64	64	O	a
11	66	73	O	physical
12	75	77	O	and
13	78	78	O	/
14	79	80	O	or
15	82	92	O	psychiatric
16	94	101	O	disorder
17	102	102	O	,
18	104	114	O	symptomatic
19	116	124	O	treatment
20	126	127	O	of
21	129	136	O	insomnia
22	138	143	O	should
23	145	146	O	be
24	148	156	O	initiated
25	158	161	O	only
26	163	167	O	after
27	169	169	O	a
28	171	177	O	careful
29	179	188	O	evaluation
30	190	191	O	of
31	193	195	O	the
32	197	203	O	patient
NULL

Rozerem	5499	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	20
Some patients have had additional symptoms such as dyspnea, throat closing, or nausea and vomiting that suggest anaphylaxis.
1	0	3	O	Some
2	5	12	O	patients
3	14	17	O	have
4	19	21	O	had
5	23	32	O	additional
6	34	41	O	symptoms
7	43	46	O	such
8	48	49	O	as
9	51	57	O	dyspnea
10	58	58	O	,
11	60	65	O	throat
12	67	73	O	closing
13	74	74	O	,
14	76	77	O	or
15	79	84	O	nausea
16	86	88	O	and
17	90	97	O	vomiting
18	99	102	O	that
19	104	110	O	suggest
20	112	122	O	anaphylaxis
NULL

Rozerem	5500	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	10
Some patients have required medical therapy in the emergency department.
1	0	3	O	Some
2	5	12	O	patients
3	14	17	O	have
4	19	26	O	required
5	28	34	O	medical
6	36	42	O	therapy
7	44	45	O	in
8	47	49	O	the
9	51	59	O	emergency
10	61	70	O	department
NULL

Rozerem	5501	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	30
The failure of insomnia to remit after 7 to 10 days of treatment may indicate the presence of a primary psychiatric and/or medical illness that should be evaluated.
1	0	2	O	The
2	4	10	O	failure
3	12	13	O	of
4	15	22	O	insomnia
5	24	25	O	to
6	27	31	O	remit
7	33	37	O	after
8	39	39	O	7
9	41	42	O	to
10	44	45	O	10
11	47	50	O	days
12	52	53	O	of
13	55	63	O	treatment
14	65	67	O	may
15	69	76	O	indicate
16	78	80	O	the
17	82	89	O	presence
18	91	92	O	of
19	94	94	O	a
20	96	102	O	primary
21	104	114	O	psychiatric
22	116	118	O	and
23	119	119	O	/
24	120	121	O	or
25	123	129	O	medical
26	131	137	O	illness
27	139	142	O	that
28	144	149	O	should
29	151	152	O	be
30	154	162	O	evaluated
NULL

Rozerem	5502	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	15
The use of alcohol and other CNS depressants may increase the risk of such behaviors.
1	0	2	O	The
2	4	6	O	use
3	8	9	O	of
4	11	17	B-D	alcohol
5	19	21	O	and
6	23	27	O	other
7	29	31	B-D	CNS
8	33	43	I-D	depressants
9	45	47	O	may
10	49	56	O	increase
11	58	60	O	the
12	62	65	O	risk
13	67	68	O	of
14	70	73	O	such
15	75	83	O	behaviors
NULL

Rozerem	5503	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	14
These events can occur in hypnotic-naive as well as in hypnotic-experienced persons.
1	0	4	B-E	These
2	6	11	I-E	events
3	13	15	O	can
4	17	21	O	occur
5	23	24	O	in
6	26	33	B-D	hypnotic
7	35	39	O	naive
8	41	42	O	as
9	44	47	O	well
10	49	50	O	as
11	52	53	O	in
12	55	62	B-D	hypnotic
13	64	74	O	experienced
14	76	82	O	persons
D/6:1:1 D/12:1:1

Rozerem	5504	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	27
Use in Adolescents and Children ROZEREM has been associated with an effect on reproductive hormones in adults, e.g., decreased testosterone levels and increased prolactin levels.
1	0	2	O	Use
2	4	5	O	in
3	7	17	O	Adolescents
4	19	21	O	and
5	23	30	O	Children
6	32	38	O	XXXXXXXX
7	40	42	O	has
8	44	47	O	been
9	49	58	O	associated
10	60	63	O	with
11	65	66	O	an
12	68	73	O	effect
13	75	76	O	on
14	78	89	O	reproductive
15	91	98	O	hormones
16	100	101	O	in
17	103	108	O	adults
18	109	109	O	,
19	111	113	O	e.g
20	115	115	O	,
21	117	125	O	decreased
22	127	138	O	testosterone
23	140	145	O	levels
24	147	149	O	and
25	151	159	O	increased
26	161	169	O	prolactin
27	171	176	O	levels
NULL

Rozerem	5505	43685-7	9de82310-70e8-47b9-b1fc-6c6848b99455	33
Worsening of insomnia, or the emergence of new cognitive or behavioral abnormalities, may be the result of an unrecognized underlying psychiatric or physical disorder and requires further evaluation of the patient.
1	0	8	O	Worsening
2	10	11	O	of
3	13	20	O	insomnia
4	21	21	O	,
5	23	24	O	or
6	26	28	O	the
7	30	38	O	emergence
8	40	41	O	of
9	43	45	O	new
10	47	55	O	cognitive
11	57	58	O	or
12	60	69	O	behavioral
13	71	83	O	abnormalities
14	84	84	O	,
15	86	88	O	may
16	90	91	O	be
17	93	95	O	the
18	97	102	O	result
19	104	105	O	of
20	107	108	O	an
21	110	121	O	unrecognized
22	123	132	O	underlying
23	134	144	O	psychiatric
24	146	147	O	or
25	149	156	O	physical
26	158	165	O	disorder
27	167	169	O	and
28	171	178	O	requires
29	180	186	O	further
30	188	197	O	evaluation
31	199	200	O	of
32	202	204	O	the
33	206	212	O	patient
NULL

