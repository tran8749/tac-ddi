Diethylpropion HCl Immediate-Release	5615	34068-7	a750a1ef-8f3e-4870-97eb-3697aaca1818	17
Diethylpropion hydrochloride controlled-release: One controlled-release 75 mg tablet daily, swallowed whole, in midmorning.
1	0	27	O	XXXXXXXX
2	29	38	O	controlled
3	40	46	O	release
4	47	47	O	:
5	49	51	O	One
6	53	62	O	controlled
7	64	70	O	release
8	72	73	O	75
9	75	76	O	mg
10	78	83	O	tablet
11	85	89	O	daily
12	90	90	O	,
13	92	100	O	swallowed
14	102	106	O	whole
15	107	107	O	,
16	109	110	O	in
17	112	121	O	midmorning
NULL

Diethylpropion HCl Immediate-Release	5616	34068-7	a750a1ef-8f3e-4870-97eb-3697aaca1818	28
Diethylpropion hydrochloride immediate-release: One immediate-release 25 mg tablet three times daily, one hour before meals, and in midevening if desired to overcome night hunger.
1	0	27	O	XXXXXXXX
2	29	37	O	immediate
3	39	45	O	release
4	46	46	O	:
5	48	50	O	One
6	52	60	O	immediate
7	62	68	O	release
8	70	71	O	25
9	73	74	O	mg
10	76	81	O	tablet
11	83	87	O	three
12	89	93	O	times
13	95	99	O	daily
14	100	100	O	,
15	102	104	O	one
16	106	109	O	hour
17	111	116	O	before
18	118	122	O	meals
19	123	123	O	,
20	125	127	O	and
21	129	130	O	in
22	132	141	O	midevening
23	143	144	O	if
24	146	152	O	desired
25	154	155	O	to
26	157	164	O	overcome
27	166	170	O	night
28	172	177	O	hunger
NULL

Diethylpropion HCl Immediate-Release	5617	34068-7	a750a1ef-8f3e-4870-97eb-3697aaca1818	33
Geriatric Use: This drug is known to be substantially excreted by the kidney, and the risk of toxic reactions to this drug may be greater in patients with impaired renal function.
1	0	8	O	Geriatric
2	10	12	O	Use
3	13	13	O	:
4	15	18	O	This
5	20	23	O	drug
6	25	26	O	is
7	28	32	O	known
8	34	35	O	to
9	37	38	O	be
10	40	52	O	substantially
11	54	61	O	excreted
12	63	64	O	by
13	66	68	O	the
14	70	75	O	kidney
15	76	76	O	,
16	78	80	O	and
17	82	84	O	the
18	86	89	O	risk
19	91	92	O	of
20	94	98	O	toxic
21	100	108	O	reactions
22	110	111	O	to
23	113	116	O	this
24	118	121	O	drug
25	123	125	O	may
26	127	128	O	be
27	130	136	O	greater
28	138	139	O	in
29	141	148	O	patients
30	150	153	O	with
31	155	162	O	impaired
32	164	168	O	renal
33	170	177	O	function
NULL

Diethylpropion HCl Immediate-Release	5618	34070-3	a750a1ef-8f3e-4870-97eb-3697aaca1818	17
During or within 14 days following the administration of monoamine oxidase inhibitors, hypertensive crises may result.
1	0	5	O	During
2	7	8	O	or
3	10	15	O	within
4	17	18	O	14
5	20	23	O	days
6	25	33	O	following
7	35	37	O	the
8	39	52	O	administration
9	54	55	O	of
10	57	65	B-D	monoamine
11	67	73	I-D	oxidase
12	75	84	I-D	inhibitors
13	85	85	O	,
14	87	98	O	hypertensive
15	100	105	O	crises
16	107	109	O	may
17	111	116	O	result
NULL

Diethylpropion HCl Immediate-Release	5619	34070-3	a750a1ef-8f3e-4870-97eb-3697aaca1818	21
Pulmonary hypertension, advanced arteriosclerosis, hyperthyroidism, known hypersensitivity or idiosyncrasy to the sympathomimetic amines, glaucoma, severe hypertension.
1	0	8	O	Pulmonary
2	10	21	O	hypertension
3	22	22	O	,
4	24	31	O	advanced
5	33	48	O	arteriosclerosis
6	49	49	O	,
7	51	65	O	hyperthyroidism
8	66	66	O	,
9	68	72	O	known
10	74	89	O	hypersensitivity
11	91	92	O	or
12	94	105	O	idiosyncrasy
13	107	108	O	to
14	110	112	O	the
15	114	128	B-D	sympathomimetic
16	130	135	I-D	amines
17	136	136	O	,
18	138	145	O	glaucoma
19	146	146	O	,
20	148	153	B-E	severe
21	155	166	I-E	hypertension
D/15:20:1

Diethylpropion HCl Immediate-Release	5620	34070-3	a750a1ef-8f3e-4870-97eb-3697aaca1818	9
Use in combination with other anorectic agents is contraindicated.
1	0	2	O	Use
2	4	5	O	in
3	7	17	O	combination
4	19	22	O	with
5	24	28	O	other
6	30	38	B-U	anorectic
7	40	45	I-U	agents
8	47	48	O	is
9	50	64	B-T	contraindicated
NULL

Diethylpropion HCl Immediate-Release	5621	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	21
Baseline cardiac evaluation should be considered to detect preexisting valvular heart diseases or pulmonary hypertension prior to initiation of diethylpropion hydrochloride treatment.
1	0	7	O	Baseline
2	9	15	O	cardiac
3	17	26	O	evaluation
4	28	33	O	should
5	35	36	O	be
6	38	47	O	considered
7	49	50	O	to
8	52	57	O	detect
9	59	69	O	preexisting
10	71	78	O	valvular
11	80	84	O	heart
12	86	93	O	diseases
13	95	96	O	or
14	98	106	O	pulmonary
15	108	119	O	hypertension
16	121	125	O	prior
17	127	128	O	to
18	130	139	O	initiation
19	141	142	O	of
20	144	171	O	XXXXXXXX
21	173	181	O	treatment
NULL

Diethylpropion HCl Immediate-Release	5622	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	15
Diethylpropion hydrochloride is not recommended for patients who used any anorectic agents within the prior year.
1	0	27	O	XXXXXXXX
2	29	30	O	is
3	32	34	B-T	not
4	36	46	I-T	recommended
5	48	50	O	for
6	52	59	O	patients
7	61	63	O	who
8	65	68	O	used
9	70	72	O	any
10	74	82	B-U	anorectic
11	84	89	I-U	agents
12	91	96	O	within
13	98	100	O	the
14	102	106	O	prior
15	108	111	O	year
NULL

Diethylpropion HCl Immediate-Release	5623	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	14
Diethylpropion hydrochloride is not recommended in patients with known heart murmur or valvular heart disease.
1	0	27	O	XXXXXXXX
2	29	30	O	is
3	32	34	O	not
4	36	46	O	recommended
5	48	49	O	in
6	51	58	O	patients
7	60	63	O	with
8	65	69	O	known
9	71	75	O	heart
10	77	82	O	murmur
11	84	85	O	or
12	87	94	O	valvular
13	96	100	O	heart
14	102	108	O	disease
NULL

Diethylpropion HCl Immediate-Release	5624	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	30
Diethylpropion hydrochloride may impair the ability of the patient to engage in potentially hazardous activities such as operating machinery or driving a motor vehicle; the patient should therefore be cautioned accordingly.
1	0	27	O	XXXXXXXX
2	29	31	O	may
3	33	38	O	impair
4	40	42	O	the
5	44	50	O	ability
6	52	53	O	of
7	55	57	O	the
8	59	65	O	patient
9	67	68	O	to
10	70	75	O	engage
11	77	78	O	in
12	80	90	O	potentially
13	92	100	O	hazardous
14	102	111	O	activities
15	113	116	O	such
16	118	119	O	as
17	121	129	O	operating
18	131	139	O	machinery
19	141	142	O	or
20	144	150	O	driving
21	152	152	O	a
22	154	158	O	motor
23	160	166	O	vehicle
24	169	171	O	the
25	173	179	O	patient
26	181	186	O	should
27	188	196	O	therefore
28	198	199	O	be
29	201	209	O	cautioned
30	211	221	O	accordingly
NULL

Diethylpropion HCl Immediate-Release	5625	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	24
Diethylpropion hydrochloride should not be used in combination with other anorectic agents, including prescribed drugs, over-the-counter preparations, and herbal products.
1	0	27	O	XXXXXXXX
2	29	34	O	should
3	36	38	B-T	not
4	40	41	I-T	be
5	43	46	I-T	used
6	48	49	O	in
7	51	61	O	combination
8	63	66	O	with
9	68	72	O	other
10	74	82	B-U	anorectic
11	84	89	I-U	agents
12	90	90	O	,
13	92	100	O	including
14	102	111	O	prescribed
15	113	117	O	drugs
16	118	118	O	,
17	120	123	O	over
18	125	127	B-D	the
19	129	135	I-D	counter
20	137	148	B-U	preparations
21	149	149	O	,
22	151	153	O	and
23	155	160	O	herbal
24	162	169	B-U	products
NULL

Diethylpropion HCl Immediate-Release	5626	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	16
Echocardiogram during and after treatment could be useful for detecting any valvular disorders which may occur.
1	0	13	O	Echocardiogram
2	15	20	O	during
3	22	24	O	and
4	26	30	O	after
5	32	40	O	treatment
6	42	46	O	could
7	48	49	O	be
8	51	56	O	useful
9	58	60	O	for
10	62	70	O	detecting
11	72	74	O	any
12	76	83	O	valvular
13	85	93	O	disorders
14	95	99	O	which
15	101	103	O	may
16	105	109	O	occur
NULL

Diethylpropion HCl Immediate-Release	5627	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	10
Hallucinations have occurred rarely following high doses of the drug.
1	0	13	B-E	Hallucinations
2	15	18	O	have
3	20	27	O	occurred
4	29	34	O	rarely
5	36	44	O	following
6	46	49	O	high
7	51	55	O	doses
8	57	58	O	of
9	60	62	O	the
10	64	67	O	drug
NULL

Diethylpropion HCl Immediate-Release	5628	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	25
If tolerance develops, the recommended dose should not be exceeded in an attempt to increase the effect; rather, the drug should be discontinued.
1	0	1	O	If
2	3	11	O	tolerance
3	13	20	O	develops
4	21	21	O	,
5	23	25	O	the
6	27	37	O	recommended
7	39	42	O	dose
8	44	49	O	should
9	51	53	O	not
10	55	56	O	be
11	58	65	O	exceeded
12	67	68	O	in
13	70	71	O	an
14	73	79	O	attempt
15	81	82	O	to
16	84	91	O	increase
17	93	95	O	the
18	97	102	O	effect
19	105	110	O	rather
20	111	111	O	,
21	113	115	O	the
22	117	120	O	drug
23	122	127	O	should
24	129	130	O	be
25	132	143	O	discontinued
NULL

Diethylpropion HCl Immediate-Release	5629	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	34
In a case-control epidemiological study, the use of anorectic agents, including diethylpropion, was associated with an increased risk of developing pulmonary hypertension, a rare, but often fatal disorder.
1	0	1	O	In
2	3	3	O	a
3	5	8	O	case
4	10	16	O	control
5	18	32	O	epidemiological
6	34	38	O	study
7	39	39	O	,
8	41	43	O	the
9	45	47	O	use
10	49	50	O	of
11	52	60	B-D	anorectic
12	62	67	I-D	agents
13	68	68	O	,
14	70	78	O	including
15	80	93	O	XXXXXXXX
16	94	94	O	,
17	96	98	O	was
18	100	109	O	associated
19	111	114	O	with
20	116	117	O	an
21	119	127	O	increased
22	129	132	O	risk
23	134	135	O	of
24	137	146	O	developing
25	148	156	O	pulmonary
26	158	169	O	hypertension
27	170	170	O	,
28	172	172	O	a
29	174	177	O	rare
30	178	178	O	,
31	180	182	O	but
32	184	188	O	often
33	190	194	O	fatal
34	196	203	O	disorder
NULL

Diethylpropion HCl Immediate-Release	5630	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	13
Increased risk of pulmonary hypertension with repeated courses of therapy cannot be excluded.
1	0	8	O	Increased
2	10	13	O	risk
3	15	16	O	of
4	18	26	O	pulmonary
5	28	39	O	hypertension
6	41	44	O	with
7	46	53	O	repeated
8	55	61	O	courses
9	63	64	O	of
10	66	72	O	therapy
11	74	79	O	cannot
12	81	82	O	be
13	84	91	O	excluded
NULL

Diethylpropion HCl Immediate-Release	5631	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	26
Possible contributing factors include use for extended periods of time, higher than recommended dose, and/or use in combination with other anorectic drugs.
1	0	7	O	Possible
2	9	20	O	contributing
3	22	28	O	factors
4	30	36	O	include
5	38	40	O	use
6	42	44	O	for
7	46	53	O	extended
8	55	61	O	periods
9	63	64	O	of
10	66	69	O	time
11	70	70	O	,
12	72	77	O	higher
13	79	82	O	than
14	84	94	O	recommended
15	96	99	O	dose
16	100	100	O	,
17	102	104	O	and
18	105	105	O	/
19	106	107	O	or
20	109	111	O	use
21	113	114	O	in
22	116	126	O	combination
23	128	131	O	with
24	133	137	O	other
25	139	147	B-U	anorectic
26	149	153	I-U	drugs
NULL

Diethylpropion HCl Immediate-Release	5632	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	14
Prolonged use of diethylpropion hydrochloride may induce dependence with withdrawal syndrome on cessation of therapy.
1	0	8	O	Prolonged
2	10	12	O	use
3	14	15	O	of
4	17	44	O	XXXXXXXX
5	46	48	O	may
6	50	55	O	induce
7	57	66	O	dependence
8	68	71	O	with
9	73	82	O	withdrawal
10	84	91	O	syndrome
11	93	94	O	on
12	96	104	O	cessation
13	106	107	O	of
14	109	115	O	therapy
NULL

Diethylpropion HCl Immediate-Release	5633	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	7
Psychosis abated after the drug was discontinued.
1	0	8	O	Psychosis
2	10	15	O	abated
3	17	21	O	after
4	23	25	O	the
5	27	30	O	drug
6	32	34	O	was
7	36	47	O	discontinued
NULL

Diethylpropion HCl Immediate-Release	5634	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	31
Several cases of toxic psychosis have been reported following the excessive use of the drug and some have been reported in which the recommended dose appears not to have been exceeded.
1	0	6	O	Several
2	8	12	O	cases
3	14	15	O	of
4	17	21	O	toxic
5	23	31	O	psychosis
6	33	36	O	have
7	38	41	O	been
8	43	50	O	reported
9	52	60	O	following
10	62	64	O	the
11	66	74	O	excessive
12	76	78	O	use
13	80	81	O	of
14	83	85	O	the
15	87	90	O	drug
16	92	94	O	and
17	96	99	O	some
18	101	104	O	have
19	106	109	O	been
20	111	118	O	reported
21	120	121	O	in
22	123	127	O	which
23	129	131	O	the
24	133	143	O	recommended
25	145	148	O	dose
26	150	156	O	appears
27	158	160	O	not
28	162	163	O	to
29	165	168	O	have
30	170	173	O	been
31	175	182	O	exceeded
NULL

Diethylpropion HCl Immediate-Release	5635	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	29
The onset or aggravation of exertional dyspnea, or unexplained symptoms of angina pectoris, syncope, or lower extremity edema suggest the possibility of occurrence of pulmonary hypertension.
1	0	2	O	The
2	4	8	O	onset
3	10	11	O	or
4	13	23	O	aggravation
5	25	26	O	of
6	28	37	O	exertional
7	39	45	O	dyspnea
8	46	46	O	,
9	48	49	O	or
10	51	61	O	unexplained
11	63	70	O	symptoms
12	72	73	O	of
13	75	80	O	angina
14	82	89	O	pectoris
15	90	90	O	,
16	92	98	O	syncope
17	99	99	O	,
18	101	102	O	or
19	104	108	O	lower
20	110	118	O	extremity
21	120	124	O	edema
22	126	132	O	suggest
23	134	136	O	the
24	138	148	O	possibility
25	150	151	O	of
26	153	162	O	occurrence
27	164	165	O	of
28	167	175	B-E	pulmonary
29	177	188	I-E	hypertension
NULL

Diethylpropion HCl Immediate-Release	5636	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	27
The potential risk of possible serious adverse effects such as valvular heart disease and pulmonary hypertension should be assessed carefully against the potential benefit of weight loss.
1	0	2	O	The
2	4	12	O	potential
3	14	17	O	risk
4	19	20	O	of
5	22	29	O	possible
6	31	37	O	serious
7	39	45	O	adverse
8	47	53	O	effects
9	55	58	O	such
10	60	61	O	as
11	63	70	O	valvular
12	72	76	O	heart
13	78	84	O	disease
14	86	88	O	and
15	90	98	O	pulmonary
16	100	111	O	hypertension
17	113	118	O	should
18	120	121	O	be
19	123	130	O	assessed
20	132	140	O	carefully
21	142	148	O	against
22	150	152	O	the
23	154	162	O	potential
24	164	170	O	benefit
25	172	173	O	of
26	175	180	O	weight
27	182	185	O	loss
NULL

Diethylpropion HCl Immediate-Release	5637	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	24
The use of anorectic agents for longer than 3 months was associated with a 23-fold increase in the risk of developing pulmonary hypertension.
1	0	2	O	The
2	4	6	O	use
3	8	9	O	of
4	11	19	O	anorectic
5	21	26	O	agents
6	28	30	O	for
7	32	37	O	longer
8	39	42	O	than
9	44	44	O	3
10	46	51	O	months
11	53	55	O	was
12	57	66	O	associated
13	68	71	O	with
14	73	73	O	a
15	75	76	O	23
16	78	81	O	fold
17	83	90	O	increase
18	92	93	O	in
19	95	97	O	the
20	99	102	O	risk
21	104	105	O	of
22	107	116	O	developing
23	118	126	B-E	pulmonary
24	128	139	I-E	hypertension
NULL

Diethylpropion HCl Immediate-Release	5638	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	46
To limit unwarranted exposure and risks, treatment with diethylpropion hydrochloride should be continued only if the patient has satisfactory weight loss within the first 4 weeks of treatment (e.g., weight loss of at least 4 pounds, or as determined by the physician and patient).
1	0	1	O	To
2	3	7	B-T	limit
3	9	19	I-T	unwarranted
4	21	28	I-T	exposure
5	30	32	O	and
6	34	38	O	risks
7	39	39	O	,
8	41	49	O	treatment
9	51	54	O	with
10	56	83	O	XXXXXXXX
11	85	90	O	should
12	92	93	O	be
13	95	103	O	continued
14	105	108	O	only
15	110	111	O	if
16	113	115	O	the
17	117	123	O	patient
18	125	127	O	has
19	129	140	O	satisfactory
20	142	147	O	weight
21	149	152	O	loss
22	154	159	O	within
23	161	163	O	the
24	165	169	O	first
25	171	171	O	4
26	173	177	O	weeks
27	179	180	O	of
28	182	190	O	treatment
29	193	195	O	e.g
30	197	197	O	,
31	199	204	O	weight
32	206	209	O	loss
33	211	212	O	of
34	214	215	O	at
35	217	221	O	least
36	223	223	O	4
37	225	230	O	pounds
38	231	231	O	,
39	233	234	O	or
40	236	237	O	as
41	239	248	O	determined
42	250	251	O	by
43	253	255	O	the
44	257	265	O	physician
45	267	269	O	and
46	271	277	O	patient
NULL

Diethylpropion HCl Immediate-Release	5639	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	23
Under these circumstances, diethylpropion hydrochloride should be immediately discontinued, and the patient should be evaluated for the possible presence of pulmonary hypertension.
1	0	4	O	Under
2	6	10	O	these
3	12	24	O	circumstances
4	25	25	O	,
5	27	54	O	XXXXXXXX
6	56	61	O	should
7	63	64	O	be
8	66	76	O	immediately
9	78	89	O	discontinued
10	90	90	O	,
11	92	94	O	and
12	96	98	O	the
13	100	106	O	patient
14	108	113	O	should
15	115	116	O	be
16	118	126	O	evaluated
17	128	130	O	for
18	132	134	O	the
19	136	143	O	possible
20	145	152	O	presence
21	154	155	O	of
22	157	165	O	pulmonary
23	167	178	O	hypertension
NULL

Diethylpropion HCl Immediate-Release	5640	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	19
Valvular heart disease associated with the use of some anorectic agents such as fenfluramine and dexfenfluramine has been reported.
1	0	7	O	Valvular
2	9	13	O	heart
3	15	21	O	disease
4	23	32	O	associated
5	34	37	O	with
6	39	41	O	the
7	43	45	O	use
8	47	48	O	of
9	50	53	B-D	some
10	55	63	B-D	anorectic
11	65	70	I-D	agents
12	72	75	O	such
13	77	78	O	as
14	80	91	B-D	fenfluramine
15	93	95	O	and
16	97	111	B-D	dexfenfluramine
17	113	115	O	has
18	117	120	O	been
19	122	129	O	reported
NULL

Diethylpropion HCl Immediate-Release	5641	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	16
Valvulopathy has been very rarely reported with diethylpropion hydrochloride monotherapy, but the causal relationship remains uncertain.
1	0	11	O	Valvulopathy
2	13	15	O	has
3	17	20	O	been
4	22	25	O	very
5	27	32	O	rarely
6	34	41	O	reported
7	43	46	O	with
8	48	75	O	XXXXXXXX
9	77	87	O	monotherapy
10	88	88	O	,
11	90	92	O	but
12	94	96	O	the
13	98	103	O	causal
14	105	116	O	relationship
15	118	124	O	remains
16	126	134	O	uncertain
NULL

Diethylpropion HCl Immediate-Release	5642	34071-1	a750a1ef-8f3e-4870-97eb-3697aaca1818	22
When central nervous system active agents are used, consideration must always be given to the possibility of adverse interactions with alcohol.
1	0	3	O	When
2	5	11	O	central
3	13	19	O	nervous
4	21	26	O	system
5	28	33	O	active
6	35	40	O	agents
7	42	44	O	are
8	46	49	O	used
9	50	50	O	,
10	52	64	O	consideration
11	66	69	O	must
12	71	76	O	always
13	78	79	O	be
14	81	85	O	given
15	87	88	O	to
16	90	92	O	the
17	94	104	O	possibility
18	106	107	O	of
19	109	115	O	adverse
20	117	128	O	interactions
21	130	133	O	with
22	135	141	B-D	alcohol
NULL

Diethylpropion HCl Immediate-Release	5643	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	14
Abuse with diethylpropion hydrochloride during pregnancy may result in withdrawal symptoms in the human neonate.
1	0	4	O	Abuse
2	6	9	O	with
3	11	38	O	XXXXXXXX
4	40	45	O	during
5	47	55	O	pregnancy
6	57	59	O	may
7	61	66	O	result
8	68	69	O	in
9	71	80	O	withdrawal
10	82	89	O	symptoms
11	91	92	O	in
12	94	96	O	the
13	98	102	O	human
14	104	110	O	neonate
NULL

Diethylpropion HCl Immediate-Release	5644	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	11
Animal reproduction studies have revealed no evidence of impairment of fertility.
1	0	5	O	Animal
2	7	18	O	reproduction
3	20	26	O	studies
4	28	31	O	have
5	33	40	O	revealed
6	42	43	O	no
7	45	52	O	evidence
8	54	55	O	of
9	57	66	O	impairment
10	68	69	O	of
11	71	79	O	fertility
NULL

Diethylpropion HCl Immediate-Release	5645	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	9
Antidiabetic drug requirements (i.e., insulin) may be altered.
1	0	11	O	Antidiabetic
2	13	16	O	drug
3	18	29	O	requirements
4	32	34	O	i.e
5	36	36	O	,
6	38	44	O	insulin
7	47	49	O	may
8	51	52	O	be
9	54	60	O	altered
NULL

Diethylpropion HCl Immediate-Release	5646	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	23
Because animal reproduction studies are not always predictive of human response, this drug should be used during pregnancy only if clearly needed.
1	0	6	O	Because
2	8	13	O	animal
3	15	26	O	reproduction
4	28	34	O	studies
5	36	38	O	are
6	40	42	O	not
7	44	49	O	always
8	51	60	O	predictive
9	62	63	O	of
10	65	69	O	human
11	71	78	O	response
12	79	79	O	,
13	81	84	O	this
14	86	89	O	drug
15	91	96	O	should
16	98	99	O	be
17	101	104	O	used
18	106	111	O	during
19	113	121	O	pregnancy
20	123	126	O	only
21	128	129	O	if
22	131	137	O	clearly
23	139	144	O	needed
NULL

Diethylpropion HCl Immediate-Release	5647	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	19
Because diethylpropion hydrochloride is a monoamine, hypertension may result when this agent is used with monoamine oxidase (MAO) inhibitors.
1	0	6	O	Because
2	8	35	O	XXXXXXXX
3	37	38	O	is
4	40	40	O	a
5	42	50	O	monoamine
6	51	51	O	,
7	53	64	O	hypertension
8	66	68	O	may
9	70	75	O	result
10	77	80	O	when
11	82	85	O	this
12	87	91	O	agent
13	93	94	O	is
14	96	99	O	used
15	101	104	O	with
16	106	114	O	monoamine
17	116	122	B-K	oxidase
18	125	127	I-K	MAO
19	130	139	I-K	inhibitors
K/17:C54357

Diethylpropion HCl Immediate-Release	5648	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	29
Because elderly patients are more likely to have decreased renal function, care should be taken in dose selection, and it may be useful to monitor renal function.
1	0	6	O	Because
2	8	14	O	elderly
3	16	23	O	patients
4	25	27	O	are
5	29	32	O	more
6	34	39	O	likely
7	41	42	O	to
8	44	47	O	have
9	49	57	O	decreased
10	59	63	O	renal
11	65	72	O	function
12	73	73	O	,
13	75	78	O	care
14	80	85	O	should
15	87	88	O	be
16	90	94	O	taken
17	96	97	O	in
18	99	102	O	dose
19	104	112	O	selection
20	113	113	O	,
21	115	117	O	and
22	119	120	O	it
23	122	124	O	may
24	126	127	O	be
25	129	134	O	useful
26	136	137	O	to
27	139	145	B-T	monitor
28	147	151	O	renal
29	153	160	O	function
NULL

Diethylpropion HCl Immediate-Release	5649	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	20
Caution is to be exercised in prescribing diethylpropion hydrochloride for patients with hypertension or with symptomatic cardiovascular disease, including arrhythmias.
1	0	6	O	Caution
2	8	9	O	is
3	11	12	O	to
4	14	15	O	be
5	17	25	O	exercised
6	27	28	O	in
7	30	40	O	prescribing
8	42	69	O	XXXXXXXX
9	71	73	O	for
10	75	82	O	patients
11	84	87	O	with
12	89	100	O	hypertension
13	102	103	O	or
14	105	108	O	with
15	110	120	O	symptomatic
16	122	135	O	cardiovascular
17	137	143	O	disease
18	144	144	O	,
19	146	154	O	including
20	156	166	O	arrhythmias
NULL

Diethylpropion HCl Immediate-Release	5650	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	24
Clinical studies of diethylpropion hydrochloride did not include sufficient numbers of subjects aged 65 and over to determine whether they respond differently from younger subjects.
1	0	7	O	Clinical
2	9	15	O	studies
3	17	18	O	of
4	20	47	O	XXXXXXXX
5	49	51	O	did
6	53	55	O	not
7	57	63	O	include
8	65	74	O	sufficient
9	76	82	O	numbers
10	84	85	O	of
11	87	94	O	subjects
12	96	99	O	aged
13	101	102	O	65
14	104	106	O	and
15	108	111	O	over
16	113	114	O	to
17	116	124	O	determine
18	126	132	O	whether
19	134	137	O	they
20	139	145	O	respond
21	147	157	O	differently
22	159	162	O	from
23	164	170	O	younger
24	172	179	O	subjects
NULL

Diethylpropion HCl Immediate-Release	5651	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	11
Concurrent use of phenothiazines may antagonize the anorectic effect of diethylpropion.
1	0	9	O	Concurrent
2	11	13	O	use
3	15	16	O	of
4	18	31	B-D	phenothiazines
5	33	35	O	may
6	37	46	B-E	antagonize
7	48	50	I-E	the
8	52	60	I-E	anorectic
9	62	67	I-E	effect
10	69	70	O	of
11	72	85	O	XXXXXXXX
D/4:6:1

Diethylpropion HCl Immediate-Release	5652	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	9
Concurrent use with general anesthetics may result in arrhythmias.
1	0	9	O	Concurrent
2	11	13	O	use
3	15	18	O	with
4	20	26	B-D	general
5	28	38	I-D	anesthetics
6	40	42	O	may
7	44	49	O	result
8	51	52	O	in
9	54	64	B-E	arrhythmias
D/4:9:1

Diethylpropion HCl Immediate-Release	5653	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	10
Diethylpropion hydrochloride should not be administered to patients with severe hypertension.
1	0	27	O	XXXXXXXX
2	29	34	O	should
3	36	38	O	not
4	40	41	O	be
5	43	54	O	administered
6	56	57	O	to
7	59	66	O	patients
8	68	71	O	with
9	73	78	O	severe
10	80	91	O	hypertension
NULL

Diethylpropion HCl Immediate-Release	5654	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	34
Efficacy of diethylpropion with other anorectic agents has not been studied and the combined use may have the potential for serious cardiac problems; therefore, the concomitant use with other anorectic agents is contraindicated.
1	0	7	O	Efficacy
2	9	10	O	of
3	12	25	O	XXXXXXXX
4	27	30	O	with
5	32	36	O	other
6	38	46	O	anorectic
7	48	53	O	agents
8	55	57	O	has
9	59	61	O	not
10	63	66	O	been
11	68	74	O	studied
12	76	78	O	and
13	80	82	O	the
14	84	91	O	combined
15	93	95	O	use
16	97	99	O	may
17	101	104	O	have
18	106	108	O	the
19	110	118	O	potential
20	120	122	O	for
21	124	130	O	serious
22	132	138	O	cardiac
23	140	147	O	problems
24	150	158	O	therefore
25	159	159	O	,
26	161	163	O	the
27	165	175	O	concomitant
28	177	179	O	use
29	181	184	O	with
30	186	190	O	other
31	192	200	B-U	anorectic
32	202	207	I-U	agents
33	209	210	O	is
34	212	226	B-T	contraindicated
NULL

Diethylpropion HCl Immediate-Release	5655	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	46
In general, dose selection for an elderly patient should be cautious, usually starting at the low end of the dosing range, reflecting the greater frequency of decreased hepatic, renal, or cardiac function, and of concomitant disease or other drug therapy.
1	0	1	O	In
2	3	9	O	general
3	10	10	O	,
4	12	15	O	dose
5	17	25	O	selection
6	27	29	O	for
7	31	32	O	an
8	34	40	O	elderly
9	42	48	O	patient
10	50	55	O	should
11	57	58	O	be
12	60	67	B-T	cautious
13	68	68	O	,
14	70	76	O	usually
15	78	85	O	starting
16	87	88	O	at
17	90	92	O	the
18	94	96	O	low
19	98	100	O	end
20	102	103	O	of
21	105	107	O	the
22	109	114	O	dosing
23	116	120	O	range
24	121	121	O	,
25	123	132	O	reflecting
26	134	136	O	the
27	138	144	O	greater
28	146	154	O	frequency
29	156	157	O	of
30	159	167	O	decreased
31	169	175	O	hepatic
32	176	176	O	,
33	178	182	O	renal
34	183	183	O	,
35	185	186	O	or
36	188	194	O	cardiac
37	196	203	O	function
38	204	204	O	,
39	206	208	O	and
40	210	211	O	of
41	213	223	O	concomitant
42	225	231	O	disease
43	233	234	O	or
44	236	240	O	other
45	242	245	O	drug
46	247	253	O	therapy
NULL

Diethylpropion HCl Immediate-Release	5656	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	6
Mutagenicity studies have not been conducted.
1	0	11	O	Mutagenicity
2	13	19	O	studies
3	21	24	O	have
4	26	28	O	not
5	30	33	O	been
6	35	43	O	conducted
NULL

Diethylpropion HCl Immediate-Release	5657	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	13
No long-term animal studies have been done to evaluate diethylpropion hydrochloride for carcinogenicity.
1	0	1	O	No
2	3	6	O	long
3	8	11	O	term
4	13	18	O	animal
5	20	26	O	studies
6	28	31	O	have
7	33	36	O	been
8	38	41	O	done
9	43	44	O	to
10	46	53	O	evaluate
11	55	82	O	XXXXXXXX
12	84	86	O	for
13	88	102	O	carcinogenicity
NULL

Diethylpropion HCl Immediate-Release	5658	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	16
Other reported clinical experience has not identified differences in responses between the elderly and younger patients.
1	0	4	O	Other
2	6	13	O	reported
3	15	22	O	clinical
4	24	33	O	experience
5	35	37	O	has
6	39	41	O	not
7	43	52	O	identified
8	54	64	O	differences
9	66	67	O	in
10	69	77	O	responses
11	79	85	O	between
12	87	89	O	the
13	91	97	O	elderly
14	99	101	O	and
15	103	109	O	younger
16	111	118	O	patients
NULL

Diethylpropion HCl Immediate-Release	5659	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	40
Pregnancy Category B. Reproduction studies have been performed in rats at doses up to 1.6 times the human dose (based on mg/m2) and have revealed no evidence of impaired fertility or harm to the fetus due to diethylpropion hydrochloride.
1	0	8	O	Pregnancy
2	10	17	O	Category
3	19	19	O	B
4	22	33	O	Reproduction
5	35	41	O	studies
6	43	46	O	have
7	48	51	O	been
8	53	61	O	performed
9	63	64	O	in
10	66	69	O	rats
11	71	72	O	at
12	74	78	O	doses
13	80	81	O	up
14	83	84	O	to
15	86	88	O	1.6
16	90	94	O	times
17	96	98	O	the
18	100	104	O	human
19	106	109	O	dose
20	112	116	O	based
21	118	119	O	on
22	121	122	O	mg
23	123	123	O	/
24	124	125	O	m2
25	128	130	O	and
26	132	135	O	have
27	137	144	O	revealed
28	146	147	O	no
29	149	156	O	evidence
30	158	159	O	of
31	161	168	O	impaired
32	170	178	O	fertility
33	180	181	O	or
34	183	186	O	harm
35	188	189	O	to
36	191	193	O	the
37	195	199	O	fetus
38	201	203	O	due
39	205	206	O	to
40	208	235	O	XXXXXXXX
NULL

Diethylpropion HCl Immediate-Release	5660	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	10
Reports suggest that diethylpropion hydrochloride may increase convulsions in some epileptics.
1	0	6	O	Reports
2	8	14	O	suggest
3	16	19	O	that
4	21	48	O	XXXXXXXX
5	50	52	O	may
6	54	61	B-T	increase
7	63	73	I-T	convulsions
8	75	76	O	in
9	78	81	O	some
10	83	92	B-K	epileptics
K/10:C54357

Diethylpropion HCl Immediate-Release	5661	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	29
Since diethylpropion hydrochloride and/or its metabolites have been shown to be excreted in human milk, caution should be exercised when diethylpropion hydrochloride is administered to a nursing woman.
1	0	4	O	Since
2	6	33	O	XXXXXXXX
3	35	37	O	and
4	38	38	O	/
5	39	40	O	or
6	42	44	O	its
7	46	56	O	metabolites
8	58	61	O	have
9	63	66	O	been
10	68	72	O	shown
11	74	75	O	to
12	77	78	O	be
13	80	87	O	excreted
14	89	90	O	in
15	92	96	O	human
16	98	101	O	milk
17	102	102	O	,
18	104	110	O	caution
19	112	117	O	should
20	119	120	O	be
21	122	130	O	exercised
22	132	135	O	when
23	137	164	O	XXXXXXXX
24	166	167	O	is
25	169	180	O	administered
26	182	183	O	to
27	185	185	O	a
28	187	193	O	nursing
29	195	199	O	woman
NULL

Diethylpropion HCl Immediate-Release	5662	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	32
Since safety and effectiveness in pediatric patients below the age of 16 have not been established, diethylpropion hydrochloride is not recommended for use in pediatric patients 16 years of age and under.
1	0	4	O	Since
2	6	11	O	safety
3	13	15	O	and
4	17	29	O	effectiveness
5	31	32	O	in
6	34	42	O	pediatric
7	44	51	O	patients
8	53	57	O	below
9	59	61	O	the
10	63	65	O	age
11	67	68	O	of
12	70	71	O	16
13	73	76	O	have
14	78	80	O	not
15	82	85	O	been
16	87	97	O	established
17	98	98	O	,
18	100	127	O	XXXXXXXX
19	129	130	O	is
20	132	134	O	not
21	136	146	O	recommended
22	148	150	O	for
23	152	154	O	use
24	156	157	O	in
25	159	167	O	pediatric
26	169	176	O	patients
27	178	179	O	16
28	181	185	O	years
29	187	188	O	of
30	190	192	O	age
31	194	196	O	and
32	198	202	O	under
NULL

Diethylpropion HCl Immediate-Release	5663	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	20
Spontaneous reports of congenital malformations have been recorded in humans, but no causal relationship to diethylpropion has been established.
1	0	10	O	Spontaneous
2	12	18	O	reports
3	20	21	O	of
4	23	32	O	congenital
5	34	46	O	malformations
6	48	51	O	have
7	53	56	O	been
8	58	65	O	recorded
9	67	68	O	in
10	70	75	O	humans
11	76	76	O	,
12	78	80	O	but
13	82	83	O	no
14	85	90	O	causal
15	92	103	O	relationship
16	105	106	O	to
17	108	121	O	XXXXXXXX
18	123	125	O	has
19	127	130	O	been
20	132	142	O	established
NULL

Diethylpropion HCl Immediate-Release	5664	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	20
The least amount feasible should be prescribed or dispensed at one time in order to minimize the possibility of overdosage.
1	0	2	O	The
2	4	8	O	least
3	10	15	O	amount
4	17	24	O	feasible
5	26	31	O	should
6	33	34	O	be
7	36	45	O	prescribed
8	47	48	O	or
9	50	58	O	dispensed
10	60	61	O	at
11	63	65	O	one
12	67	70	O	time
13	72	73	O	in
14	75	79	O	order
15	81	82	O	to
16	84	91	O	minimize
17	93	95	O	the
18	97	107	O	possibility
19	109	110	O	of
20	112	121	O	overdosage
NULL

Diethylpropion HCl Immediate-Release	5665	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	17
The patient should be advised to observe caution when driving or engaging in any potentially hazardous activity.
1	0	2	O	The
2	4	10	O	patient
3	12	17	O	should
4	19	20	O	be
5	22	28	O	advised
6	30	31	O	to
7	33	39	O	observe
8	41	47	O	caution
9	49	52	O	when
10	54	60	O	driving
11	62	63	O	or
12	65	72	O	engaging
13	74	75	O	in
14	77	79	O	any
15	81	91	O	potentially
16	93	101	O	hazardous
17	103	110	O	activity
NULL

Diethylpropion HCl Immediate-Release	5666	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	17
The patient should be cautioned about concomitant use of alcohol or other CNS-active drugs and diethylpropion hydrochloride.
1	0	2	O	The
2	4	10	O	patient
3	12	17	B-T	should
4	19	20	I-T	be
5	22	30	B-T	cautioned
6	32	36	O	about
7	38	48	O	concomitant
8	50	52	O	use
9	54	55	O	of
10	57	63	B-U	alcohol
11	65	66	O	or
12	68	72	O	other
13	74	76	B-U	CNS
14	78	83	I-U	active
15	85	89	O	drugs
16	91	93	O	and
17	95	122	O	XXXXXXXX
NULL

Diethylpropion HCl Immediate-Release	5667	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	33
The pressor effects of diethylpropion and those of other drugs may be additive when the drugs are used concomitantly; conversely, diethylpropion may interfere with antihypertensive drugs (i.e., guanethidine, A-methyldopa).
1	0	2	O	The
2	4	10	O	pressor
3	12	18	O	effects
4	20	21	O	of
5	23	36	O	XXXXXXXX
6	38	40	O	and
7	42	46	O	those
8	48	49	O	of
9	51	55	O	other
10	57	61	B-D	drugs
11	63	65	O	may
12	67	68	O	be
13	70	77	O	additive
14	79	82	O	when
15	84	86	O	the
16	88	92	B-D	drugs
17	94	96	O	are
18	98	101	O	used
19	103	115	O	concomitantly
20	118	127	O	conversely
21	128	128	O	,
22	130	143	O	XXXXXXXX
23	145	147	O	may
24	149	157	B-E	interfere
25	159	162	O	with
26	164	179	O	antihypertensive
27	181	185	O	drugs
28	188	190	O	i.e
29	192	192	O	,
30	194	205	O	guanethidine
31	206	206	O	,
32	208	208	O	A
33	210	219	O	methyldopa
D/10:24:1 D/16:24:1

Diethylpropion HCl Immediate-Release	5668	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	14
There are, however, no adequate and well-controlled studies in pregnant women.
1	0	4	O	There
2	6	8	O	are
3	9	9	O	,
4	11	17	O	however
5	18	18	O	,
6	20	21	O	no
7	23	30	O	adequate
8	32	34	O	and
9	36	39	O	well
10	41	50	O	controlled
11	52	58	O	studies
12	60	61	O	in
13	63	70	O	pregnant
14	72	76	O	women
NULL

Diethylpropion HCl Immediate-Release	5669	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	9
Therefore, epileptics receiving diethylpropion hydrochloride should be carefully monitored.
1	0	8	O	Therefore
2	9	9	O	,
3	11	20	O	epileptics
4	22	30	O	receiving
5	32	59	O	XXXXXXXX
6	61	66	O	should
7	68	69	O	be
8	71	79	O	carefully
9	81	89	B-T	monitored
NULL

Diethylpropion HCl Immediate-Release	5670	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	30
This drug is known to be substantially excreted by the kidney, and the risk of toxic reactions to this drug may be greater in patients with impaired renal function.
1	0	3	O	This
2	5	8	O	drug
3	10	11	O	is
4	13	17	O	known
5	19	20	O	to
6	22	23	O	be
7	25	37	O	substantially
8	39	46	O	excreted
9	48	49	O	by
10	51	53	O	the
11	55	60	O	kidney
12	61	61	O	,
13	63	65	O	and
14	67	69	O	the
15	71	74	O	risk
16	76	77	O	of
17	79	83	O	toxic
18	85	93	O	reactions
19	95	96	O	to
20	98	101	O	this
21	103	106	O	drug
22	108	110	O	may
23	112	113	O	be
24	115	121	O	greater
25	123	124	O	in
26	126	133	O	patients
27	135	138	O	with
28	140	147	O	impaired
29	149	153	O	renal
30	155	162	O	function
NULL

Diethylpropion HCl Immediate-Release	5671	42232-9	a750a1ef-8f3e-4870-97eb-3697aaca1818	10
Titration of dose or discontinuance of diethylpropion hydrochloride may be necessary.
1	0	8	O	Titration
2	10	11	O	of
3	13	16	O	dose
4	18	19	O	or
5	21	34	O	discontinuance
6	36	37	O	of
7	39	66	O	XXXXXXXX
8	68	70	O	may
9	72	73	O	be
10	75	83	O	necessary
NULL

