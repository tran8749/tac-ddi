<?xml version="1.0" ?>
<Label drug="Clozapine" setid="5f0c6f5f-b906-4c8f-8580-3939a476a1c1">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7     DRUG INTERACTIONS  Concomitant use of Strong CYP1A2 Inhibitors: Reduce CLOZARIL dose to one-third when coadministered with strong CYP1A2 inhibitors (e.g., fluvoxamine, ciprofloxacin, enoxacin). ( 2.6 , 7.1 )  Concomitant use of Strong CYP3A4 Inducers is not recommended. ( 2.6 , 7.1 )  Discontinuation of CYP1A2 or CYP3A4 Inducers: Consider reducing CLOZARIL dose when CYP1A2 inducers (e.g., tobacco smoke) or CYP3A4 inducers (e.g., carbamazepine) are discontinued. ( 2.6 , 7.1 )  7.1     Potential for Other Drugs to Affect CLOZARIL  Clozapine is a substrate for many cytochrome P450 isozymes, in particular CYP1A2, CYP3A4, and CYP2D6. Use caution when administering CLOZARIL concomitantly with drugs that are inducers or inhibitors of these enzymes.  CYP1A2 Inhibitors  Concomitant use of CLOZARIL and CYP1A2 inhibitors can increase plasma levels of clozapine, potentially resulting in adverse reactions. Reduce the CLOZARIL dose to one-third of the original dose when CLOZARIL is coadministered with strong CYP1A2 inhibitors (e.g., fluvoxamine, ciprofloxacin, or enoxacin). The CLOZARIL dose should be increased to the original dose when coadministration of strong CYP1A2 inhibitors is discontinued [see Dosage and Administration (2.6), Clinical Pharmacology (12.3)] .  Moderate or weak CYP1A2 inhibitors include oral contraceptives and caffeine. Monitor patients closely when CLOZARIL is coadministered with these inhibitors. Consider reducing the CLOZARIL dosage if necessary [see Dosage and Administration (2.6)] .  CYP2D6 and CYP3A4 Inhibitors  Concomitant treatment with CLOZARIL and CYP2D6 or CYP3A4 inhibitors (e.g., cimetidine, escitalopram, erythromycin, paroxetine, bupropion, fluoxetine, quinidine, duloxetine, terbinafine, or sertraline) can increase clozapine levels and lead to adverse reactions [see Clinical Pharmacology (12.3)] . Use caution and monitor patients closely when using such inhibitors. Consider reducing the CLOZARIL dose [see Dosage and Administration (2.6)] .  CYP1A2 and CYP3A4 Inducers  Concomitant treatment with drugs that induce CYP1A2 or CYP3A4 can decrease the plasma concentration of clozapine, resulting in decreased effectiveness of CLOZARIL. Tobacco smoke is a moderate inducer of CYP1A2. Strong CYP3A4 inducers include carbamazepine, phenytoin, St. John’s wort, and rifampin. It may be necessary to increase the CLOZARIL dose if used concomitantly with inducers of these enzymes. However, concomitant use of CLOZARIL and strong CYP3A4 inducers is not recommended [see Dosage and Administration (2.6)] .  Consider reducing the CLOZARIL dosage when discontinuing coadministered enzyme inducers; because discontinuation of inducers can result in increased clozapine plasma levels and an increased risk of adverse reactions [see Dosage and Administration (2.6)] .  Drugs that Cause QT Interval Prolongation  Use caution when administering concomitant medications that prolong the QT interval or inhibit the metabolism of clozapine. Drugs that cause QT prolongation include: specific antipsychotics (e.g., ziprasidone, iloperidone, chlorpromazine, thioridazine, mesoridazine, droperidol, and pimozide), specific antibiotics (e.g., erythromycin, gatifloxacin, moxifloxacin, sparfloxacin), Class 1A antiarrhythmics (e.g., quinidine, procainamide) or Class III antiarrhythmics (e.g., amiodarone, sotalol), and others (e.g., pentamidine, levomethadyl acetate, methadone, halofantrine, mefloquine, dolasetron mesylate, probucol or tacrolimus) [see Warnings and Precautions (5.8)] .  7.2     Potential for CLOZARIL to Affect Other Drugs  Concomitant use of CLOZARIL with other drugs metabolized by CYP2D6 can increase levels of these CYP2D6 substrates. Use caution when coadministering CLOZARIL with other drugs that are metabolized by CYP2D6. It may be necessary to use lower doses of such drugs than usually prescribed. Such drugs include specific antidepressants, phenothiazines, carbamazepine, and Type 1C antiarrhythmics (e.g., propafenone, flecainide, and encainide).</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12     CLINICAL PHARMACOLOGY  12.1     Mechanism of Action  The mechanism of action of clozapine is unknown. However, it has been proposed that the therapeutic efficacy of clozapine in schizophrenia is mediated through antagonism of the dopamine type 2 (D 2 ) and the serotonin type 2A (5-HT 2A ) receptors. CLOZARIL also acts as an antagonist at adrenergic, cholinergic, histaminergic and other dopaminergic and serotonergic receptors.  12.2     Pharmacodynamics  Clozapine demonstrated binding affinity to the following receptors: histamine H 1 (K i 1.1 nM), adrenergic α 1A (K i 1.6 nM), serotonin 5-HT 6 (K i 4 nM), serotonin 5-HT 2A (K i 5.4 nM), muscarinic M 1 (K i 6.2 nM), serotonin 5-HT 7 (K i 6.3 nM), serotonin 5-HT 2C (K i 9.4 nM), dopamine D 4 (K i 24 nM), adrenergic α 2A (K i 90 nM), serotonin 5-HT 3 (K i 95 nM), serotonin 5-HT 1A (K i 120 nM), dopamine D 2 (K i 160 nM), dopamine D 1 (K i 270 nM), dopamine D 5 (K i 454 nM), and dopamine D 3 (K i 555 nM).  Clozapine causes little or no prolactin elevation.  Clinical electroencephalogram (EEG) studies demonstrated that clozapine increases delta and theta activity and slows dominant alpha frequencies. Enhanced synchronization occurs. Sharp wave activity and spike and wave complexes may also develop. Patients have reported an intensification of dream activity during clozapine therapy. REM sleep was found to be increased to 85% of the total sleep time. In these patients, the onset of REM sleep occurred almost immediately after falling asleep.  12.3     Pharmacokinetics  Absorption  In humans, CLOZARIL tablets (25 mg and 100 mg) are equally bioavailable relative to a CLOZARIL solution. Following oral administration of CLOZARIL 100 mg twice daily, the average steady-state peak plasma concentration was 319 ng/mL (range: 102 to 771 ng/mL), occurring at the average of 2.5 hours (range: 1 to 6 hours) after dosing. The average minimum concentration at steady state was 122 ng/mL (range: 41 to 343 ng/mL), after 100 mg twice daily dosing. Food does not appear to affect the systemic bioavailability of CLOZARIL. Thus, CLOZARIL may be administered with or without food.  Distribution  Clozapine is approximately 97% bound to serum proteins. The interaction between clozapine and other highly protein-bound drugs has not been fully evaluated but may be important [see Drug Interactions (7)] .  Metabolism and Excretion  Clozapine is almost completely metabolized prior to excretion, and only trace amounts of unchanged drug are detected in the urine and feces. Clozapine is a substrate for many cytochrome P450 isozymes, in particular CYP1A2, CYP2D6, and CYP3A4. Approximately 50% of the administered dose is excreted in the urine and 30% in the feces. The demethylated, hydroxylated, and N-oxide derivatives are components in both urine and feces. Pharmacological testing has shown the desmethyl metabolite (norclozapine) to have only limited activity, while the hydroxylated and N- oxide derivatives were inactive. The mean elimination half-life of clozapine after a single 75 mg dose was 8 hours (range: 4 to12 hours), compared to a mean elimination half-life of 12 hours (range: 4 to 66 hours), after achieving steady state with 100 mg twice daily dosing.  A comparison of single-dose and multiple-dose administration of clozapine demonstrated that the elimination half-life increased significantly after multiple dosing relative to that after single-dose administration, suggesting the possibility of concentration-dependent pharmacokinetics. However, at steady state, approximately dose-proportional changes with respect to AUC (area under the curve), peak, and minimum clozapine plasma concentrations were observed after administration of 37.5, 75, and 150 mg twice daily.  Drug-Drug Interaction Studies  Fluvoxamine  A pharmacokinetic study was conducted in 16 schizophrenic patients who received clozapine under steady-state conditions. After coadministration of fluvoxamine for 14 days, mean trough concentrations of clozapine and its metabolites, N -desmethylclozapine and clozapine N -oxide, were elevated about 3-fold compared to baseline steady-state concentrations.  Paroxetine, Fluoxetine, and Sertraline  In a study of schizophrenic patients (n=14) who received clozapine under steady-state conditions, coadministration of paroxetine produced only minor changes in the levels of clozapine and its metabolites. However, other published reports describe modest elevations (less than 2-fold) of clozapine and metabolite concentrations when clozapine was taken with paroxetine, fluoxetine, and sertraline.  Specific Population Studies  Renal or Hepatic Impairment  No specific pharmacokinetic studies were conducted to investigate the effects of renal or hepatic impairment on the pharmacokinetics of clozapine. Higher clozapine plasma concentrations are likely in patients with significant renal or hepatic impairment when given usual doses.  CYP2D6 Poor Metabolizers  A subset (3%–10%) of the population has reduced activity of CYP2D6 (CYP2D6 poor metabolizers). These individuals may develop higher than expected plasma concentrations of clozapine when given usual doses.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Clozapine" id="2980" section="34073-7">
      

      <SentenceText>Concomitant use of Strong CYP1A2 Inhibitors: Reduce CLOZARIL dose to one-third when coadministered with strong CYP1A2 inhibitors (e.g., fluvoxamine, ciprofloxacin, enoxacin).</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="26 17" str="CYP1A2 Inhibitors" type="Precipitant"/>
      <Interaction id="I1" precipitant="M1" trigger="M1" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M2" span="111 17" str="CYP1A2 inhibitors" type="Precipitant"/>
      <Interaction id="I2" precipitant="M2" trigger="M2" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2981" section="34073-7">
      

      <SentenceText>Concomitant use of Strong CYP3A4 Inducers is not recommended.</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="45 15" str="not recommended" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="19 22" str="Strong CYP3A4 Inducers" type="Precipitant"/>
      <Interaction id="I3" precipitant="M4" trigger="M3" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2982" section="34073-7">
      

      <SentenceText>Discontinuation of CYP1A2 or CYP3A4 Inducers: Consider reducing CLOZARIL dose when CYP1A2 inducers (e.g., tobacco smoke) or CYP3A4 inducers (e.g., carbamazepine) are discontinued.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="0 15" str="Discontinuation" type="Trigger"/>
      <Mention code="NO MAP" id="M6" span="124 15" str="CYP3A4 inducers" type="Precipitant"/>
      <Interaction id="I4" precipitant="M6" trigger="M5" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M7" span="147 13" str="carbamazepine" type="Precipitant"/>
      <Interaction id="I5" precipitant="M7" trigger="M5" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2983" section="34073-7">
      

      <SentenceText>Clozapine is a substrate for many cytochrome P450 isozymes, in particular CYP1A2, CYP3A4, and CYP2D6.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2984" section="34073-7">
      

      <SentenceText>Use caution when administering CLOZARIL concomitantly with drugs that are inducers or inhibitors of these enzymes.</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="0 3" str="Use" type="Trigger"/>
      <Mention code="NO MAP" id="M9" span="4 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="74 8" str="inducers" type="Precipitant"/>
      <Interaction id="I6" precipitant="M10" trigger="M9" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M11" span="97 16" str="of these enzymes" type="Precipitant"/>
      <Interaction id="I7" precipitant="M11" trigger="M8" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2985" section="34073-7">
      

      <SentenceText>CYP1A2 Inhibitors Concomitant use of CLOZARIL and CYP1A2 inhibitors can increase plasma levels of clozapine, potentially resulting in adverse reactions.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="72 8" str="increase" type="Trigger"/>
      <Mention code="NO MAP" id="M13" span="50 17" str="CYP1A2 inhibitors" type="Precipitant"/>
      <Interaction effect="C54355" id="I8" precipitant="M13" trigger="M12" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2986" section="34073-7">
      

      <SentenceText>Reduce the CLOZARIL dose to one-third of the original dose when CLOZARIL is coadministered with strong CYP1A2 inhibitors (e.g., fluvoxamine, ciprofloxacin, or enoxacin).</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="103 17" str="CYP1A2 inhibitors" type="Precipitant"/>
      <Interaction id="I9" precipitant="M14" trigger="M14" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M15" span="141 13" str="ciprofloxacin" type="Precipitant"/>
      <Interaction id="I10" precipitant="M15" trigger="M15" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2987" section="34073-7">
      

      <SentenceText>The CLOZARIL dose should be increased to the original dose when coadministration of strong CYP1A2 inhibitors is discontinued.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="28 9" str="increased" type="Trigger"/>
      <Mention code="NO MAP" id="M17" span="4 8" str="CLOZARIL" type="Precipitant"/>
      <Interaction id="I11" precipitant="M17" trigger="M16" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2988" section="34073-7">
      

      <SentenceText>Moderate or weak CYP1A2 inhibitors include oral contraceptives and caffeine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2989" section="34073-7">
      

      <SentenceText>Monitor patients closely when CLOZARIL is coadministered with these inhibitors.</SentenceText>
      

      <Mention code="NO MAP" id="M18" span="0 7" str="Monitor" type="Trigger"/>
      <Mention code="NO MAP" id="M19" span="68 10" str="inhibitors" type="Precipitant"/>
      <Interaction id="I12" precipitant="M19" trigger="M18" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2990" section="34073-7">
      

      <SentenceText>Consider reducing the CLOZARIL dosage if necessary.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="9 28" str="reducing the CLOZARIL dosage" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2991" section="34073-7">
      

      <SentenceText>CYP2D6 and CYP3A4 Inhibitors Concomitant treatment with CLOZARIL and CYP2D6 or CYP3A4 inhibitors (e.g., cimetidine, escitalopram, erythromycin, paroxetine, bupropion, fluoxetine, quinidine, duloxetine, terbinafine, or sertraline) can increase clozapine levels and lead to adverse reactions.</SentenceText>
      

      <Mention code="NO MAP" id="M21" span="0 6" str="CYP2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I13" precipitant="M21" trigger="M21" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M22" span="56 8" str="CLOZARIL" type="Precipitant"/>
      <Interaction effect="C54355" id="I14" precipitant="M22" trigger="M22" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M23" span="69 9" str="CYP2D6 or" type="Precipitant"/>
      <Interaction effect="C54355" id="I15" precipitant="M23" trigger="M23" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M24" span="79 17" str="CYP3A4 inhibitors" type="Precipitant"/>
      <Interaction effect="C54355" id="I16" precipitant="M24" trigger="M24" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2992" section="34073-7">
      

      <SentenceText>Use caution and monitor patients closely when using such inhibitors.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="0 3" str="Use" type="Trigger"/>
      <Mention code="NO MAP" id="M26" span="4 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M27" span="16 7" str="monitor" type="Trigger"/>
      <Mention code="NO MAP" id="M28" span="57 10" str="inhibitors" type="Precipitant"/>
      <Interaction id="I17" precipitant="M28" trigger="M27" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2993" section="34073-7">
      

      <SentenceText>Consider reducing the CLOZARIL dose.</SentenceText>
      

      <Mention code="NO MAP" id="M29" span="9 12" str="reducing the" type="Trigger"/>
      <Mention code="NO MAP" id="M30" span="31 4" str="dose" type="Trigger"/>
      <Mention code="NO MAP" id="M31" span="22 8" str="CLOZARIL" type="Precipitant"/>
      <Interaction id="I18" precipitant="M31" trigger="M30" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2994" section="34073-7">
      

      <SentenceText>CYP1A2 and CYP3A4 Inducers Concomitant treatment with drugs that induce CYP1A2 or CYP3A4 can decrease the plasma concentration of clozapine, resulting in decreased effectiveness of CLOZARIL.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="93 19" str="decrease the plasma" type="Trigger"/>
      <Mention code="NO MAP" id="M33" span="154 9" str="decreased" type="Trigger"/>
      <Mention code="NO MAP" id="M34" span="164 25" str="effectiveness of CLOZARIL" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2995" section="34073-7">
      

      <SentenceText>Tobacco smoke is a moderate inducer of CYP1A2.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2996" section="34073-7">
      

      <SentenceText>Strong CYP3A4 inducers include carbamazepine, phenytoin, St. John’s wort, and rifampin.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="0 22" str="Strong CYP3A4 inducers" type="Precipitant"/>
      <Interaction effect="C54356" id="I19" precipitant="M35" trigger="M35" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2997" section="34073-7">
      

      <SentenceText>It may be necessary to increase the CLOZARIL dose if used concomitantly with inducers of these enzymes.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="23 8" str="increase" type="Trigger"/>
      <Mention code="NO MAP" id="M37" span="95 7" str="enzymes" type="Precipitant"/>
      <Interaction id="I20" precipitant="M37" trigger="M36" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2998" section="34073-7">
      

      <SentenceText>However, concomitant use of CLOZARIL and strong CYP3A4 inducers is not recommended.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="67 15" str="not recommended" type="Trigger"/>
      <Mention code="NO MAP" id="M39" span="28 8" str="CLOZARIL" type="Precipitant"/>
      <Interaction id="I21" precipitant="M39" trigger="M38" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M40" span="48 15" str="CYP3A4 inducers" type="Precipitant"/>
      <Interaction id="I22" precipitant="M40" trigger="M38" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="2999" section="34073-7">
      

      <SentenceText>Consider reducing the CLOZARIL dosage when discontinuing coadministered enzyme inducers; because discontinuation of inducers can result in increased clozapine plasma levels and an increased risk of adverse reactions.</SentenceText>
      

      <Mention code="NO MAP" id="M41" span="9 8" str="reducing" type="Trigger"/>
      <Mention code="NO MAP" id="M42" span="72 15" str="enzyme inducers" type="Precipitant"/>
      <Interaction effect="C54356" id="I23" precipitant="M42" trigger="M41" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M43" span="116 8" str="inducers" type="Precipitant"/>
      <Interaction effect="C54356" id="I24" precipitant="M43" trigger="M41" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3000" section="34073-7">
      

      <SentenceText>Drugs that Cause QT Interval Prolongation Use caution when administering concomitant medications that prolong the QT interval or inhibit the metabolism of clozapine.</SentenceText>
      

      <Mention code="NO MAP" id="M44" span="29 12" str="Prolongation" type="Trigger"/>
      <Mention code="NO MAP" id="M45" span="42 11" str="Use caution" type="Trigger"/>
      <Mention code="NO MAP" id="M46" span="129 22" str="inhibit the metabolism" type="Trigger"/>
      <Mention code="NO MAP" id="M47" span="0 28" str="Drugs that Cause QT Interval" type="Precipitant"/>
      <Interaction id="I25" precipitant="M47" trigger="M46" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M48" span="85 40" str="medications that prolong the QT interval" type="Precipitant"/>
      <Interaction id="I26" precipitant="M48" trigger="M45" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3001" section="34073-7">
      

      <SentenceText>Drugs that cause QT prolongation include: specific antipsychotics (e.g., ziprasidone, iloperidone, chlorpromazine, thioridazine, mesoridazine, droperidol, and pimozide), specific antibiotics (e.g., erythromycin, gatifloxacin, moxifloxacin, sparfloxacin), Class 1A antiarrhythmics (e.g., quinidine, procainamide) or Class III antiarrhythmics (e.g., amiodarone, sotalol), and others (e.g., pentamidine, levomethadyl acetate, methadone, halofantrine, mefloquine, dolasetron mesylate, probucol or tacrolimus).</SentenceText>
      

      <Mention code="NO MAP" id="M49" span="0 19" str="Drugs that cause QT" type="Precipitant"/>
      <Interaction id="I27" precipitant="M49" trigger="M49" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3002" section="34073-7">
      

      <SentenceText>Concomitant use of CLOZARIL with other drugs metabolized by CYP2D6 can increase levels of these CYP2D6 substrates.</SentenceText>
      

      <Mention code="NO MAP" id="M50" span="71 15" str="increase levels" type="Trigger"/>
      <Mention code="NO MAP" id="M51" span="39 27" str="drugs metabolized by CYP2D6" type="Precipitant"/>
      <Interaction effect="C54357" id="I28" precipitant="M51" trigger="M50" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M52" span="96 17" str="CYP2D6 substrates" type="Precipitant"/>
      <Interaction effect="C54357" id="I29" precipitant="M52" trigger="M50" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3003" section="34073-7">
      

      <SentenceText>Use caution when coadministering CLOZARIL with other drugs that are metabolized by CYP2D6.</SentenceText>
      

      <Mention code="NO MAP" id="M53" span="0 3" str="Use" type="Trigger"/>
      <Mention code="NO MAP" id="M54" span="4 7" str="caution" type="Trigger"/>
      <Mention code="NO MAP" id="M55" span="53 36" str="drugs that are metabolized by CYP2D6" type="Precipitant"/>
      <Interaction id="I30" precipitant="M55" trigger="M54" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3004" section="34073-7">
      

      <SentenceText>It may be necessary to use lower doses of such drugs than usually prescribed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3005" section="34073-7">
      

      <SentenceText>Such drugs include specific antidepressants, phenothiazines, carbamazepine, and Type 1C antiarrhythmics (e.g., propafenone, flecainide, and encainide).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3006" section="34090-1">
      

      <SentenceText>The mechanism of action of clozapine is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3007" section="34090-1">
      

      <SentenceText>However, it has been proposed that the therapeutic efficacy of clozapine in schizophrenia is mediated through antagonism of the dopamine type 2 (D2) and the serotonin type 2A (5-HT2A) receptors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3008" section="34090-1">
      

      <SentenceText>CLOZARIL also acts as an antagonist at adrenergic, cholinergic, histaminergic and other dopaminergic and serotonergic receptors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3009" section="34090-1">
      

      <SentenceText>Clozapine demonstrated binding affinity to the following receptors: histamine H1 (Ki 1.1 nM), adrenergic α1A (Ki 1.6 nM), serotonin 5-HT6 (Ki 4 nM), serotonin 5-HT2A (Ki 5.4 nM), muscarinic M1 (Ki 6.2 nM), serotonin 5-HT7 (Ki 6.3 nM), serotonin 5-HT2C (Ki 9.4 nM), dopamine D4 (Ki 24 nM), adrenergic α2A (Ki 90 nM), serotonin 5-HT3 (Ki 95 nM), serotonin 5-HT1A (Ki 120 nM), dopamine D2 (Ki 160 nM), dopamine D1 (Ki 270 nM), dopamine D5 (Ki 454 nM), and dopamine D3 (Ki 555 nM).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3010" section="34090-1">
      

      <SentenceText>Clozapine causes little or no prolactin elevation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3011" section="34090-1">
      

      <SentenceText>Clinical electroencephalogram (EEG) studies demonstrated that clozapine increases delta and theta activity and slows dominant alpha frequencies.</SentenceText>
      

      <Mention code="NO MAP" id="M56" span="72 9" str="increases" type="Trigger"/>
      <Mention code="NO MAP" id="M57" span="82 5" str="delta" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M58" span="92 14" str="theta activity" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M59" span="117 14" str="dominant alpha" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3012" section="34090-1">
      

      <SentenceText>Sharp wave activity and spike and wave complexes may also develop.</SentenceText>
      

      <Mention code="NO MAP" id="M60" span="0 19" str="Sharp wave activity" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3013" section="34090-1">
      

      <SentenceText>Patients have reported an intensification of dream activity during clozapine therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3014" section="34090-1">
      

      <SentenceText>REM sleep was found to be increased to 85% of the total sleep time.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3015" section="34090-1">
      

      <SentenceText>In these patients, the onset of REM sleep occurred almost immediately after falling asleep.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3016" section="34090-1">
      

      <SentenceText>Absorption In humans, CLOZARIL tablets (25 mg and 100 mg) are equally bioavailable relative to a CLOZARIL solution.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3017" section="34090-1">
      

      <SentenceText>Following oral administration of CLOZARIL 100 mg twice daily, the average steady-state peak plasma concentration was 319 ng/mL (range: 102 to 771 ng/mL), occurring at the average of 2.5 hours (range: 1 to 6 hours) after dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3018" section="34090-1">
      

      <SentenceText>The average minimum concentration at steady state was 122 ng/mL (range: 41 to 343 ng/mL), after 100 mg twice daily dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3019" section="34090-1">
      

      <SentenceText>Food does not appear to affect the systemic bioavailability of CLOZARIL.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3020" section="34090-1">
      

      <SentenceText>Thus, CLOZARIL may be administered with or without food.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3021" section="34090-1">
      

      <SentenceText>Distribution Clozapine is approximately 97% bound to serum proteins.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3022" section="34090-1">
      

      <SentenceText>The interaction between clozapine and other highly protein-bound drugs has not been fully evaluated but may be important.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3023" section="34090-1">
      

      <SentenceText>Metabolism and Excretion Clozapine is almost completely metabolized prior to excretion, and only trace amounts of unchanged drug are detected in the urine and feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3024" section="34090-1">
      

      <SentenceText>Clozapine is a substrate for many cytochrome P450 isozymes, in particular CYP1A2, CYP2D6, and CYP3A4.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3025" section="34090-1">
      

      <SentenceText>Approximately 50% of the administered dose is excreted in the urine and 30% in the feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3026" section="34090-1">
      

      <SentenceText>The demethylated, hydroxylated, and N-oxide derivatives are components in both urine and feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3027" section="34090-1">
      

      <SentenceText>Pharmacological testing has shown the desmethyl metabolite (norclozapine) to have only limited activity, while the hydroxylated and N-oxide derivatives were inactive.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3028" section="34090-1">
      

      <SentenceText>The mean elimination half-life of clozapine after a single 75 mg dose was 8 hours (range: 4 to12 hours), compared to a mean elimination half-life of 12 hours (range: 4 to 66 hours), after achieving steady state with 100 mg twice daily dosing.</SentenceText>
      

      <Mention code="NO MAP" id="M61" span="9 11" str="elimination" type="Trigger"/>
      <Mention code="NO MAP" id="M62" span="124 11" str="elimination" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3029" section="34090-1">
      

      <SentenceText>A comparison of single-dose and multiple-dose administration of clozapine demonstrated that the elimination half-life increased significantly after multiple dosing relative to that after single-dose administration, suggesting the possibility of concentration-dependent pharmacokinetics.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3030" section="34090-1">
      

      <SentenceText>However, at steady state, approximately dose-proportional changes with respect to AUC (area under the curve), peak, and minimum clozapine plasma concentrations were observed after administration of 37.5, 75, and 150 mg twice daily.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3031" section="34090-1">
      

      <SentenceText>Drug-Drug Interaction Studies Fluvoxamine A pharmacokinetic study was conducted in 16 schizophrenic patients who received clozapine under steady-state conditions.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3032" section="34090-1">
      

      <SentenceText>After coadministration of fluvoxamine for 14 days, mean trough concentrations of clozapine and its metabolites, N-desmethylclozapine and clozapine N-oxide, were elevated about 3-fold compared to baseline steady-state concentrations.</SentenceText>
      

      <Mention code="NO MAP" id="M63" span="211 20" str="state concentrations" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3033" section="34090-1">
      

      <SentenceText>Paroxetine, Fluoxetine, and Sertraline In a study of schizophrenic patients (n=14) who received clozapine under steady-state conditions, coadministration of paroxetine produced only minor changes in the levels of clozapine and its metabolites.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3034" section="34090-1">
      

      <SentenceText>However, other published reports describe modest elevations (less than 2-fold) of clozapine and metabolite concentrations when clozapine was taken with paroxetine, fluoxetine, and sertraline.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3035" section="34090-1">
      

      <SentenceText>Specific Population Studies Renal or Hepatic Impairment No specific pharmacokinetic studies were conducted to investigate the effects of renal or hepatic impairment on the pharmacokinetics of clozapine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3036" section="34090-1">
      

      <SentenceText>Higher clozapine plasma concentrations are likely in patients with significant renal or hepatic impairment when given usual doses.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3037" section="34090-1">
      

      <SentenceText>CYP2D6 Poor Metabolizers A subset (3%–10%) of the population has reduced activity of CYP2D6 (CYP2D6 poor metabolizers).</SentenceText>
      

      <Mention code="NO MAP" id="M64" span="65 16" str="reduced activity" type="Trigger"/>
      <Mention code="NO MAP" id="M65" span="85 14" str="CYP2D6 (CYP2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I31" precipitant="M65" trigger="M64" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Clozapine" id="3038" section="34090-1">
      

      <SentenceText>These individuals may develop higher than expected plasma concentrations of clozapine when given usual doses.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

