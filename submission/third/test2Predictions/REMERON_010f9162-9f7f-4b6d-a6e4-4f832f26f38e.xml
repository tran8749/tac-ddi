<?xml version="1.0" ?>
<Label drug="REMERON" setid="010f9162-9f7f-4b6d-a6e4-4f832f26f38e">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY  Pharmacodynamics  The mechanism of action of REMERON (mirtazapine) Tablets, as with other drugs effective in the treatment of major depressive disorder, is unknown.  Evidence gathered in preclinical studies suggests that mirtazapine enhances central noradrenergic and serotonergic activity. These studies have shown that mirtazapine acts as an antagonist at central presynaptic α 2 –adrenergic inhibitory autoreceptors and heteroreceptors, an action that is postulated to result in an increase in central noradrenergic and serotonergic activity.  Mirtazapine is a potent antagonist of 5-HT 2 and 5-HT 3 receptors. Mirtazapine has no significant affinity for the 5-HT 1A and 5-HT 1B receptors.  Mirtazapine is a potent antagonist of histamine (H 1 ) receptors, a property that may explain its prominent sedative effects.  Mirtazapine is a moderate peripheral α 1 –adrenergic antagonist, a property that may explain the occasional orthostatic hypotension reported in association with its use.  Mirtazapine is a moderate antagonist at muscarinic receptors, a property that may explain the relatively low incidence of anticholinergic side effects associated with its use.  Pharmacokinetics  REMERON (mirtazapine) Tablets are rapidly and completely absorbed following oral administration and have a half-life of about 20 to 40 hours. Peak plasma concentrations are reached within about 2 hours following an oral dose. The presence of food in the stomach has a minimal effect on both the rate and extent of absorption and does not require a dosage adjustment.  Mirtazapine is extensively metabolized after oral administration. Major pathways of biotransformation are demethylation and hydroxylation followed by glucuronide conjugation. In vitro data from human liver microsomes indicate that cytochrome 2D6 and 1A2 are involved in the formation of the 8-hydroxy metabolite of mirtazapine, whereas cytochrome 3A is considered to be responsible for the formation of the N-desmethyl and N-oxide metabolite. Mirtazapine has an absolute bioavailability of about 50%. It is eliminated predominantly via urine (75%) with 15% in feces. Several unconjugated metabolites possess pharmacological activity but are present in the plasma at very low levels. The (–) enantiomer has an elimination half-life that is approximately twice as long as the (+) enantiomer and therefore achieves plasma levels that are about 3 times as high as that of the (+) enantiomer.  Plasma levels are linearly related to dose over a dose range of 15 to 80 mg. The mean elimination half-life of mirtazapine after oral administration ranges from approximately 20 to 40 hours across age and gender subgroups, with females of all ages exhibiting significantly longer elimination half-lives than males (mean half-life of 37 hours for females vs. 26 hours for males). Steady state plasma levels of mirtazapine are attained within 5 days, with about 50% accumulation (accumulation ratio = 1.5).  Mirtazapine is approximately 85% bound to plasma proteins over a concentration range of 0.01 to 10 mcg/mL.  Special Populations  Geriatric  Following oral administration of REMERON (mirtazapine) Tablets 20 mg/day for 7 days to subjects of varying ages (range, 25–74), oral clearance of mirtazapine was reduced in the elderly compared to the younger subjects. The differences were most striking in males, with a 40% lower clearance in elderly males compared to younger males, while the clearance in elderly females was only 10% lower compared to younger females. Caution is indicated in administering REMERON to elderly patients (see PRECAUTIONS and DOSAGE AND ADMINISTRATION ).  Pediatrics  Safety and effectiveness of mirtazapine in the pediatric population have not been established (see PRECAUTIONS ).  Gender  The mean elimination half-life of mirtazapine after oral administration ranges from approximately 20 to 40 hours across age and gender subgroups, with females of all ages exhibiting significantly longer elimination half-lives than males (mean half-life of 37 hours for females vs. 26 hours for males) (see Pharmacokinetics ).  Race  There have been no clinical studies to evaluate the effect of race on the pharmacokinetics of REMERON.  Renal Insufficiency  The disposition of mirtazapine was studied in patients with varying degrees of renal function. Elimination of mirtazapine is correlated with creatinine clearance. Total body clearance of mirtazapine was reduced approximately 30% in patients with moderate (Clcr=11–39 mL/min/1.73 m 2 ) and approximately 50% in patients with severe (Clcr=&lt;10 mL/min/1.73 m 2 ) renal impairment when compared to normal subjects. Caution is indicated in administering REMERON to patients with compromised renal function (see PRECAUTIONS and DOSAGE AND ADMINISTRATION ).  Hepatic Insufficiency  Following a single 15-mg oral dose of REMERON, the oral clearance of mirtazapine was decreased by approximately 30% in hepatically impaired patients compared to subjects with normal hepatic function. Caution is indicated in administering REMERON to patients with compromised hepatic function (see PRECAUTIONS and DOSAGE AND ADMINISTRATION ).  Clinical Trials Showing Effectiveness  The efficacy of REMERON (mirtazapine) Tablets as a treatment for major depressive disorder was established in 4 placebo-controlled, 6-week trials in adult outpatients meeting DSM-III criteria for major depressive disorder. Patients were titrated with mirtazapine from a dose range of 5 mg up to 35 mg/day. Overall, these studies demonstrated mirtazapine to be superior to placebo on at least 3 of the following 4 measures: 21-Item Hamilton Depression Rating Scale (HDRS) total score; HDRS Depressed Mood Item; CGI Severity score; and Montgomery and Asberg Depression Rating Scale (MADRS). Superiority of mirtazapine over placebo was also found for certain factors of the HDRS, including anxiety/somatization factor and sleep disturbance factor. The mean mirtazapine dose for patients who completed these 4 studies ranged from 21 to 32 mg/day. A fifth study of similar design utilized a higher dose (up to 50 mg) per day and also showed effectiveness.  Examination of age and gender subsets of the population did not reveal any differential responsiveness on the basis of these subgroupings.  In a longer-term study, patients meeting (DSM-IV) criteria for major depressive disorder who had responded during an initial 8 to 12 weeks of acute treatment on REMERON were randomized to continuation of REMERON or placebo for up to 40 weeks of observation for relapse. Response during the open phase was defined as having achieved a HAM-D 17 total score of ≤8 and a CGI-Improvement score of 1 or 2 at 2 consecutive visits beginning with week 6 of the 8 to 12 weeks in the open-label phase of the study. Relapse during the double-blind phase was determined by the individual investigators. Patients receiving continued REMERON treatment experienced significantly lower relapse rates over the subsequent 40 weeks compared to those receiving placebo. This pattern was demonstrated in both male and female patients.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="REMERON" id="3431" section="34073-7">
      

      <SentenceText>As with other drugs, the potential for interaction by a variety of mechanisms (e.g., pharmacodynamic, pharmacokinetic inhibition or enhancement, etc.) is a possibility.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3433" section="34073-7">
      

      <SentenceText>The metabolism and pharmacokinetics of REMERON (mirtazapine) Tablets may be affected by the induction or inhibition of drug-metabolizing enzymes.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3434" section="34073-7">
      

      <SentenceText>(these studies used both drugs at steady state) In healthy male patients (n=18), phenytoin (200 mg daily) increased mirtazapine (30 mg daily) clearance about 2-fold, resulting in a decrease in average plasma mirtazapine concentrations of 45%.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="181 8" str="decrease" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="201 6" str="plasma" type="Trigger"/>
      <Mention code="NO MAP" id="M3" span="220 14" str="concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="81 9" str="phenytoin" type="Precipitant"/>
      <Interaction effect="C54602" id="I1" precipitant="M4" trigger="M3" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I2" precipitant="M4" trigger="M3" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3435" section="34073-7">
      

      <SentenceText>Mirtazapine did not significantly affect the pharmacokinetics of phenytoin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3436" section="34073-7">
      

      <SentenceText>In healthy male patients (n=24), carbamazepine (400 mg b.i.d.) clearance about 2-fold, resulting in a decrease in average plasma mirtazapine concentrations of 60%.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="102 8" str="decrease" type="Trigger"/>
      <Mention code="NO MAP" id="M6" span="122 6" str="plasma" type="Trigger"/>
      <Mention code="NO MAP" id="M7" span="141 14" str="concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M8" span="33 13" str="carbamazepine" type="Precipitant"/>
      <Interaction effect="C54602" id="I3" precipitant="M8" trigger="M7" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I4" precipitant="M8" trigger="M7" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3437" section="34073-7">
      

      <SentenceText>When phenytoin, carbamazepine, or another inducer of hepatic metabolism (such as rifampicin) is added to mirtazapine therapy, the mirtazapine dose may have to be increased.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3438" section="34073-7">
      

      <SentenceText>If treatment with such a medicinal product is discontinued, it may be necessary to reduce the mirtazapine dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3439" section="34073-7">
      

      <SentenceText>In healthy male patients (n=12), when cimetidine, a weak inhibitor of CYP1A2, CYP2D6, and CYP3A4, given at 800 mg b.i.d. at steady state was coadministered with mirtazapine (30 mg daily) at steady state, the Area Under the Curve (AUC) of mirtazapine increased more than 50%.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="190 12" str="steady state" type="Trigger"/>
      <Mention code="NO MAP" id="M10" span="204 24" str="the Area Under the Curve" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="38 10" str="cimetidine" type="Precipitant"/>
      <Interaction effect="C54355" id="I5" precipitant="M11" trigger="M10" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3440" section="34073-7">
      

      <SentenceText>Mirtazapine did not cause relevant changes in the pharmacokinetics of cimetidine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3441" section="34073-7">
      

      <SentenceText>The mirtazapine dose may have to be decreased when concomitant treatment with cimetidine is started, or increased when cimetidine treatment is discontinued.</SentenceText>
      

      <Mention code="NO MAP" id="M12" span="16 4" str="dose" type="Trigger"/>
      <Mention code="NO MAP" id="M13" span="25 4" str="have" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3442" section="34073-7">
      

      <SentenceText>In healthy, male, Caucasian patients (n=24), coadministration of the potent CYP3A4 inhibitor ketoconazole (200 mg b.i.d. for 6.5 days) increased the peak plasma levels and the AUC of a single 30-mg dose of mirtazapine by approximately 40% and 50%, respectively.</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="76 16" str="CYP3A4 inhibitor" type="Precipitant"/>
      <Mention code="NO MAP" id="M15" span="93 12" str="ketoconazole" type="Precipitant"/>
      <Interaction effect="C54355" id="I6" precipitant="M15" trigger="M15" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3443" section="34073-7">
      

      <SentenceText>Caution should be exercised when coadministering mirtazapine with potent CYP3A4 inhibitors, HIV protease inhibitors, azole antifungals, erythromycin, or nefazodone.</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="0 7" str="Caution" type="Trigger"/>
      <Mention code="NO MAP" id="M17" span="73 17" str="CYP3A4 inhibitors" type="Precipitant"/>
      <Interaction id="I7" precipitant="M17" trigger="M16" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M18" span="96 19" str="protease inhibitors" type="Precipitant"/>
      <Interaction id="I8" precipitant="M18" trigger="M16" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M19" span="117 17" str="azole antifungals" type="Precipitant"/>
      <Interaction id="I9" precipitant="M19" trigger="M16" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3444" section="34073-7">
      

      <SentenceText>In an in vivo interaction study in healthy, CYP2D6 extensive metabolizer patients (n=24), mirtazapine (30 mg/day), at steady state, did not cause relevant changes in the pharmacokinetics of steady state paroxetine (40 mg/day), a CYP2D6 inhibitor.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3445" section="34073-7">
      

      <SentenceText>In healthy, CYP2D6 extensive metabolizer patients (n=32), amitriptyline (75 mg daily), at steady state, did not cause relevant changes in the pharmacokinetics of steady state mirtazapine (30 mg daily); mirtazapine also did not cause relevant changes to the pharmacokinetics of amitriptyline.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3446" section="34073-7">
      

      <SentenceText>In healthy male subjects (n=16), mirtazapine (30 mg daily), at steady state, caused a small (0.2) but statistically significant increase in the International Normalized Ratio (INR) in subjects treated with warfarin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3447" section="34073-7">
      

      <SentenceText>As at a higher dose of mirtazapine, a more pronounced effect can not be excluded, it is advisable to monitor the INR in case of concomitant treatment of warfarin with mirtazapine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3448" section="34073-7">
      

      <SentenceText>No relevant clinical effects or significant changes in pharmacokinetics have been observed in healthy male subjects on concurrent treatment with subtherapeutic levels of lithium (600 mg/day for 10 days) at steady state and a single 30-mg dose of mirtazapine.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="170 7" str="lithium" type="Precipitant"/>
      <Interaction effect="C54357" id="I10" precipitant="M20" trigger="M20" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3449" section="34073-7">
      

      <SentenceText>The effects of higher doses of lithium on the pharmacokinetics of mirtazapine are unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3450" section="34073-7">
      

      <SentenceText>In an in vivo, nonrandomized, interaction study, subjects (n=6) in need of treatment with an antipsychotic and antidepressant drug, showed that mirtazapine (30 mg daily) at steady state did not influence the pharmacokinetics of risperidone (up to 3 mg b.i.d.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3451" section="34073-7">
      

      <SentenceText>Concomitant administration of alcohol (equivalent to 60 g) had a minimal effect on plasma levels of mirtazapine (15 mg) in 6 healthy male subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3452" section="34073-7">
      

      <SentenceText>However, the impairment of cognitive and motor skills produced by REMERON were shown to be additive with those produced by alcohol.</SentenceText>
      

      <Mention code="NO MAP" id="M21" span="91 13" str="additive with" type="Trigger"/>
      <Mention code="NO MAP" id="M22" span="123 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="C54357" id="I11" precipitant="M22" trigger="M21" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3453" section="34073-7">
      

      <SentenceText>Accordingly, patients should be advised to avoid alcohol while taking REMERON.</SentenceText>
      

      <Mention code="NO MAP" id="M23" span="49 7" str="alcohol" type="Precipitant"/>
      <Interaction id="I12" precipitant="M23" trigger="M23" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3454" section="34073-7">
      

      <SentenceText>Concomitant administration of diazepam (15 mg) had a minimal effect on plasma levels of mirtazapine (15 mg) in 12 healthy subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3455" section="34073-7">
      

      <SentenceText>However, the impairment of motor skills produced by REMERON has been shown to be additive with those caused by diazepam.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3456" section="34073-7">
      

      <SentenceText>Accordingly, patients should be advised to avoid diazepam and other similar drugs while taking REMERON.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3457" section="34073-7">
      

      <SentenceText>The risk of QT prolongation and/or ventricular arrhythmias (e.g., Torsades de Pointes) may be increased with concomitant use of medicines which prolong the QTc interval (e.g., some antipsychotics and antibiotics) and in case of mirtazapine overdose.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="12 15" str="QT prolongation" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M25" span="35 23" str="ventricular arrhythmias" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="128 9" str="medicines" type="Precipitant"/>
      <Interaction effect="M24;M25" id="I13" precipitant="M26" trigger="M26" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3458" section="34090-1">
      

      <SentenceText>The mechanism of action of REMERON (mirtazapine) Tablets, as with other drugs effective in the treatment of major depressive disorder, is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3459" section="34090-1">
      

      <SentenceText>Evidence gathered in preclinical studies suggests that mirtazapine enhances central noradrenergic and serotonergic activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3460" section="34090-1">
      

      <SentenceText>These studies have shown that mirtazapine acts as an antagonist at central presynaptic α2–adrenergic inhibitory autoreceptors and heteroreceptors, an action that is postulated to result in an increase in central noradrenergic and serotonergic activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3461" section="34090-1">
      

      <SentenceText>Mirtazapine is a potent antagonist of 5-HT2 and 5-HT3 receptors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3462" section="34090-1">
      

      <SentenceText>Mirtazapine has no significant affinity for the 5-HT1A and 5-HT1B receptors.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3463" section="34090-1">
      

      <SentenceText>Mirtazapine is a potent antagonist of histamine (H1) receptors, a property that may explain its prominent sedative effects.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="106 16" str="sedative effects" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3464" section="34090-1">
      

      <SentenceText>Mirtazapine is a moderate peripheral α1–adrenergic antagonist, a property that may explain the occasional orthostatic hypotension reported in association with its use.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="106 23" str="orthostatic hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M29" span="17 8" str="moderate" type="Precipitant"/>
      <Interaction effect="M28" id="I14" precipitant="M29" trigger="M29" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3465" section="34090-1">
      

      <SentenceText>Mirtazapine is a moderate antagonist at muscarinic receptors, a property that may explain the relatively low incidence of anticholinergic side effects associated with its use.</SentenceText>
      

      <Mention code="NO MAP" id="M30" span="26 10" str="antagonist" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3466" section="34090-1">
      

      <SentenceText>REMERON (mirtazapine) Tablets are rapidly and completely absorbed following oral administration and have a half-life of about 20 to 40 hours.</SentenceText>
      

      <Mention code="NO MAP" id="M31" span="107 12" str="half-life of" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3467" section="34090-1">
      

      <SentenceText>Peak plasma concentrations are reached within about 2 hours following an oral dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3468" section="34090-1">
      

      <SentenceText>The presence of food in the stomach has a minimal effect on both the rate and extent of absorption and does not require a dosage adjustment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3469" section="34090-1">
      

      <SentenceText>Mirtazapine is extensively metabolized after oral administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3470" section="34090-1">
      

      <SentenceText>Major pathways of biotransformation are demethylation and hydroxylation followed by glucuronide conjugation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3471" section="34090-1">
      

      <SentenceText>In vitro data from human liver microsomes indicate that cytochrome 2D6 and 1A2 are involved in the formation of the 8-hydroxy metabolite of mirtazapine, whereas cytochrome 3A is considered to be responsible for the formation of the N-desmethyl and N-oxide metabolite.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3472" section="34090-1">
      

      <SentenceText>Mirtazapine has an absolute bioavailability of about 50%.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3473" section="34090-1">
      

      <SentenceText>It is eliminated predominantly via urine (75%) with 15% in feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3474" section="34090-1">
      

      <SentenceText>Several unconjugated metabolites possess pharmacological activity but are present in the plasma at very low levels.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3475" section="34090-1">
      

      <SentenceText>The (–) enantiomer has an elimination half-life that is approximately twice as long as the (+) enantiomer and therefore achieves plasma levels that are about 3 times as high as that of the (+) enantiomer.</SentenceText>
      

      <Mention code="NO MAP" id="M32" span="38 9" str="half-life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3476" section="34090-1">
      

      <SentenceText>Plasma levels are linearly related to dose over a dose range of 15 to 80 mg.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3477" section="34090-1">
      

      <SentenceText>The mean elimination half-life of mirtazapine after oral administration ranges from approximately 20 to 40 hours across age and gender subgroups, with females of all ages exhibiting significantly longer elimination half-lives than males (mean half-life of 37 hours for females vs. 26 hours for males).</SentenceText>
      

      <Mention code="NO MAP" id="M33" span="4 21" str="mean elimination half" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3478" section="34090-1">
      

      <SentenceText>Steady state plasma levels of mirtazapine are attained within 5 days, with about 50% accumulation (accumulation ratio = 1.5).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3479" section="34090-1">
      

      <SentenceText>Mirtazapine is approximately 85% bound to plasma proteins over a concentration range of 0.01 to 10 mcg/mL.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3480" section="34090-1">
      

      <SentenceText>Following oral administration of REMERON (mirtazapine) Tablets 20 mg/day for 7 days to subjects of varying ages (range, 25–74), oral clearance of mirtazapine was reduced in the elderly compared to the younger subjects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3481" section="34090-1">
      

      <SentenceText>The differences were most striking in males, with a 40% lower clearance in elderly males compared to younger males, while the clearance in elderly females was only 10% lower compared to younger females.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3482" section="34090-1">
      

      <SentenceText>Caution is indicated in administering REMERON to elderly patients.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3483" section="34090-1">
      

      <SentenceText>Safety and effectiveness of mirtazapine in the pediatric population have not been established.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3485" section="34090-1">
      

      <SentenceText>There have been no clinical studies to evaluate the effect of race on the pharmacokinetics of REMERON.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3486" section="34090-1">
      

      <SentenceText>The disposition of mirtazapine was studied in patients with varying degrees of renal function.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3487" section="34090-1">
      

      <SentenceText>Elimination of mirtazapine is correlated with creatinine clearance.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3488" section="34090-1">
      

      <SentenceText>Total body clearance of mirtazapine was reduced approximately 30% in patients with moderate (Clcr=11–39 mL/min/1.73 m2) and approximately 50% in patients with severe (Clcr=&lt;10 mL/min/1.73 m2) renal impairment when compared to normal subjects.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="6 4" str="body" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3489" section="34090-1">
      

      <SentenceText>Caution is indicated in administering REMERON to patients with compromised renal function.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="0 7" str="Caution" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3490" section="34090-1">
      

      <SentenceText>Following a single 15-mg oral dose of REMERON, the oral clearance of mirtazapine was decreased by approximately 30% in hepatically impaired patients compared to subjects with normal hepatic function.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3491" section="34090-1">
      

      <SentenceText>Caution is indicated in administering REMERON to patients with compromised hepatic function.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3492" section="34090-1">
      

      <SentenceText>The efficacy of REMERON (mirtazapine) Tablets as a treatment for major depressive disorder was established in 4 placebo-controlled, 6-week trials in adult outpatients meeting DSM-III criteria for major depressive disorder.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3493" section="34090-1">
      

      <SentenceText>Patients were titrated with mirtazapine from a dose range of 5 mg up to 35 mg/day.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3494" section="34090-1">
      

      <SentenceText>Overall, these studies demonstrated mirtazapine to be superior to placebo on at least 3 of the following 4 measures: 21-Item Hamilton Depression Rating Scale (HDRS) total score; HDRS Depressed Mood Item; CGI Severity score; and Montgomery and Asberg Depression Rating Scale (MADRS).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3495" section="34090-1">
      

      <SentenceText>Superiority of mirtazapine over placebo was also found for certain factors of the HDRS, including anxiety/somatization factor and sleep disturbance factor.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="106 19" str="somatization factor" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M37" span="130 24" str="sleep disturbance factor" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3496" section="34090-1">
      

      <SentenceText>The mean mirtazapine dose for patients who completed these 4 studies ranged from 21 to 32 mg/day.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3497" section="34090-1">
      

      <SentenceText>A fifth study of similar design utilized a higher dose (up to 50 mg) per day and also showed effectiveness.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="93 13" str="effectiveness" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3498" section="34090-1">
      

      <SentenceText>Examination of age and gender subsets of the population did not reveal any differential responsiveness on the basis of these subgroupings.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3499" section="34090-1">
      

      <SentenceText>In a longer-term study, patients meeting (DSM-IV) criteria for major depressive disorder who had responded during an initial 8 to 12 weeks of acute treatment on REMERON were randomized to continuation of REMERON or placebo for up to 40 weeks of observation for relapse.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3500" section="34090-1">
      

      <SentenceText>Response during the open phase was defined as having achieved a HAM-D 17 total score of ≤8 and a CGI-Improvement score of 1 or 2 at 2 consecutive visits beginning with week 6 of the 8 to 12 weeks in the open-label phase of the study.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3501" section="34090-1">
      

      <SentenceText>Relapse during the double-blind phase was determined by the individual investigators.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3502" section="34090-1">
      

      <SentenceText>Patients receiving continued REMERON treatment experienced significantly lower relapse rates over the subsequent 40 weeks compared to those receiving placebo.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="REMERON" id="3503" section="34090-1">
      

      <SentenceText>This pattern was demonstrated in both male and female patients.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

