<?xml version="1.0" ?>
<Label drug="Duloxetine" setid="829a4f51-c882-4b64-81f3-abfb03a52ebe">
  <Text>
    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS    Potent inhibitors of CYP1A2 should be avoided ( 7.1 ).    Potent inhibitors of CYP2D6 may increase duloxetine concentrations ( 7.2 ).    Duloxetine is a moderate inhibitor of CYP2D6 ( 7.9 ).  Both CYP1A2 and CYP2D6 are responsible for duloxetine metabolism.  7.1 Inhibitors of CYP1A2  When duloxetine 60 mg was co-administered with fluvoxamine 100 mg, a potent CYP1A2 inhibitor, to male subjects (n=14) duloxetine AUC was increased approximately 6-fold, the C max was increased about 2.5-fold, and duloxetine t 1/2 was increased approximately 3-fold. Other drugs that inhibit CYP1A2 metabolism include cimetidine and quinolone antimicrobials such as ciprofloxacin and enoxacin [see WARNINGS AND PRECAUTIONS ( 5.12 )]  .  7.2 Inhibitors of CYP2D6  Concomitant use of duloxetine (40 mg once daily) with paroxetine (20 mg once daily) increased the concentration of duloxetine AUC by about 60%, and greater degrees of inhibition are expected with higher doses of paroxetine. Similar effects would be expected with other potent CYP2D6 inhibitors (e.g., fluoxetine, quinidine) [see WARNINGS AND PRECAUTIONS ( 5.12 )]  .  7.3 Dual Inhibition of CYP1A2 and CYP2D6  Concomitant administration of duloxetine 40 mg twice daily with fluvoxamine 100 mg, a potent CYP1A2 inhibitor, to CYP2D6 poor metabolizer subjects (n=14) resulted in a 6-fold increase in duloxetine AUC and C max  .  7.4 Drugs that Interfere with Hemostasis (e.g., NSAIDs, Aspirin, and Warfarin)  Serotonin release by platelets plays an important role in hemostasis. Epidemiological studies of the case-control and cohort design that have demonstrated an association between use of psychotropic drugs that interfere with serotonin reuptake and the occurrence of upper gastrointestinal bleeding have also shown that concurrent use of an NSAID or aspirin may potentiate this risk of bleeding. Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs or SNRIs are co-administered with warfarin. Concomitant administration of warfarin (2 to 9 mg once daily) under steady state conditions with duloxetine 60 or 120 mg once daily for up to 14 days in healthy subjects (n=15) did not significantly change INR from baseline (mean INR changes ranged from 0.05 to +0.07). The total warfarin (protein bound plus free drug) pharmacokinetics (AUC T,ss, C max,ss or t max,ss ) for both R- and S-warfarin were not altered by duloxetine. Because of the potential effect of duloxetine on platelets, patients receiving warfarin therapy should be carefully monitored when duloxetine is initiated or discontinued [see WARNINGS AND PRECAUTIONS ( 5.5 )]  .  7.5 Lorazepam  Under steady-state conditions for duloxetine (60 mg Q 12 hours) and lorazepam (2 mg Q 12 hours), the pharmacokinetics of duloxetine were not affected by co-administration.  7.6 Temazepam  Under steady-state conditions for duloxetine (20 mg qhs) and temazepam (30 mg qhs), the pharmacokinetics of duloxetine were not affected by co-administration.  7.7 Drugs that Affect Gastric Acidity  Duloxetine delayed-release capsules have an enteric coating that resists dissolution until reaching a segment of the gastrointestinal tract where the pH exceeds 5.5. In extremely acidic conditions, duloxetine, unprotected by the enteric coating, may undergo hydrolysis to form naphthol. Caution is advised in using duloxetine in patients with conditions that may slow gastric emptying (e.g., some diabetics). Drugs that raise the gastrointestinal pH may lead to an earlier release of duloxetine. However, co-administration of duloxetine with aluminum- and magnesium-containing antacids (51 mEq) or duloxetine with famotidine, had no significant effect on the rate or extent of duloxetine absorption after administration of a 40 mg oral dose. It is unknown whether the concomitant administration of proton pump inhibitors affects duloxetine absorption [see WARNINGS AND PRECAUTIONS ( 5.14 )]  .  7.8 Drugs Metabolized by CYP1A2  In vitro drug interaction studies demonstrate that duloxetine does not induce CYP1A2 activity. Therefore, an increase in the metabolism of CYP1A2 substrates (e.g., theophylline, caffeine) resulting from induction is not anticipated, although clinical studies of induction have not been performed. Duloxetine is an inhibitor of the CYP1A2 isoform in in vitro studies, and in two clinical studies the average (90% confidence interval) increase in theophylline AUC was 7% (1% to 15%) and 20% (13% to 27%) when co-administered with duloxetine (60 mg twice daily).  7.9 Drugs Metabolized by CYP2D6  Duloxetine is a moderate inhibitor of CYP2D6. When duloxetine was administered (at a dose of 60 mg twice daily) in conjunction with a single 50 mg dose of desipramine, a CYP2D6 substrate, the AUC of desipramine increased 3-fold [see WARNINGS AND PRECAUTIONS ( 5.12 )]  .  7.10 Drugs Metabolized by CYP2C9  Results of in vitro studies demonstrate that duloxetine does not inhibit activity. In a clinical study, the pharmacokinetics of S-warfarin, a CYP2C9 substrate, were not significantly affected by duloxetine [see DRUG INTERACTIONS ( 7.4 )]  .    7.11 Drugs Metabolized by CYP3A  Results of in vitro studies demonstrate that duloxetine does not inhibit or induce CYP3A activity. Therefore, an increase or decrease in the metabolism of CYP3A substrates (e.g., oral contraceptives and other steroidal agents) resulting from induction or inhibition is not anticipated, although clinical studies have not been performed.  7.12 Drugs Metabolized by CYP2C19  Results of in vitro studies demonstrate that duloxetine does not inhibit CYP2C19 activity at therapeutic concentrations. Inhibition of the metabolism of CYP2C19 substrates is therefore not anticipated, although clinical studies have not been performed.  7.13 Monoamine Oxidase Inhibitors (MAOIs)  [See DOSAGE AND ADMINISTRATION ( 2.8 , 2.9 ), CONTRAINDICATIONS ( 4 ), and WARNINGS AND PRECAUTIONS ( 5.4 )]  .  7.14 Serotonergic Drugs  [See DOSAGE AND ADMINISTRATION ( 2.8 , 2.9 ), CONTRAINDICATIONS (4), and WARNINGS AND PRECAUTIONS ( 5.4 )]  .  7.15 Alcohol  When duloxetine and ethanol were administered several hours apart so that peak concentrations of each would coincide, duloxetine did not increase the impairment of mental and motor skills caused by alcohol.  In the duloxetine clinical trials database, three duloxetine -treated patients had liver injury as manifested by ALT and total bilirubin elevations, with evidence of obstruction. Substantial intercurrent ethanol use was present in each of these cases, and this may have contributed to the abnormalities seen [see WARNINGS AND PRECAUTIONS ( 5.2 , 5.12 )]  .  7.16 CNS Drugs  [See WARNINGS AND PRECAUTIONS ( 5.12 )]  .  7.17 Drugs Highly Bound to Plasma Protein  Because duloxetine is highly bound to plasma protein, administration of duloxetine to a patient taking another drug that is highly protein bound may cause increased free concentrations of the other drug, potentially resulting in adverse reactions. However, co-administration of duloxetine (60 or 120 mg) with warfarin (2 to 9 mg), a highly protein-bound drug, did not result in significant changes in INR and in the pharmacokinetics of either total S-or total R-warfarin (protein bound plus free drug) [see DRUG INTERACTIONS ( 7.4 )]  .</Section>
    

    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
12 CLINICAL PHARMACOLOGY  12.1 Mechanism of Action  Although the exact mechanisms of the antidepressant, central pain inhibitory and anxiolytic actions of duloxetine in humans are unknown, these actions are believed to be related to its potentiation of serotonergic and noradrenergic activity in the CNS.  12.2 Pharmacodynamics  Preclinical studies have shown that duloxetine is a potent inhibitor of neuronal serotonin and norepinephrine reuptake and a less potent inhibitor of dopamine reuptake. Duloxetine has no significant affinity for dopaminergic, adrenergic, cholinergic, histaminergic, opioid, glutamate, and GABA receptors in vitro . Duloxetine does not inhibit monoamine oxidase (MAO).  Duloxetine is in a class of drugs known to affect urethral resistance. If symptoms of urinary hesitation develop during treatment with duloxetine, consideration should be given to the possibility that they might be drug-related.  12.3 Pharmacokinetics  Duloxetine has an elimination half-life of about 12 hours (range 8 to 17 hours) and its pharmacokinetics are dose proportional over the therapeutic range. Steady-state plasma concentrations are typically achieved after 3 days of dosing. Elimination of duloxetine is mainly through hepatic metabolism involving two P450 isozymes, CYP1A2 and CYP2D6.  Absorption and Distribution  Orally administered duloxetine hydrochloride is well absorbed. There is a median 2 hour lag until absorption begins (T lag ), with maximal plasma concentrations (C max ) of duloxetine occurring 6 hours post dose. Food does not affect the C max of duloxetine, but delays the time to reach peak concentration from 6 to 10 hours and it marginally decreases the extent of absorption (AUC) by about 10%. There is a 3 hour delay in absorption and a one-third increase in apparent clearance of duloxetine after an evening dose as compared to a morning dose.  The apparent volume of distribution averages about 1640 L. Duloxetine is highly bound (&gt;90%) to proteins in human plasma, binding primarily to albumin and α 1 -acid glycoprotein. The interaction between duloxetine and other highly protein bound drugs has not been fully evaluated. Plasma protein binding of duloxetine is not affected by renal or hepatic impairment.  Metabolism and Elimination  Biotransformation and disposition of duloxetine in humans have been determined following oral administration of 14 C-labeled duloxetine. Duloxetine comprises about 3% of the total radiolabeled material in the plasma, indicating that it undergoes extensive metabolism to numerous metabolites. The major biotransformation pathways for duloxetine involve oxidation of the naphthyl ring followed by conjugation and further oxidation. Both CYP1A2 and CYP2D6 catalyze the oxidation of the naphthyl ring in vitro . Metabolites found in plasma include 4-hydroxy duloxetine glucuronide and 5-hydroxy, 6-methoxy duloxetine sulfate. Many additional metabolites have been identified in urine, some representing only minor pathways of elimination. Only trace (&lt;1% of the dose) amounts of unchanged duloxetine are present in the urine. Most (about 70%) of the duloxetine dose appears in the urine as metabolites of duloxetine; about 20% is excreted in the feces. Duloxetine undergoes extensive metabolism, but the major circulating metabolites have not been shown to contribute significantly to the pharmacologic activity of duloxetine.  Pediatric use information for patient's ages 7 to 17 years is approved for Eli Lilly and Company, Inc.'s CYMBALTA ® (duloxetine) delayed-release capsules. However, due to Eli Lilly and Company, Inc.'s marketing exclusivity rights, this drug product is not labeled with that pediatric information.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Duloxetine" id="328" section="34073-7">
      

      <SentenceText>Potent inhibitors of CYP1A2 should be avoided (7.1).</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="38 7" str="avoided" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="0 27" str="Potent inhibitors of CYP1A2" type="Precipitant"/>
      <Interaction id="I1" precipitant="M2" trigger="M1" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="329" section="34073-7">
      

      <SentenceText>Potent inhibitors of CYP2D6 may increase duloxetine concentrations (7.2).</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="0 27" str="Potent inhibitors of CYP2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I2" precipitant="M3" trigger="M3" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="330" section="34073-7">
      

      <SentenceText>Duloxetine is a moderate inhibitor of CYP2D6 (7.9).</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="25 9" str="inhibitor" type="Precipitant"/>
      <Interaction effect="C54355" id="I3" precipitant="M4" trigger="M4" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="331" section="34073-7">
      

      <SentenceText>Both CYP1A2 and CYP2D6 are responsible for duloxetine metabolism.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="332" section="34073-7">
      

      <SentenceText>When duloxetine 60mg was co-administered with fluvoxamine 100mg, a potent CYP1A2 inhibitor, to male subjects (n=14) duloxetine AUC was increased approximately 6-fold, the Cmax was increased about 2.5-fold, and duloxetine t1/2 was increased approximately 3-fold.</SentenceText>
      

      <Mention code="NO MAP" id="M5" span="230 9" str="increased" type="Trigger"/>
      <Mention code="NO MAP" id="M6" span="46 11" str="fluvoxamine" type="Precipitant"/>
      <Interaction effect="C54602" id="I4" precipitant="M6" trigger="M5" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I5" precipitant="M6" trigger="M5" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M7" span="74 16" str="CYP1A2 inhibitor" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="333" section="34073-7">
      

      <SentenceText>Other drugs that inhibit CYP1A2 metabolism include cimetidine and quinolone antimicrobials such as ciprofloxacin and enoxacin.</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="12 12" str="that inhibit" type="Precipitant"/>
      <Interaction id="I6" precipitant="M8" trigger="M8" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="334" section="34073-7">
      

      <SentenceText>Concomitant use of duloxetine (40 mg once daily) with paroxetine (20 mg once daily) increased the concentration of duloxetine AUC by about 60%, and greater degrees of inhibition are expected with higher doses of paroxetine.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="167 14" str="inhibition are" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="335" section="34073-7">
      

      <SentenceText>Similar effects would be expected with other potent CYP2D6 inhibitors (e.g., fluoxetine, quinidine).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="336" section="34073-7">
      

      <SentenceText>Concomitant administration of duloxetine 40 mg twice daily with fluvoxamine 100 mg, a potent CYP1A2 inhibitor, to CYP2D6 poor metabolizer subjects (n=14) resulted in a 6-fold increase in duloxetine AUC and Cmax.</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="175 11;198 3" str="increase in | AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="64 11" str="fluvoxamine" type="Precipitant"/>
      <Interaction effect="C54602" id="I7" precipitant="M11" trigger="M10" type="Pharmacokinetic interaction"/>
      <Interaction effect="C54605" id="I8" precipitant="M11" trigger="M10" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M12" span="93 16" str="CYP1A2 inhibitor" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="337" section="34073-7">
      

      <SentenceText>Serotonin release by platelets plays an important role in hemostasis.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="338" section="34073-7">
      

      <SentenceText>Epidemiological studies of the case-control and cohort design that have demonstrated an association between use of psychotropic drugs that interfere with serotonin reuptake and the occurrence of upper gastrointestinal bleeding have also shown that concurrent use of an NSAID or aspirin may potentiate this risk of bleeding.</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="201 25" str="gastrointestinal bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M14" span="290 23" str="potentiate this risk of" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M15" span="314 8" str="bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M16" span="115 57" str="psychotropic drugs that interfere with serotonin reuptake" type="Precipitant"/>
      <Interaction effect="M13;M14;M15" id="I9" precipitant="M16" trigger="M16" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M17" span="269 5" str="NSAID" type="Precipitant"/>
      <Interaction effect="M13;M14;M15" id="I10" precipitant="M17" trigger="M17" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M18" span="278 7" str="aspirin" type="Precipitant"/>
      <Interaction effect="M13;M14;M15" id="I11" precipitant="M18" trigger="M18" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="339" section="34073-7">
      

      <SentenceText>Altered anticoagulant effects, including increased bleeding, have been reported when SSRIs or SNRIs are co-administered with warfarin.</SentenceText>
      

      <Mention code="NO MAP" id="M19" span="41 18" str="increased bleeding" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M20" span="85 5" str="SSRIs" type="Precipitant"/>
      <Interaction effect="M19" id="I12" precipitant="M20" trigger="M20" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M21" span="125 8" str="warfarin" type="Precipitant"/>
      <Interaction effect="M19" id="I13" precipitant="M21" trigger="M21" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="340" section="34073-7">
      

      <SentenceText>Concomitant administration of warfarin (2 to 9 mg once daily) under steady state conditions with duloxetine 60 or 120 mg once daily for up to 14 days in healthy subjects (n=15) did not significantly change INR from baseline (mean INR changes ranged from 0.05 to +0.07).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="341" section="34073-7">
      

      <SentenceText>The total warfarin (protein bound plus free drug) pharmacokinetics (AUCT,ss, Cmax,ss or tmax,ss) for both R- and S-warfarin were not altered by duloxetine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="342" section="34073-7">
      

      <SentenceText>Because of the potential effect of duloxetine on platelets, patients receiving warfarin therapy should be carefully monitored when duloxetine is initiated or discontinued.</SentenceText>
      

      <Mention code="NO MAP" id="M22" span="116 9" str="monitored" type="Trigger"/>
      <Mention code="NO MAP" id="M23" span="79 8" str="warfarin" type="Precipitant"/>
      <Interaction id="I14" precipitant="M23" trigger="M22" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="343" section="34073-7">
      

      <SentenceText>Under steady-state conditions for duloxetine (60 mg Q 12 hours) and lorazepam (2 mg Q 12 hours), the pharmacokinetics of duloxetine were not affected by co-administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="344" section="34073-7">
      

      <SentenceText>Under steady-state conditions for duloxetine (20 mg qhs) and temazepam (30 mg qhs), the pharmacokinetics of duloxetine were not affected by co-administration.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="345" section="34073-7">
      

      <SentenceText>Duloxetine delayed-release capsules have an enteric coating that resists dissolution until reaching a segment of the gastrointestinal tract where the pH exceeds 5.5.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="346" section="34073-7">
      

      <SentenceText>In extremely acidic conditions, duloxetine, unprotected by the enteric coating, may undergo hydrolysis to form naphthol.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="347" section="34073-7">
      

      <SentenceText>Caution is advised in using duloxetine in patients with conditions that may slow gastric emptying (e.g., some diabetics).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="348" section="34073-7">
      

      <SentenceText>Drugs that raise the gastrointestinal pH may lead to an earlier release of duloxetine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="349" section="34073-7">
      

      <SentenceText>However, co-administration of duloxetine with aluminum- and magnesium-containing antacids (51 mEq) or duloxetine with famotidine, had no significant effect on the rate or extent of duloxetine absorption after administration of a 40 mg oral dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="350" section="34073-7">
      

      <SentenceText>It is unknown whether the concomitant administration of proton pump inhibitors affects duloxetine absorption.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="351" section="34073-7">
      

      <SentenceText>In vitro drug interaction studies demonstrate that duloxetine does not induce CYP1A2 activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="352" section="34073-7">
      

      <SentenceText>Therefore, an increase in the metabolism of CYP1A2 substrates (e.g., theophylline, caffeine) resulting from induction is not anticipated, although clinical studies of induction have not been performed.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="14 26" str="increase in the metabolism" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="353" section="34073-7">
      

      <SentenceText>Duloxetine is an inhibitor of the CYP1A2 isoform in in vitro studies, and in two clinical studies the average (90% confidence interval) increase in theophylline AUC was 7% (1% to 15%) and 20% (13% to 27%) when co-administered with duloxetine (60 mg twice daily).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="354" section="34073-7">
      

      <SentenceText>Duloxetine is a moderate inhibitor of CYP2D6.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="355" section="34073-7">
      

      <SentenceText>When duloxetine was administered (at a dose of 60 mg twice daily) in conjunction with a single 50 mg dose of desipramine, a CYP2D6 substrate, the AUC of desipramine increased 3-fold.</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="109 11" str="desipramine" type="Precipitant"/>
      <Interaction effect="C54355" id="I15" precipitant="M25" trigger="M25" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M26" span="153 11" str="desipramine" type="Precipitant"/>
      <Interaction effect="C54355" id="I16" precipitant="M26" trigger="M26" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="356" section="34073-7">
      

      <SentenceText>Results of in vitro studies demonstrate that duloxetine does not inhibit activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="357" section="34073-7">
      

      <SentenceText>In a clinical study, the pharmacokinetics of S-warfarin, a CYP2C9 substrate, were not significantly affected by duloxetine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="358" section="34073-7">
      

      <SentenceText>Results of in vitro studies demonstrate that duloxetine does not inhibit or induce CYP3A activity.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="359" section="34073-7">
      

      <SentenceText>Therefore, an increase or decrease in the metabolism of CYP3A substrates (e.g., oral contraceptives and other steroidal agents) resulting from induction or inhibition is not anticipated, although clinical studies have not been performed.</SentenceText>
      

      <Mention code="NO MAP" id="M27" span="14 8" str="increase" type="Trigger"/>
      <Mention code="NO MAP" id="M28" span="26 26" str="decrease in the metabolism" type="Trigger"/>
      <Mention code="NO MAP" id="M29" span="56 16" str="CYP3A substrates" type="Precipitant"/>
      <Interaction id="I17" precipitant="M29" trigger="M28" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="360" section="34073-7">
      

      <SentenceText>Results of in vitro studies demonstrate that duloxetine does not inhibit CYP2C19 activity at therapeutic concentrations.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="361" section="34073-7">
      

      <SentenceText>Inhibition of the metabolism of CYP2C19 substrates is therefore not anticipated, although clinical studies have not been performed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="364" section="34073-7">
      

      <SentenceText>When duloxetine and ethanol were administered several hours apart so that peak concentrations of each would coincide, duloxetine did not increase the impairment of mental and motor skills caused by alcohol.</SentenceText>
      

      <Mention code="NO MAP" id="M30" span="137 12" str="increase the" type="Trigger"/>
      <Mention code="NO MAP" id="M31" span="20 7" str="ethanol" type="Precipitant"/>
      <Interaction effect="C54355" id="I18" precipitant="M31" trigger="M30" type="Pharmacokinetic interaction"/>
      <Mention code="NO MAP" id="M32" span="198 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="C54355" id="I19" precipitant="M32" trigger="M30" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="365" section="34073-7">
      

      <SentenceText>In the duloxetine clinical trials database, three duloxetine -treated patients had liver injury as manifested by ALT and total bilirubin elevations, with evidence of obstruction.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="366" section="34073-7">
      

      <SentenceText>Substantial intercurrent ethanol use was present in each of these cases, and this may have contributed to the abnormalities seen.</SentenceText>
      

      <Mention code="NO MAP" id="M33" span="25 7" str="ethanol" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="367" section="34073-7">
      

      <SentenceText>Because duloxetine is highly bound to plasma protein, administration of duloxetine to a patient taking another drug that is highly protein bound may cause increased free concentrations of the other drug, potentially resulting in adverse reactions.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="368" section="34073-7">
      

      <SentenceText>However, co-administration of duloxetine (60 or 120 mg) with warfarin (2 to 9 mg), a highly protein-bound drug, did not result in significant changes in INR and in the pharmacokinetics of either total S-or total R-warfarin (protein bound plus free drug).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="369" section="34090-1">
      

      <SentenceText>Although the exact mechanisms of the antidepressant, central pain inhibitory and anxiolytic actions of duloxetine in humans are unknown, these actions are believed to be related to its potentiation of serotonergic and noradrenergic activity in the CNS.</SentenceText>
      

      <Mention code="NO MAP" id="M34" span="232 8" str="activity" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="370" section="34090-1">
      

      <SentenceText>Preclinical studies have shown that duloxetine is a potent inhibitor of neuronal serotonin and norepinephrine reuptake and a less potent inhibitor of dopamine reuptake.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="371" section="34090-1">
      

      <SentenceText>Duloxetine has no significant affinity for dopaminergic, adrenergic, cholinergic, histaminergic, opioid, glutamate, and GABA receptors in vitro.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="372" section="34090-1">
      

      <SentenceText>Duloxetine does not inhibit monoamine oxidase (MAO).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="373" section="34090-1">
      

      <SentenceText>Duloxetine is in a class of drugs known to affect urethral resistance.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="374" section="34090-1">
      

      <SentenceText>If symptoms of urinary hesitation develop during treatment with duloxetine, consideration should be given to the possibility that they might be drug-related.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="375" section="34090-1">
      

      <SentenceText>Duloxetine has an elimination half-life of about 12 hours (range 8 to 17 hours) and its pharmacokinetics are dose proportional over the therapeutic range.</SentenceText>
      

      <Mention code="NO MAP" id="M35" span="18 21" str="elimination half-life" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="376" section="34090-1">
      

      <SentenceText>Steady-state plasma concentrations are typically achieved after 3 days of dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="377" section="34090-1">
      

      <SentenceText>Elimination of duloxetine is mainly through hepatic metabolism involving two P450 isozymes, CYP1A2 and CYP2D6.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="378" section="34090-1">
      

      <SentenceText>Absorption and Distribution Orally administered duloxetine hydrochloride is well absorbed.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="379" section="34090-1">
      

      <SentenceText>There is a median 2 hour lag until absorption begins (Tlag), with maximal plasma concentrations (Cmax) of duloxetine occurring 6 hours post dose.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="380" section="34090-1">
      

      <SentenceText>Food does not affect the Cmax of duloxetine, but delays the time to reach peak concentration from 6 to 10 hours and it marginally decreases the extent of absorption (AUC) by about 10%.</SentenceText>
      

      <Mention code="NO MAP" id="M36" span="130 13" str="decreases the" type="Trigger"/>
      <Mention code="NO MAP" id="M37" span="154 15" str="absorption (AUC" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="381" section="34090-1">
      

      <SentenceText>There is a 3 hour delay in absorption and a one-third increase in apparent clearance of duloxetine after an evening dose as compared to a morning dose.</SentenceText>
      

      <Mention code="NO MAP" id="M38" span="18 19" str="delay in absorption" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="382" section="34090-1">
      

      <SentenceText>The apparent volume of distribution averages about 1640L. Duloxetine is highly bound (&gt;90%) to proteins in human plasma, binding primarily to albumin and α1-acid glycoprotein.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="383" section="34090-1">
      

      <SentenceText>The interaction between duloxetine and other highly protein bound drugs has not been fully evaluated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="384" section="34090-1">
      

      <SentenceText>Plasma protein binding of duloxetine is not affected by renal or hepatic impairment.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="385" section="34090-1">
      

      <SentenceText>Metabolism and Elimination Biotransformation and disposition of duloxetine in humans have been determined following oral administration of 14C-labeled duloxetine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="386" section="34090-1">
      

      <SentenceText>Duloxetine comprises about 3% of the total radiolabeled material in the plasma, indicating that it undergoes extensive metabolism to numerous metabolites.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="387" section="34090-1">
      

      <SentenceText>The major biotransformation pathways for duloxetine involve oxidation of the naphthyl ring followed by conjugation and further oxidation.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="388" section="34090-1">
      

      <SentenceText>Both CYP1A2 and CYP2D6 catalyze the oxidation of the naphthyl ring in vitro.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="389" section="34090-1">
      

      <SentenceText>Metabolites found in plasma include 4-hydroxy duloxetine glucuronide and 5-hydroxy, 6-methoxy duloxetine sulfate.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="390" section="34090-1">
      

      <SentenceText>Many additional metabolites have been identified in urine, some representing only minor pathways of elimination.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="391" section="34090-1">
      

      <SentenceText>Only trace (&lt;1% of the dose) amounts of unchanged duloxetine are present in the urine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="392" section="34090-1">
      

      <SentenceText>Most (about 70%) of the duloxetine dose appears in the urine as metabolites of duloxetine; about 20% is excreted in the feces.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="393" section="34090-1">
      

      <SentenceText>Duloxetine undergoes extensive metabolism, but the major circulating metabolites have not been shown to contribute significantly to the pharmacologic activity of duloxetine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="394" section="34090-1">
      

      <SentenceText>Pediatric use information for patients ages 7 to 17 years is approved for Eli Lilly and Company, Inc.'s CYMBALTA ® (duloxetine) delayed-release capsules.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Duloxetine" id="395" section="34090-1">
      

      <SentenceText>However, due to Eli Lilly and Company, Inc.'s marketing exclusivity rights, this drug product is not labeled with that pediatric information.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

