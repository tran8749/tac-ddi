TAC 2018 Drug-Drug Interaction 
Evaluation Gold Standard
October 15, 2018

1. Introduction

This package contains the gold standard annotations for the evaluation
data for the TAC 2018 Drug-Drug Interaction (DDI) track.  For more
information, please see the TAC 2018 DDI track web page:
https://bionlp.nlm.nih.gov/tac2018druginteractions/


2. Content

./data/

The data directory contains the gold standard annotations for test set
1 and test set 2.

./tools/

The tools directory contains tacEval_relaxed.py -- a version of the
scoring script that has been relaxed to ignore wrong offsets, missing
required attributes and wrong labels.
