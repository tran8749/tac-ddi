Silenor	1687	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	16
A 3 mg once daily dose may be appropriate for some patients, if clinically indicated.
1	0	0	O	A
2	2	2	O	3
3	4	5	O	mg
4	7	10	O	once
5	12	16	O	daily
6	18	21	O	dose
7	23	25	O	may
8	27	28	O	be
9	30	40	O	appropriate
10	42	44	O	for
11	46	49	O	some
12	51	58	O	patients
13	59	59	O	,
14	61	62	O	if
15	64	73	O	clinically
16	75	83	O	indicated
NULL

Silenor	1688	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	20
Initial dose: 6 mg, once daily for adults (2.1) and 3 mg, once daily for the elderly.
1	0	6	O	Initial
2	8	11	O	dose
3	12	12	O	:
4	14	14	O	6
5	16	17	O	mg
6	18	18	O	,
7	20	23	O	once
8	25	29	O	daily
9	31	33	O	for
10	35	40	O	adults
11	43	45	O	2.1
12	48	50	O	and
13	52	52	O	3
14	54	55	O	mg
15	56	56	O	,
16	58	61	O	once
17	63	67	O	daily
18	69	71	O	for
19	73	75	O	the
20	77	83	O	elderly
NULL

Silenor	1689	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	9
Silenor should be taken within 30 minutes of bedtime.
1	0	6	O	XXXXXXXX
2	8	13	O	should
3	15	16	O	be
4	18	22	O	taken
5	24	29	O	within
6	31	32	O	30
7	34	40	O	minutes
8	42	43	O	of
9	45	51	O	bedtime
NULL

Silenor	1690	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	6
Take within 30 minutes of bedtime.
1	0	3	O	Take
2	5	10	O	within
3	12	13	O	30
4	15	21	O	minutes
5	23	24	O	of
6	26	32	B-U	bedtime
NULL

Silenor	1691	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	13
The daily dose can be increased to 6 mg, if clinically indicated.
1	0	2	O	The
2	4	8	O	daily
3	10	13	O	dose
4	15	17	O	can
5	19	20	O	be
6	22	30	O	increased
7	32	33	O	to
8	35	35	O	6
9	37	38	O	mg
10	39	39	O	,
11	41	42	O	if
12	44	53	O	clinically
13	55	63	O	indicated
NULL

Silenor	1692	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	7
The dose of Silenor should be individualized.
1	0	2	O	The
2	4	7	O	dose
3	9	10	O	of
4	12	18	O	XXXXXXXX
5	20	25	O	should
6	27	28	O	be
7	30	43	O	individualized
NULL

Silenor	1693	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	12
The recommended dose of Silenor for adults is 6 mg once daily.
1	0	2	O	The
2	4	14	O	recommended
3	16	19	O	dose
4	21	22	O	of
5	24	30	O	XXXXXXXX
6	32	34	O	for
7	36	41	O	adults
8	43	44	O	is
9	46	46	O	6
10	48	49	O	mg
11	51	54	O	once
12	56	60	O	daily
NULL

Silenor	1694	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	18
The recommended starting dose of Silenor in elderly patients (> 65 years old) is 3 mg once daily.
1	0	2	O	The
2	4	14	O	recommended
3	16	23	O	starting
4	25	28	O	dose
5	30	31	O	of
6	33	39	O	XXXXXXXX
7	41	42	O	in
8	44	50	O	elderly
9	52	59	O	patients
10	62	62	O	>
11	64	65	O	65
12	67	71	O	years
13	73	75	O	old
14	78	79	O	is
15	81	81	O	3
16	83	84	O	mg
17	86	89	O	once
18	91	95	O	daily
NULL

Silenor	1695	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	11
The total Silenor dose should not exceed 6 mg per day.
1	0	2	O	The
2	4	8	O	total
3	10	16	O	XXXXXXXX
4	18	21	O	dose
5	23	28	O	should
6	30	32	O	not
7	34	39	B-T	exceed
8	41	41	O	6
9	43	44	O	mg
10	46	48	O	per
11	50	52	O	day
NULL

Silenor	1696	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	20
To minimize the potential for next day effects, Silenor should not be taken within 3 hours of a meal.
1	0	1	O	To
2	3	10	O	minimize
3	12	14	O	the
4	16	24	O	potential
5	26	28	O	for
6	30	33	O	next
7	35	37	O	day
8	39	45	O	effects
9	46	46	O	,
10	48	54	O	XXXXXXXX
11	56	61	O	should
12	63	65	O	not
13	67	68	O	be
14	70	74	O	taken
15	76	81	O	within
16	83	83	O	3
17	85	89	O	hours
18	91	92	O	of
19	94	94	O	a
20	96	99	O	meal
NULL

Silenor	1697	34068-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	19
Total daily dose should not exceed 6 mg. (2.3) Should not be taken within 3 hours of a meal.
1	0	4	O	Total
2	6	10	O	daily
3	12	15	O	dose
4	17	22	O	should
5	24	26	O	not
6	28	33	O	exceed
7	35	35	O	6
8	37	38	O	mg
9	42	44	O	2.3
10	47	52	O	Should
11	54	56	O	not
12	58	59	O	be
13	61	65	O	taken
14	67	72	O	within
15	74	74	O	3
16	76	80	O	hours
17	82	83	O	of
18	85	85	O	a
19	87	90	O	meal
NULL

Silenor	1698	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	25
Co-administration with Monoamine Oxidase Inhibitors (MAOIs): Do not administer if patient is taking MAOIs or has used MAOIs within the past two weeks.
1	0	1	O	Co
2	3	16	O	administration
3	18	21	O	with
4	23	31	B-U	Monoamine
5	33	39	I-U	Oxidase
6	41	50	I-U	Inhibitors
7	53	57	I-U	MAOIs
8	59	59	O	:
9	61	62	B-T	Do
10	64	66	I-T	not
11	68	77	I-T	administer
12	79	80	O	if
13	82	88	O	patient
14	90	91	O	is
15	93	98	O	taking
16	100	104	O	MAOIs
17	106	107	O	or
18	109	111	O	has
19	113	116	O	used
20	118	122	O	MAOIs
21	124	129	O	within
22	131	133	O	the
23	135	138	O	past
24	140	142	O	two
25	144	148	O	weeks
NULL

Silenor	1699	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	19
Do not administer Silenor if patient is currently on MAOIs or has used MAOIs within the past two weeks.
1	0	1	B-T	Do
2	3	5	I-T	not
3	7	16	I-T	administer
4	18	24	O	XXXXXXXX
5	26	27	O	if
6	29	35	O	patient
7	37	38	O	is
8	40	48	O	currently
9	50	51	O	on
10	53	57	O	MAOIs
11	59	60	O	or
12	62	64	O	has
13	66	69	O	used
14	71	75	O	MAOIs
15	77	82	O	within
16	84	86	O	the
17	88	91	O	past
18	93	95	O	two
19	97	101	O	weeks
NULL

Silenor	1700	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	11
Hypersensitivity to doxepin hydrochloride, inactive ingredients, or other dibenzoxepines.
1	0	15	O	Hypersensitivity
2	17	18	O	to
3	20	26	O	XXXXXXXX
4	28	40	O	hydrochloride
5	41	41	O	,
6	43	50	O	inactive
7	52	62	O	ingredients
8	63	63	O	,
9	65	66	O	or
10	68	72	O	other
11	74	87	O	dibenzoxepines
NULL

Silenor	1701	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	19
Serious side effects and even death have been reported following the concomitant use of certain drugs with MAO inhibitors.
1	0	6	O	Serious
2	8	11	O	side
3	13	19	O	effects
4	21	23	O	and
5	25	28	O	even
6	30	34	B-E	death
7	36	39	O	have
8	41	44	O	been
9	46	53	O	reported
10	55	63	O	following
11	65	67	O	the
12	69	79	O	concomitant
13	81	83	O	use
14	85	86	O	of
15	88	94	O	certain
16	96	100	B-D	drugs
17	102	105	O	with
18	107	109	B-D	MAO
19	111	120	I-D	inhibitors
D/16:6:1 D/18:6:1

Silenor	1702	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	22
Silenor is contraindicated in individuals who have shown hypersensitivity to doxepin HCl, any of its inactive ingredients, or other dibenzoxepines.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	25	O	contraindicated
4	27	28	O	in
5	30	40	O	individuals
6	42	44	O	who
7	46	49	O	have
8	51	55	O	shown
9	57	72	O	hypersensitivity
10	74	75	O	to
11	77	83	O	XXXXXXXX
12	85	87	O	HCl
13	88	88	O	,
14	90	92	O	any
15	94	95	O	of
16	97	99	O	its
17	101	108	O	inactive
18	110	120	O	ingredients
19	121	121	O	,
20	123	124	O	or
21	126	130	O	other
22	132	145	O	dibenzoxepines
NULL

Silenor	1703	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	14
Silenor is contraindicated in individuals with untreated narrow angle glaucoma or severe urinary retention.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	25	O	contraindicated
4	27	28	O	in
5	30	40	O	individuals
6	42	45	O	with
7	47	55	O	untreated
8	57	62	O	narrow
9	64	68	O	angle
10	70	77	O	glaucoma
11	79	80	O	or
12	82	87	O	severe
13	89	95	O	urinary
14	97	105	O	retention
NULL

Silenor	1704	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	17
The exact length of time may vary depending on the particular MAOI dosage and duration of treatment.
1	0	2	O	The
2	4	8	O	exact
3	10	15	O	length
4	17	18	O	of
5	20	23	O	time
6	25	27	O	may
7	29	32	O	vary
8	34	42	O	depending
9	44	45	O	on
10	47	49	O	the
11	51	60	O	particular
12	62	65	O	MAOI
13	67	72	O	dosage
14	74	76	O	and
15	78	85	O	duration
16	87	88	O	of
17	90	98	O	treatment
NULL

Silenor	1705	34070-3	1bec1223-5239-4eb6-a9e8-62444106d2c0	8
Untreated narrow angle glaucoma or severe urinary retention.
1	0	8	O	Untreated
2	10	15	O	narrow
3	17	21	O	angle
4	23	30	O	glaucoma
5	32	33	O	or
6	35	40	O	severe
7	42	48	O	urinary
8	50	58	O	retention
NULL

Silenor	1706	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	33
A case of severe hypoglycemia has been reported in a type II diabetic patient maintained on tolazamide (1 g/day) 11 days after the addition of oral doxepin (75 mg/day).
1	0	0	O	A
2	2	5	O	case
3	7	8	O	of
4	10	15	B-E	severe
5	17	28	I-E	hypoglycemia
6	30	32	O	has
7	34	37	O	been
8	39	46	O	reported
9	48	49	O	in
10	51	51	O	a
11	53	56	O	type
12	58	59	O	II
13	61	68	O	diabetic
14	70	76	O	patient
15	78	87	O	maintained
16	89	90	O	on
17	92	101	O	tolazamide
18	104	104	O	1
19	106	106	O	g
20	107	107	O	/
21	108	110	O	day
22	113	114	O	11
23	116	119	O	days
24	121	125	O	after
25	127	129	O	the
26	131	138	O	addition
27	140	141	O	of
28	143	146	O	oral
29	148	154	O	XXXXXXXX
30	157	158	O	75
31	160	161	O	mg
32	162	162	O	/
33	163	165	O	day
NULL

Silenor	1707	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	19
A maximum dose of 3 mg is recommended in adults and elderly when cimetidine is co-administered with Silenor.
1	0	0	O	A
2	2	8	O	maximum
3	10	13	O	dose
4	15	16	O	of
5	18	18	O	3
6	20	21	O	mg
7	23	24	O	is
8	26	36	O	recommended
9	38	39	O	in
10	41	46	O	adults
11	48	50	O	and
12	52	58	O	elderly
13	60	63	O	when
14	65	74	O	cimetidine
15	76	77	O	is
16	79	80	O	co
17	82	93	O	administered
18	95	98	O	with
19	100	106	O	XXXXXXXX
NULL

Silenor	1708	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	9
Alcohol: Sedative effects may be increased with doxepin.
1	0	6	B-D	Alcohol
2	7	7	O	:
3	9	16	B-E	Sedative
4	18	24	I-E	effects
5	26	28	O	may
6	30	31	O	be
7	33	41	O	increased
8	43	46	O	with
9	48	54	O	XXXXXXXX
D/1:3:1

Silenor	1709	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	6
Cimetidine: Increases exposure to doxepin.
1	0	9	B-K	Cimetidine
2	10	10	O	:
3	12	20	B-T	Increases
4	22	29	I-T	exposure
5	31	32	O	to
6	34	40	O	XXXXXXXX
K/1:C54355

Silenor	1710	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	13
CNS Depressants and Sedating Antihistamines: Sedative effects may be increased with doxepin.
1	0	2	B-D	CNS
2	4	14	I-D	Depressants
3	16	18	O	and
4	20	27	B-D	Sedating
5	29	42	O	Antihistamines
6	43	43	O	:
7	45	52	B-E	Sedative
8	54	60	I-E	effects
9	62	64	O	may
10	66	67	O	be
11	69	77	O	increased
12	79	82	O	with
13	84	90	O	XXXXXXXX
D/1:7:1 D/4:7:1

Silenor	1711	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	10
Inhibitors of these isozymes may increase the exposure of doxepin.
1	0	9	B-K	Inhibitors
2	11	12	I-K	of
3	14	18	I-K	these
4	20	27	I-K	isozymes
5	29	31	O	may
6	33	40	B-T	increase
7	42	44	I-T	the
8	46	53	I-T	exposure
9	55	56	O	of
10	58	64	O	XXXXXXXX
K/1:C54355

Silenor	1712	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	17
MAO inhibitors: Silenor should not be administered in patients on MAOIs within the past two weeks.
1	0	2	B-U	MAO
2	4	13	O	inhibitors
3	14	14	O	:
4	16	22	O	XXXXXXXX
5	24	29	O	should
6	31	33	O	not
7	35	36	B-T	be
8	38	49	I-T	administered
9	51	52	O	in
10	54	61	O	patients
11	63	64	O	on
12	66	70	O	MAOIs
13	72	77	O	within
14	79	81	O	the
15	83	86	O	past
16	88	90	O	two
17	92	96	O	weeks
NULL

Silenor	1713	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	16
Silenor exposure is doubled with concomitant administration of cimetidine, a nonspecific inhibitor of CYP isozymes.
1	0	6	O	XXXXXXXX
2	8	15	B-T	exposure
3	17	18	O	is
4	20	26	O	doubled
5	28	31	O	with
6	33	43	O	concomitant
7	45	58	O	administration
8	60	61	O	of
9	63	72	B-K	cimetidine
10	73	73	O	,
11	75	75	O	a
12	77	87	O	nonspecific
13	89	97	B-K	inhibitor
14	99	100	O	of
15	102	104	B-K	CYP
16	106	113	I-K	isozymes
K/9:C54358 K/13:C54358 K/15:C54355

Silenor	1714	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	13
Silenor is not an inhibitor of any CYP isozymes at therapeutically relevant concentrations.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	13	O	not
4	15	16	O	an
5	18	26	O	inhibitor
6	28	29	O	of
7	31	33	O	any
8	35	37	O	CYP
9	39	46	B-U	isozymes
10	48	49	O	at
11	51	65	O	therapeutically
12	67	74	O	relevant
13	76	89	O	concentrations
NULL

Silenor	1715	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	23
Silenor is primarily metabolized by hepatic cytochrome P450 isozymes CYP2C19 and CYP2D6, and to a lesser extent, by CYP1A2 and CYP2C9.
1	0	6	O	XXXXXXXX
2	8	9	O	is
3	11	19	O	primarily
4	21	31	O	metabolized
5	33	34	O	by
6	36	42	O	hepatic
7	44	53	O	cytochrome
8	55	58	O	P450
9	60	67	O	isozymes
10	69	75	O	CYP2C19
11	77	79	O	and
12	81	86	O	CYP2D6
13	87	87	O	,
14	89	91	O	and
15	93	94	O	to
16	96	96	O	a
17	98	103	O	lesser
18	105	110	O	extent
19	111	111	O	,
20	113	114	O	by
21	116	121	O	CYP1A2
22	123	125	O	and
23	127	132	O	CYP2C9
NULL

Silenor	1716	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	11
The ability of Silenor to induce CYP isozymes is not known.
1	0	2	O	The
2	4	10	O	ability
3	12	13	O	of
4	15	21	O	XXXXXXXX
5	23	24	O	to
6	26	31	O	induce
7	33	35	O	CYP
8	37	44	O	isozymes
9	46	47	O	is
10	49	51	O	not
11	53	57	O	known
NULL

Silenor	1717	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	10
Tolazamide: A case of severe hypoglycemia has been reported.
1	0	9	B-D	Tolazamide
2	10	10	O	:
3	12	12	O	A
4	14	17	O	case
5	19	20	O	of
6	22	27	B-E	severe
7	29	40	I-E	hypoglycemia
8	42	44	O	has
9	46	49	O	been
10	51	58	O	reported
D/1:6:1

Silenor	1718	34073-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	17
When taken with Silenor, the sedative effects of sedating antihistamines and CNS depressants may be potentiated.
1	0	3	O	When
2	5	9	O	taken
3	11	14	O	with
4	16	22	O	XXXXXXXX
5	23	23	O	,
6	25	27	O	the
7	29	36	B-E	sedative
8	38	44	I-E	effects
9	46	47	O	of
10	49	56	B-D	sedating
11	58	71	I-D	antihistamines
12	73	75	O	and
13	77	79	B-D	CNS
14	81	91	I-D	depressants
15	93	95	O	may
16	97	98	O	be
17	100	110	B-E	potentiated
D/10:7:1 D/10:17:1 D/13:7:1 D/13:17:1

Silenor	1719	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	15
Abnormal thinking, behavioral changes, complex behaviors: May include "Sleep-driving" and hallucinations.
1	0	7	O	Abnormal
2	9	16	O	thinking
3	17	17	O	,
4	19	28	O	behavioral
5	30	36	O	changes
6	37	37	O	,
7	39	45	O	complex
8	47	55	O	behaviors
9	56	56	O	:
10	58	60	O	May
11	62	68	O	include
12	70	75	O	"Sleep
13	77	84	O	driving"
14	86	88	O	and
15	90	103	O	hallucinations
NULL

Silenor	1720	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	16
After taking Silenor, patients should confine their activities to those necessary to prepare for bed.
1	0	4	O	After
2	6	11	O	taking
3	13	19	O	XXXXXXXX
4	20	20	O	,
5	22	29	O	patients
6	31	36	O	should
7	38	44	O	confine
8	46	50	O	their
9	52	61	O	activities
10	63	64	O	to
11	66	70	O	those
12	72	80	O	necessary
13	82	83	O	to
14	85	91	O	prepare
15	93	95	O	for
16	97	99	O	bed
NULL

Silenor	1721	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	47
Although behaviors such as "sleep-driving" may occur with hypnotics alone at therapeutic doses, the use of alcohol and other CNS depressants with hypnotics appears to increase the risk of such behaviors, as does the use of hypnotics at doses exceeding the maximum recommended dose.
1	0	7	O	Although
2	9	17	O	behaviors
3	19	22	O	such
4	24	25	O	as
5	27	32	O	"sleep
6	34	41	O	driving"
7	43	45	O	may
8	47	51	O	occur
9	53	56	O	with
10	58	66	B-D	hypnotics
11	68	72	O	alone
12	74	75	O	at
13	77	87	O	therapeutic
14	89	93	O	doses
15	94	94	O	,
16	96	98	O	the
17	100	102	O	use
18	104	105	O	of
19	107	113	B-D	alcohol
20	115	117	O	and
21	119	123	O	other
22	125	127	B-D	CNS
23	129	139	I-D	depressants
24	141	144	O	with
25	146	154	B-D	hypnotics
26	156	162	O	appears
27	164	165	O	to
28	167	174	B-E	increase
29	176	178	I-E	the
30	180	183	I-E	risk
31	185	186	O	of
32	188	191	O	such
33	193	201	O	behaviors
34	202	202	O	,
35	204	205	O	as
36	207	210	O	does
37	212	214	O	the
38	216	218	O	use
39	220	221	O	of
40	223	231	B-D	hypnotics
41	233	234	O	at
42	236	240	O	doses
43	242	250	O	exceeding
44	252	254	O	the
45	256	262	O	maximum
46	264	274	O	recommended
47	276	279	O	dose
D/10:28:1 D/19:28:1 D/22:28:1 D/25:28:1 D/40:28:1

Silenor	1722	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	11
Amnesia, anxiety and other neuro-psychiatric symptoms may occur unpredictably.
1	0	6	O	Amnesia
2	7	7	O	,
3	9	15	O	anxiety
4	17	19	O	and
5	21	25	O	other
6	27	31	O	neuro
7	33	43	O	psychiatric
8	45	52	O	symptoms
9	54	56	O	may
10	58	62	O	occur
11	64	76	B-E	unpredictably
NULL

Silenor	1723	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	34
Antidepressants increased the risk compared to placebo of suicidal thinking and behavior (suicidality) in children, adolescents, and young adults in short-term studies of major depressive disorder (MDD) and other psychiatric disorders.
1	0	14	O	Antidepressants
2	16	24	O	increased
3	26	28	O	the
4	30	33	O	risk
5	35	42	O	compared
6	44	45	O	to
7	47	53	O	placebo
8	55	56	O	of
9	58	65	O	suicidal
10	67	74	O	thinking
11	76	78	O	and
12	80	87	O	behavior
13	90	100	O	suicidality
14	103	104	O	in
15	106	113	O	children
16	114	114	O	,
17	116	126	O	adolescents
18	127	127	O	,
19	129	131	O	and
20	133	137	O	young
21	139	144	O	adults
22	146	147	O	in
23	149	153	O	short
24	155	158	O	term
25	160	166	O	studies
26	168	169	O	of
27	171	175	O	major
28	177	186	O	depressive
29	188	195	O	disorder
30	198	200	O	MDD
31	203	205	O	and
32	207	211	O	other
33	213	223	O	psychiatric
34	225	233	O	disorders
NULL

Silenor	1724	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	12
As with "sleep-driving", patients usually do not remember these events.
1	0	1	O	As
2	3	6	O	with
3	8	13	O	"sleep
4	15	22	O	driving"
5	23	23	O	,
6	25	32	O	patients
7	34	40	O	usually
8	42	43	O	do
9	45	47	O	not
10	49	56	O	remember
11	58	62	O	these
12	64	69	O	events
NULL

Silenor	1725	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	17
Avoid engaging in hazardous activities such as operating a motor vehicle or heavy machinery after taking drug.
1	0	4	O	Avoid
2	6	13	O	engaging
3	15	16	O	in
4	18	26	O	hazardous
5	28	37	O	activities
6	39	42	O	such
7	44	45	O	as
8	47	55	O	operating
9	57	57	O	a
10	59	63	O	motor
11	65	71	O	vehicle
12	73	74	O	or
13	76	80	O	heavy
14	82	90	O	machinery
15	92	96	O	after
16	98	103	O	taking
17	105	108	O	drug
NULL

Silenor	1726	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	31
Because sleep disturbances may be the presenting manifestation of a physical and/or psychiatric disorder, symptomatic treatment of insomnia should be initiated only after careful evaluation of the patient.
1	0	6	O	Because
2	8	12	O	sleep
3	14	25	O	disturbances
4	27	29	O	may
5	31	32	O	be
6	34	36	O	the
7	38	47	O	presenting
8	49	61	O	manifestation
9	63	64	O	of
10	66	66	O	a
11	68	75	O	physical
12	77	79	O	and
13	80	80	O	/
14	81	82	O	or
15	84	94	O	psychiatric
16	96	103	O	disorder
17	104	104	O	,
18	106	116	O	symptomatic
19	118	126	O	treatment
20	128	129	O	of
21	131	138	O	insomnia
22	140	145	O	should
23	147	148	O	be
24	150	158	O	initiated
25	160	163	O	only
26	165	169	O	after
27	171	177	O	careful
28	179	188	O	evaluation
29	190	191	O	of
30	193	195	O	the
31	197	203	O	patient
NULL

Silenor	1727	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	11
CNS-depressant effects: Use can impair alertness and motor coordination.
1	0	2	B-D	CNS
2	4	13	I-D	depressant
3	15	21	I-D	effects
4	22	22	O	:
5	24	26	O	Use
6	28	30	O	can
7	32	37	O	impair
8	39	47	O	alertness
9	49	51	O	and
10	53	57	O	motor
11	59	70	B-E	coordination
D/1:11:1

Silenor	1728	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	29
Complex behaviors such as "sleep-driving" (i.e., driving while not fully awake after ingestion of a hypnotic, with amnesia for the event) have been reported with hypnotics.
1	0	6	O	Complex
2	8	16	O	behaviors
3	18	21	O	such
4	23	24	O	as
5	26	31	O	"sleep
6	33	40	O	driving"
7	43	45	O	i.e
8	47	47	O	,
9	49	55	O	driving
10	57	61	O	while
11	63	65	O	not
12	67	71	O	fully
13	73	77	O	awake
14	79	83	O	after
15	85	93	O	ingestion
16	95	96	O	of
17	98	98	O	a
18	100	107	O	hypnotic
19	108	108	O	,
20	110	113	O	with
21	115	121	O	amnesia
22	123	125	O	for
23	127	129	O	the
24	131	135	O	event
25	138	141	O	have
26	143	146	O	been
27	148	155	O	reported
28	157	160	O	with
29	162	170	B-D	hypnotics
NULL

Silenor	1729	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	10
Depression: Worsening of depression or suicidal thinking may occur.
1	0	9	O	Depression
2	10	10	O	:
3	12	20	O	Worsening
4	22	23	O	of
5	25	34	O	depression
6	36	37	O	or
7	39	46	O	suicidal
8	48	55	O	thinking
9	57	59	O	may
10	61	65	O	occur
NULL

Silenor	1730	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	21
Doxepin, the active ingredient in Silenor, is an antidepressant at doses 10- to 100-fold higher than in Silenor.
1	0	6	O	XXXXXXXX
2	7	7	O	,
3	9	11	O	the
4	13	18	O	active
5	20	29	O	ingredient
6	31	32	O	in
7	34	40	O	XXXXXXXX
8	41	41	O	,
9	43	44	O	is
10	46	47	O	an
11	49	62	O	antidepressant
12	64	65	O	at
13	67	71	O	doses
14	73	74	O	10
15	77	78	O	to
16	80	82	O	100
17	84	87	O	fold
18	89	94	O	higher
19	96	99	O	than
20	101	102	O	in
21	104	110	O	XXXXXXXX
NULL

Silenor	1731	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	26
Due to the risk to the patient and the community, discontinuation of Silenor should be strongly considered for patients who report a "sleep-driving" episode.
1	0	2	O	Due
2	4	5	O	to
3	7	9	O	the
4	11	14	O	risk
5	16	17	O	to
6	19	21	O	the
7	23	29	O	patient
8	31	33	O	and
9	35	37	O	the
10	39	47	O	community
11	48	48	O	,
12	50	64	O	discontinuation
13	66	67	O	of
14	69	75	O	XXXXXXXX
15	77	82	O	should
16	84	85	O	be
17	87	94	O	strongly
18	96	105	O	considered
19	107	109	O	for
20	111	118	O	patients
21	120	122	O	who
22	124	129	O	report
23	131	131	O	a
24	133	138	O	"sleep
25	140	147	O	driving"
26	149	155	O	episode
NULL

Silenor	1732	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	23
Exacerbation of insomnia or the emergence of new cognitive or behavioral abnormalities may be the consequence of an unrecognized psychiatric or physical disorder.
1	0	11	O	Exacerbation
2	13	14	O	of
3	16	23	O	insomnia
4	25	26	O	or
5	28	30	O	the
6	32	40	O	emergence
7	42	43	O	of
8	45	47	O	new
9	49	57	O	cognitive
10	59	60	O	or
11	62	71	O	behavioral
12	73	85	O	abnormalities
13	87	89	O	may
14	91	92	O	be
15	94	96	O	the
16	98	108	O	consequence
17	110	111	O	of
18	113	114	O	an
19	116	127	O	unrecognized
20	129	139	O	psychiatric
21	141	142	O	or
22	144	151	O	physical
23	153	160	O	disorder
NULL

Silenor	1733	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	7
Immediately evaluate any new onset behavioral changes.
1	0	10	O	Immediately
2	12	19	O	evaluate
3	21	23	O	any
4	25	27	O	new
5	29	33	O	onset
6	35	44	O	behavioral
7	46	52	O	changes
NULL

Silenor	1734	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	28
In primarily depressed patients, worsening of depression, including suicidal thoughts and actions (including completed suicides), has been reported in association with the use of hypnotics.
1	0	1	O	In
2	3	11	O	primarily
3	13	21	O	depressed
4	23	30	O	patients
5	31	31	O	,
6	33	41	O	worsening
7	43	44	O	of
8	46	55	O	depression
9	56	56	O	,
10	58	66	O	including
11	68	75	O	suicidal
12	77	84	O	thoughts
13	86	88	O	and
14	90	96	O	actions
15	99	107	O	including
16	109	117	O	completed
17	119	126	O	suicides
18	128	128	O	,
19	130	132	O	has
20	134	137	O	been
21	139	146	O	reported
22	148	149	O	in
23	151	161	O	association
24	163	166	O	with
25	168	170	O	the
26	172	174	O	use
27	176	177	O	of
28	179	187	B-D	hypnotics
NULL

Silenor	1735	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	35
It can rarely be determined with certainty whether a particular instance of the abnormal behaviors listed above is drug induced, spontaneous in origin, or a result of an underlying psychiatric or physical disorder.
1	0	1	O	It
2	3	5	O	can
3	7	12	O	rarely
4	14	15	O	be
5	17	26	O	determined
6	28	31	O	with
7	33	41	O	certainty
8	43	49	O	whether
9	51	51	O	a
10	53	62	O	particular
11	64	71	O	instance
12	73	74	O	of
13	76	78	O	the
14	80	87	O	abnormal
15	89	97	O	behaviors
16	99	104	O	listed
17	106	110	O	above
18	112	113	O	is
19	115	118	O	drug
20	120	126	O	induced
21	127	127	O	,
22	129	139	O	spontaneous
23	141	142	O	in
24	144	149	O	origin
25	150	150	O	,
26	152	153	O	or
27	155	155	O	a
28	157	162	O	result
29	164	165	O	of
30	167	168	O	an
31	170	179	O	underlying
32	181	191	O	psychiatric
33	193	194	O	or
34	196	203	O	physical
35	205	212	O	disorder
NULL

Silenor	1736	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	19
Need to Evaluate for Co-morbid Diagnoses: Reevaluate if insomnia persists after 7 to 10 days of use.
1	0	3	O	Need
2	5	6	O	to
3	8	15	O	Evaluate
4	17	19	O	for
5	21	22	O	Co
6	24	29	O	morbid
7	31	39	O	Diagnoses
8	40	40	O	:
9	42	51	O	Reevaluate
10	53	54	O	if
11	56	63	O	insomnia
12	65	72	O	persists
13	74	78	O	after
14	80	80	O	7
15	82	83	O	to
16	85	86	O	10
17	88	91	O	days
18	93	94	O	of
19	96	98	O	use
NULL

Silenor	1737	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	18
Nonetheless, the emergence of any new behavioral sign or symptom of concern requires careful and immediate evaluation.
1	0	10	O	Nonetheless
2	11	11	O	,
3	13	15	O	the
4	17	25	O	emergence
5	27	28	O	of
6	30	32	O	any
7	34	36	O	new
8	38	47	O	behavioral
9	49	52	O	sign
10	54	55	O	or
11	57	63	O	symptom
12	65	66	O	of
13	68	74	O	concern
14	76	83	O	requires
15	85	91	O	careful
16	93	95	O	and
17	97	105	O	immediate
18	107	116	O	evaluation
NULL

Silenor	1738	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	31
Other complex behaviors (e.g., preparing and eating food, making phone calls, or having sex) have been reported in patients who are not fully awake after taking a hypnotic.
1	0	4	O	Other
2	6	12	O	complex
3	14	22	O	behaviors
4	25	27	O	e.g
5	29	29	O	,
6	31	39	O	preparing
7	41	43	O	and
8	45	50	O	eating
9	52	55	O	food
10	56	56	O	,
11	58	63	O	making
12	65	69	O	phone
13	71	75	O	calls
14	76	76	O	,
15	78	79	O	or
16	81	86	O	having
17	88	90	O	sex
18	93	96	O	have
19	98	101	O	been
20	103	110	O	reported
21	112	113	O	in
22	115	122	O	patients
23	124	126	O	who
24	128	130	O	are
25	132	134	O	not
26	136	140	O	fully
27	142	146	O	awake
28	148	152	O	after
29	154	159	O	taking
30	161	161	O	a
31	163	170	B-U	hypnotic
NULL

Silenor	1739	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	44
Patients should avoid engaging in hazardous activities, such as operating a motor vehicle or heavy machinery, at night after taking Silenor, and should be cautioned about potential impairment in the performance of such activities that may occur the day following ingestion.
1	0	7	O	Patients
2	9	14	O	should
3	16	20	O	avoid
4	22	29	O	engaging
5	31	32	O	in
6	34	42	O	hazardous
7	44	53	O	activities
8	54	54	O	,
9	56	59	O	such
10	61	62	O	as
11	64	72	O	operating
12	74	74	O	a
13	76	80	O	motor
14	82	88	O	vehicle
15	90	91	O	or
16	93	97	O	heavy
17	99	107	O	machinery
18	108	108	O	,
19	110	111	O	at
20	113	117	O	night
21	119	123	O	after
22	125	130	O	taking
23	132	138	O	XXXXXXXX
24	139	139	O	,
25	141	143	O	and
26	145	150	O	should
27	152	153	O	be
28	155	163	O	cautioned
29	165	169	O	about
30	171	179	O	potential
31	181	190	O	impairment
32	192	193	O	in
33	195	197	O	the
34	199	209	O	performance
35	211	212	O	of
36	214	217	O	such
37	219	228	O	activities
38	230	233	O	that
39	235	237	O	may
40	239	243	O	occur
41	245	247	O	the
42	249	251	O	day
43	253	261	O	following
44	263	271	O	ingestion
NULL

Silenor	1740	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	19
Patients should be cautioned about potential additive effects of Silenor used in combination with CNS depressants or sedating antihistamines.
1	0	7	O	Patients
2	9	14	O	should
3	16	17	O	be
4	19	27	O	cautioned
5	29	33	O	about
6	35	43	O	potential
7	45	52	B-T	additive
8	54	60	B-E	effects
9	62	63	O	of
10	65	71	O	XXXXXXXX
11	73	76	O	used
12	78	79	O	in
13	81	91	O	combination
14	93	96	O	with
15	98	100	B-D	CNS
16	102	112	I-D	depressants
17	114	115	O	or
18	117	124	O	sedating
19	126	139	O	antihistamines
D/15:8:1

Silenor	1741	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	7
Patients should not consume alcohol with Silenor.
1	0	7	O	Patients
2	9	14	O	should
3	16	18	B-T	not
4	20	26	I-T	consume
5	28	34	B-U	alcohol
6	36	39	O	with
7	41	47	O	XXXXXXXX
NULL

Silenor	1742	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	16
Patients with severe sleep apnea: Silenor is ordinarily not recommended for use in this population.
1	0	7	O	Patients
2	9	12	O	with
3	14	19	O	severe
4	21	25	O	sleep
5	27	31	O	apnea
6	32	32	O	:
7	34	40	O	XXXXXXXX
8	42	43	O	is
9	45	54	O	ordinarily
10	56	58	O	not
11	60	70	O	recommended
12	72	74	O	for
13	76	78	O	use
14	80	81	O	in
15	83	86	O	this
16	88	97	O	population
NULL

Silenor	1743	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	13
Potential additive effects when used in combination with CNS depressants or sedating antihistamines.
1	0	8	B-E	Potential
2	10	17	B-E	additive
3	19	25	I-E	effects
4	27	30	O	when
5	32	35	O	used
6	37	38	O	in
7	40	50	O	combination
8	52	55	O	with
9	57	59	B-D	CNS
10	61	71	I-D	depressants
11	73	74	O	or
12	76	83	B-D	sedating
13	85	98	I-D	antihistamines
D/9:1:1 D/9:2:1 D/12:1:1 D/12:2:1

Silenor	1744	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	9
Prescribe the least amount feasible to avoid intentional overdose.
1	0	8	O	Prescribe
2	10	12	O	the
3	14	18	O	least
4	20	25	O	amount
5	27	34	O	feasible
6	36	37	O	to
7	39	43	B-T	avoid
8	45	55	O	intentional
9	57	64	O	overdose
NULL

Silenor	1745	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	13
Risk from the lower dose of doxepin in Silenor can not be excluded.
1	0	3	O	Risk
2	5	8	O	from
3	10	12	O	the
4	14	18	O	lower
5	20	23	O	dose
6	25	26	O	of
7	28	34	O	XXXXXXXX
8	36	37	O	in
9	39	45	O	XXXXXXXX
10	47	49	O	can
11	51	53	O	not
12	55	56	O	be
13	58	65	O	excluded
NULL

Silenor	1746	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	12
Such findings have emerged during the course of treatment with hypnotic drugs.
1	0	3	O	Such
2	5	12	O	findings
3	14	17	O	have
4	19	25	O	emerged
5	27	32	O	during
6	34	36	O	the
7	38	43	O	course
8	45	46	O	of
9	48	56	O	treatment
10	58	61	O	with
11	63	70	B-D	hypnotic
12	72	76	I-D	drugs
NULL

Silenor	1747	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	30
The failure of insomnia to remit after 7 to 10 days of treatment may indicate the presence of a primary psychiatric and/or medical illness that should be evaluated.
1	0	2	O	The
2	4	10	O	failure
3	12	13	O	of
4	15	22	O	insomnia
5	24	25	O	to
6	27	31	O	remit
7	33	37	O	after
8	39	39	O	7
9	41	42	O	to
10	44	45	O	10
11	47	50	O	days
12	52	53	O	of
13	55	63	O	treatment
14	65	67	O	may
15	69	76	O	indicate
16	78	80	O	the
17	82	89	O	presence
18	91	92	O	of
19	94	94	O	a
20	96	102	O	primary
21	104	114	O	psychiatric
22	116	118	O	and
23	119	119	O	/
24	120	121	O	or
25	123	129	O	medical
26	131	137	O	illness
27	139	142	O	that
28	144	149	O	should
29	151	152	O	be
30	154	162	O	evaluated
NULL

Silenor	1748	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	14
These events can occur in hypnotic-naive as well as in hypnotic-experienced persons.
1	0	4	O	These
2	6	11	O	events
3	13	15	O	can
4	17	21	O	occur
5	23	24	O	in
6	26	33	B-D	hypnotic
7	35	39	I-D	naive
8	41	42	O	as
9	44	47	O	well
10	49	50	O	as
11	52	53	O	in
12	55	62	B-D	hypnotic
13	64	74	O	experienced
14	76	82	O	persons
NULL

Silenor	1749	43685-7	1bec1223-5239-4eb6-a9e8-62444106d2c0	22
When taken with Silenor, the sedative effects of alcoholic beverages, sedating antihistamines, and other CNS depressants may be potentiated.
1	0	3	O	When
2	5	9	O	taken
3	11	14	O	with
4	16	22	O	XXXXXXXX
5	23	23	O	,
6	25	27	O	the
7	29	36	B-E	sedative
8	38	44	I-E	effects
9	46	47	O	of
10	49	57	B-D	alcoholic
11	59	67	I-D	beverages
12	68	68	O	,
13	70	77	B-D	sedating
14	79	92	I-D	antihistamines
15	93	93	O	,
16	95	97	O	and
17	99	103	O	other
18	105	107	B-D	CNS
19	109	119	I-D	depressants
20	121	123	O	may
21	125	126	O	be
22	128	138	B-E	potentiated
D/10:7:1 D/10:22:1 D/13:7:1 D/13:22:1 D/18:7:1 D/18:22:1

