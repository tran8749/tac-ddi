<?xml version="1.0" ?>
<Label drug="Doxepin" setid="c66a11c1-3093-45ef-b348-3b196c05ba0c">
  <Text>
    <Section id="34090-1" name="CLINICAL PHARMACOLOGY SECTION">
CLINICAL PHARMACOLOGY Although doxepin HCl does have H1 and H2 histamine receptor blocking actions, the exact mechanism by which doxepin exerts its antipruritic effect is unknown. PRUDOXIN Cream can produce drowsiness which may reduce awareness, including awareness of pruritic symptoms. In 19 pruritic eczema patients treated with PRUDOXIN Cream, plasma doxepin concentrations ranged from nondetectable to 47 ng/mL from percutaneous absorption. Plasma levels from topical application of PRUDOXIN Cream can result in CNS and other systemic side effects.  Once absorbed into the systemic circulation, doxepin undergoes hepatic metabolism that results in conversion to pharmacologically-active desmethyldoxepin. Further glucuronidation results in urinary excretion of the parent drug and its metabolites. Desmethyldoxepin has a half-life that ranges from 28 to 52 hours and is not affected by multiple dosing. Plasma levels of both doxepin and desmethyldoxepin are highly variable and are poorly correlated with dosage. Wide distribution occurs in body tissues including lungs, heart, brain, and liver. Renal disease, genetic factors, age, and other medications affect the metabolism and subsequent elimination of doxepin. (See PRECAUTIONS - Drug Interactions .)</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Doxepin" id="119" section="34073-7">
      

      <SentenceText>Studies have not been performed examining drug interactions with PRUDOXIN Cream.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="120" section="34073-7">
      

      <SentenceText>However, since plasma levels of doxepin following topical application of PRUDOXIN Cream can reach levels obtained with oral doxepin HCl therapy, the following drug interactions are possible following topical PRUDOXIN Cream application: Drugs Metabolized by P450 2D6: The biochemical activity of the drug metabolizing isozyme cytochrome P450 2D6 (debrisoquin hydroxylase) is reduced in a subset of the Caucasian population (about 7-10% of Caucasians are so-called &quot;poor metabolizers&quot;); reliable estimates of the prevalence of reduced P450 2D6 isozyme activity among Asian, African and other populations are not yet available.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="121" section="34073-7">
      

      <SentenceText>Poor metabolizers have higher than expected plasma concentrations of tricyclic antidepressants (TCAs) when given usual doses.</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="23 42" str="higher than expected plasma concentrations" type="Trigger"/>
      <Mention code="NO MAP" id="M2" span="69 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Interaction effect="C54357" id="I1" precipitant="M2" trigger="M1" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="122" section="34073-7">
      

      <SentenceText>Depending on the fraction of drug metabolized by P450 2D6, the increase in plasma concentration may be small, or quite large (8-fold increase in plasma AUC of the TCA).</SentenceText>
      

      <Mention code="NO MAP" id="M3" span="72 2" str="in" type="Trigger"/>
      <Mention code="NO MAP" id="M4" span="133 22" str="increase in plasma AUC" type="Trigger"/>
      <Mention code="NO MAP" id="M5" span="29 28" str="drug metabolized by P450 2D6" type="Precipitant"/>
      <Interaction effect="C54355" id="I2" precipitant="M5" trigger="M4" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="123" section="34073-7">
      

      <SentenceText>In addition, certain drugs inhibit the activity of this isozyme and make normal metabolizers resemble poor metabolizers.</SentenceText>
      

      <Mention code="NO MAP" id="M6" span="39 8" str="activity" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="124" section="34073-7">
      

      <SentenceText>An individual who is stable on a given dosage regimen of a TCA may become abruptly toxic when given one of these inhibiting drugs as concomitant therapy.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="125" section="34073-7">
      

      <SentenceText>The drugs that inhibit cytochrome P450 2D6 include some that are not metabolized by the enzyme (quinidine; cimetidine) and many that are substrates for P450 2D6 (many other antidepressants, phenothiazines, and the Type 1C antiarrhythmics propafenone and flecainide).</SentenceText>
      

      <Mention code="NO MAP" id="M7" span="65 15" str="not metabolized" type="Trigger"/>
      <Mention code="NO MAP" id="M8" span="23 15" str="cytochrome P450" type="Precipitant"/>
      <Interaction id="I3" precipitant="M8" trigger="M7" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="126" section="34073-7">
      

      <SentenceText>While all the selective serotonin reuptake inhibitors (SSRIs), e.g., fluoxetine, sertraline, and paroxetine, inhibit P450 2D6, they may vary in the extent of inhibition.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="127" section="34073-7">
      

      <SentenceText>The extent to which SSRI-TCA interactions may pose clinical problems will depend on the degree of inhibition and the pharmacokinetics of the SSRI involved.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="128" section="34073-7">
      

      <SentenceText>Nevertheless, caution is indicated in the co-administration of TCAs with any of the SSRIs.</SentenceText>
      

      <Mention code="NO MAP" id="M9" span="14 7" str="caution" type="Trigger"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="129" section="34073-7">
      

      <SentenceText>Of particular importance, sufficient time must elapse before initiating TCA treatment in a patient being withdrawn from fluoxetine, given the long half-life of the parent and active metabolite (at least 5 weeks may be necessary).</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="130" section="34073-7">
      

      <SentenceText>Concomitant use of tricyclic antidepressants with drugs that can inhibit cytochrome P450 2D6 may require lower doses than usually prescribed for either the tricyclic antidepressant or the other drug.</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="105 24" str="lower doses than usually" type="Trigger"/>
      <Mention code="NO MAP" id="M11" span="19 25" str="tricyclic antidepressants" type="Precipitant"/>
      <Mention code="NO MAP" id="M12" span="84 8" str="P450 2D6" type="Precipitant"/>
      <Mention code="NO MAP" id="M13" span="156 9" str="tricyclic" type="Precipitant"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="131" section="34073-7">
      

      <SentenceText>It is desirable to monitor TCA plasma levels whenever a TCA is going to be co-administered with another drug known to be an inhibitor of P450 2D6.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="132" section="34073-7">
      

      <SentenceText>MAO Inhibitors: Serious side effects and even death have been reported following the concomitant use of certain drugs with MAO inhibitors.</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="46 5" str="death" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M15" span="0 14" str="MAO Inhibitors" type="Precipitant"/>
      <Interaction effect="M14" id="I4" precipitant="M15" trigger="M15" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M16" span="123 14" str="MAO inhibitors" type="Precipitant"/>
      <Interaction effect="M14" id="I5" precipitant="M16" trigger="M16" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="133" section="34073-7">
      

      <SentenceText>Therefore, MAO inhibitors should be discontinued at least two weeks prior to the cautious initiation of therapy with PRUDOXIN Cream.</SentenceText>
      

      <Mention code="NO MAP" id="M17" span="81 8" str="cautious" type="Trigger"/>
      <Mention code="NO MAP" id="M18" span="11 14" str="MAO inhibitors" type="Precipitant"/>
      <Interaction id="I6" precipitant="M18" trigger="M17" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M19" span="117 14" str="PRUDOXIN Cream" type="Precipitant"/>
      <Interaction id="I7" precipitant="M19" trigger="M17" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="134" section="34073-7">
      

      <SentenceText>The exact length of time may vary and is dependent upon the particular MAO inhibitor being used, the length of time it has been administered, and the dosage involved.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="135" section="34073-7">
      

      <SentenceText>Cimetidine: Serious anticholinergic symptoms (i.e., severe dry mouth, urinary retention and blurred vision) have been associated with elevations in the serum levels of tricyclic antidepressant when cimetidine therapy is initiated.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="136" section="34073-7">
      

      <SentenceText>Additionally, higher than expected tricyclic antidepressant levels have been observed when they are begun in patients already taking cimetidine.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="137" section="34073-7">
      

      <SentenceText>Alcohol: Alcohol ingestion may exacerbate the potential sedative effects of PRUDOXIN Cream.</SentenceText>
      

      <Mention code="NO MAP" id="M20" span="31 44" str="exacerbate the potential sedative effects of" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M21" span="0 7" str="Alcohol" type="Precipitant"/>
      <Interaction effect="M20" id="I8" precipitant="M21" trigger="M21" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M22" span="9 7" str="Alcohol" type="Precipitant"/>
      <Interaction effect="M20" id="I9" precipitant="M22" trigger="M22" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M23" span="76 14" str="PRUDOXIN Cream" type="Precipitant"/>
      <Interaction effect="M20" id="I10" precipitant="M23" trigger="M23" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="138" section="34073-7">
      

      <SentenceText>This is especially important in patients who may use alcohol excessively.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="53 7" str="alcohol" type="Precipitant"/>
      <Interaction id="I11" precipitant="M24" trigger="M24" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="139" section="34073-7">
      

      <SentenceText>Tolazamide: A case of severe hypoglycemia has been reported in a type II diabetic patient maintained on tolazamide (1 gm/day) 11 days after the addition of oral doxepin (75 mg/day).</SentenceText>
      

      <Mention code="NO MAP" id="M25" span="22 19" str="severe hypoglycemia" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M26" span="0 10" str="Tolazamide" type="Precipitant"/>
      <Interaction effect="M25" id="I12" precipitant="M26" trigger="M26" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M27" span="104 10" str="tolazamide" type="Precipitant"/>
      <Interaction effect="M25" id="I13" precipitant="M27" trigger="M27" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="140" section="34090-1">
      

      <SentenceText>Although doxepin HCl does have H1 and H2 histamine receptor blocking actions, the exact mechanism by which doxepin exerts its antipruritic effect is unknown.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="141" section="34090-1">
      

      <SentenceText>PRUDOXIN Cream can produce drowsiness which may reduce awareness, including awareness of pruritic symptoms.</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="27 10" str="drowsiness" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M29" span="89 17" str="pruritic symptoms" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M30" span="0 14" str="PRUDOXIN Cream" type="Precipitant"/>
      <Interaction effect="M28;M29" id="I14" precipitant="M30" trigger="M30" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="142" section="34090-1">
      

      <SentenceText>In 19 pruritic eczema patients treated with PRUDOXIN Cream, plasma doxepin concentrations ranged from nondetectable to 47 ng/mL from percutaneous absorption.</SentenceText>
      

      <Mention code="NO MAP" id="M31" span="90 6" str="ranged" type="Trigger"/>
      <Mention code="NO MAP" id="M32" span="44 14" str="PRUDOXIN Cream" type="Precipitant"/>
      <Interaction effect="C54358" id="I15" precipitant="M32" trigger="M31" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="143" section="34090-1">
      

      <SentenceText>Plasma levels from topical application of PRUDOXIN Cream can result in CNS and other systemic side effects.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="144" section="34090-1">
      

      <SentenceText>Once absorbed into the systemic circulation, doxepin undergoes hepatic metabolism that results in conversion to pharmacologically-active desmethyldoxepin.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="145" section="34090-1">
      

      <SentenceText>Further glucuronidation results in urinary excretion of the parent drug and its metabolites.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="146" section="34090-1">
      

      <SentenceText>Desmethyldoxepin has a half-life that ranges from 28 to 52 hours and is not affected by multiple dosing.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="147" section="34090-1">
      

      <SentenceText>Plasma levels of both doxepin and desmethyldoxepin are highly variable and are poorly correlated with dosage.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="148" section="34090-1">
      

      <SentenceText>Wide distribution occurs in body tissues including lungs, heart, brain, and liver.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Doxepin" id="149" section="34090-1">
      

      <SentenceText>Renal disease, genetic factors, age, and other medications affect the metabolism and subsequent elimination of doxepin.</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

