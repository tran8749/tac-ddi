import os, sys, time, random, re, json, collections, argparse
import sklearn.metrics
import pandas as pd
import numpy as np
import tensorflow as tf

DEBUG_MODE = False

parser = argparse.ArgumentParser(description='Process drug labels')
parser.add_argument('--nlm180', default=False, action='store_true',
                    help='additionally train on full nlm180 data')
parser.add_argument('--no-embeddings', default=False, action='store_true',
                    help='use word embeddings')
parser.add_argument('--relation-bias', default=10.0, type=float, action='store',
                    help='bias factor toward relation type')
parser.add_argument('--core-bias', default=3.0, type=float, action='store',
                    help='bias factor toward tac-22')
parser.add_argument('--output-dir', type=str, default=None,
                    help='path to dump data')

NeuralModel = collections.namedtuple('NeuralModel',  ['word_vocab', 'seqlen', 'sess', 
        'saver', 'dropout_keep', 'relation_bias', 'core_bias', 'x_input', 'xz_input', 
        'x_length', 'is_core', 'output_ner', 'output_pd', 'output_pk'])

Leaf = collections.namedtuple('Leaf',  ['labels','y_true','y_out','y_loss','train_step'])
Eval = collections.namedtuple('Eval',  ['f','p','r','acc'])

def main(args):
    print(args)
    
    if args.output_dir == None:
        args.output_dir = ('xval-tcnnfull-nlm{:d}-e{:d}-c{:.1f}-r{:.1f}'
                            .format(args.nlm180,not args.no_embeddings, 
                                    args.core_bias, args.relation_bias).lower())
    print('Target output directory: {}'.format(args.output_dir))

    with open('tac-22.txt','r') as f:
        dataset = load_data(f)

    dataset_extra = []
    if args.nlm180:
        with open('nlm-180.txt','r') as f:
            dataset_extra = load_data(f)

    label_dist, seqlen_mean, seqlen_max = data_stats(dataset + dataset_extra)
    print("Dataset size> {}".format(len(dataset)))
    print("Token length> Mean={} Max={}".format(seqlen_mean,seqlen_max))
    print("Label distribution> {}".format(label_dist))

    dataset_pd = pd_examples(dataset + dataset_extra)
    dataset_pk = pk_examples(dataset + dataset_extra)
    pd_labels, _, _ = data_stats(dataset_pd)
    pk_labels, _, _ = data_stats(dataset_pk)
    print("PD label distribution> {}".format(pd_labels))
    print("PK label distribution> {}".format(pk_labels))

    word_vocab = build_vocab(dataset + dataset_extra)
    print("Word vocab> Length={} : .., {},..".format(len(word_vocab),', '.join(word_vocab[-100:-50])))

    buckets = {}
    for (attr, x), y in dataset:
        dname = attr[0]
        if dname not in buckets:
            buckets[dname] = []

        buckets[dname].append(((attr, x), y))

    flatten = lambda l: [item for sublist in l for item in sublist]

    drugnames = sorted(list(buckets.keys()))
    for k in range(int(len(drugnames)/2)):
        d1 = drugnames.pop()
        d2 = drugnames.pop()

        d1path = '{}/{}.txt'.format(args.output_dir,d1.replace(' ','_'))
        d2path = '{}/{}.txt'.format(args.output_dir,d2.replace(' ','_'))
        if os.path.exists(d1path) and os.path.exists(d2path):
            print('Skipping {}, {}'.format(d1,d2))
            continue

        testset = buckets[d1] + buckets[d2]
        trainset = [ b for k, b in buckets.items() if k not in [d1,d2] ]

        # core dataset
        devset = flatten(trainset[:4])
        trainset = flatten(trainset[4:]) 
        trainset += dataset_extra
        
        print('@ Train set',len(trainset))
        print('@ Dev set', len(devset))
        print('@ Test set {} > {}, {}'.format(len(testset),d1,d2))
        
        #trainset = apply_unks(trainset, word_vocab)
        #devset = apply_unks(devset, word_vocab)
        
        # Build the model
        if args.no_embeddings:
            word_embeddings = None
        else:
            word_embeddings = load_embeddings('PubMed-w2v.bin',word_vocab)
        
        model = new_model(word_vocab, seqlen_max, relation_bias=args.relation_bias,
                            core_bias=args.core_bias, word_embeddings=word_embeddings)

        model = train_model(model,trainset, devset)

        #testset = apply_unks(testset, word_vocab)
        testset_feed = compile_examples(model, testset)
        
        print("Test Set Evaluation> ")
        test_eval = eval_model(model,testset_feed)
        print("    Core:  f1={:.2%}  p={:.2%}  r={:.2%}  acc={:.2%}".format(*test_eval))

        testset_feed_pd = compile_examples(model, testset, target='pd')
        if testset_feed_pd is not None:
            test_eval_pd = eval_model_outcome(model,testset_feed_pd, target='pd')
            print("    PD:    f1={:.2%}  p={:.2%}  r={:.2%}  acc={:.2%}".format(*test_eval_pd))
        else:
            print("    PD:    No examples to test on")
    
        testset_feed_pk = compile_examples(model, testset, target='pk')
        if testset_feed_pk is not None:
            test_eval_pk = eval_model_outcome(model,testset_feed_pk, target='pk')
            print("    PK:    f1={:.2%}  p={:.2%}  r={:.2%}  acc={:.2%}".format(*test_eval_pk))
        else:
            print("    PK:    No examples to test on")

        # annotate the test set
        y_true, y_pred = model_predict(model,testset_feed)
        testset_annotated = []
        for (attr, x), y in testset:
            dname, sent_id, sect_id, set_id, orig_sent, _ = attr

            yp = []
            for i in range(len(y)):
                yp.append(y_pred.pop(0))

            yp = fix_iob(yp)

            pds = []
            pks = []
            for i in range(len(yp)):
                for j in range(len(yp)):
                    if yp[i] == 'B-D' and yp[j] == 'B-E':
                        pds.append((i,j,'NULL'))
            
                if yp[i] == 'B-K':
                    pks.append((i,'NULL'))
                    
            attr = dname, sent_id, sect_id, set_id, orig_sent, (pds,pks)
            annotated_example = [((attr, x), yp)]

            if len(pds) > 0:
                pd_feed = compile_examples(model,annotated_example, target='pd')
                _, y_pred_pd = model_predict_outcome(model,pd_feed,target='pd')
                for i, (prep_pos, eff_pos, code) in enumerate(pds):
                    if y_pred_pd[i] == 'POS':
                        y_pred_pd[i] = '1'
                    else:
                        y_pred_pd[i] = '0'
                    pds[i] = (prep_pos,eff_pos,y_pred_pd[i])

            if len(pks) > 0:
                pk_feed = compile_examples(model,annotated_example, target='pk')
                _, y_pred_pk = model_predict_outcome(model,pk_feed,target='pk')
                for i, (prep_pos, code) in enumerate(pks):
                    pks[i] = (prep_pos,y_pred_pk[i])
            
            attr = dname, sent_id, sect_id, set_id, orig_sent, (pds,pks)
            testset_annotated.append(((attr, x), yp))

        assert(len(y_pred) == 0)
        save_data(testset_annotated, args.output_dir)

def train_model(model, trainset, devset):
    # Train the model
    dropout_keep = 0.80
    batch_size = int(len(trainset)/300) + 1
    
    if DEBUG_MODE:
        num_epoch = 2
    else:
        num_epoch = 30
    
    tmproot = 'tmp'

    devfeed = compile_examples(model, devset)
    trainsample = random.sample(trainset,len(devset))
    fitfeed = compile_examples(model, trainsample)

    pk_devfeed = compile_examples(model, devset + trainsample, target='pk')
    pd_devfeed = compile_examples(model, devset + trainsample, target='pd')

    sess_id = int(time.time())
    best_df1 = -1
    best_model = None 
    print('Num. Epochs: {}, Batch Size: {}'.format(num_epoch,batch_size))
    for ep in range(1,num_epoch+1):
        random.shuffle(trainset)
        core_loss = 1e2
        pd_loss = 1e2
        pk_loss = 1e2
        for i in range(0,len(trainset),batch_size):
            minibatch = trainset[i:i+batch_size]
            batch_feed = compile_examples(model, minibatch, keep_prob=dropout_keep)
            _, loss = model.sess.run([model.output_ner.train_step, model.output_ner.y_loss], batch_feed)
            core_loss = core_loss * 0.5 + loss * 0.5

            batch_feed_pd = compile_examples(model, minibatch, target='pd', keep_prob=dropout_keep)
            if batch_feed_pd is not None:
                _, loss = model.sess.run([model.output_pd.train_step, model.output_pd.y_loss], batch_feed_pd)
                pd_loss = pd_loss * 0.5 + loss * 0.5

            batch_feed_pk = compile_examples(model, minibatch, target='pk', keep_prob=dropout_keep)
            if batch_feed_pk is not None:
                _, loss = model.sess.run([model.output_pk.train_step, model.output_pk.y_loss], batch_feed_pk)
                pk_loss = pk_loss * 0.5 + loss * 0.5

            print("epoch {}> iter {}> {}/{} loss>  core={:.4f}  pd={:.4f}  pk={:.4f}        "
                .format(ep, int(i/batch_size), i, len(trainset), core_loss, pd_loss, pk_loss),end='\r')

        b_eval = eval_model(model,fitfeed)
        d_eval = eval_model(model,devfeed)
        dp_eval = eval_model_outcome(model,pd_devfeed, target='pd')
        dk_eval = eval_model_outcome(model,pk_devfeed, target='pk')
        if d_eval.f > best_df1:
            best_df1 = d_eval.f
            best_model = "{}/model-{}-ep{}.ckpt".format(tmproot,sess_id,ep)
            
            if not os.path.exists(tmproot):
                os.mkdir(tmproot)
            
            model.saver.save(model.sess, best_model)
            marker = '*'
        else:
            marker = ' '
            
        print("epoch {}> loss {} fit={:.2%}  dev> f={:.2%} p={:.0%} r={:.0%} pd={:.2%} pk={:.2%} {}"
                    .format(ep, core_loss, b_eval.f, d_eval.f, d_eval.p, d_eval.r, 
                            dp_eval.f, dk_eval.f, marker))

    print("Restoring best model: {}".format(best_model))
    model.saver.restore(model.sess, best_model)
    
    print("Dev. Set Evaluation> ")
    dev_eval = eval_model(model,devfeed)
    print("    Core:  f1={:.2%}  p={:.2%}  r={:.2%}  acc={:.2%}".format(*dev_eval))
    dev_eval_pd = eval_model_outcome(model,pd_devfeed, target='pd')
    print("    PD:    f1={:.2%}  p={:.2%}  r={:.2%}  acc={:.2%}".format(*dev_eval_pd))
    dev_eval_pk = eval_model_outcome(model,pk_devfeed, target='pk')
    print("    PK:    f1={:.2%}  p={:.2%}  r={:.2%}  acc={:.2%}".format(*dev_eval_pk))

    return model

def data_stats(dataset):
    label_freq = {}
    token_lengths = []

    for (sent_id, x), y in dataset:
        if not isinstance(y,list):
            y = [y]

        for label in y:
            if label in label_freq:
                label_freq[label] += 1
            else:
                label_freq[label] = 1
    
        token_lengths.append(len(x))
    
    return label_freq, np.mean(token_lengths), np.max(token_lengths)

def model_predict(model, feed_dict):
    y_true, y_out = model.sess.run([model.output_ner.y_true, 
                                model.output_ner.y_out], feed_dict)

    y_true_masked = []
    y_out_masked = []
    for i in range(y_true.shape[0]):
        for j in range(y_true.shape[1]):
            if np.sum(y_true[i,j,:]) == 0:
                continue
        
            y_true_masked.append(y_true[i,j,:])
            y_out_masked.append(y_out[i,j,:])

    labelize = lambda i: model.output_ner.labels[i]
    y_pred = [ labelize(idx) for idx in np.argmax(y_out_masked,axis=-1) ]
    y_true = [ labelize(idx) for idx in np.argmax(y_true_masked,axis=-1) ]
    return y_true, y_pred

def eval_model(model, feed_dict):
    y_true, y_pred = model_predict(model, feed_dict)

    target_labels = [ l for l in model.output_ner.labels if l != 'O' ]
    f1 = sklearn.metrics.f1_score(y_true, y_pred, 
            average='micro', labels=target_labels)
    precision = sklearn.metrics.precision_score(y_true, y_pred, 
            average='micro',  labels=target_labels)
    recall = sklearn.metrics.recall_score(y_true, y_pred, 
            average='micro',  labels=target_labels)
    acc = sklearn.metrics.accuracy_score(y_true, y_pred)
    return Eval(f1, precision, recall, acc)

def eval_model_outcome(model, feed_dict, target = 'pk'):
    
    y_true, y_pred = model_predict_outcome(model, feed_dict, 
                                    target=target)

    if target == 'pd':
        labels = model.output_pd.labels
    elif target == 'pk':
        labels = model.output_pk.labels
    else:
        raise Exception('what')

    target_labels = [ l for l in labels if l not in ['NEG','NULL'] ]
    f1 = sklearn.metrics.f1_score(y_true, y_pred, 
            average='micro', labels=target_labels)
    precision = sklearn.metrics.precision_score(y_true, y_pred, 
            average='micro',  labels=target_labels)
    recall = sklearn.metrics.recall_score(y_true, y_pred, 
            average='micro',  labels=target_labels)
    acc = sklearn.metrics.accuracy_score(y_true, y_pred)
    return Eval(f1, precision, recall, acc)

def model_predict_outcome(model, feed_dict, target = 'pk'):
    if target == 'pd':
        output_node = model.output_pd
    elif target == 'pk':
        output_node = model.output_pk
    else:
        raise Exception('what')
        
    y_true, y_out = model.sess.run([output_node.y_true, 
                            output_node.y_out], feed_dict)

    labelize = lambda i: output_node.labels[i]
    y_pred = [ labelize(idx) for idx in np.argmax(y_out,axis=-1) ]
    y_true = [ labelize(idx) for idx in np.argmax(y_true,axis=-1) ]
    return y_true, y_pred

def pk_examples(dataset):
    examples = []
    for (attr, x), y in dataset:
        dname, sent_id, sect_id, set_id, orig_sent, (pds,pks) = attr
        for prep_pos, code in pks:
            x_bound = entity_bind(prep_pos,x,y)
            examples.append((((sent_id, prep_pos), x_bound), code))
        
    return examples

def pd_examples(dataset):
    examples = []
    for (attr, x), y in dataset:
        dname, sent_id, sect_id, set_id, orig_sent, (pds,pks) = attr
        for prep_pos, eff_pos, code in pds:
            x_bound = entity_bind(prep_pos,x,y)
            x_bound = entity_bind(eff_pos,x_bound,y)
            examples.append((((sent_id, prep_pos, eff_pos), x_bound), code))

    return examples

def entity_bind(ent_pos, x, y):
    inside = False
    bound_x = []
    for i in range(len(y)):
        if not inside and i == ent_pos:
            inside = True
        elif y[i][0] == 'B' or y[i] == 'O':
            inside = False
        
        if inside:
            if y[i][-1] in ['K','D']:
                entok = 'YYYYYYYY'
            elif y[i][-1] == 'E':
                entok = 'ZZZZZZZZ'
            else:
                inside = False
                entok = x[i][2]
                #raise Exception('what? {}'.format(y[i]))

            bound_x.append((x[i][0],x[i][1],entok))
        else:
            bound_x.append(x[i])
    
    return bound_x

def apply_unks(dataset, word_vocab):
    new_dataset = []
    for (attr, x), y in dataset:
        x_new = []
        for start, end, w in x:
            w2 = w if w in word_vocab else 'UNK'
            x_new.append((start, end, w2))

        new_dataset.append(((attr, x_new), y))

    return new_dataset

def build_vocab(dataset):
    word_freq = {}
    for (sent_id, x), y in dataset:
        for start, end, w in x:
            w = w.lower()
            if w not in word_freq:
                word_freq[w] = 1
            else:
                word_freq[w] += 1
    
    word_vocab = ['ZERO','UNK', 'XXXXXXXX', 'YYYYYYYY', 'ZZZZZZZZ' ]
    word_vocab +=  sorted([ w for w, freq in word_freq.items() 
                        if freq >= 1 and w not in word_vocab ])

    return word_vocab

def compile_examples(model, examples, target='core', keep_prob=1):
    bx_length = []
    bx_input = []
    bxz_input = []
    by_true = []
    by_true_pk = []
    by_true_pd = []
    is_core = []

    onehot = lambda i, n : np.eye(n)[i,:]

    for (attr, x), y in examples:
        _, _, _, set_id, _, (pds,pks) = attr
        
        if set_id == 'NO_SETID':
            is_core.append(0)
        else:
            is_core.append(1)

        assert(len(x) == len(y))

        if target == 'core':
            xencoded = encode(x,model.word_vocab,model.seqlen)
            bx_input.append(xencoded)
            bx_length.append(len(x))

            lhot = []
            for label in y:
                lhot.append(onehot(model.output_ner.labels.index(label), 
                                len(model.output_ner.labels)))

            by_true.append(lhot + [ np.zeros(len(model.output_ner.labels)) 
                                for i in range(model.seqlen - len(x)) ])
        elif target == 'pd':
            for prep_pos, eff_pos, code in pds:
                xencoded = encode(x,model.word_vocab,model.seqlen)
                bx_input.append(xencoded)
                bx_length.append(len(x))

                xz_bound = entity_bind(prep_pos,x,y)
                xz_bound = entity_bind(eff_pos,xz_bound,y)
                zencoded = encode(xz_bound,model.word_vocab,model.seqlen)
                bxz_input.append(zencoded)

                if code == 1:
                    code = 'POS'
                else:
                    code = 'NEG'
                by_true_pd.append(onehot(model.output_pd.labels.index(code), 
                                        len(model.output_pd.labels)))
        elif target == 'pk':
            for prep_pos, code in pks:
                xencoded = encode(x,model.word_vocab,model.seqlen)
                bx_input.append(xencoded)
                bx_length.append(len(x))

                xz_bound = entity_bind(prep_pos,x,y)
                zencoded = encode(xz_bound,model.word_vocab,model.seqlen)
                bxz_input.append(zencoded)
                by_true_pk.append(onehot(model.output_pk.labels.index(code), 
                                        len(model.output_pk.labels)))

    
    feed_dict = {   model.x_input : np.array(bx_input), 
                    model.x_length : np.array(bx_length), 
                    model.is_core : np.array(is_core),
                    model.dropout_keep : keep_prob }
    
    if target == 'core':
        feed_dict[model.output_ner.y_true] = np.array(by_true)
    elif target in ['pd','pk']:
        if len(bxz_input) == 0:
            return None
        
        feed_dict[model.xz_input] = np.array(bxz_input)

        if target == 'pd':
            feed_dict[model.output_pd.y_true] = np.array(by_true_pd)
        else:
            feed_dict[model.output_pk.y_true] = np.array(by_true_pk)
    else:
        raise Exception('what')

    return feed_dict

def encode(x,word_vocab,seqlen):
    tidx = []
    for start, end, w in x:
        if w not in word_vocab:
            w = w.lower()

        tidx.append(word_vocab.index(w))
    
    return tidx + [0] * (seqlen - len(x))

randinit = lambda x: tf.truncated_normal(x, stddev=0.1)
def new_model(word_vocab, seqlen, relation_bias=10.0, core_bias=10.0,
                word_embeddings=None, embedding_size=200, 
                rnn_hidden_size=100, filter_sizes=[3,4,5],
                num_filters=50):
    
    tf.reset_default_graph()
    x_input = tf.placeholder(tf.int32, [None, seqlen])
    xz_input = tf.placeholder(tf.int32, [None, seqlen])
    x_length = tf.placeholder(tf.int32, [None])
    is_core = tf.placeholder(tf.float32, [None])

    dropout_keep = tf.placeholder(tf.float32, None)
    relation_bias = tf.constant(relation_bias)
    core_bias = tf.constant(core_bias)

    w_input = tf.placeholder(tf.float32, [len(word_vocab), embedding_size])
    W_em = tf.Variable(randinit([len(word_vocab), embedding_size]))
    embedding_init = W_em.assign(w_input)

    xw_input = tf.nn.embedding_lookup(W_em, x_input)

    # ----------------------------------------------------------------------------

    powerbase = 2
    num_layers = int(np.log(seqlen)) + 1
    num_filters = 100
    filter_size = 3

    xw_input_projection = tf.layers.dense(xw_input, num_filters)

    with tf.variable_scope('fw_out'):
        layers = []
        for i in range(num_layers):
            dilation_size = powerbase ** i

            if i == 0:
                xin = xw_input_projection
            else:
                xin = layers[-1]
            
            padding_size = (filter_size-1) * dilation_size
            padding = tf.constant([[0,0],[1,0],[0,0]]) * padding_size

            xin_pad = tf.reshape(tf.pad(xin, padding), 
                        shape=[-1, seqlen + padding_size, num_filters])
            
            xout = tf.layers.conv1d(xin_pad, num_filters, 
                        kernel_size=filter_size, strides=1,
                        dilation_rate=dilation_size, 
                        activation=tf.nn.relu)
            
            xout = tf.contrib.layers.layer_norm(xout)
            xout = tf.nn.dropout(xout,noise_shape=[1, 1, num_filters], 
                                keep_prob=dropout_keep)

            layers.append(xout)

        fw_out = layers[-1]
    
    with tf.variable_scope('bw_out'):
        layers = []
        for i in range(num_layers):
            dilation_size = powerbase ** i

            if i == 0:
                xin = tf.reverse(xw_input_projection,axis=[1])
            else:
                xin = layers[-1]
            
            padding_size = (filter_size-1) * dilation_size
            padding = tf.constant([[0,0],[1,0],[0,0]]) * padding_size

            xin_pad = tf.reshape(tf.pad(xin, padding), 
                        shape=[-1, seqlen + padding_size, num_filters])
            
            xout = tf.layers.conv1d(xin_pad, num_filters, 
                        kernel_size=filter_size, strides=1,
                        dilation_rate=dilation_size, 
                        activation=tf.nn.relu)
            xout = tf.contrib.layers.layer_norm(xout)
            
            xout = tf.nn.dropout(xout,noise_shape=[1, 1, num_filters], 
                                keep_prob=dropout_keep)

            layers.append(xout)

        bw_out = tf.reverse(layers[-1],axis=[1])

    print("Receptive field={}".format(1 + (filter_size - 1)*(2 ** num_layers - 1)))
    # -------- BRANCH --- NER
    ner_leaf = branch_ner(fw_out, bw_out, xw_input, embedding_size, 
                            seqlen, is_core, dropout_keep, x_length, 
                            rnn_hidden_size, core_bias, relation_bias)
    
    # -------- BRANCH --- OUTCOME: PD AND PK
    xzw_input = tf.nn.embedding_lookup(W_em, xz_input)
    pd_leaf, pk_leaf = branch_outcome(fw_out, bw_out, xzw_input, seqlen, 
                                        dropout_keep, embedding_size, 
                                        rnn_hidden_size, filter_sizes,
                                        num_filters)
    
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    if word_embeddings is not None:
        sess.run(embedding_init, { w_input: word_embeddings })

    param_count = 0
    for v in tf.trainable_variables():
        param_count += np.prod([ int(dimsize) for dimsize in v.get_shape() ])

    print("Compiled model with {} variables and {} parameters".format(
            len(tf.trainable_variables()),param_count))
    
    saver = tf.train.Saver(max_to_keep=100)

    return NeuralModel(word_vocab, seqlen, sess, saver, 
                        dropout_keep, relation_bias, core_bias, 
                        x_input, xz_input, x_length, is_core, 
                        ner_leaf, pd_leaf, pk_leaf)

def branch_ner(fw_out, bw_out, x_input, embedding_size, 
                seqlen, is_core, dropout_keep, x_length, 
                rnn_hidden_size, core_bias, relation_bias):
    labels = [ 'O', 'B-D', 'I-D', 'B-K', 'I-K', 
                'B-U', 'I-U' ,'B-T', 'I-T', 'B-E', 'I-E' ]

    y_true = tf.placeholder(tf.float32, [None, seqlen, len(labels)], name='y_true')

    output_states = tf.concat([fw_out, bw_out, x_input], axis=-1)
    output_states_drop = tf.nn.dropout(output_states, 
                            noise_shape=[1, 1, 2*rnn_hidden_size + embedding_size], 
                            keep_prob=dropout_keep)

    # ----------------------------------------------------------------------------

    powerbase = 2
    num_layers = 3 #int(np.log(seqlen)) + 1
    num_filters = 200
    filter_size = 3

    layers = []
    for i in range(num_layers):
        dilation_size = powerbase ** i

        if i == 0:
            xin = tf.layers.dense(output_states_drop, num_filters)
        else:
            xin = layers[-1]
        
        padding_size = (filter_size-1) * dilation_size
        padding = tf.constant([[0,0],[1,0],[0,0]]) * padding_size

        xin_pad = tf.reshape(tf.pad(xin, padding), 
                    shape=[-1, seqlen + padding_size, num_filters])
        
        xout = tf.layers.conv1d(xin_pad, num_filters, 
                    kernel_size=filter_size, strides=1,
                    dilation_rate=dilation_size, 
                    activation=tf.nn.relu)
        xout = tf.contrib.layers.layer_norm(xout)
        
        xout = tf.nn.dropout(xout,noise_shape=[1, 1, num_filters], 
                            keep_prob=dropout_keep)
        
        xout_pad = tf.reshape(tf.pad(xout, padding), 
                    shape=[-1, seqlen + padding_size, num_filters])
        
        xout = tf.layers.conv1d(xout_pad, num_filters, 
                    kernel_size=filter_size, strides=1,
                    dilation_rate=dilation_size,
                    activation=tf.nn.relu)
        xout = tf.contrib.layers.layer_norm(xout)
        
        xout = tf.nn.dropout(xout,noise_shape=[1, 1, num_filters], 
                            keep_prob=dropout_keep)

        layers.append(tf.nn.relu(xin + xout))

    # ----------------------------------------------------------------------------

    outputs_flat = tf.reshape(layers[-1],shape=[-1, num_filters])

    W_out = tf.Variable(randinit([num_filters, len(labels)]))
    b_out = tf.Variable(randinit([len(labels)]))
    y_logits = tf.reshape(outputs_flat @ W_out + b_out, shape=[-1, seqlen, len(labels)])

    y_out = tf.nn.softmax(y_logits)

    seq_mask = tf.sequence_mask(x_length, seqlen, dtype=tf.float32) # bsize, seqlen

    y_true_flat = tf.reshape(y_true,shape=[-1,len(labels)])
    y_logit_flat = tf.reshape(y_logits,shape=[-1,len(labels)])

    losses_flat = tf.losses.softmax_cross_entropy(y_true_flat, y_logit_flat,
                                    reduction=tf.losses.Reduction.NONE)
    
    y_true_label = tf.argmax(y_true_flat,axis=-1)
    y_true_switch = tf.cast(tf.clip_by_value(y_true_label,0,1),dtype=tf.float32)

    losses = y_true_switch * losses_flat * relation_bias + (1 - y_true_switch) * losses_flat
    masked_losses = tf.reshape(losses,shape=[-1,seqlen]) * seq_mask
    normalized_losses = tf.reduce_sum(masked_losses,axis=-1) / tf.cast(x_length,dtype=tf.float32)
    y_loss = tf.reduce_sum(is_core * normalized_losses * core_bias + (1 - is_core) * normalized_losses)

    print("Receptive field={}".format(1 + (filter_size - 1)*(2 ** num_layers - 1)))

    train_step = tf.train.AdamOptimizer(learning_rate=0.001).minimize(y_loss)
    return Leaf(labels, y_true, y_out, y_loss, train_step)

def branch_outcome(fw_out, bw_out, x_input, seqlen, 
                    dropout_keep, embedding_size,
                    rnn_hidden_size, filter_sizes, 
                    num_filters):

    pd_labels = ['NULL','NEG', 'POS']
    pk_labels = ['NULL'] + list(pkcode_map.keys())
    
    y_true_pd = tf.placeholder(tf.float32, [None, len(pd_labels)], name='y_true_pd')
    y_true_pk = tf.placeholder(tf.float32, [None, len(pk_labels)], name='y_true_pk')

    output_states = tf.concat([fw_out, bw_out, x_input], axis=-1)
    output_states_drop = tf.nn.dropout(output_states, keep_prob=dropout_keep)
    output_states_exp = tf.expand_dims(output_states_drop,axis=-1)
    input_width = rnn_hidden_size * 2 + embedding_size

    pooled_outputs = []
    outcome_variables = []
    for i, filter_size in enumerate(filter_sizes):
        filter_shape = [filter_size, input_width, 1, num_filters]
        W_conv = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.15))
        b_conv = tf.Variable(tf.constant(0.1, shape=[num_filters]))
        outcome_variables += [W_conv, b_conv]
        conv = tf.nn.conv2d(output_states_exp, W_conv, strides=[1, 1, 1, 1], padding="VALID")

        h = tf.nn.relu(tf.nn.bias_add(conv, b_conv))
        pooled_outputs.append(tf.reduce_max(h,axis=1))

    num_filters_total = num_filters * len(filter_sizes)
    cnn_pool = tf.reshape(tf.concat(pooled_outputs,axis=-1), [-1, num_filters_total])


    y_out_pd = tf.layers.dense(tf.nn.dropout(cnn_pool, dropout_keep),len(pd_labels), name='pd_out')
    y_out_pk = tf.layers.dense(tf.nn.dropout(cnn_pool, dropout_keep),len(pk_labels), name='pk_out')

    with tf.variable_scope('pd_out', reuse=True):
        outcome_variables.append(tf.get_variable('kernel'))
        outcome_variables.append(tf.get_variable('bias'))
    
    with tf.variable_scope('pk_out', reuse=True):
        outcome_variables.append(tf.get_variable('kernel'))
        outcome_variables.append(tf.get_variable('bias'))

    y_loss_pd = tf.losses.softmax_cross_entropy(y_true_pd,y_out_pd)
    opt_pd = tf.train.AdamOptimizer(learning_rate=0.001)
    gradients_pd = opt_pd.compute_gradients(y_loss_pd)
    for i, (grad, var) in enumerate(gradients_pd):
        if grad is not None and var not in outcome_variables:
            gradients_pd[i] = (tf.scalar_mul(0.1,grad), var)

    train_step_pd = opt_pd.apply_gradients(gradients_pd)

    y_loss_pk = tf.losses.softmax_cross_entropy(y_true_pk,y_out_pk)
    opt_pk = tf.train.AdamOptimizer(learning_rate=0.001)
    gradients_pk = opt_pk.compute_gradients(y_loss_pk)
    for i, (grad, var) in enumerate(gradients_pk):
        if grad is not None and var not in outcome_variables:
            gradients_pk[i] = (tf.scalar_mul(0.1,grad), var)

    train_step_pk = opt_pk.apply_gradients(gradients_pk)

    return (Leaf(pd_labels, y_true_pd, y_out_pd, y_loss_pd, train_step_pd),
            Leaf(pk_labels, y_true_pk, y_out_pk, y_loss_pk, train_step_pk))

def load_data(f):
    cast_int = lambda x: tuple([ int(z) if not z.startswith('C') else z for z in x ])

    examples = []
    while True:
        header = f.readline()
        if header == "":
            break

        orig_sent = f.readline().strip()
        if orig_sent == "":
            break
        
        dname, sent_id, sect_id, set_id, num_tokens = header.split('\t')

        x = []
        y = []
        for i in range(int(num_tokens)):
            _, st, ed, label, tok = f.readline().rstrip().split('\t')
            x.append((int(st), int(ed), tok))
            y.append(label)

        linkages = f.readline().strip()
        
        pds = [ cast_int(z) for z in 
                re.findall(r'D\/([0-9]+):([0-9]+):([01])',linkages) ]
        pks = [ cast_int(z) for z in 
                re.findall(r'K\/([0-9]+):(C[0-9]+)',linkages) ]
        
        examples.append((((dname, sent_id, sect_id, set_id, 
                            orig_sent, (pds,pks)), x), y))
        f.readline()

    return examples

def save_data(dataset, outdir='outputs'):
    dnames = []
    for (attr, x), y in dataset:
        dname = attr[0].replace(' ','_')
        if dname not in dnames:
            dnames.append(dname)

    if not os.path.exists(outdir):
        os.mkdir(outdir)

    filehandler = {}
    for dname in dnames:
        filehandler[dname] = open('{}/{}.txt'.format(outdir,dname),'w')

    for (attr, x), y in dataset:
        dname, sent_id, sect_id, set_id, orig_sent, (pds,pks) = attr
        f = filehandler[dname.replace(' ','_')]
        print(dname, sent_id, sect_id, set_id, len(x), sep='\t', file=f)
        print(orig_sent, file=f)
        for i, ((start, end, w), t) in enumerate(zip(x,y)):
            print(i, start, end, t, w, sep='\t', file=f)
        
        linkpd = ' '.join(['D/' + ':'.join([ str(z) for z in v]) for v in pds])
        linkpk = ' '.join(['K/' + ':'.join([ str(z) for z in v]) for v in pks])
        link = linkpd + linkpk
        if link == '':
            link = 'NULL'
        
        print(link, file=f)
        print('', file=f)
    
    for fh in filehandler.values():
        fh.close()

def load_embeddings(fname,vocab,dim=200):
    from gensim.models import KeyedVectors
    shape = (len(vocab), dim)
    weight_matrix = np.random.uniform(-0.10, 0.10, shape).astype(np.float32)
    w2v = KeyedVectors.load_word2vec_format(fname, binary=True)

    c = 0 
    for i in range(len(vocab)):
        if vocab[i] in w2v.vocab:
            weight_matrix[i,:] = w2v[vocab[i]]
        else:
            c += 1
    
    print("{}/{} pretrained word embeddings loaded".format(len(vocab)-c,len(vocab)))
    return weight_matrix

def fix_iob(y):
    y_new = [x for x in y ]
    for i in range(len(y)):
        if y[i] != 'O':
            if i > 0:
                if y[i-1] == 'O':
                    y_new[i] = 'B-' + y[i][-1]
                elif y[i-1][-1] != y[i][-1]:
                    y_new[i] = 'B-' + y[i][-1]
            else:
                if not y[i].startswith('B'):
                    y_new[i] = 'B-' + y[i][-1]

        #print(y[i],'\t',y_new[i])

    return y_new

pkcode_map = {'C54355': 'INCREASED DRUG LEVEL',
            'C54602': 'INCREASED DRUG CMAX',
            'C54603': 'INCREASED DRUG HALF LIFE',
            'C54604': 'INCREASED DRUG TMAX',
            'C54605': 'INCREASED DRUG AUC',
            'C54357': 'INCREASED CONCOMITANT DRUG LEVEL',
            'C54610': 'INCREASED CONCOMITANT DRUG CMAX',
            'C54611': 'INCREASED CONCOMITANT DRUG HALF LIFE',
            'C54612': 'INCREASED CONCOMITANT DRUG TMAX',
            'C54613': 'INCREASED CONCOMITANT DRUG AUC',
            'C54356': 'DECREASED DRUG LEVEL',
            'C54606': 'DECREASED DRUG CMAX',
            'C54607': 'DECREASED DRUG HALF LIFE',
            'C54608': 'DECREASED DRUG TMAX',
            'C54609': 'DECREASED DRUG AUC',
            'C54358': 'DECREASED CONCOMITANT DRUG LEVEL',
            'C54615': 'DECREASED CONCOMITANT DRUG CMAX',
            'C54616': 'DECREASED CONCOMITANT DRUG HALF LIFE',
            'C54617': 'DECREASED CONCOMITANT DRUG TMAX',
            'C54614': 'DECREASED CONCOMITANT DRUG AUC' }

if __name__ == '__main__':
    main(parser.parse_args())