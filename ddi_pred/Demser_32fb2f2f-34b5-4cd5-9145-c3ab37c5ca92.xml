<?xml version="1.0" ?>
<Label drug="Demser" setid="32fb2f2f-34b5-4cd5-9145-c3ab37c5ca92">
  <Text>
    <Section id="34070-3" name="CONTRAINDICATIONS SECTION">
CONTRAINDICATIONS 

 DEMSER is contraindicated in persons known to be hypersensitive to this compound.</Section>
    

    <Section id="34071-1" name="WARNINGS SECTION">
WARNINGS 

 Maintain Fluid Volume During and After Surgery 

 When DEMSER is used preoperatively, alone or especially in combination with alpha-adrenergic blocking drugs, adequate intravascular volume must be maintained intraoperatively (especially after tumor removal) and postoperatively to avoid hypotension and decreased perfusion of vital organs resulting from vasodilatation and expanded volume capacity. Following tumor removal, large volumes of plasma may be needed to maintain blood pressure and central venous pressure within the normal range. 
 In addition, life-threatening arrhythmias may occur during anesthesia and surgery, and may require treatment with a beta-blocker or lidocaine. During surgery, patients should have continuous monitoring of blood pressure and electrocardiogram. 

 Intraoperative Effects 

 While the preoperative use of DEMSER in patients with pheochromocytoma is thought to decrease intraoperative problems with blood pressure control, DEMSER does not eliminate the danger of hypertensive crises or arrhythmias during manipulation of the tumor, and the alpha-adrenergic blocking drug, phentolamine, may be needed. 

 Interaction with Alcohol 

 DEMSER may add to the sedative effects of alcohol and other CNS depressants, e.g., hypnotics, sedatives, and tranquilizers. (See 

 PRECAUTIONS, Information for Patients 

 and 
 Drug Interactions 
 )</Section>
    

    <Section id="42232-9" name="PRECAUTIONS SECTION">
PRECAUTIONS 

 General 

 Metyrosine Crystalluria 

 Crystalluria and urolithiasis have been found in dogs treated with DEMSER (Metyrosine) at doses similar to those used in humans, and crystalluria has also been observed in a few patients. To minimize the risk of crystalluria, patients should be urged to maintain water intake sufficient to achieve a daily urine volume of 2000 mL or more, particularly when doses greater than 2 g per day are given. Routine examination of the urine should be carried out. Metyrosine will crystallize as needles or rods. If metyrosine crystalluria occurs, fluid intake should be increased further. If crystalluria persists, the dosage should be reduced or the drug discontinued. 

 Relatively Little Data Regarding Long-term Use 

 The total human experience with the drug is quite limited and few patients have been studied long-term. Chronic animal studies have not been carried out. Therefore, suitable laboratory tests should be carried out periodically in patients requiring prolonged use of DEMSER and caution should be observed in patients with impaired hepatic or renal function. 

 Information for Patients 

 When receiving DEMSER, patients should be warned about engaging in activities requiring mental alertness and motor coordination, such as driving a motor vehicle or operating machinery. DEMSER may have additive sedative effects with alcohol and other CNS depressants, e.g., hypnotics, sedatives, and tranquilizers. 
 Patients should be advised to maintain a liberal fluid intake. (See 

 PRECAUTIONS, General 

 ) 

 Drug Interactions 

 Caution should be observed in administering DEMSER to patients receiving phenothiazines or haloperidol because the extrapyramidal effects of these drugs can be expected to be potentiated by inhibition of catecholamine synthesis. 
 Concurrent use of DEMSER with alcohol or other CNS depressants can increase their sedative effects. (See 
 WARNINGS 
 and 

 PRECAUTIONS, Information for Patients 

 .) 

 Laboratory Test Interference 

 Spurious increases in urinary catecholamines may be observed in patients receiving DEMSER due to the presence of metabolites of the drug. 

 Carcinogenesis, Mutagenesis, Impairment of Fertility 

 Long-term carcinogenic studies in animals and studies on mutagenesis and impairment of fertility have not been performed with metyrosine. 

 Pregnancy 

 Pregnancy Category C 

 Animal reproduction studies have not been conducted with DEMSER. It is also not known whether DEMSER can cause fetal harm when administered to a pregnant woman or can affect reproduction capacity. DEMSER should be given to a pregnant woman only if clearly needed. 

 Nursing Mothers 

 It is not known whether DEMSER is excreted in human milk. Because many drugs are excreted in human milk, caution should be exercised when DEMSER is administered to a nursing woman. 

 Pediatric Use 

 Safety and effectiveness in pediatric patients below the age of 12 years have not been established. 

 Geriatric Use 

 Clinical studies of DEMSER did not include sufficient numbers of subjects aged 65 and over to determine whether they respond differently from younger subjects. Other reported clinical experience has not identified differences in responses between the elderly and younger patients. In general, dose selection for an elderly patient should be cautious, usually starting at the low end of the dosing range, reflecting the greater frequency of decreased hepatic, renal, or cardiac function, and of concomitant disease or other drug therapy.</Section>
    

    <Section id="34068-7" name="DOSAGE &amp; ADMINISTRATION SECTION">
DOSAGE AND ADMINISTRATION 

 The recommended initial dosage of DEMSER for adults and children 12 years of age and older is 250 mg orally four times daily. This may be increased by 250 mg to 500 mg every day to a maximum of 4.0 g/day in divided doses. When used for preoperative preparation, the optimally effective dosage of DEMSER should be given for at least five to seven days. 
 Optimally effective dosages of DEMSER usually are between 2.0 and 3.0 g/day, and the dose should be titrated by monitoring clinical symptoms and catecholamine excretion. In patients who are hypertensive, dosage should be titrated to achieve normalization of blood pressure and control of clinical symptoms. In patients who are usually normotensive, dosage should be titrated to the amount that will reduce urinary metanephrines and/or vanillylmandelic acid by 50 percent or more. 
 If patients are not adequately controlled by the use of DEMSER, an alpha-adrenergic blocking agent (phenoxybenzamine) should be added. 
 Use of DEMSER in children under 12 years of age has been limited and a dosage schedule for this age group cannot be given.</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Demser" id="100" section="42232-9">
      

      <SentenceText>Concurrent use of DEMSER with alcohol or other CNS depressants can increase their sedative effects .</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="82 16" str="sedative effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="30 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="M1" id="I1" precipitant="M2" trigger="M2" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M3" span="47 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="M1" id="I2" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Demser" id="101" section="42232-9">
      

      <SentenceText>DEMSER may have additive sedative effects with alcohol and other CNS depressants , e.g. , hypnotics , sedatives , and tranquilizers .</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="25 16" str="sedative effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M5" span="47 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="M4" id="I3" precipitant="M5" trigger="M5" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M6" span="65 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="M4" id="I4" precipitant="M6" trigger="M6" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M7" span="90 9" str="hypnotics" type="Precipitant"/>
      <Interaction effect="M4" id="I5" precipitant="M7" trigger="M7" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M8" span="102 9" str="sedatives" type="Precipitant"/>
      <Interaction effect="M4" id="I6" precipitant="M8" trigger="M8" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M9" span="118 13" str="tranquilizers" type="Precipitant"/>
      <Interaction effect="M4" id="I7" precipitant="M9" trigger="M9" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Demser" id="93" section="34068-7">
      

      <SentenceText>If patients are not adequately controlled by the use of DEMSER , an alpha-adrenergic blocking agent ( phenoxybenzamine ) should be added .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Demser" id="94" section="34068-7">
      

      <SentenceText>Optimally effective dosages of DEMSER usually are between 2.0 and 3.0 g/day , and the dose should be titrated by monitoring clinical symptoms and catecholamine excretion .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Demser" id="95" section="34071-1">
      

      <SentenceText>DEMSER may add to the sedative effects of alcohol and other CNS depressants , e.g. , hypnotics , sedatives , and tranquilizers .</SentenceText>
      

      <Mention code="NO MAP" id="M10" span="22 16" str="sedative effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M11" span="42 7" str="alcohol" type="Precipitant"/>
      <Interaction effect="M10" id="I8" precipitant="M11" trigger="M11" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M12" span="60 15" str="CNS depressants" type="Precipitant"/>
      <Interaction effect="M10" id="I9" precipitant="M12" trigger="M12" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M13" span="85 9" str="hypnotics" type="Precipitant"/>
      <Interaction effect="M10" id="I10" precipitant="M13" trigger="M13" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M14" span="97 9" str="sedatives" type="Precipitant"/>
      <Interaction effect="M10" id="I11" precipitant="M14" trigger="M14" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M15" span="113 13" str="tranquilizers" type="Precipitant"/>
      <Interaction effect="M10" id="I12" precipitant="M15" trigger="M15" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Demser" id="96" section="34071-1">
      

      <SentenceText>When DEMSER is used preoperatively , alone or especially in combination with alpha-adrenergic blocking drugs , adequate intravascular volume must be maintained intraoperatively ( especially after tumor removal ) and postoperatively to avoid hypotension and decreased perfusion of vital organs resulting from vasodilatation and expanded volume capacity .</SentenceText>
      

      <Mention code="NO MAP" id="M16" span="241 11" str="hypotension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M17" span="257 35" str="decreased perfusion of vital organs" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M18" span="308 14" str="vasodilatation" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M19" span="327 24" str="expanded volume capacity" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M20" span="77 31" str="alpha-adrenergic blocking drugs" type="Precipitant"/>
      <Interaction effect="M16;M17;M18;M19" id="I13" precipitant="M20" trigger="M20" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Demser" id="97" section="34071-1">
      

      <SentenceText>While the preoperative use of DEMSER in patients with pheochromocytoma is thought to decrease intraoperative problems with blood pressure control , DEMSER does not eliminate the danger of hypertensive crises or arrhythmias during manipulation of the tumor , and the alpha-adrenergic blocking drug , phentolamine , may be needed .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Demser" id="98" section="34073-7">
      

      <SentenceText>(See WARNINGS and PRECAUTIONS, Information for Patients .)</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Demser" id="99" section="42232-9">
      

      <SentenceText>Caution should be observed in administering DEMSER to patients receiving phenothiazines or haloperidol because the extrapyramidal effects of these drugs can be expected to be potentiated by inhibition of catecholamine synthesis .</SentenceText>
      

      <Mention code="NO MAP" id="M21" span="115 22" str="extrapyramidal effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M22" span="73 14" str="phenothiazines" type="Precipitant"/>
      <Interaction effect="M21" id="I14" precipitant="M22" trigger="M22" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M23" span="91 11" str="haloperidol" type="Precipitant"/>
      <Interaction effect="M21" id="I15" precipitant="M23" trigger="M23" type="Pharmacodynamic interaction"/>
    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

