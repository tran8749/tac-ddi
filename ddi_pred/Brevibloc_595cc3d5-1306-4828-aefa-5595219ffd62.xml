<?xml version="1.0" ?>
<Label drug="Brevibloc" setid="595cc3d5-1306-4828-aefa-5595219ffd62">
  <Text>
    <Section id="34068-7" name="DOSAGE &amp; ADMINISTRATION SECTION">
2 DOSAGE AND ADMINISTRATION 

 • Administer intravenously ( 2.1 , 2.2 ) 

 • Titrate using ventricular rate or blood pressure at ≥4-minute intervals. ( 2.1 , 2.2 ) 

 • Supraventricular tachycardia (SVT) or noncompensatory sinus tachycardia ( 2.1 ) 

 • Optional loading dose: 500 mcg per kg infused over one minute 

 • Then 50 mcg per kg per minute for the next 4 minutes 

 • Adjust dose as needed to a maximum of 200 mcg per kg per minute. 

 • Additional loading doses may be administered 

 • Perioperative tachycardia and hypertension ( 2.2 ) 

 • Loading dose: 500 mcg per kg over 1 minute for gradual control (1 mg per kg over 30 seconds for immediate control) 

 • Then 50 mcg per kg per min for gradual control (150 mcg per kg per minute for immediate control) adjusted to a maximum of 200 (tachycardia) or 300 (hypertension) mcg per kg per min ( 2.2 ) 

 2.1 Dosing for the Treatment of Supraventricular Tachycardia or Noncompensatory Sinus Tachycardia 

 BREVIBLOC is administered by continuous intravenous infusion with or without a loading dose. Additional loading doses and/or titration of the maintenance infusion (step-wise dosing) may be necessary based on desired ventricular response. 

 Table 1 Step-Wise Dosing 

 Step 

 Action 

 1 

 Optional loading dose (500 mcg per kg over 1 minute), then 50 mcg per kg per min for 4 min 

 2 

 Optional loading dose if necessary, then 100 mcg per kg per min for 4 min 

 3 

 Optional loading dose if necessary, then 150 mcg per kg per min for 4 min 

 4 

 If necessary, increase dose to 200 mcg per kg per min 

 In the absence of loading doses, continuous infusion of a single concentration of esmolol reaches pharmacokinetic and pharmacodynamic steady-state in about 30 minutes. 
 The effective maintenance dose for continuous and step-wise dosing is 50 to 200 mcg per kg per minute, although doses as low as 25 mcg per kg per minute have been adequate. Dosages greater than 200 mcg per kg per minute provide little added heart rate lowering effect, and the rate of adverse reactions increases. 
 Maintenance infusions may be continued for up to 48 hours. 

 2.2 Intraoperative and Postoperative Tachycardia and Hypertension 

 In this setting it is not always advisable to slowly titrate to a therapeutic effect. Therefore two dosing options are presented: immediate control and gradual control. 

 Immediate Control 

 • Administer 1 mg per kg as a bolus dose over 30 seconds followed by an infusion of 150 mcg per kg per min if necessary. 

 • Adjust the infusion rate as required to maintain desired heart rate and blood pressure. Refer to Maximum Recommended Doses below. 

 Gradual Control 

 • Administer 500 mcg per kg as a bolus dose over 1 minute followed by a maintenance infusion of 50 mcg per kg per min for 4 minutes. 

 • Depending on the response obtained, continue dosing as outlined for supraventricular tachycardia. Refer to Maximum Recommended Doses below. 

 Maximum Recommended Doses 

 • For the treatment of tachycardia, maintenance infusion dosages greater than 200 mcg per kg per min are not recommended; dosages greater than 200 mcg per kg per min provide little additional heart rate-lowering effect, and the rate of adverse reactions increases. 

 • For the treatment of hypertension, higher maintenance infusion dosages (250-300 mcg per kg per min) may be required. The safety of doses above 300 mcg per kg per minute has not been studied. 

 2.3 Transition from BREVIBLOC Injection Therapy to Alternative Drugs 

 After patients achieve adequate control of the heart rate and a stable clinical status, transition to alternative antiarrhythmic drugs may be accomplished. 
 When transitioning from BREVIBLOC to alternative drugs, the physician should carefully consider the labeling instructions of the alternative drug selected and reduce the dosage of BREVIBLOC as follows: 

 • Thirty minutes following the first dose of the alternative drug, reduce the BREVIBLOC infusion rate by one-half (50%). 

 • After administration of the second dose of the alternative drug, monitor the patient’s response and if satisfactory control is maintained for the first hour, discontinue the BREVIBLOC infusion. 

 2.4 Directions for Use 

 BREVIBLOC injection is available in a pre-mixed bag and ready-to-use vial. BREVIBLOC is not compatible with Sodium Bicarbonate (5%) solution (limited stability) or furosemide (precipitation). 
 Parenteral drug products should be inspected visually for particulate matter and discoloration prior to administration, whenever solution and container permit. 

 Premixed Bag 

 • The medication port is to be used solely for withdrawing an initial bolus from the bag. 

 • Use aseptic technique when withdrawing the bolus dose. 

 • Do not add any additional 

 Figure 1: Two-Port INTRAVIA Bag 

 Figure 1: Two-Port INTRAVIA Bag 
 Figure 1: Two-Port INTRAVIA Bag 

 • medications to the bag. 

 Figure 1: Two-Port INTRAVIA Bag 

 Figure 1: Two-Port INTRAVIA Bag 

 Ready-to-Use Vial 

 The Ready-to-use Vial may be used to administer a loading dosage by hand-held syringe while the maintenance infusion is being prepared [see How Supplied/Storage and Handling ( 16.2 )]. 

 Compatibility with Commonly Used Intravenous Fluids 

 BREVIBLOC was tested for compatibility with ten commonly used intravenous fluids at a final concentration of 10 mg esmolol hydrochloride per mL. BREVIBLOC was found to be compatible with the following solutions and was stable for at least 24 hours at controlled room temperature or under refrigeration: 

 • Dextrose (5%) Injection, USP 

 • Dextrose (5%) in Lactated Ringer’s Injection 

 • Dextrose (5%) in Ringer’s Injection 

 • Dextrose (5%) and Sodium Chloride (0.45%) Injection, USP 

 • Dextrose (5%) and Sodium Chloride (0.9%) Injection, USP 

 • Lactated Ringer’s Injection, USP 

 • Potassium Chloride (40 mEq/liter) in Dextrose (5%) Injection, USP 

 • Sodium Chloride (0.45%) Injection, USP 

 • Sodium Chloride (0.9%) Injection, USP</Section>
    

    <Section id="34070-3" name="CONTRAINDICATIONS SECTION">
4 CONTRAINDICATIONS 

 BREVIBLOC (Esmolol Hydrochloride) is contraindicated in patients with: 

 • Severe sinus bradycardia: May precipitate or worsen bradycardia resulting in cardiogenic shock and cardiac arrest [see Warnings and Precautions ( 5.2 )]. 

 • Heart block greater than first degree: Second- or third-degree atrioventricular block may precipitate or worsen bradycardia resulting in cardiogenic shock and cardiac arrest [see Warnings and Precautions ( 5.2 )]. 

 • Sick sinus syndrome: May precipitate or worsen bradycardia resulting in cardiogenic shock and cardiac arrest [see Warnings and Precautions ( 5.2 )]. 

 • Decompensated heart failure: May worsen heart failure. 

 • Cardiogenic shock: May precipitate further cardiovascular collapse and cause cardiac arrest. 

 • IV administration of cardiodepressant calcium-channel antagonists (e.g., verapamil) and BREVIBLOC in close proximity (i.e., while cardiac effects from the other are still present); fatal cardiac arrests have occurred in patients receiving BREVIBLOC and intravenous verapamil. 

 • Pulmonary hypertension: May precipitate cardiorespiratory compromise. 

 • Hypersensitivity reactions, including anaphylaxis, to esmolol or any of the inactive ingredients of the product (cross-sensitivity between beta blockers is possible). 

 • Severe sinus bradycardia ( 4 ) 

 • Heart block greater than first degree ( 4 ) 

 • Sick sinus syndrome ( 4 ) 

 • Decompensated heart failure ( 4 ) 

 • Cardiogenic shock ( 4 ) 

 • Coadministration of IV cardiodepressant calcium-channel antagonists (e.g. verapamil) in close proximity to BREVIBLOC injection ( 4 , 7 ) 

 • Pulmonary hypertension ( 4 ) 

 • Known hypersensitivity to esmolol ( 4 )</Section>
    

    <Section id="43685-7" name="WARNINGS AND PRECAUTIONS SECTION">
5 WARNINGS AND PRECAUTIONS 

 • Risk of hypotension, bradycardia, and cardiac failure: Monitor for signs and symptoms of cardiovascular adverse effects. Reduce or discontinue use ( 5.1 , 5.2 , 5.3 , 5.10 ) 

 • Risk of exacerbating reactive airway disease ( 5.5 ) 

 • Diabetes mellitus: Increases the effect of hypoglycemic agents and masks hypoglycemic tachycardia ( 5.6 ) 

 • Risk of unopposed alpha-agonism and severe hypertension in untreated pheochromocytoma ( 5.9 ) 

 • Risk of myocardial ischemia when abruptly discontinued in patients with coronary artery disease ( 5.12 , 5.15 ) 

 5.1 Hypotension 

 Hypotension can occur at any dose but is dose-related. Patients with hemodynamic compromise or on interacting medications are at particular risk. Severe reactions may include loss of consciousness, cardiac arrest, and death. For control of ventricular heart rate, maintenance doses greater than 200 mcg per kg per min are not recommended. Monitor patients closely, especially if pretreatment blood pressure is low. In case of an unacceptable drop in blood pressure, reduce or stop BREVIBLOC injection. Decrease of dose or termination of infusion reverses hypotension, usually within 30 minutes. 

 5.2 Bradycardia 

 Bradycardia, including sinus pause, heart block, severe bradycardia, and cardiac arrest have occurred with the use of BREVIBLOC injection. Patients with first-degree atrioventricular block, sinus node dysfunction, or conduction disorders may be at increased risk. Monitor heart rate and rhythm in patients receiving BREVIBLOC [see Contraindications (4)]. 

 If severe bradycardia develops, reduce or stop BREVIBLOC. 

 5.3 Cardiac Failure 

 Beta blockers, like BREVIBLOC injection, can cause depression of myocardial contractility and may precipitate heart failure and cardiogenic shock. At the first sign or symptom of impending cardiac failure, stop BREVIBLOC and start supportive therapy [see Overdosage ( 10 )] . 

 5.4 Intraoperative and Postoperative Tachycardia and/or Hypertension 

 Monitor vital signs closely and titrate BREVIBLOC slowly in the treatment of patients whose blood pressure is primarily driven by vasoconstriction associated with hypothermia. 

 5.5 Reactive Airways Disease 

 Patients with reactive airways disease should, in general, not receive beta blockers. Because of its relative beta 1 selectivity and titratability, titrate BREVIBLOC to the lowest possible effective dose. In the event of bronchospasm, stop the infusion immediately; a beta 2 stimulating agent may be administered with appropriate monitoring of ventricular rates. 

 5.6 Use in Patients with Diabetes Mellitus and Hypoglycemia 

 In patients with hypoglycemia, or diabetic patients (especially those with labile diabetes) who are receiving insulin or other hypoglycemic agents, beta blockers may mask tachycardia occurring with hypoglycemia, but other manifestations such as dizziness and sweating may not be masked. 
 Concomitant use of beta blockers and antidiabetic agents can enhance the effect of antidiabetic agents (blood glucose–lowering). 

 5.7 Infusion Site Reactions 

 Infusion site reactions have occurred with the use of BREVIBLOC injection. They include irritation, inflammation, and severe reactions (thrombophlebitis, necrosis, and blistering), in particular when associated with extravasation [see Adverse Reactions ( 6 )]. Avoid infusions into small veins or through a butterfly catheter. 
 If a local infusion site reaction develops, use an alternative infusion site and avoid extravasation. 

 5.8 Use in Patients with Prinzmetal's Angina 

 Beta blockers may exacerbate anginal attacks in patients with Prinzmetal’s angina because of unopposed alpha receptor–mediated coronary artery vasoconstriction. Do not use nonselective beta blockers. 

 5.9 Use in Patients with Pheochromocytoma 

 If BREVIBLOC is used in the setting of pheochromocytoma, give it in combination with an alpha-blocker, and only after the alpha-blocker has been initiated. Administration of beta-blockers alone in the setting of pheochromocytoma has been associated with a paradoxical increase in blood pressure from the attenuation of beta-mediated vasodilation in skeletal muscle. 

 5.10 Use in Hypovolemic Patients 

 In hypovolemic patients, BREVIBLOC injection can attenuate reflex tachycardia and increase the risk of hypotension. 

 5.11 Use in Patients with Peripheral Circulatory Disorders 

 In patients with peripheral circulatory disorders (including Raynaud’s disease or syndrome, and peripheral occlusive vascular disease), BREVIBLOC may aggravate peripheral circulatory disorders. 

 5.12 Abrupt Discontinuation of BREVIBLOC Injection 

 Severe exacerbations of angina, myocardial infarction, and ventricular arrhythmias have been reported in patients with coronary artery disease upon abrupt discontinuation of beta blocker therapy. Observe patients for signs of myocardial ischemia when discontinuing BREVIBLOC. 
 Heart rate increases moderately above pretreatment levels 30 minutes after BREVIBLOC discontinuation. 

 5.13 Hyperkalemia 

 Beta blockers, including BREVIBLOC, have been associated with increases in serum potassium levels and hyperkalemia. The risk is increased in patients with risk factors such as renal impairment. Intravenous administration of beta blockers has been reported to cause potentially life-threatening hyperkalemia in hemodialysis patients. Monitor serum electrolytes during therapy with BREVIBLOC. 

 5.14 Use in Patients with Metabolic Acidosis 

 Beta blockers, including BREVIBLOC, have been reported to cause hyperkalemic renal tubular acidosis. Acidosis in general may be associated with reduced cardiac contractility. 

 5.15 Use in Patients with Hyperthyroidism 

 Beta-adrenergic blockade may mask certain clinical signs (e.g., tachycardia) of hyperthyroidism. Abrupt withdrawal of beta blockade might precipitate a thyroid storm; therefore, monitor patients for signs of thyrotoxicosis when withdrawing beta blocking therapy. 

 5.16 Use in Patients at Risk of Severe Acute Hypersensitivity Reactions 

 When using beta blockers, patients at risk of anaphylactic reactions may be more reactive to allergen exposure (accidental, diagnostic, or therapeutic). 
 Patients using beta blockers may be unresponsive to the usual doses of epinephrine used to treat anaphylactic or anaphylactoid reactions [see Drug Interactions ( 7 )] .</Section>
    

    <Section id="34073-7" name="DRUG INTERACTIONS SECTION">
7 DRUG INTERACTIONS 

 Concomitant use of BREVIBLOC injection with other drugs that can lower blood pressure, reduce myocardial contractility, or interfere with sinus node function or electrical impulse propagation in the myocardium can exaggerate BREVIBLOC’s effects on blood pressure, contractility, and impulse propagation. Severe interactions with such drugs can result in, for example, severe hypotension, cardiac failure, severe bradycardia, sinus pause, sinoatrial block, atrioventricular block, and/or cardiac arrest. In addition, with some drugs, beta blockade may precipitate increased withdrawal effects. (See clonidine, guanfacine, and moxonidine below.) BREVIBLOC should therefore be used only after careful individual assessment of the risks and expected benefits in patients receiving drugs that can cause these types of pharmacodynamic interactions, including but not limited to: 

 • Digitalis glycosides: Concomitant administration of digoxin and BREVIBLOC leads to an approximate 10% to 20% increase of digoxin blood levels at some time points. Digoxin does not affect BREVIBLOC pharmacokinetics. Both digoxin and beta blockers slow atrioventricular conduction and decrease heart rate. Concomitant use increases the risk of bradycardia. 

 • Anticholinesterases: BREVIBLOC prolonged the duration of succinylcholine-induced neuromuscular blockade and moderately prolonged clinical duration and recovery index of mivacurium. 

 • Antihypertensive agents clonidine, guanfacine, or moxonidine: Beta blockers also increase the risk of clonidine-, guanfacine-, or moxonidine-withdrawal rebound hypertension. If, during concomitant use of a beta blocker, antihypertensive therapy needs to be interrupted or discontinued, discontinue the beta blocker first, and the discontinuation should be gradual. 

 • Calcium channel antagonists: In patients with depressed myocardial function, use of BREVIBLOC with cardiodepressant calcium channel antagonists (e.g., verapamil) can lead to fatal cardiac arrests. 

 • Sympathomimetic drugs: Sympathomimetic drugs having beta-adrenergic agonist activity will counteract effects of BREVIBLOC. 

 • Vasoconstrictive and positive inotropic agents: Because of the risk of reducing cardiac contractility in presence of high systemic vascular resistance, do not use BREVIBLOC to control tachycardia in patients receiving drugs that are vasoconstrictive and have positive inotropic effects, such as epinephrine, norepinephrine, and dopamine. 

 • Digitalis glycosides: Risk of bradycardia ( 7 ) 

 • Anticholinesterases: Prolongs neuromuscular blockade ( 7 ) 

 • Antihypertensive agents: Risk of rebound hypertension ( 7 ) 

 • Sympathomimetic drugs: Dose adjustment needed ( 7 ) 

 • Vasoconstrictive and positive inotropic effect substances: Avoid concomitant use ( 7 )</Section>
    

  </Text>
  <Sentences>
    <Sentence LabelDrug="Brevibloc" id="206" section="34068-7">
      

      <SentenceText>BREVIBLOC is not compatible with Sodium Bicarbonate ( 5 % ) solution ( limited stability ) or furosemide ( precipitation ) .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="207" section="34070-3">
      

      <SentenceText>Coadministration of IV cardiodepressant calcium-channel antagonists ( e.g. verapamil ) in close proximity to BREVIBLOC injection ( 4 , 7 ) .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="208" section="34070-3">
      

      <SentenceText>Hypersensitivity reactions , including anaphylaxis , to esmolol or any of the inactive ingredients of the product ( cross-sensitivity between beta blockers is possible ) .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="209" section="34070-3">
      

      <SentenceText>IV administration of cardiodepressant calcium-channel antagonists ( e.g. , verapamil ) and BREVIBLOC in close proximity ( i.e. , while cardiac effects from the other are still present ) , fatal cardiac arrests have occurred in patients receiving BREVIBLOC and intravenous verapamil .</SentenceText>
      

      <Mention code="NO MAP" id="M1" span="194 7" str="cardiac" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M2" span="38 27" str="calcium-channel antagonists" type="Precipitant"/>
      <Interaction effect="M1" id="I1" precipitant="M2" trigger="M2" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M3" span="75 9" str="verapamil" type="Precipitant"/>
      <Interaction effect="M1" id="I2" precipitant="M3" trigger="M3" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="210" section="34073-7">
      

      <SentenceText>(See clonidine, guanfacine, and moxonidine below.)</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="211" section="34073-7">
      

      <SentenceText>Anticholinesterases : BREVIBLOC prolonged the duration of succinylcholine-induced neuromuscular blockade and moderately prolonged clinical duration and recovery index of mivacurium .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="212" section="34073-7">
      

      <SentenceText>Antihypertensive agents clonidine, guanfacine, or moxonidine: Beta blockers also increase the risk of clonidine-, guanfacine-, or moxonidine-withdrawal rebound hypertension.</SentenceText>
      

      <Mention code="NO MAP" id="M4" span="152 20" str="rebound hypertension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M5" span="24 9" str="clonidine" type="Precipitant"/>
      <Interaction effect="M4" id="I3" precipitant="M5" trigger="M5" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M6" span="35 10" str="guanfacine" type="Precipitant"/>
      <Interaction effect="M4" id="I4" precipitant="M6" trigger="M6" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M7" span="50 10" str="moxonidine" type="Precipitant"/>
      <Interaction effect="M4" id="I5" precipitant="M7" trigger="M7" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="213" section="34073-7">
      

      <SentenceText>Both digoxin and beta blockers slow atrioventricular conduction and decrease heart rate.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="214" section="34073-7">
      

      <SentenceText>BREVIBLOC should therefore be used only after careful individual assessment of the risks and expected benefits in patients receiving drugs that can cause these types of pharmacodynamic interactions , including but not limited to : .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="215" section="34073-7">
      

      <SentenceText>Calcium channel antagonists : In patients with depressed myocardial function , use of BREVIBLOC with cardiodepressant calcium channel antagonists ( e.g. , verapamil ) can lead to fatal cardiac arrests .</SentenceText>
      

      <Mention code="NO MAP" id="M8" span="179 21" str="fatal cardiac arrests" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M9" span="118 27" str="calcium channel antagonists" type="Precipitant"/>
      <Interaction effect="M8" id="I6" precipitant="M9" trigger="M9" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M10" span="155 9" str="verapamil" type="Precipitant"/>
      <Interaction effect="M8" id="I7" precipitant="M10" trigger="M10" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="216" section="34073-7">
      

      <SentenceText>Concomitant use increases the risk of bradycardia.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="217" section="34073-7">
      

      <SentenceText>Concomitant use of BREVIBLOC injection with other drugs that can lower blood pressure , reduce myocardial contractility , or interfere with sinus node function or electrical impulse propagation in the myocardium can exaggerate BREVIBLOC s effects on blood pressure , contractility , and impulse propagation .</SentenceText>
      

      <Mention code="NO MAP" id="M11" span="65 20" str="lower blood pressure" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M12" span="88 31" str="reduce myocardial contractility" type="SpecificInteraction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="218" section="34073-7">
      

      <SentenceText>Digitalis glycosides : Concomitant administration of digoxin and BREVIBLOC leads to an approximate 10 % to 20 % increase of digoxin blood levels at some time points .</SentenceText>
      

      <Mention code="NO MAP" id="M13" span="53 7" str="digoxin" type="Precipitant"/>
      <Interaction effect="C54357" id="I8" precipitant="M13" trigger="M13" type="Pharmacokinetic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="219" section="34073-7">
      

      <SentenceText>Digitalis glycosides: Risk of bradycardia (7) Anticholinesterases: Prolongs neuromuscular blockade (7) Antihypertensive agents: Risk of rebound hypertension (7) Sympathomimetic drugs: Dose adjustment needed (7) Vasoconstrictive and positive inotropic effect substances: Avoid concomitant use (7)</SentenceText>
      

      <Mention code="NO MAP" id="M14" span="30 11" str="bradycardia" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M15" span="67 31" str="Prolongs neuromuscular blockade" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M16" span="136 20" str="rebound hypertension" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M17" span="184 22" str="Dose adjustment needed" type="Trigger"/>
      <Mention code="NO MAP" id="M18" span="270 5" str="Avoid" type="Trigger"/>
      <Mention code="NO MAP" id="M19" span="0 20" str="Digitalis glycosides" type="Precipitant"/>
      <Interaction effect="M14" id="I9" precipitant="M19" trigger="M18" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M20" span="46 19" str="Anticholinesterases" type="Precipitant"/>
      <Interaction effect="M15" id="I10" precipitant="M20" trigger="M17" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M21" span="103 23" str="Antihypertensive agents" type="Precipitant"/>
      <Interaction effect="M16" id="I11" precipitant="M21" trigger="M17" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M22" span="161 21" str="Sympathomimetic drugs" type="Precipitant"/>
      <Interaction id="I12" precipitant="M22" trigger="M17" type="Unspecified interaction"/>
      <Mention code="NO MAP" id="M23" span="232 36" str="positive inotropic effect substances" type="Precipitant"/>
      <Interaction id="I13" precipitant="M23" trigger="M17" type="Unspecified interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="220" section="34073-7">
      

      <SentenceText>Digoxin does not affect BREVIBLOC pharmacokinetics .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="221" section="34073-7">
      

      <SentenceText>If, during concomitant use of a beta blocker, antihypertensive therapy needs to be interrupted or discontinued, discontinue the beta blocker first, and the discontinuation should be gradual.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="222" section="34073-7">
      

      <SentenceText>In addition, with some drugs, beta blockade may precipitate increased withdrawal effects.</SentenceText>
      

      <Mention code="NO MAP" id="M24" span="70 18" str="withdrawal effects" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M25" span="30 13" str="beta blockade" type="Precipitant"/>
      <Interaction effect="M24" id="I14" precipitant="M25" trigger="M25" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="223" section="34073-7">
      

      <SentenceText>Severe interactions with such drugs can result in, for example, severe hypotension, cardiac failure, severe bradycardia, sinus pause, sinoatrial block, atrioventricular block, and/or cardiac arrest.</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="224" section="34073-7">
      

      <SentenceText>Sympathomimetic drugs : Sympathomimetic drugs having beta-adrenergic agonist activity will counteract effects of BREVIBLOC .</SentenceText>
      

      <Mention code="NO MAP" id="M26" span="91 31" str="counteract effects of BREVIBLOC" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M27" span="0 21" str="Sympathomimetic drugs" type="Precipitant"/>
      <Interaction effect="M26" id="I15" precipitant="M27" trigger="M27" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="225" section="34073-7">
      

      <SentenceText>Vasoconstrictive and positive inotropic agents : Because of the risk of reducing cardiac contractility in presence of high systemic vascular resistance , do not use BREVIBLOC to control tachycardia in patients receiving drugs that are vasoconstrictive and have positive inotropic effects , such as epinephrine , norepinephrine , and dopamine .</SentenceText>
      

      <Mention code="NO MAP" id="M28" span="72 30" str="reducing cardiac contractility" type="SpecificInteraction"/>
      <Mention code="NO MAP" id="M29" span="21 25" str="positive inotropic agents" type="Precipitant"/>
      <Interaction effect="M28" id="I16" precipitant="M29" trigger="M29" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M30" span="298 11" str="epinephrine" type="Precipitant"/>
      <Interaction effect="M28" id="I17" precipitant="M30" trigger="M30" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M31" span="312 14" str="norepinephrine" type="Precipitant"/>
      <Interaction effect="M28" id="I18" precipitant="M31" trigger="M31" type="Pharmacodynamic interaction"/>
      <Mention code="NO MAP" id="M32" span="333 8" str="dopamine" type="Precipitant"/>
      <Interaction effect="M28" id="I19" precipitant="M32" trigger="M32" type="Pharmacodynamic interaction"/>
    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="226" section="43685-7">
      

      <SentenceText>Beta blockers , including BREVIBLOC , have been associated with increases in serum potassium levels and hyperkalemia .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="227" section="43685-7">
      

      <SentenceText>Beta blockers , including BREVIBLOC , have been reported to cause hyperkalemic renal tubular acidosis .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="228" section="43685-7">
      

      <SentenceText>Beta blockers , like BREVIBLOC injection , can cause depression of myocardial contractility and may precipitate heart failure and cardiogenic shock .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="229" section="43685-7">
      

      <SentenceText>If BREVIBLOC is used in the setting of pheochromocytoma , give it in combination with an alpha-blocker , and only after the alpha-blocker has been initiated .</SentenceText>
      

    </Sentence>
    

    <Sentence LabelDrug="Brevibloc" id="230" section="43685-7">
      

      <SentenceText>Monitor serum electrolytes during therapy with BREVIBLOC .</SentenceText>
      

    </Sentence>
    

  </Sentences>
  <LabelInteractions/>
</Label>

